; -------------------------------------------
; Spy VS Spy III - Arctic Antics.
; Title screen.
; Disassembled by Franck Charlet.
; -------------------------------------------

                    SECTION  spy3titles000000,CODE_c
ProgStart:
START_CODE:         jmp      START

PLANES:             dc.w     $E2,0,$E0,0,$E6,0,$E4,0,$EA,0,$E8,0,$EE,0,$EC,0
COPPER_SPRITE:      dc.w     $120
lbW000028:          dc.w     0,$122,0,$124,0,$126,0,$128,0,$12A,0,$12C,0,$12E
                    dc.w     0,$130,0,$132,0,$134,0,$136,0,$138,0,$13A,0,$13C
                    dc.w     0,$13E,0
                    dc.w     $1fc, 0, $106, 0
                    dc.w     $FFFF,$FFFE
DUMMY_SPRITE:       dc.w     0,0

WAIT_FOR_RASTER:    move.l   d1,d2
                    add.w    #$B00,d2
lbC000078:          move.l   $DFF004,d0
                    and.l    #$1FFFF,d0
                    cmp.l    d1,d0
                    bls.s    lbC000078
                    cmp.l    d2,d0
                    bhi.s    lbC000078
                    rts

ATARI_COPY:         move.l   #$F9F,d0
lbC000094:          move.w   (a0)+,(a1)+
                    move.w   (a0)+,7998(a1)
                    move.w   (a0)+,15998(a1)
                    move.w   (a0)+,23998(a1)
                    dbra     d0,lbC000094
                    rts

COLOUR_COPY:        lea      $DFF180,a1
                    move.w   #15,d0
lbC0000B2:          move.w   (a0)+,d1
                    lsl.w    #1,d1
                    move.w   d1,(a1)+
                    dbra     d0,lbC0000B2
                    rts

LOAD_FILE:          move.l   d2,-(sp)
                    move.l   d1,-(sp)
                    move.l   4,a6
                    moveq    #0,d0
                    move.l   #DOS_NAME,a1
                    jsr      -552(a6)
                    move.l   d0,DOS_BASE
                    move.l   d0,a6
                    move.l   (sp)+,d1
                    move.l   #$3ED,d2
                    jsr      -30(a6)
                    move.l   d0,DOS_HANDLE
                    move.l   d0,d1
                    move.l   (sp)+,d2
                    move.l   #$7D80,d3
                    jsr      -42(a6)
                    move.l   DOS_HANDLE,d1
                    jsr      -36(a6)
                    rts

SETUP_SCREEN:       move.l   ONESCREEN,SCREEN1
                    move.l   TWOSCREEN,SCREEN2
                    lea      $DFF000,a6
                    move.w   #$3FF,$96(a6)
                    move.l   SCREEN2,d1
                    bsr      WHICH_PLANES
                    move.w   #7,d0
                    lea      lbW000028,a0
                    move.l   #DUMMY_SPRITE,d1
lbC000142:          swap     d1
                    move.w   d1,(a0)
                    addq.w   #4,a0
                    swap     d1
                    move.w   d1,(a0)
                    addq.w   #4,a0
                    dbra     d0,lbC000142
                    move.w   #$34,d0
                    subq.w   #1,d0
                    lea      PLANES,a0
                    lea      COPPER_LIST2,a1
lbC000164:          move.w   (a0)+,(a1)+
                    dbra     d0,lbC000164
                    move.l   SCREEN1,d1
                    bsr      WHICH_PLANES
                    move.l   #PLANES,COPPER1
                    move.l   #COPPER_LIST2,COPPER2
                    move.l   #PLANES,$80(a6)
                    move.w   #$3070,$8E(a6)
                    move.w   #$F8B1,$90(a6)
                    move.w   #$30,$92(a6)
                    move.w   #$C8,$94(a6)
                    clr.w    $108(a6)
                    clr.w    $10A(a6)
                    clr.w    $102(a6)
                    move.w   #$4200,$100(a6)
                    move.w   #4,$104(a6)
                    clr.w    $88(a6)
                    move.w   #$83DF,$96(a6)
                    rts

WHICH_PLANES:       move.w   #3,d0
                    lea      PLANES,a0
lbC0001D6:          move.w   d1,2(a0)
                    swap     d1
                    move.w   d1,6(a0)
                    swap     d1
                    add.l    #$1F40,d1
                    addq.w   #8,a0
                    dbra     d0,lbC0001D6
                    rts

BLIT:               bsr      WAIT_BLIT
                    move.l   d0,$DFF060
                    move.l   d1,$DFF064
                    move.l   d2,$DFF040
                    move.w   #$FFFF,$DFF044
                    move.w   #$FFFF,$DFF046
BLIT2:              move.l   a0,$DFF050
                    move.l   a1,$DFF04C
                    move.l   a2,$DFF048
                    move.l   a3,$DFF054
                    move.w   d3,$DFF058
                    rts

WAIT_BLIT:          btst     #6,$DFF002
                    bne.s    WAIT_BLIT
                    rts

SWAP_SCREEN:        movem.l  d0/d1,-(sp)
                    bsr.s    WAIT_BLIT
                    move.l   SCREEN1,d0
                    move.l   SCREEN2,SCREEN1
                    move.l   d0,SCREEN2
                    move.l   COPPER1,d0
                    move.l   COPPER2,COPPER1
                    move.l   d0,COPPER2
                    move.l   COPPER1,$DFF080
                    move.w   #1,INT_REQ
lbC000286:          tst.w    INT_REQ
                    bne.s    lbC000286
                    movem.l  (sp)+,d0/d1
                    rts

CLEAR_SCREEN:       move.w   #$7CF,d0
lbC000298:          clr.l    (a0)+
                    clr.l    (a0)+
                    clr.l    (a0)+
                    clr.l    (a0)+
                    dbra     d0,lbC000298
                    rts

ALLOCATE_MEMORY:    move.l   #SCREENA,ONESCREEN
                    move.l   #SCREENB,TWOSCREEN
                    move.l   #SCREENC,BACK
                    rts

FIND32K:            move.l   4,a6
                    move.l   #$8000,d0
                    move.l   #2,d1
                    jsr      -198(a6)
                    tst.l    d0
                    beq      WHOS_REMOVED_THE_RAM_CHIPS
                    rts

WHOS_REMOVED_THE_RAM_CHIPS:
                    move.w   d7,$DFF180
                    move.w   d6,$DFF180
                    bra.s    WHOS_REMOVED_THE_RAM_CHIPS

DECO_PIC:           move.l   d2,-(sp)
                    move.l   BACK,d2
                    bsr      LOAD_FILE
                    move.l   (sp)+,a1
DECO:               move.l   BACK,a0
                    addq.w   #2,a0
                    addq.w   #4,a1
                    move.w   #15,d0
DECO_PAL:           move.w   (a0)+,(a1)+
                    dbra     d0,DECO_PAL
                    add.w    #$5C,a1
                    move.w   #3,d0
DECO_THREE:         move.l   a1,a2
                    move.w   #$1F40,d1
                    clr.w    FLAG
DECO_LOOP:          move.b   (a0)+,d2
                    cmp.b    #$CD,d2
                    bne      DECO_ORD
                    move.b   (a0)+,d2
                    bne      NOT_CD_DECO
                    move.b   #$CD,d2
                    bra      DECO_ORD

NOT_CD_DECO:        bra      DECO_REP

DECO_ORD:           move.b   d2,(a1)
                    tst.w    FLAG
                    bne      ADD_LOTS
                    addq.w   #1,FLAG
                    addq.w   #1,a1
                    subq.w   #1,d1
                    beq      DONE_DECO
                    bra.s    DECO_LOOP

ADD_LOTS:           clr.w    FLAG
                    addq.w   #7,a1
                    subq.w   #1,d1
                    beq      DONE_DECO
                    bra.s    DECO_LOOP

DECO_REP:           move.w   d2,d3
                    and.w    #$7F,d3
                    tst.b    d2
                    bmi      USE_FF
                    move.w   #0,d2
                    bra      DECO_LOTS

USE_FF:             move.w   #$FFFF,d2
DECO_LOTS:          move.b   d2,(a1)
                    tst.w    FLAG
                    bne      ADD_LOTS2
                    add.w    #1,FLAG
                    add.w    #1,a1
                    sub.w    #1,d1
                    beq      DONE_DECO
                    sub.b    #1,d3
                    bne.s    DECO_LOTS
                    bra      DECO_LOOP

ADD_LOTS2:          clr.w    FLAG
                    addq.w   #7,a1
                    subq.w   #1,d1
                    beq      DONE_DECO
                    subq.b   #1,d3
                    bne.s    DECO_LOTS
                    bra      DECO_LOOP

DONE_DECO:          lea      2(a2),a1
                    dbra     d0,DECO_THREE
                    rts

FLAG:               dc.w     0
DOS_NAME:           dc.b     'dos.library',0
DOS_BASE:           dc.l     0
DOS_HANDLE:         dc.l     0
ONESCREEN:          dc.l     0
TWOSCREEN:          dc.l     0
LOADSCREEN:         dc.l     0
SCREEN2:            dc.l     0
SCREEN1:            dc.l     0
COPPER1:            dc.l     0
COPPER2:            dc.l     0
COPPER_LIST2:       dcb.l    $1A,0

                    jmp      TEST

FREQS:              dc.w     $194,$17D,$167,$153,$140,$12E,$11D,$10D,$FE,$F0
                    dc.w     $E2,$D5
ENVELOPE1:          dc.w     1,$32,0,$19,$FFFE,0,$FFFF
ENVELOPE2:          dc.w     1,$1E,0,1,$FFFF,$FFFC,1,0,4,1,$FFFF,$FFFC,1,0,4,1
                    dc.w     $FFFF,$FFFC,1,0,4,1,$FFFF,$FFFC,1,0,4,1,$FFFF
                    dc.w     $FFFC,1,0,4,1,$FFFF,$FFFC,1,0,4,1,$FFFF,$FFFC,1,0
                    dc.w     4,1,$FFFF,$FFFC,1,0,4,1,$FFFF,$FFFC,1,0,4,1,$FFFF
                    dc.w     $FFFC,1,0,4,1,$FFFF,$FFFC,1,0,4,1,$FFFF,$FFFC,1,0
                    dc.w     4,1,$FFFF,$FFFC,1,0,4,1,$FFFF,$FFFC,1,0,4,1,$FFFF
                    dc.w     $FFFC,1,0,4,1,$FFFF,$FFFC,1,0,4,1,$FFFF,$FFFC,1,0
                    dc.w     4,1,$FFFF,$FFFC,1,0,4,1,$FFFF,$FFFC,1,0,4,1,$FFFF
                    dc.w     $FFFC,1,0,4,1,$FFFF,$FFFC,1,0,4,1,$FFFF,$FFFC,1,0
                    dc.w     4,1,$FFFF,$FFFC,1,0,4,1,$FFFF,$FFFC,1,0,4,1,$FFFF
                    dc.w     $FFFC,1,0,4,1,$FFFF,$FFFC,1,0,4,1,$FFFF,$FFFC,1,0
                    dc.w     4,1,$FFFF,$FFFC,1,0,4,1,$FFFF,$FFFC,1,0,4,1,$FFFF
                    dc.w     $FFFC,1,0,4,$FFFF
ENVELOPE3:          dc.w     1,$1E,0,15,$FFFE,1,$FFFF
DUMMY:              dc.w     $FFFF
TRIANGLE32:         dc.b     $80,$90,$A0,$50,$C0,$D0,$E0,$F0,0,$10,$20,$30,$40
                    dc.b     $50,$60,$70,$7F,$70,$60,$50,$40,$30,$20,$10,0,$F0
                    dc.b     $E0,$D0,$C0,$B0,$A0,$90
WAVEFORM128:        dc.b     0,6,13,$13,$19,$1F,$25,$2B,$31,$37,$3C,$42,$47
                    dc.b     $4C,$51,$56,$5B,$5F,$63,$67,$6A,$6E,$71,$74,$76
                    dc.b     $79,$7A,$7C,$7E,$7F,$7F,$7F,$7F,$7F,$7F,$7F,$7E
                    dc.b     $7C,$7A,$79,$76,$74,$71,$6E,$6A,$67,$63,$5F,$5B
                    dc.b     $56,$51,$4C,$47,$42,$3C,$37,$31,$2B,$25,$1F,$19
                    dc.b     $13,13,6,0,$FA,$F3,$ED,$E7,$E1,$DB,$D5,$CF,$C9
                    dc.b     $C4,$BE,$B9,$B4,$AF,$AA,$A5,$A1,$9D,$99,$96,$92
                    dc.b     $8F,$8C,$8A,$87,$86,$84,$82,$81,$81,$80,$80,$80
                    dc.b     $81,$81,$82,$84,$86,$87,$8A,$8C,$8F,$92,$96,$99
                    dc.b     $9D,$A1,$A5,$AA,$AF,$B4,$B9,$BE,$C4,$C9,$CF,$D5
                    dc.b     $DB,$E1,$E7,$ED,$F3,$FA
WAVEFORM64:         dc.b     0,13,$19,$25,$31,$3C,$47,$51,$5B,$63,$6A,$71,$76
                    dc.b     $7A,$7E,$7F,$7F,$7F,$7E,$7A,$76,$71,$6A,$63,$5B
                    dc.b     $51,$47,$3C,$31,$25,$19,13,0,$F3,$E7,$DB,$CF,$C4
                    dc.b     $B9,$AF,$A5,$9D,$96,$8F,$8A,$86,$82,$81,$80,$81
                    dc.b     $82,$86,$8A,$8F,$96,$9D,$A5,$AF,$B9,$C4,$CF,$DB
                    dc.b     $E7,$F3
WAVEFORM32:         dc.b     0,$19,$31,$47,$5B,$6A,$76,$7E,$7F,$7E,$76,$6A,$5B
                    dc.b     $47,$31,$19,0,$E7,$CF,$B9,$A5,$96,$8A,$82,$80,$82
                    dc.b     $8A,$96,$A5,$B9,$CF,$E7
WAVEFORM16:         dc.b     0,$31,$5B,$76,$7F,$76,$5B,$31,0,$CF,$A5,$8A,$80
                    dc.b     $8A,$A5,$CF
WAVEFORM8:          dc.b     0,$5B,$7F,$5B,0,$A5,$80,$A5
WAVEFORM4:          dc.b     0,$7F,0,$80
WAVEFORM2:          dc.b     $C0,$40
WHICH_WAVE:         dc.l     WAVEFORM128
                    dc.l     WAVEFORM64
                    dc.l     WAVEFORM32
                    dc.l     WAVEFORM16
                    dc.l     WAVEFORM8
                    dc.l     WAVEFORM4
                    dc.l     WAVEFORM2
WAVE_LENGTH:        dc.w     $40,$20,$10,8,4,2,1
E_HIGH_PING:        dc.w     1,$20,0,5,0,1,$20,$FFFF,0,$FFFF
E_MEDIUM_PING:      dc.w     1,$20,0,5,0,1,$20,$FFFF,0,$FFFF
E_LOW_PING:         dc.w     1,$2C,0,$64,0,0,1,$FFD4,0,$FFFF
E_MEDIUM_BEEP:      dc.w     1,$40,0,$19,0,0,1,$FFC0,0,$FFFF
E_BANG:             dc.w     1,$40,0,$20,$FFFE,0,$FFFF
E_BOOM:             dc.w     1,$40,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0
                    dc.w     7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7
                    dc.w     0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0
                    dc.w     0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0
                    dc.w     1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1
                    dc.w     $FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1
                    dc.w     $FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1
                    dc.w     $FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1
                    dc.w     $FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1
                    dc.w     $FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1
                    dc.w     $FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1
                    dc.w     $FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1
                    dc.w     $FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1
                    dc.w     $FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1
                    dc.w     $FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1
                    dc.w     $FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1
                    dc.w     $FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1
                    dc.w     $FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1
                    dc.w     $FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1
                    dc.w     $FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1
                    dc.w     $FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1
                    dc.w     $FFFF,0,7,0,0,$FFFF
E_GRAVE:            dc.w     1,$40,0,$360,0,0,$40,$FFFF,0,$FFFF
E_BURY:             dc.w     $40,1,$FFFD,1,$FFC0,0,$46,0,0,$FFFF
E_SPLASH:           dc.w     $40,1,0,$50,0,$FFFC,$40,$FFFF,0,$FFFF
E_SUB:              dc.w     1,$18,0,10,0,$FFFE,$190,0,0,$18,$FFFF,0,$FFFF
E_FEET:             dc.w     10,1,$FFFB,1,$FFF6,0,$FFFF
E_PICK:             dc.w     1,$37,0,1,0,0,5,$FFF5,0,$28,0,0,1,$37,0,1,0,0,5
                    dc.w     $FFF5,0,$28,0,0,1,$37,0,1,0,0,5,$FFF5,0,$28,0,0,1
                    dc.w     $37,0,1,0,0,5,$FFF5,0,$28,0,0,1,$37,0,1,0,0,5
                    dc.w     $FFF5,0,$28,0,0,1,$37,0,1,0,0,5,$FFF5,0,$28,0,0,1
                    dc.w     $37,0,1,0,0,5,$FFF5,0,$28,0,0,1,$37,0,1,0,0,5
                    dc.w     $FFF5,0,$28,0,0,1,$37,0,1,0,0,5,$FFF5,0,$28,0,0,1
                    dc.w     $37,0,1,0,0,5,$FFF5,0,$28,0,0,1,$37,0,1,0,0,5
                    dc.w     $FFF5,0,$28,0,0,$FFFF
E_SAW:              dc.w     1,$1E,0,$14,0,0,1,$FFE2,0,$14,0,0,1,$1E,$3E8,$14
                    dc.w     0,0,1,$FFE2,$FC18,$14,0,0,1,$1E,0,$14,0,0,1,$FFE2
                    dc.w     0,$14,0,0,1,$1E,$3E8,$14,0,0,1,$FFE2,$FC18,$14,0
                    dc.w     0,1,$1E,0,$14,0,0,1,$FFE2,0,$14,0,0,1,$1E,$3E8
                    dc.w     $14,0,0,1,$FFE2,$FC18,$14,0,0,1,$1E,0,$14,0,0,1
                    dc.w     $FFE2,0,$14,0,0,1,$1E,$3E8,$14,0,0,1,$FFE2,$FC18
                    dc.w     $14,0,0,1,$1E,0,$14,0,0,1,$FFE2,0,$14,0,0,1,$1E
                    dc.w     $3E8,$14,0,0,1,$FFE2,$FC18,$14,0,0,1,$1E,0,$14,0
                    dc.w     0,1,$FFE2,0,$14,0,0,1,$1E,$3E8,$14,0,0,1,$FFE2
                    dc.w     $FC18,$14,0,0,$FFFF
E_KISS:             dc.w     1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1
                    dc.w     1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1
                    dc.w     0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0
                    dc.w     2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2
                    dc.w     0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0
                    dc.w     0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0
                    dc.w     1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1
                    dc.w     1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1
                    dc.w     0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0
                    dc.w     2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,$14,0,0,1,$FFFF,0,2
                    dc.w     0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0
                    dc.w     0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0
                    dc.w     1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0
                    dc.w     $578,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1
                    dc.w     0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0
                    dc.w     2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2
                    dc.w     0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0
                    dc.w     0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0
                    dc.w     1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1
                    dc.w     1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1
                    dc.w     0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0
                    dc.w     2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2
                    dc.w     0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,$14,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,$578,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1
                    dc.w     1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1
                    dc.w     0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0
                    dc.w     2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2
                    dc.w     0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0
                    dc.w     0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0
                    dc.w     1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1
                    dc.w     1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1
                    dc.w     0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0
                    dc.w     2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2
                    dc.w     0,0,$14,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,$578,0,0,1,1,0,2,0
                    dc.w     0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0
                    dc.w     1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1
                    dc.w     1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1
                    dc.w     0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0
                    dc.w     2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2
                    dc.w     0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0
                    dc.w     0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0
                    dc.w     1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1
                    dc.w     1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1,0,2,0,0,1,1
                    dc.w     0,2,0,0,1,1,0,2,0,0,$14,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1
                    dc.w     $FFFF,0,2,0,0,1,$FFFF,0,2,0,0,1,$FFFF,0,2,0,0
                    dc.w     $FFFF
E_RUMBLE:           dc.w     1,$40,0,$32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0
                    dc.w     0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1
                    dc.w     $32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1
                    dc.w     $FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0
                    dc.w     0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1
                    dc.w     $32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1
                    dc.w     $FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0
                    dc.w     0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1
                    dc.w     $32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1
                    dc.w     $FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0
                    dc.w     0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1
                    dc.w     $32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1
                    dc.w     $FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0
                    dc.w     0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1
                    dc.w     $32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1
                    dc.w     $FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0
                    dc.w     0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1
                    dc.w     $32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1
                    dc.w     $FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0
                    dc.w     0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1
                    dc.w     $32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1
                    dc.w     $FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0
                    dc.w     0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1
                    dc.w     $32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1
                    dc.w     $FFFF,1,$FFFF

TEST:               move.l   4,a6
                    jsr      -132(a6)
                    bsr      START_SOUNDS
                    move.w   #$10,d5
lbC001F9C:          move.w   #$13,d7
                    bsr      NEW_SOUND
                    move.w   #1,MUSIC_SWITCH
lbC001FAC:          btst     #6,$BFE001
                    bne.s    lbC001FAC
lbC001FB6:          btst     #6,$BFE001
                    beq.s    lbC001FB6
                    dbra     d5,lbC001F9C
                    move.l   SAVEINT6,$78
                    move.w   #15,$DFF096
                    move.l   4,a6
                    jsr      -138(a6)
                    rts

START_SOUNDS:       move.w   #1,MASTER_ENABLE
                    clr.w    MUSIC_POINTER
                    clr.w    MUSIC_DELAY
                    lea      RANDOM_AREA,a0
                    move.w   #$1FFF,d0
lbC002000:          move.l   RND1,d1
                    add.l    RND2,d1
                    add.w    $DFF006,d1
                    ror.l    #1,d1
                    move.l   d1,RND1
                    sub.l    RND3,d1
                    rol.l    #1,d1
                    add.w    d1,RND2
                    and.w    #$FF,d1
                    move.b   d1,(a0)+
                    dbra     d0,lbC002000
                    clr.w    MUSIC_SWITCH
                    move.w   #15,$DFF096
                    move.w   #$FF,$DFF09E
                    lea      $DFF0A0,a5
                    lea      MUSINF_0,a6
                    clr.w    0(a6)
                    clr.w    6(a6)
                    move.w   #$20,4(a5)
                    move.w   #$8001,8(a6)
                    move.l   #ENVELOPE1,10(a6)
                    move.l   10(a6),$10(a6)
                    move.l   #DUMMY,10(a6)
                    clr.w    4(a6)
                    move.w   #$FFFF,14(a6)
                    lea      $DFF0B0,a5
                    lea      MUSINF_1,a6
                    clr.w    0(a6)
                    clr.w    6(a6)
                    move.w   #$20,4(a5)
                    move.w   #$8002,8(a6)
                    move.l   #ENVELOPE2,10(a6)
                    move.l   10(a6),$10(a6)
                    move.l   #DUMMY,10(a6)
                    clr.w    4(a6)
                    move.w   #$FFFF,14(a6)
                    lea      $DFF0C0,a5
                    lea      MUSINF_2,a6
                    clr.w    0(a6)
                    clr.w    6(a6)
                    move.w   #$20,4(a5)
                    move.w   #$8004,8(a6)
                    move.l   #ENVELOPE3,10(a6)
                    move.l   10(a6),$10(a6)
                    move.l   #DUMMY,10(a6)
                    clr.w    4(a6)
                    move.w   #$FFFF,14(a6)
                    lea      $DFF0D0,a5
                    lea      MUSINF_3,a6
                    clr.w    0(a6)
                    move.w   #$20,4(a5)
                    clr.w    6(a6)
                    move.w   #$8008,8(a6)
                    move.l   #ENVELOPE1,10(a6)
                    move.l   10(a6),$10(a6)
                    move.l   #DUMMY,10(a6)
                    clr.w    4(a6)
                    move.w   #0,14(a6)
                    bsr      INITMUSC
                    move.l   $78,SAVEINT6
                    move.l   #MYINT6,$78
                    lea      $BFD000,a0
                    move.b   #0,$600(a0)
                    move.b   #14,$700(a0)
                    move.b   #$11,$F00(a0)
                    move.b   #$82,$D00(a0)
                    move.w   #$A000,$DFF09A
                    clr.b    lbB002BAD
                    rts

GO_SOUND:           move.l   a1,(a5)
                    move.w   #$FF,$DFF09E
                    move.w   d0,4(a5)
                    move.w   d1,$14(a6)
                    move.w   d1,6(a5)
                    clr.w    6(a6)
                    clr.w    0(a6)
                    clr.w    4(a6)
                    move.w   #1,14(a6)
                    move.l   a0,10(a6)
                    rts

HIGH_PING:          move.w   #8,d0
                    move.w   #$DC,d1
                    lea      E_HIGH_PING,a0
                    lea      WAVEFORM16,a1
                    bsr.s    GO_SOUND
                    rts

MEDIUM_PING:        move.w   #8,d0
                    move.w   #$190,d1
                    lea      E_MEDIUM_PING,a0
                    lea      WAVEFORM16,a1
                    bsr.s    GO_SOUND
                    rts

LOW_PING:           move.w   #$40,d0
                    move.w   #$C8,d1
                    lea      E_LOW_PING,a0
                    lea      WAVEFORM128,a1
                    bsr.s    GO_SOUND
                    rts

DROP_PING:          move.w   #4,d0
                    move.w   #$898,d1
                    lea      E_LOW_PING,a0
                    lea      WAVEFORM8,a1
                    bsr      GO_SOUND
                    rts

TAKE_PING:          move.w   #4,d0
                    move.w   #$9C4,d1
                    lea      E_LOW_PING,a0
                    lea      WAVEFORM8,a1
                    bsr      GO_SOUND
                    rts

MEDIUM_BEEP:        move.w   #4,d0
                    move.w   #$258,d1
                    lea      E_MEDIUM_BEEP,a0
                    lea      WAVEFORM8,a1
                    bsr      GO_SOUND
                    rts

BOP:                move.w   #$1000,d0
                    move.w   #$FA0,d1
                    lea      E_BANG,a0
                    lea      RANDOM_AREA,a1
                    bsr      GO_SOUND
                    rts

BANG:               move.w   #$1000,d0
                    move.w   #$2BC,d1
                    lea      E_BANG,a0
                    lea      RANDOM_AREA,a1
                    bsr      GO_SOUND
                    rts

OTHER_SMACK:        move.w   #$1000,d0
                    move.w   #$1F4,d1
                    lea      E_BANG,a0
                    lea      RANDOM_AREA,a1
                    bsr      GO_SOUND
                    rts

GRAVESOUND:         move.w   #$12B,d0
                    move.w   #$1644,d1
                    lea      E_GRAVE,a0
                    lea      RANDOM_AREA,a1
                    bsr      GO_SOUND
                    rts

BOOM:               move.w   #$1000,d0
                    move.w   #$1194,d1
                    lea      E_BOOM,a0
                    lea      RANDOM_AREA,a1
                    bsr      GO_SOUND
                    rts

BURY:               move.w   #$1000,d0
                    move.w   #$5DC,d1
                    lea      E_BURY,a0
                    lea      RANDOM_AREA,a1
                    bsr      GO_SOUND
                    rts

SPLASH:             move.w   #$1000,d0
                    move.w   #$3E8,d1
                    lea      E_SPLASH,a0
                    lea      RANDOM_AREA,a1
                    bsr      GO_SOUND
                    rts

RUMBLE:             move.w   #$1000,d0
                    move.w   #$11F8,d1
                    lea      E_RUMBLE,a0
                    lea      RANDOM_AREA,a1
                    bsr      GO_SOUND
                    rts

FEET:               move.w   #$1000,d0
                    move.w   #$5DC,d1
                    lea      E_FEET,a0
                    lea      RANDOM_AREA,a1
                    bsr      GO_SOUND
                    rts

PICKY:              move.w   #$1000,d0
                    move.w   #$5DC,d1
                    lea      E_PICK,a0
                    lea      RANDOM_AREA,a1
                    bsr      GO_SOUND
                    rts

SAWY:               move.w   #$1000,d0
                    move.w   #$BB8,d1
                    lea      E_SAW,a0
                    lea      RANDOM_AREA,a1
                    bsr      GO_SOUND
                    rts

KISS:               move.w   #$1000,d0
                    move.w   #$84,d1
                    lea      E_KISS,a0
                    lea      RANDOM_AREA,a1
                    bsr      GO_SOUND
                    rts

SUB:                move.w   #4,d0
                    move.w   #$1388,d1
                    lea      E_SUB,a0
                    lea      WAVEFORM16,a1
                    bsr      GO_SOUND
                    rts

HANDLE_SOUNDS:      lea      $DFF0A0,a5
                    lea      MUSINF_0,a6
                    bsr      HANDLE_REQUEST
                    bne      lbC0023F0
                    lea      $DFF0B0,a5
                    lea      MUSINF_1,a6
                    bsr      HANDLE_REQUEST
                    bne      lbC0023F0
                    lea      $DFF0C0,a5
                    lea      MUSINF_2,a6
                    bsr      HANDLE_REQUEST
                    bne      lbC0023F0
                    lea      $DFF0D0,a5
                    lea      MUSINF_3,a6
                    bsr      HANDLE_REQUEST
lbC0023F0:          rts

HANDLE_REQUEST:     lea      REQUESTS,a0
                    tst.w    14(a6)
                    bne      lbC002448
                    tst.w    (a0)+
                    bmi      lbC00240A
                    bne      lbC002428
lbC00240A:          tst.w    (a0)+
                    bmi      lbC002414
                    bne      lbC002428
lbC002414:          tst.w    (a0)+
                    bmi      lbC00241E
                    bne      lbC002428
lbC00241E:          tst.w    (a0)+
                    bmi      lbC00244C
                    beq      lbC00244C
lbC002428:          move.w   -(a0),d0
                    and.w    #$FF,d0
                    or.w     #$8000,(a0)
                    and.w    #$9FFF,(a0)
                    move.l   a0,$16(a6)
                    lsl.w    #2,d0
                    lea      SOUND_ROUTINES,a0
                    move.l   0(a0,d0.w),a0
                    jsr      (a0)
lbC002448:          moveq    #0,d0
                    rts

lbC00244C:          moveq    #-1,d0
                    rts

NEW_SOUND:          movem.l  d0-d2/a0,-(sp)
                    move.w   d7,d0
                    move.w   d0,d2
                    and.w    #$3FF,d0
                    btst     #13,d2
                    beq      lbC00247A
                    tst.w    BSND_FLAG
                    bne      NEW_RET
                    move.w   #1,BSND_FLAG
                    bra      NO_MATCH

lbC00247A:          lea      REQUESTS,a0
                    move.w   (a0)+,d1
                    and.w    #$3FF,d1
                    cmp.w    d1,d0
                    beq      MATCH_SOUND
                    move.w   (a0)+,d1
                    and.w    #$3FF,d1
                    cmp.w    d1,d0
                    beq      MATCH_SOUND
                    move.w   (a0)+,d1
                    and.w    #$3FF,d1
                    cmp.w    d1,d0
                    beq      MATCH_SOUND
                    move.w   (a0)+,d1
                    and.w    #$3FF,d1
                    cmp.w    d1,d0
                    bne      NO_MATCH
MATCH_SOUND:        tst.w    d2
                    bmi      NEW_RET
                    or.w     #$4000,-(a0)
                    bra      NEW_RET

NO_MATCH:           lea      REQUESTS,a0
                    tst.w    (a0)+
                    beq      lbC0024E8
                    tst.w    (a0)+
                    beq      lbC0024E8
                    tst.w    (a0)+
                    beq      lbC0024E8
                    tst.w    (a0)+
                    beq      lbC0024E8
                    btst     #14,d2
                    bne      lbC0024E8
                    bra      NEW_RET

lbC0024E8:          move.w   d0,-(a0)
NEW_RET:            movem.l  (sp)+,d0-d2/a0
                    rts

REQUESTS:           dcb.l    2,0
SOUND_ROUTINES:     dc.l     HIGH_PING
                    dc.l     HIGH_PING
                    dc.l     BANG
                    dc.l     BURY
                    dc.l     BOOM
                    dc.l     SPLASH
                    dc.l     MEDIUM_PING
                    dc.l     LOW_PING
                    dc.l     MEDIUM_BEEP
                    dc.l     RUMBLE
                    dc.l     SUB
                    dc.l     FEET
                    dc.l     GRAVESOUND
                    dc.l     KISS
                    dc.l     TAKE_PING
                    dc.l     DROP_PING
                    dc.l     OTHER_SMACK
                    dc.l     PICKY
                    dc.l     SAWY
                    dc.l     BOP

MYINT6:             tst.b    $BFDD00
                    tst.w    MASTER_ENABLE
                    beq      MYINT6_RET
                    movem.l  d0-d7/a0-a6,-(sp)
                    tst.w    MUSIC_DELAY
                    beq      lbC002570
                    subq.w   #1,MUSIC_DELAY
                    bra      ENVS

lbC002570:          move.w   #3,MUSIC_DELAY
                    tst.w    MUSIC_SWITCH
                    beq      ENVS
                    bsr      PLAYMUS
                    bsr      X8912
ENVS:               lea      $DFF0A0,a5
                    lea      MUSINF_0,a6
                    tst.w    14(a6)
                    beq      lbC0025A2
                    bsr      CHANGE_ENVELOPE
lbC0025A2:          lea      $DFF0B0,a5
                    lea      MUSINF_1,a6
                    tst.w    14(a6)
                    beq      lbC0025BA
                    bsr      CHANGE_ENVELOPE
lbC0025BA:          lea      $DFF0C0,a5
                    lea      MUSINF_2,a6
                    tst.w    14(a6)
                    beq      lbC0025D2
                    bsr      CHANGE_ENVELOPE
lbC0025D2:          lea      $DFF0D0,a5
                    lea      MUSINF_3,a6
                    tst.w    14(a6)
                    beq      _HANDLE_SOUNDS
                    bsr      CHANGE_ENVELOPE
_HANDLE_SOUNDS:     bsr      HANDLE_SOUNDS
                    movem.l  (sp)+,d0-d7/a0-a6
MYINT6_RET:         move.w   #$2000,$DFF09C
                    rte

CHANGE_ENVELOPE:    tst.w    0(a6)
                    bne      SAME_STEP
                    or.w     #$8000,8(a6)
                    tst.w    14(a6)
                    bmi      lbC002616
                    clr.w    14(a6)
lbC002616:          move.l   10(a6),a0
CE_1:               move.w   (a0),d0
                    bmi      END_OF_ENVY
                    or.w     #1,14(a6)
                    addq.w   #2,a0
                    move.w   d0,0(a6)
                    move.w   (a0)+,2(a6)
                    move.w   (a0)+,4(a6)
                    move.l   a0,10(a6)
SAME_STEP:          tst.w    0(a6)
                    beq      DO_VOLUME
                    subq.w   #1,0(a6)
                    move.w   6(a6),d0
                    add.w    2(a6),d0
                    move.w   d0,6(a6)
                    move.w   $14(a6),d0
                    add.w    4(a6),d0
                    move.w   d0,$14(a6)
                    bra      DO_VOLUME

END_OF_ENVY:        cmp.w    #$FFFE,d0
                    bne      lbC002688
                    move.l   $16(a6),a1
                    move.w   (a1),d0
                    btst     #14,d0
                    beq      lbC002684
                    bclr     #14,d0
                    move.w   d0,(a1)
                    move.w   2(a0),d0
                    sub.w    d0,a0
                    bra.s    CE_1

lbC002684:          addq.w   #4,a0
                    bra.s    CE_1

lbC002688:          tst.w    6(a6)
                    bne      lbC002696
                    and.w    #$7FFF,8(a6)
lbC002696:          move.l   $16(a6),a0
                    move.w   (a0),d0
                    and.w    #$4000,d0
                    bne      ALLOW_AGAIN
                    clr.w    (a0)
                    bra      DO_VOLUME

ALLOW_AGAIN:        and.w    #$3FF,(a0)
DO_VOLUME:          move.w   $14(a6),6(a5)
                    move.w   6(a6),8(a5)
                    move.w   8(a6),$DFF096
                    rts

X8912:              lea      REGS,a4
                    lea      $DFF0A0,a5
                    lea      MUSINF_0,a6
                    move.b   7(a4),d0
                    move.w   d0,d1
                    and.w    #9,d1
                    cmp.w    #9,d1
                    beq      XDONE1
                    and.w    #8,d1
                    beq      XNOISE1
                    moveq    #0,d2
                    moveq    #0,d3
                    move.b   0(a4),d2
                    move.b   1(a4),d3
                    lsl.w    #1,d2
                    lea      WAVE_LENGTH,a0
                    move.w   0(a0,d2.w),4(a5)
                    lsl.w    #1,d2
                    lea      WHICH_WAVE,a0
                    move.l   0(a0,d2.w),(a5)
                    lsl.w    #1,d3
                    lea      FREQS,a0
ISITENV1:           cmp.b    #$10,8(a4)
                    beq      DO_ENVEL1
                    move.w   0(a0,d3.w),6(a5)
                    move.w   0(a0,d3.w),$14(a6)
                    clr.w    4(a6)
                    move.b   8(a4),d0
                    and.w    #15,d0
                    lsl.w    #1,d0
                    move.w   d0,6(a6)
                    bra      XDONE1

DO_ENVEL1:          tst.b    13(a4)
                    beq      XDONE1
                    move.w   0(a0,d3.w),6(a5)
                    move.w   0(a0,d3.w),$14(a6)
                    clr.w    4(a6)
                    move.l   $10(a6),10(a6)
                    clr.w    6(a6)
                    clr.w    0(a6)
                    bra      XDONE1

XNOISE1:            lea      $DFF0C0,a5
                    lea      MUSINF_2,a6
                    move.l   #RANDOM_AREA,(a5)
                    move.w   #$1000,4(a5)
                    moveq    #0,d0
                    move.b   6(a4),d0
                    and.w    #$1F,d0
                    lsl.w    #7,d0
                    move.w   d0,TEMP_MUSIC
                    lea      TEMP_MUSIC,a0
                    clr.w    d3
                    bra      ISITENV1

XDONE1:             lea      $DFF0B0,a5
                    lea      MUSINF_1,a6
                    move.b   7(a4),d0
                    move.w   d0,d1
                    and.w    #$12,d1
                    cmp.w    #$12,d1
                    beq      XDONE2
                    and.w    #10,d1
                    bne      XNOISE2
                    moveq    #0,d2
                    moveq    #0,d3
                    move.b   2(a4),d2
                    move.b   3(a4),d3
                    lsl.w    #1,d2
                    lea      WAVE_LENGTH,a0
                    move.w   0(a0,d2.w),4(a5)
                    lsl.w    #1,d2
                    lea      WHICH_WAVE,a0
                    move.l   0(a0,d2.w),(a5)
                    lsl.w    #1,d3
                    lea      FREQS,a0
ISITENV2:           cmp.b    #$10,9(a4)
                    beq      DO_ENVEL2
                    move.w   0(a0,d3.w),6(a5)
                    move.w   0(a0,d3.w),$14(a6)
                    clr.w    4(a6)
                    move.b   9(a4),d0
                    and.w    #15,d0
                    lsl.w    #1,d0
                    move.w   d0,6(a6)
                    bra      XDONE2

DO_ENVEL2:          tst.b    13(a4)
                    beq      XDONE2
                    move.w   0(a0,d3.w),6(a5)
                    move.w   0(a0,d3.w),$14(a6)
                    clr.w    4(a6)
                    move.l   $10(a6),10(a6)
                    clr.w    6(a6)
                    clr.w    0(a6)
                    bra      XDONE2

XNOISE2:            lea      $DFF0C0,a5
                    lea      MUSINF_2,a6
                    move.l   #RANDOM_AREA,(a5)
                    move.w   #$1000,4(a5)
                    moveq    #0,d0
                    move.b   6(a4),d0
                    and.w    #$1F,d0
                    lsl.w    #7,d0
                    move.w   d0,TEMP_MUSIC
                    lea      TEMP_MUSIC,a0
                    clr.w    d3
                    bra      ISITENV2

XDONE2:             clr.b    13(a4)
                    rts

PSGW:               move.l   a0,-(sp)
                    lea      REGS,a0
                    move.b   d1,0(a0,d0.w)
                    move.l   (sp)+,a0
                    rts

INITMUSC:           move.w   #1,MUSICON
                    move.w   #7,d0
                    move.w   #$FC,d1
                    bsr.s    PSGW
                    move.w   #8,d0
                    move.w   #0,d1
                    bsr.s    PSGW
                    move.w   #9,d0
                    move.w   #0,d1
                    bsr.s    PSGW
                    move.l   #NOTES1,POINTERA
                    move.l   #NOTES2,POINTERB
                    clr.w    DELAYA
                    clr.w    DELAYB
                    clr.w    FLAGA
                    clr.w    FLAGB
                    rts

PLAYMUS:            tst.w    MUSICON
                    beq      lbC002940
                    move.l   POINTERA,a4
                    move.w   DELAYA,d7
                    move.w   #0,d5
                    bsr      PLAY_CHANNEL
                    move.w   d7,DELAYA
                    move.l   a4,POINTERA
                    move.l   POINTERB,a4
                    move.w   DELAYB,d7
                    move.w   #$FFFF,d5
                    bsr      PLAY_CHANNEL
                    move.w   d7,DELAYB
                    move.l   a4,POINTERB
                    rts

lbC002940:          move.w   #8,d0
                    move.w   #0,d1
                    bsr      PSGW
                    move.w   #9,d0
                    move.w   #0,d1
                    bsr      PSGW
                    rts

PLAY_CHANNEL:       tst.w    d7
                    beq      PLAY2
                    subq.w   #1,d7
                    tst.w    d5
                    bne      lbC002984
                    tst.w    FLAGA
                    beq      lbC0029A0
                    cmp.w    #9,d7
                    bgt      lbC0029A0
                    move.w   d7,d1
                    move.w   #8,d0
                    bsr      PSGW
lbC002984:          tst.w    FLAGB
                    beq      lbC0029A0
                    cmp.w    #10,d7
                    bgt      lbC0029A0
                    move.w   d7,d1
                    move.w   #9,d0
                    bsr      PSGW
lbC0029A0:          rts

PLAY2:              tst.w    (a4)
                    bpl      lbC0029AC
                    move.l   2(a4),a4
lbC0029AC:          move.w   (a4)+,d2
                    move.w   (a4)+,d3
                    move.w   (a4)+,d7
                    tst.w    d3
                    bmi      lbC002A18
                    tst.w    d5
                    bne      lbC0029EC
                    subq.w   #1,d2
                    move.w   #$FFFF,FLAGA
                    move.w   #8,d0
                    move.w   #9,d1
                    bsr      PSGW
                    move.w   #0,d0
                    move.w   d2,d1
                    bsr      PSGW
                    move.w   #1,d0
                    move.w   d3,d1
                    bsr      PSGW
                    bra      lbC002A42

lbC0029EC:          move.w   #$FFFF,FLAGB
                    move.w   #9,d0
                    move.w   #10,d1
                    bsr      PSGW
                    move.w   #2,d0
                    move.w   d2,d1
                    bsr      PSGW
                    move.w   #3,d0
                    move.w   d3,d1
                    bsr      PSGW
                    bra      lbC002A42

lbC002A18:          tst.w    d5
                    bne      lbC002A30
                    move.w   #8,d0
                    move.w   #0,d1
                    bsr      PSGW
                    clr.w    FLAGA
lbC002A30:          move.w   #9,d0
                    move.w   #0,d1
                    bsr      PSGW
                    clr.w    FLAGB
lbC002A42:          rts

NOTES1:             dc.w     0,$FFFF,$160
NOTES1A:            dc.w     0,$FFFF,$2C0,5,7,$58,0,$FFFF,$58,5,6,$58,0,$FFFF
                    dc.w     $58,5,2,$58,0,$FFFF,$58,5,0,$58,0,$FFFF,$58,4,7
                    dc.w     $58,0,$FFFF,$58,4,6,$58,0,$FFFF,$58,4,2,$58,0
                    dc.w     $FFFF,$58,4,0,$58,0,$FFFF,$58,3,7,$58,0,$FFFF,$58
                    dc.w     3,6,$58,0,$FFFF,$58,3,2,$58,0,$FFFF,$58,3,0,$58,0
                    dc.w     $FFFF,$58,$FFFF
                    dc.l     NOTES1A
NOTES2:             dc.w     1,2,11,1,2,11,1,2,$16,0,$FFFF,$2C,0,$FFFF,$58,1,2
                    dc.w     11,1,2,11,1,2,$16,0,$FFFF,$2C,0,$FFFF,$58
NOTES2A:            dc.w     1,2,11,1,2,11,2,9,11,1,2,11,2,0,11,1,2,11,2,9,11
                    dc.w     2,7,$16,1,2,11,2,6,11,1,2,11,2,4,11,1,2,11,2,0,11
                    dc.w     1,9,11,$FFFF
                    dc.l     NOTES2A
MUSICON:            dc.w     0
DELAYA:             dc.w     0
DELAYB:             dc.w     0
POINTERA:           dc.l     0
POINTERB:           dc.l     0
FLAGA:              dc.w     0
FLAGB:              dc.w     0
WARBLEA:            dc.w     0
FREQA:              dc.w     0
MUSIC_SWITCH:       dc.w     0
SAVE_INT2:          dcb.w    2,0
TEMP_MUSIC:         dc.w     0
REGS:               dcb.b    13,0
lbB002BAD:          dc.b     0
REG7:               dcb.b    2,0
SAVEINT6:           dc.l     0
MUSINF_0:           dcb.w    13,0
MUSINF_1:           dcb.w    13,0
MUSINF_2:           dcb.w    13,0
MUSINF_3:           dcb.w    13,0
MUSIC_POINTER:      dc.w     0
MUSIC_DELAY:        dc.w     0
FRED:               dc.w     0
MASTER_ENABLE:      dc.w     0
RANDOM_AREA:        dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $20,0
BSND_FLAG:          dc.w     0
MUSIC_DELAY2:       dc.w     0
RND1:               dc.l     $81281233
RND2:               dc.l     $12871332
RND3:               dc.l     $1111FFED

START:              jsr      ALLOCATE_MEMORY
                    jsr      START_SOUNDS
                    jsr      SETUP_SCREEN
                    move.l   TWOSCREEN,SCREEN2
                    move.l   ONESCREEN,SCREEN1
                    bsr      READALL
                    move.l   #$130000,d0
lbC004C64:          subq.l   #1,d0
                    bne.s    lbC004C64
                    move.l   $6C,-(sp)
                    move.l   #MYINT3,$6C
                    jsr      SETUPZ
                    jsr      TITLES
                    move.w   #$A0,$DFF096
                    move.l   (sp)+,$6C
                    move.l   SAVEINT6,$78
                    moveq    #0,d1
                    move.l   DOS_BASE,a6
                    moveq    #0,d0
                    jsr      -144(a6)
                    moveq    #0,d0
                    rts

READALL:            move.l   #OBJNAME1,a0
                    bsr      READPIX
                    move.l   BACK,a0
                    move.l   #0,d1
                    move.l   #0,d2
                    move.w   #4,d6
                    move.w   #$1B,d7
                    lea      BLACK_SUB,a1
                    bsr      RSTUFF_BUFF
                    move.l   #4,d1
                    lea      WHITE_SUB,a1
                    bsr      RSTUFF_BUFF
                    move.l   #8,d1
                    move.l   #4,d2
                    move.l   #3,d6
                    move.l   #$1D,d7
                    lea      ROCK1,a1
                    bsr      RSTUFF_BUFF
                    move.l   #12,d1
                    move.l   #0,d2
                    move.l   #4,d6
                    move.l   #$16,d7
                    lea      ROCK2,a1
                    bsr      RSTUFF_BUFF
                    move.l   #$10,d1
                    move.l   #1,d2
                    move.l   #4,d6
                    move.l   #$1C,d7
                    lea      ROCK3,a1
                    bsr      RSTUFF_BUFF
                    move.l   #12,d1
                    move.l   #$20,d2
                    move.l   #2,d6
                    move.l   #$11,d7
                    lea      ROCK4,a1
                    bsr      RSTUFF_BUFF
                    move.l   #14,d1
                    move.l   #$34,d2
                    move.l   #2,d6
                    move.l   #$1E,d7
                    lea      PENGY_L,a1
                    bsr      RSTUFF_BUFF
                    move.l   #$10,d1
                    lea      PENGY_F,a1
                    bsr      RSTUFF_BUFF
                    move.l   #$12,d1
                    lea      PENGY_R,a1
                    bsr      RSTUFF_BUFF
                    move.l   #0,d1
                    move.l   #$22,d2
                    move.l   #12,d6
                    move.l   #$30,d7
                    lea      SPYVSPY,a1
                    bsr      RSTUFF_BUFF
                    move.l   #$5D,d2
                    move.l   #$10,d6
                    move.l   #$1C,d7
                    lea      MAD_MAG,a1
                    bsr      RSTUFF_BUFF
                    move.w   #$86,d2
                    move.w   #$12,d6
                    move.w   #$42,d7
                    lea      ARCTIC_ANTS,a1
                    bsr      RSTUFF_BUFF
                    move.l   #OBJNAME2,a0
                    bsr      READPIX
                    move.l   BACK,a0
                    move.l   #0,d1
                    move.l   #0,d2
                    move.w   #5,d6
                    move.l   #14,d7
                    lea      SPLASH1,a1
                    bsr      RSTUFF_BUFF
                    move.l   #14,d2
                    lea      SPLASH2,a1
                    bsr      RSTUFF_BUFF
                    move.l   #$21,d2
                    lea      SPLASH3,a1
                    bsr      RSTUFF_BUFF
                    move.l   #$36,d2
                    lea      SPLASH4,a1
                    bsr      RSTUFF_BUFF
                    move.l   #12,d1
                    move.l   #0,d2
                    move.l   #4,d6
                    move.l   #$38,d7
                    lea      OVERLAY,a1
                    bsr      RSTUFF_BUFF
                    move.l   #5,d1
                    move.l   #0,d2
                    move.l   #7,d6
                    move.l   #$22,d7
                    lea      BLACK_CAVE,a1
                    bsr      RSTUFF_BUFF
                    move.w   #$27,d2
                    lea      RED_CAVE,a1
                    bsr      RSTUFF_BUFF
                    move.l   #0,d1
                    move.l   #$65,d2
                    move.l   #$14,d6
                    move.l   #$17,d7
                    lea      FIRSTSTAR,a1
                    bsr      RSTUFF_BUFF
                    lea      BGNAME,a0
                    bsr      READPIX
                    rts

MYINT3:             jsr      TINTERRUPT
                    clr.w    INT_REQ
                    move.w   #$70,$DFF09C
                    rte

INT_REQ:            dc.w     0
INT_FLAG:           dc.w     0
MY_VERY_OWN_STACK:  dc.l     0

READPIX:            move.l   a0,d1
                    move.l   SCREEN2,d2
                    jsr      DECO_PIC
                    move.l   SCREEN2,a0
                    add.w    #$80,a0
                    move.l   BACK,a1
                    jsr      ATARI_COPY
                    move.l   SCREEN2,a0
                    addq.w   #4,a0
                    move.w   (a0),d7
                    jsr      COLOUR_COPY
                    rts

RNDER:              move.l   RND1,d0
                    add.l    RND2,d0
                    move.l   d0,RND1
                    add.l    RND3,d0
                    move.l   d0,RND2
                    rol.l    #3,d0
                    sub.l    RND1,d0
                    add.l    #$FED11137,d0
                    eor.l    d0,RND3
                    rts

RNDER2:             move.l   d0,-(sp)
                    bsr.s    RNDER
                    move.l   d0,d1
                    move.l   (sp)+,d0
                    rts

SWAPSCREEN:         jmp      SWAP_SCREEN

BGNAME:             dc.b     'backgnd.pi1',0
OBJNAME1:           dc.b     'objects.pi1',0
OBJNAME2:           dc.b     'object2.pi1',0

                    jmp      GOERROR

JUNK:               dc.b     $78,$30,$78,$FC,$18,$FC,$78,$FC,$78,$78,0,$C0,0
                    dc.b     12,0,$38,0,$C0,$30,$30,$C0,$70,0,0,0,0,0,0,0,0,0
                    dc.b     0,0,0,0,0,$CC,$70,$CC,$18,$38,$C0,$C0,12,$CC,$CC
                    dc.b     0,$C0,0,12,0,$60,0,$C0,0,0,$C0,$30,0,0,0,0,0,0,0
                    dc.b     $30,0,0,0,0,0,0,$DC,$30,12,$30,$78,$F8,$C0,$18
                    dc.b     $CC,$CC,$78,$F8,$78,$7C,$78,$F8,$7C,$F8,$70,$30
                    dc.b     $CC,$31,$D8,$F8,$78,$F8,$7C,$F8,$7C,$FC,$CC,$CD
                    dc.b     $8C,$CC,$CC,$FC,$EC,$30,$18,$18,$D8,12,$F8,$30
                    dc.b     $78,$7C,12,$CC,$C0,$CC,$CC,$60,$CC,$CC,$30,$30
                    dc.b     $D8,$31,$FC,$CC,$CC,$CC,$CC,$CC,$C0,$30,$CC,$CD
                    dc.b     $8C,$78,$CC,$18,$CC,$30,$30,12,$FC,12,$CC,$60,$CC
                    dc.b     12,$7C,$CC,$C0,$CC,$FC,$60,$CC,$CC,$30,$30,$F0
                    dc.b     $31,$AC,$CC,$CC,$CC,$CC,$C0,$78,$30,$CC,$CD,$AC
                    dc.b     $30,$CC,$30,$CC,$30,$60,$CC,$18,$CC,$CC,$60,$CC
                    dc.b     $18,$CC,$CC,$C0,$CC,$C0,$60,$7C,$CC,$30,$30,$D8
                    dc.b     $31,$8C,$CC,$CC,$CC,$CC,$C0,12,$30,$CC,$78,$F8
                    dc.b     $78,$7C,$60,$78,$FC,$FC,$78,$18,$78,$78,$60,$78
                    dc.b     $70,$7C,$F8,$78,$7C,$78,$60,12,$CC,$78,$30,$CC
                    dc.b     $79,$8C,$CC,$78,$F8,$7C,$C0,$F8,$1C,$7C,$30,$D8
                    dc.b     $CC,12,$FC,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,$F8,0
                    dc.b     0,$E0,0,0,0,0,0,$C0,12,0,0,0,0,0,0,0,$F8,0

GOERROR:            move.l   a0,AREGISTERS
                    lea      DREGISTERS,a0
                    movem.l  d0-d7,(a0)
                    lea      lbL005346,a0
                    movem.l  a1-a7,(a0)
                    move.l   #$12345678,-(sp)
                    move.l   #$98989898,-(sp)
                    move.l   sp,a1
                    lea      FROMSTACK,a0
                    move.w   #$3F,d0
lbC0050FE:          move.l   (a1)+,(a0)+
                    dbra     d0,lbC0050FE
                    lea      START,a0
                    move.l   a0,VUSP
                    lea      PALETTEZ,a0
                    lea      $DFF180,a1
                    move.w   #15,d0
lbC005120:          move.w   (a0)+,(a1)+
                    dbra     d0,lbC005120
CLEARS:             move.l   SCREEN1,a0
                    move.w   #$F9F,d0
lbC005130:          clr.l    (a0)+
                    clr.l    (a0)+
                    dbra     d0,lbC005130
                    move.l   SCREEN1,a0
                    lea      TEXT1,a1
                    move.l   a0,a2
                    bsr      OPTEXTZ
                    lea      $140(a2),a0
                    lea      AREGISTERS,a1
                    bsr      OPEIGHT
                    lea      TEXT2,a1
                    move.l   a0,a2
                    bsr      OPTEXTZ
                    lea      $140(a2),a0
                    lea      DREGISTERS,a1
                    bsr      OPEIGHT
                    lea      TEXT3,a1
                    move.l   a0,a2
                    bsr      OPTEXTZ
                    lea      $140(a2),a0
                    move.l   a0,a2
                    move.l   VUSP,d0
                    bsr      OPHEX
                    move.l   a2,a0
                    add.w    #$140,a0
                    lea      TEXT4,a1
                    move.l   a0,a2
                    bsr      OPTEXTZ
                    lea      $140(a2),a0
                    lea      FROMSTACK,a1
                    bsr      OPEIGHT
                    bsr      OPEIGHT
                    bsr      OPEIGHT
                    bsr      OPEIGHT
                    move.l   #$1A0000,d0
lbC0051C0:          subq.l   #1,d0
                    bne.s    lbC0051C0
BLOOP:              jmp      BLOOP

OPEIGHT:            bsr      OPFOUR
                    bsr      OPFOUR
                    rts

OPFOUR:             movem.l  d1/a0,-(sp)
                    move.w   #3,d1
lbC0051DC:          move.l   (a1)+,d0
                    bsr      OPHEX
                    move.l   a1,-(sp)
                    lea      SPACES,a1
                    bsr      OPTEXTZ
                    move.l   (sp)+,a1
                    dbra     d1,lbC0051DC
                    movem.l  (sp)+,d1/a0
                    add.w    #$140,a0
                    rts

OPHEX:              move.w   d1,-(sp)
                    move.w   #7,d1
lbC005204:          rol.l    #4,d0
                    move.w   d0,-(sp)
                    and.w    #15,d0
                    bsr      OPITEM
                    move.w   (sp)+,d0
                    dbra     d1,lbC005204
                    move.w   (sp)+,d1
                    rts

OPTEXTZ:            move.w   d0,-(sp)
lbC00521C:          move.l   #0,d0
                    move.b   (a1)+,d0
                    beq      lbC005256
                    cmp.b    #' ',d0
                    beq      lbC00524C
                    cmp.b    #'a',d0
                    blt      lbC005244
                    sub.b    #'a',d0
                    add.w    #10,d0
                    bra      _OPITEM

lbC005244:          sub.w    #$30,d0
                    bra      _OPITEM

lbC00524C:          move.w   #$24,d0
_OPITEM:            bsr      OPITEM
                    bra.s    lbC00521C

lbC005256:          move.w   (sp)+,d0
                    rts

OPITEM:             movem.l  d0/d1/a1,-(sp)
                    cmp.w    #$24,d0
                    beq      lbC005292
                    lea      JUNK,a1
                    add.w    d0,a1
                    move.l   a0,-(sp)
                    move.w   #7,d1
lbC005274:          move.b   (a1),d0
                    move.b   d0,(a0)
                    move.b   d0,8000(a0)
                    move.b   d0,16000(a0)
                    move.b   d0,24000(a0)
                    add.w    #$24,a1
                    add.w    #$28,a0
                    dbra     d1,lbC005274
                    move.l   (sp)+,a0
lbC005292:          move.l   a0,d0
                    and.w    #1,d0
                    bne      lbC0052A2
                    addq.w   #1,a0
                    bra      lbC0052A4

lbC0052A2:          addq.w   #1,a0
lbC0052A4:          movem.l  (sp)+,d0/d1/a1
                    rts

ILLEG:              bra      GOERROR

SETUPZ:             move.l   #ILLEG,8
                    move.l   #ILLEG,12
                    move.l   #ILLEG,$10
                    rts

TESTTEXT:           dc.b     $11,$11,$11,$11,$99,$99,$99,$99,$77,$77,$77,$77
                    dc.b     $12,$31,$23,$12,$31,$41,$59,$26,$10,0,0,1,$98,$79
                    dc.b     $87,$98,$12,$31,$23,$12
TEXT1:              dc.b     'a0 to a7',0
TEXT2:              dc.b     'd0 to d7',0
TEXT3:              dc.b     'start address',0
TEXT4:              dc.b     'top of the stack',0
SPACES:             dc.b     '  ',0
PALETTEZ:           dc.w     0,$777,$777,$777,$777,$777,$666,$777,$777,$777
                    dc.w     $777,$777,$777,$777,$777,$700
AREGISTERS:         dc.l     0
lbL005346:          dcb.l    7,0
DREGISTERS:         dcb.l    8,0
FROMSTACK:          dcb.l    $3F,0
                    dc.l     0
VUSP:               dc.l     0
VSTATUS:            dc.l     0

RSPRITER:           movem.l  d0-d7/a0-a2,-(sp)
                    subq.w   #1,d6
                    subq.w   #1,d7
                    move.w   d1,d4
                    and.w    #15,d4
                    lsl.w    #3,d2
                    add.w    d2,a0
                    lsl.w    #2,d2
                    add.w    d2,a0
                    and.w    #$FFF0,d1
                    asr.w    #3,d1
                    add.w    d1,a0
lbC0054A8:          move.l   a0,a2
                    move.w   d6,d0
lbC0054AC:          moveq    #0,d2
                    move.w   (a1),d2
                    or.w     2(a1),d2
                    or.w     4(a1),d2
                    or.w     6(a1),d2
                    ror.l    d4,d2
                    not.l    d2
                    and.w    d2,(a0)
                    and.w    d2,8000(a0)
                    and.w    d2,16000(a0)
                    and.w    d2,24000(a0)
                    swap     d2
                    and.w    d2,2(a0)
                    and.w    d2,8002(a0)
                    and.w    d2,16002(a0)
                    and.w    d2,24002(a0)
                    tst.w    d4
                    beq      lbC005526
                    moveq    #0,d2
                    move.w   (a1)+,d2
                    ror.l    d4,d2
                    or.w     d2,(a0)+
                    swap     d2
                    or.w     d2,(a0)
                    moveq    #0,d2
                    move.w   (a1)+,d2
                    ror.l    d4,d2
                    or.w     d2,7998(a0)
                    swap     d2
                    or.w     d2,8000(a0)
                    moveq    #0,d2
                    move.w   (a1)+,d2
                    ror.l    d4,d2
                    or.w     d2,15998(a0)
                    swap     d2
                    or.w     d2,16000(a0)
                    moveq    #0,d2
                    move.w   (a1)+,d2
                    ror.l    d4,d2
                    or.w     d2,23998(a0)
                    swap     d2
                    or.w     d2,24000(a0)
                    bra      lbC00553C

lbC005526:          move.w   (a1)+,d2
                    or.w     d2,(a0)+
                    move.w   (a1)+,d2
                    or.w     d2,7998(a0)
                    move.w   (a1)+,d2
                    or.w     d2,15998(a0)
                    move.w   (a1)+,d2
                    or.w     d2,23998(a0)
lbC00553C:          dbra     d0,lbC0054AC
                    lea      40(a2),a0
                    dbra     d7,lbC0054A8
                    movem.l  (sp)+,d0-d7/a0-a2
                    rts

RSAVE_BUFF:         movem.l  d0-d7/a0-a2,-(sp)
                    subq.w   #1,d6
                    subq.w   #1,d7
                    lsl.w    #3,d2
                    add.w    d2,a0
                    lsl.w    #2,d2
                    add.w    d2,a0
                    asl.w    #1,d1
                    add.w    d1,a0
lbC005562:          move.l   a0,a2
                    move.w   d6,d0
lbC005566:          move.w   (a0)+,(a1)+
                    move.w   7998(a0),(a1)+
                    move.w   15998(a0),(a1)+
                    move.w   23998(a0),(a1)+
                    dbra     d0,lbC005566
                    lea      $28(a2),a0
                    dbra     d7,lbC005562
                    movem.l  (sp)+,d0-d7/a0-a2
                    rts

RDRAW_BUFF:         movem.l  d0-d7/a0-a2,-(sp)
                    subq.w   #1,d6
                    subq.w   #1,d7
                    lsl.w    #3,d2
                    add.w    d2,a0
                    lsl.w    #2,d2
                    add.w    d2,a0
                    asl.w    #1,d1
                    add.w    d1,a0
lbC00559A:          move.l   a0,a2
                    move.w   d6,d0
lbC00559E:          move.w   (a1)+,(a0)+
                    move.w   (a1)+,7998(a0)
                    move.w   (a1)+,15998(a0)
                    move.w   (a1)+,23998(a0)
                    dbra     d0,lbC00559E
                    lea      $28(a2),a0
                    dbra     d7,lbC00559A
                    movem.l  (sp)+,d0-d7/a0-a2
                    rts

RSTUFF_BUFF:        movem.l  d0-d7/a0-a2,-(sp)
                    addq.w   #1,d6
                    move.w   d6,d3
                    mulu     d7,d3
                    lsl.w    #1,d3
                    move.l   d3,(a1)+
                    move.w   d6,(a1)+
                    move.w   d7,(a1)+
                    subq.w   #2,d6
                    subq.w   #1,d7
                    lsl.w    #3,d2
                    add.w    d2,a0
                    lsl.w    #2,d2
                    add.w    d2,a0
                    asl.w    #1,d1
                    add.w    d1,a0
lbC0055E0:          move.l   a0,a2
                    move.w   d6,d0
lbC0055E4:          move.w   (a0),d1
                    or.w     8000(a0),d1
                    or.w     16000(a0),d1
                    or.w     24000(a0),d1
                    move.w   d1,(a1)
                    move.w   d3,d2
                    move.w   (a0),0(a1,d2.w)
                    add.w    d3,d2
                    move.w   $1F40(a0),0(a1,d2.w)
                    add.w    d3,d2
                    move.w   $3E80(a0),0(a1,d2.w)
                    add.w    d3,d2
                    move.w   $5DC0(a0),0(a1,d2.w)
                    addq.w   #2,a1
                    addq.w   #2,a0
                    dbra     d0,lbC0055E4
                    clr.w    (a1)+
                    lea      $28(a2),a0
                    dbra     d7,lbC0055E0
                    movem.l  (sp)+,d0-d7/a0-a2
                    rts

RSTUFF_BUFF_ST:     movem.l  d0-d7/a0-a2,-(sp)
                    addq.w   #1,d6
                    move.w   d6,d3
                    mulu     d7,d3
                    lsl.w    #1,d3
                    move.l   d3,(a1)+
                    move.w   d6,(a1)+
                    move.w   d7,(a1)+
                    subq.w   #2,d6
                    subq.w   #1,d7
                    lsl.w    #5,d2
                    add.w    d2,a0
                    lsl.w    #2,d2
                    add.w    d2,a0
                    asl.w    #3,d1
                    add.w    d1,a0
lbC00564C:          move.l   a0,a2
                    move.w   d6,d0
lbC005650:          move.w   (a0),d1
                    or.w     2(a0),d1
                    or.w     4(a0),d1
                    or.w     6(a0),d1
                    move.w   d1,(a1)
                    move.w   d3,d2
                    move.w   (a0)+,0(a1,d2.w)
                    add.w    d3,d2
                    move.w   (a0)+,0(a1,d2.w)
                    add.w    d3,d2
                    move.w   (a0)+,0(a1,d2.w)
                    add.w    d3,d2
                    move.w   (a0)+,0(a1,d2.w)
                    addq.w   #2,a1
                    dbra     d0,lbC005650
                    clr.w    (a1)+
                    lea      $A0(a2),a0
                    dbra     d7,lbC00564C
                    movem.l  (sp)+,d0-d7/a0-a2
                    rts

RSTUFF_BUFF_SHORT:  movem.l  d0-d7/a0-a2,-(sp)
                    move.w   d6,d3
                    mulu     d7,d3
                    lsl.w    #1,d3
                    move.l   d3,(a1)+
                    move.w   d6,(a1)+
                    move.w   d7,(a1)+
                    subq.w   #1,d6
                    subq.w   #1,d7
                    lsl.w    #3,d2
                    add.w    d2,a0
                    lsl.w    #2,d2
                    add.w    d2,a0
                    asl.w    #1,d1
                    add.w    d1,a0
lbC0056AE:          move.l   a0,a2
                    move.w   d6,d0
lbC0056B2:          move.w   d3,d2
                    move.w   (a0),(a1)
                    move.w   $1F40(a0),0(a1,d2.w)
                    add.w    d3,d2
                    move.w   $3E80(a0),0(a1,d2.w)
                    add.w    d3,d2
                    move.w   $5DC0(a0),0(a1,d2.w)
                    addq.w   #2,a1
                    addq.w   #2,a0
                    dbra     d0,lbC0056B2
                    lea      $28(a2),a0
                    dbra     d7,lbC0056AE
                    movem.l  (sp)+,d0-d7/a0-a2
                    rts

RSPRITER_AM:        movem.l  d0-d7/a0-a4,-(sp)
                    move.w   MINTERMS,d5
                    lea      $DFF000,a2
                    move.w   #$8440,$96(a2)
                    move.w   4(a1),d6
                    cmp.w    #$FE2,d5
                    beq      lbC005706
                    subq.w   #1,d6
lbC005706:          lsl.w    #3,d2
                    add.w    d2,a0
                    lsl.w    #2,d2
                    add.w    d2,a0
                    move.w   d1,d3
                    and.w    #15,d3
                    lsr.w    #3,d1
                    and.w    #$FFFE,d1
                    add.w    d1,a0
                    move.w   #$FFFF,$44(a2)
                    move.w   #$FFFF,$46(a2)
                    ror.w    #4,d3
                    move.w   d3,$42(a2)
                    or.w     d5,d3
                    move.w   d3,$40(a2)
                    move.w   #0,$64(a2)
                    cmp.w    #$FE2,d5
                    beq      lbC005748
                    move.w   #2,$64(a2)
lbC005748:          move.w   #0,$62(a2)
                    move.w   #$28,d3
                    sub.w    d6,d3
                    sub.w    d6,d3
                    move.w   d3,$60(a2)
                    move.w   d3,$66(a2)
                    lsl.w    #6,d7
                    or.w     d6,d7
                    move.l   (a1),a3
                    lea      8(a1),a1
                    move.l   a1,a4
                    add.l    a3,a1
                    move.l   a1,$50(a2)
                    move.l   a4,$4C(a2)
                    move.l   a0,$48(a2)
                    move.l   a0,$54(a2)
                    move.w   d7,$58(a2)
                    add.w    #$1F40,a0
                    add.l    a3,a1
                    bsr      WAIT_BLIT
                    move.l   a1,$50(a2)
                    move.l   a4,$4C(a2)
                    move.l   a0,$48(a2)
                    move.l   a0,$54(a2)
                    move.w   d7,$58(a2)
                    add.w    #$1F40,a0
                    add.l    a3,a1
                    bsr      WAIT_BLIT
                    move.l   a1,$50(a2)
                    move.l   a4,$4C(a2)
                    move.l   a0,$48(a2)
                    move.l   a0,$54(a2)
                    move.w   d7,$58(a2)
                    add.w    #$1F40,a0
                    add.l    a3,a1
                    bsr      WAIT_BLIT
                    move.l   a1,$50(a2)
                    move.l   a4,$4C(a2)
                    move.l   a0,$48(a2)
                    move.l   a0,$54(a2)
                    move.w   d7,$58(a2)
                    movem.l  (sp)+,d0-d7/a0-a4
                    rts

X:                  dc.w     0
Y:                  dc.w     0
WIDTH:              dc.w     0
HEIGHT:             dc.w     0
BUFFER:             dc.l     0
SCREEN:             dc.l     0
MINTERMS:           dc.w     $FE2

TITLES:             move.w   #0,d0
                    bsr      MAKE_PALETTE_D0
                    move.w   #6,TPALETTE
                    clr.w    FADE_FLAG
INIT_CL:            move.l   SCREEN1,a0
                    move.l   SCREEN2,a1
                    move.w   #$1F3F,d0
lbC005818:          clr.l    (a0)+
                    clr.l    (a1)+
                    dbra     d0,lbC005818
                    bsr      SWAPSCREEN
                    bsr      FADE_IN
                    move.w   #$FFFF,FADE_VALUE
                    clr.w    ALTER_PALETTE_FLAG
                    bsr      INTRO_SEQUENCE
                    clr.w    TPALETTE
                    move.w   #$FFFF,FADE_FLAG
lbC005848:          tst.w    FADE_FLAG
                    bne.s    lbC005848
                    rts

TINTERRUPT:         addq.w   #1,TFERDINAND
                    tst.w    ALTER_PALETTE_FLAG
                    beq      lbC00586C
                    subq.w   #1,ALTER_PALETTE_FLAG
                    bra      lbC005878

lbC00586C:          move.w   #4,ALTER_PALETTE_FLAG
                    bsr      ALTER_PALETTE
lbC005878:          rts

MAKE_PALETTE_D0:    movem.l  a0/a1,-(sp)
                    move.w   d0,FADE_VALUE
                    lsl.w    #5,d0
                    lea      PA,a0
                    add.w    d0,a0
                    move.w   #15,d0
                    lea      $DFF180,a1
lbC005898:          move.w   (a0)+,d2
                    lsl.w    #1,d2
                    add.w    #$111,d2
                    move.w   d2,(a1)+
                    dbra     d0,lbC005898
                    movem.l  (sp)+,a0/a1
                    rts

ALTER_PALETTE:      movem.l  d1/d2,-(sp)
                    tst.w    FADE_FLAG
                    beq      lbC005904
                    bpl      lbC0058DE
                    subq.w   #1,FADE_VALUE
                    move.w   FADE_VALUE,d1
                    cmp.w    TPALETTE,d1
                    bne      lbC0058F6
                    clr.w    FADE_FLAG
                    bra      lbC0058F6

lbC0058DE:          addq.w   #1,FADE_VALUE
                    cmp.w    #$15,FADE_VALUE
                    bne      lbC0058F6
                    clr.w    FADE_FLAG
lbC0058F6:          move.l   d0,-(sp)
                    move.w   FADE_VALUE,d0
                    bsr      MAKE_PALETTE_D0
                    move.l   (sp)+,d0
lbC005904:          movem.l  (sp)+,d1/d2
                    rts

FADE_IN:            move.w   #$7CF,d0
                    move.l   BACK,a0
                    move.l   SCREEN2,a1
lbC00591A:          move.l   (a0)+,(a1)+
                    move.l   (a0)+,(a1)+
                    move.l   (a0)+,(a1)+
                    move.l   (a0)+,(a1)+
                    dbra     d0,lbC00591A
                    bsr      SWAPSCREEN
                    rts

INTRO_SEQUENCE:     clr.w    TCOUNTER
                    clr.w    SUBHTW
                    clr.w    SUBHTB
                    move.w   #4,SUBXW
                    move.w   #$100,SUBXB
                    moveq    #13,d7
                    bsr      NEW_SOUND
IS_1:               eor.w    #1,NEW_COUNTER
                    tst.w    NEW_COUNTER
                    bne      lbC00596C
                    addq.w   #1,TCOUNTER
lbC00596C:          move.w   TCOUNTER,d4
                    move.l   BACK,a0
                    btst     #0,d4
                    bne      lbC005994
                    cmp.w    #$140,d4
                    blt      lbC005994
                    cmp.w    #$150,d4
                    bgt      lbC005994
                    add.w    #$50,a0
lbC005994:          move.l   SCREEN2,a1
                    move.w   #$7CF,d0
lbC00599E:          move.l   (a0)+,(a1)+
                    move.l   (a0)+,(a1)+
                    move.l   (a0)+,(a1)+
                    move.l   (a0)+,(a1)+
                    dbra     d0,lbC00599E
                    cmp.w    #$100,d4
                    bge      IS_5
                    cmp.w    #$40,d4
                    bgt      lbC0059DC
                    lea      FIRSTSTAR,a1
                    move.l   #0,d1
                    move.l   #5,d2
                    move.l   #$14,d6
                    move.l   #$17,d7
                    bra      lbC005A4A

lbC0059DC:          cmp.w    #$80,d4
                    bgt      lbC005A06
                    lea      MAD_MAG,a1
                    move.l   #$20,d1
                    move.l   #5,d2
                    move.l   #$10,d6
                    move.l   #$1C,d7
                    bra      lbC005A4A

lbC005A06:          cmp.w    #$C0,d4
                    bgt      lbC005A30
                    lea      SPYVSPY,a1
                    move.l   #$4A,d1
                    move.l   #3,d2
                    move.l   #12,d6
                    move.l   #$30,d7
                    bra      lbC005A4A

lbC005A30:          lea      ARCTIC_ANTS,a1
                    move.l   #$22,d1
                    moveq    #0,d2
                    move.l   #$12,d6
                    move.l   #$42,d7
lbC005A4A:          move.l   SCREEN2,a0
                    bsr      RSPRITER_AM
                    move.w   d4,d0
                    and.w    #$3F,d0
                    cmp.w    #1,d0
                    bne      lbC005A6E
                    move.w   #1,FADE_FLAG
                    bra      _IS_9

lbC005A6E:          cmp.w    #$28,d0
                    bne      _IS_9
                    move.w   #$FFFF,FADE_FLAG
_IS_9:              bra      IS_9

IS_5:               cmp.w    #$104,d4
                    bne      lbC005A9A
                    tst.w    NEW_COUNTER
                    bne      lbC005A9A
                    moveq    #5,d7
                    bsr      NEW_SOUND
lbC005A9A:          cmp.w    #$11C,d4
                    beq      lbC005AAA
                    cmp.w    #$12C,d4
                    bne      lbC005ABE
lbC005AAA:          tst.w    NEW_COUNTER
                    bne      lbC005ABE
                    moveq    #7,d7
                    bsr      NEW_SOUND
                    bra      lbC005ABE

lbC005ABE:          cmp.w    #$140,d4
                    bge      IS_8
                    move.l   SCREEN2,a0
                    move.w   SUBXW,d1
                    move.w   SUBHTW,d7
                    lea      WHITE_SUB,a1
                    moveq    #1,d4
                    bsr      MOVE_SUB
                    move.w   d7,SUBHTW
                    move.w   d1,SUBXW
                    move.w   SUBXB,d1
                    move.w   SUBHTB,d7
                    lea      BLACK_SUB,a1
                    moveq    #-1,d4
                    bsr      MOVE_SUB
                    move.w   d7,SUBHTB
                    move.w   d1,SUBXB
                    move.w   #$80,d1
                    move.w   #$64,d2
                    move.w   #4,d6
                    move.w   #$38,d7
                    lea      OVERLAY,a1
                    move.l   SCREEN2,a0
                    bsr      RSPRITER_AM
                    bra      IS_9

IS_8:               cmp.w    #$140,d4
                    bne      lbC005B50
                    tst.w    NEW_COUNTER
                    bne      lbC005B50
                    moveq    #4,d7
                    bsr      NEW_SOUND
lbC005B50:          cmp.w    #$168,d4
                    bge      IS_10
                    move.w   d4,d0
                    and.w    #1,d0
                    lsl.w    #2,d0
                    lea      CAVES,a1
                    move.l   0(a1,d0.w),a1
                    move.w   #$60,d1
                    move.w   #$61,d2
                    move.w   #$22,d7
                    move.w   #7,d6
                    move.l   SCREEN2,a0
                    bsr      RSPRITER_AM
IS_9:               bsr      DO_PENGUIN
lbC005B88:          cmp.w    #2,TFERDINAND
                    blt.s    lbC005B88
                    bsr      SWAPSCREEN
                    clr.w    TFERDINAND
                    bra      IS_1

IS_10:              rts

SPLASHES:           dc.l     SPLASH1
                    dc.l     SPLASH2
                    dc.l     SPLASH3
                    dc.l     SPLASH4
CAVES:              dc.l     RED_CAVE
                    dc.l     BLACK_CAVE

MOVE_SUB:           cmp.w    #$1B,d7
                    beq      lbC005BC8
                    addq.w   #1,d7
                    bra      lbC005BCA

lbC005BC8:          add.w    d4,d1
lbC005BCA:          move.w   #$7F,d2
                    sub.w    d7,d2
                    move.w   #4,d6
                    bsr      RSPRITER_AM
                    move.w   d7,d4
                    move.w   d1,d3
                    move.w   #$7F,d2
                    subq.w   #4,d1
                    move.w   TCOUNTER,d0
                    and.w    #3,d0
                    lsl.w    #2,d0
                    lea      SPLASHES,a1
                    move.l   0(a1,d0.w),a1
                    move.w   #5,d6
                    move.w   #14,d7
                    bsr      RSPRITER_AM
                    move.w   d3,d1
                    move.w   d4,d7
                    rts

DO_PENGUIN:         lea      ROCKDATA1,a3
                    bsr      ONE_PENGY
                    lea      ROCKDATA2,a3
                    bsr      ONE_PENGY
                    lea      ROCKDATA3,a3
                    bsr      ONE_PENGY
                    lea      ROCKDATA4,a3
                    bsr      ONE_PENGY
                    rts

WHICH_PENGUIN:      dc.l     PENGY_L
                    dc.l     PENGY_F
                    dc.l     PENGY_R

ONE_PENGY:          move.w   0(a3),d1
                    move.w   2(a3),d2
                    move.w   $14(a3),d3
                    bpl      lbC005C64
                    add.w    d3,d2
                    cmp.w    #$A8,d2
                    bne      lbC005C74
                    move.w   #1,$14(a3)
                    bra      lbC005C74

lbC005C64:          add.w    d3,d2
                    cmp.w    #$C1,d2
                    bne      lbC005C74
                    move.w   #$FFFF,$14(a3)
lbC005C74:          move.w   d2,2(a3)
                    move.l   8(a3),a1
                    move.w   6(a3),d6
                    move.w   4(a3),d7
                    move.l   SCREEN2,a0
                    bsr      RSPRITER_LIMITED
                    add.w    12(a3),d1
                    add.w    14(a3),d2
                    move.w   #$1E,d7
                    move.w   #2,d6
                    tst.w    $12(a3)
                    beq      lbC005CAE
                    subq.w   #1,$12(a3)
                    bra      lbC005CC6

lbC005CAE:          move.w   #$14,$12(a3)
                    addq.w   #1,$10(a3)
                    cmp.w    #3,$10(a3)
                    bne      lbC005CC6
                    clr.w    $10(a3)
lbC005CC6:          move.w   $10(a3),d0
                    lsl.w    #2,d0
                    lea      WHICH_PENGUIN,a1
                    move.l   0(a1,d0.w),a1
                    bsr      RSPRITER_LIMITED
                    rts

RSPRITER_LIMITED:   move.w   d0,-(sp)
                    move.w   d2,d0
                    add.w    d7,d0
                    cmp.w    #$C8,d0
                    ble      _RSPRITER_AM
                    move.w   #$C8,d7
                    sub.w    d2,d7
_RSPRITER_AM:       bsr      RSPRITER_AM
                    move.w   (sp)+,d0
                    rts

MASK_TABLE:         dc.w     0,$4000,$2000,$10,$8010,$4100,$8200,$8010,$4210
                    dc.w     $8124,$4217,$8127,$4213,$971E,$8818,$6666,$9999
                    dc.w     $5555,$9999,$5555,$9999,$5555,$9999,$5555,$AAAA
                    dc.w     $5555,$AAAA,$5555,$AAAA,$5555,$AAAA,$5555,$AAAA
                    dc.w     $5555,$AAAA,$5555,$AAAA,$5555,$AAAA,$5555,$AAAA
                    dc.w     $5555,$AAAA,$5555,$AAAA,$5555,$AAAA,$5555,$AAAA
                    dc.w     $5555,$AAAA,$5555,$AAAA,$5555,$AAAA,$5555,$AAAA
                    dc.w     $5555,$AAAA,$5555,$AAAA,$5555,$AAAA,$5555
ROCKDATA1:          dc.w     $14,$B9,$1D,3
                    dc.l     ROCK1
                    dc.w     8,$FFEE,0,6,1
ROCKDATA2:          dc.w     $50,$AA,$16,4
                    dc.l     ROCK2
                    dc.w     $10,$FFF1,1,$12,1
ROCKDATA3:          dc.w     $A0,$B4,$1C,4
                    dc.l     ROCK3
                    dc.w     12,$FFF1,2,14,1
ROCKDATA4:          dc.w     $F0,$BE,$11,2
                    dc.l     ROCK4
                    dc.w     4,$FFEC,0,9,1
PA:                 dc.w     $357,$357,$357,$357,$357,$357,$357,$357,$357,$357
                    dc.w     $357,$357,$357,$357,$357,$357
PB:                 dc.w     $357,$357,$357,$357,$347,$346,$356,$457,$357,$357
                    dc.w     $347,$456,$357,$357,$357,$357
PC:                 dc.w     $357,$357,$456,$357,$247,$234,$356,$557,$357,$357
                    dc.w     $247,$445,$347,$357,$357,$357
PD:                 dc.w     $357,$357,$456,$357,$237,$233,$355,$567,$357,$357
                    dc.w     $236,$544,$347,$357,$367,$357
PE:                 dc.w     $357,$357,$555,$357,$127,$122,$445,$667,$357,$357
                    dc.w     $126,$533,$337,$357,$367,$357
PF:                 dc.w     $357,$357,$555,$357,$117,$11,$445,$667,$357,$357
                    dc.w     $116,$631,$337,$357,$367,$357
P0:                 dc.w     $357,$357,$555,$357,7,0,$444,$777,$357,$357,6
                    dc.w     $630,$337,$357,$377,$357
P1:                 dc.w     $357,$357,$555,$357,7,0,$444,$777,$357,$357,6
                    dc.w     $630,$337,$357,$377,$357
P2:                 dc.w     $357,$356,$555,$456,7,0,$444,$777,$457,$357,6
                    dc.w     $630,$337,$357,$377,$346
P3:                 dc.w     $357,$356,$555,$456,7,0,$444,$777,$457,$357,6
                    dc.w     $630,$337,$357,$377,$246
P4:                 dc.w     $357,$455,$555,$455,7,0,$444,$777,$457,$357,6
                    dc.w     $630,$337,$357,$377,$245
P5:                 dc.w     $357,$445,$555,$455,7,0,$444,$777,$457,$357,6
                    dc.w     $630,$337,$357,$377,$235
P6:                 dc.w     $357,$444,$555,$554,7,0,$444,$777,$557,$357,6
                    dc.w     $630,$337,$357,$377,$234
P7:                 dc.w     $357,$444,$555,$564,7,0,$444,$777,$567,$456,6
                    dc.w     $630,$337,$357,$377,$234
P8:                 dc.w     $357,$543,$555,$563,7,0,$444,$777,$567,$456,6
                    dc.w     $630,$337,$357,$377,$133
P9:                 dc.w     $357,$543,$555,$563,7,0,$444,$777,$567,$456,6
                    dc.w     $630,$337,$357,$377,$123
P10:                dc.w     $357,$543,$555,$663,7,0,$444,$777,$667,$456,6
                    dc.w     $630,$337,$357,$377,$123
P11:                dc.w     $357,$542,$555,$662,7,0,$444,$777,$667,$456,6
                    dc.w     $630,$337,$357,$377,$122
P12:                dc.w     $357,$642,$555,$662,7,0,$444,$777,$667,$456,6
                    dc.w     $630,$337,$357,$377,$112
P13:                dc.w     $357,$631,$555,$671,7,0,$444,$777,$677,$555,6
                    dc.w     $630,$337,$357,$377,$111
P14:                dc.w     $357,$631,$555,$771,7,0,$444,$777,$777,$555,6
                    dc.w     $630,$337,$357,$377,$11
P15:                dc.w     $357,$720,$555,$770,7,0,$444,$777,$777,$555,6
                    dc.w     $630,$337,$357,$377,0
NEW_COUNTER:        dc.l     0

                    SECTION  spy3titles006094,BSS,CHIP
WHITE_SUB:          ds.l     $153
                    ds.w     1
BLACK_SUB:          ds.b     $54E
ROCK1:              ds.l     $124
ROCK2:              ds.l     $115
ROCK3:              ds.l     $160
ROCK4:              ds.l     $81
                    ds.w     1
PENGY_L:            ds.l     $E3
PENGY_F:            ds.l     $E3
PENGY_R:            ds.l     $E3
SPYVSPY:            ds.l     $61A
MAD_MAG:            ds.l     $4A8
ARCTIC_ANTS:        ds.l     $C41
SPLASH1:            ds.l     $D4
SPLASH2:            ds.l     $D4
SPLASH3:            ds.l     $D4
SPLASH4:            ds.l     $D4
OVERLAY:            ds.l     $2BE
BLACK_CAVE:         ds.l     $2AA
RED_CAVE:           ds.l     $2AA
FIRSTSTAR:          ds.l     $4B9
                    ds.w     1
SUBXW:              ds.w     1
SUBXB:              ds.w     1
SUBHTW:             ds.w     1
SUBHTB:             ds.w     1
SPLASH_BACK:        ds.w     1
SPLASH_COUNT:       ds.w     1
ALTER_PALETTE_FLAG: ds.w     1
FADE_FLAG:          ds.w     1
FADE_VALUE:         ds.w     1
SCREENA:            ds.l     $2000
SCREENB:            ds.l     $2000
SCREENC:            ds.l     $2000
BACK:               ds.l     1
TPALETTE:           ds.w     $10
TCOUNTER:           ds.w     1
TFERDINAND:         ds.w     1
TSAVE_KEY:          ds.w     2
TSAVE_ADD:          ds.w     2
TSAVE_STACK:        ds.w     3

                    end
