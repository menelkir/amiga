; -------------------------------------------
; Spy VS Spy III - Arctic Antics.
; Game.
; Disassembled by Franck Charlet.
; -------------------------------------------

                    SECTION  ArcticAntics000000,CODE,CHIP
ProgStart:
START_CODE:         jmp      START

PLANES:             dc.w     $E2,0,$E0,0,$E6,0,$E4,0,$EA,0,$E8,0,$EE,0,$EC,0
COPPER_SPRITE:      dc.w     $120
lbW000028:          dc.w     0,$122,0,$124,0,$126,0,$128,0,$12A,0,$12C,0,$12E
                    dc.w     0,$130,0,$132,0,$134,0,$136,0,$138,0,$13A,0,$13C
                    dc.w     0,$13E,0,$192
TOP_GREEN:          dc.w     $7B3,$7D01,$FFFE,$192,$7B3
                    dc.w     $1fc,0,$106,0
                    dc.w     $FFFF,$FFFE
DUMMY_SPRITE:       dcb.w    2,0

WAIT_FOR_RASTER:    move.l   d1,d2
                    add.w    #$B00,d2
lbC000080:          move.l   $DFF004,d0
                    and.l    #$1FFFF,d0
                    cmp.l    d1,d0
                    bls.s    lbC000080
                    cmp.l    d2,d0
                    bhi.s    lbC000080
                    rts

ATARI_COPY:         move.l   #$F9F,d0
lbC00009C:          move.w   (a0)+,(a1)+
                    move.w   (a0)+,7998(a1)
                    move.w   (a0)+,15998(a1)
                    move.w   (a0)+,23998(a1)
                    dbra     d0,lbC00009C
                    rts

COLOUR_COPY:        lea      $DFF180,a1
                    move.w   #15,d0
lbC0000BA:          move.w   (a0)+,d1
                    lsl.w    #1,d1
                    move.w   d1,(a1)+
                    dbra     d0,lbC0000BA
                    rts

LOAD_FILE:          move.l   d2,-(sp)
                    move.l   d1,-(sp)
                    move.l   4,a6
                    moveq    #0,d0
                    move.l   #DOS_NAME,a1
                    jsr      -552(a6)
                    move.l   d0,DOS_BASE
                    move.l   d0,a6
                    move.l   (sp)+,d1
                    move.l   #$3ED,d2
                    jsr      -30(a6)
                    move.l   d0,DOS_HANDLE
                    move.l   d0,d1
                    move.l   (sp)+,d2
                    move.l   #$7D80,d3
                    jsr      -42(a6)
                    move.l   DOS_HANDLE,d1
                    jsr      -36(a6)
                    rts

SETUP_SCREEN:       move.l   ONESCREEN,SCREEN1
                    move.l   TWOSCREEN,SCREEN2
                    lea      $DFF000,a6
                    move.w   #$3FF,$96(a6)
                    move.l   SCREEN2,d1
                    bsr      WHICH_PLANES
                    move.w   #7,d0
                    lea      lbW000028,a0
                    move.l   #DUMMY_SPRITE,d1
lbC00014A:          swap     d1
                    move.w   d1,(a0)
                    addq.w   #4,a0
                    swap     d1
                    move.w   d1,(a0)
                    addq.w   #4,a0
                    dbra     d0,lbC00014A
                    move.w   #$38,d0
                    subq.w   #1,d0
                    lea      PLANES,a0
                    lea      COPPER_LIST2,a1
lbC00016C:          move.w   (a0)+,(a1)+
                    dbra     d0,lbC00016C
                    move.l   SCREEN1,d1
                    bsr      WHICH_PLANES
                    move.l   #PLANES,COPPER1
                    move.l   #COPPER_LIST2,COPPER2
                    move.l   #PLANES,$80(a6)
                    move.w   #$3070,$8E(a6)
                    move.w   #$F8B1,$90(a6)
                    move.w   #$30,$92(a6)
                    move.w   #$C8,$94(a6)
                    clr.w    $108(a6)
                    clr.w    $10A(a6)
                    clr.w    $102(a6)
                    move.w   #$4200,$100(a6)
                    move.w   #4,$104(a6)
                    clr.w    $88(a6)
                    move.w   #$83FF,$96(a6)
                    rts

WHICH_PLANES:       move.w   #3,d0
                    lea      PLANES,a0
lbC0001DE:          move.w   d1,2(a0)
                    swap     d1
                    move.w   d1,6(a0)
                    swap     d1
                    add.l    #$1F40,d1
                    addq.w   #8,a0
                    dbra     d0,lbC0001DE
                    rts

BLIT:               bsr      WAIT_BLIT
                    move.l   d0,$DFF060
                    move.l   d1,$DFF064
                    move.l   d2,$DFF040
                    move.w   #$FFFF,$DFF044
                    move.w   #$FFFF,$DFF046
BLIT2:              move.l   a0,$DFF050
                    move.l   a1,$DFF04C
                    move.l   a2,$DFF048
                    move.l   a3,$DFF054
                    move.w   d3,$DFF058
                    rts

WAIT_BLIT:          btst     #6,$DFF002
                    bne.s    WAIT_BLIT
                    rts

SWAP_SCREEN:        movem.l  d0/d1,-(sp)
                    bsr.s    WAIT_BLIT
                    move.l   SCREEN1,d0
                    move.l   SCREEN2,SCREEN1
                    move.l   d0,SCREEN2
                    move.l   COPPER1,d0
                    move.l   COPPER2,COPPER1
                    move.l   d0,COPPER2
                    move.l   COPPER1,$DFF080
                    move.w   #1,INT_REQ
lbC00028E:          tst.w    INT_REQ
                    bne.s    lbC00028E
                    movem.l  (sp)+,d0/d1
                    rts

CLEAR_SCREEN:       move.w   #$7CF,d0
lbC0002A0:          clr.l    (a0)+
                    clr.l    (a0)+
                    clr.l    (a0)+
                    clr.l    (a0)+
                    dbra     d0,lbC0002A0
                    rts

MYINT2:             movem.l  d0-d2/a0,-(sp)
                    bsr      ADD_MATRIX
                    bset     #6,$BFEE01
                    move.b   #0,$BFEC01
                    move.w   #$50,d0
lbC0002CA:          dbra     d0,lbC0002CA
                    bclr     #6,$BFEE01
                    tst.b    $BFED01
                    movem.l  (sp)+,d0-d2/a0
                    move.w   #8,$DFF09C
                    rte

ADD_MATRIX:         move.b   $BFEC01,d0
                    ror.b    #1,d0
                    move.b   d0,d1
                    lea      KB_MATRIX,a0
                    and.w    #$7F,d0
                    eor.w    #$7F,d0
                    move.w   d0,d2
                    and.w    #7,d2
                    lsr.w    #3,d0
                    tst.b    d1
                    bmi.s    DOWN_STROKE
UP_STROKE:          bset     d2,0(a0,d0.w)
                    rts

DOWN_STROKE:        bclr     d2,0(a0,d0.w)
                    rts

INIT_KEY:           moveq    #-1,d0
                    lea      KB_MATRIX,a0
                    move.l   d0,(a0)+
                    move.l   d0,(a0)+
                    move.l   d0,(a0)+
                    move.l   d0,(a0)+
                    rts

INKEY:              movem.l  d2/a0,-(sp)
                    move.w   d0,d2
                    and.w    #7,d2
                    lsr.w    #3,d0
                    lea      KB_MATRIX,a0
                    btst     d2,0(a0,d0.w)
                    bne      lbC00034C
                    moveq    #-1,d0
                    bra      lbC00034E

lbC00034C:          moveq    #0,d0
lbC00034E:          movem.l  (sp)+,d2/a0
                    rts

SCAN_JOY:           movem.l  d0/d1,-(sp)
                    move.w   $DFF00A,d0
                    move.w   d0,d1
                    and.w    #$202,d0
                    move.w   d0,LEFT0
                    lsr.w    #1,d0
                    eor.w    d0,d1
                    and.w    #$101,d1
                    move.w   d1,UP0
                    move.w   $DFF00C,d0
                    move.w   d0,d1
                    and.w    #$202,d0
                    move.w   d0,LEFT1
                    lsr.w    #1,d0
                    eor.w    d0,d1
                    and.w    #$101,d1
                    move.w   d1,UP1
                    clr.w    FIRE0
                    moveq    #0,d0
                    move.b   $BFE001,d0
                    btst     #6,d0
                    bne      lbC0003B6
                    move.w   #$FFFF,FIRE0
lbC0003B6:          clr.w    FIRE1
                    btst     #7,d0
                    bne      lbC0003CC
                    move.w   #$FFFF,FIRE1
lbC0003CC:          move.w   #$4C,d0
                    bsr      INKEY
                    move.b   d0,KUP1
                    move.w   #$4D,d0
                    bsr      INKEY
                    move.b   d0,KDOWN1
                    move.w   #$4F,d0
                    bsr      INKEY
                    move.b   d0,KLEFT1
                    move.w   #$4E,d0
                    bsr      INKEY
                    move.b   d0,KRIGHT1
                    movem.l  (sp)+,d0/d1
                    rts

KB_MATRIX:          dcb.l    4,$FFFFFFFF
LEFT0:              dc.b     $FF
RIGHT0:             dc.b     $FF
UP0:                dc.b     0
DOWN0:              dc.b     1
FIRE0:              dc.w     0
LEFT1:              dc.b     $FF
RIGHT1:             dc.b     $FF
UP1:                dc.b     $FF
DOWN1:              dc.b     $FF
FIRE1:              dc.w     0

HANDLE_MOUSE:       movem.l  d0-d2/a0,-(sp)
                    move.w   $DFF00A,d0
                    lea      LAST_MOUSE0,a0
                    bsr      DO_MOUSE
                    move.w   $DFF00C,d0
                    lea      LAST_MOUSE1,a0
                    bsr      DO_MOUSE
                    clr.w    MFIRE0
                    move.w   $DFF016,d0
                    btst     #10,d0
                    bne      lbC000466
                    move.w   #$FFFF,MFIRE0
lbC000466:          clr.w    MFIRE1
                    btst     #14,d0
                    bne      lbC00047C
                    move.w   #$FFFF,MFIRE1
lbC00047C:          movem.l  (sp)+,d0-d2/a0
                    rts

DO_MOUSE:           move.l   2(a0),d1
                    beq      CHECK_MOUSE
                    tst.b    d1
                    beq      lbC000494
                    sub.b    #1,d1
lbC000494:          rol.l    #8,d1
                    tst.b    d1
                    beq      lbC0004A0
                    sub.b    #1,d1
lbC0004A0:          rol.l    #8,d1
                    tst.b    d1
                    beq      lbC0004AC
                    sub.b    #1,d1
lbC0004AC:          rol.l    #8,d1
                    tst.b    d1
                    beq      lbC0004B8
                    sub.b    #1,d1
lbC0004B8:          rol.l    #8,d1
                    move.l   d1,2(a0)
CHECK_MOUSE:        move.w   d0,d2
                    sub.b    1(a0),d0
                    beq      DOM_UD
                    bpl      DOM_RIGHT
                    neg.b    d0
                    cmp.b    #1,d0
                    bls      DOM_UD
                    move.b   #$14,2(a0)
                    bra      DOM_UD

DOM_RIGHT:          cmp.b    #1,d0
                    bls      DOM_UD
                    move.b   #$14,3(a0)
DOM_UD:             move.w   d2,d0
                    lsr.w    #8,d0
                    sub.b    (a0),d0
                    beq      DOM_RET
                    bpl      DOM_DOWN
                    neg.b    d0
                    cmp.b    #1,d0
                    bls      DOM_RET
                    move.b   #$14,4(a0)
                    bra      DOM_RET

DOM_DOWN:           cmp.b    #1,d0
                    bls      DOM_RET
                    move.b   #$14,5(a0)
DOM_RET:            move.w   d2,(a0)
                    rts

LAST_MOUSE0:        dc.w     0
MLEFT0:             dc.b     0
MRIGHT0:            dc.b     0
MUP0:               dc.b     0
MDOWN0:             dc.b     0
MFIRE0:             dc.w     0
LAST_MOUSE1:        dc.w     0
MLEFT1:             dc.b     0
MRIGHT1:            dc.b     0
MUP1:               dc.b     0
MDOWN1:             dc.b     0
MFIRE1:             dc.w     0
KLEFT1:             dc.b     0
KRIGHT1:            dc.b     0
KUP1:               dc.b     0
KDOWN1:             dc.b     0

ALLOCATE_MEMORY:    move.w   #$100,d7
                    move.w   #$700,d6
                    bsr      FIND32K
                    bsr      CLEARD0
                    move.l   d0,ONESCREEN
                    move.w   #$10,d7
                    move.w   #$70,d6
                    bsr      FIND32K
                    bsr      CLEARD0
                    move.w   #1,d7
                    move.w   #7,d6
                    move.l   d0,TWOSCREEN
                    bsr      FIND32K
                    move.l   d0,BACK
                    rts

CLEARD0:            move.l   d0,a0
                    move.w   #$1F3F,d1
lbC00057C:          clr.l    (a0)+
                    dbra     d1,lbC00057C
                    rts

FIND32K:            move.l   4,a6
                    move.l   #$8000,d0
                    move.l   #2,d1
                    jsr      -$C6(a6)
                    tst.l    d0
                    beq      WHOS_REMOVED_THE_RAM_CHIPS
                    rts

WHOS_REMOVED_THE_RAM_CHIPS:
                    move.w   d7,$DFF180
                    move.w   d6,$DFF180
                    bra.s    WHOS_REMOVED_THE_RAM_CHIPS

DECO_PIC:           move.l   d2,-(sp)
                    move.l   BACK,d2
                    bsr      LOAD_FILE
                    move.l   (sp)+,a1
DECO:               move.l   BACK,a0
                    addq.w   #2,a0
                    addq.w   #4,a1
                    move.w   #15,d0
DECO_PAL:           move.w   (a0)+,(a1)+
                    dbra     d0,DECO_PAL
                    add.w    #$5C,a1
                    move.w   #3,d0
DECO_THREE:         move.l   a1,a2
                    move.w   #$1F40,d1
                    clr.w    FLAG
DECO_LOOP:          move.b   (a0)+,d2
                    cmp.b    #$CD,d2
                    bne      DECO_ORD
                    move.b   (a0)+,d2
                    bne      NOT_CD_DECO
                    move.b   #$CD,d2
                    bra      DECO_ORD

NOT_CD_DECO:        bra      DECO_REP

DECO_ORD:           move.b   d2,(a1)
                    tst.w    FLAG
                    bne      ADD_LOTS
                    addq.w   #1,FLAG
                    addq.w   #1,a1
                    subq.w   #1,d1
                    beq      DONE_DECO
                    bra.s    DECO_LOOP

ADD_LOTS:           clr.w    FLAG
                    addq.w   #7,a1
                    subq.w   #1,d1
                    beq      DONE_DECO
                    bra.s    DECO_LOOP

DECO_REP:           move.w   d2,d3
                    and.w    #$7F,d3
                    tst.b    d2
                    bmi      USE_FF
                    move.w   #0,d2
                    bra      DECO_LOTS

USE_FF:             move.w   #$FFFF,d2
DECO_LOTS:          move.b   d2,(a1)
                    tst.w    FLAG
                    bne      ADD_LOTS2
                    add.w    #1,FLAG
                    add.w    #1,a1
                    sub.w    #1,d1
                    beq      DONE_DECO
                    sub.b    #1,d3
                    bne.s    DECO_LOTS
                    bra      DECO_LOOP

ADD_LOTS2:          clr.w    FLAG
                    addq.w   #7,a1
                    subq.w   #1,d1
                    beq      DONE_DECO
                    subq.b   #1,d3
                    bne.s    DECO_LOTS
                    bra      DECO_LOOP

DONE_DECO:          lea      2(a2),a1
                    dbra     d0,DECO_THREE
                    rts

FLAG:               dc.w     0
DOS_NAME:           dc.b     'dos.library',0
DOS_BASE:           dc.l     0
DOS_HANDLE:         dc.l     0
ONESCREEN:          dc.l     0
TWOSCREEN:          dc.l     0
LOADSCREEN:         dc.l     0
SCREEN2:            dc.l     0
SCREEN1:            dc.l     0
COPPER1:            dc.l     0
COPPER2:            dc.l     0
COPPER_LIST2:       dcb.l    $1C,0

                    jmp      TEST

FREQS:              dc.w     $194,$17D,$167,$153,$140,$12E,$11D,$10D,$FE,$F0
                    dc.w     $E2,$D5
ENVELOPE1:          dc.w     1,$32,0,$19,$FFFE,0,$FFFF
ENVELOPE2:          dc.w     1,$1E,0,1,$FFFF,$FFFC,1,0,4,1,$FFFF,$FFFC,1,0,4,1
                    dc.w     $FFFF,$FFFC,1,0,4,1,$FFFF,$FFFC,1,0,4,1,$FFFF
                    dc.w     $FFFC,1,0,4,1,$FFFF,$FFFC,1,0,4,1,$FFFF,$FFFC,1,0
                    dc.w     4,1,$FFFF,$FFFC,1,0,4,1,$FFFF,$FFFC,1,0,4,1,$FFFF
                    dc.w     $FFFC,1,0,4,1,$FFFF,$FFFC,1,0,4,1,$FFFF,$FFFC,1,0
                    dc.w     4,1,$FFFF,$FFFC,1,0,4,1,$FFFF,$FFFC,1,0,4,1,$FFFF
                    dc.w     $FFFC,1,0,4,1,$FFFF,$FFFC,1,0,4,1,$FFFF,$FFFC,1,0
                    dc.w     4,1,$FFFF,$FFFC,1,0,4,1,$FFFF,$FFFC,1,0,4,1,$FFFF
                    dc.w     $FFFC,1,0,4,1,$FFFF,$FFFC,1,0,4,1,$FFFF,$FFFC,1,0
                    dc.w     4,1,$FFFF,$FFFC,1,0,4,1,$FFFF,$FFFC,1,0,4,1,$FFFF
                    dc.w     $FFFC,1,0,4,1,$FFFF,$FFFC,1,0,4,1,$FFFF,$FFFC,1,0
                    dc.w     4,1,$FFFF,$FFFC,1,0,4,1,$FFFF,$FFFC,1,0,4,1,$FFFF
                    dc.w     $FFFC,1,0,4,$FFFF
ENVELOPE3:          dc.w     1,$1E,0,15,$FFFE,1,$FFFF
DUMMY:              dc.w     $FFFF
TRIANGLE32:         dc.b     $80,$90,$A0,$50,$C0,$D0,$E0,$F0,0,$10,$20,$30,$40
                    dc.b     $50,$60,$70,$7F,$70,$60,$50,$40,$30,$20,$10,0,$F0
                    dc.b     $E0,$D0,$C0,$B0,$A0,$90
WAVEFORM128:        dc.b     0,6,13,$13,$19,$1F,$25,$2B,$31,$37,$3C,$42,$47
                    dc.b     $4C,$51,$56,$5B,$5F,$63,$67,$6A,$6E,$71,$74,$76
                    dc.b     $79,$7A,$7C,$7E,$7F,$7F,$7F,$7F,$7F,$7F,$7F,$7E
                    dc.b     $7C,$7A,$79,$76,$74,$71,$6E,$6A,$67,$63,$5F,$5B
                    dc.b     $56,$51,$4C,$47,$42,$3C,$37,$31,$2B,$25,$1F,$19
                    dc.b     $13,13,6,0,$FA,$F3,$ED,$E7,$E1,$DB,$D5,$CF,$C9
                    dc.b     $C4,$BE,$B9,$B4,$AF,$AA,$A5,$A1,$9D,$99,$96,$92
                    dc.b     $8F,$8C,$8A,$87,$86,$84,$82,$81,$81,$80,$80,$80
                    dc.b     $81,$81,$82,$84,$86,$87,$8A,$8C,$8F,$92,$96,$99
                    dc.b     $9D,$A1,$A5,$AA,$AF,$B4,$B9,$BE,$C4,$C9,$CF,$D5
                    dc.b     $DB,$E1,$E7,$ED,$F3,$FA
WAVEFORM64:         dc.b     0,13,$19,$25,$31,$3C,$47,$51,$5B,$63,$6A,$71,$76
                    dc.b     $7A,$7E,$7F,$7F,$7F,$7E,$7A,$76,$71,$6A,$63,$5B
                    dc.b     $51,$47,$3C,$31,$25,$19,13,0,$F3,$E7,$DB,$CF,$C4
                    dc.b     $B9,$AF,$A5,$9D,$96,$8F,$8A,$86,$82,$81,$80,$81
                    dc.b     $82,$86,$8A,$8F,$96,$9D,$A5,$AF,$B9,$C4,$CF,$DB
                    dc.b     $E7,$F3
WAVEFORM32:         dc.b     0,$19,$31,$47,$5B,$6A,$76,$7E,$7F,$7E,$76,$6A,$5B
                    dc.b     $47,$31,$19,0,$E7,$CF,$B9,$A5,$96,$8A,$82,$80,$82
                    dc.b     $8A,$96,$A5,$B9,$CF,$E7
WAVEFORM16:         dc.b     0,$31,$5B,$76,$7F,$76,$5B,$31,0,$CF,$A5,$8A,$80
                    dc.b     $8A,$A5,$CF
WAVEFORM8:          dc.b     0,$5B,$7F,$5B,0,$A5,$80,$A5
WAVEFORM4:          dc.b     0,$7F,0,$80
WAVEFORM2:          dc.b     $C0,$40
WHICH_WAVE:         dc.l     WAVEFORM128
                    dc.l     WAVEFORM64
                    dc.l     WAVEFORM32
                    dc.l     WAVEFORM16
                    dc.l     WAVEFORM8
                    dc.l     WAVEFORM4
                    dc.l     WAVEFORM2
WAVE_LENGTH:        dc.w     $40,$20,$10,8,4,2,1
E_HIGH_PING:        dc.w     1,$20,0,5,0,1,$20,$FFFF,0,$FFFF
E_MEDIUM_PING:      dc.w     1,$20,0,5,0,1,$20,$FFFF,0,$FFFF
E_LOW_PING:         dc.w     1,$20,0,5,0,1,$20,$FFFF,0,$FFFF
E_MEDIUM_BEEP:      dc.w     1,$40,0,$19,0,0,1,$FFC0,0,$FFFF
E_BANG:             dc.w     1,$40,0,$20,$FFFE,0,$FFFF
E_BOOM:             dc.w     1,$40,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0
                    dc.w     7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7
                    dc.w     0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0
                    dc.w     0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0
                    dc.w     1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1
                    dc.w     $FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1
                    dc.w     $FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1
                    dc.w     $FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1
                    dc.w     $FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1
                    dc.w     $FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1
                    dc.w     $FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1
                    dc.w     $FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1
                    dc.w     $FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1
                    dc.w     $FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1
                    dc.w     $FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1
                    dc.w     $FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1
                    dc.w     $FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1
                    dc.w     $FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1
                    dc.w     $FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1
                    dc.w     $FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1
                    dc.w     $FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1,$FFFF,0,7,0,0,1
                    dc.w     $FFFF,0,7,0,0,$FFFF
E_GRAVE:            dc.w     1,$40,0,$360,0,0,$40,$FFFF,0,$FFFF
E_BURY:             dc.w     $40,1,$FFFD,1,$FFC0,0,$46,0,0,$FFFF
E_SPLASH:           dc.w     $20,2,0,$28,0,$FFFC,$40,$FFFF,0,$FFFF
E_SUB:              dc.w     1,$18,0,10,0,$FFFE,$190,0,0,$18,$FFFF,0,$FFFF
E_FEET:             dc.w     10,1,$FFFB,1,$FFF6,0,$FFFF
E_PICK:             dc.w     1,$37,0,1,0,0,5,$FFF5,0,$28,0,0,1,$37,0,1,0,0,5
                    dc.w     $FFF5,0,$28,0,0,1,$37,0,1,0,0,5,$FFF5,0,$28,0,0,1
                    dc.w     $37,0,1,0,0,5,$FFF5,0,$28,0,0,1,$37,0,1,0,0,5
                    dc.w     $FFF5,0,$28,0,0,1,$37,0,1,0,0,5,$FFF5,0,$28,0,0,1
                    dc.w     $37,0,1,0,0,5,$FFF5,0,$28,0,0,1,$37,0,1,0,0,5
                    dc.w     $FFF5,0,$28,0,0,1,$37,0,1,0,0,5,$FFF5,0,$28,0,0,1
                    dc.w     $37,0,1,0,0,5,$FFF5,0,$28,0,0,1,$37,0,1,0,0,5
                    dc.w     $FFF5,0,$28,0,0,$FFFF
E_SAW:              dc.w     1,$1E,0,$14,0,0,1,$FFE2,0,$14,0,0,1,$1E,$3E8,$14
                    dc.w     0,0,1,$FFE2,$FC18,$14,0,0,1,$1E,0,$14,0,0,1,$FFE2
                    dc.w     0,$14,0,0,1,$1E,$3E8,$14,0,0,1,$FFE2,$FC18,$14,0
                    dc.w     0,1,$1E,0,$14,0,0,1,$FFE2,0,$14,0,0,1,$1E,$3E8
                    dc.w     $14,0,0,1,$FFE2,$FC18,$14,0,0,1,$1E,0,$14,0,0,1
                    dc.w     $FFE2,0,$14,0,0,1,$1E,$3E8,$14,0,0,1,$FFE2,$FC18
                    dc.w     $14,0,0,1,$1E,0,$14,0,0,1,$FFE2,0,$14,0,0,1,$1E
                    dc.w     $3E8,$14,0,0,1,$FFE2,$FC18,$14,0,0,1,$1E,0,$14,0
                    dc.w     0,1,$FFE2,0,$14,0,0,1,$1E,$3E8,$14,0,0,1,$FFE2
                    dc.w     $FC18,$14,0,0,$FFFF
E_KISS:             dc.w     1,15,0,$14,0,$FFF3,1,$FFF1,0,$FFFF
E_RUMBLE:           dc.w     1,$40,0,$32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0
                    dc.w     0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1
                    dc.w     $32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1
                    dc.w     $FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0
                    dc.w     0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1
                    dc.w     $32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1
                    dc.w     $FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0
                    dc.w     0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1
                    dc.w     $32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1
                    dc.w     $FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0
                    dc.w     0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1
                    dc.w     $32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1
                    dc.w     $FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0
                    dc.w     0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1
                    dc.w     $32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1
                    dc.w     $FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0
                    dc.w     0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1
                    dc.w     $32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1
                    dc.w     $FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0
                    dc.w     0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1
                    dc.w     $32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1
                    dc.w     $FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0
                    dc.w     0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1
                    dc.w     $32,0,0,1,$FFFF,1,$32,0,0,1,$FFFF,1,$32,0,0,1
                    dc.w     $FFFF,1,$FFFF

TEST:               move.l   4,a6
                    jsr      -132(a6)
                    bsr      START_SOUNDS
                    move.w   #$10,d5
lbC00134A:          move.w   #$13,d7
                    bsr      NEW_SOUND
                    move.w   #1,MUSIC_SWITCH
lbC00135A:          btst     #6,$BFE001
                    bne.s    lbC00135A
lbC001364:          btst     #6,$BFE001
                    beq.s    lbC001364
                    dbra     d5,lbC00134A
                    move.l   SAVEINT6,$78
                    move.w   #15,$DFF096
                    move.l   4,a6
                    jsr      -138(a6)
                    rts

START_SOUNDS:       move.w   #1,MASTER_ENABLE
                    clr.w    MUSIC_POINTER
                    clr.w    MUSIC_DELAY
                    lea      RANDOM_AREA,a0
                    move.w   #$1FFF,d0
lbC0013AE:          move.l   RND1,d1
                    add.l    RND2,d1
                    add.w    $DFF006,d1
                    ror.l    #1,d1
                    move.l   d1,RND1
                    sub.l    RND3,d1
                    rol.l    #1,d1
                    add.w    d1,RND2
                    and.w    #$FF,d1
                    move.b   d1,(a0)+
                    dbra     d0,lbC0013AE
                    clr.w    MUSIC_SWITCH
                    move.w   #15,$DFF096
                    move.w   #$FF,$DFF09E
                    lea      $DFF0A0,a5
                    lea      MUSINF_0,a6
                    clr.w    0(a6)
                    clr.w    6(a6)
                    move.w   #$20,4(a5)
                    move.w   #$8001,8(a6)
                    move.l   #ENVELOPE1,10(a6)
                    move.l   10(a6),$10(a6)
                    move.l   #DUMMY,10(a6)
                    clr.w    4(a6)
                    move.w   #$FFFF,14(a6)
                    lea      $DFF0B0,a5
                    lea      MUSINF_1,a6
                    clr.w    0(a6)
                    clr.w    6(a6)
                    move.w   #$20,4(a5)
                    move.w   #$8002,8(a6)
                    move.l   #ENVELOPE2,10(a6)
                    move.l   10(a6),$10(a6)
                    move.l   #DUMMY,10(a6)
                    clr.w    4(a6)
                    move.w   #$FFFF,14(a6)
                    lea      $DFF0C0,a5
                    lea      MUSINF_2,a6
                    clr.w    0(a6)
                    clr.w    6(a6)
                    move.w   #$20,4(a5)
                    move.w   #$8004,8(a6)
                    move.l   #ENVELOPE3,10(a6)
                    move.l   10(a6),$10(a6)
                    move.l   #DUMMY,10(a6)
                    clr.w    4(a6)
                    move.w   #$FFFF,14(a6)
                    lea      $DFF0D0,a5
                    lea      MUSINF_3,a6
                    clr.w    0(a6)
                    move.w   #$20,4(a5)
                    clr.w    6(a6)
                    move.w   #$8008,8(a6)
                    move.l   #ENVELOPE1,10(a6)
                    move.l   10(a6),$10(a6)
                    move.l   #DUMMY,10(a6)
                    clr.w    4(a6)
                    move.w   #0,14(a6)
                    bsr      INITMUSC
                    move.l   $78,SAVEINT6
                    move.l   #MYINT6,$78
                    lea      $BFD000,a0
                    move.b   #0,$600(a0)
                    move.b   #14,$700(a0)
                    move.b   #$11,$F00(a0)
                    move.b   #$82,$D00(a0)
                    move.w   #$A000,$DFF09A
                    clr.b    lbB001F19
                    rts

GO_SOUND:           move.l   a1,(a5)
                    move.w   #$FF,$DFF09E
                    move.w   d0,4(a5)
                    move.w   d1,$14(a6)
                    move.w   d1,6(a5)
                    clr.w    6(a6)
                    clr.w    0(a6)
                    clr.w    4(a6)
                    move.w   #1,14(a6)
                    move.l   a0,10(a6)
                    rts

HIGH_PING:          move.w   #8,d0
                    move.w   #$DC,d1
                    lea      E_HIGH_PING,a0
                    lea      WAVEFORM16,a1
                    bsr.s    GO_SOUND
                    rts

MEDIUM_PING:        move.w   #8,d0
                    move.w   #$190,d1
                    lea      E_MEDIUM_PING,a0
                    lea      WAVEFORM16,a1
                    bsr.s    GO_SOUND
                    rts

LOW_PING:           move.w   #4,d0
                    move.w   #$708,d1
                    lea      E_LOW_PING,a0
                    lea      WAVEFORM8,a1
                    bsr.s    GO_SOUND
                    rts

DROP_PING:          move.w   #4,d0
                    move.w   #$898,d1
                    lea      E_LOW_PING,a0
                    lea      WAVEFORM8,a1
                    bsr      GO_SOUND
                    rts

TAKE_PING:          move.w   #4,d0
                    move.w   #$9C4,d1
                    lea      E_LOW_PING,a0
                    lea      WAVEFORM8,a1
                    bsr      GO_SOUND
                    rts

MEDIUM_BEEP:        move.w   #4,d0
                    move.w   #$258,d1
                    lea      E_MEDIUM_BEEP,a0
                    lea      WAVEFORM8,a1
                    bsr      GO_SOUND
                    rts

BOP:                move.w   #$1000,d0
                    move.w   #$FA0,d1
                    lea      E_BANG,a0
                    lea      RANDOM_AREA,a1
                    bsr      GO_SOUND
                    rts

BANG:               move.w   #$1000,d0
                    move.w   #$2BC,d1
                    lea      E_BANG,a0
                    lea      RANDOM_AREA,a1
                    bsr      GO_SOUND
                    rts

OTHER_SMACK:        move.w   #$1000,d0
                    move.w   #$1F4,d1
                    lea      E_BANG,a0
                    lea      RANDOM_AREA,a1
                    bsr      GO_SOUND
                    rts

GRAVESOUND:         move.w   #$12B,d0
                    move.w   #$1644,d1
                    lea      E_GRAVE,a0
                    lea      RANDOM_AREA,a1
                    bsr      GO_SOUND
                    rts

BOOM:               move.w   #$1000,d0
                    move.w   #$1194,d1
                    lea      E_BOOM,a0
                    lea      RANDOM_AREA,a1
                    bsr      GO_SOUND
                    rts

BURY:               move.w   #$1000,d0
                    move.w   #$5DC,d1
                    lea      E_BURY,a0
                    lea      RANDOM_AREA,a1
                    bsr      GO_SOUND
                    rts

SPLASH:             move.w   #$1000,d0
                    move.w   #$3E8,d1
                    lea      E_SPLASH,a0
                    lea      RANDOM_AREA,a1
                    bsr      GO_SOUND
                    rts

RUMBLE:             move.w   #$1000,d0
                    move.w   #$11F8,d1
                    lea      E_RUMBLE,a0
                    lea      RANDOM_AREA,a1
                    bsr      GO_SOUND
                    rts

FEET:               move.w   #$1000,d0
                    move.w   #$5DC,d1
                    lea      E_FEET,a0
                    lea      RANDOM_AREA,a1
                    bsr      GO_SOUND
                    rts

PICKY:              move.w   #$1000,d0
                    move.w   #$5DC,d1
                    lea      E_PICK,a0
                    lea      RANDOM_AREA,a1
                    bsr      GO_SOUND
                    rts

SAWY:               move.w   #$1000,d0
                    move.w   #$BB8,d1
                    lea      E_SAW,a0
                    lea      RANDOM_AREA,a1
                    bsr      GO_SOUND
                    rts

KISS:               move.w   #$1000,d0
                    move.w   #$84,d1
                    lea      E_KISS,a0
                    lea      RANDOM_AREA,a1
                    bsr      GO_SOUND
                    rts

SUB:                move.w   #4,d0
                    move.w   #$1388,d1
                    lea      E_SUB,a0
                    lea      WAVEFORM16,a1
                    bsr      GO_SOUND
                    rts

HANDLE_SOUNDS:      lea      $DFF0A0,a5
                    lea      MUSINF_0,a6
                    bsr      HANDLE_REQUEST
                    bne      lbC00179E
                    lea      $DFF0B0,a5
                    lea      MUSINF_1,a6
                    bsr      HANDLE_REQUEST
                    bne      lbC00179E
                    lea      $DFF0C0,a5
                    lea      MUSINF_2,a6
                    bsr      HANDLE_REQUEST
                    bne      lbC00179E
                    lea      $DFF0D0,a5
                    lea      MUSINF_3,a6
                    bsr      HANDLE_REQUEST
lbC00179E:          rts

HANDLE_REQUEST:     lea      REQUESTS,a0
                    tst.w    14(a6)
                    bne      lbC0017F6
                    tst.w    (a0)+
                    bmi      lbC0017B8
                    bne      lbC0017D6
lbC0017B8:          tst.w    (a0)+
                    bmi      lbC0017C2
                    bne      lbC0017D6
lbC0017C2:          tst.w    (a0)+
                    bmi      lbC0017CC
                    bne      lbC0017D6
lbC0017CC:          tst.w    (a0)+
                    bmi      lbC0017FA
                    beq      lbC0017FA
lbC0017D6:          move.w   -(a0),d0
                    and.w    #$FF,d0
                    or.w     #$8000,(a0)
                    and.w    #$9FFF,(a0)
                    move.l   a0,$16(a6)
                    lsl.w    #2,d0
                    lea      SOUND_ROUTINES,a0
                    move.l   0(a0,d0.w),a0
                    jsr      (a0)
lbC0017F6:          moveq    #0,d0
                    rts

lbC0017FA:          moveq    #-1,d0
                    rts

NEW_SOUND:          movem.l  d0-d2/a0,-(sp)
                    move.w   d7,d0
                    move.w   d0,d2
                    and.w    #$3FF,d0
                    btst     #13,d2
                    beq      lbC001828
                    tst.w    BSND_FLAG
                    bne      NEW_RET
                    move.w   #1,BSND_FLAG
                    bra      NO_MATCH

lbC001828:          lea      REQUESTS,a0
                    move.w   (a0)+,d1
                    and.w    #$3FF,d1
                    cmp.w    d1,d0
                    beq      MATCH_SOUND
                    move.w   (a0)+,d1
                    and.w    #$3FF,d1
                    cmp.w    d1,d0
                    beq      MATCH_SOUND
                    move.w   (a0)+,d1
                    and.w    #$3FF,d1
                    cmp.w    d1,d0
                    beq      MATCH_SOUND
                    move.w   (a0)+,d1
                    and.w    #$3FF,d1
                    cmp.w    d1,d0
                    bne      NO_MATCH
MATCH_SOUND:        tst.w    d2
                    bmi      NEW_RET
                    or.w     #$4000,-(a0)
                    bra      NEW_RET

NO_MATCH:           lea      REQUESTS,a0
                    tst.w    (a0)+
                    beq      lbC001896
                    tst.w    (a0)+
                    beq      lbC001896
                    tst.w    (a0)+
                    beq      lbC001896
                    tst.w    (a0)+
                    beq      lbC001896
                    btst     #14,d2
                    bne      lbC001896
                    bra      NEW_RET

lbC001896:          move.w   d0,-(a0)
NEW_RET:            movem.l  (sp)+,d0-d2/a0
                    rts

REQUESTS:           dcb.l    2,0
SOUND_ROUTINES:     dc.l     HIGH_PING
                    dc.l     HIGH_PING
                    dc.l     BANG
                    dc.l     BURY
                    dc.l     BOOM
                    dc.l     SPLASH
                    dc.l     MEDIUM_PING
                    dc.l     LOW_PING
                    dc.l     MEDIUM_BEEP
                    dc.l     RUMBLE
                    dc.l     SUB
                    dc.l     FEET
                    dc.l     GRAVESOUND
                    dc.l     KISS
                    dc.l     TAKE_PING
                    dc.l     DROP_PING
                    dc.l     OTHER_SMACK
                    dc.l     PICKY
                    dc.l     SAWY
                    dc.l     BOP

MYINT6:             tst.b    $BFDD00
                    bsr      HANDLE_MOUSE
                    tst.w    MASTER_ENABLE
                    beq      MYINT6_RET
                    movem.l  d0-d7/a0-a6,-(sp)
                    tst.w    MUSIC_DELAY
                    beq      lbC001922
                    subq.w   #1,MUSIC_DELAY
                    bra      ENVS

lbC001922:          move.w   #3,MUSIC_DELAY
                    tst.w    MUSIC_SWITCH
                    beq      ENVS
                    bsr      PLAYMUS
                    bsr      X8912
ENVS:               lea      $DFF0A0,a5
                    lea      MUSINF_0,a6
                    tst.w    14(a6)
                    beq      lbC001954
                    bsr      CHANGE_ENVELOPE
lbC001954:          lea      $DFF0B0,a5
                    lea      MUSINF_1,a6
                    tst.w    14(a6)
                    beq      lbC00196C
                    bsr      CHANGE_ENVELOPE
lbC00196C:          lea      $DFF0C0,a5
                    lea      MUSINF_2,a6
                    tst.w    14(a6)
                    beq      lbC001984
                    bsr      CHANGE_ENVELOPE
lbC001984:          lea      $DFF0D0,a5
                    lea      MUSINF_3,a6
                    tst.w    14(a6)
                    beq      lbC00199C
                    bsr      CHANGE_ENVELOPE
lbC00199C:          bsr      HANDLE_SOUNDS
                    movem.l  (sp)+,d0-d7/a0-a6
MYINT6_RET:         move.w   #$2000,$DFF09C
                    rte

CHANGE_ENVELOPE:    tst.w    0(a6)
                    bne      SAME_STEP
                    or.w     #$8000,8(a6)
                    tst.w    14(a6)
                    bmi      lbC0019C8
                    clr.w    14(a6)
lbC0019C8:          move.l   10(a6),a0
CE_1:               move.w   (a0),d0
                    bmi      END_OF_ENVY
                    or.w     #1,14(a6)
                    addq.w   #2,a0
                    move.w   d0,0(a6)
                    move.w   (a0)+,2(a6)
                    move.w   (a0)+,4(a6)
                    move.l   a0,10(a6)
SAME_STEP:          tst.w    0(a6)
                    beq      DO_VOLUME
                    subq.w   #1,0(a6)
                    move.w   6(a6),d0
                    add.w    2(a6),d0
                    move.w   d0,6(a6)
                    move.w   $14(a6),d0
                    add.w    4(a6),d0
                    move.w   d0,$14(a6)
                    bra      DO_VOLUME

END_OF_ENVY:        cmp.w    #$FFFE,d0
                    bne      lbC001A3A
                    move.l   $16(a6),a1
                    move.w   (a1),d0
                    btst     #14,d0
                    beq      lbC001A36
                    bclr     #14,d0
                    move.w   d0,(a1)
                    move.w   2(a0),d0
                    sub.w    d0,a0
                    bra.s    CE_1

lbC001A36:          addq.w   #4,a0
                    bra.s    CE_1

lbC001A3A:          tst.w    6(a6)
                    bne      lbC001A48
                    and.w    #$7FFF,8(a6)
lbC001A48:          move.l   $16(a6),a0
                    move.w   (a0),d0
                    and.w    #$4000,d0
                    bne      ALLOW_AGAIN
                    clr.w    (a0)
                    bra      DO_VOLUME

ALLOW_AGAIN:        and.w    #$3FF,(a0)
DO_VOLUME:          move.w   $14(a6),6(a5)
                    move.w   6(a6),8(a5)
                    move.w   8(a6),$DFF096
                    rts

X8912:              lea      REGS,a4
                    lea      $DFF0A0,a5
                    lea      MUSINF_0,a6
                    move.b   7(a4),d0
                    move.w   d0,d1
                    and.w    #9,d1
                    cmp.w    #9,d1
                    beq      XDONE1
                    and.w    #8,d1
                    beq      XNOISE1
                    moveq    #0,d2
                    moveq    #0,d3
                    move.b   0(a4),d2
                    move.b   1(a4),d3
                    lsl.w    #1,d2
                    lea      WAVE_LENGTH,a0
                    move.w   0(a0,d2.w),4(a5)
                    lsl.w    #1,d2
                    lea      WHICH_WAVE,a0
                    move.l   0(a0,d2.w),(a5)
                    lsl.w    #1,d3
                    lea      FREQS,a0
ISITENV1:           cmp.b    #$10,8(a4)
                    beq      DO_ENVEL1
                    move.w   0(a0,d3.w),6(a5)
                    move.w   0(a0,d3.w),$14(a6)
                    clr.w    4(a6)
                    move.b   8(a4),d0
                    and.w    #15,d0
                    lsl.w    #1,d0
                    move.w   d0,6(a6)
                    bra      XDONE1

DO_ENVEL1:          tst.b    13(a4)
                    beq      XDONE1
                    move.w   0(a0,d3.w),6(a5)
                    move.w   0(a0,d3.w),$14(a6)
                    clr.w    4(a6)
                    move.l   $10(a6),10(a6)
                    clr.w    6(a6)
                    clr.w    0(a6)
                    bra      XDONE1

XNOISE1:            lea      $DFF0C0,a5
                    lea      MUSINF_2,a6
                    move.l   #RANDOM_AREA,(a5)
                    move.w   #$1000,4(a5)
                    moveq    #0,d0
                    move.b   6(a4),d0
                    and.w    #$1F,d0
                    lsl.w    #7,d0
                    move.w   d0,TEMP_MUSIC
                    lea      TEMP_MUSIC,a0
                    clr.w    d3
                    bra      ISITENV1

XDONE1:             lea      $DFF0B0,a5
                    lea      MUSINF_1,a6
                    move.b   7(a4),d0
                    move.w   d0,d1
                    and.w    #$12,d1
                    cmp.w    #$12,d1
                    beq      XDONE2
                    and.w    #10,d1
                    bne      XNOISE2
                    moveq    #0,d2
                    moveq    #0,d3
                    move.b   2(a4),d2
                    move.b   3(a4),d3
                    lsl.w    #1,d2
                    lea      WAVE_LENGTH,a0
                    move.w   0(a0,d2.w),4(a5)
                    lsl.w    #1,d2
                    lea      WHICH_WAVE,a0
                    move.l   0(a0,d2.w),(a5)
                    lsl.w    #1,d3
                    lea      FREQS,a0
ISITENV2:           cmp.b    #$10,9(a4)
                    beq      DO_ENVEL2
                    move.w   0(a0,d3.w),6(a5)
                    move.w   0(a0,d3.w),$14(a6)
                    clr.w    4(a6)
                    move.b   9(a4),d0
                    and.w    #15,d0
                    lsl.w    #1,d0
                    move.w   d0,6(a6)
                    bra      XDONE2

DO_ENVEL2:          tst.b    13(a4)
                    beq      XDONE2
                    move.w   0(a0,d3.w),6(a5)
                    move.w   0(a0,d3.w),$14(a6)
                    clr.w    4(a6)
                    move.l   $10(a6),10(a6)
                    clr.w    6(a6)
                    clr.w    0(a6)
                    bra      XDONE2

XNOISE2:            lea      $DFF0C0,a5
                    lea      MUSINF_2,a6
                    move.l   #RANDOM_AREA,(a5)
                    move.w   #$1000,4(a5)
                    moveq    #0,d0
                    move.b   6(a4),d0
                    and.w    #$1F,d0
                    lsl.w    #7,d0
                    move.w   d0,TEMP_MUSIC
                    lea      TEMP_MUSIC,a0
                    clr.w    d3
                    bra      ISITENV2

XDONE2:             clr.b    13(a4)
                    rts

PSGW:               move.l   a0,-(sp)
                    lea      REGS,a0
                    move.b   d1,0(a0,d0.w)
                    move.l   (sp)+,a0
                    rts

INITMUSC:           move.w   #1,MUSICON
                    move.w   #7,d0
                    move.w   #$FC,d1
                    bsr.s    PSGW
                    move.w   #8,d0
                    move.w   #0,d1
                    bsr.s    PSGW
                    move.w   #9,d0
                    move.w   #0,d1
                    bsr.s    PSGW
                    move.l   #NOTES1,POINTERA
                    move.l   #NOTES2,POINTERB
                    clr.w    DELAYA
                    clr.w    DELAYB
                    clr.w    FLAGA
                    clr.w    FLAGB
                    rts

PLAYMUS:            tst.w    MUSICON
                    beq      lbC001CF2
                    move.l   POINTERA,a4
                    move.w   DELAYA,d7
                    move.w   #0,d5
                    bsr      PLAY_CHANNEL
                    move.w   d7,DELAYA
                    move.l   a4,POINTERA
                    move.l   POINTERB,a4
                    move.w   DELAYB,d7
                    move.w   #$FFFF,d5
                    bsr      PLAY_CHANNEL
                    move.w   d7,DELAYB
                    move.l   a4,POINTERB
                    rts

lbC001CF2:          move.w   #8,d0
                    move.w   #0,d1
                    bsr      PSGW
                    move.w   #9,d0
                    move.w   #0,d1
                    bsr      PSGW
                    rts

PLAY_CHANNEL:       tst.w    d7
                    beq      PLAY2
                    subq.w   #1,d7
                    tst.w    d5
                    bne      lbC001D36
                    tst.w    FLAGA
                    beq      lbC001D52
                    cmp.w    #9,d7
                    bgt      lbC001D52
                    move.w   d7,d1
                    move.w   #8,d0
                    bsr      PSGW
lbC001D36:          tst.w    FLAGB
                    beq      lbC001D52
                    cmp.w    #10,d7
                    bgt      lbC001D52
                    move.w   d7,d1
                    move.w   #9,d0
                    bsr      PSGW
lbC001D52:          rts

PLAY2:              tst.w    (a4)
                    bpl      lbC001D5E
                    move.l   2(a4),a4
lbC001D5E:          move.w   (a4)+,d2
                    move.w   (a4)+,d3
                    move.w   (a4)+,d7
                    subq.w   #1,d7
                    tst.w    d3
                    bmi      lbC001DCC
                    tst.w    d5
                    bne      lbC001DA0
                    subq.w   #1,d2
                    move.w   #$FFFF,FLAGA
                    move.w   #8,d0
                    move.w   #9,d1
                    bsr      PSGW
                    move.w   #0,d0
                    move.w   d2,d1
                    bsr      PSGW
                    move.w   #1,d0
                    move.w   d3,d1
                    bsr      PSGW
                    bra      lbC001DF6

lbC001DA0:          move.w   #$FFFF,FLAGB
                    move.w   #9,d0
                    move.w   #10,d1
                    bsr      PSGW
                    move.w   #2,d0
                    move.w   d2,d1
                    bsr      PSGW
                    move.w   #3,d0
                    move.w   d3,d1
                    bsr      PSGW
                    bra      lbC001DF6

lbC001DCC:          tst.w    d5
                    bne      lbC001DE4
                    move.w   #8,d0
                    move.w   #0,d1
                    bsr      PSGW
                    clr.w    FLAGA
lbC001DE4:          move.w   #9,d0
                    move.w   #0,d1
                    bsr      PSGW
                    clr.w    FLAGB
lbC001DF6:          rts

NOTES1:             dc.w     0,$FFFF,$140
NOTES1A:            dc.w     0,$FFFF,$280,4,7,$A0,4,6,$A0,4,2,$A0,4,0,$A0,3,7
                    dc.w     $A0,3,6,$A0,3,2,$A0,3,0,$A0,2,7,$A0,2,6,$A0,2,2
                    dc.w     $A0,2,0,$A0,$FFFF
                    dc.l     NOTES1A
NOTES2:             dc.w     1,2,10,1,2,10,1,2,$14,0,$FFFF,$28,0,$FFFF,$50,1,2
                    dc.w     10,1,2,10,1,2,$14,0,$FFFF,$28,0,$FFFF,$50
NOTES2A:            dc.w     1,2,10,1,2,10,2,9,10,1,2,10,2,0,10,1,2,10,2,9,10
                    dc.w     2,7,$14,1,2,10,2,6,10,1,2,10,2,4,10,1,2,10,2,0,10
                    dc.w     1,9,10,$FFFF
                    dc.l     NOTES2A
MUSICON:            dc.w     0
DELAYA:             dc.w     0
DELAYB:             dc.w     0
POINTERA:           dc.l     0
POINTERB:           dc.l     0
FLAGA:              dc.w     0
FLAGB:              dc.w     0
WARBLEA:            dc.w     0
FREQA:              dc.w     0
MUSIC_SWITCH:       dc.w     0
SAVE_INT2:          dc.l     0
TEMP_MUSIC:         dc.w     0
REGS:               dcb.b    13,0
lbB001F19:          dc.b     0
REG7:               dcb.b    2,0
SAVEINT6:           dc.l     0
MUSINF_0:           dcb.b    14,0
lbW001F2E:          dcb.w    6,0
MUSINF_1:           dcb.b    14,0
lbW001F48:          dcb.w    6,0
MUSINF_2:           dcb.b    14,0
lbW001F62:          dcb.w    6,0
MUSINF_3:           dcb.b    $1A,0
MUSIC_POINTER:      dc.w     0
MUSIC_DELAY:        dc.w     0
FRED:               dc.w     0
MASTER_ENABLE:      dc.w     0
BSND_FLAG:          dc.w     0

START:              jsr      ALLOCATE_MEMORY
                    jsr      INIT_KEY
                    jsr      START_SOUNDS
                    move.l   $68,SAVE_INT2
                    move.w   #1,MUSIC_SWITCH
                    jsr      SETUP_SCREEN
                    move.l   TWOSCREEN,SCREEN2
                    move.l   ONESCREEN,SCREEN1
                    bsr      READALL
                    move.l   #$130000,d0
lbC001FDA:          subq.l   #1,d0
                    bne.s    lbC001FDA
                    move.l   #MYINT3,$6C
                    move.l   #MYINT2,$68
                    jsr      SETUPZ
                    move.w   #2,S1MODE
                    move.w   #0,S2MODE
DEVICE:             move.l   #$1000,sp
                    jsr      GETMODE
                    move.w   d1,S1MODE
                    move.w   d2,S2MODE
PLAY:               move.w   #$23,DANGER
                    bsr      PLAYINIT
                    tst.w    ABORT
                    beq      _DO_GAME
                    clr.w    ABORT
                    bra.s    DEVICE

_DO_GAME:           bsr      DO_GAME
                    bra.s    PLAY

READALL:            move.l   #SPYPIX,a0
                    bsr      READPIX
                    move.l   BACK,a0
                    move.w   #0,d1
                    move.w   #0,d2
                    move.l   #BUF11,a1
                    bsr      GRABBIG
                    move.w   #8,d1
                    move.w   #0,d2
                    move.l   #BUF12,a1
                    bsr      GRABBIG
                    move.w   #0,d1
                    move.w   #$2A,d2
                    move.l   #BUF13,a1
                    bsr      GRABBIG
                    move.w   #8,d1
                    move.w   #$2A,d2
                    move.l   #BUF14,a1
                    bsr      GRABBIG
                    move.w   #$10,d1
                    move.w   #$53,d2
                    move.l   #BUF16,a1
                    move.w   #2,d6
                    move.w   #$1E,d7
                    jsr      RSAVE_BUFF
                    move.w   #0,d1
                    move.w   #$53,d2
                    move.l   #BUF21,a1
                    bsr      GRABBIG
                    move.w   #8,d1
                    move.w   #$53,d2
                    move.l   #BUF22,a1
                    bsr      GRABBIG
                    move.w   #0,d1
                    move.w   #$7C,d2
                    move.l   #BUF23,a1
                    bsr      GRABBIG
                    move.w   #8,d1
                    move.w   #$7C,d2
                    move.l   #BUF24,a1
                    bsr      GRABBIG
                    move.w   #$10,d1
                    move.w   #$7C,d2
                    move.l   #BUF26,a1
                    move.w   #2,d6
                    move.w   #$1E,d7
                    jsr      RSAVE_BUFF
                    move.w   #$1E,d7
                    move.w   #2,d6
                    move.w   #$53,d2
                    move.w   #$12,d1
                    lea      BUFWL1,a1
                    jsr      RSAVE_BUFF
                    lea      BUFWL2,a1
                    move.w   #$A8,d2
                    move.w   #4,d1
                    bsr      RSAVE_BUFF
                    move.w   #6,d1
                    lea      BUFWL3,a1
                    bsr      RSAVE_BUFF
                    move.w   #$1E,d7
                    move.w   #2,d6
                    move.w   #$53,d2
                    move.w   #$10,d1
                    lea      BUFWR1,a1
                    jsr      RSAVE_BUFF
                    lea      BUFWR2,a1
                    move.w   #$A8,d2
                    move.w   #0,d1
                    bsr      RSAVE_BUFF
                    move.w   #2,d1
                    lea      BUFWR3,a1
                    bsr      RSAVE_BUFF
                    move.w   #$1E,d7
                    move.w   #2,d6
                    move.w   #$7C,d2
                    move.w   #$12,d1
                    lea      BUFBL1,a1
                    jsr      RSAVE_BUFF
                    lea      BUFBL2,a1
                    move.w   #$A8,d2
                    move.w   #12,d1
                    bsr      RSAVE_BUFF
                    move.w   #14,d1
                    lea      BUFBL3,a1
                    bsr      RSAVE_BUFF
                    move.w   #$1E,d7
                    move.w   #2,d6
                    move.w   #$7C,d2
                    move.w   #$10,d1
                    lea      BUFBR1,a1
                    jsr      RSAVE_BUFF
                    lea      BUFBR2,a1
                    move.w   #$A8,d2
                    move.w   #8,d1
                    bsr      RSAVE_BUFF
                    move.w   #10,d1
                    lea      BUFBR3,a1
                    bsr      RSAVE_BUFF
                    move.l   #SPYPIX2,a0
                    bsr      READPIX
                    move.l   BACK,a0
                    move.w   #$33,d2
                    move.w   #4,d1
                    lea      WLAUGH1,a1
                    bsr      RSAVE_BUFF
                    move.w   #6,d1
                    lea      WLAUGH2,a1
                    bsr      RSAVE_BUFF
                    move.w   #$81,d2
                    move.w   #4,d1
                    lea      BLAUGH1,a1
                    bsr      RSAVE_BUFF
                    move.w   #6,d1
                    lea      BLAUGH2,a1
                    bsr      RSAVE_BUFF
                    move.w   #0,d1
                    move.w   #0,d2
                    lea      WSPYSAT,a1
                    move.w   #$1E,d7
                    move.w   #2,d6
                    bsr      RSAVE_BUFF
                    move.w   #2,d1
                    lea      BSPYSAT,a1
                    bsr      RSAVE_BUFF
                    move.w   #$29,d7
                    move.w   #0,d2
                    lea      WPOURR1,a1
                    move.w   #6,d1
                    bsr      RSAVE_BUFF
                    lea      WPOURR2,a1
                    move.w   #4,d1
                    bsr      RSAVE_BUFF
                    move.w   #8,d1
                    lea      WPOURL1,a1
                    bsr      RSAVE_BUFF
                    move.w   #10,d1
                    lea      WPOURL2,a1
                    bsr      RSAVE_BUFF
                    move.w   #$55,d2
                    lea      BPOURR1,a1
                    move.w   #2,d1
                    bsr      RSAVE_BUFF
                    lea      BPOURR2,a1
                    move.w   #0,d1
                    bsr      RSAVE_BUFF
                    move.w   #4,d1
                    lea      BPOURL1,a1
                    bsr      RSAVE_BUFF
                    move.w   #6,d1
                    lea      BPOURL2,a1
                    bsr      RSAVE_BUFF
                    move.w   #8,d1
                    move.w   #$29,d7
                    move.w   #$55,d2
                    lea      WSAWR1,a1
                    bsr      RSAVE_BUFF
                    lea      WSAWR2,a1
                    move.w   #10,d1
                    bsr      RSAVE_BUFF
                    move.w   #12,d1
                    lea      WSAWL1,a1
                    bsr      RSAVE_BUFF
                    move.w   #14,d1
                    lea      WSAWL2,a1
                    bsr      RSAVE_BUFF
                    move.w   #$9E,d2
                    lea      BSAWR1,a1
                    move.w   #0,d1
                    bsr      RSAVE_BUFF
                    lea      BSAWR2,a1
                    move.w   #2,d1
                    bsr      RSAVE_BUFF
                    move.w   #4,d1
                    lea      BSAWL1,a1
                    bsr      RSAVE_BUFF
                    move.w   #6,d1
                    lea      BSAWL2,a1
                    bsr      RSAVE_BUFF
                    move.w   #8,d1
                    move.w   #$29,d7
                    move.w   #$9F,d2
                    lea      WPICKR1,a1
                    bsr      RSAVE_BUFF
                    lea      WPICKR2,a1
                    move.w   #10,d1
                    bsr      RSAVE_BUFF
                    move.w   #12,d1
                    lea      WPICKL1,a1
                    bsr      RSAVE_BUFF
                    move.w   #14,d1
                    lea      WPICKL2,a1
                    bsr      RSAVE_BUFF
                    move.w   #1,d2
                    lea      BPICKR1,a1
                    move.w   #12,d1
                    bsr      RSAVE_BUFF
                    lea      BPICKR2,a1
                    move.w   #14,d1
                    bsr      RSAVE_BUFF
                    move.w   #$33,d2
                    move.w   #12,d1
                    lea      BPICKL1,a1
                    bsr      RSAVE_BUFF
                    move.w   #14,d1
                    lea      BPICKL2,a1
                    bsr      RSAVE_BUFF
                    move.w   #10,d1
                    move.w   #$80,d2
                    lea      WHOLE1,a1
                    bsr      RSAVE_BUFF
                    move.w   #12,d1
                    lea      HOLE2,a1
                    bsr      RSAVE_BUFF
                    move.w   #$12,d1
                    lea      HOLE3,a1
                    bsr      RSAVE_BUFF
                    move.w   #$10,d1
                    lea      HOLE4,a1
                    bsr      RSAVE_BUFF
                    move.w   #14,d1
                    lea      HOLE5,a1
                    bsr      RSAVE_BUFF
                    move.w   #$10,d1
                    move.w   #$55,d2
                    lea      BHOLE1,a1
                    bsr      RSAVE_BUFF
                    move.w   #0,d1
                    move.w   #4,d2
                    move.l   #BUF17,a1
                    bsr      GRAB
                    move.w   #8,d1
                    move.w   #4,d2
                    move.l   #BUF18,a1
                    bsr      GRAB
                    move.w   #2,d1
                    move.w   #$2F,d2
                    move.l   #BUF19,a1
                    bsr      GRAB1
                    move.w   #10,d1
                    move.w   #$2F,d2
                    move.l   #BUF1A,a1
                    bsr      GRAB1
                    move.w   #0,d1
                    move.w   #$52,d2
                    move.l   #BUF27,a1
                    bsr      GRAB
                    move.w   #8,d1
                    move.w   #$52,d2
                    move.l   #BUF28,a1
                    bsr      GRAB
                    move.w   #2,d1
                    move.w   #$7C,d2
                    move.l   #BUF29,a1
                    bsr      GRAB1
                    move.w   #8,d1
                    move.w   #$7C,d2
                    move.l   #BUF2A,a1
                    bsr      GRAB1
                    move.w   #12,d2
                    move.w   #$10,d1
                    move.w   #2,d6
                    move.w   #$29,d7
                    move.l   #BUF1D,a1
                    jsr      RSAVE_BUFF
                    move.w   #12,d2
                    move.w   #$12,d1
                    move.w   #2,d6
                    move.w   #$29,d7
                    move.l   #lbL019EF8,a1
                    jsr      RSAVE_BUFF
                    move.w   #$33,d2
                    move.w   #$10,d1
                    move.w   #2,d6
                    move.w   #$29,d7
                    move.l   #BUF2D,a1
                    jsr      RSAVE_BUFF
                    move.w   #$33,d2
                    move.w   #$12,d1
                    move.w   #2,d6
                    move.w   #$29,d7
                    move.l   #lbL01D420,a1
                    jsr      RSAVE_BUFF
                    move.l   #GRAVE,a1
                    move.w   #$10,d1
                    move.w   #$9F,d2
                    move.w   #2,d6
                    move.w   #$29,d7
                    jsr      RSAVE_BUFF
                    move.w   #$78,d2
                    move.w   #0,d1
                    move.w   #1,d6
                    move.w   #8,d7
                    move.l   #MAPBOX,a1
                    jsr      RSAVE_BUFF
                    move.w   #$80,d2
                    move.w   #0,d1
                    move.w   #1,d6
                    move.w   #8,d7
                    move.l   #MAPSPOT,a1
                    jsr      RSAVE_BUFF
                    move.w   #2,d1
                    add.l    #$1E0,a1
                    jsr      RSAVE_BUFF
                    move.l   #LANDPIX,a0
                    bsr      READPIX
                    move.l   BACK,a0
                    move.w   #0,d1
                    move.w   #3,d2
                    move.l   #LAND,a1
                    bsr      GRABLAND
                    move.w   #0,d1
                    move.w   #$45,d2
                    bsr      GRABLAND
                    move.w   #0,d1
                    move.w   #$87,d2
                    bsr      GRABLAND
                    movem.l  a0/a1,-(sp)
                    move.l   #LANDPIX3,a0
                    bsr      READPIX
                    movem.l  (sp)+,a0/a1
                    move.w   #0,d1
                    move.w   #$88,d2
                    move.w   #14,d3
                    bsr      GRABLAND2
                    move.w   #2,d6
                    move.w   #$4A,d7
                    move.w   #$7C,d2
                    move.w   #15,d1
                    lea      WROCKET1,a1
                    bsr      RSAVE_BUFF
                    move.w   #$11,d1
                    lea      WROCKET2,a1
                    bsr      RSAVE_BUFF
                    move.w   #$27,d2
                    move.w   #15,d1
                    lea      BROCKET1,a1
                    bsr      RSAVE_BUFF
                    move.w   #$11,d1
                    lea      BROCKET2,a1
                    bsr      RSAVE_BUFF
                    move.w   #2,d6
                    move.w   #$29,d7
                    move.w   #$43,d2
                    move.w   #0,d1
                    lea      WICICLED1,a1
                    bsr      RSAVE_BUFF
                    move.w   #2,d1
                    lea      WICICLED2,a1
                    bsr      RSAVE_BUFF
                    move.w   #4,d1
                    lea      WICICLED3,a1
                    bsr      RSAVE_BUFF
                    move.w   #2,d6
                    move.w   #$29,d7
                    move.w   #$64,d2
                    move.w   #0,d1
                    lea      BICICLED1,a1
                    bsr      RSAVE_BUFF
                    move.w   #2,d1
                    lea      BICICLED2,a1
                    bsr      RSAVE_BUFF
                    move.w   #4,d1
                    lea      BICICLED3,a1
                    bsr      RSAVE_BUFF
                    move.w   #0,d1
                    move.w   #$25,d2
                    lea      WPLUNGER1,a1
                    bsr      RSAVE_BUFF
                    move.w   #2,d1
                    lea      WPLUNGER2,a1
                    bsr      RSAVE_BUFF
                    move.w   #6,d1
                    lea      WPLUNGEL1,a1
                    bsr      RSAVE_BUFF
                    move.w   #4,d1
                    lea      WPLUNGEL2,a1
                    bsr      RSAVE_BUFF
                    move.w   #0,d1
                    move.w   #0,d2
                    lea      BPLUNGER1,a1
                    bsr      RSAVE_BUFF
                    move.w   #2,d1
                    lea      BPLUNGER2,a1
                    bsr      RSAVE_BUFF
                    move.w   #6,d1
                    lea      BPLUNGEL1,a1
                    bsr      RSAVE_BUFF
                    move.w   #4,d1
                    lea      BPLUNGEL2,a1
                    bsr      RSAVE_BUFF
                    move.w   #$24,d2
                    move.w   #8,d1
                    lea      WBLUP1,a1
                    bsr      RSAVE_BUFF
                    move.w   #10,d1
                    lea      WBLUP2,a1
                    bsr      RSAVE_BUFF
                    move.w   #12,d1
                    lea      WBLUP3,a1
                    bsr      RSAVE_BUFF
                    move.w   #$42,d2
                    move.w   #8,d1
                    lea      BBLUP1,a1
                    bsr      RSAVE_BUFF
                    move.w   #10,d1
                    lea      BBLUP2,a1
                    bsr      RSAVE_BUFF
                    move.w   #12,d1
                    lea      BBLUP3,a1
                    bsr      RSAVE_BUFF
                    movem.l  a0/a1,-(sp)
                    move.l   #LANDPIX2,a0
                    bsr      READPIX
                    movem.l  (sp)+,a0/a1
                    move.w   #10,d6
                    move.w   #$20,d7
                    move.w   #0,d1
                    move.w   #0,d2
                    lea      SNOWSLAB,a1
                    jsr      RSAVE_BUFF
                    move.w   #10,d6
                    move.w   #$20,d7
                    move.w   #0,d1
                    move.w   #$21,d2
                    lea      SLEETSLAB,a1
                    jsr      RSAVE_BUFF
                    move.w   #10,d6
                    move.w   #$20,d7
                    move.w   #0,d1
                    move.w   #$43,d2
                    lea      DRYSLAB,a1
                    jsr      RSAVE_BUFF
                    move.w   #1,d6
                    move.w   #$6E,d2
                    move.w   #0,d1
                    move.w   #3,d7
                    lea      G_EMPTY,a1
                    jsr      RSAVE_BUFF
                    lea      G_FULL,a1
                    move.w   #$72,d2
                    jsr      RSAVE_BUFF
                    lea      G_SNOWBALL,a1
                    move.w   #10,d1
                    move.w   #$99,d2
                    move.w   #5,d7
                    bsr      RSAVE_BUFF
                    bsr      READ_G
                    move.l   #MAPPIX,a0
                    bsr      READPIX
                    move.l   BACK,a0
                    move.w   #0,d1
                    move.w   #0,d2
                    move.l   #MAPS,a1
                    bsr      GRABMAP
                    add.w    #7,d1
                    move.w   #0,d2
                    bsr      GRABMAP
                    move.w   #3,d6
                    move.w   #$15,d7
                    move.w   #1,d2
                    move.w   #13,d1
                    lea      GO_H,a1
                    bsr      RSAVE_BUFF
                    move.w   #$10,d1
                    lea      GO_D,a1
                    bsr      RSAVE_BUFF
                    move.w   #7,d7
                    move.w   #13,d1
                    move.w   #$16,d2
                    lea      SHOW_H,a1
                    bsr      RSAVE_BUFF
                    move.w   #$10,d1
                    lea      SHOW_D,a1
                    bsr      RSAVE_BUFF
                    move.w   #13,d1
                    move.w   #$1E,d2
                    lea      HIDE_H,a1
                    bsr      RSAVE_BUFF
                    move.w   #$10,d1
                    lea      HIDE_D,a1
                    bsr      RSAVE_BUFF
                    move.w   #2,d6
                    move.w   #$1C,d7
                    move.w   #13,d1
                    move.w   #$2A,d2
                    lea      MISSILE_H,a1
                    bsr      RSAVE_BUFF
                    move.w   #15,d1
                    lea      MISSILE_D,a1
                    bsr      RSAVE_BUFF
                    move.w   #$1D,d7
                    move.w   #13,d1
                    move.w   #$6A,d2
                    lea      WSPY_D,a1
                    bsr      RSAVE_BUFF
                    move.w   #15,d1
                    lea      BSPY_D,a1
                    bsr      RSAVE_BUFF
                    move.w   #13,d1
                    move.w   #$8C,d2
                    lea      WSPY_H,a1
                    bsr      RSAVE_BUFF
                    move.w   #15,d1
                    lea      BSPY_H,a1
                    bsr      RSAVE_BUFF
                    move.w   #$12,d1
                    move.w   #$6B,d2
                    move.w   #$1E,d7
                    move.w   #2,d6
                    lea      COMP_H,a1
                    bsr      RSAVE_BUFF
                    move.w   #$8A,d2
                    lea      COMP_D,a1
                    bsr      RSAVE_BUFF
                    lea      NUMS,a1
                    move.w   #0,d1
                    move.w   #$AF,d2
                    move.w   #1,d6
                    move.w   #6,d7
                    bsr      RSAVE_BUFF
                    add.w    #$30,a1
                    addq.w   #1,d1
                    bsr      RSAVE_BUFF
                    add.w    #$30,a1
                    addq.w   #1,d1
                    bsr      RSAVE_BUFF
                    add.w    #$30,a1
                    addq.w   #1,d1
                    bsr      RSAVE_BUFF
                    add.w    #$30,a1
                    addq.w   #1,d1
                    bsr      RSAVE_BUFF
                    move.l   #BACKPIX,a0
                    bsr      READPIX
                    move.l   BACK,a0
                    move.w   #14,d1
                    move.w   #10,d2
                    move.l   #RTCOVER,a1
                    move.w   #$40,d7
                    move.w   #2,d6
                    bsr      RSAVE_BUFF
                    move.w   #6,d1
                    move.w   #$73,d2
                    move.w   #7,d6
                    move.w   #$37,d7
                    move.l   #CONTROLS,a1
                    bsr      RSAVE_BUFF
                    move.w   #$60,d1
                    move.w   #$73,d2
                    move.w   #9,d6
                    move.w   #$37,d7
                    bsr      RCLEARBLOCK
                    move.w   #3,d1
                    move.w   #9,d2
                    move.w   #1,d6
                    move.w   #$41,d7
                    lea      STRENGTH_FULL,a1
                    bsr      RSAVE_BUFF
                    move.w   #3,d1
                    move.w   #$6E,d2
                    move.w   #1,d6
                    move.w   #$41,d7
                    lea      STRENGTH_EMPTY,a1
                    bsr      RSAVE_BUFF
                    move.w   #4,d6
                    move.w   #11,d7
                    move.w   #1,d1
                    move.w   #$53,d2
                    lea      G_HOLD_BACK,a1
                    bsr      RSAVE_BUFF
                    move.l   #SPY2PICA,a0
                    bsr      READPIX
                    move.l   BACK,a0
                    move.l   #BACKPIX,a0
                    bsr      READPIX
                    move.l   BACK,a0
                    move.w   #$60,d1
                    move.w   #$73,d2
                    move.w   #9,d6
                    move.w   #$35,d7
                    bsr      RCLEARBLOCK
                    rts

MYINT3:             addq.w   #1,X462
                    clr.w    INT_REQ
                    move.w   #$70,$DFF09C
                    rte

GRAB:               move.w   #4,d3
                    subq.w   #1,d3
lbC002AB4:          move.w   #2,d6
                    move.w   #$1E,d7
                    bsr      RSAVE_BUFF
                    add.w    #$1E0,a1
                    add.w    #2,d1
                    dbra     d3,lbC002AB4
                    rts

GRABBIG:            move.w   #4,d3
                    subq.w   #1,d3
lbC002AD4:          move.w   #2,d6
                    move.w   #$29,d7
                    bsr      RSAVE_BUFF
                    add.w    #$290,a1
                    add.w    #2,d1
                    dbra     d3,lbC002AD4
                    rts

GRAB1:              move.w   #2,d6
                    move.w   #$1E,d7
                    bsr      RSAVE_BUFF
                    add.w    #$1E0,a1
                    add.w    #2,d1
                    rts

GRABOBJ:            move.w   #$14,d3
                    subq.w   #1,d3
GRABOBJ1:           move.w   #1,d6
                    move.w   #8,d7
                    bsr      RSAVE_BUFF
                    add.w    #$40,a1
                    add.w    #1,d1
                    dbra     d3,GRABOBJ1
                    rts

GRABLAND:           move.w   #$14,d3
                    subq.w   #1,d3
GRABLAND2:          move.w   #$40,d7
                    move.w   #1,d6
                    bsr      RSTUFF_BUFF_SHORT
                    add.w    #$208,a1
                    add.w    #1,d1
                    dbra     d3,GRABLAND2
                    rts

GRABMAP:            move.w   #3,d3
lbC002B48:          move.w   #6,d6
                    move.w   #$29,d7
                    bsr      RSAVE_BUFF
                    add.w    #$7B0,a1
                    add.w    #$2C,d2
                    dbra     d3,lbC002B48
                    rts

PLAYINIT:           move.w   #2,REFRESH
                    bsr      CLEANTRAIL
                    clr.w    S1FLASH
                    clr.w    S2FLASH
                    bsr      CLEANTRAIL
                    bsr      PARMS
                    bsr      CLEANTRAIL
PLAYINIT0:          bsr      CLEANTRAIL
                    clr.w    S1MENU
                    clr.w    S2MENU
                    clr.w    S1NUDGE
                    clr.w    S2NUDGE
                    clr.w    HATCH_RAND
                    move.w   #0,S1AUTO
                    move.w   #0,S2AUTO
                    cmp.w    #1,PLAYERS
                    bne      lbC002BCC
                    move.w   #1,S2AUTO
lbC002BCC:          move.w   LEVEL,d1
                    mulu     #$F00,d1
                    move.l   #MAP,a1
                    add.l    a1,d1
                    move.l   d1,a0
                    move.w   #$780,d1
PLAYINIT1:          move.w   (a0)+,(a1)+
                    dbra     d1,PLAYINIT1
                    move.w   LEVEL,d1
                    mulu     #$3C0,d1
                    lea      TERRAIN,a0
                    move.l   a0,a1
                    add.w    d1,a0
                    move.w   #$EF,d1
lbC002C02:          move.l   (a0)+,(a1)+
                    dbra     d1,lbC002C02
                    clr.w    SNOW_LINE
                    move.l   #XROCKET,S1CHOICEX
                    move.l   #YROCKET,S1CHOICEY
                    move.l   #XROCKET,S2CHOICEX
                    move.l   #YROCKET,S2CHOICEY
                    move.w   #0,XROCKET
                    move.w   #0,YROCKET
                    move.w   #0,XMIDNOSE
                    move.w   #0,YMIDNOSE
                    move.w   #0,XMIDTAIL
                    move.w   #0,YMIDTAIL
                    move.l   #BUF11,S1FADDR
                    move.l   #BUF21,S2FADDR
                    move.w   #$3F,S1ENERGY
                    move.w   #$3F,S2ENERGY
                    move.w   #$64,d1
                    lea      TRAPLIST,a4
lbC002C94:          clr.l    (a4)+
                    clr.w    (a4)+
                    dbra     d1,lbC002C94
                    clr.l    IN_CASE
                    move.w   #7,d0
                    lea      SNOW_LIST,a0
lbC002CAC:          clr.l    (a0)+
                    clr.l    (a0)+
                    clr.w    (a0)+
                    dbra     d0,lbC002CAC
                    clr.w    S1OFFSETX
                    clr.w    S2OFFSETX
                    clr.w    S1OFFSETY
                    clr.w    S2OFFSETY
                    move.w   #$3C,RY
                    move.w   #0,RHT
                    move.w   #$4A,RLEN
                    clr.w    S1WIN
                    clr.w    S2WIN
                    clr.w    S1ROCKET
                    clr.w    S2ROCKET
                    clr.w    S1SHCT
                    clr.w    S2SHCT
                    clr.w    S1BRAIN
                    clr.w    S2BRAIN
                    clr.w    S1CT
                    clr.w    S2CT
                    move.w   #0,S1DEAD
                    move.w   #0,S2DEAD
                    move.w   #0,S1SWAMP
                    move.w   #0,S2SWAMP
                    move.w   #0,S1DEPTH
                    move.w   #0,S2DEPTH
                    move.w   #0,BUF1X
                    move.w   #$FFFF,BUF1Y
                    move.w   #0,BUF2X
                    move.w   #$FFFF,BUF2Y
                    move.w   #0,TENMINS
                    move.w   #0,ONEMINS
                    move.w   #0,TENSECS
                    move.w   #0,ONESECS
                    clr.w    S1IGLOO
                    clr.w    S2IGLOO
                    clr.w    S1SNSH
                    clr.w    S2SNSH
                    clr.w    S1TNT
                    clr.w    S2TNT
                    clr.w    S1SAW
                    clr.w    S2SAW
                    clr.w    S1BUCKET
                    clr.w    S2BUCKET
                    clr.w    S1PICK
                    clr.w    S2PICK
                    clr.w    S1PLUNGER
                    clr.w    S2PLUNGER
                    move.w   #0,S1HAND
                    move.w   #0,S2HAND
                    bsr      STUFFSPY
                    move.w   #1,d1
                    move.w   #$1E,d2
                    bsr      STIT
                    move.l   #XJAR,a1
                    move.l   #YJAR,a2
                    bsr      SETXY
                    move.w   #1,d1
                    move.w   #$16,d2
                    bsr      STIT
                    move.l   #XCARD,a1
                    move.l   #YCARD,a2
                    bsr      SETXY
                    move.w   #1,d1
                    move.w   #14,d2
                    bsr      STIT
                    move.l   #XCASE,a1
                    move.l   #YCASE,a2
                    bsr      SETXY
                    move.w   #1,d1
                    move.w   #$26,d2
                    bsr      STIT
                    move.l   #XGYRO,a1
                    move.l   #YGYRO,a2
                    bsr      SETXY
                    move.w   LEVEL,d2
                    subq.w   #1,d2
                    lsl.w    #2,d2
                    lea      WHICHTABLE,a4
                    add.w    d2,a4
                    move.l   (a4),a4
                    move.w   (a4)+,XIGLOO1
                    move.w   (a4)+,YIGLOO1
                    move.w   (a4)+,XIGLOO2
                    move.w   (a4)+,YIGLOO2
                    bsr      GETA4
                    move.w   d1,TENMINS
                    bsr      GETA4
                    move.w   d1,ONEMINS
                    bsr      GETA4
                    move.w   d1,S1TNT
                    move.w   d1,S2TNT
                    bsr      GETA4
                    move.w   d1,S1SAW
                    move.w   d1,S2SAW
                    bsr      GETA4
                    move.w   d1,S1PICK
                    move.w   d1,S2PICK
                    bsr      GETA4
                    move.w   d1,S1SNSH
                    move.w   d2,S2SNSH
                    bsr      GETA4
                    move.w   #$56,d2
                    bsr      STIT
                    bsr      GETA4
                    move.w   #$2E,d2
                    bsr      STIT
                    bsr      GETA4
                    move.w   #$46,d2
                    bsr      STIT
                    bsr      GETA4
                    move.w   #$4E,d2
                    bsr      STIT
                    bsr      GETA4
                    move.w   #$36,d2
                    move.w   d1,-(sp)
                    bsr      STIT
                    move.l   #XWBUCK,a1
                    move.l   #YWBUCK,a2
                    bsr      SETXY
                    move.w   #$3E,d2
                    move.w   (sp)+,d1
                    bsr      STIT
                    move.l   #XBBUCK,a1
                    move.l   #YBBUCK,a2
                    bsr      SETXY
                    bsr      GETA4
                    move.w   #$5E,d2
                    move.w   d1,-(sp)
                    bsr      STIT
                    move.l   #XBPLUNG,a1
                    move.l   #YBPLUNG,a2
                    bsr      SETXY
                    move.w   #$66,d2
                    move.w   (sp)+,d1
                    bsr      STIT
                    move.l   #XWPLUNG,a1
                    move.l   #YWPLUNG,a2
                    bsr      SETXY
                    clr.l    HATCHAD
                    clr.w    XROCKET
                    clr.w    YROCKET
                    move.w   #$60,MAXMAPX
                    move.w   #$14,MAXMAPY
                    rts

GETA4:              clr.w    d1
                    move.b   (a4)+,d1
                    rts

WHICHTABLE:         dc.l     WHICH1
                    dc.l     WHICH2
                    dc.l     WHICH3
                    dc.l     WHICH4
                    dc.l     WHICH5
                    dc.l     WHICH6
                    dc.l     WHICH7
WHICH1:             dc.w     $1D,5,$1D,9,6,$901,$101,$600,0,$101
WHICH2:             dc.w     $1E,5,$12,9,8,$A01,$101,$C00,0,$101
WHICH3:             dc.w     $24,5,$11,9,$101,$A01,$100,$1400,2,$101
WHICH4:             dc.w     $1E,5,$30,9,$104,$800,0,$1802,$202,$101
WHICH5:             dc.w     $2E,5,$15,1,$108,$600,0,$1802,$202,$101
WHICH6:             dc.w     7,5,$17,9,$202,$400,0,$1802,$202,$101
WHICH7:             dc.w     5,5,$28,9,$207,$200,0,$1402,$202,$101

READ_G:             move.w   #$4B,d0
                    lea      ITEMS_GRAPHICS,a2
                    lea      ITEMS_X,a3
                    lea      ITEMS_Y,a4
                    lea      ITEMS_HEIGHT,a5
lbC003078:          move.l   (a2)+,a1
                    move.w   (a3)+,d1
                    move.w   (a4)+,d2
                    move.w   (a5)+,d7
                    beq      lbC00308C
                    move.w   #1,d6
                    bsr      RSAVE_BUFF
lbC00308C:          dbra     d0,lbC003078
                    rts

ITEMS_GRAPHICS:     dc.l     G_MOUND
                    dc.l     G_CASE
                    dc.l     G_CARD
                    dc.l     G_URANIUM
                    dc.l     G_GYRO
                    dc.l     G_SAW
                    dc.l     G_WHITE_BUCKET
                    dc.l     G_BLACK_BUCKET
                    dc.l     G_PICKAXE
                    dc.l     G_SNOWSHOES
                    dc.l     G_TNT
                    dc.l     G_BLACK_PLUNGER
                    dc.l     G_WHITE_PLUNGER,0,0,0
                    dc.l     G_ICEHOLE
                    dc.l     G_ICEPATCH,0
                    dc.l     G_EXIT_NORTH1,0,0,0,0,0
                    dc.l     G_IGLOO_LEFT1
                    dc.l     G_IGLOO_LEFT2
                    dc.l     G_IGLOO_LEFT3
                    dc.l     G_IGLOO_LEFT4
                    dc.l     G_IGLOO_LEFT5,0,0
                    dc.l     G_IGLOO_RIGHT1
                    dc.l     G_IGLOO_RIGHT2
                    dc.l     G_IGLOO_RIGHT3
                    dc.l     G_IGLOO_RIGHT4
                    dc.l     G_IGLOO_RIGHT5
                    dc.l     G_POLAR_BEARA1
                    dc.l     G_POLAR_BEARA2
                    dc.l     G_POLAR_BEARA3
                    dc.l     G_POLAR_BEARB1
                    dc.l     G_POLAR_BEARB2
                    dc.l     G_POLAR_BEARB3
                    dc.l     G_POLAR_BEARC1
                    dc.l     G_POLAR_BEARC2
                    dc.l     G_POLAR_BEARC3,0,0,0
                    dc.l     G_EXIT_SOUTH
                    dc.l     G_EXIT_NORTH1
                    dc.l     G_EXIT_NORTH2
                    dc.l     G_EXIT_NORTH3,0
                    dc.l     G_FIREL1
                    dc.l     G_FIRER1
                    dc.l     G_FIREL2
                    dc.l     G_FIRER2
                    dc.l     G_SNOW1
                    dc.l     G_SNOW2
                    dc.l     G_SNOW3
                    dc.l     G_SNOW4
                    dc.l     G_ICE1
                    dc.l     G_ICE2
                    dc.l     G_CASE
                    dc.l     G_CARDR
                    dc.l     G_URANIUM
                    dc.l     G_GYRO
                    dc.l     G_SAWR
                    dc.l     G_WHITE_BUCKET
                    dc.l     G_BLACK_BUCKET
                    dc.l     G_PICKAXER
                    dc.l     G_SNOWSHOES
                    dc.l     G_TNTR
                    dc.l     G_BLACK_PLUNGER
                    dc.l     G_WHITE_PLUNGER
ITEMS_X:            dc.w     $12,12,13,14,15,$10,$11,$12,$13,12,13,14,15,0,0,0
                    dc.w     $10,$11,0,5,0,0,0,0,0,13,14,15,$10,$11,0,0,12,13
                    dc.w     14,15,$10,$11,$12,$13,$11,$12,$13,$11,$12,$13,$11
                    dc.w     $12,$13,$13,5,6,7,0,$10,$11,$12,$13,0,1,2,3,5,6
                    dc.w     12,0,14,15,1,$11,$12,2,12,3,14,15
ITEMS_Y:            dc.w     11,0,0,0,0,0,0,0,0,12,11,11,11,0,0,0,12,11,0,$65
                    dc.w     0,0,0,0,0,$19,$19,$19,$19,$19,0,0,$3E,$3E,$3E,$3E
                    dc.w     $3E,$61,$61,$61,$6D,$6D,$6D,$8A,$8A,$8A,$5C,$5C
                    dc.w     $5C,$A9,$65,$65,$65,0,$B1,$B1,$B1,$B1,$90,$90,$90
                    dc.w     $90,$8F,$8F,0,$64,0,0,$64,0,0,$64,12,$64,11,11
ITEMS_HEIGHT:       dc.w     7,10,10,10,10,10,10,10,11,9,12,10,10,0,0,0,5,9,0
                    dc.w     $1C,0,0,0,0,0,$23,$23,$23,$23,$23,0,0,$23,$23,$23
                    dc.w     $23,$23,11,11,11,$1D,$1D,$1D,$1F,$1F,$1F,0,0,0,3
                    dc.w     $1C,$1C,$1C,0,14,14,14,14,$20,$20,$20,$20,$20,$20
                    dc.w     10,9,10,10,7,10,10,11,9,12,10,10
LANDPIX2:           dc.b     'land2.pi1',0
LANDPIX3:           dc.b     'pica.pi1',0,0
SPY2PICA:           dc.b     'spy2pica.pi1',0,0
INT_REQ:            dc.w     0
INT_FLAG:           dc.w     0
MY_VERY_OWN_STACK:  dcb.w    2,0

DRAW_NEW_SEL:       move.l   SCREEN2,a0
                    move.w   #$40,d1
                    move.w   #10,d2
                    move.w   #$3E,d7
                    move.w   #$13,d6
                    bsr      RCLEARBLOCK
                    move.w   #$6F,d2
                    bsr      RCLEARBLOCK
                    cmp.w    #1,d3
                    bne      lbC00343E
                    move.w   d3,-(sp)
                    move.w   #14,d7
                    move.w   #$5F,d1
                    move.w   #$15,d2
                    move.w   d1,d3
                    move.w   #$3F,d4
                    bsr      DRAW_LINE
                    subq.w   #1,d1
                    move.w   d1,d3
                    bsr      DRAW_LINE
                    move.w   #$C0,d1
                    move.w   d1,d3
                    bsr      DRAW_LINE
                    addq.w   #1,d1
                    move.w   d1,d3
                    bsr      DRAW_LINE
                    move.w   #$5E,d1
                    move.w   #$C1,d3
                    move.w   #$14,d2
                    move.w   d2,d4
                    bsr      DRAW_LINE
                    subq.w   #1,d2
                    move.w   d2,d4
                    bsr      DRAW_LINE
                    move.w   #$3F,d2
                    move.w   d2,d4
                    bsr      DRAW_LINE
                    addq.w   #1,d2
                    move.w   d2,d4
                    bsr      DRAW_LINE
                    move.w   (sp)+,d3
lbC00343E:          move.w   LEVEL,d1
                    subq.w   #1,d1
                    mulu     #$7B0,d1
                    add.l    #MAPS,d1
                    move.l   d1,a1
                    move.w   #$60,d1
                    move.w   #$15,d2
                    move.w   #6,d6
                    move.w   #$29,d7
                    bsr      RSPRITER
                    move.w   #2,d6
                    move.w   #$1D,d7
                    move.w   #$40,d1
                    move.w   #$82,d2
                    lea      WSPY_D,a1
                    cmp.w    #2,d3
                    bne      _RSPRITER
                    lea      WSPY_H,a1
_RSPRITER:          bsr      RSPRITER
                    cmp.w    #2,PLAYERS
                    bne      lbC0034BA
                    move.w   #$60,d1
                    move.w   #$82,d2
                    lea      BSPY_D,a1
                    cmp.w    #2,d3
                    bne      _RSPRITER0
                    lea      BSPY_H,a1
_RSPRITER0:         bsr      RSPRITER
lbC0034BA:          move.w   #$1C,d7
                    move.w   #$88,d1
                    move.w   #$70,d2
                    lea      MISSILE_D,a1
                    cmp.w    #3,d3
                    bne      _RSPRITER1
                    lea      MISSILE_H,a1
_RSPRITER1:         bsr      RSPRITER
                    move.w   #$80,d1
                    move.w   #$8D,d2
                    move.w   #3,d6
                    move.w   #7,d7
                    cmp.w    #3,d3
                    beq      lbC003510
                    lea      HIDE_D,a1
                    tst.w    DRAWSUB
                    beq      _RSPRITER2
                    lea      SHOW_D,a1
                    bra      _RSPRITER2

lbC003510:          lea      HIDE_H,a1
                    tst.w    DRAWSUB
                    beq      _RSPRITER2
                    lea      SHOW_H,a1
_RSPRITER2:         bsr      RSPRITER
                    move.w   #$B8,d1
                    move.w   #$7F,d2
                    move.w   #$1E,d7
                    move.w   #2,d6
                    lea      COMP_D,a1
                    cmp.w    #4,d3
                    bne      _RSPRITER3
                    lea      COMP_H,a1
_RSPRITER3:         bsr      RSPRITER
                    move.w   IQ,d6
                    subq.w   #1,d6
                    mulu     #$30,d6
                    lea      NUMS,a1
                    add.w    d6,a1
                    move.w   #1,d6
                    move.w   #$C6,d1
                    move.w   #$90,d2
                    move.w   #6,d7
                    bsr      RSPRITER
                    move.w   #$80,d1
                    move.w   #$98,d2
                    move.w   #3,d6
                    move.w   #$15,d7
                    lea      GO_D,a1
                    cmp.w    #5,d3
                    bne      _RSPRITER4
                    lea      GO_H,a1
_RSPRITER4:         bsr      RSPRITER
                    bsr      SWAPSCREEN
                    rts

PARMS:              clr.w    INT_REQ
                    clr.w    SOUNDNUM
                    clr.w    SOUNDCT
                    move.l   #7,d7
                    bsr      NEW_SOUND
                    move.w   #0,DEMO
                    clr.w    S1AUTO
                    clr.w    S2AUTO
                    clr.w    S1DEAD
                    clr.w    S2DEAD
                    move.w   #5,d3
                    move.w   #$1F3F,d0
                    move.l   SCREEN2,a0
                    move.l   SCREEN1,a1
lbC0035F8:          move.l   (a0)+,(a1)+
                    dbra     d0,lbC0035F8
PARMS1:             bsr      READ_TRIGS
                    move.w   COUNTER,d1
                    and.w    #3,d1
                    beq      PARMS1_0_1
                    cmp.w    #2,d1
                    beq      PARMS1_0_2
                    bra      PARMSEND

PARMS1_0_1:         clr.l    d0
                    move.w   d3,-(sp)
                    bsr      JOYMOVE
                    move.w   (sp)+,d3
                    bra      PARMS1_0_3

PARMS1_0_2:         move.w   #1,d0
                    move.w   d3,-(sp)
                    bsr      JOYMOVE
                    move.w   (sp)+,d3
PARMS1_0_3:         tst.w    d2
                    bne      PARMS1_5
                    tst.w    d1
                    beq      PARMS2
                    bmi      PARMS1_3
                    move.l   #7,d7
                    bsr      NEW_SOUND
                    clr.w    DEMO
                    move.w   #4,d3
                    bra      PARMS2

PARMS1_3:           move.l   #7,d7
                    bsr      NEW_SOUND
                    clr.w    DEMO
                    move.w   #2,d3
                    bra      PARMS2

PARMS1_5:           bmi      PARMS1_6
                    move.l   #7,d7
                    bsr      NEW_SOUND
                    clr.w    DEMO
                    cmp.w    #1,d3
                    bne      lbC00369A
                    move.w   #3,d3
                    bra      PARMS2

lbC00369A:          move.w   #5,d3
                    bra      PARMS2

PARMS1_6:           move.l   #7,d7
                    bsr      NEW_SOUND
                    clr.w    DEMO
                    cmp.w    #3,d3
                    bne      lbC0036C2
                    move.w   #1,d3
                    bra      PARMS2

lbC0036C2:          cmp.w    #1,d3
                    beq      PARMS2
                    move.w   #3,d3
PARMS2:             tst.w    JOY1TRIG
                    bne      PARMS2_1
                    tst.w    JOY2TRIG
                    beq      PARMSEND
PARMS2_1:           clr.w    DEMO
                    move.l   #1,d7
                    bsr      NEW_SOUND
                    cmp.w    #1,d3
                    bne      PARMS2_2
                    addq.w   #1,LEVEL
                    cmp.w    #8,LEVEL
                    blt      PARMSEND
                    move.w   #1,LEVEL
                    bra      PARMSEND

PARMS2_2:           cmp.w    #2,d3
                    bne      PARMS2_3
                    cmp.w    #1,PLAYERS
                    beq      lbC003738
                    move.w   #1,PLAYERS
                    bra      PARMSEND

lbC003738:          move.w   #2,PLAYERS
                    bra      PARMSEND

PARMS2_3:           cmp.w    #3,d3
                    bne      PARMS2_4
                    eor.w    #$FFFF,DRAWSUB
                    bra      PARMSEND

PARMS2_4:           cmp.w    #4,d3
                    bne      PARMS2_5
                    addq.w   #1,IQ
                    cmp.w    #6,IQ
                    bne      PARMSEND
                    move.w   #1,IQ
                    bra      PARMSEND

PARMS2_5:           bra      PARMSRET

PARMSEND:           bsr      DRAW_NEW_SEL
                    addq.w   #1,DEMO
                    add.w    #1,COUNTER
                    cmp.w    #$190,DEMO
                    bne      lbC0037D2
                    bsr      PLAYINIT0
                    move.w   #1,S1AUTO
                    move.w   #1,S2AUTO
                    movem.l  d0-d7/a0-a6,-(sp)
                    bsr      CLEANTRAIL
                    bsr      DO_GAME
                    movem.l  (sp)+,d0-d7/a0-a6
                    move.w   #1,d3
                    clr.w    DEMO
                    bra      PARMS

lbC0037D2:          tst.w    ABORT
                    bne      PARMSRET
                    bra      PARMS1

PARMSRET:           clr.w    DEMO
                    rts

DO_GAME:            bsr      CLEAROUTCRAP
                    move.l   #DRYSLAB,CURRENT_SNOW
                    move.l   #DRYSLAB,PREVIOUS_SNOW
                    clr.w    SNOW_CHANGE
                    move.w   #$700,S1CT
                    move.w   #$700,S2CT
                    move.w   #4,COUNTER
                    clr.w    B1TIME
                    clr.w    B2TIME
DO_GAME_AGAIN:      move.w   HATCH_RAND,d6
                    tst.l    HATCHAD
                    bne      lbC0038D0
                    tst.w    DRAWSUB
                    bne      lbC00385A
                    move.b   IN_CASE,d1
                    and.b    lbB00B239,d1
                    and.b    lbB00B23A,d1
                    beq      lbC0038D0
lbC00385A:          move.l   #lbL00B68A,a1
                    move.w   #4,d3
lbC003864:          move.l   a1,a0
                    move.w   #$3F,d4
_RNDER:             bsr      RNDER
                    and.w    #$1F,d0
                    cmp.w    d6,d0
                    bgt      lbC00388E
                    tst.w    (a0)
                    bne      lbC00388E
                    tst.w    2(a0)
                    bne      lbC00388E
                    tst.w    4(a0)
                    beq      lbC0038A6
lbC00388E:          addq.w   #2,a0
                    dbra     d4,_RNDER
                    add.w    #$300,a1
                    dbra     d3,lbC003864
                    addq.w   #1,HATCH_RAND
                    bra      lbC0038D0

lbC0038A6:          move.w   #$128,(a0)
                    move.w   #$130,2(a0)
                    move.w   #$138,4(a0)
                    lea      2(a0),a0
                    move.l   a0,HATCHAD
                    lea      XHATCH,a1
                    lea      YHATCH,a2
                    bsr      SETXY
lbC0038D0:          tst.w    S1CT
                    bne      NO_SHOW_HATCH
                    tst.w    S2CT
                    bne      NO_SHOW_HATCH
                    tst.l    HATCHAD
                    beq      NO_SHOW_HATCH
                    move.l   HATCHAD,a0
                    move.w   #$128,-2(a0)
                    move.w   #$130,(a0)
                    move.w   #$138,2(a0)
NO_SHOW_HATCH:      tst.w    ABORT
                    beq      lbC003916
                    clr.w    ABORT
                    rts

lbC003916:          cmp.w    #$1B1,IGGY
                    bne      lbC00392E
                    move.w   #$1C1,IGGY
                    bra      lbC003936

lbC00392E:          move.w   #$1B1,IGGY
lbC003936:          clr.w    FEET_FLAG
                    move.w   #0,d0
                    clr.w    DONE_MOVE
                    move.w   S1CT,d1
                    bsr      BUSY
DO1_0:              move.w   S1MAPX,SPYX
                    move.w   S1MAPY,SPYY
                    bsr      SETWINDOW
                    move.w   S1CT,OLD_BUSY
                    tst.w    S1CT
                    bne      _CHECKMEET
                    tst.w    JOY1TRIG
                    bne      _CHECKMEET
                    tst.w    S1ROCKET
                    bne      _CHECKMEET
                    move.w   COUNTER,d7
                    and.w    #1,d7
                    bne      lbC0039DE
                    tst.w    S1SWAMP
                    bne      _CHECKMEET
                    tst.w    TENMINS
                    bne      lbC0039DE
                    tst.w    ONEMINS
                    bne      lbC0039DE
                    tst.w    S1IGLOO
                    bne      lbC0039DE
                    move.w   COUNTER,d7
                    and.w    #3,d7
                    bne      _CHECKMEET
                    subq.w   #1,S1ENERGY
                    bra      _CHECKMEET

lbC0039DE:          move.w   #$FFFF,DONE_MOVE
                    bsr      MOVE
                    move.w   SPYX,S1MAPX
                    move.w   SPYY,S1MAPY
_CHECKMEET:         bsr      CHECKMEET
                    move.w   SPYX,d1
                    lsr.w    #2,d1
                    move.w   d1,S1CELLX
                    move.w   SPYY,d1
                    lsr.w    #2,d1
                    move.w   d1,S1CELLY
                    bsr      GETWINDOW
                    tst.w    DONE_MOVE
                    beq      DO2_0
                    tst.w    OLD_BUSY
                    bne      DO2_0
                    tst.w    S1DEAD
                    bne      DO2_0
                    bsr      DIRFIX
                    tst.w    JOY1TRIG
                    beq      DO2_0
                    clr.w    S1F
DO2_0:              move.w   #1,d0
                    clr.w    DONE_MOVE
                    move.w   S2CT,d1
                    bsr      BUSY
                    move.w   S2CT,OLD_BUSY
DO2_1:              move.w   S2MAPX,SPYX
                    move.w   S2MAPY,SPYY
                    bsr      SETWINDOW
                    tst.w    S2CT
                    bne      _CHECKMEET0
                    tst.w    JOY2TRIG
                    bne      _CHECKMEET0
                    tst.w    S2ROCKET
                    bne      _CHECKMEET0
                    move.w   COUNTER,d7
                    and.w    #1,d7
                    bne      lbC003AF6
                    tst.w    S2SWAMP
                    bne      _CHECKMEET0
                    tst.w    TENMINS
                    bne      lbC003AF6
                    tst.w    ONEMINS
                    bne      lbC003AF6
                    tst.w    S2IGLOO
                    bne      lbC003AF6
                    move.w   COUNTER,d7
                    and.w    #3,d7
                    bne      _CHECKMEET0
                    subq.w   #1,S2ENERGY
                    bra      _CHECKMEET0

lbC003AF6:          move.w   #$FFFF,DONE_MOVE
                    bsr      MOVE
                    move.w   SPYX,S2MAPX
                    move.w   SPYY,S2MAPY
_CHECKMEET0:        bsr      CHECKMEET
                    move.w   SPYX,d1
                    lsr.w    #2,d1
                    move.w   d1,S2CELLX
                    move.w   SPYY,d1
                    lsr.w    #2,d1
                    move.w   d1,S2CELLY
                    bsr      GETWINDOW
                    tst.w    DONE_MOVE
                    beq      DO3_0
                    tst.w    OLD_BUSY
                    bne      DO3_0
                    tst.w    S2DEAD
                    bne      DO3_0
                    bsr      DIRFIX
                    tst.w    JOY2TRIG
                    beq      DO3_0
                    clr.w    S2F
DO3_0:              bsr      DRAWMOVE
                    move.w   S1CT,d1
                    move.w   d1,d2
                    lsr.w    #8,d2
                    and.w    #$FF,d1
                    cmp.w    #5,d2
                    bne      lbC003B8C
                    clr.w    d0
                    bsr      BUSY5_0
lbC003B8C:          move.w   S2CT,d1
                    move.w   d1,d2
                    lsr.w    #8,d2
                    and.w    #$FF,d1
                    cmp.w    #5,d2
                    bne      _SWAPSCREEN
                    move.w   #1,d0
                    bsr      BUSY5_0
_SWAPSCREEN:        bsr      SWAPSCREEN
WAITFLY:            cmp.w    #6,X462
                    blt.s    WAITFLY
                    move.w   #0,X462
                    move.w   #7,d1
                    and.w    COUNTER,d1
                    bne      DO4_0
                    tst.w    TENMINS
                    bne      lbC003BFC
                    tst.w    ONEMINS
                    bne      lbC003BFC
                    move.l   #8,d7
                    bsr      NEW_SOUND
                    move.w   TENSECS,d1
                    or.w     ONESECS,d1
                    beq      DO4_0
lbC003BFC:          subq.w   #1,ONESECS
                    bge      DO4_0
                    move.w   #9,ONESECS
                    subq.w   #1,TENSECS
                    bge      DO4_0
                    move.w   #5,TENSECS
                    subq.w   #1,ONEMINS
                    bge      DO4_0
                    move.w   #9,ONEMINS
                    subq.w   #1,TENMINS
DO4_0:              move.w   S1DEAD,d1
                    add.w    S2DEAD,d1
                    cmp.w    #2,d1
                    bne      lbC003C5C
                    move.w   S1CT,d1
                    or.w     S2CT,d1
                    beq      TIMEGONE
lbC003C5C:          move.w   TENMINS,d1
                    add.w    ONEMINS,d1
                    add.w    TENSECS,d1
                    add.w    ONESECS,d1
                    beq      TIMEGONE
                    addq.w   #1,COUNTER
                    tst.w    RLEN
                    bne      DO_GAMEEND
                    move.w   #0,TENMINS
                    move.w   #0,ONEMINS
                    move.w   #0,TENSECS
                    move.w   #0,ONESECS
                    rts

DO_GAMEEND:         cmp.w    #$28,DEMO
                    bne      lbC003CB8
                    rts

lbC003CB8:          clr.w    LOOPTIME
                    bra      DO_GAME_AGAIN

TIMEGONE:           rts

READPIX:            move.l   a0,d1
                    move.l   SCREEN2,d2
                    jsr      DECO_PIC
                    move.l   SCREEN2,a0
                    add.w    #$80,a0
                    move.l   BACK,a1
                    jsr      ATARI_COPY
                    move.l   SCREEN2,a0
                    addq.w   #4,a0
                    move.w   (a0),d7
                    jsr      COLOUR_COPY
                    rts

STIT:               tst.w    d1
                    beq      STUFFITEND
                    bsr      GETRAND
                    move.w   d2,(a0)
                    subq.w   #1,d1
                    addq.w   #1,COUNTER
                    bra.s    STIT

STUFFITEND:         rts

STUFFSPY:           movem.l  d0-d5,-(sp)
STUFFSPY0:          bsr      GETRAND
                    move.l   #S1MAPX,a1
                    move.l   #S1MAPY,a2
                    bsr      SETXY
                    move.w   S1MAPX,d5
                    lsl.w    #2,d5
                    add.w    #1,d5
                    move.w   d5,S1MAPX
                    move.w   S1MAPY,d0
                    lsl.w    #2,d0
                    move.w   d0,S1MAPY
                    and.w    #$FFF0,d0
                    move.w   d0,WIN1Y
                    sub.w    #13,d5
                    cmp.w    #2,d5
                    ble.s    STUFFSPY0
                    move.w   d5,WIN1X
STUFFSPY1:          bsr      GETRAND
                    move.l   #S2MAPX,a1
                    move.l   #S2MAPY,a2
                    bsr      SETXY
                    move.w   S2MAPX,d5
                    lsl.w    #2,d5
                    add.w    #1,d5
                    move.w   d5,S2MAPX
                    move.w   S2MAPY,d0
                    lsl.w    #2,d0
                    move.w   d0,S2MAPY
                    and.w    #$FFF0,d0
                    move.w   d0,WIN2Y
                    sub.w    #13,d5
                    cmp.w    #2,d5
                    ble.s    STUFFSPY1
                    move.w   d5,WIN2X
                    move.w   S1MAPY,d0
                    move.w   S2MAPY,d1
                    lsr.w    #4,d0
                    lsr.w    #4,d1
                    cmp.w    d0,d1
                    bgt      lbC003DE4
                    move.w   S1MAPX,d0
                    sub.w    S2MAPX,d0
                    bge      lbC003DDA
                    neg.w    d0
lbC003DDA:          cmp.w    #$28,d0
                    bgt      lbC003DE4
                    bra.s    STUFFSPY1

lbC003DE4:          movem.l  (sp)+,d0-d5
                    rts

RNDER:              move.l   RND1,d0
                    add.l    RND2,d0
                    move.l   d0,RND1
                    add.l    RND3,d0
                    move.l   d0,RND2
                    rol.l    #3,d0
                    sub.l    RND1,d0
                    add.l    #$FED11137,d0
                    eor.l    d0,RND3
                    rts

RNDER2:             move.l   d0,-(sp)
                    bsr.s    RNDER
                    move.l   d0,d1
                    move.l   (sp)+,d0
                    rts

GETRAND:            move.l   d1,-(sp)
lbC003E2A:          lea      MAP,a0
                    bsr.s    RNDER
                    and.w    #1,d0
                    move.w   d0,d1
                    bsr.s    RNDER
                    and.w    #3,d0
                    add.w    d1,d0
                    move.w   d0,SLABY
                    mulu     #$300,d0
                    add.w    d0,a0
                    bsr.s    RNDER
                    and.w    #3,d0
                    mulu     #$C0,d0
                    add.w    d0,a0
                    bsr.s    RNDER
                    and.w    #$7E,d0
                    move.w   d0,SLABX
                    add.w    d0,a0
                    tst.w    (a0)
                    bne.s    lbC003E2A
                    move.l   a0,d0
                    move.l   (sp)+,d1
                    rts

SETXY:              move.l   d0,-(sp)
                    move.l   a0,d0
                    sub.l    #MAP,d0
                    lsr.w    #1,d0
                    divu     #$60,d0
                    move.w   d0,(a2)
                    swap     d0
                    move.w   d0,(a1)
                    move.l   (sp)+,d0
                    rts

CLEAROUTCRAP:       move.l   #$FFFFFFFF,d0
                    lea      L_TENMINS,a0
                    move.l   d0,(a0)
                    move.l   d0,4(a0)
                    move.l   d0,8(a0)
                    move.l   d0,12(a0)
                    move.l   d0,$10(a0)
                    move.l   d0,$14(a0)
                    rts

L_TENMINS:          dc.l     0
L_ONEMINS:          dc.l     0
L_TENSECS:          dc.l     0
L_ONESECS:          dc.l     0
L_S1FUEL:           dc.l     0
L_S2FUEL:           dc.l     0
X462:               dc.w     0
SLABX:              dc.w     0
SLABY:              dc.w     0
OLD_BUSY:           dc.w     0
RND1:               dc.l     $343411FD
RND2:               dc.l     $8787112F
RND3:               dc.l     $716721ED

                    jmp      GOERROR

JUNK:               dc.w     $7830,$78FC,$18FC,$78FC,$7878,$C0,12,$38,$C0
                    dc.w     $3030,$C070,0,0,0,0,0,0,0,$CC70,$CC18,$38C0,$C00C
                    dc.w     $CCCC,$C0,12,$60,$C0,0,$C030,0,0,0,$30,0,0,0
                    dc.w     $DC30,$C30,$78F8,$C018,$CCCC,$78F8,$787C,$78F8
                    dc.w     $7CF8,$7030,$CC31,$D8F8,$78F8,$7CF8,$7CFC,$CCCD
                    dc.w     $8CCC,$CCFC,$EC30,$1818,$D80C,$F830,$787C,$CCC
                    dc.w     $C0CC,$CC60,$CCCC,$3030,$D831,$FCCC,$CCCC,$CCCC
                    dc.w     $C030,$CCCD,$8C78,$CC18,$CC30,$300C,$FC0C,$CC60
                    dc.w     $CC0C,$7CCC,$C0CC,$FC60,$CCCC,$3030,$F031,$ACCC
                    dc.w     $CCCC,$CCC0,$7830,$CCCD,$AC30,$CC30,$CC30,$60CC
                    dc.w     $18CC,$CC60,$CC18,$CCCC,$C0CC,$C060,$7CCC,$3030
                    dc.w     $D831,$8CCC,$CCCC,$CCC0,$C30,$CC78,$F878,$7C60
                    dc.w     $78FC,$FC78,$1878,$7860,$7870,$7CF8,$787C,$7860
                    dc.w     $CCC,$7830,$CC79,$8CCC,$78F8,$7CC0,$F81C,$7C30
                    dc.w     $D8CC,$CFC,0,0,0,0,0,0,0,0,$F800,$E0,0,0,$C0,$C00
                    dc.w     0,0,0,$F800

GOERROR:            move.l   a0,AREGISTERS
                    lea      DREGISTERS,a0
                    movem.l  d0-d7,(a0)
                    lea      lbL00427A,a0
                    movem.l  a1-a7,(a0)
                    move.l   #$12345678,-(sp)
                    move.l   #$98989898,-(sp)
                    move.l   sp,a1
                    lea      FROMSTACK,a0
                    move.w   #$3F,d0
lbC004032:          move.l   (a1)+,(a0)+
                    dbra     d0,lbC004032
                    lea      START,a0
                    move.l   a0,VUSP
                    lea      PALETTEZ,a0
                    lea      $DFF180,a1
                    move.w   #15,d0
lbC004054:          move.w   (a0)+,(a1)+
                    dbra     d0,lbC004054
CLEARS:             move.l   SCREEN1,a0
                    move.w   #$F9F,d0
lbC004064:          clr.l    (a0)+
                    clr.l    (a0)+
                    dbra     d0,lbC004064
                    move.l   SCREEN1,a0
                    lea      TEXT1,a1
                    move.l   a0,a2
                    bsr      OPTEXTZ
                    lea      $140(a2),a0
                    lea      AREGISTERS,a1
                    bsr      OPEIGHT
                    lea      TEXT2,a1
                    move.l   a0,a2
                    bsr      OPTEXTZ
                    lea      $140(a2),a0
                    lea      DREGISTERS,a1
                    bsr      OPEIGHT
                    lea      TEXT3,a1
                    move.l   a0,a2
                    bsr      OPTEXTZ
                    lea      $140(a2),a0
                    move.l   a0,a2
                    move.l   VUSP,d0
                    bsr      OPHEX
                    move.l   a2,a0
                    add.w    #$140,a0
                    lea      TEXT4,a1
                    move.l   a0,a2
                    bsr      OPTEXTZ
                    lea      $140(a2),a0
                    lea      FROMSTACK,a1
                    bsr      OPEIGHT
                    bsr      OPEIGHT
                    bsr      OPEIGHT
                    bsr      OPEIGHT
                    move.l   #$1A0000,d0
lbC0040F4:          subq.l   #1,d0
                    bne.s    lbC0040F4
BLOOP:              jmp      BLOOP

OPEIGHT:            bsr      OPFOUR
                    bsr      OPFOUR
                    rts

OPFOUR:             movem.l  d1/a0,-(sp)
                    move.w   #3,d1
lbC004110:          move.l   (a1)+,d0
                    bsr      OPHEX
                    move.l   a1,-(sp)
                    lea      SPACES,a1
                    bsr      OPTEXTZ
                    move.l   (sp)+,a1
                    dbra     d1,lbC004110
                    movem.l  (sp)+,d1/a0
                    add.w    #$140,a0
                    rts

OPHEX:              move.w   d1,-(sp)
                    move.w   #7,d1
lbC004138:          rol.l    #4,d0
                    move.w   d0,-(sp)
                    and.w    #15,d0
                    bsr      OPITEM
                    move.w   (sp)+,d0
                    dbra     d1,lbC004138
                    move.w   (sp)+,d1
                    rts

OPTEXTZ:            move.w   d0,-(sp)
lbC004150:          move.l   #0,d0
                    move.b   (a1)+,d0
                    beq      lbC00418A
                    cmp.b    #$20,d0
                    beq      lbC004180
                    cmp.b    #$61,d0
                    blt      lbC004178
                    sub.b    #$61,d0
                    add.w    #10,d0
                    bra      _OPITEM

lbC004178:          sub.w    #$30,d0
                    bra      _OPITEM

lbC004180:          move.w   #$24,d0
_OPITEM:            bsr      OPITEM
                    bra.s    lbC004150

lbC00418A:          move.w   (sp)+,d0
                    rts

OPITEM:             movem.l  d0/d1/a1,-(sp)
                    cmp.w    #$24,d0
                    beq      lbC0041C6
                    lea      JUNK,a1
                    add.w    d0,a1
                    move.l   a0,-(sp)
                    move.w   #7,d1
lbC0041A8:          move.b   (a1),d0
                    move.b   d0,(a0)
                    move.b   d0,8000(a0)
                    move.b   d0,16000(a0)
                    move.b   d0,24000(a0)
                    add.w    #$24,a1
                    add.w    #$28,a0
                    dbra     d1,lbC0041A8
                    move.l   (sp)+,a0
lbC0041C6:          move.l   a0,d0
                    and.w    #1,d0
                    bne      lbC0041D6
                    addq.w   #1,a0
                    bra      lbC0041D8

lbC0041D6:          addq.w   #1,a0
lbC0041D8:          movem.l  (sp)+,d0/d1/a1
                    rts

ILLEG:              bra      GOERROR

SETUPZ:             move.l   #ILLEG,8
                    move.l   #ILLEG,12
                    move.l   #ILLEG,$10
                    rts

TESTTEXT:           dc.w     $1111,$1111,$9999,$9999,$7777,$7777,$1231,$2312
                    dc.w     $3141,$5926,$1000,1,$9879,$8798,$1231,$2312
TEXT1:              dc.b     'a0 to a7',0
TEXT2:              dc.b     'd0 to d7',0
TEXT3:              dc.b     'start address',0
TEXT4:              dc.b     'top of the stack',0
SPACES:             dc.b     '  ',0
PALETTEZ:           dc.w     0,$777,$777,$777,$777,$777,$666,$777,$777,$777
                    dcb.w    5,$777
                    dc.w     $700
AREGISTERS:         dc.l     0
lbL00427A:          dcb.l    7,0
DREGISTERS:         dcb.l    8,0
FROMSTACK:          dcb.l    $3F,0
                    dc.l     0
VUSP:               dc.l     0
VSTATUS:            dc.l     0

DRAWSPY:            tst.w    d0
                    bne      DRAWSPY2_0
                    move.w   SPYWIN,d1
                    cmp.w    #2,d1
                    beq      DRAWSPY3_0
                    clr.l    d2
                    move.w   S1MAPX,d2
                    lsr.w    #2,d2
                    cmp.w    d2,d4
                    bne      DRAWSPY1_2
                    move.w   S1MAPX,d2
                    sub.w    WIN1X,d2
                    lsl.w    #2,d2
                    add.w    #$30,d2
                    add.w    S1OFFSETX,d2
                    move.w   d2,X
                    clr.l    d2
                    move.w   S1MAPY,d2
                    lsr.w    #2,d2
                    cmp.w    d2,d5
                    bne      DRAWSPY1_2
                    tst.w    S1DEPTH
                    beq      lbC004422
                    move.w   #$26,d2
                    bra      lbC00443A

lbC004422:          move.w   S1MAPY,d2
                    sub.w    WIN1Y,d2
                    lsl.w    #1,d2
                    add.w    #14,d2
                    add.w    S1SWAMP,d2
lbC00443A:          add.w    S1DEPTH,d2
                    sub.w    S1ALTITUDE,d2
                    add.w    S1OFFSETY,d2
                    move.w   d2,Y
                    move.l   #$290,d2
                    mulu     S1F,d2
                    add.l    S1FADDR,d2
                    move.w   #2,WIDTH
                    move.w   #$1E,d3
                    sub.w    S1SWAMP,d3
                    sub.w    S1DEPTH,d3
                    move.w   d3,HEIGHT
                    move.l   d2,BUFFER
                    move.l   SCREEN2,SCREEN
                    move.w   X,S1PLOTX
                    move.w   Y,S1PLOTY
                    move.w   Y,-(sp)
                    move.w   X,-(sp)
                    movem.l  d0,-(sp)
                    move.w   #10,RYOFF
                    bsr      SPRITER_MOD
                    movem.l  (sp)+,d0
                    move.w   (sp)+,X
                    move.w   (sp)+,Y
                    bsr      DRAWHANDS
DRAWSPY1_2:         move.w   SPYWIN,d1
                    cmp.w    #1,d1
                    bne      DRAWSPY3_0
                    clr.l    d2
                    move.w   S2MAPX,d2
                    lsr.w    #2,d2
                    cmp.w    d2,d4
                    bne      DRAWSPY2_0
                    move.w   S2MAPX,d2
                    sub.w    WIN1X,d2
                    lsl.w    #2,d2
                    add.w    #$30,d2
                    add.w    S2OFFSETX,d2
                    move.w   d2,X
                    clr.l    d2
                    move.w   S2MAPY,d2
                    lsr.w    #2,d2
                    cmp.w    d2,d5
                    bne      DRAWSPY2_0
                    tst.w    S2DEPTH
                    beq      lbC004534
                    move.w   #$26,d2
                    bra      lbC00454C

lbC004534:          move.w   S2MAPY,d2
                    sub.w    WIN1Y,d2
                    lsl.w    #1,d2
                    add.w    #14,d2
                    add.w    S2SWAMP,d2
lbC00454C:          add.w    S2DEPTH,d2
                    sub.w    S2ALTITUDE,d2
                    add.w    S2OFFSETY,d2
                    move.w   d2,Y
                    move.l   #$290,d2
                    mulu     S2F,d2
                    add.l    S2FADDR,d2
                    move.w   #2,WIDTH
                    move.w   #$1E,d3
                    sub.w    S2SWAMP,d3
                    sub.w    S2DEPTH,d3
                    move.w   d3,HEIGHT
                    move.l   d2,BUFFER
                    move.l   SCREEN2,SCREEN
                    move.w   X,S2PLOTX
                    move.w   Y,S2PLOTY
                    move.w   Y,-(sp)
                    move.w   X,-(sp)
                    movem.l  d0,-(sp)
                    move.w   #1,d0
                    move.w   #10,RYOFF
                    bsr      SPRITER_MOD
                    movem.l  (sp)+,d0
                    move.w   (sp)+,X
                    move.w   (sp)+,Y
                    moveq    #1,d0
                    bsr      DRAWHANDS
                    moveq    #0,d0
                    bra      DRAWSPY3_0

DRAWSPY2_0:         move.w   SPYWIN,d1
                    cmp.w    #1,d1
                    beq      DRAWSPY3_0
                    clr.l    d2
                    move.w   S2MAPX,d2
                    lsr.w    #2,d2
                    cmp.w    d2,d4
                    bne      DRAWSPY2_1
                    move.w   S2MAPX,d2
                    sub.w    WIN2X,d2
                    lsl.w    #2,d2
                    add.w    #$30,d2
                    add.w    S2OFFSETX,d2
                    move.w   d2,X
                    moveq    #0,d2
                    move.w   S2MAPY,d2
                    lsr.w    #2,d2
                    cmp.w    d2,d5
                    bne      DRAWSPY2_1
                    tst.w    S2DEPTH
                    beq      lbC004652
                    move.w   #$8B,d2
                    bra      lbC00466A

lbC004652:          move.w   S2MAPY,d2
                    sub.w    WIN2Y,d2
                    lsl.w    #1,d2
                    add.w    #$73,d2
                    add.w    S2SWAMP,d2
lbC00466A:          add.w    S2DEPTH,d2
                    sub.w    S2ALTITUDE,d2
                    add.w    S2OFFSETY,d2
                    move.w   d2,Y
                    move.l   #$290,d2
                    mulu     S2F,d2
                    add.l    S2FADDR,d2
                    move.w   #2,WIDTH
                    move.w   #$1E,d3
                    sub.w    S2SWAMP,d3
                    sub.w    S2DEPTH,d3
                    move.w   d3,HEIGHT
                    move.l   d2,BUFFER
                    move.l   SCREEN2,SCREEN
                    move.w   X,S2PLOTX
                    move.w   Y,S2PLOTY
                    move.w   Y,-(sp)
                    move.w   X,-(sp)
                    movem.l  d0,-(sp)
                    move.w   #$6F,RYOFF
                    bsr      SPRITER_MOD
                    movem.l  (sp)+,d0
                    move.w   (sp)+,X
                    move.w   (sp)+,Y
                    bsr      DRAWHANDS
DRAWSPY2_1:         move.w   SPYWIN,d1
                    cmp.w    #2,d1
                    bne      DRAWSPY3_0
                    clr.l    d2
                    move.w   S1MAPX,d2
                    lsr.w    #2,d2
                    cmp.w    d2,d4
                    bne      DRAWSPY3_0
                    move.w   S1MAPX,d2
                    sub.w    WIN2X,d2
                    lsl.w    #2,d2
                    add.w    #$30,d2
                    add.w    S1OFFSETX,d2
                    move.w   d2,X
                    clr.l    d2
                    move.w   S1MAPY,d2
                    lsr.w    #2,d2
                    cmp.w    d2,d5
                    bne      DRAWSPY3_0
                    tst.w    S1DEPTH
                    beq      lbC004764
                    move.w   #$8B,d2
                    bra      lbC00477C

lbC004764:          move.w   S1MAPY,d2
                    sub.w    WIN2Y,d2
                    lsl.w    #1,d2
                    add.w    #$73,d2
                    add.w    S1SWAMP,d2
lbC00477C:          add.w    S1DEPTH,d2
                    sub.w    S1ALTITUDE,d2
                    add.w    S1OFFSETY,d2
                    move.w   d2,Y
                    move.l   #$290,d2
                    mulu     S1F,d2
                    add.l    S1FADDR,d2
                    move.w   #2,WIDTH
                    move.w   #$1E,d3
                    sub.w    S1SWAMP,d3
                    sub.w    S1DEPTH,d3
                    move.w   d3,HEIGHT
                    move.l   d2,BUFFER
                    move.l   SCREEN2,SCREEN
                    move.w   X,S1PLOTX
                    move.w   Y,S1PLOTY
                    move.w   Y,-(sp)
                    move.w   X,-(sp)
                    movem.l  d0,-(sp)
                    clr.w    d0
                    move.w   #$6F,RYOFF
                    bsr      SPRITER_MOD
                    movem.l  (sp)+,d0
                    move.w   (sp)+,X
                    move.w   (sp)+,Y
                    moveq    #0,d0
                    bsr      DRAWHANDS
                    moveq    #1,d0
DRAWSPY3_0:         rts

DRAWHANDS:          movem.l  d0-d3/a0,-(sp)
                    move.w   #0,d7
                    move.w   #0,d6
                    tst.w    d0
                    bne      DRAWHANDS1
                    tst.w    S1CT
                    bne      DRAWHANDS3
                    cmp.w    #8,S1DEPTH
                    bge      DRAWHANDS3
                    cmp.w    #8,S1SWAMP
                    bge      DRAWHANDS3
                    move.w   S1HAND,d2
                    move.l   S1FADDR,d1
                    cmp.l    #BUF14,d1
                    bne      lbC004882
                    move.w   #8,d6
                    add.w    #7,X
                    add.w    #14,Y
                    bra      DRAWHANDS2

lbC004882:          cmp.l    #BUF13,d1
                    beq      DRAWHANDS3
                    add.w    #$11,X
                    add.w    #14,Y
                    cmp.l    #BUF12,d1
                    bne      DRAWHANDS2
                    sub.w    #$11,X
                    move.w   #$3F,d7
                    bra      DRAWHANDS2

DRAWHANDS1:         tst.w    S2CT
                    bne      DRAWHANDS3
                    cmp.w    #8,S2DEPTH
                    bge      DRAWHANDS3
                    cmp.w    #8,S2SWAMP
                    bge      DRAWHANDS3
                    move.w   S2HAND,d2
                    move.l   S2FADDR,d1
                    cmp.l    #BUF24,d1
                    bne      lbC004906
                    move.w   #8,d6
                    add.w    #7,X
                    add.w    #14,Y
                    bra      DRAWHANDS2

lbC004906:          cmp.l    #BUF23,d1
                    beq      DRAWHANDS3
                    add.w    #$11,X
                    add.w    #14,Y
                    cmp.l    #BUF22,d1
                    bne      DRAWHANDS2
                    sub.w    #$11,X
                    move.w   #$3F,d7
DRAWHANDS2:         move.w   d2,d1
                    lsr.w    #3,d1
                    beq      DRAWHANDS3
                    cmp.w    #8,d1
                    bne      lbC004952
                    subq.w   #6,Y
                    add.w    d6,X
lbC004952:          cmp.w    #9,d1
                    beq      DRAWHANDS3
                    add.w    d7,d1
                    lsl.w    #1,d1
                    lea      ITEMS_HEIGHT,a0
                    move.w   0(a0,d1.w),d7
                    lsl.w    #1,d1
                    lea      ITEMS_GRAPHICS,a0
                    move.l   0(a0,d1.w),a1
                    move.l   SCREEN2,a0
                    move.w   X,d1
                    move.w   Y,d2
                    move.w   #1,d6
                    bsr      RSPRITER
DRAWHANDS3:         movem.l  (sp)+,d0-d3/a0
                    rts

DRAWBUTTONS:        tst.w    d0
                    bne      DRAWBUTTONS1
                    move.w   #$4F,d2
                    move.l   #S1PLUNGER,a0
                    move.l   #S1SAW,a1
                    move.l   #S1BUCKET,a2
                    move.l   #S1PICK,a3
                    move.l   #S1SNSH,a4
                    move.l   #S1TNT,a6
                    move.w   S1FLASH,d4
                    beq      DRAWBUTTONS2
                    move.w   S1MENU,d4
                    bra      DRAWBUTTONS2

DRAWBUTTONS1:       move.w   #$B4,d2
                    move.l   #S2PLUNGER,a0
                    move.l   #S2SAW,a1
                    move.l   #S2BUCKET,a2
                    move.l   #S2PICK,a3
                    move.l   #S2SNSH,a4
                    move.l   #S2TNT,a6
                    move.w   S2FLASH,d4
                    beq      DRAWBUTTONS2
                    move.w   S2MENU,d4
DRAWBUTTONS2:       move.w   COUNTER,d5
                    and.w    #1,d5
                    move.w   #$5E,d1
                    cmp.w    #1,d4
                    bne      DRAWBUTTONS2_A
                    move.w   d5,d3
                    bra      DRAWBUTTONS2_B

DRAWBUTTONS2_A:     move.w   (a0),d3
DRAWBUTTONS2_B:     bsr      ONEBUTTON
                    add.w    #$16,d1
                    cmp.w    #2,d4
                    bne      DRAWBUTTONS2_0
                    move.w   d5,d3
                    bra      DRAWBUTTONS2_1

DRAWBUTTONS2_0:     move.w   (a1),d3
DRAWBUTTONS2_1:     bsr      ONEBUTTON
                    add.w    #$16,d1
                    cmp.w    #3,d4
                    bne      DRAWBUTTONS2_2
                    move.w   d5,d3
                    bra      DRAWBUTTONS2_3

DRAWBUTTONS2_2:     move.w   (a2),d3
DRAWBUTTONS2_3:     bsr      ONEBUTTON
                    add.w    #$16,d1
                    cmp.w    #4,d4
                    bne      DRAWBUTTONS2_4
                    move.w   d5,d3
                    bra      DRAWBUTTONS2_5

DRAWBUTTONS2_4:     move.w   (a3),d3
DRAWBUTTONS2_5:     bsr      ONEBUTTON
                    add.w    #$16,d1
                    cmp.w    #5,d4
                    bne      DRAWBUTTONS2_6
                    move.w   d5,d3
                    bra      DRAWBUTTONS2_7

DRAWBUTTONS2_6:     move.w   (a4),d3
DRAWBUTTONS2_7:     bsr      ONEBUTTON
                    add.w    #$16,d1
                    cmp.w    #6,d4
                    bne      DRAWBUTTONS2_8
                    move.w   d5,d3
                    bra      DRAWBUTTONS2_9

DRAWBUTTONS2_8:     move.w   (a6),d3
DRAWBUTTONS2_9:     bsr      ONEBUTTON
                    add.w    #$16,d1
                    cmp.w    #7,d4
                    bne      DRAWBUTTONS2_10
                    move.w   d5,d3
                    bra      DRAWBUTTONS2_11

DRAWBUTTONS2_10:    move.w   #1,d3
DRAWBUTTONS2_11:    bsr      ONEBUTTON
                    rts

ONEBUTTON:          movem.l  d0-d5/a0-a4,-(sp)
                    move.w   d1,X
                    move.w   d2,Y
                    move.l   SCREEN2,SCREEN
                    move.w   #1,WIDTH
                    move.w   #3,HEIGHT
                    tst.w    d3
                    bne      ONEBUTTON1
                    move.l   #G_EMPTY,BUFFER
                    bra      ONEBUTTON2

ONEBUTTON1:         move.l   #G_FULL,BUFFER
ONEBUTTON2:         bsr      SPRITER
                    movem.l  (sp)+,d0-d5/a0-a4
                    rts

SWAPSCREEN:         jmp      SWAP_SCREEN

DRAWTIME:           move.l   SCREEN2,a0
                    add.w    #$EFA,a0
                    move.w   TENMINS,d0
                    lea      L_TENMINS,a6
                    bsr      COMPARETWICE
                    beq      lbC004B40
                    jsr      NEXTCHAR
lbC004B40:          addq.w   #1,a0
                    move.w   ONEMINS,d0
                    lea      L_ONEMINS,a6
                    bsr      COMPARETWICE
                    beq      lbC004B5C
                    jsr      NEXTCHAR
lbC004B5C:          addq.w   #2,a0
                    move.w   TENSECS,d0
                    lea      L_TENSECS,a6
                    bsr      COMPARETWICE
                    beq      lbC004B78
                    jsr      NEXTCHAR
lbC004B78:          addq.w   #1,a0
                    move.w   ONESECS,d0
                    lea      L_ONESECS,a6
                    bsr      COMPARETWICE
                    beq      lbC004B94
                    jsr      NEXTCHAR
lbC004B94:          rts

HLINE:              movem.l  d0-d7/a0-a6,-(sp)
                    move.w   COLOR,d7
                    move.w   X,a2
                    move.w   a2,a3
                    add.w    COUNT,a3
                    move.l   SCREEN,a0
                    move.w   Y,d2
                    bsr      HORLINE
                    movem.l  (sp)+,d0-d7/a0-a6
                    rts

METER:              move.w   S1ENERGY,d3
                    bpl      lbC004BD4
                    clr.w    d3
                    bra      lbC004BE0

lbC004BD4:          cmp.w    #$41,d3
                    bls      lbC004BE0
                    move.w   #$41,d3
lbC004BE0:          move.w   #1,d6
                    move.w   #9,d2
                    move.w   #3,d1
                    move.w   #$41,d7
                    move.l   SCREEN2,a0
                    sub.w    d3,d7
                    beq      lbC004C06
                    lea      STRENGTH_EMPTY,a1
                    bsr      RDRAW_BUFF
lbC004C06:          lea      STRENGTH_FULL,a1
                    move.w   d7,d2
                    lsl.w    #3,d7
                    add.w    d7,a1
                    move.w   d3,d7
                    beq      METER1_0
                    add.w    #9,d2
                    bsr      RDRAW_BUFF
METER1_0:           move.w   S2ENERGY,d3
                    bpl      lbC004C30
                    clr.w    d3
                    bra      lbC004C3C

lbC004C30:          cmp.w    #$41,d3
                    bls      lbC004C3C
                    move.w   #$41,d3
lbC004C3C:          move.w   #1,d6
                    move.w   #$6E,d2
                    move.w   #3,d1
                    move.w   #$41,d7
                    move.l   SCREEN2,a0
                    sub.w    d3,d7
                    beq      lbC004C62
                    lea      STRENGTH_EMPTY,a1
                    bsr      RDRAW_BUFF
lbC004C62:          lea      STRENGTH_FULL,a1
                    move.w   d7,d2
                    lsl.w    #3,d7
                    add.w    d7,a1
                    move.w   d3,d7
                    beq      METER3
                    add.w    #$6E,d2
                    bsr      RDRAW_BUFF
METER3:             rts

OUNTER:             dc.w     0

FLASH:              move.w   #$10,d1
                    move.w   #$B8,d2
                    move.l   #G_HOLD_BACK,a1
                    move.w   #4,d6
                    move.w   #11,d7
                    move.l   SCREEN2,a0
                    bsr      RSPRITER
                    move.w   #$53,d2
                    bsr      RSPRITER
                    move.w   COUNTER,d3
                    and.w    #2,d3
                    beq      lbC004CE2
                    move.w   #1,d6
                    move.w   #10,d7
                    move.w   S1HAND,d3
                    lea      S1WIN,a6
                    bsr      FLASH_ONE
                    move.w   #$B8,d2
                    move.w   S2HAND,d3
                    lea      S2WIN,a6
                    bsr      FLASH_ONE
lbC004CE2:          rts

FLASH_ONE:          clr.w    (a6)
                    tst.w    d3
                    beq      lbC004D6C
                    lsr.w    #3,d3
                    cmp.w    #2,d3
                    beq      lbC004D0A
                    cmp.w    #1,d3
                    bne      lbC004D18
                    tst.b    IN_CASE
                    beq      lbC004D18
                    addq.w   #1,(a6)
lbC004D0A:          move.w   #$1A,d1
                    move.l   #G_CARD,a1
                    bsr      RSPRITER
lbC004D18:          cmp.w    #3,d3
                    beq      lbC004D34
                    cmp.w    #1,d3
                    bne      lbC004D42
                    tst.b    lbB00B239
                    beq      lbC004D42
                    addq.w   #1,(a6)
lbC004D34:          move.w   #$2A,d1
                    move.l   #G_URANIUM,a1
                    bsr      RSPRITER
lbC004D42:          cmp.w    #4,d3
                    beq      lbC004D5E
                    cmp.w    #1,d3
                    bne      lbC004D6C
                    tst.b    lbB00B23A
                    beq      lbC004D6C
                    addq.w   #1,(a6)
lbC004D5E:          move.w   #$3A,d1
                    move.l   #G_GYRO,a1
                    bsr      RSPRITER
lbC004D6C:          rts

COMPARETWICE:       cmp.w    (a6),d0
                    bne      lbC004D80
                    lea      2(a6),a6
                    cmp.w    (a6),d0
                    bne      lbC004D80
                    rts

lbC004D80:          move.w   d0,(a6)
                    cmp.w    #$FFFE,d0
                    rts

SPRITER_MOD:        tst.w    d0
                    bne      lbC004DA8
                    tst.w    S1ROCKET
                    bne      SPRITER_ROCKET1
                    move.w   S1HAND,d7
                    move.w   S1CT,d6
                    bra      SP_M2

lbC004DA8:          tst.w    S2ROCKET
                    bne      SPRITER_ROCKET2
                    move.w   S2HAND,d7
                    move.w   S2CT,d6
SP_M2:              tst.w    d6
                    bne      SP_M9
                    cmp.w    #$48,d7
                    bne      SP_M9
                    cmp.w    #$14,HEIGHT
                    ble      SP_M9
                    move.w   HEIGHT,-(sp)
                    move.w   #$14,HEIGHT
                    bsr      SPRITER
                    move.w   (sp)+,d7
                    sub.w    #$14,d7
                    move.w   d7,HEIGHT
                    add.w    #$14,Y
                    add.l    #$1F0,BUFFER
SP_M9:              bsr      SPRITER
                    rts

SPRITER_ROCKET1:    clr.w    S1HAND
                    bra      DO_R1

SPRITER_ROCKET2:    clr.w    S2HAND
DO_R1:              move.w   RYOFF,d2
                    move.w   X,d1
                    tst.w    RLEN
                    beq      DO_R4
                    cmp.w    #$3C,RHT
                    beq      lbC004E50
                    addq.w   #2,RHT
                    subq.w   #2,RY
                    bra      DO_R3

lbC004E50:          add.l    #$20,RDATA
                    subq.w   #2,RLEN
                    move.w   RLEN,d3
                    bne      lbC004E74
                    clr.w    SOUNDCT
                    bra      DO_R4

lbC004E74:          cmp.w    #$3C,d3
                    bge      DO_R3
                    move.w   d3,d7
                    bra      DO_R3A

DO_R3:              move.w   RHT,d7
DO_R3A:             move.w   #2,d6
                    add.w    RY,d2
                    move.l   RDATA,a1
                    move.w   COUNTER,d3
                    and.w    #1,d3
                    beq      lbC004EAA
                    add.w    #$4A0,a1
lbC004EAA:          move.l   SCREEN2,a0
                    bsr      RSPRITER
DO_R4:              rts

RSPRITER:           movem.l  d0-d7/a0-a2,-(sp)
                    subq.w   #1,d6
                    subq.w   #1,d7
                    move.w   d1,d4
                    and.w    #15,d4
                    lsl.w    #3,d2
                    add.w    d2,a0
                    lsl.w    #2,d2
                    add.w    d2,a0
                    and.w    #$FFF0,d1
                    asr.w    #3,d1
                    add.w    d1,a0
lbC004ED4:          move.l   a0,a2
                    move.w   d6,d0
lbC004ED8:          moveq    #0,d2
                    move.w   (a1),d2
                    or.w     2(a1),d2
                    or.w     4(a1),d2
                    or.w     6(a1),d2
                    ror.l    d4,d2
                    not.l    d2
                    and.w    d2,(a0)
                    and.w    d2,8000(a0)
                    and.w    d2,16000(a0)
                    and.w    d2,24000(a0)
                    swap     d2
                    and.w    d2,2(a0)
                    and.w    d2,8002(a0)
                    and.w    d2,16002(a0)
                    and.w    d2,24002(a0)
                    tst.w    d4
                    beq      lbC004F52
                    moveq    #0,d2
                    move.w   (a1)+,d2
                    ror.l    d4,d2
                    or.w     d2,(a0)+
                    swap     d2
                    or.w     d2,(a0)
                    moveq    #0,d2
                    move.w   (a1)+,d2
                    ror.l    d4,d2
                    or.w     d2,7998(a0)
                    swap     d2
                    or.w     d2,8000(a0)
                    moveq    #0,d2
                    move.w   (a1)+,d2
                    ror.l    d4,d2
                    or.w     d2,15998(a0)
                    swap     d2
                    or.w     d2,16000(a0)
                    moveq    #0,d2
                    move.w   (a1)+,d2
                    ror.l    d4,d2
                    or.w     d2,23998(a0)
                    swap     d2
                    or.w     d2,24000(a0)
                    bra      lbC004F68

lbC004F52:          move.w   (a1)+,d2
                    or.w     d2,(a0)+
                    move.w   (a1)+,d2
                    or.w     d2,7998(a0)
                    move.w   (a1)+,d2
                    or.w     d2,15998(a0)
                    move.w   (a1)+,d2
                    or.w     d2,23998(a0)
lbC004F68:          dbra     d0,lbC004ED8
                    lea      $28(a2),a0
                    dbra     d7,lbC004ED4
                    movem.l  (sp)+,d0-d7/a0-a2
                    rts

RBACKER:            movem.l  d0-d7/a0-a2,-(sp)
                    subq.w   #1,d6
                    subq.w   #1,d7
                    move.w   d1,d4
                    and.w    #15,d4
                    lsl.w    #3,d2
                    add.w    d2,a0
                    lsl.w    #2,d2
                    add.w    d2,a0
                    and.w    #$FFF0,d1
                    asr.w    #3,d1
                    add.w    d1,a0
lbC004F98:          move.l   a0,a2
                    move.w   d6,d0
                    clr.w    (a0)
                    clr.w    8000(a0)
                    clr.w    16000(a0)
                    clr.w    24000(a0)
lbC004FAA:          moveq    #0,d2
                    move.w   (a1)+,d2
                    ror.l    d4,d2
                    or.w     d2,(a0)+
                    swap     d2
                    move.w   d2,(a0)
                    moveq    #0,d2
                    move.w   (a1)+,d2
                    ror.l    d4,d2
                    or.w     d2,7998(a0)
                    swap     d2
                    move.w   d2,8000(a0)
                    moveq    #0,d2
                    move.w   (a1)+,d2
                    ror.l    d4,d2
                    or.w     d2,15998(a0)
                    swap     d2
                    move.w   d2,16000(a0)
                    moveq    #0,d2
                    move.w   (a1)+,d2
                    ror.l    d4,d2
                    or.w     d2,23998(a0)
                    swap     d2
                    move.w   d2,24000(a0)
                    bra      lbC004FEA

lbC004FEA:          dbra     d0,lbC004FAA
                    lea      $28(a2),a0
                    dbra     d7,lbC004F98
                    movem.l  (sp)+,d0-d7/a0-a2
                    rts

RSAVE_BUFF:         movem.l  d0-d7/a0-a2,-(sp)
                    subq.w   #1,d6
                    subq.w   #1,d7
                    lsl.w    #3,d2
                    add.w    d2,a0
                    lsl.w    #2,d2
                    add.w    d2,a0
                    asl.w    #1,d1
                    add.w    d1,a0
lbC005010:          move.l   a0,a2
                    move.w   d6,d0
lbC005014:          move.w   (a0)+,(a1)+
                    move.w   7998(a0),(a1)+
                    move.w   15998(a0),(a1)+
                    move.w   23998(a0),(a1)+
                    dbra     d0,lbC005014
                    lea      40(a2),a0
                    dbra     d7,lbC005010
                    movem.l  (sp)+,d0-d7/a0-a2
                    rts

RDRAW_BUFF:         movem.l  d0-d7/a0-a2,-(sp)
                    subq.w   #1,d6
                    subq.w   #1,d7
                    lsl.w    #3,d2
                    add.w    d2,a0
                    lsl.w    #2,d2
                    add.w    d2,a0
                    asl.w    #1,d1
                    add.w    d1,a0
lbC005048:          move.l   a0,a2
                    move.w   d6,d0
lbC00504C:          move.w   (a1)+,(a0)+
                    move.w   (a1)+,7998(a0)
                    move.w   (a1)+,15998(a0)
                    move.w   (a1)+,23998(a0)
                    dbra     d0,lbC00504C
                    lea      40(a2),a0
                    dbra     d7,lbC005048
                    movem.l  (sp)+,d0-d7/a0-a2
                    rts

RSTUFF_BUFF:        movem.l  d0-d7/a0-a2,-(sp)
                    move.w   d6,d3
                    mulu     d7,d3
                    lsl.w    #1,d3
                    move.l   d3,(a1)+
                    move.w   d6,(a1)+
                    move.w   d7,(a1)+
                    subq.w   #2,d6
                    subq.w   #1,d7
                    lsl.w    #3,d2
                    add.w    d2,a0
                    lsl.w    #2,d2
                    add.w    d2,a0
                    asl.w    #1,d1
                    add.w    d1,a0
lbC00508C:          move.l   a0,a2
                    move.w   d6,d0
lbC005090:          move.w   (a0),d1
                    or.w     8000(a0),d1
                    or.w     16000(a0),d1
                    or.w     24000(a0),d1
                    move.w   d1,(a1)
                    move.w   d3,d2
                    move.w   (a0),0(a1,d2.w)
                    add.w    d3,d2
                    move.w   8000(a0),0(a1,d2.w)
                    add.w    d3,d2
                    move.w   16000(a0),0(a1,d2.w)
                    add.w    d3,d2
                    move.w   24000(a0),0(a1,d2.w)
                    addq.w   #2,a1
                    addq.w   #2,a0
                    dbra     d0,lbC005090
                    clr.w    (a1)+
                    lea      $28(a2),a0
                    dbra     d7,lbC00508C
                    movem.l  (sp)+,d0-d7/a0-a2
                    rts

RSTUFF_BUFF_SHORT:  movem.l  d0-d7/a0-a2,-(sp)
                    move.w   d6,d3
                    mulu     d7,d3
                    lsl.w    #1,d3
                    move.l   d3,(a1)+
                    move.w   d6,(a1)+
                    move.w   d7,(a1)+
                    subq.w   #1,d6
                    subq.w   #1,d7
                    lsl.w    #3,d2
                    add.w    d2,a0
                    lsl.w    #2,d2
                    add.w    d2,a0
                    asl.w    #1,d1
                    add.w    d1,a0
lbC0050F6:          move.l   a0,a2
                    move.w   d6,d0
lbC0050FA:          move.w   d3,d2
                    move.w   (a0),(a1)
                    move.w   8000(a0),0(a1,d2.w)
                    add.w    d3,d2
                    move.w   16000(a0),0(a1,d2.w)
                    add.w    d3,d2
                    move.w   24000(a0),0(a1,d2.w)
                    addq.w   #2,a1
                    addq.w   #2,a0
                    dbra     d0,lbC0050FA
                    lea      40(a2),a0
                    dbra     d7,lbC0050F6
                    movem.l  (sp)+,d0-d7/a0-a2
                    rts

RSPRITER_AM:        movem.l  d0-d7/a0-a4,-(sp)
                    move.w   MINTERMS,d5
                    lea      $DFF000,a2
                    move.w   #$8440,$96(a2)
                    move.w   4(a1),d6
                    cmp.w    #$FE2,d5
                    beq      lbC00514E
                    subq.w   #1,d6
lbC00514E:          lsl.w    #3,d2
                    add.w    d2,a0
                    lsl.w    #2,d2
                    add.w    d2,a0
                    move.w   d1,d3
                    and.w    #15,d3
                    lsr.w    #3,d1
                    and.w    #$FFFE,d1
                    add.w    d1,a0
                    move.w   #$FFFF,$44(a2)
                    move.w   #$FFFF,$46(a2)
                    ror.w    #4,d3
                    move.w   d3,$42(a2)
                    or.w     d5,d3
                    move.w   d3,$40(a2)
                    move.w   #0,$64(a2)
                    cmp.w    #$FE2,d5
                    beq      lbC005190
                    move.w   #2,$64(a2)
lbC005190:          move.w   #0,$62(a2)
                    move.w   #$28,d3
                    sub.w    d6,d3
                    sub.w    d6,d3
                    move.w   d3,$60(a2)
                    move.w   d3,$66(a2)
                    lsl.w    #6,d7
                    or.w     d6,d7
                    move.l   (a1),a3
                    lea      8(a1),a1
                    move.l   a1,a4
                    add.l    a3,a1
                    move.l   a1,$50(a2)
                    move.l   a4,$4C(a2)
                    move.l   a0,$48(a2)
                    move.l   a0,$54(a2)
                    move.w   d7,$58(a2)
                    add.w    #$1F40,a0
                    add.l    a3,a1
                    bsr      WAIT_BLIT
                    move.l   a1,$50(a2)
                    move.l   a4,$4C(a2)
                    move.l   a0,$48(a2)
                    move.l   a0,$54(a2)
                    move.w   d7,$58(a2)
                    add.w    #$1F40,a0
                    add.l    a3,a1
                    bsr      WAIT_BLIT
                    move.l   a1,$50(a2)
                    move.l   a4,$4C(a2)
                    move.l   a0,$48(a2)
                    move.l   a0,$54(a2)
                    move.w   d7,$58(a2)
                    add.w    #$1F40,a0
                    add.l    a3,a1
                    bsr      WAIT_BLIT
                    move.l   a1,$50(a2)
                    move.l   a4,$4C(a2)
                    move.l   a0,$48(a2)
                    move.l   a0,$54(a2)
                    move.w   d7,$58(a2)
                    movem.l  (sp)+,d0-d7/a0-a4
                    rts

SPRITER:            movem.l  d0-d7/a0/a1,-(sp)
                    move.w   WIDTH,d6
                    move.w   HEIGHT,d7
                    move.w   X,d1
                    move.w   Y,d2
                    move.l   SCREEN,a0
                    move.l   BUFFER,a1
                    bsr      RSPRITER
                    movem.l  (sp)+,d0-d7/a0/a1
                    rts

SPRITER_AM:         movem.l  d0-d7/a0/a1,-(sp)
                    move.w   X,d1
                    move.w   Y,d2
                    move.w   HEIGHT,d7
                    move.l   SCREEN,a0
                    move.l   BUFFER,a1
                    bsr      RSPRITER_AM
                    movem.l  (sp)+,d0-d7/a0/a1
                    rts

BACKER:             movem.l  d0-d7/a0/a1,-(sp)
                    move.w   WIDTH,d6
                    move.w   HEIGHT,d7
                    move.w   X,d1
                    move.w   Y,d2
                    move.l   SCREEN,a0
                    move.l   BUFFER,a1
                    bsr      RBACKER
                    movem.l  (sp)+,d0-d7/a0/a1
                    rts

X:                  dc.w     0
Y:                  dc.w     0
WIDTH:              dc.w     0
HEIGHT:             dc.w     0
BUFFER:             dc.l     0
SCREEN:             dc.l     0
MINTERMS:           dc.w     $FE2

RSNOWING:           move.w   #$3F,d7
                    tst.w    SNOW_CHANGE
                    bne      lbC005326
                    move.l   CURRENT_SNOW,PREVIOUS_SNOW
                    lea      DRYSLAB,a3
                    tst.w    TENMINS
                    bne      lbC00538C
                    cmp.w    #1,ONEMINS
                    bhi      lbC00538C
                    lea      SLEETSLAB,a3
                    cmp.w    #1,ONEMINS
                    beq      lbC005316
                    lea      SNOWSLAB,a3
lbC005316:          move.l   a3,CURRENT_SNOW
                    move.w   d7,SNOW_CHANGE
                    bra      lbC00532C

lbC005326:          subq.w   #1,SNOW_CHANGE
lbC00532C:          move.l   CURRENT_SNOW,a3
                    move.l   SCREEN2,a0
                    add.w    #$198,a0
                    move.w   SNOWCOUNT,d4
                    subq.w   #1,d4
                    and.w    #15,d4
                    move.w   d4,SNOWCOUNT
                    cmp.w    #2,SPYWIN
                    beq      lbC005368
                    tst.w    S1IGLOO
                    bne      lbC005368
                    bsr      DOSNOW
lbC005368:          move.l   SCREEN2,a0
                    add.w    #$1160,a0
                    cmp.w    #1,SPYWIN
                    beq      lbC00538C
                    tst.w    S2IGLOO
                    bne      lbC00538C
                    bsr      DOSNOW
lbC00538C:          rts

DOSNOW:             movem.l  d7/a3/a4,-(sp)
                    move.w   SNOWCOUNT,d4
                    mulu     #$50,d4
                    add.w    d4,a3
                    move.l   a3,a4
                    move.w   #0,d3
lbC0053A4:          cmp.w    SNOW_CHANGE,d7
                    bne      lbC0053C6
                    sub.l    CURRENT_SNOW,a3
                    add.l    PREVIOUS_SNOW,a3
                    sub.l    CURRENT_SNOW,a4
                    add.l    PREVIOUS_SNOW,a4
lbC0053C6:          cmp.w    #1,d7
                    bgt      lbC0053D6
                    move.l   #DRYSLAB,a3
                    move.l   a3,a4
lbC0053D6:          and.w    #15,d3
                    bne      lbC0053E0
                    move.l   a3,a4
lbC0053E0:          subq.w   #1,d3
                    move.l   a0,a2
                    move.w   #9,d0
lbC0053E8:          move.w   (a4)+,d2
                    and.w    d2,(a0)+
                    move.w   (a4)+,d2
                    or.w     d2,$1F3E(a0)
                    move.w   (a4)+,d2
                    or.w     d2,$3E7E(a0)
                    move.w   (a4)+,d2
                    or.w     d2,$5DBE(a0)
                    dbra     d0,lbC0053E8
                    lea      $28(a2),a0
                    dbra     d7,lbC0053A4
                    movem.l  (sp)+,d7/a3/a4
                    rts

SNOWCOUNT:          dc.w     0
CURRENT_SNOW:       dc.l     0
PREVIOUS_SNOW:      dc.l     0
SNOW_CHANGE:        dc.w     0

PUTCHAR:            movem.l  d2/a0,-(sp)
                    lsl.w    #3,d2
                    add.w    d2,a0
                    lsl.w    #2,d2
                    add.w    d2,a0
                    add.w    d1,a0
                    bsr      NEXTCHAR
                    movem.l  (sp)+,d2/a0
                    rts

NEXTCHAR:           movem.l  d1/a0/a1,-(sp)
                    and.w    #15,d0
                    lsl.w    #5,d0
                    lea      FONT,a1
                    add.w    d0,a1
                    move.w   #7,d0
lbC00544A:          move.b   (a1)+,(a0)
                    move.b   (a1)+,8000(a0)
                    move.b   (a1)+,16000(a0)
                    move.b   (a1)+,24000(a0)
                    add.w    #40,a0
                    dbra     d0,lbC00544A
                    movem.l  (sp)+,d1/a0/a1
                    rts

FONT:               dc.w     $C33E,$3EFF,$817F,$7FFF,$9977,$77FF,$9977,$77FF
                    dc.w     $9977,$77FF,$9977,$77FF,$817F,$7FFF,$C33C,$3CFF
                    dc.w     $E71C,$1CFF,$C73C,$3CFF,$C73C,$3CFF,$E71C,$1CFF
                    dc.w     $E71C,$1CFF,$E71C,$1CFF,$C33E,$3EFF,$C33E,$3EFF
                    dc.w     $E31C,$1CFF,$C13F,$3FFF,$9977,$77FF,$F10F,$FFF
                    dc.w     $E31E,$1EFF,$C73C,$3CFF,$817F,$7FFF,$817F,$7FFF
                    dc.w     $C33E,$3EFF,$817F,$7FFF,$F907,$7FF,$E11F,$1FFF
                    dc.w     $E11F,$1FFF,$F907,$7FF,$817F,$7FFF,$C33E,$3EFF
                    dc.w     $9F60,$60FF,$9F70,$70FF,$937C,$7CFF,$937E,$7EFF
                    dc.w     $917F,$7FFF,$817F,$7FFF,$F30E,$EFF,$F30E,$EFF
                    dc.w     $817F,$7FFF,$817F,$7FFF,$9F70,$70FF,$837C,$7CFF
                    dc.w     $F907,$7FF,$B967,$67FF,$817F,$7FFF,$C33E,$3EFF
                    dc.w     $E718,$18FF,$C13F,$3FFF,$9F70,$70FF,$837E,$7EFF
                    dc.w     $817F,$7FFF,$9977,$77FF,$C13F,$3FFF,$E31E,$1EFF
                    dc.w     $817E,$7EFF,$817F,$7FFF,$F907,$7FF,$F107,$7FF
                    dc.w     $F10D,$DFF,$E30E,$EFF,$E31E,$1EFF,$E71C,$1CFF
                    dc.w     $C33E,$3EFF,$817F,$7FFF,$9977,$77FF,$C33E,$3EFF
                    dc.w     $C33E,$3EFF,$9977,$77FF,$817F,$7FFF,$C33E,$3EFF
                    dc.w     $C13E,$3EFF,$817F,$7FFF,$9977,$77FF,$817F,$7FFF
                    dc.w     $C13F,$3FFF,$F907,$7FF,$F907,$7FF,$F907,$7FF,$FF
                    dc.w     $7878,$FF,$FCFC,$FF,$FCFC,$FF,$FCFC,$FF,$FCFC,$FF
                    dc.w     $FCFC,$FF,$7878,$FF,0

CALCAD:             movem.l  d1/d2/a1,-(sp)
                    lsl.w    #3,d2
                    add.w    d2,a0
                    lsl.w    #2,d2
                    add.w    d2,a0
                    move.w   d1,d6
                    lsr.w    #3,d1
                    and.w    #$FFFE,d1
                    add.w    d1,a0
                    and.w    #15,d6
                    lsl.w    #1,d6
                    lea      BITTAB,a1
                    move.w   0(a1,d6.w),d6
                    movem.l  (sp)+,d1/d2/a1
                    rts

PLOTPOINT:          and.w    d6,(a0)
                    and.w    d6,8000(a0)
                    and.w    d6,16000(a0)
                    and.w    d6,24000(a0)
                    or.w     d1,(a0)
                    or.w     d2,8000(a0)
                    or.w     d3,16000(a0)
                    or.w     d4,24000(a0)
                    rts

LEFT_RIGHT:         tst.w    GO_LEFT
                    bne      BACKWARDS
                    ror.w    #1,d1
                    ror.w    #1,d2
                    ror.w    #1,d3
                    ror.w    #1,d4
                    ror.w    #1,d6
                    bcs      LR_RET
                    addq.w   #2,a0
                    rts

BACKWARDS:          rol.w    #1,d1
                    rol.w    #1,d2
                    rol.w    #1,d3
                    rol.w    #1,d4
                    rol.w    #1,d6
                    bcs      LR_RET
                    subq.w   #2,a0
LR_RET:             rts

UP_DOWN:            tst.w    GO_UP
                    bne      UPWARDS
                    add.w    #$28,a0
                    rts

UPWARDS:            sub.w    #$28,a0
                    rts

DRAW_LINE:          movem.l  d0-d7/a0-a2,-(sp)
                    moveq    #0,d5
                    bsr      CALCAD
                    move.w   d3,d0
                    sub.w    d1,d0
                    bpl      lbC005674
                    neg.w    d0
                    move.w   #$FFFF,GO_LEFT
                    bra      lbC00567A

lbC005674:          clr.w    GO_LEFT
lbC00567A:          move.w   d4,d5
                    sub.w    d2,d5
                    bpl      lbC005690
                    neg.w    d5
                    move.w   #$FFFF,GO_UP
                    bra      lbC005696

lbC005690:          clr.w    GO_UP
lbC005696:          addq.w   #1,d5
                    addq.w   #1,d0
                    lsl.w    #8,d5
                    divu     d0,d5
                    and.l    #$FFFF,d5
                    lsl.l    #8,d5
SETUPCOLOUR:        move.w   #0,d1
                    move.w   #0,d2
                    move.w   #0,d3
                    move.w   #0,d4
                    ror.w    #1,d7
                    bcc      lbC0056BE
                    move.w   d6,d1
lbC0056BE:          ror.w    #1,d7
                    bcc      lbC0056C6
                    move.w   d6,d2
lbC0056C6:          ror.w    #1,d7
                    bcc      lbC0056CE
                    move.w   d6,d3
lbC0056CE:          ror.w    #1,d7
                    bcc      lbC0056D6
                    move.w   d6,d4
lbC0056D6:          not.w    d6
                    clr.l    d7
MORE_POINTS:        bsr      PLOTPOINT
                    add.l    d5,d7
                    swap     d7
                    tst.w    d7
                    beq      DONE_POINT
_UP_DOWN:           bsr      UP_DOWN
                    bsr      PLOTPOINT
                    subq.w   #1,d7
                    bne.s    _UP_DOWN
DONE_POINT:         bsr      LEFT_RIGHT
                    swap     d7
                    subq.w   #1,d0
                    bne.s    MORE_POINTS
                    movem.l  (sp)+,d0-d7/a0-a2
                    rts

BITTAB:             dc.w     $8000,$4000,$2000,$1000,$800,$400,$200,$100,$80
                    dc.w     $40,$20,$10,8,4,2,1
GO_LEFT:            dc.w     0
GO_UP:              dc.w     0

HORLINE:            movem.l  d0-d5/a0,-(sp)
                    move.w   a3,d4
                    move.w   a2,d0
                    sub.w    d0,d4
                    addq.w   #1,d4
                    move.w   a2,d1
                    bsr      POINT
                    move.w   d1,d5
                    and.w    #15,d5
                    beq      GOT_BOUNDARY_STRAIGHT
EARLY:              tst.w    d4
                    beq      DONE_LINE
                    move.w   d1,d5
                    and.w    #15,d5
                    beq      GOT_BOUNDARY
                    move.w   d7,d0
                    ror.w    #1,d0
                    bcc      lbC005762
                    or.w     d3,(a0)
                    bra      lbC005768

lbC005762:          not.w    d3
                    and.w    d3,(a0)
                    not.w    d3
lbC005768:          ror.w    #1,d0
                    bcc      lbC005776
                    or.w     d3,8000(a0)
                    bra      lbC00577E

lbC005776:          not.w    d3
                    and.w    d3,8000(a0)
                    not.w    d3
lbC00577E:          ror.w    #1,d0
                    bcc      lbC00578C
                    or.w     d3,16000(a0)
                    bra      lbC005794

lbC00578C:          not.w    d3
                    and.w    d3,16000(a0)
                    not.w    d3
lbC005794:          ror.w    #1,d0
                    bcc      lbC0057A2
                    or.w     d3,24000(a0)
                    bra      lbC0057AA

lbC0057A2:          not.w    d3
                    and.w    d3,24000(a0)
                    not.w    d3
lbC0057AA:          lsr.w    #1,d3
                    addq.w   #1,d1
                    subq.w   #1,d4
                    bra.s    EARLY

GOT_BOUNDARY:       addq.w   #2,a0
GOT_BOUNDARY_STRAIGHT:
                    cmp.w    #15,d4
                    ble      LATER
                    move.w   #$FFFF,d3
                    move.w   d7,d0
                    ror.w    #1,d0
                    bcc      lbC0057CE
                    move.w   d3,(a0)
                    bra      lbC0057D0

lbC0057CE:          clr.w    (a0)
lbC0057D0:          ror.w    #1,d0
                    bcc      lbC0057DE
                    move.w   d3,8000(a0)
                    bra      lbC0057E2

lbC0057DE:          clr.w    $1F40(a0)
lbC0057E2:          ror.w    #1,d0
                    bcc      lbC0057F0
                    move.w   d3,16000(a0)
                    bra      lbC0057F4

lbC0057F0:          clr.w    $3E80(a0)
lbC0057F4:          ror.w    #1,d0
                    bcc      lbC005802
                    move.w   d3,24000(a0)
                    bra      lbC005806

lbC005802:          clr.w    24000(a0)
lbC005806:          sub.w    #$10,d4
                    add.w    #$10,d1
                    bra.s    GOT_BOUNDARY

LATER:              tst.w    d4
                    beq      DONE_LINE
                    move.w   #$8000,d3
LATER2:             move.w   d7,d0
                    ror.w    #1,d0
                    bcc      lbC005828
                    or.w     d3,(a0)
                    bra      lbC00582E

lbC005828:          not.w    d3
                    and.w    d3,(a0)
                    not.w    d3
lbC00582E:          ror.w    #1,d0
                    bcc      lbC00583C
                    or.w     d3,8000(a0)
                    bra      lbC005844

lbC00583C:          not.w    d3
                    and.w    d3,8000(a0)
                    not.w    d3
lbC005844:          ror.w    #1,d0
                    bcc      lbC005852
                    or.w     d3,16000(a0)
                    bra      lbC00585A

lbC005852:          not.w    d3
                    and.w    d3,16000(a0)
                    not.w    d3
lbC00585A:          ror.w    #1,d0
                    bcc      lbC005868
                    or.w     d3,24000(a0)
                    bra      lbC005870

lbC005868:          not.w    d3
                    and.w    d3,24000(a0)
                    not.w    d3
lbC005870:          ror.w    #1,d3
                    subq.w   #1,d4
                    bne.s    LATER2
DONE_LINE:          movem.l  (sp)+,d0-d5/a0
BYE_BYE_LINE:       rts

POINT:              move.w   d2,d0
                    lsl.w    #3,d0
                    add.w    d0,a0
                    lsl.w    #2,d0
                    add.w    d0,a0
                    move.w   d1,d0
                    lsr.w    #3,d0
                    and.w    #$FE,d0
                    add.w    d0,a0
                    move.w   d1,d0
                    and.w    #15,d0
                    move.w   #$8000,d3
                    lsr.w    d0,d3
                    rts

GET_RANDOM:         move.l   d1,-(sp)
                    move.l   RANDOM1,d0
                    add.l    RANDOM2,d0
                    move.l   d0,RANDOM1
                    sub.l    RANDOM3,d0
                    add.l    d0,RANDOM2
                    move.w   d0,d1
                    and.w    #7,d1
                    swap     d0
                    ror.l    d1,d0
                    sub.l    d0,RANDOM3
                    add.l    d0,RANDOM1
                    move.l   (sp)+,d1
                    rts

RANDOM1:            dc.l     $98081FDD
RANDOM2:            dc.l     $FECA1919
RANDOM3:            dc.l     $121212FD

READ_TRIGS:         move.l   d0,-(sp)
                    bsr      SCAN_JOY
                    cmp.w    #2,S1MODE
                    bne      lbC005906
                    move.w   #$40,d0
                    bsr      INKEY
                    beq      ZER1
                    bra      NZ1

lbC005906:          tst.w    S1MODE
                    bne      lbC00591E
                    tst.w    FIRE1
                    bne      NZ1
                    bra      ZER1

lbC00591E:          tst.w    FIRE0
                    bne      NZ1
ZER1:               clr.w    JOY1TRIG
                    bra      RT2

NZ1:                move.w   #$FFFF,JOY1TRIG
RT2:                cmp.w    #2,S2MODE
                    bne      lbC005956
                    move.w   #$40,d0
                    bsr      INKEY
                    beq      ZER2
                    bra      NZ2

lbC005956:          bhi      lbC005980
                    tst.w    S2MODE
                    bne      lbC005972
                    tst.w    FIRE1
                    bne      NZ2
                    bra      ZER2

lbC005972:          tst.w    FIRE0
                    bne      NZ2
                    bra      ZER2

lbC005980:          cmp.w    #3,S2MODE
                    bne      lbC00599A
                    tst.w    FIRE0
                    bne      NZ2
                    bra      ZER2

lbC00599A:          tst.w    FIRE1
                    bne      NZ2
ZER2:               clr.w    JOY2TRIG
                    bra      RT_RET

NZ2:                move.w   #$FFFF,JOY2TRIG
RT_RET:             move.l   (sp)+,d0
                    rts

MOVE:               bsr      JOYMOVE
                    move.l   d0,-(sp)
                    tst.w    DEMO
                    beq      NO_DEMO
                    bsr      SCAN_JOY
                    tst.w    FIRE0
                    bne      OUT_DEMO
                    tst.w    FIRE1
                    bne      OUT_DEMO
                    move.w   #$40,d0
                    bsr      INKEY
                    bne      OUT_DEMO
                    move.w   #$44,d0
                    bsr      INKEY
                    bne      OUT_DEMO
                    move.w   #$45,d0
                    bsr      INKEY
                    beq      NO_DEMO
OUT_DEMO:           cmp.w    #$28,DEMO
                    blt      NO_DEMO
                    move.w   #$28,DEMO
NO_DEMO:            move.l   (sp)+,d0
                    tst.w    d0
                    bne      lbC005A36
                    move.l   #S1BDIR,a4
                    tst.w    S1AUTO
                    beq      lbC005A78
                    bra      lbC005A46

lbC005A36:          move.l   #S2BDIR,a4
                    tst.w    S2AUTO
                    beq      lbC005A78
lbC005A46:          movem.l  a4,-(sp)
                    bsr      BRAINMOVE
                    cmp.w    #8,d1
                    bgt      lbC005A5E
                    cmp.w    #$FFF8,d1
                    bgt      lbC005A60
lbC005A5E:          clr.w    d1
lbC005A60:          tst.w    d1
                    bne      _RANDMOVE
                    tst.w    d2
                    beq      lbC005A70
_RANDMOVE:          bsr      RANDMOVE
lbC005A70:          movem.l  (sp)+,a4
                    bra      lbC005A84

lbC005A78:          movem.l  a4,-(sp)
                    bsr      JOYMOVE
                    movem.l  (sp)+,a4
lbC005A84:          lsl.w    #1,d1
                    lsl.w    #1,d2
MOVE0_0:            tst.w    d2
                    beq      MOVE0_2
                    tst.w    d0
                    bne      lbC005AD2
                    move.w   S1MAPY,d3
                    move.w   d3,d5
                    move.w   S1MAPX,d6
                    tst.w    S1CT
                    beq      lbC005B0C
                    move.w   S1CT,d7
                    and.w    #$FF00,d7
                    cmp.w    #$F00,d7
                    beq      lbC005B0C
                    move.w   #0,d1
                    move.w   #0,d2
                    move.w   #0,SPYDIR
                    bra      lbC005B0C

lbC005AD2:          move.w   S2MAPY,d3
                    move.w   d3,d5
                    move.w   S2MAPX,d6
                    tst.w    S2CT
                    beq      lbC005B0C
                    move.w   S2CT,d7
                    and.w    #$FF00,d7
                    cmp.w    #$F00,d7
                    beq      lbC005B0C
                    move.w   #0,d1
                    move.w   #0,d2
                    move.w   #0,SPYDIR
lbC005B0C:          move.w   d3,d4
                    add.w    d2,d4
                    lsr.w    #4,d3
                    lsr.w    #4,d4
                    cmp.w    d3,d4
                    beq      MOVE0_2
                    lea      MAP,a0
                    move.w   SPYY,d3
                    lsr.w    #2,d3
                    mulu     MAXMAPX,d3
                    move.w   SPYX,d4
                    lsr.w    #2,d4
                    add.w    d4,d3
                    lsl.w    #1,d3
                    add.w    d3,a0
                    move.w   (a0),d3
                    lsr.w    #3,d3
                    cmp.w    #$FFFE,d2
                    bne      lbC005B80
                    cmp.w    #$32,d3
                    beq      MOVE0_2
                    tst.w    d0
                    bne      lbC005B64
                    tst.w    S1AUTO
                    beq      lbC005BC4
                    bra      MOVE0_7

lbC005B64:          tst.w    S2AUTO
                    beq      lbC005BC4
                    bra      MOVE0_7

                    move.w   #4,SPYDIR
                    move.w   #2,(a4)
                    rts

lbC005B80:          cmp.w    #2,d2
                    bne      lbC005BC4
                    cmp.w    #$31,d3
                    beq      MOVE0_2
                    tst.w    d0
                    bne      lbC005BA8
                    tst.w    S1AUTO
                    beq      lbC005BC4
                    bra      MOVE0_7

                    bra      lbC005BB6

lbC005BA8:          tst.w    S2AUTO
                    beq      lbC005BC4
                    bra      MOVE0_7

lbC005BB6:          move.w   #3,SPYDIR
                    move.w   #1,(a4)
                    rts

lbC005BC4:          move.w   #0,SPYDIR
                    move.w   #0,(a4)
                    rts

MOVE0_2:            move.w   MAXMAPX,d5
                    asl.w    #2,d5
                    move.w   MAXMAPY,d6
                    asl.w    #2,d6
                    move.l   #0,d3
                    move.w   SPYY,d3
                    add.w    d2,d3
                    lsr.w    #2,d3
                    mulu     MAXMAPX,d3
                    move.w   SPYX,d4
                    add.w    d1,d4
                    lsr.w    #2,d4
                    add.w    d4,d3
                    lsl.w    #1,d3
                    add.l    #MAP,d3
                    move.l   d3,a0
                    cmp.w    #$5D,d4
                    bhi      MOVE0_2_0
                    cmp.w    #1,d4
                    bls      MOVE0_2_0
                    move.w   (a0),d4
                    btst     #0,d4
                    beq      MOVE1_1
                    tst.w    d0
                    bne      lbC005CB4
                    tst.w    S1IGLOO
                    bne      lbC005C4E
                    move.w   S1AUTO,d3
                    beq      lbC005C4E
                    cmp.w    #$32,S1ENERGY
                    bgt      MOVE0_2_0
lbC005C4E:          lea      S1SAVE_MAPX,a1
                    lea      S1SAVE_MAPY,a2
                    lea      S1SAVE_WINX,a3
                    lea      S1SAVE_WINY,a4
                    lea      S1IGLOO,a5
                    lea      S1HAND,a6
                    tst.w    S2IGLOO
                    beq      lbC005D36
                    move.w   S2SAVE_MAPX,d5
                    move.w   SPYX,d7
                    and.w    #$FFFC,d7
                    and.w    #$FFFC,d5
                    cmp.w    d5,d7
                    bne      lbC005D36
                    move.w   S2SAVE_MAPY,d5
                    move.w   SPYY,d7
                    and.w    #$FFF0,d7
                    and.w    #$FFF0,d5
                    cmp.w    d5,d7
                    beq      MOVE0_2_0
                    bra      lbC005D36

lbC005CB4:          tst.w    S2IGLOO
                    bne      lbC005CD4
                    move.w   S2AUTO,d3
                    beq      lbC005CD4
                    cmp.w    #$32,S2ENERGY
                    bgt      MOVE0_2_0
lbC005CD4:          lea      S2SAVE_MAPX,a1
                    lea      S2SAVE_MAPY,a2
                    lea      S2SAVE_WINX,a3
                    lea      S2SAVE_WINY,a4
                    lea      S2IGLOO,a5
                    lea      S2HAND,a6
                    tst.w    S1IGLOO
                    beq      lbC005D36
                    move.w   S1SAVE_MAPX,d5
                    move.w   SPYX,d7
                    and.w    #$FFFC,d7
                    and.w    #$FFFC,d5
                    cmp.w    d5,d7
                    bne      lbC005D36
                    move.w   S1SAVE_MAPY,d5
                    move.w   SPYY,d7
                    and.w    #$FFF0,d7
                    and.w    #$FFF0,d5
                    cmp.w    d5,d7
                    beq      MOVE0_2_0
lbC005D36:          cmp.w    #$C9,(a0)
                    bne      lbC005D9E
                    cmp.w    #2,d1
                    bne      MOVE0_2_0
                    tst.w    (a6)
                    beq      lbC005D5E
                    tst.w    d3
                    beq      MOVE0_2_0
                    movem.l  d0-d7/a0-a6,-(sp)
                    bsr      BUSY4_A
                    movem.l  (sp)+,d0-d7/a0-a6
lbC005D5E:          move.w   SPYX,(a1)
                    move.w   SPYY,(a2)
                    move.w   SPYWX,(a3)
                    move.w   SPYWY,(a4)
                    move.w   #1,(a5)
                    move.w   #$158,SPYX
                    move.w   #$140,SPYWX
                    move.w   #12,SPYY
                    move.w   #0,SPYWY
                    bra      lbC005E28

lbC005D9E:          cmp.w    #$121,(a0)
                    bne      lbC005E06
                    cmp.w    #$FFFE,d1
                    bne      MOVE0_2_0
                    tst.w    (a6)
                    beq      lbC005DC6
                    tst.w    d3
                    beq      MOVE0_2_0
                    movem.l  d0-d7/a0-a6,-(sp)
                    bsr      BUSY4_A
                    movem.l  (sp)+,d0-d7/a0-a6
lbC005DC6:          move.w   SPYX,(a1)
                    move.w   SPYY,(a2)
                    move.w   SPYWX,(a3)
                    move.w   SPYWY,(a4)
                    move.w   #1,(a5)
                    move.w   #$114,SPYX
                    move.w   #$100,SPYWX
                    move.w   #12,SPYY
                    move.w   #0,SPYWY
                    bra      lbC005E28

lbC005E06:          cmp.w    #$F9,(a0)
                    bne      MOVE0_2_0
                    move.w   (a1),SPYX
                    move.w   (a2),SPYY
                    move.w   (a3),SPYWX
                    move.w   (a4),SPYWY
                    clr.w    (a5)
lbC005E28:          tst.w    d0
                    bne      lbC005E40
                    move.w   #$FFFF,BUF1Y
                    clr.w    S1NUDGE
                    bra      MOVE0_2_0

lbC005E40:          move.w   #$FFFF,BUF2Y
                    clr.w    S2NUDGE
MOVE0_2_0:          tst.w    d0
                    bne      lbC005E80
                    move.w   SPYX,S1MAPX
                    move.w   SPYY,S1MAPY
                    move.w   SPYWX,WIN1X
                    move.w   SPYWY,WIN1Y
                    bra      lbC005EA8

lbC005E80:          move.w   SPYX,S2MAPX
                    move.w   SPYY,S2MAPY
                    move.w   SPYWX,WIN2X
                    move.w   SPYWY,WIN2Y
lbC005EA8:          cmp.w    #$FFFE,d2
                    bne      MOVE0_2_0_A
                    move.w   (a0),d5
                    and.w    #$1F0,d5
                    cmp.w    #$1B0,d5
                    beq      lbC005EC6
                    cmp.w    #$1C0,d5
                    bne      MOVE0_2_0_A
lbC005EC6:          tst.w    d0
                    bne      lbC005EE6
                    lea      S1ENERGY,a1
                    move.l   #BUF13,S1FADDR
                    clr.w    S1F
                    bra      lbC005EFC

lbC005EE6:          move.l   #BUF23,S2FADDR
                    clr.w    S2F
                    lea      S2ENERGY,a1
lbC005EFC:          addq.w   #1,(a1)
                    cmp.w    #$3F,(a1)
                    ble      _MOVE0_9
                    move.w   #$3F,(a1)
_MOVE0_9:           bra      MOVE0_9

MOVE0_2_0_A:        tst.w    d0
                    bne      lbC005F36
                    move.l   #S1HAND,a1
                    move.l   #S1FUEL,a2
                    move.l   #S1BUMP,a3
                    move.l   #S1BDIR,a4
                    move.w   S1MAPY,d5
                    bra      lbC005F54

lbC005F36:          move.l   #S2HAND,a1
                    move.l   #S2FUEL,a2
                    move.l   #S2BUMP,a3
                    move.l   #S2BDIR,a4
                    move.w   S2MAPY,d5
lbC005F54:          move.w   (a0),d4
MOVE0_7:            tst.w    d0
                    bne      lbC005F6C
                    lea      S1BUMP,a3
                    lea      S1BDIR,a4
                    bra      lbC005F78

lbC005F6C:          lea      S2BUMP,a3
                    lea      S2BDIR,a4
lbC005F78:          move.w   (a4),d1
MOVE0_7_5:          and.w    #3,d1
                    bne      lbC005FA2
                    move.w   #1,d4
                    move.w   #3,d2
                    bsr      RNDER2
                    and.w    #1,d1
                    bne      lbC005FBE
                    move.w   #2,d4
                    move.w   #4,d2
                    bra      lbC005FBE

lbC005FA2:          move.w   #4,d4
                    move.w   #1,d2
                    bsr      RNDER2
                    and.w    #1,d1
                    bne      lbC005FBE
                    move.w   #8,d4
                    move.w   #2,d2
lbC005FBE:          move.w   d4,(a4)
                    move.w   d2,SPYDIR
                    bsr      RNDER2
                    and.w    #7,d1
                    tst.w    d0
                    bne      lbC005FDE
                    move.w   d1,S1NUDGE
                    bra      _MOVE0_8

lbC005FDE:          move.w   d1,S2NUDGE
_MOVE0_8:           bra      MOVE0_8

                    clr.w    (a4)
MOVE0_8:            move.w   #0,(a3)
MOVE0_9:            move.w   #0,SPYDIR
                    move.w   #0,d1
                    move.w   #0,d2
                    rts

MOVE1_1:            move.w   IQ,d3
                    tst.w    d0
                    bne      lbC006030
                    move.l   #S1BDIR,a5
                    move.w   S1WIN,d7
                    tst.w    S1AUTO
                    beq      lbC0060E4
                    tst.w    S1SAFE
                    bne      lbC0060E4
                    bra      lbC006050

lbC006030:          move.l   #S2BDIR,a5
                    move.w   S2WIN,d7
                    tst.w    S2AUTO
                    beq      lbC0060E4
                    tst.w    S2SAFE
                    bne      lbC0060E4
lbC006050:          cmp.w    #3,d7
                    beq      lbC006068
                    cmp.w    #$130,d4
                    bne      lbC006068
                    move.w   #5,d3
                    bra      lbC006078

lbC006068:          cmp.w    #$80,d4
                    blt      lbC0060E4
                    cmp.w    #$8F,d4
                    bgt      lbC0060E4
lbC006078:          movem.l  d0-d3/a0-a3,-(sp)
                    bsr      RNDER
                    and.w    #15,d0
                    addq.w   #1,d0
                    move.w   d0,d7
                    movem.l  (sp)+,d0-d3/a0-a3
                    lsl.w    #1,d3
                    addq.w   #5,d3
                    cmp.w    d3,d7
                    bge      lbC00609E
                    move.w   #0,(a5)
                    bra      MOVE0_2_0

lbC00609E:          tst.w    d0
                    bne      lbC0060BE
                    tst.w    S1AUTO
                    beq      lbC0060E4
                    cmp.w    #3,S1WIN
                    beq      lbC0060D8
                    bra      lbC0060E4

lbC0060BE:          tst.w    S2AUTO
                    beq      lbC0060E4
                    cmp.w    #3,S2WIN
                    beq      lbC0060D8
                    bra      lbC0060E4

lbC0060D8:          movem.l  d0-d7/a0-a6,-(sp)
                    bsr      BUSY4_A
                    movem.l  (sp)+,d0-d7/a0-a6
lbC0060E4:          movem.l  d4-d6,-(sp)
                    tst.w    d0
                    bne      MOVE1_1_0
                    move.l   #S1DEPTH,a1
                    move.l   #S1ENERGY,a2
                    move.l   #S1WATERCT,a3
                    move.l   #S1SWAMP,a4
                    move.l   #S1DROWN,a5
                    move.l   #S1RUN,a6
                    move.w   S1HAND,d6
                    tst.w    S1DEAD
                    bne      MOVE1_1_1_9
                    bra      MOVE1_1_0_1

MOVE1_1_0:          move.l   #S2DEPTH,a1
                    move.l   #S2ENERGY,a2
                    move.l   #S2WATERCT,a3
                    move.l   #S2SWAMP,a4
                    move.l   #S2DROWN,a5
                    move.l   #S2RUN,a6
                    move.w   S2HAND,d6
                    tst.w    S2DEAD
                    bne      MOVE1_1_1_9
MOVE1_1_0_1:        move.w   (a0),d4
                    move.w   #0,(a5)
                    tst.w    d0
                    bne      lbC0061BE
                    move.l   a0,S1DIG
                    clr.w    S1TNTREADY
                    cmp.w    #$7E,d4
                    bne      MOVE1_1_0_1_5
                    cmp.w    #$58,S2HAND
                    beq      lbC006190
                    tst.w    S2PLUNGER
                    beq      lbC006198
lbC006190:          move.w   #$FFFF,S1TNTREADY
lbC006198:          move.w   S2CT,d7
                    and.w    #$FF00,d7
                    cmp.w    #$5800,d7
                    bne      MOVE1_1_0_1_5
                    bsr      DELETE_TRAP
                    move.w   #$5900,d3
                    bsr      SETSTATE
                    movem.l  (sp)+,d4-d6
                    bra      MOVE1_1_Z

lbC0061BE:          clr.w    S2TNTREADY
                    move.l   a0,S2DIG
                    cmp.w    #$76,d4
                    bne      MOVE1_1_0_1_5
                    cmp.w    #$60,S1HAND
                    beq      lbC0061E8
                    tst.w    S1PLUNGER
                    beq      lbC0061F0
lbC0061E8:          move.w   #$FFFF,S2TNTREADY
lbC0061F0:          move.w   S1CT,d7
                    and.w    #$FF00,d7
                    cmp.w    #$5800,d7
                    bne      MOVE1_1_0_1_5
                    move.w   #$5900,d3
                    bsr      SETSTATE
                    movem.l  (sp)+,d4-d6
                    bra      MOVE1_1_Z

MOVE1_1_0_1_5:      cmp.w    #$A8,d4
                    beq      lbC006222
                    cmp.w    #$B0,d4
                    bne      lbC00626A
lbC006222:          move.w   #1,(a5)
                    clr.w    (a6)
                    move.w   #$17,d4
                    cmp.w    (a1),d4
                    beq      lbC00624E
                    blt      lbC00624C
                    tst.w    (a1)
                    bne      lbC006246
                    move.l   #5,d7
                    bsr      NEW_SOUND
lbC006246:          addq.w   #1,(a1)
                    bra      lbC00624E

lbC00624C:          subq.w   #1,(a1)
lbC00624E:          move.w   COUNTER,d3
                    and.w    #7,d3
                    bne      lbC00625E
                    subq.w   #1,(a2)
lbC00625E:          move.w   #2,REFRESH
                    bra      lbC0062E8

lbC00626A:          tst.w    (a1)
                    beq      lbC0062E8
                    move.w   #$1500,d3
                    bsr      SETSTATE
                    tst.w    d0
                    bne      lbC0062B0
                    move.l   #BUF1D,S1FADDR
                    move.w   #0,S1F
                    cmp.w    #$A8,2(a0)
                    beq      lbC0062A4
                    cmp.w    #$B0,2(a0)
                    bne      lbC0062DE
lbC0062A4:          move.w   #1,S1F
                    bra      lbC0062DE

lbC0062B0:          move.l   #BUF2D,S2FADDR
                    move.w   #0,S2F
                    cmp.w    #$A8,2(a0)
                    beq      lbC0062D6
                    cmp.w    #$B0,2(a0)
                    bne      lbC0062DE
lbC0062D6:          move.w   #1,S2F
lbC0062DE:          move.w   #0,(a3)
                    movem.l  (sp)+,d4-d6
                    rts

lbC0062E8:          cmp.w    #$48,d6
                    beq      lbC0063EE
                    cmp.w    #$C0,(a0)
                    beq      lbC006310
                    cmp.w    #$80,(a0)
                    beq      lbC006330
                    cmp.w    #$1F0,(a0)
                    blt      lbC00636A
                    cmp.w    #$1FF,(a0)
                    bgt      lbC00636A
lbC006310:          move.l   d1,-(sp)
                    bsr      RNDER2
                    move.w   d1,d7
                    move.l   (sp)+,d1
                    and.w    #15,d7
                    cmp.w    #7,d7
                    bne      lbC00636A
                    tst.w    d0
                    beq      lbC006340
                    bra      lbC006354

lbC006330:          tst.w    d0
                    bne      lbC00634A
                    tst.w    S1SAFE
                    bne      lbC00636A
lbC006340:          move.l   a0,S1DIG
                    bra      lbC00635A

lbC00634A:          tst.w    S2SAFE
                    bne      lbC00636A
lbC006354:          move.l   a0,S2DIG
lbC00635A:          move.w   #$5400,d3
                    bsr      SETSTATE
                    movem.l  (sp)+,d4-d6
                    bra      MOVE1_1_Z

lbC00636A:          cmp.w    #$88,(a0)
                    bne      lbC0063AC
                    tst.w    d0
                    bne      lbC00638C
                    tst.w    S1SAFE
                    bne      lbC0063AC
                    move.l   a0,S1DIG
                    bra      lbC00639C

lbC00638C:          tst.w    S2SAFE
                    bne      lbC0063AC
                    move.l   a0,S2DIG
lbC00639C:          move.w   #$5100,d3
                    bsr      SETSTATE
                    movem.l  (sp)+,d4-d6
                    bra      MOVE1_1_Z

lbC0063AC:          cmp.w    #$130,(a0)
                    bne      lbC0063EE
                    tst.w    d0
                    bne      lbC0063CE
                    tst.w    S1SAFE
                    bne      lbC0063EE
                    move.l   a0,S1DIG
                    bra      lbC0063DE

lbC0063CE:          tst.w    S2SAFE
                    bne      lbC0063EE
                    move.l   a0,S2DIG
lbC0063DE:          move.w   #$5700,d3
                    bsr      SETSTATE
                    movem.l  (sp)+,d4-d6
                    bra      MOVE1_1_Z

lbC0063EE:          cmp.w    #$98,(a0)
                    bne      lbC006430
                    tst.w    d0
                    bne      lbC006410
                    tst.w    S1SAFE
                    bne      lbC006430
                    move.l   a0,S1DIG
                    bra      lbC006420

lbC006410:          tst.w    S2SAFE
                    bne      lbC006430
                    move.l   a0,S2DIG
lbC006420:          move.w   #$5600,d3
                    bsr      SETSTATE
                    movem.l  (sp)+,d4-d6
                    bra      MOVE1_1_Z

lbC006430:          clr.w    (a4)
                    cmp.w    #$48,d6
                    beq      MOVE1_1_1_9
                    cmp.w    #$B8,(a0)
                    beq      MOVE1_1_1_0
                    cmp.w    #$1D0,(a0)
                    blt      MOVE1_1_1_9
                    cmp.w    #$1EF,(a0)
                    bgt      MOVE1_1_1_9
MOVE1_1_1_0:        move.w   #5,(a4)
                    move.w   COUNTER,d4
                    tst.w    (a2)
                    beq      MOVE1_1_1_9
                    subq.w   #1,(a2)
MOVE1_1_1_9:        movem.l  (sp)+,d4-d6
                    tst.w    d0
                    bne      MOVE1_1_A_0
                    tst.w    S1SWAMP
                    bne      MOVE1_1_Z
                    tst.w    S1DEPTH
                    bne      MOVE1_1_Z
                    tst.w    S1SAFE
                    bne      MOVE1_1_A_3
                    bra      MOVE1_1_A_5

MOVE1_1_A_0:        tst.w    S2SWAMP
                    bne      MOVE1_1_Z
                    tst.w    S2DEPTH
                    bne      MOVE1_1_Z
                    tst.w    S2SAFE
                    bne      MOVE1_1_A_3
                    bra      MOVE1_1_A_5

MOVE1_1_A_3:        move.w   d1,d3
                    add.w    d2,d3
                    beq      MOVE1_1_Z
                    tst.w    d0
                    bne      MOVE1_1_A_4
                    subq.w   #1,S1SAFE
                    bra      MOVE1_1_Z

MOVE1_1_A_4:        subq.w   #1,S2SAFE
                    bra      MOVE1_1_Z

MOVE1_1_A_5:        move.w   (a0),d4
                    and.w    #$FFF8,d4
MOVE1_1_Z:          add.w    d1,SPYX
                    add.w    d2,SPYY
                    bmi      MOVEABORT
                    tst.w    d0
                    bne      MOVE1_1_Z_3
                    tst.w    S1IGLOO
                    bne      MOVE1_1_2
                    movem.l  d0/d1/a0,-(sp)
                    move.w   SPYX,d0
                    lsr.w    #2,d0
                    divu     #10,d0
                    lsl.w    #8,d0
                    move.w   SPYY,d1
                    lsr.w    #4,d1
                    or.w     d1,d0
                    cmp.w    S1TRAIL,d0
                    beq      lbC006540
                    move.l   #S1TRAIL,a0
                    move.l   #8,d1
                    add.l    d1,a0
lbC00652E:          move.w   (a0),2(a0)
                    sub.w    #2,a0
                    sub.w    #2,d1
                    bge.s    lbC00652E
                    move.w   d0,2(a0)
lbC006540:          movem.l  (sp)+,d0/d1/a0
                    tst.w    S2DEAD
                    bne      MOVE1_1_2
                    move.w   SPYX,d3
                    sub.w    S2MAPX,d3
                    bge      MOVE1_1_Z_1
                    neg.w    d3
MOVE1_1_Z_1:        cmp.w    #4,d3
                    bge      MOVE1_1_2
                    move.w   SPYY,d3
                    move.w   S2MAPY,d4
                    lsr.w    #4,d3
                    lsr.w    #4,d4
                    cmp.w    d3,d4
                    bne      MOVE1_1_2
                    move.w   SPYY,d3
                    sub.w    S2MAPY,d3
                    bge      MOVE1_1_Z_2
                    neg.w    d3
MOVE1_1_Z_2:        cmp.w    #3,d3
                    bmi      MOVEABORT
                    bra      MOVE1_1_2

MOVE1_1_Z_3:        tst.w    S2IGLOO
                    bne      MOVE1_1_2
                    movem.l  d0/d1/a0,-(sp)
                    move.w   SPYX,d0
                    lsr.w    #6,d0
                    lsl.w    #8,d0
                    move.w   SPYY,d1
                    lsr.w    #4,d1
                    or.w     d1,d0
                    cmp.w    S2TRAIL,d0
                    beq      lbC0065E8
                    move.l   #S2TRAIL,a0
                    move.l   #8,d1
                    add.l    d1,a0
lbC0065D6:          move.w   (a0),2(a0)
                    sub.w    #2,a0
                    sub.w    #2,d1
                    bge.s    lbC0065D6
                    move.w   d0,2(a0)
lbC0065E8:          movem.l  (sp)+,d0/d1/a0
                    tst.w    S1DEAD
                    bne      MOVE1_1_2
                    move.w   SPYX,d3
                    sub.w    S1MAPX,d3
                    bge      MOVE1_1_Z_4
                    neg.w    d3
MOVE1_1_Z_4:        cmp.w    #4,d3
                    bge      MOVE1_1_2
                    move.w   SPYY,d3
                    move.w   S1MAPY,d4
                    lsr.w    #4,d3
                    lsr.w    #4,d4
                    cmp.w    d3,d4
                    bne      MOVE1_1_2
                    move.w   SPYY,d3
                    sub.w    S1MAPY,d3
                    bge      MOVE1_1_Z_5
                    neg.w    d3
MOVE1_1_Z_5:        cmp.w    #3,d3
                    bmi      MOVEABORT
MOVE1_1_2:          tst.w    SPYX
                    bmi      MOVEABORT
                    tst.w    d1
                    bne      lbC006656
                    tst.w    d2
                    beq      _CHECKMEET1
lbC006656:          tst.w    FEET_FLAG
                    bne      _CHECKMEET1
                    addq.w   #1,FEET_FLAG
                    addq.w   #1,FEET_COUNT
                    move.w   FEET_COUNT,d7
                    and.w    #1,d7
                    bne      _CHECKMEET1
                    move.l   #11,d7
                    bsr      NEW_SOUND
_CHECKMEET1:        bsr      CHECKMEET
                    move.w   SPYWIN,d3
                    tst.w    d3
                    beq      MOVE1_1_5
                    sub.w    #1,d3
                    cmp.w    d0,d3
                    bne      MOVEEND
MOVE1_1_5:          tst.w    d1
                    beq      MOVE2_0
                    move.w   SPYX,d3
                    sub.w    #4,d3
                    sub.w    SPYWX,d3
                    bgt      MOVE1_2
                    move.w   SPYX,d3
                    cmp.w    #4,d3
                    ble      MOVEABORT
                    subq.w   #2,SPYWX
                    rts

MOVE1_2:            move.w   SPYX,d3
                    sub.w    SPYWX,d3
                    sub.w    #$24,d3
                    blt      MOVEEND
                    addq.w   #2,SPYWX
MOVEEND:            rts

MOVE2_0:            move.w   SPYY,d3
                    sub.w    SPYWY,d3
                    bge      MOVE2_2
                    move.w   SPYWY,d3
                    ble      MOVEABORT
                    move.w   #$10,d3
                    sub.w    d3,SPYWY
                    rts

MOVE2_2:            move.w   SPYY,d3
                    sub.w    SPYWY,d3
                    sub.w    #$10,d3
                    blt.s    MOVEEND
                    move.w   SPYY,d3
                    sub.w    d6,d3
                    bge      MOVEABORT
                    move.w   #$10,d3
                    add.w    d3,SPYWY
                    rts

MOVEABORT:          tst.w    d0
                    bne      lbC006756
                    move.l   #S1BDIR,a4
                    clr.w    S1BUMP
                    clr.w    S1BDIR
                    bra      lbC006768

lbC006756:          move.l   #S2BDIR,a4
                    clr.w    S2BUMP
                    clr.w    S2BDIR
lbC006768:          sub.w    d1,SPYX
                    sub.w    d2,SPYY
                    bra      MOVE0_7

CHECKMEET:          movem.l  d0-d7/a0-a6,-(sp)
                    move.w   d1,d4
                    move.w   d2,d5
                    move.w   S1MAPY,d1
                    move.w   S2MAPY,d2
                    and.w    #$FFF0,d1
                    and.w    #$FFF0,d2
                    move.w   d1,WIN1Y
                    move.w   d2,WIN2Y
                    cmp.w    d1,d2
                    bne      SEPARATE
                    tst.w    S1IGLOO
                    bne      SEPARATE
                    tst.w    S2IGLOO
                    bne      SEPARATE
                    tst.w    SPYWIN
                    beq      lbC0067D4
                    move.w   SPYWIN,d1
                    sub.w    #1,d1
                    cmp.w    d0,d1
                    beq      CHECKMEETBYE
lbC0067D4:          tst.w    d0
                    bne      lbC00681A
                    move.w   WIN2X,d2
                    add.w    #4,d2
                    move.w   S1MAPX,d1
                    cmp.w    d2,d1
                    blt      SEPARATE
                    add.w    #$20,d2
                    cmp.w    d2,d1
                    bge      SEPARATE
                    move.w   WIN2Y,SPYWY
                    move.w   WIN2X,SPYWX
                    move.w   #2,SPYWIN
                    bra      FORCE_DRAW

lbC00681A:          move.w   WIN1X,d2
                    add.w    #4,d2
                    move.w   S2MAPX,d1
                    cmp.w    d2,d1
                    blt      SEPARATE
                    add.w    #$20,d2
                    cmp.w    d2,d1
                    bge      SEPARATE
                    move.w   WIN1Y,SPYWY
                    move.w   WIN1X,SPYWX
                    move.w   #1,SPYWIN
FORCE_DRAW:         move.w   #$FFFF,BUF1Y
                    move.w   #$FFFF,BUF2Y
                    bra      CHECKMEETBYE

                    movem.l  d0-d7/a0-a6,-(sp)
                    move.w   S1MAPY,d1
                    move.w   S2MAPY,d2
                    and.w    #$F0,d1
                    and.w    #$F0,d2
                    move.w   d1,WIN1Y
                    move.w   d2,WIN2Y
                    cmp.w    d1,d2
                    bne      SEPARATE
                    move.w   S1MAPX,d1
                    sub.w    S2MAPX,d1
                    bge      lbC0068A6
                    neg.w    d1
lbC0068A6:          cmp.w    #$20,d1
                    bgt      SEPARATE
                    tst.w    SPYWIN
                    bne      CHECKMEET1
                    tst.w    d0
                    bne      lbC0068D4
                    move.w   S1CT,d1
                    move.w   WIN1X,d2
                    move.w   WIN1Y,d3
                    bra      lbC0068E6

lbC0068D4:          move.w   S2CT,d1
                    move.w   WIN2X,d2
                    move.w   WIN2Y,d3
lbC0068E6:          move.w   #2,SPYWIN
                    sub.w    d0,SPYWIN
                    move.w   d0,SPYWIN
                    add.w    #1,SPYWIN
                    move.w   d2,SPYWX
                    move.w   d3,SPYWY
CHECKMEET1:         moveq    #0,d2
lbC006910:          move.w   S1MAPX,d1
                    sub.w    SPYWX,d1
                    sub.w    #4,d1
                    bgt      lbC00693E
                    cmp.w    #1,SPYWIN
                    beq      SEPARATE
                    add.w    #1,d2
                    sub.w    #2,SPYWX
                    bra.s    lbC006910

lbC00693E:          move.w   S1MAPX,d1
                    sub.w    SPYWX,d1
                    sub.w    #$20,d1
                    blt      lbC00696C
                    cmp.w    #1,SPYWIN
                    beq      SEPARATE
                    add.w    #1,d2
                    add.w    #2,SPYWX
                    bra.s    lbC00693E

lbC00696C:          move.w   S2MAPX,d1
                    sub.w    SPYWX,d1
                    sub.w    #4,d1
                    bgt      lbC00699A
                    add.w    #1,d2
                    cmp.w    #2,SPYWIN
                    beq      SEPARATE
                    sub.w    #2,SPYWX
                    bra.s    lbC00696C

lbC00699A:          move.w   S2MAPX,d1
                    sub.w    SPYWX,d1
                    sub.w    #$20,d1
                    blt      CHECKMEETEND
                    cmp.w    #2,SPYWIN
                    beq      SEPARATE
                    add.w    #1,d2
                    add.w    #2,SPYWX
                    bra.s    lbC00699A

CHECKMEETEND:       cmp.w    #1,d2
                    ble      CHECKMEETBYE
                    move.w   #$FFFF,BUF1Y
                    move.w   #$FFFF,BUF2Y
CHECKMEETBYE:       movem.l  (sp)+,d0-d7/a0-a6
                    rts

SEPARATE:           tst.w    SPYWIN
                    beq.s    CHECKMEETBYE
                    move.w   #$FFFF,BUF1Y
                    move.w   #$FFFF,BUF2Y
                    move.w   SPYWIN,d1
                    clr.w    SPYWIN
                    cmp.w    #2,d1
                    bne      lbC006A32
                    move.w   S1MAPX,d1
                    sub.w    #$15,d1
                    cmp.w    #2,d1
                    bgt      lbC006A28
                    move.w   #2,d1
lbC006A28:          move.w   d1,WIN1X
                    bra      _SETTEMPS

lbC006A32:          move.w   S2MAPX,d1
                    sub.w    #$15,d1
                    cmp.w    #2,d1
                    bgt      lbC006A48
                    move.w   #2,d1
lbC006A48:          move.w   d1,WIN2X
_SETTEMPS:          bsr      SETTEMPS
                    movem.l  (sp)+,d0-d7/a0-a6
                    rts

SETTEMPS:           tst.w    d0
                    bne      lbC006A8A
                    move.w   WIN1X,SPYWX
                    move.w   WIN1Y,SPYWY
                    move.w   S1MAPX,SPYX
                    move.w   S1MAPY,SPYY
                    bra      lbC006AB2

lbC006A8A:          move.w   WIN2X,SPYWX
                    move.w   WIN2Y,SPYWY
                    move.w   S2MAPX,SPYX
                    move.w   S2MAPY,SPYY
lbC006AB2:          rts

DIRFIX:             movem.l  d1/d2,-(sp)
                    move.w   SPYDIR,d2
                    tst.w    d0
                    bne      BODY2_0
                    cmp.w    #2,d2
                    bne      BODY1_1
                    move.l   #BUF11,S1FADDR
                    bra      BODY1_20

BODY1_1:            cmp.w    #1,d2
                    bne      BODY1_2
                    move.l   #BUF12,S1FADDR
                    bra      BODY1_20

BODY1_2:            cmp.w    #3,d2
                    bne      BODY1_3
                    move.l   #BUF13,S1FADDR
                    bra      BODY1_20

BODY1_3:            cmp.w    #4,d2
                    bne      BODY1_80
                    move.l   #BUF14,S1FADDR
BODY1_20:           move.w   S1F,d1
                    add.w    #1,d1
                    cmp.w    #4,d1
                    bne      BODY1_20_1
                    move.w   #0,d1
BODY1_20_1:         move.w   d1,S1F
                    bra      BODY1_99

BODY1_80:           move.w   #0,S1F
BODY1_99:           movem.l  (sp)+,d1/d2
                    rts

BODY2_0:            cmp.w    #2,d2
                    bne      BODY2_1
                    move.l   #BUF21,S2FADDR
                    bra      BODY2_20

BODY2_1:            cmp.w    #1,d2
                    bne      BODY2_2
                    move.l   #BUF22,S2FADDR
                    bra      BODY2_20

BODY2_2:            cmp.w    #3,d2
                    bne      BODY2_3
                    move.l   #BUF23,S2FADDR
                    bra      BODY2_20

BODY2_3:            cmp.w    #4,d2
                    bne      BODY2_80
                    move.l   #BUF24,S2FADDR
BODY2_20:           move.w   S2F,d1
                    add.w    #1,d1
                    cmp.w    #4,d1
                    bne      BODY2_20_1
                    move.w   #0,d1
BODY2_20_1:         move.w   d1,S2F
                    bra      BODY2_99

BODY2_80:           move.w   #0,S2F
BODY2_99:           movem.l  (sp)+,d1/d2
                    rts

TEST_OPTIONS:       tst.w    SPECIAL_DELAY
                    beq      _TEST_M
                    subq.w   #1,SPECIAL_DELAY
                    bra      lbC006BE0

_TEST_M:            bsr      TEST_M
lbC006BE0:          rts

TEST_M:             move.w   #$37,d0
                    bsr      INKEY
                    beq      lbC006C4C
                    move.w   #$14,SPECIAL_DELAY
                    tst.w    MUSIC_SWITCH
                    bne      lbC006C22
                    move.w   #$FFFF,lbW001F2E
                    move.w   #$FFFF,lbW001F48
                    move.w   #$FFFF,lbW001F62
                    move.w   #1,MUSIC_SWITCH
                    rts

lbC006C22:          clr.w    MUSIC_SWITCH
                    clr.w    lbW001F2E
                    clr.w    lbW001F48
                    clr.w    lbW001F62
                    clr.w    $DFF0A8
                    clr.w    $DFF0B8
                    clr.w    $DFF0C8
lbC006C4C:          rts

JOYMOVE:            movem.l  d0/d7/a0,-(sp)
                    move.w   d0,d7
                    bsr      SCAN_JOY
                    bsr      TEST_OPTIONS
                    move.w   #$19,d0
                    bsr      INKEY
                    beq      JM2
_LITTLE_DELAY:      bsr      LITTLE_DELAY
                    move.w   #$19,d0
                    bsr      INKEY
                    bne.s    _LITTLE_DELAY
_TEST_OPTIONS:      bsr      TEST_OPTIONS
                    bsr      LITTLE_DELAY
                    move.w   #$19,d0
                    bsr      INKEY
                    beq.s    _TEST_OPTIONS
_LITTLE_DELAY0:     bsr      LITTLE_DELAY
                    move.w   #$19,d0
                    bsr      INKEY
                    bne.s    _LITTLE_DELAY0
JM2:                move.w   #$45,d0
                    bsr      INKEY
                    beq      JM3
lbC006CA2:          move.w   #$45,d0
                    bsr      INKEY
                    bne.s    lbC006CA2
                    move.w   #1,ABORT
JM3:                move.w   d7,d0
                    tst.w    BRAINON
                    bne      lbC006D58
                    moveq    #0,d1
                    moveq    #0,d2
                    clr.w    SPYDIR
                    tst.w    d0
                    bne      lbC006CE4
                    tst.w    S1DEAD
                    bne      lbC006D58
                    move.w   S1MODE,d7
                    bra      lbC006CF4

lbC006CE4:          tst.w    S2DEAD
                    bne      lbC006D58
                    move.w   S2MODE,d7
lbC006CF4:          lsl.w    #2,d7
                    lea      JOY_TABLE,a0
                    move.l   0(a0,d7.w),a0
                    tst.b    (a0)+
                    beq      lbC006D18
                    move.w   #$FFFF,d1
                    moveq    #0,d2
                    move.w   #1,SPYDIR
                    bra      lbC006D58

lbC006D18:          tst.b    (a0)+
                    beq      lbC006D30
                    move.w   #1,d1
                    moveq    #0,d2
                    move.w   #2,SPYDIR
                    bra      lbC006D58

lbC006D30:          tst.b    (a0)+
                    beq      lbC006D46
                    moveq    #0,d1
                    moveq    #-1,d2
                    move.w   #3,SPYDIR
                    bra      lbC006D58

lbC006D46:          tst.b    (a0)+
                    beq      lbC006D58
                    moveq    #0,d1
                    moveq    #1,d2
                    move.w   #4,SPYDIR
lbC006D58:          clr.w    BRAINON
                    movem.l  (sp)+,d0/d7/a0
                    rts

JOY_TABLE:          dc.l     LEFT1
                    dc.l     MLEFT0
                    dc.l     KLEFT1
                    dc.l     LEFT0
                    dc.l     MLEFT1

SETWINDOW:          cmp.w    #1,SPYWIN
                    beq      SETWINDOW1
                    cmp.w    #2,SPYWIN
                    beq      SETWINDOW2
                    tst.w    d0
                    bne      SETWINDOW2
SETWINDOW1:         move.w   WIN1X,SPYWX
                    move.w   WIN1Y,SPYWY
                    rts

SETWINDOW2:         move.w   WIN2X,SPYWX
                    move.w   WIN2Y,SPYWY
                    rts

GETWINDOW:          cmp.w    #1,SPYWIN
                    beq      GETWINDOW1
                    cmp.w    #2,SPYWIN
                    beq      GETWINDOW2
                    tst.w    d0
                    bne      GETWINDOW2
GETWINDOW1:         move.w   SPYWX,WIN1X
                    move.w   SPYWY,WIN1Y
                    rts

GETWINDOW2:         move.w   SPYWX,WIN2X
                    move.w   SPYWY,WIN2Y
                    rts

LITTLE_DELAY:       move.w   d0,-(sp)
                    move.w   #$F530,d0
lbC006E12:          dbra     d0,lbC006E12
                    move.w   (sp)+,d0
                    rts

FEET_COUNT:         dc.w     0
FEET_FLAG:          dc.w     0

DRAWMOVE:           moveq    #0,d0
                    moveq    #0,d3
                    bsr      FLASH
                    clr.w    d3
                    clr.w    d0
DRAWMOVE0:          cmp.w    #2,SPYWIN
                    beq      DRAWMOVE0_1
                    bsr      DRAWLAND
                    bsr      DRAWOBJ
NOLAND:             bra      DRAWMOVE1

DRAWMOVE0_1:        move.l   SCREEN2,a0
                    move.w   #$40,d1
                    move.w   #10,d2
                    move.w   #$3E,d7
                    move.w   #$13,d6
                    bsr      RCLEARBLOCK
DRAWMOVE1:          move.w   #1,d0
                    move.w   S2CT,d1
                    beq      DRAWMOVE2
                    and.w    #$FF00,d1
                    cmp.w    #$800,d1
                    bge      DRAWMOVE2
                    cmp.w    #$200,d1
                    beq      DRAWMOVE2
DRAWMOVE2:          cmp.w    #1,SPYWIN
                    beq      DRAWMOVE2_1
                    bsr      DRAWLAND
                    bsr      DRAWOBJ
NOLAND2:            bra      DRAWMOVE3

DRAWMOVE2_1:        move.l   SCREEN2,a0
                    move.w   #$40,d1
                    move.w   #$6F,d2
                    move.w   #$3E,d7
                    move.w   #$13,d6
                    bsr      RCLEARBLOCK
DRAWMOVE3:          bsr      DRAWBUTTONS
                    moveq    #0,d0
                    bsr      DRAWBUTTONS
                    bsr      DRAWTIME
                    tst.w    S1ENERGY
                    bge      DRAWMOVE4
                    move.w   #$FFFF,S1ENERGY
                    tst.w    S1DEAD
                    bne      DRAWMOVE4
                    tst.w    S1CT
                    bne      DRAWMOVE4
                    move.w   #$1400,S1CT
                    tst.w    S1DROWN
                    bne      DRAWMOVE4
DRAWMOVE4:          tst.w    S2ENERGY
                    bge      DRAWMOVE5
                    move.w   #$FFFF,S2ENERGY
                    tst.w    S2DEAD
                    bne      DRAWMOVE5
                    tst.w    S2CT
                    bne      DRAWMOVE5
                    move.w   #$1400,S2CT
                    tst.w    S2DROWN
                    bne      DRAWMOVE5
DRAWMOVE5:          bsr      METER
                    bsr      MOVE_SNOWBALL
                    bsr      RSNOWING
                    bsr      RLINES
                    rts

RLINES:             tst.w    TENMINS
                    bne      lbC006FE0
                    tst.w    ONEMINS
                    bne      lbC006FE0
                    addq.w   #1,SNOW_LINE
                    move.w   SNOW_LINE,d1
                    lsr.w    #5,d1
                    move.l   SCREEN2,a0
                    move.l   a0,a1
                    add.w    #$B70,a0
                    add.w    #$1B38,a1
                    tst.w    d1
                    beq      lbC006FE0
lbC006F78:          move.w   #9,d3
lbC006F7C:          tst.w    S1IGLOO
                    bne      lbC006FA6
                    cmp.w    #2,SPYWIN
                    beq      lbC006FA6
                    clr.w    (a0)+
                    move.w   #$FFFF,7998(a0)
                    move.w   #$FFFF,15998(a0)
                    move.w   #$FFFF,23998(a0)
lbC006FA6:          tst.w    S2IGLOO
                    bne      lbC006FD0
                    cmp.w    #1,SPYWIN
                    beq      lbC006FD0
                    clr.w    (a1)+
                    move.w   #$FFFF,7998(a1)
                    move.w   #$FFFF,15998(a1)
                    move.w   #$FFFF,23998(a1)
lbC006FD0:          dbra     d3,lbC006F7C
                    sub.w    #$3C,a0
                    sub.w    #$3C,a1
                    dbra     d1,lbC006F78
lbC006FE0:          rts

MOVE_SNOWBALL:      clr.l    d5
                    clr.l    d6
                    move.w   S1MAPY,d5
                    lsr.w    #2,d5
                    mulu     MAXMAPX,d5
                    move.w   S1MAPX,d7
                    lsr.w    #2,d7
                    add.w    d7,d5
                    lsl.w    #1,d5
                    add.l    #MAP,d5
                    move.w   S2MAPY,d6
                    lsr.w    #2,d6
                    mulu     MAXMAPX,d6
                    move.w   S2MAPX,d7
                    lsr.w    #2,d7
                    add.w    d7,d6
                    lsl.w    #1,d6
                    add.l    #MAP,d6
                    move.w   #7,d7
                    lea      SNOW_LIST,a0
lbC007030:          move.l   (a0),d1
                    beq      lbC007106
                    move.w   6(a0),d2
                    ext.l    d2
                    add.l    d2,d1
                    move.l   d1,(a0)
                    btst     #0,d1
                    bne      lbC007106
                    subq.w   #2,4(a0)
                    cmp.w    #8,4(a0)
                    blt      lbC007104
                    move.l   d1,a1
                    move.w   (a1),d2
                    and.w    #1,d2
                    bne      lbC007104
                    cmp.w    #$40,4(a0)
                    blt      lbC007106
                    tst.w    8(a0)
                    beq      lbC0070BA
                    move.l   d1,d3
                    sub.l    d5,d3
                    tst.w    6(a0)
                    bpl      lbC00708E
                    cmp.l    #0,d3
                    beq      lbC007098
                    bra      lbC0070BA

lbC00708E:          cmp.l    #0,d3
                    bne      lbC0070BA
lbC007098:          cmp.b    #$50,S1CT
                    beq      lbC0070AE
                    tst.w    S1CT
                    bne      lbC007106
lbC0070AE:          move.w   #$900,S1CT
                    bra      lbC007104

lbC0070BA:          tst.w    8(a0)
                    bne      lbC007106
                    move.l   d1,d3
                    sub.l    d6,d3
                    tst.w    6(a0)
                    bpl      lbC0070DC
                    cmp.l    #0,d3
                    beq      lbC0070E6
                    bra      lbC007106

lbC0070DC:          cmp.l    #0,d3
                    bne      lbC007106
lbC0070E6:          cmp.b    #$50,S2CT
                    beq      lbC0070FC
                    tst.w    S2CT
                    bne      lbC007106
lbC0070FC:          move.w   #$900,S2CT
lbC007104:          clr.l    (a0)
lbC007106:          add.w    #10,a0
                    dbra     d7,lbC007030
                    rts

DRAWLAND:           tst.w    d0
                    bne      DRAWLAND0_0
                    move.l   S1FADDR,d1
                    sub.l    #BUF11,d1
                    move.l   #S1BACK,WHICHBACK
                    move.w   WIN1X,d2
                    move.w   WIN1Y,d3
                    move.w   BUF1X,d4
                    move.w   BUF1Y,d5
                    move.w   #$32,ULX
                    move.w   #10,ULY
                    move.w   d2,BUF1X
                    move.w   d3,BUF1Y
                    bra      DRAWLAND0_1

DRAWLAND0_0:        move.l   S2FADDR,d1
                    sub.l    #BUF21,d1
                    move.l   #S2BACK,WHICHBACK
                    move.w   WIN2X,d2
                    move.w   WIN2Y,d3
                    move.w   BUF2X,d4
                    move.w   BUF2Y,d5
                    move.w   #$32,ULX
                    move.w   #$6F,ULY
                    move.w   d2,BUF2X
                    move.w   d3,BUF2Y
DRAWLAND0_1:        movem.l  d0-d5,-(sp)
                    lsr.w    #4,d3
                    lsl.w    #1,d3
                    subq.w   #1,d2
                    lsr.w    #2,d2
                    lsl.w    #1,d2
                    mulu     #$60,d3
                    add.w    d2,d3
                    move.l   d3,a0
                    add.l    #TERRAIN,a0
                    movem.l  (sp),d0-d5
                    subq.w   #1,d2
                    subq.w   #1,d4
                    lsr.w    #2,d2
                    lsr.w    #4,d3
                    lsr.w    #2,d4
                    lsr.w    #4,d5
                    cmp.w    d3,d5
                    bne      DRAWLAND1_0
                    cmp.w    d2,d4
                    beq      DRAWLAND5_0
DRAWLAND1_0:        move.w   #10,d1
                    moveq    #0,d3
DRAWLAND1_1:        move.w   (a0)+,d4
                    bsr      DRAWSLAB
                    addq.w   #1,d3
                    dbra     d1,DRAWLAND1_1
DRAWLAND5_0:        movem.l  (sp)+,d0-d5
                    lea      $DFF000,a2
                    move.l   WHICHBACK,a1
                    move.l   SCREEN2,a0
                    move.w   ULY,d1
                    lsl.w    #3,d1
                    add.w    d1,a0
                    lsl.w    #2,d1
                    add.w    d1,a0
                    add.w    #6,a0
                    clr.w    $42(a2)
                    and.w    #3,d2
                    neg.w    d2
                    add.w    #4,d2
                    and.w    #3,d2
                    lsl.w    #2,d2
                    ror.w    #4,d2
                    or.w     #$9F0,d2
                    move.w   d2,$40(a2)
                    clr.w    $64(a2)
                    move.w   #$12,$66(a2)
                    move.l   a1,$50(a2)
                    move.l   a0,$54(a2)
                    move.w   #$100B,$58(a2)
                    add.w    #$580,a1
                    add.w    #$1F40,a0
                    bsr      WAIT_BLIT
                    move.l   a1,$50(a2)
                    move.l   a0,$54(a2)
                    move.w   #$100B,$58(a2)
                    add.w    #$580,a1
                    add.w    #$1F40,a0
                    bsr      WAIT_BLIT
                    move.l   a1,$50(a2)
                    move.l   a0,$54(a2)
                    move.w   #$100B,$58(a2)
                    add.w    #$580,a1
                    add.w    #$1F40,a0
                    bsr      WAIT_BLIT
                    move.l   a1,$50(a2)
                    move.l   a0,$54(a2)
                    move.w   #$100B,$58(a2)
                    bsr      WAIT_BLIT
                    rts

ONE_SLAB:           movem.l  d0-d4/a0-a4,-(sp)
                    lea      $DFF000,a2
                    tst.w    d4
                    bpl      lbC0072BC
                    moveq    #0,d4
lbC0072BC:          mulu     #$208,d4
                    lea      LAND,a1
                    add.l    d4,a1
                    move.l   (a1),a4
                    addq.w   #8,a1
                    move.l   WHICHBACK,a0
                    lsl.w    #1,d3
                    add.w    d3,a0
                    move.w   #$8440,$96(a2)
                    move.w   #$FFFF,$44(a2)
                    move.w   #$FFFF,$46(a2)
                    clr.w    $42(a2)
                    move.w   #$9F0,$40(a2)
                    move.w   #0,$64(a2)
                    move.w   #$14,$66(a2)
                    move.l   a1,$50(a2)
                    move.l   a0,$54(a2)
                    move.w   d7,$58(a2)
                    add.w    a4,a1
                    add.w    #$580,a0
                    bsr      WAIT_BLIT
                    move.l   a1,$50(a2)
                    move.l   a0,$54(a2)
                    move.w   d7,$58(a2)
                    add.w    a4,a1
                    add.w    #$580,a0
                    bsr      WAIT_BLIT
                    move.l   a1,$50(a2)
                    move.l   a0,$54(a2)
                    move.w   d7,$58(a2)
                    add.w    a4,a1
                    add.w    #$580,a0
                    bsr      WAIT_BLIT
                    move.l   a1,$50(a2)
                    move.l   a0,$54(a2)
                    move.w   d7,$58(a2)
                    bsr      WAIT_BLIT
                    movem.l  (sp)+,d0-d4/a0-a4
                    rts

DRAWSLAB:           movem.l  d3/d4,-(sp)
                    move.w   #$1001,d7
                    bsr      ONE_SLAB
                    movem.l  (sp)+,d3/d4
                    rts

FIRE_IN_HERE:       movem.l  d2/d3,-(sp)
                    tst.w    d0
                    bne      lbC007386
                    tst.w    S1IGLOO
                    beq      lbC0073B4
                    move.w   S1SAVE_MAPY,d2
                    bra      lbC007396

lbC007386:          tst.w    S2IGLOO
                    beq      lbC0073B4
                    move.w   S2SAVE_MAPY,d2
lbC007396:          lsr.w    #4,d2
                    move.w   YIGLOO1,d3
                    lsr.w    #2,d3
                    cmp.w    d2,d3
                    beq      lbC0073B4
                    move.w   YIGLOO2,d3
                    lsr.w    #2,d3
                    cmp.w    d2,d3
                    bne      lbC0073BC
lbC0073B4:          moveq    #-1,d2
lbC0073B6:          movem.l  (sp)+,d2/d3
                    rts

lbC0073BC:          move.w   #0,d2
                    bra.s    lbC0073B6

DRAWOBJ:            bsr.s    FIRE_IN_HERE
                    beq      DRAWSPYOBJ
DRAWOBJ2:           tst.w    d0
                    bne      DRAWOBJ1_0
                    move.w   WIN1X,d1
                    move.w   WIN1Y,d2
                    move.w   #$32,ULX
                    move.w   #2,ULY
                    move.l   #S1DEPTH,a0
                    bra      DRAWOBJ1_1

DRAWOBJ1_0:         move.w   WIN2X,d1
                    move.w   WIN2Y,d2
                    move.w   #$32,ULX
                    move.w   #$67,ULY
                    move.l   #S2DEPTH,a0
DRAWOBJ1_1:         move.w   d1,d3
                    lsr.w    #2,d1
                    lsr.w    #2,d2
                    move.w   d1,WINX
                    move.w   d2,WINY
                    and.w    #3,d3
                    lsl.w    #2,d3
                    neg.w    d3
                    add.w    #14,d3
                    move.w   d3,OFFSET
                    move.l   #0,d5
                    bra      DRAW_7

DRAW_6:             move.l   #0,d4
                    bra      DRAW_11

DRAW_10:            move.w   d5,d3
                    add.w    WINY,d3
                    move.w   MAXMAPX,d1
                    asl.w    #1,d1
                    mulu     d1,d3
                    move.w   d4,d2
                    add.w    WINX,d2
                    ext.l    d2
                    asl.l    #1,d2
                    add.l    d2,d3
                    lea      MAP,a6
                    add.w    d3,a6
                    move.w   d4,d1
                    lsl.w    #4,d1
                    add.w    ULX,d1
                    add.w    OFFSET,d1
                    move.w   d5,d2
                    lsl.w    #3,d2
                    add.w    ULY,d2
                    add.w    #$2E,d2
                    clr.l    d6
                    move.w   (a6),d6
                    beq      DRAW_14
                    btst     #2,d6
                    beq      lbC0074AC
                    move.w   #0,d6
                    bra      lbC0074E0

lbC0074AC:          move.w   d6,d3
                    and.w    #$1F0,d3
                    cmp.w    #$1B0,d3
                    beq      lbC0074C2
                    cmp.w    #$1C0,d3
                    bne      lbC0074DA
lbC0074C2:          move.w   IGGY,(a6)
                    move.w   d6,d3
                    and.w    #15,d3
                    cmp.w    #1,d3
                    beq      lbC0074DA
                    add.w    #8,(a6)
lbC0074DA:          lsr.w    #3,d6
                    beq      DRAW_14
lbC0074E0:          move.w   d6,d3
                    lea      ITEMS_HEIGHT,a1
                    lsl.w    #1,d6
                    move.w   0(a1,d6.w),d7
                    beq      DRAW_14
                    lea      ITEMS_GRAPHICS,a1
                    lsl.w    #1,d6
                    move.l   0(a1,d6.w),a1
                    move.w   #1,d6
                    sub.w    d7,d2
                    cmp.w    #$13,d3
                    beq      lbC007524
                    cmp.w    #$32,d3
                    beq      lbC007524
                    cmp.w    #$33,d3
                    beq      lbC007524
                    cmp.w    #$34,d3
                    bne      lbC00752C
lbC007524:          sub.w    #10,d2
                    bra      lbC007538

lbC00752C:          cmp.w    #$31,d3
                    bne      lbC007538
                    add.w    #2,d2
lbC007538:          move.l   SCREEN2,a0
                    bsr      RSPRITER
DRAW_14:            addq.w   #1,d4
DRAW_11:            cmp.w    #10,d4
                    ble      DRAW_10
                    clr.w    d4
                    addq.w   #1,d5
DRAW_7:             move.w   #$10,d1
                    lsr.w    #2,d1
                    cmp.w    d1,d5
                    blt      DRAW_6
                    bsr      DRAWSPYOBJ
                    rts

DRAWSPYOBJ:         tst.w    d0
                    bne      SDRAWOBJ1_0
                    move.w   WIN1X,d1
                    move.w   WIN1Y,d2
                    move.w   #$32,ULX
                    move.w   #2,ULY
                    move.l   #S1DEPTH,a0
                    bra      SDRAWOBJ1_1

SDRAWOBJ1_0:        move.w   WIN2X,d1
                    move.w   WIN2Y,d2
                    move.w   #$32,ULX
                    move.w   #$67,ULY
                    move.l   #S2DEPTH,a0
SDRAWOBJ1_1:        move.w   d1,d3
                    lsr.w    #2,d1
                    lsr.w    #2,d2
                    move.w   d1,WINX
                    move.w   d2,WINY
                    and.w    #3,d3
                    lsl.w    #2,d3
                    neg.w    d3
                    add.w    #14,d3
                    move.w   d3,OFFSET
                    move.l   #0,d5
                    bra      SDRAW_7

SDRAW_6:            move.l   #0,d4
                    bra      SDRAW_11

SDRAW_10:           move.w   d5,d3
                    add.w    WINY,d3
                    move.w   MAXMAPX,d1
                    asl.w    #1,d1
                    mulu     d1,d3
                    move.w   d4,d2
                    add.w    WINX,d2
                    ext.l    d2
                    asl.l    #1,d2
                    add.l    d2,d3
                    lea      MAP,a6
                    add.w    d3,a6
                    move.w   d4,d1
                    lsl.w    #4,d1
                    add.w    ULX,d1
                    add.w    OFFSET,d1
                    move.w   d5,d2
                    lsl.w    #3,d2
                    add.w    ULY,d2
                    add.w    #$2E,d2
SDRAW_13A:          lea      SNOW_LIST,a4
                    move.w   #7,d7
lbC007638:          movem.l  d1/d2/d7,-(sp)
                    move.l   a6,d6
                    move.l   (a4),d7
                    btst     #0,d7
                    beq      lbC00764A
                    addq.w   #8,d1
lbC00764A:          and.l    #$FFFFFFFE,d6
                    and.l    #$FFFFFFFE,d7
                    cmp.l    d7,d6
                    bne      lbC007680
                    move.w   4(a4),d3
                    lsr.w    #3,d3
                    sub.w    d3,d2
                    sub.w    #5,d2
                    move.w   #1,d6
                    move.w   #5,d7
                    move.l   #G_SNOWBALL,a1
                    move.l   SCREEN2,a0
                    bsr      RSPRITER
lbC007680:          movem.l  (sp)+,d1/d2/d7
                    add.w    #10,a4
                    dbra     d7,lbC007638
SDRAW_14:           movem.l  d0-d6,-(sp)
                    lsl.w    #2,d4
                    lsl.w    #2,d5
                    tst.w    d0
                    bne      SDRAW14_1
                    add.w    WIN1Y,d5
                    add.w    WIN1X,d4
                    bra      SDRAW14_2

SDRAW14_1:          add.w    WIN2Y,d5
                    add.w    WIN2X,d4
SDRAW14_2:          lsr.w    #2,d4
                    lsr.w    #2,d5
                    bsr      DRAWSPY
                    movem.l  (sp)+,d0-d6
                    addq.w   #1,d4
SDRAW_11:           cmp.w    #10,d4
                    ble      SDRAW_10
                    clr.w    d4
                    addq.w   #1,d5
SDRAW_7:            move.w   #$10,d1
                    lsr.w    #2,d1
                    cmp.w    d1,d5
                    blt      SDRAW_6
                    tst.w    d0
                    bne      SDRAWLAND5_3
                    move.w   #10,Y
                    bra      SDRAWLAND5_4

SDRAWLAND5_3:       move.w   #$6F,Y
SDRAWLAND5_4:       move.w   #$E0,X
                    move.l   #RTCOVER,BUFFER
                    move.l   SCREEN2,SCREEN
                    move.w   #2,WIDTH
                    move.w   #$40,HEIGHT
                    movem.l  d0,-(sp)
                    bsr      SPRITER
                    movem.l  (sp)+,d0
                    rts

WHICHBACK:          dcb.l    4,0
SEL_FLAG:           dc.l     0
LASTSPY:            equ      *-2
SNOW_LIST:          dcb.l    $14,0

BUSY:               bsr      READ_TRIGS
                    move.w   d1,d2
                    move.w   d1,d3
                    lsr.w    #8,d2
                    and.w    #$FF,d1
                    cmp.w    #0,d2
                    beq      BUSY1_0
                    cmp.w    #2,d2
                    beq      BUSY2_0
                    cmp.w    #3,d2
                    beq      BUSY3_0
                    cmp.w    #4,d2
                    beq      BUSY4_0
                    cmp.w    #6,d2
                    beq      BUSY6_0
                    cmp.w    #7,d2
                    beq      BUSY7_0
                    cmp.w    #9,d2
                    beq      BUSY9_0
                    cmp.w    #10,d2
                    beq      BUSY10_0
                    cmp.w    #$14,d2
                    beq      BUSY20_0
                    cmp.w    #$15,d2
                    beq      BUSY21_0
                    cmp.w    #$16,d2
                    beq      BUSY22
                    cmp.w    #$40,d2
                    beq      BUSY40_0
                    cmp.w    #$41,d2
                    beq      BUSY41_0
                    cmp.w    #$24,d2
                    beq      BUSY36
                    cmp.w    #$50,d2
                    beq      BUSY50
                    cmp.w    #$51,d2
                    beq      BUSY51
                    cmp.w    #$52,d2
                    beq      BUSY52
                    cmp.w    #$53,d2
                    beq      BUSY53
                    cmp.w    #$54,d2
                    beq      BUSY54
                    cmp.w    #$55,d2
                    beq      BUSY55
                    cmp.w    #$56,d2
                    beq      BUSY56
                    cmp.w    #$57,d2
                    beq      BUSY57
                    cmp.w    #$58,d2
                    beq      BUSY58
                    cmp.w    #$59,d2
                    beq      BUSY59
                    rts

DELAY_TABLE:        dcb.w    $1A,0
DELAY1:             dc.w     0
DELAY2:             dc.w     0

BUSY1_0:            tst.w    d0
                    bne      lbC0078B0
                    tst.w    S1AUTO
                    beq      lbC0078BE
                    bra      BRAINBUSY

lbC0078B0:          tst.w    S2AUTO
                    beq      lbC0078BE
                    bra      BRAINBUSY

lbC0078BE:          tst.w    d0
                    bne      BUSY1_0_1
                    tst.w    S1IGLOO
                    bne      lbC0078D8
                    tst.w    S1DEAD
                    beq      lbC0078DA
lbC0078D8:          rts

lbC0078DA:          move.w   JOY1TRIG,d1
                    bra      BUSY1_0_2

BUSY1_0_1:          tst.w    S2IGLOO
                    bne      lbC0078F8
                    tst.w    S2DEAD
                    beq      lbC0078FA
lbC0078F8:          rts

lbC0078FA:          move.w   JOY2TRIG,d1
BUSY1_0_2:          bne      BUSY1_0_3
                    bsr      GETBTIME
                    add.w    #1,d1
                    cmp.w    COUNTER,d1
                    beq      BUSY1_1
                    rts

BUSY1_0_3:          bsr      GETBTIME
                    add.w    #1,d1
                    cmp.w    COUNTER,d1
                    beq      BUSY1_0_4
                    bsr      GETBTIME
                    add.w    #4,d1
                    cmp.w    COUNTER,d1
                    blt      BUSY1_0_4
                    move.w   #$600,d3
                    bra      SETSTATE

BUSY1_0_4:          tst.w    d0
                    bne      BUSY1_0_6
                    move.w   COUNTER,B1TIME
                    bsr      JOYMOVE
                    tst.w    S1DEPTH
                    bne      lbC007978
                    tst.w    S1HAND
                    bne      lbC007972
                    tst.w    d1
                    bne      BUSY_SW
lbC007972:          tst.w    d2
                    bne      BUSY4_0
lbC007978:          rts

BUSY1_0_6:          move.w   COUNTER,B2TIME
                    bsr      JOYMOVE
                    tst.w    S2DEPTH
                    bne      lbC0079A8
                    tst.w    S2HAND
                    bne      lbC0079A2
                    tst.w    d1
                    bne      BUSY_SB
lbC0079A2:          tst.w    d2
                    bne      BUSY4_0
lbC0079A8:          rts

BUSY1_1:            tst.w    d0
                    bne      BUSY1_1_0
                    move.w   S1HAND,d2
                    bra      BUSY1_1_1

BUSY1_1_0:          move.w   S2HAND,d2
BUSY1_1_1:          move.w   d2,d7
                    cmp.w    #8,d2
                    beq      BUSY1_2
                    tst.w    d2
                    beq      BUSY1_2
BUSY1_1_1_1:        bsr      JOYMOVE
                    or.w     d2,d1
                    tst.w    d1
                    bne      BUSY4_0
                    move.w   #3,d3
                    lsl.w    #8,d3
                    bsr      SETSTATE
                    bra      BUSY3_0

BUSY1_2:            tst.w    d0
                    bne      BUSY1_2_1
                    tst.w    S1SAFE
                    bne      BUSY1_3
                    move.w   S1MAPX,SPYX
                    move.w   S1MAPY,SPYY
                    bra      BUSY1_2_2

BUSY1_2_1:          tst.w    S2SAFE
                    bne      BUSY1_3
                    move.w   S2MAPX,SPYX
                    move.w   S2MAPY,SPYY
BUSY1_2_2:          move.w   SPYY,d1
                    lsr.w    #2,d1
                    lsl.w    #1,d1
                    mulu     MAXMAPX,d1
                    move.w   SPYX,d2
                    lsr.w    #2,d2
                    lsl.w    #1,d2
                    add.w    d2,d1
                    move.w   d1,a0
                    add.l    #MAP,a0
                    move.w   (a0),d1
                    and.w    #2,d1
                    bne      BUSY2_0_5
                    cmp.w    #8,d7
                    beq      BUSY1_1_1_1
BUSY1_3:            rts

BUSY_SW:            tst.w    d1
                    bmi      lbC007A84
                    move.w   S1MAPX,d1
                    addq.w   #2,d1
                    lea      BUFWR1,a0
                    move.w   #1,d3
                    bra      lbC007A98

lbC007A84:          move.w   S1MAPX,d1
                    sub.w    #0,d1
                    lea      BUFWL1,a0
                    move.w   #$FFFF,d3
lbC007A98:          clr.w    S1F
                    move.l   a0,S1FADDR
                    move.w   S1MAPY,d2
                    move.w   d3,S1SAVE_DIR
                    lea      S1SAVE_ADD,a1
                    bra      BUSY_S_SETUP

BUSY_SB:            tst.w    d1
                    bmi      lbC007AD6
                    move.w   S2MAPX,d1
                    addq.w   #2,d1
                    lea      BUFBR1,a0
                    move.w   #1,d3
                    bra      lbC007AEA

lbC007AD6:          move.w   S2MAPX,d1
                    sub.w    #0,d1
                    lea      BUFBL1,a0
                    move.w   #$FFFF,d3
lbC007AEA:          clr.w    S2F
                    move.l   a0,S2FADDR
                    move.w   S2MAPY,d2
                    move.w   d3,S2SAVE_DIR
                    lea      S2SAVE_ADD,a1
BUSY_S_SETUP:       lsr.w    #2,d2
                    mulu     MAXMAPX,d2
                    lsr.w    #2,d1
                    add.w    d2,d1
                    lsl.w    #1,d1
                    lea      MAP,a0
                    add.w    d1,a0
                    move.l   a0,(a1)
                    move.w   #$5000,d3
                    bra      SETSTATE

BUSY2_0:            tst.w    d1
                    bne      BUSY2_4
                    tst.w    d0
                    bne      lbC007B44
                    move.w   S1MAPX,d1
                    move.w   S1MAPY,d2
                    bra      lbC007B50

lbC007B44:          move.w   S2MAPX,d1
                    move.w   S2MAPY,d2
lbC007B50:          lsr.w    #2,d2
                    mulu     MAXMAPX,d2
                    lsr.w    #2,d1
                    add.w    d2,d1
                    lsl.w    #1,d1
                    lea      MAP,a0
                    add.w    d1,a0
                    clr.w    d1
BUSY2_0_5:          move.w   (a0),d4
                    beq      BUSY6_4
                    cmp.w    #$A7,(a0)
                    bgt      BUSY6_4
                    cmp.w    #$80,(a0)
                    beq      BUSY6_4
                    cmp.w    #$88,(a0)
                    beq      BUSY6_4
                    cmp.w    #$98,(a0)
                    beq      BUSY6_4
                    tst.w    d0
                    bne      lbC007BB6
                    tst.w    S1HAND
                    beq      lbC007BD4
                    cmp.w    #8,S1HAND
                    bne      BUSY6_4
                    cmp.w    #$28,d4
                    bge      BUSY6_4
                    bra      lbC007BD4

lbC007BB6:          tst.w    S2HAND
                    beq      lbC007BD4
                    cmp.w    #8,S2HAND
                    bne      BUSY6_4
                    cmp.w    #$28,d4
                    bge      BUSY6_4
lbC007BD4:          move.l   #14,d7
                    jsr      NEW_SOUND
                    bsr      DELETE_TRAP
                    and.w    #$FFF8,d4
                    bsr      KILLDOUBLE
                    tst.w    d0
                    bne      lbC007BFE
                    cmp.w    #$78,d4
                    beq      lbC007C06
                    bra      lbC007C0E

lbC007BFE:          cmp.w    #$70,d4
                    bne      lbC007C0E
lbC007C06:          move.w   #$5900,d3
                    bra      SETSTATE

lbC007C0E:          tst.w    d0
                    bne      lbC007C1E
                    move.w   S1HAND,d7
                    bra      lbC007C24

lbC007C1E:          move.w   S2HAND,d7
lbC007C24:          cmp.w    #8,d7
                    bne      lbC007C58
                    move.w   d4,d7
                    lsr.w    #3,d7
                    cmp.w    #1,d7
                    ble      BUSY6_4
                    cmp.w    #4,d7
                    bgt      BUSY6_4
                    subq.w   #2,d7
                    lea      IN_CASE,a2
                    move.b   #$FF,0(a2,d7.w)
                    move.w   d4,d7
                    move.w   #8,d4
                    bra      lbC007C5A

lbC007C58:          move.w   d4,d7
lbC007C5A:          cmp.w    #8,d7
                    bne      lbC007C72
                    clr.w    XCASE
                    clr.w    YCASE
                    bra      lbC007D2E

lbC007C72:          cmp.w    #$10,d7
                    bne      lbC007C8A
                    clr.w    XCARD
                    clr.w    YCARD
                    bra      lbC007D2E

lbC007C8A:          cmp.w    #$18,d7
                    bne      lbC007CA2
                    clr.w    XJAR
                    clr.w    YJAR
                    bra      lbC007D2E

lbC007CA2:          cmp.w    #$20,d7
                    bne      lbC007CBA
                    clr.w    XGYRO
                    clr.w    YGYRO
                    bra      lbC007D2E

lbC007CBA:          cmp.w    #$58,d7
                    bne      lbC007CD2
                    clr.w    XBPLUNG
                    clr.w    YBPLUNG
                    bra      lbC007D2E

lbC007CD2:          cmp.w    #$60,d7
                    bne      lbC007CEA
                    clr.w    XWPLUNG
                    clr.w    YWPLUNG
                    bra      lbC007D2E

lbC007CEA:          cmp.w    #$30,d7
                    bne      lbC007D02
                    clr.w    XWBUCK
                    clr.w    YWBUCK
                    bra      lbC007D2E

lbC007D02:          cmp.w    #$38,d7
                    bne      lbC007D1A
                    clr.w    XBBUCK
                    clr.w    YBBUCK
                    bra      lbC007D2E

lbC007D1A:          cmp.w    #$70,d4
                    beq      lbC007D2A
                    cmp.w    #$78,d4
                    bne      lbC007D2E
lbC007D2A:          move.w   #$50,d4
lbC007D2E:          tst.w    d0
                    bne      BUSY2_2_1
                    move.w   d4,S1HAND
                    move.w   #0,S1F
                    move.l   #BUF16,S1FADDR
                    bra      BUSY2_4

BUSY2_2_1:          move.w   d4,S2HAND
                    move.w   #0,S2F
                    move.l   #BUF26,S2FADDR
BUSY2_4:            cmp.w    #4,d1
                    beq      BUSY2_5
                    add.w    #$201,d1
                    move.w   d1,d3
                    bra      SETSTATE

BUSY2_5:            tst.w    d0
                    bne      BUSY2_6
                    move.l   #BUF14,S1FADDR
                    bra      BUSY2_7

BUSY2_6:            move.l   #BUF24,S2FADDR
BUSY2_7:            move.w   #$700,d3
                    bra      SETSTATE

BUSY3_0:            tst.w    d0
                    bne      BUSY3_1
                    move.w   S1MAPX,SPYX
                    move.w   S1MAPY,SPYY
                    move.w   S1HAND,d3
                    bra      BUSY3_2

BUSY3_1:            move.w   S2MAPX,SPYX
                    move.w   S2MAPY,SPYY
                    move.w   S2HAND,d3
BUSY3_2:            tst.w    d3
                    bne      BUSY3_2_0_1
                    rts

BUSY3_2_0_1:        bsr      KILLDOUBLE
                    move.l   #15,d7
                    jsr      NEW_SOUND
BUSY3_2_A:          cmp.w    #$40,d3
                    bne      BUSY3_2_B
                    tst.w    d0
                    bne      BUSY3_2_A_0
                    addq.w   #1,S1PICK
                    clr.w    S1HAND
                    bra      BUSY6_4

BUSY3_2_A_0:        addq.w   #1,S2PICK
                    clr.w    S2HAND
                    bra      BUSY6_4

BUSY3_2_B:          cmp.w    #$28,d3
                    bne      BUSY3_2_W
                    tst.w    d0
                    bne      BUSY3_2_B_0
                    addq.w   #1,S1SAW
                    clr.w    S1HAND
                    bra      BUSY6_4

BUSY3_2_B_0:        addq.w   #1,S2SAW
                    clr.w    S2HAND
                    bra      BUSY6_4

BUSY3_2_W:          cmp.w    #$50,d3
                    bne      BUSY3_2_Y
                    tst.w    d0
                    bne      BUSY3_2_W_0
                    addq.w   #1,S1TNT
                    clr.w    S1HAND
                    bra      BUSY6_4

BUSY3_2_W_0:        addq.w   #1,S2TNT
                    clr.w    S2HAND
                    bra      BUSY6_4

BUSY3_2_Y:          tst.w    d0
                    bne      BUSY3_2_Y_0
                    cmp.w    #$60,d3
                    bne      BUSY3_2_C
                    addq.w   #1,S1PLUNGER
                    clr.w    S1HAND
                    bra      BUSY6_4

BUSY3_2_Y_0:        cmp.w    #$58,d3
                    bne      BUSY3_2_C
                    addq.w   #1,S2PLUNGER
                    clr.w    S2HAND
                    bra      BUSY6_4

BUSY3_2_C:          cmp.w    #$30,d3
                    bne      BUSY3_2_C5
                    tst.w    d0
                    bne      BUSY3_2_C5
                    addq.w   #1,S1BUCKET
                    clr.w    S1HAND
                    bra      BUSY6_4

BUSY3_2_C5:         cmp.w    #$38,d3
                    bne      BUSY3_2_D
                    tst.w    d0
                    beq      BUSY3_2_D
                    addq.w   #1,S2BUCKET
                    clr.w    S2HAND
                    bra      BUSY6_4

BUSY3_2_D:          cmp.w    #$48,d3
                    bne      BUSY3_2_Z
                    tst.w    d0
                    bne      BUSY3_2_D_0
                    addq.w   #1,S1SNSH
                    clr.w    S1HAND
                    bra      BUSY6_4

BUSY3_2_D_0:        addq.w   #1,S2SNSH
                    clr.w    S2HAND
                    bra      BUSY6_4

BUSY3_2_Z:          move.w   SPYY,d1
                    lsr.w    #2,d1
                    lsl.w    #1,d1
                    mulu     MAXMAPX,d1
                    move.w   SPYX,d2
                    lsr.w    #2,d2
                    lsl.w    #1,d2
                    add.w    d2,d1
                    move.w   d1,a0
                    add.l    #MAP,a0
                    cmp.w    #$A7,(a0)
                    bgt      lbC007F7C
                    bsr      ADD_TRAP
                    movem.l  d0-d3/a0-a2,-(sp)
                    bsr      DOSETXY
                    movem.l  (sp)+,d0-d3/a0-a2
                    and.w    #$FFF8,d3
                    or.w     #2,d3
                    move.w   d3,(a0)
                    tst.w    d0
                    bne      lbC007F76
                    clr.w    S1HAND
                    bra      lbC007F7C

lbC007F76:          clr.w    S2HAND
lbC007F7C:          move.w   #$700,d3
                    bra      SETSTATE

BUSY4_0:            bsr      KILLDOUBLE
                    tst.w    d0
                    bne      BUSY4_1
                    tst.w    S1SAFE
                    bne      BUSY6_4
                    move.l   #S1FADDR,a6
                    move.l   #BUF11,d6
                    move.w   S1MAPX,SPYX
                    move.w   S1MAPY,SPYY
                    move.w   S1HAND,d3
                    bra      BUSY4_2

BUSY4_1:            move.l   #S2FADDR,a6
                    move.l   #BUF21,d6
                    tst.w    S2SAFE
                    bne      BUSY6_4
                    move.w   S2MAPX,SPYX
                    move.w   S2MAPY,SPYY
                    move.w   S2HAND,d3
BUSY4_2:            tst.w    d3
                    bne      BUSY4_2_0
                    bra      BUSY6_4

BUSY4_2_0:          bsr      KILLDOUBLE
BUSY4_2_1_0:        move.w   SPYY,d1
                    lsr.w    #2,d1
                    lsl.w    #1,d1
                    mulu     MAXMAPX,d1
                    move.w   SPYX,d2
                    lsr.w    #2,d2
                    lsl.w    #1,d2
                    add.w    d2,d1
                    move.w   d1,a0
                    add.l    #MAP,a0
                    move.w   (a0),d1
                    and.w    #$FFF8,d1
                    cmp.w    #$A7,d1
                    bgt      BUSY6_4
BUSY4_2_1:          move.l   a0,a1
                    bsr      ADD_TRAP
BUSY4_3:            and.w    #$FFF8,d3
BUSY4_4A:           tst.w    d0
                    bne      lbC008066
                    cmp.w    #$50,d3
                    bne      lbC008052
                    move.w   #$70,d3
                    bra      BUSY4_6

lbC008052:          cmp.w    #$30,d3
                    beq      lbC008096
                    cmp.w    #$60,d3
                    beq      _DELETE_TRAP
                    bra      BUSY4_5

lbC008066:          cmp.w    #$50,d3
                    bne      lbC008076
                    move.w   #$78,d3
                    bra      BUSY4_6

lbC008076:          cmp.w    #$58,d3
                    beq      _DELETE_TRAP
                    cmp.w    #$38,d3
                    bne      BUSY4_5
                    bra      lbC008096

_DELETE_TRAP:       bsr      DELETE_TRAP
                    move.w   #$5800,d3
                    bra      SETSTATE

lbC008096:          cmp.l    a0,a1
                    bne      BUSY4_5
                    clr.w    d3
                    clr.w    d1
                    bra      C99

BUSY4_5:            cmp.w    #$28,d3
                    bne      BUSY4_5A
                    move.w   #0,d3
                    cmp.l    a0,a1
                    bne      BUSY4_5A
                    clr.w    d1
                    bra      D99

BUSY4_5A:           cmp.w    #$40,d3
                    bne      BUSY4_6
                    bsr      DELETE_TRAP
                    move.l   a1,a0
                    clr.w    d1
                    cmp.w    #$190,(a0)
                    beq      _E99
                    cmp.w    #$190,2(a0)
                    beq      lbC008128
                    cmp.w    #$190,-2(a0)
                    beq      lbC008122
                    tst.w    d0
                    bne      lbC008108
                    tst.w    S1AUTO
                    beq      BUSY6_4
                    clr.w    S1HAND
                    addq.w   #1,S1PICK
                    bra      BUSY6_4

lbC008108:          tst.w    S2AUTO
                    beq      BUSY6_4
                    clr.w    S2HAND
                    addq.w   #1,S2PICK
                    bra      BUSY6_4

lbC008122:          subq.w   #2,a0
_E99:               bra      E99

lbC008128:          addq.w   #2,a0
                    bra      E99

BUSY4_6:            tst.w    d0
                    bne      lbC00813E
                    clr.w    S1HAND
                    bra      lbC008144

lbC00813E:          clr.w    S2HAND
lbC008144:          move.l   #3,d7
                    jsr      NEW_SOUND
                    or.w     #6,d3
                    move.w   d3,(a0)
BUSY4_7:            move.w   (a0),d3
                    beq      lbC008164
                    and.w    #$FFF8,d3
                    bsr      DOSETXY
lbC008164:          move.w   #$700,d3
                    bra      SETSTATE

BUSY4_A:            movem.l  d1-d3,-(sp)
                    tst.w    d0
                    bne      BUSY4_B
                    move.w   S1MAPX,SPYX
                    move.w   S1MAPY,SPYY
                    move.w   S1HAND,d3
                    move.w   #0,S1HAND
                    bra      BUSY4_C

BUSY4_B:            move.w   S2MAPX,SPYX
                    move.w   S2MAPY,SPYY
                    move.w   S2HAND,d3
                    move.w   #0,S2HAND
BUSY4_C:            tst.w    d3
                    bne      BUSY4_D
                    movem.l  (sp)+,d1-d3
                    rts

BUSY4_D:            move.w   SPYY,d1
                    lsr.w    #2,d1
                    lsl.w    #1,d1
                    mulu     MAXMAPX,d1
                    move.w   SPYX,d2
                    lsr.w    #2,d2
                    lsl.w    #1,d2
                    add.w    d2,d1
                    move.w   d1,a0
                    add.l    #MAP,a0
BUSY4_E:            cmp.w    #$A8,(a0)
                    beq      lbC0081FE
                    cmp.w    #$B0,(a0)
                    bne      lbC008224
lbC0081FE:          movem.l  d0-d4/a0-a3,-(sp)
                    movem.l  d0-d3,-(sp)
                    move.w   #1,d1
                    move.w   d3,d2
                    or.w     #6,d2
                    bsr      STIT
                    movem.l  (sp)+,d0-d3
                    bsr      DOSETXY
                    movem.l  (sp)+,d0-d4/a0-a3
                    bra      BUSY4_H

lbC008224:          tst.w    -(a0)
                    beq      BUSY4_G
                    tst.w    -(a0)
                    beq      BUSY4_G
                    add.w    #4,a0
BUSY4_F:            add.w    #2,a0
                    tst.w    (a0)
                    bne.s    BUSY4_F
BUSY4_G:            and.w    #$FFF8,d3
                    or.w     #6,d3
                    move.w   d3,(a0)
BUSY4_H:            movem.l  d0-d3/a0-a2,-(sp)
                    move.w   (a0),d3
                    and.w    #$FFF8,d3
                    bsr      DOSETXY
                    move.w   #$700,d3
                    bsr      SETSTATE
                    movem.l  (sp)+,d0-d3/a0-a2
                    movem.l  (sp)+,d1-d3
                    rts

DELETE_TRAP:        movem.l  d6/d7/a4,-(sp)
                    lea      TRAPLIST,a4
                    move.l   a0,d7
                    move.w   #$63,d6
lbC008276:          cmp.l    (a4),d7
                    beq      lbC008288
                    addq.w   #6,a4
                    dbra     d6,lbC008276
                    clr.w    (a0)
                    bra      lbC008292

lbC008288:          move.w   4(a4),(a0)
                    clr.l    (a4)
                    clr.w    4(a4)
lbC008292:          movem.l  (sp)+,d6/d7/a4
                    rts

ADD_TRAP:           movem.l  d3-d7/a2/a4,-(sp)
                    tst.w    d0
                    bne      lbC0082AC
                    move.w   S1AUTO,d7
                    bra      lbC0082B2

lbC0082AC:          move.w   S2AUTO,d7
lbC0082B2:          move.w   #0,d5
lbC0082B6:          move.w   (a0),d4
                    beq      lbC00830A
                    tst.w    d7
                    bne      lbC0082D6
                    btst     #0,d4
                    bne      lbC0082D6
                    and.w    #$1F8,d4
                    cmp.w    #$60,d4
                    ble      lbC0082EC
lbC0082D6:          clr.w    d7
                    addq.w   #2,d5
                    addq.w   #1,d3
                    and.w    #1,d3
                    beq      lbC0082E8
                    sub.w    d5,a0
                    bra.s    lbC0082B6

lbC0082E8:          add.w    d5,a0
                    bra.s    lbC0082B6

lbC0082EC:          lea      TRAPLIST,a4
                    move.w   #$63,d6
lbC0082F6:          tst.l    (a4)
                    beq      lbC008306
                    addq.w   #6,a4
                    dbra     d6,lbC0082F6
                    bra      lbC00830A

lbC008306:          move.l   a0,(a4)+
                    move.w   (a0),(a4)
lbC00830A:          movem.l  (sp)+,d3-d7/a2/a4
                    rts

DOSETXY:            cmp.w    #8,d3
                    bne      lbC00832C
                    move.l   #XCASE,a1
                    move.l   #YCASE,a2
                    bsr      SETXY
                    bra      lbC0083EC

lbC00832C:          cmp.w    #$10,d3
                    bne      lbC008348
                    move.l   #XCARD,a1
                    move.l   #YCARD,a2
                    bsr      SETXY
                    bra      lbC0083EC

lbC008348:          cmp.w    #$18,d3
                    bne      lbC008364
                    move.l   #XJAR,a1
                    move.l   #YJAR,a2
                    bsr      SETXY
                    bra      lbC0083EC

lbC008364:          cmp.w    #$20,d3
                    bne      lbC008380
                    move.l   #XGYRO,a1
                    move.l   #YGYRO,a2
                    bsr      SETXY
                    bra      lbC0083EC

lbC008380:          cmp.w    #$58,d3
                    bne      lbC00839C
                    move.l   #XBPLUNG,a1
                    move.l   #YBPLUNG,a2
                    bsr      SETXY
                    bra      lbC0083EC

lbC00839C:          cmp.w    #$60,d3
                    bne      lbC0083B8
                    move.l   #XWPLUNG,a1
                    move.l   #YWPLUNG,a2
                    bsr      SETXY
                    bra      lbC0083EC

lbC0083B8:          cmp.w    #$30,d3
                    bne      lbC0083D4
                    move.l   #XWBUCK,a1
                    move.l   #YWBUCK,a2
                    bsr      SETXY
                    bra      lbC0083EC

lbC0083D4:          cmp.w    #$38,d3
                    bne      lbC0083EC
                    move.l   #XBBUCK,a1
                    move.l   #YBBUCK,a2
                    bsr      SETXY
lbC0083EC:          rts

COUNTER1:           dc.w     0
COUNTER2:           dc.w     0

BUSY5_0:            tst.w    SPYWIN
                    beq      lbC008400
                    bra      BUSY6_4

lbC008400:          movem.l  d0-d3,-(sp)
                    bsr      JOYMOVE
                    movem.l  (sp)+,d0-d3
                    move.w   d1,d6
                    tst.w    d0
                    bne      BUSY5_0_1
                    tst.w    S1DEPTH
                    bne      BUSY6_4
                    tst.w    S1SWAMP
                    bne      BUSY6_4
                    move.w   #2,d2
                    move.w   JOY1TRIG,d3
                    bra      BUSY5_0_2

BUSY5_0_1:          tst.w    S2DEPTH
                    bne      BUSY6_4
                    tst.w    S2SWAMP
                    bne      BUSY6_4
                    move.w   #$67,d2
                    move.w   JOY2TRIG,d3
BUSY5_0_2:          cmp.w    #0,d1
                    bne      BUSY5_1
                    tst.w    d3
                    beq      BUSY5_0_3
                    rts

BUSY5_0_3:          move.w   #$501,d3
                    bra      SETSTATE

BUSY5_1:            cmp.w    #1,d1
                    bne      BUSY5_2
                    move.w   #$29,d3
                    lsr.w    #2,d3
                    move.w   d3,HEIGHT
                    bsr      SHOWMAP
                    move.w   #$502,d3
                    bra      SETSTATE

BUSY5_2:            cmp.w    #2,d1
                    bne      BUSY5_3
                    move.w   #$29,d3
                    lsr.w    #1,d3
                    move.w   d3,HEIGHT
                    bsr      SHOWMAP
                    move.w   #$503,d3
                    bra      SETSTATE

BUSY5_3:            cmp.w    #3,d1
                    bne      BUSY5_4
                    move.w   #$29,d3
                    mulu     #3,d3
                    lsr.w    #2,d3
                    move.w   d3,HEIGHT
                    bsr      SHOWMAP
                    move.w   #$504,d3
                    bra      SETSTATE

BUSY5_4:            move.w   d3,-(sp)
                    move.w   d1,d3
                    add.w    #$501,d3
                    cmp.w    #$FF,d1
                    bne      _SETSTATE
                    sub.w    #8,d3
_SETSTATE:          bsr      SETSTATE
                    move.w   #$29,HEIGHT
                    bsr      SHOWMAP
                    tst.w    d0
                    bne      BUSY5_4_0
                    clr.l    d1
                    move.w   S1MAPX,d1
                    move.w   S1MAPY,d2
                    move.l   #S1TRAIL,a1
                    move.w   #$13,d5
                    bra      BUSY5_4_1

BUSY5_4_0:          clr.l    d1
                    move.w   S2MAPX,d1
                    move.w   S2MAPY,d2
                    move.l   #S2TRAIL,a1
                    move.w   #$78,d5
BUSY5_4_1:          bsr      GETPOS
                    move.w   COUNTER,d2
                    and.w    #1,d2
                    bne      BUSY5_4_2
                    move.w   #1,WIDTH
                    move.w   #8,HEIGHT
                    move.l   #MAPBOX,BUFFER
                    move.l   SCREEN2,SCREEN
                    movem.l  d0-d6/a0-a3,-(sp)
                    bsr      SPRITER
                    movem.l  (sp)+,d0-d6/a0-a3
BUSY5_4_2:          tst.w    XCASE
                    beq      lbC0085C2
                    move.w   XCASE,d1
                    move.w   YCASE,d2
                    ext.l    d1
                    ext.l    d2
                    lsl.w    #2,d1
                    lsl.w    #2,d2
                    bsr      GETPOS
                    move.w   #1,WIDTH
                    move.w   #8,HEIGHT
                    move.l   #MAPSPOT,BUFFER
                    move.l   SCREEN2,SCREEN
                    movem.l  d0-d6/a0-a3,-(sp)
                    bsr      SPRITER
                    movem.l  (sp)+,d0-d6/a0-a3
lbC0085C2:          tst.w    XCARD
                    beq      lbC008614
                    move.w   XCARD,d1
                    move.w   YCARD,d2
                    ext.l    d1
                    ext.l    d2
                    lsl.w    #2,d1
                    lsl.w    #2,d2
                    bsr      GETPOS
                    move.w   #1,WIDTH
                    move.w   #8,HEIGHT
                    move.l   #MAPSPOT,BUFFER
                    move.l   SCREEN2,SCREEN
                    movem.l  d0-d6/a0-a3,-(sp)
                    bsr      SPRITER
                    movem.l  (sp)+,d0-d6/a0-a3
lbC008614:          tst.w    XJAR
                    beq      lbC008666
                    move.w   XJAR,d1
                    move.w   YJAR,d2
                    ext.l    d1
                    ext.l    d2
                    lsl.w    #2,d1
                    lsl.w    #2,d2
                    bsr      GETPOS
                    move.w   #1,WIDTH
                    move.w   #8,HEIGHT
                    move.l   #MAPSPOT,BUFFER
                    move.l   SCREEN2,SCREEN
                    movem.l  d0-d6/a0-a3,-(sp)
                    bsr      SPRITER
                    movem.l  (sp)+,d0-d6/a0-a3
lbC008666:          tst.w    XGYRO
                    beq      lbC0086B8
                    move.w   XGYRO,d1
                    move.w   YGYRO,d2
                    ext.l    d1
                    ext.l    d2
                    lsl.w    #2,d1
                    lsl.w    #2,d2
                    bsr      GETPOS
                    move.w   #1,WIDTH
                    move.w   #8,HEIGHT
                    move.l   #MAPSPOT,BUFFER
                    move.l   SCREEN2,SCREEN
                    movem.l  d0-d6/a0-a3,-(sp)
                    bsr      SPRITER
                    movem.l  (sp)+,d0-d6/a0-a3
lbC0086B8:          movem.l  d0-d7/a0-a4,-(sp)
lbC0086BC:          bra      lbC00874A

                    cmp.w    #$FFFF,2(a1)
                    beq      lbC00874A
                    move.w   (a1),d2
                    move.w   2(a1),d4
                    move.w   d2,d1
                    move.w   d4,d3
                    lsr.w    #8,d1
                    and.w    #$FF,d2
                    lsr.w    #8,d3
                    and.w    #$FF,d4
                    sub.w    d1,d3
                    sub.w    d2,d4
                    lsl.w    #1,d3
                    lsl.w    #1,d4
                    movem.l  d0-d2/a1,-(sp)
                    bsr      GETPOS1
                    movem.l  (sp)+,d0-d2/a1
                    add.w    #6,X
                    add.w    #3,Y
                    move.w   #8,d6
                    tst.w    d3
                    bne      lbC008712
                    move.w   #4,d6
lbC008712:          move.w   #15,COLOR
                    move.w   #1,COUNT
                    movem.l  d0-d5/a1,-(sp)
                    bsr      HLINE
                    movem.l  (sp)+,d0-d5/a1
                    add.w    d3,X
                    add.w    d4,Y
                    sub.w    #1,d6
                    bne.s    lbC008712
                    add.l    #2,a1
                    bra      lbC0086BC

lbC00874A:          movem.l  (sp)+,d0-d7/a0-a4
BUSY5_5:            move.w   d6,d1
                    move.w   (sp)+,d3
                    tst.w    d0
                    bne      lbC008766
                    tst.w    S1AUTO
                    beq      lbC008786
                    bra      lbC008770

lbC008766:          tst.w    S2AUTO
                    beq      lbC008786
lbC008770:          move.w   IQ,d2
                    lsl.w    #3,d2
                    move.w   #$3C,d3
                    sub.w    d2,d3
                    sub.w    d1,d3
                    blt      BUSY5_6
                    rts

lbC008786:          tst.w    d3
                    bne      BUSY5_6
                    rts

BUSY5_6:            move.w   #0,d3
                    bra      SETSTATE

BUSY6_0:            bra      lbC0087AA

                    clr.w    S1FLASH
                    clr.w    S2FLASH
                    bra      BUSY6_4

lbC0087AA:          tst.w    d0
                    bne      BUSY6_0_1
                    clr.w    S1F
                    move.w   JOY1TRIG,d2
                    move.w   S1MENU,d4
                    move.w   #1,S1FLASH
                    bra      BUSY6_0_2

BUSY6_0_1:          clr.w    S2F
                    move.w   JOY2TRIG,d2
                    move.w   S2MENU,d4
                    move.w   #1,S2FLASH
BUSY6_0_2:          tst.w    d1
                    bne      BUSY6_1
                    tst.w    d2
                    beq      BUSY6_0_3
                    rts

BUSY6_0_3:          bra      BUSY6_0_4

                    movem.l  d0-d7/a0-a6,-(sp)
                    tst.w    d0
                    bne      lbC00881A
                    move.l   #S1BACK,a1
                    move.w   #10,d2
                    move.w   #$FFFF,BUF1Y
                    bra      lbC00882C

lbC00881A:          move.l   #S2BACK,a1
                    move.w   #$6F,d2
                    move.w   #$FFFF,BUF2Y
lbC00882C:          move.w   #4,d1
                    move.w   #12,d6
                    move.w   #$40,d7
                    move.l   SCREEN1,a0
                    bsr      RSAVE_BUFF
                    move.w   #4,d1
                    move.w   #12,d6
                    move.w   #$40,d7
                    move.l   SCREEN2,a0
                    bsr      RDRAW_BUFF
                    movem.l  (sp)+,d0-d7/a0-a6
BUSY6_0_4:          move.w   #$601,d3
                    bsr      SETSTATE
BUSY6_1:            tst.w    d2
                    bne      BUSY6_2
                    move.w   COUNTER,d1
                    and.w    #1,d1
                    beq      BUSY6_1_4
                    move.w   d4,-(sp)
                    bsr      JOYMOVE
                    move.w   (sp)+,d4
                    tst.w    d1
                    beq      lbC008892
                    move.l   #1,d7
                    jsr      NEW_SOUND
lbC008892:          add.w    d1,d4
                    tst.w    d4
                    bgt      BUSY6_1_1
                    move.w   #7,d4
                    bra      BUSY6_1_2

BUSY6_1_1:          cmp.w    #7,d4
                    ble      BUSY6_1_2
                    move.w   #1,d4
BUSY6_1_2:          tst.w    d0
                    bne      BUSY6_1_3
                    move.w   d4,S1MENU
                    rts

BUSY6_1_3:          move.w   d4,S2MENU
BUSY6_1_4:          rts

BUSY6_2:            tst.w    d0
                    bne      BUSY6_2_1
                    move.w   #0,S1FLASH
                    bra      BUSY6_3

BUSY6_2_1:          move.w   #0,S2FLASH
BUSY6_3:            tst.w    d0
                    bne      BUSY6_3_0_1
                    move.w   S1HAND,d1
                    bra      BUSY6_3_0_2

BUSY6_3_0_1:        move.w   S2HAND,d1
BUSY6_3_0_2:        tst.w    d1
                    bne      BUSY6_4
                    cmp.w    #1,d4
                    bne      BUSY6_3_1
                    tst.w    d0
                    bne      BUSY6_3_0_3
                    move.w   S1PLUNGER,d1
                    beq      BUSY6_4
                    move.w   #$60,S1HAND
                    subq.w   #1,S1PLUNGER
                    move.w   #$700,d3
                    bra      SETSTATE

BUSY6_3_0_3:        move.w   S2PLUNGER,d1
                    beq      BUSY6_4
                    move.w   #$58,S2HAND
                    subq.w   #1,S2PLUNGER
                    move.w   #$700,d3
                    bra      SETSTATE

BUSY6_3_1:          cmp.w    #2,d4
                    bne      BUSY6_3_2
                    tst.w    d0
                    bne      BUSY6_3_1_0
                    move.w   S1SAW,d1
                    beq      BUSY6_4
                    move.w   #$28,S1HAND
                    subq.w   #1,S1SAW
                    move.w   #$700,d3
                    bra      SETSTATE

BUSY6_3_1_0:        move.w   S2SAW,d1
                    beq      BUSY6_4
                    move.w   #$28,S2HAND
                    subq.w   #1,S2SAW
                    move.w   #$700,d3
                    bra      SETSTATE

BUSY6_3_2:          cmp.w    #3,d4
                    bne      BUSY6_3_3
                    tst.w    d0
                    bne      BUSY6_3_2_0
                    move.w   S1BUCKET,d1
                    beq      BUSY6_4
                    move.w   #$30,S1HAND
                    subq.w   #1,S1BUCKET
                    move.w   #$700,d3
                    bra      SETSTATE

BUSY6_3_2_0:        move.w   S2BUCKET,d1
                    beq      BUSY6_4
                    move.w   #$38,S2HAND
                    subq.w   #1,S2BUCKET
                    move.w   #$700,d3
                    bra      SETSTATE

BUSY6_3_3:          cmp.w    #4,d4
                    bne      BUSY6_3_4
                    tst.w    d0
                    bne      BUSY6_3_3_0
                    move.w   S1PICK,d1
                    beq      BUSY6_4
                    move.w   #$40,S1HAND
                    subq.w   #1,S1PICK
                    move.w   #$700,d3
                    bra      SETSTATE

BUSY6_3_3_0:        move.w   S2PICK,d1
                    beq      BUSY6_4
                    move.w   #$40,S2HAND
                    subq.w   #1,S2PICK
                    move.w   #$700,d3
                    bra      SETSTATE

BUSY6_3_4:          cmp.w    #5,d4
                    bne      BUSY6_3_4A
                    tst.w    d0
                    bne      BUSY6_3_4_0
                    move.w   S1SNSH,d1
                    beq      BUSY6_4
                    move.w   #$48,S1HAND
                    subq.w   #1,S1SNSH
                    move.w   #$700,d3
                    bra      SETSTATE

BUSY6_3_4_0:        move.w   S2SNSH,d1
                    beq      BUSY6_4
                    move.w   #$48,S2HAND
                    subq.w   #1,S2SNSH
                    move.w   #$700,d3
                    bra      SETSTATE

BUSY6_3_4A:         cmp.w    #6,d4
                    bne      BUSY6_3_5
                    tst.w    d0
                    bne      BUSY6_3_4A_0
                    move.w   S1TNT,d1
                    beq      BUSY6_4
                    move.w   #$50,S1HAND
                    subq.w   #1,S1TNT
                    move.w   #$700,d3
                    bra      SETSTATE

BUSY6_3_4A_0:       move.w   S2TNT,d1
                    beq      BUSY6_4
                    move.w   #$50,S2HAND
                    subq.w   #1,S2TNT
                    move.w   #$700,d3
                    bra      SETSTATE

BUSY6_3_5:          cmp.w    #7,d4
                    bne      BUSY6_4
                    move.w   #$500,d3
                    bra      SETSTATE

BUSY6_4:            move.w   #0,d3
                    bra      SETSTATE

BUSY7_0:            move.w   #$700,d3
                    bsr      SETSTATE
                    tst.w    d0
                    bne      BUSY7_1
                    move.w   JOY1TRIG,d1
                    bra      BUSY7_2

BUSY7_1:            move.w   JOY2TRIG,d1
BUSY7_2:            beq      BUSY7_3
                    rts

BUSY7_3:            move.w   #0,d3
                    bra      SETSTATE

SETSAFE:            tst.w    d0
                    bne      SETSAFE1
                    move.w   #4,S1SAFE
                    rts

SETSAFE1:           move.w   #4,S2SAFE
                    rts

KILLDOUBLE:         movem.l  d1,-(sp)
                    move.w   COUNTER,d1
                    sub.w    #$14,d1
                    tst.w    d0
                    bne      lbC008B4A
                    move.w   d1,B1TIME
                    bra      lbC008B50

lbC008B4A:          move.w   d1,B2TIME
lbC008B50:          movem.l  (sp)+,d1
                    rts

GETPOS:             asr.w    #2,d1
                    divs     #10,d1
                    asr.w    #4,d2
GETPOS1:            lsl.w    #4,d1
                    lsl.w    #3,d2
                    add.w    #$61,d1
                    add.w    d5,d2
                    move.w   d1,X
                    move.w   d2,Y
                    rts

BUSY9_0:            tst.w    d1
                    bne      BUSY9_4
                    tst.w    d0
                    bne      BUSY9_2
                    tst.w    S1DEAD
                    bne      BUSY6_4
                    move.l   #2,d7
                    jsr      NEW_SOUND
                    tst.w    S1HAND
                    beq      lbC008BA6
                    bsr      BUSY4_A
lbC008BA6:          sub.w    #3,S1ENERGY
                    move.w   #2,REFRESH
                    move.w   #0,S1F
                    move.w   S1MAPX,d1
                    sub.w    S2MAPX,d1
                    bgt      BUSY9_1
                    move.l   #BUF19,S1FADDR
                    move.w   #$901,d3
                    bra      SETSTATE

BUSY9_1:            move.l   #BUF1A,S1FADDR
                    move.w   #$901,d3
                    bra      SETSTATE

BUSY9_2:            tst.w    S2DEAD
                    bne      BUSY6_4
                    move.l   #2,d7
                    jsr      NEW_SOUND
                    tst.w    S2HAND
                    beq      lbC008C16
                    bsr      BUSY4_A
lbC008C16:          sub.w    #3,S2ENERGY
                    move.w   #2,REFRESH
                    move.w   #0,S2F
                    move.w   S2MAPX,d1
                    sub.w    S1MAPX,d1
                    bgt      BUSY9_3
                    move.l   #BUF29,S2FADDR
                    move.w   #$901,d3
                    bra      SETSTATE

BUSY9_3:            move.l   #BUF2A,S2FADDR
                    move.w   #$901,d3
                    bra      SETSTATE

BUSY9_4:            move.w   d1,d3
                    add.w    #$901,d3
                    cmp.w    #$903,d3
                    bne      SETSTATE
                    tst.w    d0
                    bne      BUSY9_5
                    move.l   S1FADDR,d1
                    cmp.l    #BUF19,d1
                    beq      BUSY9_4_0
                    move.w   #1,SPYDIR
                    move.l   #BUF12,S1FADDR
                    move.w   #0,S1F
                    bra      BUSY9_6

BUSY9_4_0:          move.w   #2,SPYDIR
                    move.l   #BUF11,S1FADDR
                    move.w   #0,S1F
                    bra      BUSY9_6

BUSY9_5:            move.l   S2FADDR,d1
                    cmp.l    #BUF29,d1
                    beq      BUSY9_5_0
                    move.w   #1,SPYDIR
                    move.l   #BUF22,S2FADDR
                    move.w   #0,S2F
                    bra      BUSY9_6

BUSY9_5_0:          move.w   #2,SPYDIR
                    move.l   #BUF21,S2FADDR
                    move.w   #0,S2F
BUSY9_6:            move.w   #0,d3
                    bra      SETSTATE

BUSY10_0:           tst.w    d0
                    bne      BUSY10_0_0
                    tst.w    S1DEAD
                    bne      BUSY6_4
                    move.l   #WLAUGH1,S1FADDR
                    move.l   #S1F,a0
                    bra      BUSY10_0_1

BUSY10_0_0:         tst.w    S2DEAD
                    bne      BUSY6_4
                    move.l   #BLAUGH1,S2FADDR
                    move.l   #S2F,a0
BUSY10_0_1:         cmp.w    #8,d1
                    beq      BUSY10_4
                    btst     #0,d1
                    beq      BUSY10_1
                    move.w   #0,(a0)
                    add.w    #$A01,d1
                    move.w   d1,d3
                    bra      SETSTATE

BUSY10_1:           move.w   #1,(a0)
                    add.w    #$A01,d1
                    move.w   d1,d3
                    bra      SETSTATE

BUSY10_4:           tst.w    d0
                    bne      BUSY10_5
                    move.l   #BUF11,S1FADDR
                    bra      BUSY10_6

BUSY10_5:           move.l   #BUF21,S2FADDR
BUSY10_6:           bra      BUSY7_0

BUSY20_0:           clr.w    S1RUN
                    clr.w    S2RUN
                    tst.w    d0
                    bne      lbC008E00
                    move.l   #S1FADDR,a1
                    move.l   #S1F,a2
                    move.l   #S1DEPTH,a3
                    move.w   #1,S1DEAD
                    move.l   #S1DROWN,a4
                    move.w   S1MAPX,d3
                    move.w   S1MAPY,d4
                    move.w   #0,S1SWAMP
                    tst.w    d1
                    bne      lbC008E4C
                    tst.w    S2CT
                    bne      lbC008E4C
                    move.w   #$A00,S2CT
                    bra      lbC008E4C

lbC008E00:          move.l   #S2FADDR,a1
                    move.l   #S2F,a2
                    move.l   #S2DEPTH,a3
                    move.w   #1,S2DEAD
                    move.l   #S2DROWN,a4
                    move.w   S2MAPX,d3
                    move.w   S2MAPY,d4
                    move.w   #0,S2SWAMP
                    tst.w    d1
                    bne      lbC008E4C
                    tst.w    S1CT
                    bne      lbC008E4C
                    move.w   #$A00,S1CT
lbC008E4C:          tst.w    d1
                    bne      lbC008E6E
                    move.w   #$1D,(a3)
                    move.l   #12,d7
                    jsr      NEW_SOUND
                    movem.l  d0-d7/a0-a6,-(sp)
                    bsr      BUSY4_A
                    movem.l  (sp)+,d0-d7/a0-a6
lbC008E6E:          clr.w    (a2)
                    move.l   #GRAVE,(a1)
                    subq.w   #1,(a3)
                    cmp.w    #5,(a3)
                    bgt      lbC008E84
                    bra      BUSY6_4

lbC008E84:          add.w    #$1401,d1
                    move.w   d1,d3
                    bra      SETSTATE

BUSY21_0:           cmp.w    #2,d1
                    bne      BUSY21_1
                    tst.w    d0
                    bne      lbC008EB8
                    clr.w    S1ALTITUDE
                    move.l   #BUF14,S1FADDR
                    move.w   #0,S1F
                    bra      _BUSY6_4

lbC008EB8:          clr.w    S2ALTITUDE
                    move.l   #BUF24,S2FADDR
                    move.w   #0,S2F
_BUSY6_4:           bra      BUSY6_4

BUSY21_1:           tst.w    d0
                    bne      lbC008F0A
                    move.w   #$FFF8,S1ALTITUDE
                    move.w   #0,S1DEPTH
                    move.l   #S1F,a0
                    move.l   #S1MAPX,a1
                    move.l   #S1MAPY,a2
                    move.l   #BUF1D,S1FADDR
                    bra      lbC008F36

lbC008F0A:          move.w   #$FFF8,S2ALTITUDE
                    move.w   #0,S2DEPTH
                    move.l   #S2F,a0
                    move.l   #S2MAPX,a1
                    move.l   #S2MAPY,a2
                    move.l   #BUF2D,S2FADDR
lbC008F36:          move.w   #1,(a0)
                    clr.l    d3
                    move.w   (a2),d3
                    lsr.w    #2,d3
                    mulu     MAXMAPX,d3
                    move.w   (a1),d4
                    sub.w    #4,d4
                    lsr.w    #2,d4
                    add.w    d4,d3
                    lsl.w    #1,d3
                    add.l    #MAP,d3
                    move.l   d3,a3
                    cmp.w    #$A8,(a3)
                    beq      lbC008F6A
                    cmp.w    #$B0,(a3)
                    bne      lbC008F6E
lbC008F6A:          move.w   #0,(a0)
lbC008F6E:          add.w    #$1501,d1
                    move.w   d1,d3
                    bra      SETSTATE

BUSY40_0:           tst.w    d0
                    bne      lbC008F8A
                    move.w   #0,S1F
                    bra      lbC008F92

lbC008F8A:          move.w   #0,S2F
lbC008F92:          add.w    #$4100,d1
                    move.w   d1,d3
                    bra      SETSTATE

BUSY41_0:           lsl.w    #8,d1
                    move.w   d1,d3
                    bra      SETSTATE

                    rts

SETSTATE:           tst.w    d0
                    bne      SETSTATE2
                    move.w   d3,S1CT
                    bne      lbC008FBC
                    clr.w    S1FLASH
lbC008FBC:          rts

SETSTATE2:          move.w   d3,S2CT
                    bne      lbC008FCE
                    clr.w    S2FLASH
lbC008FCE:          rts

GETBTIME:           tst.w    d0
                    bne      GETBTIME2
                    move.w   B1TIME,d1
                    rts

GETBTIME2:          move.w   B2TIME,d1
                    rts

SHOWMAP:            move.w   #$60,X
                    add.w    #$10,d2
                    move.w   d2,Y
                    move.w   #6,WIDTH
                    move.l   SCREEN2,SCREEN
                    move.w   LEVEL,d4
                    sub.w    #1,d4
                    mulu     #$7B0,d4
                    add.l    #MAPS,d4
                    move.l   d4,BUFFER
                    movem.l  d0-d3/a0,-(sp)
                    bsr      SPRITER
                    movem.l  (sp)+,d0-d3/a0
                    rts

ABSD2:              tst.w    d2
                    bge      lbC00903A
                    neg.w    d2
lbC00903A:          rts

CLEANTRAIL:         movem.l  d0/a0/a1,-(sp)
                    move.l   #S1TRAIL,a0
                    move.l   #S2TRAIL,a1
                    move.w   #5,d0
                    add.w    #1,d0
lbC009054:          move.w   #$FFFF,(a0)+
                    move.w   #$FFFF,(a1)+
                    dbra     d0,lbC009054
                    movem.l  (sp)+,d0/a0/a1
                    rts

BUSY36:             tst.w    SPYWIN
                    bne      _BUSY6_40
                    tst.w    d0
                    bne      lbC0090B0
                    tst.w    S1DROWN
                    bne      _BUSY6_40
                    tst.w    S1SWAMP
                    bne      _BUSY6_40
                    tst.w    S1DEPTH
                    bne      _BUSY6_40
                    move.l   #BUF14,d5
                    lea      S1FADDR,a4
                    lea      S1F,a5
                    move.w   S1ENERGY,d4
                    bra      lbC0090E6

lbC0090B0:          tst.w    S2DROWN
                    bne      _BUSY6_40
                    tst.w    S2SWAMP
                    bne      _BUSY6_40
                    tst.w    S2DEPTH
                    bne      _BUSY6_40
                    move.l   #BUF24,d5
                    lea      S2FADDR,a4
                    lea      S2F,a5
                    move.w   S2ENERGY,d4
lbC0090E6:          cmp.w    #$32,d4
                    bhi      _BUSY6_40
                    move.l   d5,(a4)
                    clr.w    (a5)
                    move.w   #$2401,d3
                    bra      SETSTATE

_BUSY6_40:          bra      BUSY6_4

BUSY22:             tst.w    d0
                    bne      lbC00912E
                    lea      S1MENU,a2
                    lea      S1AIMMENU,a3
                    lea      S1HAND,a4
                    lea      S1PLUNGER,a5
                    lea      S1FLASH,a6
                    move.w   #$60,d3
                    move.w   #$30,d2
                    bra      BUSY22A

lbC00912E:          lea      S2MENU,a2
                    lea      S2AIMMENU,a3
                    lea      S2HAND,a4
                    lea      S2PLUNGER,a5
                    lea      S2FLASH,a6
                    move.w   #$58,d3
                    move.w   #$38,d2
BUSY22A:            tst.w    SPYWIN
                    bne      BUSY6_4
                    move.w   COUNTER,d4
                    and.w    #1,d4
                    bne      lbC00918C
                    move.w   (a2),d4
                    cmp.w    (a3),d4
                    beq      BUSY22B
                    blt      lbC00917E
                    subq.w   #1,(a2)
                    bra      lbC009180

lbC00917E:          addq.w   #1,(a2)
lbC009180:          move.l   #1,d7
                    jsr      NEW_SOUND
lbC00918C:          move.w   #$1600,d3
                    bra      SETSTATE

BUSY22B:            clr.w    (a6)
                    cmp.w    #1,d4
                    bne      lbC0091AC
                    tst.w    (a5)
                    beq      _BUSY6_41
                    subq.w   #1,(a5)
                    move.w   d3,(a4)
                    bra      _BUSY6_41

lbC0091AC:          cmp.w    #2,d4
                    bne      lbC0091C8
                    tst.w    2(a5)
                    beq      _BUSY6_41
                    subq.w   #1,2(a5)
                    move.w   #$28,(a4)
                    bra      _BUSY6_41

lbC0091C8:          cmp.w    #3,d4
                    bne      lbC0091E2
                    tst.w    4(a5)
                    beq      _BUSY6_41
                    subq.w   #1,4(a5)
                    move.w   d2,(a4)
                    bra      _BUSY6_41

lbC0091E2:          cmp.w    #4,d4
                    bne      lbC0091FE
                    tst.w    6(a5)
                    beq      _BUSY6_41
                    subq.w   #1,6(a5)
                    move.w   #$40,(a4)
                    bra      _BUSY6_41

lbC0091FE:          cmp.w    #5,d4
                    bne      lbC00921A
                    tst.w    8(a5)
                    beq      _BUSY6_41
                    subq.w   #1,8(a5)
                    move.w   #$48,(a4)
                    bra      _BUSY6_41

lbC00921A:          cmp.w    #6,d4
                    bne      lbC009236
                    tst.w    10(a5)
                    beq      _BUSY6_41
                    subq.w   #1,10(a5)
                    move.w   #$50,(a4)
                    bra      _BUSY6_41

lbC009236:          tst.w    d0
                    bne      lbC009246
                    clr.w    S1F
                    bra      lbC00924C

lbC009246:          clr.w    S2F
lbC00924C:          move.w   #$4005,d3
                    bra      SETSTATE

_BUSY6_41:          bra      BUSY6_4

BUSY50:             tst.w    d0
                    bne      lbC009274
                    lea      S1SAVE_ADD,a1
                    lea      S1SAVE_DIR,a2
                    lea      S1FADDR,a3
                    bra      BUSY50_1

lbC009274:          lea      S2SAVE_ADD,a1
                    lea      S2SAVE_DIR,a2
                    lea      S2FADDR,a3
BUSY50_1:           move.w   d1,d3
                    add.w    #$5001,d3
                    cmp.w    #$5004,d3
                    bne      BUSY50_2
                    add.l    #$1E0,(a3)
                    bra      SETSTATE

BUSY50_2:           cmp.w    #$5003,d3
                    bne      BUSY50_3
                    add.l    #$1E0,(a3)
                    move.w   #6,d7
                    lea      SNOW_LIST,a4
lbC0092B6:          tst.w    (a4)
                    beq      lbC0092C4
                    add.w    #10,a4
                    dbra     d7,lbC0092B6
lbC0092C4:          move.l   (a1),(a4)
                    move.w   #$90,4(a4)
                    move.w   (a2),6(a4)
                    move.w   d0,8(a4)
                    bra      SETSTATE

BUSY50_3:           cmp.w    #$5008,d3
                    bne      SETSTATE
                    cmp.l    #BUFWL3,(a3)
                    beq      lbC009308
                    cmp.l    #BUFWR3,(a3)
                    beq      lbC009312
                    cmp.l    #BUFBL3,(a3)
                    beq      lbC00931C
                    move.l   #BUF21,(a3)
                    bra      _BUSY6_42

lbC009308:          move.l   #BUF12,(a3)
                    bra      _BUSY6_42

lbC009312:          move.l   #BUF11,(a3)
                    bra      _BUSY6_42

lbC00931C:          move.l   #BUF22,(a3)
_BUSY6_42:          bra      BUSY6_4

BUSY51:             bsr      BUSY4_A
                    tst.w    d0
                    bne      lbC009358
                    lea      S1FADDR,a1
                    lea      S1F,a2
                    lea      S1ENERGY,a3
                    move.l   S1DIG,a4
                    move.l   #BUF14,d5
                    move.l   #WSPYSAT,d6
                    bra      BUSY51_1

lbC009358:          lea      S2FADDR,a1
                    lea      S2F,a2
                    lea      S2ENERGY,a3
                    move.l   S2DIG,a4
                    move.l   #BUF24,d5
                    move.l   #BSPYSAT,d6
BUSY51_1:           move.w   d1,d3
                    add.w    #$5101,d3
                    cmp.w    #$5108,d3
                    bge      BUSY51_2
                    cmp.w    #$88,(a4)
                    bne      lbC009398
                    move.l   a4,a0
                    bsr      DELETE_TRAP
lbC009398:          and.w    #3,d1
                    move.w   d1,(a2)
                    bra      SETSTATE

BUSY51_2:           cmp.w    #$5108,d3
                    bne      BUSY51_3
                    subq.w   #4,(a3)
                    move.l   d6,(a1)
                    clr.w    (a2)
                    bra      SETSTATE

BUSY51_3:           cmp.w    #$5120,d3
                    bne      SETSTATE
                    move.l   d5,(a1)
                    clr.w    (a2)
                    bra      BUSY6_4

BUSY52:             bsr      SETUPA2
                    tst.w    d1
                    bne      C3
                    bsr      ADD_TRAP
                    bra      C99

C98:                move.w   #0,d3
                    bra      SETSTATE

C99:                bsr      SETUPA2
                    tst.w    d0
                    bne      C1
                    move.l   a0,S1DIG
                    tst.w    S1DEPTH
                    bne.s    C98
                    tst.w    S1SWAMP
                    bne.s    C98
                    bra      C2

C1:                 move.l   a0,S2DIG
                    tst.w    S2DEPTH
                    bne.s    C98
                    tst.w    S2SWAMP
                    bne.s    C98
C2:                 bsr      SETUPA2
                    clr.w    (a3)
                    cmp.l    (a2),d6
                    beq      lbC00942A
                    move.l   d4,(a2)
                    bra      _C4

lbC00942A:          move.l   d5,(a2)
_C4:                bra      C4

C3:                 tst.w    (a3)
                    bne      lbC00943C
                    addq.w   #1,(a3)
                    bra      C4

lbC00943C:          clr.w    (a3)
C4:                 cmp.w    #$14,d1
                    blt      C5
                    move.w   #$88,(a4)
                    move.l   d7,(a2)
                    clr.w    (a3)
                    bsr      SETSAFE
                    tst.w    d0
                    bne      lbC009460
                    move.w   #$30,(a5)
                    bra      lbC009464

lbC009460:          move.w   #$38,(a5)
lbC009464:          tst.w    d0
                    bne      lbC009484
                    tst.w    S1AUTO
                    beq      BUSY6_4
                    clr.w    S1HAND
                    addq.w   #1,S1BUCKET
                    bra      BUSY6_4

lbC009484:          tst.w    S2AUTO
                    beq      BUSY6_4
                    clr.w    S2HAND
                    addq.w   #1,S2BUCKET
                    bra      BUSY6_4

C5:                 move.w   d1,d3
                    add.w    #$5201,d3
                    bra      SETSTATE

SETUPA2:            tst.w    d0
                    bne      lbC0094E6
                    lea      S1FADDR,a2
                    lea      S1F,a3
                    move.l   S1DIG,a4
                    move.l   #S1HAND,a5
                    move.l   #S1SHCT,a6
                    move.l   #WPOURL1,d4
                    move.l   #WPOURR1,d5
                    move.l   #BUF11,d6
                    move.l   #BUF14,d7
                    rts

lbC0094E6:          lea      S2FADDR,a2
                    lea      S2F,a3
                    move.l   S2DIG,a4
                    move.l   #S2HAND,a5
                    move.l   #S2SHCT,a6
                    move.l   #BPOURL1,d4
                    move.l   #BPOURR1,d5
                    move.l   #BUF21,d6
                    move.l   #BUF24,d7
                    rts

BUSY53:             bsr      SETUPA2D
                    tst.w    d1
                    bne      D3_
                    moveq    #$12,d7
                    jsr      NEW_SOUND
                    bsr      ADD_TRAP
                    bra      D99

D98:                move.w   #0,d3
                    bra      SETSTATE

D99:                moveq    #$12,d7
                    jsr      NEW_SOUND
                    bsr      SETUPA2D
                    tst.w    d0
                    bne      D1_
                    move.l   a0,S1DIG
                    tst.w    S1DEPTH
                    bne.s    D98
                    tst.w    S1SWAMP
                    bne.s    D98
                    bra      D2_

D1_:                move.l   a0,S2DIG
                    tst.w    S2DEPTH
                    bne.s    D98
                    tst.w    S2SWAMP
                    bne.s    D98
D2_:                bsr      SETUPA2D
                    clr.w    (a3)
                    cmp.l    (a2),d6
                    beq      lbC009594
                    move.l   d4,(a2)
                    bra      _D4

lbC009594:          move.l   d5,(a2)
_D4:                bra      D4_

D3_:                tst.w    (a3)
                    bne      lbC0095A6
                    addq.w   #1,(a3)
                    bra      D4_

lbC0095A6:          clr.w    (a3)
D4_:                cmp.w    #$14,d1
                    blt      D5_
                    move.w   #$80,(a4)
                    move.l   d7,(a2)
                    clr.w    (a3)
                    bsr      SETSAFE
                    move.w   #$28,(a5)
                    tst.w    d0
                    bne      lbC0095E0
                    tst.w    S1AUTO
                    beq      BUSY6_4
                    clr.w    S1HAND
                    addq.w   #1,S1SAW
                    bra      BUSY6_4

lbC0095E0:          tst.w    S2AUTO
                    beq      BUSY6_4
                    clr.w    S2HAND
                    addq.w   #1,S2SAW
                    bra      BUSY6_4

D5_:                move.w   d1,d3
                    add.w    #$5301,d3
                    bra      SETSTATE

SETUPA2D:           tst.w    d0
                    bne      lbC009642
                    lea      S1FADDR,a2
                    lea      S1F,a3
                    move.l   S1DIG,a4
                    move.l   #S1HAND,a5
                    move.l   #S1SHCT,a6
                    move.l   #WSAWL1,d4
                    move.l   #WSAWR1,d5
                    move.l   #BUF11,d6
                    move.l   #BUF14,d7
                    rts

lbC009642:          lea      S2FADDR,a2
                    lea      S2F,a3
                    move.l   S2DIG,a4
                    move.l   #S2HAND,a5
                    move.l   #S2SHCT,a6
                    move.l   #BSAWL1,d4
                    move.l   #BSAWR1,d5
                    move.l   #BUF21,d6
                    move.l   #BUF24,d7
                    rts

BUSY54:             bsr      BUSY4_A
                    tst.w    d0
                    bne      lbC0096B2
                    lea      S1FADDR,a1
                    lea      S1F,a2
                    lea      S1ENERGY,a3
                    move.l   S1DIG,a4
                    lea      S2CT,a5
                    move.l   #BUF14,d5
                    move.l   #WHOLE1,d6
                    bra      BUSY54_1

lbC0096B2:          lea      S2FADDR,a1
                    lea      S2F,a2
                    lea      S2ENERGY,a3
                    move.l   S2DIG,a4
                    lea      S1CT,a5
                    move.l   #BUF24,d5
                    move.l   #BHOLE1,d6
BUSY54_1:           tst.w    d1
                    bne      lbC009712
                    tst.w    (a5)
                    bne      lbC0096EC
                    move.w   #$A00,(a5)
lbC0096EC:          move.l   #5,d7
                    jsr      NEW_SOUND
                    cmp.w    #$80,(a4)
                    bne      lbC009706
                    move.l   a4,a0
                    bsr      DELETE_TRAP
lbC009706:          move.l   d6,(a1)
                    clr.w    (a2)
                    move.w   #$5401,d3
                    bra      SETSTATE

lbC009712:          cmp.w    #1,d1
                    bne      lbC009728
                    move.l   #HOLE2,(a1)
                    move.w   #$5402,d3
                    bra      SETSTATE

lbC009728:          move.w   COUNTER,d3
                    and.w    #3,d3
                    bne      lbC009740
                    cmp.w    #3,(a2)
                    beq      lbC009740
                    addq.w   #1,(a2)
lbC009740:          move.w   d1,d3
                    add.w    #$5401,d3
BUSY54_2:           cmp.w    #$5404,d3
                    bne      BUSY54_3
                    subq.w   #4,(a3)
                    bra      SETSTATE

BUSY54_3:           cmp.w    #$5420,d3
                    bne      SETSTATE
                    move.l   d5,(a1)
                    clr.w    (a2)
                    bra      BUSY6_4

BUSY55:             bsr      SETUPA2E
                    tst.w    d1
                    bne      E3
                    moveq    #$11,d7
                    jsr      NEW_SOUND
                    bsr      ADD_TRAP
                    bra      E99

E98:                move.w   #0,d3
                    bra      SETSTATE

E99:                moveq    #$11,d7
                    jsr      NEW_SOUND
                    bsr      SETUPA2E
                    tst.w    d0
                    bne      E1
                    move.l   a0,S1DIG
                    tst.w    S1DEPTH
                    bne.s    E98
                    tst.w    S1SWAMP
                    bne.s    E98
                    bra      E2

E1:                 move.l   a0,S2DIG
                    tst.w    S2DEPTH
                    bne.s    E98
                    tst.w    S2SWAMP
                    bne.s    E98
E2:                 bsr      SETUPA2E
                    clr.w    (a3)
                    cmp.l    (a2),d6
                    beq      lbC0097DA
                    move.l   d4,(a2)
                    bra      _E4

lbC0097DA:          move.l   d5,(a2)
_E4:                bra      E4

E3:                 tst.w    (a3)
                    bne      lbC0097EC
                    addq.w   #1,(a3)
                    bra      E4

lbC0097EC:          clr.w    (a3)
E4:                 cmp.w    #$14,d1
                    blt      E5
                    move.w   #$98,(a4)
                    move.l   d7,(a2)
                    clr.w    (a3)
                    bsr      SETSAFE
                    move.w   #$40,(a5)
                    tst.w    d0
                    bne      lbC009826
                    tst.w    S1AUTO
                    beq      BUSY6_4
                    clr.w    S1HAND
                    addq.w   #1,S1PICK
                    bra      BUSY6_4

lbC009826:          tst.w    S2AUTO
                    beq      BUSY6_4
                    clr.w    S2HAND
                    addq.w   #1,S2PICK
                    bra      BUSY6_4

E5:                 move.w   d1,d3
                    add.w    #$5501,d3
                    bra      SETSTATE

SETUPA2E:           tst.w    d0
                    bne      lbC009888
                    lea      S1FADDR,a2
                    lea      S1F,a3
                    move.l   S1DIG,a4
                    move.l   #S1HAND,a5
                    move.l   #S1SHCT,a6
                    move.l   #WPICKL1,d4
                    move.l   #WPICKR1,d5
                    move.l   #BUF11,d6
                    move.l   #BUF14,d7
                    rts

lbC009888:          lea      S2FADDR,a2
                    lea      S2F,a3
                    move.l   S2DIG,a4
                    move.l   #S2HAND,a5
                    move.l   #S2SHCT,a6
                    move.l   #BPICKL1,d4
                    move.l   #BPICKR1,d5
                    move.l   #BUF21,d6
                    move.l   #BUF24,d7
                    rts

BUSY56:             bsr      BUSY4_A
                    tst.w    d0
                    bne      lbC0098F8
                    lea      S1FADDR,a1
                    lea      S1F,a2
                    lea      S1ENERGY,a3
                    move.l   S1DIG,a4
                    lea      S2CT,a5
                    move.l   #BUF14,d5
                    move.l   #WICICLED1,d6
                    bra      BUSY56_1

lbC0098F8:          lea      S2FADDR,a1
                    lea      S2F,a2
                    lea      S2ENERGY,a3
                    move.l   S2DIG,a4
                    lea      S1CT,a5
                    move.l   #BUF24,d5
                    move.l   #BICICLED1,d6
BUSY56_1:           tst.w    d1
                    bne      lbC00994A
                    moveq    #$13,d7
                    jsr      NEW_SOUND
                    tst.w    (a5)
                    bne      lbC00993A
                    move.w   #$A00,(a5)
lbC00993A:          move.w   #$198,(a4)
                    move.l   d5,(a1)
                    clr.w    (a2)
                    move.w   #$5601,d3
                    bra      SETSTATE

lbC00994A:          cmp.w    #1,d1
                    bne      lbC009966
                    move.w   #$1A0,(a4)
                    move.w   #$5602,d3
                    sub.w    #10,(a3)
                    bsr      SETSTATE
                    bra      BUSY4_A

lbC009966:          cmp.w    #$3C,d1
                    bgt      lbC00998A
                    move.l   d6,(a1)
                    addq.w   #1,(a2)
                    cmp.w    #3,(a2)
                    bne      lbC00997C
                    clr.w    (a2)
lbC00997C:          move.w   #$190,(a4)
                    move.w   d1,d3
                    add.w    #$5601,d3
                    bra      SETSTATE

lbC00998A:          clr.w    (a2)
                    move.l   d5,(a1)
                    bra      BUSY6_4

BUSY57:             tst.w    d0
                    bne      lbC0099F2
                    cmp.w    #3,S1WIN
                    beq      lbC0099A8
                    bsr      BUSY4_A
lbC0099A8:          and.w    #$FFFC,S1MAPX
                    and.w    #$FFFC,S1MAPY
                    lea      S1ENERGY,a1
                    lea      S1FADDR,a2
                    lea      S1F,a3
                    lea      S1DEPTH,a4
                    lea      S1ALTITUDE,a5
                    lea      S1SAFE,a6
                    move.l   S1DIG,a0
                    move.l   #BUF14,d6
                    move.l   #BUF12,d5
                    bra      BUSY57_0_5

lbC0099F2:          cmp.w    #3,S2WIN
                    beq      lbC009A02
                    bsr      BUSY4_A
lbC009A02:          and.w    #$FFFC,S2MAPX
                    and.w    #$FFFC,S2MAPY
                    lea      S2ENERGY,a1
                    lea      S2FADDR,a2
                    lea      S2F,a3
                    lea      S2DEPTH,a4
                    lea      S2ALTITUDE,a5
                    lea      S2SAFE,a6
                    move.l   S2DIG,a0
                    move.l   #BUF24,d6
                    move.l   #BUF22,d5
BUSY57_0_5:         tst.w    d1
                    bne      BUSY57_1
                    tst.w    d0
                    bne      lbC009A64
                    addq.w   #8,S1OFFSETX
                    addq.w   #2,S1OFFSETY
                    bra      lbC009A70

lbC009A64:          addq.w   #8,S2OFFSETX
                    addq.w   #2,S2OFFSETY
lbC009A70:          move.l   d6,(a2)
                    move.w   #$140,-2(a0)
                    move.w   #$148,(a0)
                    move.w   #$150,2(a0)
                    move.w   #$5701,d3
                    bra      SETSTATE

BUSY57_1:           cmp.w    #1,d1
                    bne      BUSY57_2
                    cmp.w    #$14,(a4)
                    bgt      BUSY57_1_5
                    addq.w   #2,(a4)
                    rts

BUSY57_1_5:         tst.w    d0
                    bne      lbC009AE2
                    tst.w    S1ROCKET
                    bne      lbC009AE0
                    cmp.w    #3,S1WIN
                    bne      lbC009B1E
                    move.l   #WROCKET1,RDATA
                    move.w   #1,S1ROCKET
                    move.w   #2,SOUNDCT
                    move.l   #$10009,d7
                    jsr      NEW_SOUND
lbC009AE0:          rts

lbC009AE2:          tst.w    S2ROCKET
                    bne.s    lbC009AE0
                    cmp.w    #3,S2WIN
                    bne      lbC009B1E
                    move.l   #BROCKET1,RDATA
                    move.w   #1,S2ROCKET
                    move.w   #2,SOUNDCT
                    move.l   #$10009,d7
                    jsr      NEW_SOUND
                    rts

lbC009B1E:          move.l   d5,(a2)
                    move.w   #$5702,d3
                    bra      SETSTATE

BUSY57_2:           cmp.w    #2,d1
                    bne      BUSY57_3
                    subq.w   #1,(a1)
                    bpl      lbC009B3A
                    move.w   #1,(a1)
lbC009B3A:          addq.w   #1,(a3)
                    and.w    #3,(a3)
                    subq.w   #4,(a4)
                    bmi      BUSY57_2_5
                    rts

BUSY57_2_5:         clr.w    (a4)
                    move.w   #$158,-2(a0)
                    move.w   #$160,(a0)
                    move.w   #$168,2(a0)
                    move.w   #$5703,d3
                    bra      SETSTATE

BUSY57_3:           cmp.w    #3,d1
                    bne      BUSY57_4
                    addq.w   #1,(a3)
                    and.w    #3,(a3)
                    cmp.w    #15,(a5)
                    bgt      BUSY57_3_5
                    addq.w   #5,(a5)
                    rts

BUSY57_3_5:         move.w   #$128,-2(a0)
                    move.w   #$130,(a0)
                    move.w   #$138,2(a0)
                    move.w   #$5704,d3
                    bra      SETSTATE

BUSY57_4:           addq.w   #1,(a3)
                    and.w    #3,(a3)
                    subq.w   #3,(a5)
                    bmi      BUSY57_4_5
                    rts

BUSY57_4_5:         clr.w    (a5)
                    move.w   #4,(a6)
                    tst.w    d0
                    bne      lbC009BBE
                    subq.w   #8,S1OFFSETX
                    subq.w   #2,S1OFFSETY
                    bra      _BUSY6_43

lbC009BBE:          subq.w   #8,S2OFFSETX
                    subq.w   #2,S2OFFSETY
_BUSY6_43:          bra      BUSY6_4

BUSY58:             tst.w    d0
                    bne      lbC009C0C
                    clr.w    S2TNTREADY
                    lea      S1FADDR,a1
                    lea      S1F,a2
                    lea      S1HAND,a3
                    move.l   #BUF11,d3
                    move.l   #WPLUNGER1,d4
                    move.l   #WPLUNGEL1,d5
                    move.l   #BUF14,d6
                    move.w   #$60,d7
                    bra      BUSY58_1

lbC009C0C:          clr.w    S1TNTREADY
                    lea      S2FADDR,a1
                    lea      S2F,a2
                    lea      S2HAND,a3
                    move.l   #BUF21,d3
                    move.l   #BPLUNGER1,d4
                    move.l   #BPLUNGEL1,d5
                    move.l   #BUF24,d6
                    move.w   #$58,d7
BUSY58_1:           tst.w    d1
                    bne      BUSY58_2
                    cmp.w    (a1),d3
                    beq      lbC009C52
                    move.l   d5,(a1)
                    bra      lbC009C54

lbC009C52:          move.l   d4,(a1)
lbC009C54:          clr.w    (a2)
                    move.w   #$5801,d3
                    bra      SETSTATE

BUSY58_2:           move.w   d1,d3
                    add.w    #$5801,d3
                    cmp.w    #$5808,d3
                    bgt      BUSY58_3
                    bne      SETSTATE
                    move.w   #1,(a2)
                    bra      SETSTATE

BUSY58_3:           cmp.w    #$5810,d3
                    bgt      BUSY58_4
                    bne      SETSTATE
                    clr.w    (a2)
                    bra      SETSTATE

BUSY58_4:           cmp.w    #$5818,d3
                    blt      SETSTATE
                    move.l   d6,(a1)
                    move.w   d7,(a3)
                    tst.w    d0
                    bne      lbC009CB6
                    tst.w    S1AUTO
                    beq      BUSY6_4
                    clr.w    S1HAND
                    addq.w   #1,S1PLUNGER
                    bra      BUSY6_4

lbC009CB6:          tst.w    S2AUTO
                    beq      BUSY6_4
                    clr.w    S2HAND
                    addq.w   #1,S2PLUNGER
                    bra      BUSY6_4

BUSY59:             bsr      BUSY4_A
                    tst.w    d0
                    bne      lbC009D0E
                    lea      S1DIG,a0
                    lea      S1FADDR,a1
                    lea      S1F,a2
                    lea      S1ENERGY,a3
                    lea      S1ALTITUDE,a4
                    lea      S2CT,a5
                    move.l   #BUF14,d4
                    move.l   #WBLUP1,d5
                    bra      BUSY59_1

lbC009D0E:          lea      S2DIG,a0
                    lea      S2FADDR,a1
                    lea      S2F,a2
                    lea      S2ENERGY,a3
                    lea      S2ALTITUDE,a4
                    lea      S1CT,a5
                    move.l   #BUF24,d4
                    move.l   #BBLUP1,d5
BUSY59_1:           tst.w    d1
                    bne      BUSY59_2
                    tst.w    (a5)
                    bne      lbC009D4E
                    move.w   #$A00,(a5)
lbC009D4E:          move.l   #4,d7
                    jsr      NEW_SOUND
                    move.l   d5,(a1)
                    clr.w    (a2)
                    sub.w    #10,(a3)
                    move.w   #$5901,d3
                    bra      SETSTATE

BUSY59_2:           move.w   COUNTER,d3
                    and.w    #1,d3
                    add.w    #1,d3
                    move.w   d3,(a2)
                    cmp.w    #1,d1
                    bne      BUSY59_3
                    addq.w   #1,(a4)
                    cmp.w    #6,(a4)
                    blt      BUSY59_9
                    move.w   #$5902,d3
                    bra      SETSTATE

BUSY59_3:           subq.w   #1,(a4)
                    bgt      BUSY59_9
                    move.l   d4,(a1)
                    clr.w    (a2)
                    move.l   #DUMMY_WORD,(a0)
                    move.w   #$5620,d3
                    bra      SETSTATE

BUSY59_9:           rts

DUMMY_WORD:         dc.w     0

GETMODE:            move.w   #2,BG_ONCE
                    move.w   #$1F4,DEMO_COUNT
GETM2:              bsr      TEST_OPTIONS
                    subq.w   #1,DEMO_COUNT
                    bne      lbC009E08
                    move.w   #$190,DEMO
                    jsr      PLAYINIT0
                    move.w   #1,S1AUTO
                    move.w   #1,S2AUTO
                    movem.l  d0-d7/a0-a6,-(sp)
                    bsr      CLEANTRAIL
                    bsr      DO_GAME
                    bsr      CLEANTRAIL
                    movem.l  (sp)+,d0-d7/a0-a6
                    clr.w    DEMO
                    bra.s    GETMODE

lbC009E08:          tst.w    BG_ONCE
                    beq      GETM2A
                    subq.w   #1,BG_ONCE
                    move.l   BACK,a0
                    move.l   SCREEN2,a1
                    move.w   #$1F3F,d0
lbC009E28:          move.l   (a0)+,(a1)+
                    dbra     d0,lbC009E28
                    move.w   #$40,d1
                    move.w   #10,d2
                    move.w   #$12,d6
                    move.w   #$3E,d7
                    move.l   SCREEN2,a0
                    jsr      RCLEARBLOCK
                    move.w   #$40,d1
                    move.w   #$6F,d2
                    move.w   #$12,d6
                    move.w   #$3E,d7
                    move.l   SCREEN2,a0
                    jsr      RCLEARBLOCK
                    move.w   #$6A,d1
                    move.w   #$15,d2
                    move.w   #7,d6
                    move.w   #$29,d7
                    move.l   #CONTROLS,a1
                    bsr      RSPRITER
                    move.w   #$6A,d1
                    move.w   #$74,d2
                    move.w   #7,d6
                    move.w   #$37,d7
                    move.l   #CONTROLS,a1
                    bsr      RSPRITER
                    move.w   #$40,d1
                    move.w   #$1B,d2
                    move.w   #2,d6
                    move.w   #$1E,d7
                    move.l   #lbL016EF0,a1
                    bsr      RSPRITER
                    move.w   #$40,d1
                    move.w   #$7E,d2
                    move.w   #2,d6
                    move.w   #$1E,d7
                    move.l   #lbL01A418,a1
                    bsr      RSPRITER
                    move.w   S1MODE,d1
                    move.w   S2MODE,d2
                    move.w   #6,d3
GETM2A:             bsr      DRAWITEMS
                    bsr      SWAPSCREEN
GETM3:              move.w   #$44,d0
                    jsr      INKEY
                    bne      GETM4
                    move.w   #$40,d0
                    jsr      INKEY
                    beq      GETM5
GETM4:              cmp.w    #6,d3
                    bne      lbC009F20
                    cmp.w    d2,d1
                    bne      lbC009F1E
                    tst.w    d1
                    bne      lbC009F1C
                    moveq    #3,d2
                    bra      lbC009F1E

lbC009F1C:          moveq    #4,d2
lbC009F1E:          rts

lbC009F20:          cmp.w    #2,d3
                    bhi      lbC009F3E
                    move.w   d3,d1
                    cmp.w    #2,d2
                    bne      _GETM2
                    cmp.w    d2,d1
                    bne      _GETM2
                    moveq    #0,d2
                    bra      _GETM2

lbC009F3E:          move.w   d3,d2
                    subq.w   #3,d2
                    cmp.w    #2,d2
                    bne      _GETM2
                    cmp.w    d2,d1
                    bne      _GETM2
                    moveq    #0,d1
_GETM2:             bra      GETM2

GETM5:              move.w   #$4C,d0
                    jsr      INKEY
                    beq      GETM6
lbC009F64:          move.w   #$4C,d0
                    jsr      INKEY
                    bne.s    lbC009F64
                    move.w   #$1F4,DEMO_COUNT
                    tst.w    d3
                    beq      GETM2
                    sub.w    #1,d3
                    bra      GETM2

GETM6:              move.w   #$4D,d0
                    jsr      INKEY
                    beq      GETM2
lbC009F94:          move.w   #$4D,d0
                    jsr      INKEY
                    bne.s    lbC009F94
                    move.w   #$1F4,DEMO_COUNT
                    cmp.w    #6,d3
                    beq      GETM2
                    add.w    #1,d3
                    bra      GETM2

DRAWITEMS:          clr.w    d4
DRAWITEMS1:         move.w   #$68,PX1
                    move.w   #$88,PX2
                    cmp.w    #3,d4
                    bge      lbC009FEC
                    move.w   d4,d6
                    mulu     #15,d6
                    add.w    #$14,d6
                    move.w   d6,PY1
                    move.w   d6,PY2
                    bra      lbC00A006

lbC009FEC:          move.w   d4,d6
                    sub.w    #3,d6
                    mulu     #15,d6
                    add.w    #$73,d6
                    move.w   d6,PY1
                    move.w   d6,PY2
lbC00A006:          move.w   #1,d5
                    cmp.w    d3,d4
                    bne      _DRAWLINE
                    move.w   #14,d5
_DRAWLINE:          bsr      DRAWLINE
                    add.w    #12,PY1
                    add.w    #12,PY2
                    bsr      DRAWLINE
                    sub.w    #$20,PX2
                    sub.w    #11,PY2
                    bsr      DRAWLINE
                    add.w    #$20,PX1
                    add.w    #$20,PX2
                    bsr      DRAWLINE
                    move.w   #1,d5
                    cmp.w    d1,d4
                    bne      lbC00A062
                    move.w   #7,d5
lbC00A062:          add.w    #3,d2
                    cmp.w    d2,d4
                    bne      lbC00A070
                    move.w   #7,d5
lbC00A070:          sub.w    #3,d2
                    move.w   #$69,PX1
                    move.w   #$87,PX2
                    cmp.w    #3,d4
                    bge      DRAWITEMS2
                    move.w   d4,d6
                    mulu     #15,d6
                    add.w    #$15,d6
                    move.w   d6,PY1
                    move.w   d6,PY2
                    bra      DRAWITEMS3

DRAWITEMS2:         move.w   d4,d6
                    sub.w    #3,d6
                    mulu     #15,d6
                    add.w    #$74,d6
                    move.w   d6,PY1
                    move.w   d6,PY2
DRAWITEMS3:         bsr      DRAWLINE
                    add.w    #10,PY1
                    add.w    #10,PY2
                    bsr      DRAWLINE
                    sub.w    #$1E,PX2
                    sub.w    #9,PY2
                    bsr      DRAWLINE
                    add.w    #$1E,PX1
                    add.w    #$1E,PX2
                    bsr      DRAWLINE
                    add.w    #1,d4
                    cmp.w    #6,d4
                    ble      DRAWITEMS1
                    rts

SELECTITEM:         rts

DRAWLINE:           movem.l  d0-d7/a0-a6,-(sp)
                    move.w   d5,d7
                    move.w   PX1,d1
                    move.w   PY1,d2
                    move.w   PX2,d3
                    move.w   PY2,d4
                    move.l   SCREEN2,a0
                    bsr      DRAW_LINE
                    movem.l  (sp)+,d0-d7/a0-a6
                    rts

PX1:                dc.w     0
PY1:                dc.w     0
PX2:                dc.w     0
PY2:                dc.w     0
BG_ONCE:            dc.w     0

RCLEARBLOCK:        movem.l  d0-d3/d7/a0/a2,-(sp)
                    addq.w   #1,d7
                    lsl.w    #3,d2
                    add.w    d2,a0
                    lsl.w    #2,d2
                    add.w    d2,a0
                    move.w   d1,d0
                    and.w    #$FFF8,d1
                    lsr.w    #3,d1
                    add.w    d1,a0
lbC00A160:          move.w   d6,d3
                    move.l   a0,a2
lbC00A164:          move.b   #$FF,(a0)+
                    move.b   #0,$1F3F(a0)
                    move.b   #0,$3E7F(a0)
                    move.b   #0,$5DBF(a0)
                    dbra     d3,lbC00A164
                    lea      $28(a2),a0
                    dbra     d7,lbC00A160
                    movem.l  (sp)+,d0-d3/d7/a0/a2
                    rts

BRAINMOVE:          clr.w    GOING_TO_KILL_YOU1
                    clr.w    GOING_TO_KILL_YOU2
                    move.w   #0,SPYDIR
                    move.w   #0,d1
                    move.w   #0,d2
                    tst.w    d0
                    bne      lbC00A21E
                    cmp.w    #3,S2WIN
                    beq      lbC00A1C6
                    cmp.w    #$33,S1ENERGY
                    bls      lbC00A1CC
lbC00A1C6:          clr.w    IN_TROUBLE1
lbC00A1CC:          tst.w    S1NUDGE
                    beq      lbC00A1DC
                    subq.w   #1,S1NUDGE
lbC00A1DC:          move.w   S1MAPX,d3
                    move.w   S1MAPY,d4
                    move.w   S1HAND,d5
                    move.w   S1SAFE,d6
                    move.l   S1CHOICEX,a2
                    move.l   S1CHOICEY,a3
                    move.l   #S1BDIR,a4
                    move.l   #S1RUN,a6
                    move.l   #S1CT,a1
                    tst.w    S1DEAD
                    beq      lbC00A28E
                    rts

lbC00A21E:          cmp.w    #3,S1WIN
                    bne      lbC00A236
                    cmp.w    #$33,S2ENERGY
                    bls      lbC00A23C
lbC00A236:          clr.w    IN_TROUBLE2
lbC00A23C:          tst.w    S2NUDGE
                    beq      lbC00A24C
                    subq.w   #1,S2NUDGE
lbC00A24C:          move.w   S2MAPX,d3
                    move.w   S2MAPY,d4
                    move.w   S2HAND,d5
                    move.w   S2SAFE,d6
                    move.l   S2CHOICEX,a2
                    move.l   S2CHOICEY,a3
                    move.l   #S2BDIR,a4
                    move.l   #S2RUN,a6
                    move.l   #S2CT,a1
                    tst.w    S2DEAD
                    beq      lbC00A28E
                    rts

lbC00A28E:          lsr.w    #2,d3
                    lsr.w    #2,d4
                    movem.l  d4,-(sp)
                    mulu     #$60,d4
                    add.w    d3,d4
                    lsl.w    #1,d4
                    add.l    #MAP,d4
                    move.l   d4,a0
                    movem.l  (sp)+,d4
BRAINMOVE0:         tst.w    d0
                    bne      lbC00A2C8
                    cmp.w    #$16,S1ENERGY
                    bhi      BMOVE1A
                    move.w   #$3C,IN_TROUBLE1
                    bra      BMOVE1A

lbC00A2C8:          cmp.w    #$16,S2ENERGY
                    bhi      BMOVE1A
                    move.w   #$3C,IN_TROUBLE2
BMOVE1A:            tst.w    d0
                    bne      lbC00A2FC
                    tst.w    IN_TROUBLE1
                    beq      lbC00A316
                    clr.w    S1BASTARD_COUNT
                    subq.w   #1,IN_TROUBLE1
                    bra      lbC00A408

lbC00A2FC:          tst.w    IN_TROUBLE2
                    beq      lbC00A316
                    clr.w    S2BASTARD_COUNT
                    subq.w   #1,IN_TROUBLE2
                    bra      lbC00A408

lbC00A316:          tst.w    d0
                    bne      lbC00A37C
                    tst.w    S2DEAD
                    bne      lbC00A35C
                    tst.w    S2SWAMP
                    bne      lbC00A35C
                    tst.w    S2DEPTH
                    bne      lbC00A35C
                    cmp.b    #$50,S2CT
                    beq      lbC00A368
                    cmp.b    #9,S2CT
                    beq      lbC00A368
                    tst.b    S2CT
                    beq      lbC00A368
lbC00A35C:          move.w   #0,S1BASTARD_COUNT
                    bra      lbC00A408

lbC00A368:          tst.w    S1BASTARD_COUNT
                    beq      lbC00A3DC
                    subq.w   #1,S1BASTARD_COUNT
                    bra      lbC00A56C

lbC00A37C:          tst.w    S1DEAD
                    bne      lbC00A3BC
                    tst.w    S1DEPTH
                    bne      lbC00A3BC
                    tst.w    S1SWAMP
                    bne      lbC00A3BC
                    cmp.b    #$50,S1CT
                    beq      lbC00A3C8
                    cmp.b    #9,S1CT
                    beq      lbC00A3C8
                    tst.b    S1CT
                    beq      lbC00A3C8
lbC00A3BC:          move.w   #0,S2BASTARD_COUNT
                    bra      lbC00A408

lbC00A3C8:          tst.w    S2BASTARD_COUNT
                    beq      lbC00A3DC
                    subq.w   #1,S2BASTARD_COUNT
                    bra      lbC00A56C

lbC00A3DC:          tst.w    SPYWIN
                    bne      lbC00A3EA
                    bra      lbC00A408

lbC00A3EA:          tst.w    d0
                    bne      lbC00A3FC
                    move.w   #5,S1BASTARD_COUNT
                    bra      lbC00A56C

lbC00A3FC:          move.w   #5,S2BASTARD_COUNT
                    bra      lbC00A56C

lbC00A408:          tst.w    d0
                    bne      lbC00A442
                    lea      S1WIN,a5
                    tst.w    S1STILL
                    bne      BRAINMOVE1
                    tst.w    S1TRAPNEXT
                    bne      BRAINMOVE1
                    tst.w    IN_TROUBLE1
                    beq      lbC00A476
lbC00A432:          move.l   #XIGLOO1,a2
                    move.l   #YIGLOO1,a3
                    bra      lbC00A638

lbC00A442:          lea      S2WIN,a5
                    tst.w    S2TRAPNEXT
                    bne      BRAINMOVE1
                    tst.w    S2STILL
                    bne      BRAINMOVE1
                    tst.w    IN_TROUBLE2
                    beq      lbC00A476
                    move.l   #XIGLOO2,a2
                    move.l   #YIGLOO2,a3
                    bra      lbC00A638

lbC00A476:          cmp.w    #3,(a5)
                    bne      lbC00A48E
                    move.l   #XHATCH,a2
                    move.l   #YHATCH,a3
                    bra      lbC00A638

lbC00A48E:          cmp.w    #8,d5
                    beq      lbC00A51E
                    tst.w    d0
                    bne      lbC00A4D0
                    tst.w    XWPLUNG
                    beq      lbC00A4B6
                    move.l   #XWPLUNG,a2
                    move.l   #YWPLUNG,a3
                    bra      lbC00A5C8

lbC00A4B6:          tst.w    XWBUCK
                    beq      lbC00A504
                    move.l   #XWBUCK,a2
                    move.l   #YWBUCK,a3
                    bra      lbC00A5C8

lbC00A4D0:          tst.w    XBPLUNG
                    beq      lbC00A4EA
                    move.l   #XBPLUNG,a2
                    move.l   #YBPLUNG,a3
                    bra      lbC00A5C8

lbC00A4EA:          tst.w    XBBUCK
                    beq      lbC00A504
                    move.l   #XBBUCK,a2
                    move.l   #YBBUCK,a3
                    bra      lbC00A5C8

lbC00A504:          tst.w    XCASE
                    beq      lbC00A56C
                    move.l   #XCASE,a2
                    move.l   #YCASE,a3
                    bra      lbC00A5C8

lbC00A51E:          tst.w    XCARD
                    beq      lbC00A538
                    move.l   #XCARD,a2
                    move.l   #YCARD,a3
                    bra      lbC00A5C8

lbC00A538:          tst.w    XGYRO
                    beq      lbC00A552
                    move.l   #XGYRO,a2
                    move.l   #YGYRO,a3
                    bra      lbC00A5C8

lbC00A552:          tst.w    XJAR
                    beq      lbC00A56C
                    move.l   #XJAR,a2
                    move.l   #YJAR,a3
                    bra      lbC00A5C8

lbC00A56C:          tst.w    d0
                    bne      lbC00A5E8
                    tst.w    S2IGLOO
                    bne      lbC00A432
                    move.w   #$FFFF,GOING_TO_KILL_YOU1
                    movem.l  d1/d2,-(sp)
                    move.w   S2CELLX,d1
                    move.w   S2CELLY,d2
                    addq.w   #1,d2
                    cmp.w    S1CELLX,d1
                    ble      lbC00A5A6
                    subq.w   #6,d1
                    bra      lbC00A5A8

lbC00A5A6:          addq.w   #8,d1
lbC00A5A8:          move.w   d1,XPLACE
                    move.w   d2,YPLACE
                    lea      XPLACE,a2
                    lea      YPLACE,a3
                    movem.l  (sp)+,d1/d2
                    bra      lbC00A638

lbC00A5C8:          move.w   (a2),XPLACE
                    move.w   (a3),YPLACE
                    move.l   #XPLACE,a2
                    move.l   #YPLACE,a3
                    bsr      FIND_A_MOUND
                    bra      lbC00A638

lbC00A5E8:          tst.w    S1IGLOO
                    bne      lbC00A432
                    move.w   #$FFFF,GOING_TO_KILL_YOU2
                    movem.l  d1/d2,-(sp)
                    move.w   S1CELLX,d1
                    move.w   S1CELLY,d2
                    cmp.w    S2CELLX,d1
                    ble      lbC00A61A
                    subq.w   #6,d1
                    bra      lbC00A61C

lbC00A61A:          addq.w   #8,d1
lbC00A61C:          move.w   d1,XPLACE
                    move.w   d2,YPLACE
                    lea      XPLACE,a2
                    lea      YPLACE,a3
                    movem.l  (sp)+,d1/d2
lbC00A638:          tst.w    d0
                    bne      lbC00A64E
                    move.l   a2,S1CHOICEX
                    move.l   a3,S1CHOICEY
                    bra      BRAINMOVE1

lbC00A64E:          move.l   a2,S2CHOICEX
                    move.l   a3,S2CHOICEY
BRAINMOVE1:         movem.l  d0-d6/a0-a6,-(sp)
                    tst.w    d0
                    bne      lbC00A696
                    clr.w    S1STILL
                    tst.w    S1NUDGE
                    bne      BRAINMOVE1_1
                    tst.w    S1SWAMP
                    bne      BM9
                    tst.w    S1DEPTH
                    bne      BM9
                    tst.w    GOING_TO_KILL_YOU1
                    bne      lbC00A6C4
                    bra      BM9

lbC00A696:          clr.w    S2STILL
                    tst.w    S2NUDGE
                    bne      BRAINMOVE1_1
                    tst.w    S2SWAMP
                    bne      BM9
                    tst.w    S2DEPTH
                    bne      BM9
                    tst.w    GOING_TO_KILL_YOU2
                    beq      BM9
lbC00A6C4:          move.w   S1MAPY,d1
                    sub.w    S2MAPY,d1
                    bge      lbC00A6D6
                    neg.w    d1
lbC00A6D6:          cmp.w    #1,d1
                    bgt      BM9
                    move.w   S1MAPX,d1
                    sub.w    S2MAPX,d1
                    bge      lbC00A6F0
                    neg.w    d1
lbC00A6F0:          cmp.w    #7,d1
                    blt      BM9
                    cmp.w    #15,d1
                    blt      lbC00A712
                    bsr      RNDER2
                    and.w    #7,d1
                    cmp.w    IQ,d1
                    bgt      BM9
lbC00A712:          tst.w    d0
                    bne      BBUSY_SB
BBUSY_SW:           move.w   S1MAPX,d1
                    cmp.w    S2MAPX,d1
                    bgt      lbC00A73E
                    move.w   S1MAPX,d1
                    addq.w   #2,d1
                    lea      BUFWR1,a0
                    move.w   #1,d3
                    bra      lbC00A752

lbC00A73E:          move.w   S1MAPX,d1
                    sub.w    #0,d1
                    lea      BUFWL1,a0
                    move.w   #$FFFF,d3
lbC00A752:          clr.w    S1F
                    move.l   a0,S1FADDR
                    move.w   S1MAPY,d2
                    move.w   d3,S1SAVE_DIR
                    lea      S1SAVE_ADD,a1
                    bra      BBUSY_S_SETUP

BBUSY_SB:           move.w   S2MAPX,d1
                    cmp.w    S1MAPX,d1
                    bgt      lbC00A79A
                    move.w   S2MAPX,d1
                    addq.w   #2,d1
                    lea      BUFBR1,a0
                    move.w   #1,d3
                    bra      lbC00A7AE

lbC00A79A:          move.w   S2MAPX,d1
                    sub.w    #0,d1
                    lea      BUFBL1,a0
                    move.w   #$FFFF,d3
lbC00A7AE:          clr.w    S2F
                    move.l   a0,S2FADDR
                    move.w   S2MAPY,d2
                    move.w   d3,S2SAVE_DIR
                    lea      S2SAVE_ADD,a1
BBUSY_S_SETUP:      lsr.w    #2,d2
                    mulu     MAXMAPX,d2
                    lsr.w    #2,d1
                    add.w    d2,d1
                    lsl.w    #1,d1
                    lea      MAP,a0
                    add.w    d1,a0
                    move.l   a0,(a1)
                    move.w   #$5000,d3
                    bsr      SETSTATE
                    move.w   #$55,DANGER
                    movem.l  (sp)+,d0-d6/a0-a6
                    move.w   #0,d1
                    move.w   #0,d2
                    move.w   #0,SPYDIR
                    rts

BM9:                bsr      BUSY6_4
BRAINMOVE1_0:       tst.w    SPYWIN
                    beq      BRAINMOVE1_1
                    movem.l  d0-d7/a0-a6,-(sp)
                    tst.w    d0
                    bne      lbC00A86E
                    tst.w    GOING_TO_KILL_YOU1
                    beq      lbC00A936
                    tst.w    S2DEAD
                    bne      lbC00A936
                    move.w   S1MAPX,d1
                    move.w   S1MAPY,d2
                    move.w   S2MAPX,d3
                    move.w   S2MAPY,d4
                    tst.w    S2SWAMP
                    beq      lbC00A8BA
                    tst.w    S2DEPTH
                    beq      lbC00A8BA
                    move.w   #1,S1RUN
                    bra      lbC00A936

lbC00A86E:          tst.w    GOING_TO_KILL_YOU2
                    beq      lbC00A936
                    tst.w    S1DEAD
                    bne      lbC00A936
                    move.w   S2MAPX,d1
                    move.w   S2MAPY,d2
                    move.w   S1MAPX,d3
                    move.w   S1MAPY,d4
                    tst.w    S1SWAMP
                    beq      lbC00A8BA
                    tst.w    S1DEPTH
                    beq      lbC00A8BA
                    move.w   #1,S2RUN
                    bra      lbC00A936

lbC00A8BA:          lsr.w    #2,d1
                    lsr.w    #2,d3
                    move.w   d2,d5
                    move.w   d4,d6
                    lsr.w    #4,d5
                    lsr.w    #4,d6
                    cmp.w    d5,d6
                    bne      lbC00A936
                    move.w   d1,d5
                    sub.w    d3,d5
                    move.w   d2,d6
                    sub.w    d4,d6
                    tst.w    d6
                    beq      lbC00A936
                    tst.w    d5
                    bpl      lbC00A8E2
                    neg.w    d5
lbC00A8E2:          cmp.w    #$40,d5
                    bhi      lbC00A936
                    tst.w    d6
                    bpl      lbC00A904
                    move.w   #0,d1
                    move.w   #1,d2
                    move.w   #4,SPYDIR
                    bra      lbC00A914

lbC00A904:          move.w   #0,d1
                    move.w   #$FFFF,d2
                    move.w   #3,SPYDIR
lbC00A914:          move.w   d1,TEMP1
                    move.w   d2,TEMP2
                    movem.l  (sp)+,d0-d7/a0-a6
                    movem.l  (sp)+,d0-d6/a0-a6
                    move.w   TEMP1,d1
                    move.w   TEMP2,d2
                    rts

lbC00A936:          movem.l  (sp)+,d0-d7/a0-a6
BRAINMOVE1_1:       movem.l  (sp)+,d0-d6/a0-a6
                    tst.w    d0
                    bne      lbC00A982
                    tst.w    S1NUDGE
                    bne      GO_MOVE
                    tst.w    S1IGLOO
                    beq      BRAINMOVE1_1A
                    cmp.w    #$3E,S1ENERGY
                    bge      lbC00A9C4
                    bsr      FIRE_IN_HERE
                    beq      lbC00A9C4
                    move.w   COUNTER,d7
                    and.w    #$FF,d7
                    cmp.w    #10,d7
                    beq      lbC00A9C4
                    bra      lbC00A9BC

lbC00A982:          tst.w    S2NUDGE
                    bne      GO_MOVE
                    tst.w    S2IGLOO
                    beq      BRAINMOVE1_1A
                    cmp.w    #$3E,S2ENERGY
                    bge      lbC00A9C4
                    bsr      FIRE_IN_HERE
                    beq      lbC00A9C4
                    move.w   COUNTER,d7
                    and.w    #$FF,d7
                    cmp.w    #$2C,d7
                    beq      lbC00A9C4
lbC00A9BC:          move.w   #1,(a4)
                    bra      GO_MOVE

lbC00A9C4:          cmp.w    #$F9,2(a0)
                    beq      lbC00A9F4
                    cmp.w    #$F9,4(a0)
                    beq      lbC00A9F4
                    cmp.w    #$F9,6(a0)
                    beq      lbC00A9F4
                    cmp.w    #$F9,8(a0)
                    beq      lbC00A9F4
                    move.w   #4,(a4)
                    bra      lbC00A9F8

lbC00A9F4:          move.w   #8,(a4)
lbC00A9F8:          tst.w    d0
                    bne      lbC00AA0A
                    move.w   #9,S1NUDGE
                    bra      GO_MOVE

lbC00AA0A:          move.w   #9,S2NUDGE
                    bra      GO_MOVE

BRAINMOVE1_1A:      tst.w    d0
                    bne      lbC00AA2A
                    tst.w    S1NUDGE
                    bne      GO_MOVE
                    bra      lbC00AA34

lbC00AA2A:          tst.w    S2NUDGE
                    bne      GO_MOVE
lbC00AA34:          move.w   d4,d6
                    move.w   (a3),d5
                    move.w   #0,d1
                    move.w   #0,d2
                    move.w   #0,SPYDIR
                    lsr.w    #2,d5
                    lsr.w    #2,d6
                    cmp.w    d5,d6
                    beq      BRAINMOVE2
                    cmp.w    #$20,(a4)
                    bne      lbC00AA5C
                    clr.w    (a4)
lbC00AA5C:          move.w   d4,d6
                    and.w    #$FFFC,d6
                    mulu     #$60,d6
                    add.w    d3,d6
                    lsl.w    #1,d6
                    add.l    #MAP,d6
                    move.l   d6,a5
                    move.w   (a5),d6
                    cmp.w    #$98,d6
                    beq      lbC00AA84
                    cmp.w    #$190,d6
                    bne      lbC00AA92
lbC00AA84:          cmp.w    (a3),d4
                    ble      lbC00AA92
                    move.w   #1,(a4)
                    bra      lbC00AB34

lbC00AA92:          cmp.w    #$188,$240(a5)
                    bne      lbC00AAAA
                    cmp.w    (a3),d4
                    bge      lbC00AAAA
                    move.w   #2,(a4)
                    bra      lbC00AB34

lbC00AAAA:          tst.w    d0
                    bne      lbC00AABE
                    tst.w    S1NUDGE
                    beq      _RNDER2
                    bra      lbC00AB10

lbC00AABE:          tst.w    S2NUDGE
                    bne      lbC00AB10
_RNDER2:            jsr      RNDER2
                    and.w    #3,d1
                    beq      lbC00AB10
                    move.l   a5,-(sp)
lbC00AAD8:          move.w   -(a5),d1
                    cmp.w    #$B1,d1
                    beq      lbC00AB0A
                    cmp.w    (a3),d4
                    ble      lbC00AB00
                    cmp.w    #$98,d1
                    beq      lbC00AAF6
                    cmp.w    #$190,d1
                    bne.s    lbC00AAD8
lbC00AAF6:          move.l   (sp)+,a5
                    move.w   #4,(a4)
                    bra      lbC00AB10

lbC00AB00:          cmp.w    #$188,$240(a5)
                    bne.s    lbC00AAD8
                    bra.s    lbC00AAF6

lbC00AB0A:          move.l   (sp)+,a5
                    move.w   #8,(a4)
lbC00AB10:          clr.l    d1
                    tst.w    (a4)
                    bne      lbC00AB34
                    movem.l  d0-d2,-(sp)
                    jsr      RNDER
                    move.w   d0,d4
                    and.w    #3,d4
                    move.w   #1,d0
                    lsl.w    d4,d0
                    move.w   d0,(a4)
                    movem.l  (sp)+,d0-d2
lbC00AB34:          move.w   (a4),d4
                    bsr      GETD0D1
                    rts

BRAINMOVE2:         move.l   d0,-(sp)
                    bsr      RNDER
                    move.w   d0,d7
                    move.l   (sp)+,d0
                    and.w    #3,d7
                    beq      lbC00AB88
                    cmp.w    (a2),d3
                    beq      lbC00AB88
lbC00AB54:          tst.w    d0
                    bne      lbC00AB68
                    tst.w    S1NUDGE
                    bne      GO_MOVE
                    bra      lbC00AB72

lbC00AB68:          tst.w    S2NUDGE
                    bne      GO_MOVE
lbC00AB72:          cmp.w    (a2),d3
                    blt      lbC00AB80
                    move.w   #4,(a4)
                    bra      GO_MOVE

lbC00AB80:          move.w   #8,(a4)
                    bra      GO_MOVE

lbC00AB88:          cmp.w    (a3),d4
                    beq      lbC00ABA4
                    cmp.w    (a3),d4
                    blt      lbC00AB9C
                    move.w   #1,(a4)
                    bra      GO_MOVE

lbC00AB9C:          move.w   #2,(a4)
                    bra      GO_MOVE

lbC00ABA4:          cmp.w    (a2),d3
                    bne.s    lbC00AB54
                    move.w   S1HAND,d5
                    move.w   S1BADMOVE,d7
                    tst.w    d0
                    beq      lbC00ABC6
                    move.w   S2HAND,d5
                    move.w   S2BADMOVE,d7
lbC00ABC6:          tst.w    d5
                    beq      lbC00AC08
                    tst.w    d0
                    bne      lbC00ABDE
                    move.w   #1,S1STILL
                    bra      lbC00ABE6

lbC00ABDE:          move.w   #1,S2STILL
lbC00ABE6:          tst.w    d7
                    bne      lbC00ABFC
                    cmp.w    #$27,(a0)
                    bgt      lbC00ABFC
                    cmp.w    #8,d5
                    beq      lbC00AC08
lbC00ABFC:          move.w   #$300,d3
                    bsr      SETSTATE
                    bra      lbC00AC4E

lbC00AC08:          tst.w    d0
                    bne      lbC00AC2C
                    tst.w    GOING_TO_KILL_YOU1
                    bne      lbC00AC4E
                    tst.w    S1TRAPNEXT
                    bne      lbC00AC4E
                    clr.w    S1STILL
                    bra      lbC00AC46

lbC00AC2C:          tst.w    GOING_TO_KILL_YOU2
                    bne      lbC00AC4E
                    tst.w    S2TRAPNEXT
                    bne      lbC00AC4E
                    clr.w    S2STILL
lbC00AC46:          move.w   #$200,d3
                    bsr      SETSTATE
lbC00AC4E:          clr.w    (a4)
                    move.w   #0,d1
                    move.w   #0,d2
                    move.w   #0,SPYDIR
                    rts

GO_MOVE:            move.w   (a4),d4
                    bsr      GETD0D1
                    rts

GETD0D1:            lsr.w    #1,d4
                    bcc      lbC00AC80
                    move.w   #$FFFF,d2
                    clr.w    d1
                    move.w   #3,SPYDIR
                    rts

lbC00AC80:          lsr.w    #1,d4
                    bcc      lbC00AC96
                    move.w   #1,d2
                    clr.w    d1
                    move.w   #4,SPYDIR
                    rts

lbC00AC96:          lsr.w    #1,d4
                    bcc      lbC00ACAC
                    move.w   #$FFFF,d1
                    clr.w    d2
                    move.w   #1,SPYDIR
                    rts

lbC00ACAC:          lsr.w    #1,d4
                    bcc      lbC00ACC2
                    move.w   #1,d1
                    clr.w    d2
                    move.w   #2,SPYDIR
                    rts

lbC00ACC2:          clr.w    d1
                    clr.w    d2
                    clr.w    SPYDIR
                    rts

BTEMP:              dc.w     0

BRAINBUSY:          tst.w    d0
                    bne      lbC00AD58
                    tst.w    S1IGLOO
                    bne      lbC00ADD8
                    move.w   S2TNTREADY,BTEMP
                    move.l   #S1BRAIN,a1
                    move.l   #S1BDIR,a2
                    move.l   #S1CT,a3
                    move.w   S1F,d3
                    move.w   S1MAPX,d6
                    move.w   S1MAPY,d7
                    move.l   #S1HAND,a4
                    move.l   #S1FUEL,a6
                    tst.w    S1TRAPNEXT
                    bne      BRAINBUSY0_5
                    tst.w    S2DEAD
                    bne      lbC00ADD8
                    tst.w    S1DROWN
                    bne      lbC00ADD8
                    tst.w    S1SWAMP
                    bne      lbC00ADD8
                    tst.w    S1DEPTH
                    bne      lbC00ADD8
                    tst.w    S1DEAD
                    beq      lbC00ADDA
                    rts

lbC00AD58:          tst.w    S2IGLOO
                    bne      lbC00ADD8
                    move.w   S1TNTREADY,BTEMP
                    move.l   #S2BRAIN,a1
                    move.l   #S2BDIR,a2
                    move.l   #S2CT,a3
                    move.w   S2F,d3
                    move.w   S2MAPX,d6
                    move.w   S2MAPY,d7
                    move.l   #S2HAND,a4
                    move.l   #S2FUEL,a6
                    tst.w    S2TRAPNEXT
                    bne      BRAINBUSY0_5
                    tst.w    S1DEAD
                    bne      lbC00ADD8
                    tst.w    S2DROWN
                    bne      lbC00ADD8
                    tst.w    S2SWAMP
                    bne      lbC00ADD8
                    tst.w    S2DEPTH
                    bne      lbC00ADD8
                    tst.w    S2DEAD
                    beq      lbC00ADDA
lbC00ADD8:          rts

lbC00ADDA:          move.l   d0,-(sp)
                    bsr      RNDER
                    move.w   d0,d4
                    move.l   (sp)+,d0
                    movem.l  d4/d6/d7,-(sp)
                    tst.w    BTEMP
                    bne      BRAINBUSY0
                    tst.w    TENMINS
                    bne      lbC00AE08
                    cmp.w    #1,ONEMINS
                    ble      lbC00AE58
lbC00AE08:          and.w    #$FF,d4
                    move.w   #$16,d6
                    sub.w    IQ,d6
                    sub.w    IQ,d6
                    move.w   S1ENERGY,d7
                    tst.w    d0
                    beq      lbC00AE2E
                    move.w   S2ENERGY,d7
lbC00AE2E:          cmp.w    #$23,d7
                    bgt      lbC00AE46
                    move.w   #$10,d6
                    sub.w    IQ,d6
                    sub.w    IQ,d6
lbC00AE46:          cmp.w    d6,d4
                    blt      BRAINBUSY0
                    cmp.w    #10,COUNTER
                    blt      BRAINBUSY0
lbC00AE58:          cmp.w    #$28,(a4)
                    blt      lbC00AE6C
                    and.w    #7,d4
                    cmp.w    #1,d4
                    beq      BRAINBUSY0
lbC00AE6C:          movem.l  (sp)+,d4/d6/d7
                    rts

BRAINBUSY0:         movem.l  (sp)+,d4/d6/d7
BRAINBUSY0_5:       tst.w    SPYWIN
                    bne      FULL_HANDS
                    tst.w    (a4)
                    beq      BB_2
                    cmp.w    #$27,(a4)
                    bgt      FULL_HANDS
                    tst.w    d0
                    bne      lbC00AEA0
                    move.w   #1,S1TRAPNEXT
                    bra      lbC00AEA8

lbC00AEA0:          move.w   #1,S2TRAPNEXT
lbC00AEA8:          move.w   #$300,d3
                    bra      SETSTATE

BB_2:               tst.w    BTEMP
                    beq      lbC00AEC2
                    move.w   #1,d4
                    bra      USED4

lbC00AEC2:          cmp.w    #10,COUNTER
                    blt      CHOOSE5
                    jsr      RNDER2
                    move.w   d1,d4
                    and.w    #7,d4
                    beq      CHOOSE5
                    cmp.w    #1,d4
                    beq      CHOOSE5
                    bra      USED4

CHOOSE5:            move.w   #7,d4
USED4:              movem.l  d4/a0,-(sp)
                    cmp.w    #7,d4
                    beq      ALLOW_D4
                    lea      S1SAVE_WINY,a0
                    tst.w    d0
                    beq      UD4A
                    lea      IGGY,a0
UD4A:               lsl.w    #1,d4
                    add.w    d4,a0
                    tst.w    (a0)
                    bne      ALLOW_D4
                    movem.l  (sp)+,d4/a0
                    addq.w   #1,d4
                    bra.s    USED4

ALLOW_D4:           movem.l  (sp)+,d4/a0
                    cmp.w    #5,d4
                    beq      BUSY6_4
                    cmp.w    #7,d4
                    bne      lbC00AF46
                    tst.w    MAP_TIME
                    beq      lbC00AF46
                    subq.w   #1,MAP_TIME
                    bra      BUSY6_4

lbC00AF46:          move.w   IQ,d3
                    addq.w   #1,d3
                    move.w   d3,MAP_TIME
                    tst.w    d0
                    bne      lbC00AF8C
                    clr.w    S1TRAPNEXT
                    tst.w    S1SWAMP
                    bne      FULL_HANDS
                    tst.w    S1DEPTH
                    bne      FULL_HANDS
                    move.w   d4,S1AIMMENU
                    move.w   #1,S1FLASH
                    clr.w    S1F
                    bra      lbC00AFBA

lbC00AF8C:          clr.w    S2TRAPNEXT
                    tst.w    S2SWAMP
                    bne      FULL_HANDS
                    tst.w    S2DEPTH
                    bne      FULL_HANDS
                    clr.w    S2F
                    move.w   d4,S2AIMMENU
                    move.w   #1,S2FLASH
lbC00AFBA:          move.w   #$1600,d3
                    bra      SETSTATE

FULL_HANDS:         move.w   (a4),d4
                    beq      lbC00AFD4
                    move.w   #$400,d3
                    bsr      SETSTATE
                    bra      lbC00AFD4

lbC00AFD4:          rts

BRAINBUSY1:         tst.w    (a3)
                    bne      lbC00AFFC
                    tst.w    d0
                    bne      lbC00AFEE
                    move.w   #0,S1F
                    bra      lbC00AFF6

lbC00AFEE:          move.w   #0,S2F
lbC00AFF6:          move.w   #$4005,d3
                    move.w   d3,(a3)
lbC00AFFC:          rts

RANDMOVE:           move.l   d0,-(sp)
                    tst.w    d0
                    bne      lbC00B014
                    tst.w    S1IGLOO
                    bne      lbC00B0F0
                    bra      _RNDER0

lbC00B014:          tst.w    S2IGLOO
                    bne      lbC00B0F0
_RNDER0:            jsr      RNDER
                    and.w    #$1F,d0
                    cmp.w    #12,d0
                    bne      lbC00B0F0
                    jsr      RNDER
                    move.w   #0,d1
                    btst     #6,d0
                    beq      lbC00B050
                    move.w   #1,d1
                    btst     #7,d0
                    beq      lbC00B050
                    neg.w    d1
lbC00B050:          move.w   #0,d2
                    tst.w    d1
                    bne      lbC00B068
                    move.w   #1,d2
                    btst     #9,d0
                    beq      lbC00B068
                    neg.w    d2
lbC00B068:          cmp.w    #$FFFF,d1
                    bne      lbC00B07C
                    move.w   #1,SPYDIR
                    bra      lbC00B0C0

lbC00B07C:          cmp.w    #1,d1
                    bne      lbC00B090
                    move.w   #2,SPYDIR
                    bra      lbC00B0C0

lbC00B090:          cmp.w    #$FFFF,d2
                    bne      lbC00B0A4
                    move.w   #3,SPYDIR
                    bra      lbC00B0C0

lbC00B0A4:          cmp.w    #1,d2
                    bne      lbC00B0B8
                    move.w   #4,SPYDIR
                    bra      lbC00B0C0

lbC00B0B8:          move.w   #0,SPYDIR
lbC00B0C0:          tst.w    d0
                    bne      _RNDER1
                    jsr      RNDER
                    and.w    #7,d0
                    move.w   d0,S1NUDGE
                    clr.w    d0
                    bra      lbC00B0F0

_RNDER1:            jsr      RNDER
                    and.w    #7,d0
                    move.w   d0,S2NUDGE
                    move.w   #1,d0
lbC00B0F0:          move.l   (sp)+,d0
                    rts

FIND_A_MOUND:       movem.l  d1-d7/a0/a1/a4/a5,-(sp)
                    clr.w    d7
                    move.w   (a2),d1
                    move.w   d1,a4
                    move.w   (a3),d2
                    move.w   d2,a5
                    lsl.w    #1,d2
                    mulu     MAXMAPX,d2
                    lea      MAP,a0
                    add.w    d2,a0
                    lsl.w    #1,d1
                    add.w    d1,a0
                    move.w   (a0),d5
                    move.w   a4,d1
                    move.w   a5,d2
                    and.w    #$1C,d2
                    and.w    #$70,d1
                    lsl.w    #1,d2
                    mulu     MAXMAPX,d2
                    lea      MAP,a0
                    add.w    d2,a0
                    lsl.w    #1,d1
                    add.w    d1,a0
                    move.w   a4,d1
                    move.w   a5,d2
                    and.w    #$1C,d2
                    and.w    #$70,d1
                    move.w   #3,d3
lbC00B148:          move.l   a0,a1
                    move.w   d1,d6
                    move.w   #15,d4
lbC00B150:          move.w   (a0)+,d5
                    beq      lbC00B182
                    cmp.w    d1,a4
                    bne      lbC00B162
                    cmp.w    d2,a5
                    beq      lbC00B19C
lbC00B162:          cmp.w    #$68,d5
                    bge      lbC00B182
                    btst     #1,d5
                    beq      lbC00B182
                    cmp.w    #$27,d5
                    blt      lbC00B182
                    btst     #2,d5
                    bne      lbC00B198
lbC00B182:          addq.w   #1,d1
                    dbra     d4,lbC00B150
                    lea      $C0(a1),a0
                    move.w   d6,d1
                    addq.w   #1,d2
                    dbra     d3,lbC00B148
                    bra      lbC00B1A0

lbC00B198:          move.w   #1,d7
lbC00B19C:          move.w   d1,(a2)
                    move.w   d2,(a3)
lbC00B1A0:          tst.w    d0
                    bne      lbC00B1B0
                    move.w   d7,S1BADMOVE
                    bra      lbC00B1B6

lbC00B1B0:          move.w   d7,S2BADMOVE
lbC00B1B6:          movem.l  (sp)+,d1-d7/a0/a1/a4/a5
                    rts

XPLACE:             dc.w     0
YPLACE:             dc.w     0
GOING_TO_KILL_YOU1: dc.w     0
GOING_TO_KILL_YOU2: dc.w     0
S1NUDGE:            dc.w     0
S2NUDGE:            dc.w     0
DANGER:             dc.w     $55
S1BASTARD_COUNT:    dc.w     0
S2BASTARD_COUNT:    dc.w     0
IN_TROUBLE1:        dc.w     0
IN_TROUBLE2:        dc.w     0

OLDXSUB:            or.b     #0,d0
OLDYSUB:            equ      *-2
SPECIAL_DELAY:      dc.w     0
MAP_TIME:           dc.w     0
DONE_MOVE:          dc.w     0
DEMO_COUNT:         dc.w     0
HATCH_RAND:         dc.w     0
HATCHAD:            dc.l     0
RDATA:              dc.l     0
RY:                 dc.w     0
RHT:                dc.w     0
RLEN:               dc.w     0
RYOFF:              dc.w     0
S1ROCKET:           dc.w     0
S2ROCKET:           dc.w     0
S1WIN:              dc.w     0
S2WIN:              dc.w     0
BRAINON:            dc.w     0
PATTERN:            dc.w     $FFFF
TEMP1:              dc.w     0
TEMP2:              dc.w     0
ABORT:              dc.w     0
RIGHTSHIFT:         dc.w     0
SNOW_LINE:          dc.w     0
TEMPKEY:            dcb.w    2,0
OLDSTACK:           dcb.w    2,0
PRIV:               dc.w     0
VBAT:               dcb.w    2,0
PALETTE:            dcb.w    $10,0
BACK:               dc.l     0
IN_CASE:            dc.b     0
lbB00B239:          dc.b     0
lbB00B23A:          dcb.b    2,0
DEMOCT:             dcb.b    2,0
DEMO:               dc.w     0
FILEPTR:            dc.w     0
SPYPIX:             dc.b     'spy1x.pi1',0
SPYPIX2:            dc.b     'spy2x.pi1',0
BACKPIX:            dc.b     'back.pi1',0,0
LANDPIX:            dc.b     'land.pi1',0,0
MAPPIX:             dc.b     'maps.pi1',0,0
S1TRAIL:            dcb.w    7,$FFFF
S1OFFSETX:          dc.w     0
S1OFFSETY:          dc.w     0
S1SAVE_ADD:         dc.l     0
S1SAVE_DIR:         dc.w     0
S1MODE:             dc.w     2
S1RUN:              dc.w     0
S1AUTO:             dc.w     0
S1BRAIN:            dc.w     0
S1BDIR:             dc.w     0
S1DEAD:             dc.w     0
S1DROWN:            dc.w     0
S1F:                dc.w     0
S1DIR:              dc.w     0
S1FADDR:            dc.l     0
S1MAPX:             dc.w     0
S1MAPY:             dc.w     0
S1CELLX:            dc.w     0
S1CELLY:            dc.w     0
S1OLDX:             dc.w     0
S1OLDY:             dc.w     0
S1CT:               dc.w     0
S1WATERCT:          dc.w     0
S1ENERGY:           dc.w     0
S1FUEL:             dc.w     0
S1BUMP:             dc.w     0
S1SAFE:             dc.w     0
S1TEMPHAND:         dc.w     0
S1HAND:             dc.w     0
S1DIG:              dc.l     0
S1SWAMP:            dc.w     0
S1DEPTH:            dc.w     0
S1ALTITUDE:         dc.w     0
B1TIME:             dc.w     0
S1SHCT:             dc.w     0
S1FLASH:            dc.w     0
S1MENU:             dc.w     1
BUF1X:              dc.w     0
BUF1Y:              dc.w     $FFFF
S1TREEX:            dc.w     0
S1TREEY:            dc.w     0
S1PLOTX:            dc.w     0
S1PLOTY:            dc.w     0
S1LINEX1:           dc.w     0
S1LINEY1:           dc.w     0
S1LINEX2:           dc.w     0
S1LINEY2:           dc.w     0
S1DRLINE:           dc.w     0
S1CHOICEX:          dc.l     0
S1CHOICEY:          dc.l     0
S1BASH:             dc.w     0
S1AIMMENU:          dc.w     0
S1TRAPNEXT:         dc.w     0
S1STILL:            dc.w     0
S1BADMOVE:          dc.w     0
S1IGLOO:            dc.w     0
S1SAVE_MAPX:        dc.w     0
S1SAVE_MAPY:        dc.w     0
S1SAVE_WINX:        dc.w     0
S1SAVE_WINY:        dc.w     0
S1PLUNGER:          dc.w     0
S1SAW:              dc.w     0
S1BUCKET:           dc.w     0
S1PICK:             dc.w     0
S1SNSH:             dc.w     0
S1TNT:              dc.w     0
S1TNTREADY:         dc.w     0
S2TRAIL:            dcb.w    7,$FFFF
S2RUN:              dc.w     0
S2OFFSETX:          dc.w     0
S2OFFSETY:          dc.w     0
S2SAVE_ADD:         dc.l     0
S2SAVE_DIR:         dc.w     0
S2MODE:             dc.w     0
S2AUTO:             dc.w     0
S2BRAIN:            dc.w     0
S2BDIR:             dc.w     0
S2DEAD:             dc.w     0
S2DROWN:            dc.w     0
S2F:                dc.w     0
S2DIR:              dc.w     0
S2FADDR:            dc.l     0
S2MAPX:             dc.w     0
S2MAPY:             dc.w     0
S2CELLX:            dc.w     0
S2CELLY:            dc.w     0
S2OLDX:             dc.w     0
S2OLDY:             dc.w     0
S2CT:               dc.w     0
S2WATERCT:          dc.w     0
S2ENERGY:           dc.w     0
S2FUEL:             dc.w     0
S2BUMP:             dc.w     0
S2SAFE:             dc.w     0
S2TEMPHAND:         dc.w     0
S2HAND:             dc.w     0
S2DIG:              dc.l     0
S2SWAMP:            dc.w     0
S2DEPTH:            dc.w     0
S2ALTITUDE:         dc.w     0
B2TIME:             dc.w     0
S2SHCT:             dc.w     0
S2FLASH:            dc.w     0
S2MENU:             dc.w     1
BUF2X:              dc.w     0
BUF2Y:              dc.w     $FFFF
S2TREEX:            dc.w     0
S2TREEY:            dc.w     0
S2PLOTX:            dc.w     0
S2PLOTY:            dc.w     0
S2LINEX1:           dc.w     0
S2LINEY1:           dc.w     0
S2LINEX2:           dc.w     0
S2LINEY2:           dc.w     0
S2DRLINE:           dc.w     0
S2CHOICEX:          dc.l     0
S2CHOICEY:          dc.l     0
S2BASH:             dc.w     0
S2AIMMENU:          dc.w     0
S2TRAPNEXT:         dc.w     0
S2STILL:            dc.w     0
S2BADMOVE:          dc.w     0
S2IGLOO:            dc.w     0
S2SAVE_MAPX:        dc.w     0
S2SAVE_MAPY:        dc.w     0
S2SAVE_WINX:        dc.w     0
S2SAVE_WINY:        dc.w     0
IGGY:               dc.w     0
S2PLUNGER:          dc.w     0
S2SAW:              dc.w     0
S2BUCKET:           dc.w     0
S2PICK:             dc.w     0
S2SNSH:             dc.w     0
S2TNT:              dc.w     0
S2TNTREADY:         dc.w     0
SOUNDNUM:           dc.w     0
SOUNDCT:            dc.w     0
SPYX:               dc.w     0
SPYY:               dc.w     0
SPYDIR:             dc.w     0
SPYWIN:             dc.w     0
SPYWX:              dc.w     0
SPYWY:              dc.w     0
WIN1X:              dc.w     0
WIN1Y:              dc.w     0
WIN2X:              dc.w     0
WIN2Y:              dc.w     0
CURMAXX:            dc.w     $3C
CURMAXY:            dc.w     $20
REFRESH:            dc.w     2
BULLET:             dc.w     4
BUSYDIR:            dc.w     0
COUNTER:            dc.w     1
TENMINS:            dc.w     0
ONEMINS:            dc.w     0
TENSECS:            dc.w     0
ONESECS:            dc.w     0
XROCKET:            dc.w     0
YROCKET:            dc.w     0
XMIDNOSE:           dc.w     0
YMIDNOSE:           dc.w     0
XMIDTAIL:           dc.w     0
YMIDTAIL:           dc.w     0
XNOSE:              dc.w     0
YNOSE:              dc.w     0
XMID:               dc.w     0
YMID:               dc.w     0
XTAIL:              dc.w     0
YTAIL:              dc.w     0
XCARD:              dc.w     0
YCARD:              dc.w     0
XGYRO:              dc.w     0
YGYRO:              dc.w     0
XJAR:               dc.w     0
YJAR:               dc.w     0
XCASE:              dc.w     0
YCASE:              dc.w     0
XHATCH:             dc.w     0
YHATCH:             dc.w     0
XIGLOO1:            dc.w     0
YIGLOO1:            dc.w     0
XIGLOO2:            dc.w     0
YIGLOO2:            dc.w     0
XWPLUNG:            dc.w     0
YWPLUNG:            dc.w     0
XBPLUNG:            dc.w     0
YBPLUNG:            dc.w     0
XWBUCK:             dc.w     0
YWBUCK:             dc.w     0
XBBUCK:             dc.w     0
YBBUCK:             dc.w     0
LOOPTIME:           dc.w     0
SCREEN_P:           dc.l     0
CHPLACE_P:          dc.l     0
CHSRC_P:            dc.l     0
FOURBITS:           dc.w     0
L_COLOR:            dc.w     14
COUNT:              dc.w     0
COLOR:              dc.w     0
WINX:               dc.w     0
WINY:               dc.w     0
ULX:                dc.w     0
ULY:                dc.w     0
OFFSET:             dc.w     0
PLAYERS:            dc.w     1
LEVEL:              dc.w     1
IQ:                 dc.w     1
DRAWSUB:            dc.w     0
MAXMAPX:            dc.w     0
MAXMAPY:            dc.w     0
MAP:                dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $12,0
lbL00B68A:          dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3F,0
                    dcb.l    $3C,0
MAP1:               dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,$1B1,$1B9,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,$1B1,$1B9,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,$F9,1,1,1,1,1,1,1
                    dc.w     1,1,$F9,0,0,0,0,0,1,1,1,1,1,1,1,1,$B1,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B1,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,$C9,$D1,$D9,$E1,$E9,0,0,0,0,0,$B1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,$B1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,0,0,0,0,0,0,0,0,0
                    dc.w     0,$188,0,0,0,0,0,0,0,0,0,0,0,$B1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,$B1,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,$190,0,0,0,0,1,1,1,1,1,0
                    dc.w     0,0,0,0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B1,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,$C9,$D1,$D9,$E1,$E9,0
                    dc.w     0,0,0,0,$B1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,$B1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     $B1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1
MAP2:               dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,$1B1,$1B9,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,$1B1,$1B9,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,$F9,1,1,1,1,1,1,1
                    dc.w     1,1,$F9,0,0,0,0,0,1,1,1,1,1,1,1,1,$B1,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,0,0,0,0,0,0,0,0
                    dc.w     $C0,$C0,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B1
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,0,0,0,0,0,0,0,0,$C0,$C0,0,0,0,0,0,0,0,0
                    dc.w     $C9,$D1,$D9,$E1,$E9,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     $B1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,0
                    dc.w     0,0,0,0,0,0,$C0,$C0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,$B1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,0,0,0,0,0,0,$1F0,$1F8
                    dc.w     0,0,0,0,$188,0,0,0,0,0,0,0,0,0,$B1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,$B1,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,$190,0,0,0,$B8
                    dc.w     $B8,$B8,$B8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B1,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,0,0,0,0,$101
                    dc.w     $109,$111,$119,$121,0,0,0,0,0,0,0,0,0,$B8,$B8,$B8
                    dc.w     $B8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,$B8,$B8,$B8,$B8,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,0,0,0,0,0,0,0,0,0,$188,0,0,0
                    dc.w     0,0,0,$1D0,$1D8,$1E0,$1E8,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,0
                    dc.w     $190,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B1,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,0,0,0,0,0,0,0,$C9,$D1,$D9,$E1,$E9
                    dc.w     0,0,0,0,0,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,0,0,0,0,0,0,0,0,0,0,0,0,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1
MAP3:               dc.w     $B1,$B0,0,0,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,$C0
                    dc.w     $C0,0,0,0,0,0,$C0,$C0,0,0,0,0,0,0,0,0,0,0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B1,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B1,$B1,0,0,0,0,0,0,0
                    dc.w     $C9,$D1,$D9,$E1,$E9,0,0,0,0,0,0,$C0,$C0,0,0,0,0,0
                    dc.w     $C0,$C0,0,0,0,0,0,0,0,0,0,0,$B1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,$B1
                    dc.w     $B1,$B1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,$C0,$C0
                    dc.w     0,0,0,0,0,$C0,$C0,0,0,0,0,0,0,0,0,$B1,$B1,$B1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,$1B1,$1B9,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,$1B1
                    dc.w     $1B9,1,1,1,1,1,1,1,1,1,$B1,$B1,$B1,$B1,0,0,$188,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,$1F0,$1F8,0,0,0,0,0,$1F0
                    dc.w     $1F8,0,0,0,$188,0,0,0,$B1,$B1,$B1,$B1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0
                    dc.w     0,$F9,1,1,1,1,1,1,1,1,1,$F9,0,0,0,0,0,1,1,1,1,1,1
                    dc.w     1,1,$B1,$B0,0,0,0,0,$190,0,0,0,0,$B8,$B8,$B8,$B8
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,$190,0,0,0,1,1
                    dc.w     1,1,1,0,0,0,0,0,0,0,0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B1,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B1,$B1,0,0,0,0,0,0,0,0,0,$B8,$B8
                    dc.w     $B8,$B8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     $C9,$D1,$D9,$E1,$E9,0,0,0,0,0,0,0,0,$B1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,$B1,$B1,$B1,0,0,0
                    dc.w     0,0,0,0,0,$B8,$B8,$B8,$B8,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,$B1,$B1
                    dc.w     $B1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,$B1
                    dc.w     $B1,$B1,$B1,0,0,0,0,0,0,0,$1D0,$1D8,$1E0,$1E8,0,0
                    dc.w     0,0,0,0,0,0,0,$188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,$B1,$B1,$B1,$B1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,$B1,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,0,0,0,1,1,1,1,1,0,0,0,0,$190
                    dc.w     0,0,$B8,$B8,$B8,$B8,0,0,0,0,0,0,0,$C0,$C0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B1,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,1,1,1,1,1,1,1,1,1
                    dc.w     1,$B1,$B1,0,0,0,$101,$109,$111,$119,$121,0,0,0,0
                    dc.w     0,0,0,$B8,$B8,$B8,$B8,0,0,0,0,0,0,0,$C0,$C0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,$B1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,$B1,$B1,$B1,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,$B8,$B8,$B8,$B8,0,0,0,0,0,0,0
                    dc.w     $C0,$C0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,$B1,$B1
                    dc.w     $B1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,$B1
                    dc.w     $B1,$B1,$B1,0,0,0,0,0,0,0,0,0,0,0,0,0,$1D0,$1D8
                    dc.w     $1E0,$1E8,0,0,0,0,$188,0,0,$1F0,$1F8,0,0,0,0,0,0
                    dc.w     0,0,0,$188,0,0,0,0,0,0,$B1,$B1,$B1,$B1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,$B1,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,0,0,0,0,1,1,1,1,1,0,0,0,0,$190,0,0,0,$B8,$B8
                    dc.w     $B8,$B8,0,0,0,0,0,0,$190,0,0,0,0,0,0,0,0,0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B1,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,$B1,$B1,0,0
                    dc.w     0,0,$101,$109,$111,$119,$121,0,0,0,0,0,0,0,0,$B8
                    dc.w     $B8,$B8,$B8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,$B1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,$B1,$B1,$B1,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,$B8,$B8,$B8,$B8,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     $B1,$B1,$B1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,$B1,$B1,$B1,$B1,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,$1D0,$1D8,$1E0,$1E8,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,$B1,$B1,$B1,$B1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1
MAP4:               dc.w     $B1,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,0,0,0,0,0
                    dc.w     0,1,1,1,1,1,0,0,0,0,0,0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B1,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,$B1,$B1,0,0,0,0,0,0,$101,$109,$111,$119,$121,0
                    dc.w     0,0,0,0,0,$B1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,$B1,$B1,$B1,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,$B1,$B1,$B1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,$1B1,$1B9,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,$1B1,$1B9,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,$B1,$B1,$B1,$B1,0
                    dc.w     0,0,0,$188,0,0,0,0,0,0,0,$B1,$B1,$B1,$B1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0
                    dc.w     0,0,0,$F9,1,1,1,1,1,1,1,1,1,$F9,0,0,0,0,0,1,1,1,1
                    dc.w     1,1,1,1,$B1,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,$190,0,1
                    dc.w     1,1,1,1,0,0,0,0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B1,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,1,1,1,1,1,1,1,1,1,1,$B1,$B1,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,$C9,$D1,$D9,$E1,$E9,0,0,0,0,$B1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,$B1,$B1,$B1,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,$B1,$B1
                    dc.w     $B1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,$B1,$B1,$B1
                    dc.w     $B1,0,$188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,$B1,$B1,$B1,$B1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,$B1,$B0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,$190,0,0,$C0,$C0,0,0,0,0,0,$C0,$C0
                    dc.w     0,0,0,0,$C0,$C0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1
                    dc.w     1,1,1,0,0,0,0,0,0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B1,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B1,$B1,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,$C0,$C0,0,0,0,0,0,$C0,$C0,0,0,0,0,$C0
                    dc.w     $C0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,$C9,$D1,$D9,$E1
                    dc.w     $E9,0,0,0,0,0,0,$B1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,$B1,$B1
                    dc.w     $B1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,$C0,$C0,0,0,0,0
                    dc.w     0,$C0,$C0,0,0,0,0,$C0,$C0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,$B1,$B1,$B1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,$B1,$B1,$B1,$B1,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     $188,0,0,$1F0,$1F8,0,0,0,0,0,$1F0,$1F8,0,0,0,0
                    dc.w     $1F0,$1F8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,$188,0,0
                    dc.w     0,0,0,0,0,$B1,$B1,$B1,$B1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     $B1,$B0,0,0,0,0,0,0,0,0,0,0,0,0,0,$190,0,$B8,$B8
                    dc.w     $B8,$B8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,$B8
                    dc.w     $B8,$B8,$B8,0,0,0,0,$190,0,0,0,0,0,0,0,0,0,0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B1,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B1,$B1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,$B8,$B8,$B8
                    dc.w     $B8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,$B8,$B8
                    dc.w     $B8,$B8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,$B1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,$B1,$B1,$B1,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,$B8,$B8,$B8,$B8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,$B8,$B8,$B8,$B8,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     $B1,$B1,$B1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,$B1,$B1,$B1,$B1
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,$1D0,$1D8,$1E0,$1E8,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,$188,0,0,0,0,0,0,$1D0,$1D8
                    dc.w     $1E0,$1E8,0,0,0,0,0,0,0,0,0,0,0,0,$B1,$B1,$B1,$B1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,$B1,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,0,0,0,1,1,1,1,1,0,0,0,$190,0,0,0,0,0
                    dc.w     $C0,$C0,0,0,0,0,0,0,0,0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B1,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,1,1,1,1,1,1,1,1,1,1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,0,0,0
                    dc.w     $101,$109,$111,$119,$121,0,0,0,0,0,0,0,0,0,$C0
                    dc.w     $C0,0,0,0,0,0,0,0,0,$B1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,$C0,$C0,0,0,0,0,0,0,$B1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     $1F0,$1F8,0,0,0,0,0,$B1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1
MAP5:               dc.w     $B1,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,0
                    dc.w     0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0,0,$B8,$B8,$B8,$B8
                    dc.w     0,0,0,0,0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B1,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,1,1,1
                    dc.w     1,1,1,1,1,1,1,$B1,$B1,0,0,0,0,0,$101,$109,$111
                    dc.w     $119,$121,0,0,0,0,0,0,0,0,$B8,$B8,$B8,$B8,0,0,0,0
                    dc.w     0,$B1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,$B1,$B1,$B1
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,$B8,$B8,$B8,$B8
                    dc.w     0,0,0,$B1,$B1,$B1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,$1B1,$1B9,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,$1B1,$1B9,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,$B1,$B1,$B1,$B1,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     $188,0,0,0,0,$1D0,$1D8,$1E0,$1E8,0,0,$B1,$B1,$B1
                    dc.w     $B1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,0,0,0,0,0,$F9,1,1,1,1,1,1,1,1,1,$F9,0,0,0
                    dc.w     0,0,1,1,1,1,1,1,1,1,$B1,$B0,0,0,0,0,0,0,0,0,0,$C0
                    dc.w     $C0,0,0,0,0,0,0,0,0,0,0,0,0,$190,0,0,0,0,$B8,$B8
                    dc.w     $B8,$B8,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0,0,0
                    dc.w     0,0,0,0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B1,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B1,$B1,0,0,0,0,0,0,0,0,0,$C0,$C0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,$B8,$B8,$B8,$B8,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,$C9,$D1,$D9,$E1,$E9,0,0,0,0,0
                    dc.w     0,0,0,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,0,0,0,0,0,0,0,0,$C0,$C0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,$B8,$B8,$B8,$B8,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,0,0,0,0,0,0,0,$1F0,$1F8,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,$1D0,$1D8,$1E0,$1E8,0
                    dc.w     0,0,0,0,0,0,0,0,0,$188,0,0,0,0,0,0,0,0,0,0,0,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B0,0,0,0,$190,0,0,0,0,0,1,1,1,1
                    dc.w     1,0,0,0,$A8,$A8,$A8,$A8,$A8,$A8,$A8,$A8,$A8,$A8
                    dc.w     $A8,$A8,$A8,$A8,$A8,$A8,$A8,$A8,$A8,$A8,$A8,$A8
                    dc.w     $A8,0,0,$190,0,0,0,0,0,0,0,0,0,0,0,0,0,0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B1,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B1
                    dc.w     $B1,0,0,0,0,0,0,0,0,0,$C9,$D1,$D9,$E1,$E9,0,0,0
                    dc.w     $A9,$A9,$A9,$A9,$A9,$A9,$A9,$A9,$A9,$A9,$A9,$A9
                    dc.w     $A9,$A9,$A9,$A9,$A9,$A9,$A9,$A9,$A9,$A9,$A9,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,$A9,$A9,$A9,$A9,$A9,$A9,$A9
                    dc.w     $A9,$A9,$A9,$A9,$A9,$A9,$A9,$A9,$A9,$A9,$A9,$A9
                    dc.w     $A9,$A9,$A9,$A9,$A9,$A9,$A9,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,$A9,$A9,$A9,$A9,$A9,$A9,$A9,$A9,$A9,$A9,$A9
                    dc.w     $A9,$A9,$A9,$A9,$A9,$A9,$A9,$A9,$A9,$A9,$A9,$A9
                    dc.w     $A9,$A9,$A9,$A9,$A9,0,0,0,$188,0,0,0,0,0,0,0,0
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B0,0,0,0,0,0,$B8,$B8,$B8,$B8
                    dc.w     0,0,0,0,0,0,$C0,$C0,0,0,0,0,0,0,0,1,1,1,1,1,0,0,0
                    dc.w     0,0,0,0,0,0,$B8,$B8,$B8,$B8,0,0,0,$190,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B1
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B1,$B1,0,0,0,0,0,$B8,$B8,$B8,$B8
                    dc.w     0,0,0,0,0,0,$C0,$C0,0,0,0,0,0,0,0,$101,$109,$111
                    dc.w     $119,$121,0,0,0,0,0,0,0,0,0,$B8,$B8,$B8,$B8,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,0,0,0,0
                    dc.w     $B8,$B8,$B8,$B8,0,0,0,0,0,0,$C0,$C0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,$B8,$B8,$B8,$B8,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,0
                    dc.w     0,0,$1D0,$1D8,$1E0,$1E8,0,0,0,0,0,0,$1F0,$1F8,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,$188,0,0,$1D0
                    dc.w     $1D8,$1E0,$1E8,0,0,0,0,0,0,0,0,0,0,0,0,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,0
                    dc.w     0,0,0,0,0,0,0,0,$C0,$C0,0,0,0,0,$190,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B1,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,0,0,0
                    dc.w     0,0,0,0,0,0,$C0,$C0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,0,0,0,0,0
                    dc.w     0,0,0,$C0,$C0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,0,0,0,0,0,0,0,0,$1F0,$1F8,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
MAP6:               dc.w     $B1,$B0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0,0,0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B1,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B1,$B1,0,0,0,$101,$109,$111,$119
                    dc.w     $121,0,0,0,0,0,0,0,0,0,$B1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,$B1,$B1,$B1,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,$B1,$B1,$B1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,$1B1,$1B9,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,$1B1,$1B9,1,1,1,1,1,1,1,1,1,$B1
                    dc.w     $B1,$B1,$B1,0,0,0,0,0,0,0,0,0,$188,0,0,$B1,$B1
                    dc.w     $B1,$B1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,0,0,0,0,0,$F9,1,1,1,1,1,1,1,1,1,$F9,0,0,0,0,0,1
                    dc.w     1,1,1,1,1,1,1,$B1,$B0,0,0,0,1,1,1,1,1,0,0,0,$190
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,$C0,$C0,0,0,0,0,0,$C0,$C0,0
                    dc.w     0,0,0,$B8,$B8,$B8,$B8,0,0,0,0,0,0,0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B1,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B1,$B1,0,0,0
                    dc.w     $101,$109,$111,$119,$121,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,$C0,$C0,0,0,0,0,0,$C0,$C0,0,0,0,0,$B8,$B8
                    dc.w     $B8,$B8,0,0,0,0,0,0,0,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,$C0,$C0,0,0,0,0,0,$C0,$C0,0
                    dc.w     0,0,0,$B8,$B8,$B8,$B8,0,0,0,0,0,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     $1F0,$1F8,0,0,0,0,0,$1F0,$1F8,$188,0,0,0,$1D0
                    dc.w     $1D8,$1E0,$1E8,0,0,$188,0,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,$B8,$B8,$B8,$B8,0,0,0,0,0,1,1,1,1,1
                    dc.w     0,0,$B8,$B8,$B8,$B8,0,0,0,0,0,0,$C0,$C0,0,0,$190
                    dc.w     0,0,0,0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B1,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B1,$B1,0,0,0,0,0,0,0,0,0,0,0,0,$B8,$B8,$B8
                    dc.w     $B8,0,0,0,0,0,$C9,$D1,$D9,$E1,$E9,0,0,$B8,$B8,$B8
                    dc.w     $B8,0,0,0,0,0,0,$C0,$C0,0,0,0,0,0,0,0,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,$B8,$B8,$B8,$B8,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,$B8,$B8,$B8,$B8,0,0,0,0,0,0,$C0,$C0,0,0,0,0
                    dc.w     0,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,0,0,0,0,0,0,0,$188,0,0,$1D0
                    dc.w     $1D8,$1E0,$1E8,0,0,0,0,0,0,0,0,0,0,0,0,$1D0,$1D8
                    dc.w     $1E0,$1E8,0,0,$188,0,0,0,$1F0,$1F8,0,0,0,0,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B0,0,0,0,0,0,0,0,0,0,$190,0,0,0,0,0,0,0
                    dc.w     $A8,$A8,$A8,$A8,$A8,$A8,$A8,$A8,$A8,$A8,$A8,$A8
                    dc.w     $A8,0,0,0,0,$190,0,1,1,1,1,1,0,$C0,$C0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B1,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B1,$B1,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,$A9,$A9,$A9,$A9,$A9,$A9,$A9,$A9,$A9
                    dc.w     $A9,$A9,$A9,$A9,0,0,0,0,0,0,$101,$109,$111,$119
                    dc.w     $121,0,$C0,$C0,0,0,0,0,0,0,0,0,0,0,0,0,0,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,$A9,$A9,$A9
                    dc.w     $A9,$A9,$A9,$A9,$A9,$A9,$A9,$A9,$A9,$A9,$A9,$A9
                    dc.w     $A9,0,0,0,0,0,0,0,0,0,0,0,$C0,$C0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,$A9,$A9,$A9,$A9,$A9,$A9,$A9,$A9,$A9,$A9,$A9
                    dc.w     $A9,$A9,$A9,$A9,$A9,$A9,$A9,0,0,0,0,0,0,0,0,0,0
                    dc.w     $1F0,$1F8,0,0,0,0,$188,0,0,0,0,0,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,0,0,0,0,0,0,0,0,$190,0,0
                    dc.w     0,0,0,0,0,0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B1
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1
MAP7:               dc.w     $B1,$B0,0,0,0,0,0,0,0,0,0,0,0,0,0,$C0,$C0,0,0,0,0
                    dc.w     0,0,0,0,0,$C0,$C0,0,0,0,0,0,0,$C0,$C0,0,0,0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B1,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B1,$B1,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,$C0,$C0,0,0,0,0,0,0,0,0,0,$C0,$C0
                    dc.w     0,0,0,0,0,0,$C0,$C0,0,0,0,$B1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,$B1,$B1
                    dc.w     $B1,0,0,0,0,0,0,0,0,0,0,0,0,$C0,$C0,0,0,0,0,0,0,0
                    dc.w     0,0,$C0,$C0,0,0,0,0,0,0,$C0,$C0,0,$B1,$B1,$B1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,$1B1,$1B9,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,$1B1
                    dc.w     $1B9,1,1,1,1,1,1,1,1,1,$B1,$B1,$B1,$B1,0,0,0,0,0
                    dc.w     0,0,$188,0,0,0,$1F0,$1F8,0,0,0,0,0,0,0,0,0,$1F0
                    dc.w     $1F8,0,0,0,0,0,0,$1F0,$1F8,$B1,$B1,$B1,$B1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0
                    dc.w     0,0,0,0,$F9,1,1,1,1,1,1,1,1,1,$F9,0,0,0,0,0,1,1,1
                    dc.w     1,1,1,1,1,$B1,$B0,0,1,1,1,1,1,0,0,0,$190,0,0,0
                    dc.w     $B8,$B8,$B8,$B8,0,0,0,0,0,0,0,0,0,0,0,0,$C0,$C0,0
                    dc.w     0,0,0,0,0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B1,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B1
                    dc.w     $B1,0,$101,$109,$111,$119,$121,0,0,0,0,0,0,0,$B8
                    dc.w     $B8,$B8,$B8,0,0,0,0,0,0,0,0,0,0,0,0,$C0,$C0,0,0,0
                    dc.w     0,0,0,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,0,0,0,0,0,0,0,0,0,0,0,0,$B8,$B8,$B8,$B8,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,$C0,$C0,0,0,0,0,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,$1D0,$1D8,$1E0,$1E8,0,0,0,0,0,0,0
                    dc.w     $188,0,0,0,0,$1F0,$1F8,0,0,0,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,$C0,$C0,0,0,0,$190,0,0
                    dc.w     $C0,$C0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0,0,0,$C0
                    dc.w     $C0,0,0,0,0,0,0,0,0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B1,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B1,$B1,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,$C0,$C0,0,0,0,0,0,0,$C0,$C0,0
                    dc.w     0,0,0,0,0,0,0,0,$C9,$D1,$D9,$E1,$E9,0,0,0,0,$C0
                    dc.w     $C0,0,0,0,0,0,0,0,0,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,$C0,$C0,0,0,0,0,0,0,$C0,$C0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,$C0,$C0,0,0,0
                    dc.w     0,0,0,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,0,0,0,0,0,0,0,0,0
                    dc.w     0,$188,0,0,0,0,0,0,$1F0,$1F8,0,0,0,0,0,0,$1F0
                    dc.w     $1F8,0,0,0,0,0,$188,0,0,0,0,0,0,0,0,0,0,0,0,$1F0
                    dc.w     $1F8,0,0,0,0,0,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,0,0,$190,0,1,1,1
                    dc.w     1,1,0,0,0,0,0,0,0,0,$B8,$B8,$B8,$B8,0,0,0,$190,0
                    dc.w     0,0,$B8,$B8,$B8,$B8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B1,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,0,0,0,0,$C9,$D1,$D9,$E1,$E9,0,0,0,0,0,0,0,0
                    dc.w     $B8,$B8,$B8,$B8,0,0,0,0,0,0,0,$B8,$B8,$B8,$B8,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,$B8,$B8,$B8,$B8,0,0,0,0,0,0,0,$B8
                    dc.w     $B8,$B8,$B8,0,0,0,0,0,0,0,0,0,0,0,0,0,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,$1D0,$1D8
                    dc.w     $1E0,$1E8,0,0,0,0,0,0,0,$1D0,$1D8,$1E0,$1E8,0,0,0
                    dc.w     0,0,0,0,$188,0,0,0,0,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,0,0,1,1,1,1,1,0,0,0,0,$C0,$C0
                    dc.w     0,0,0,0,0,0,$190,0,0,0,0,0,0,0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B1,$B0,$B0,$B0,$B0,$B0,$B0,$B0
                    dc.w     $B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B0,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,0,0,$101,$109,$111,$119,$121
                    dc.w     0,0,0,0,$C0,$C0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,0,0,0,0,0
                    dc.w     0,0,0,0,0,$C0,$C0,0,0,0,0,0,0,0,0,0,0,0,0,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,$B1,$B1,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,0,0,0
                    dc.w     0,0,0,0,0,0,$1F0,$1F8,0,0,0,0,0,0,0,0,0,0,0,$B1
                    dc.w     $B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1,$B1
                    dc.w     $B1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                    dc.w     1,1,1,$FFFF,$FFFF,$FFFF
TERRAIN:            dc.l     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.l     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.l     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.l     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.l     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.l     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.l     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.l     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.l     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.l     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
TERRAIN1:           dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,$3C,$3C,$3C,$3D,$3E,$3F
                    dc.w     $40,$41,$42,$43,$3C,$3C,0,0,0,0,$3C,$3C,$3C,$44
                    dc.w     $45,$46,$47,$48,$49,$4A,$3C,$3C,0,0,0,0,0,$FFFF,0
                    dc.w     0,0,0,0,0,0,0,0,0,1,2,3,4,5,6,7,8,9,10,11,12,13
                    dc.w     14,15,$10,$11,$12,$13,$32,$33,$34,$35,$36,$37,$38
                    dc.w     $39,$3A,$3B,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     $FFFF,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,$FFFF,0,0,0,0,0,0,0,0
                    dc.w     0,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,$10,$11
                    dc.w     $12,$13,$32,$33,$34,$35,$36,$37,$38,$39,$3A,$3B,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,$FFFF,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
TERRAIN2:           dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,$3C,$3C,$3C,$3D,$3E,$3F
                    dc.w     $40,$41,$42,$43,$3C,$3C,0,0,0,0,$3C,$3C,$3C,$44
                    dc.w     $45,$46,$47,$48,$49,$4A,$3C,$3C,0,0,0,0,0,$FFFF,0
                    dc.w     0,0,0,0,0,0,0,0,0,1,2,3,4,5,6,7,8,9,10,11,12,13
                    dc.w     14,15,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$1A
                    dc.w     $1B,$1C,$1D,$32,$33,$34,$35,$36,$37,$38,$39,$3A
                    dc.w     $3B,0,0,0,0,0,0,0,0,0,$FFFF,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     $FFFF,0,0,0,0,0,0,0,0,0,0,1,2,3,4,5,6,7,8,9,10,11
                    dc.w     12,13,14,15,$10,$11,$12,$13,$14,$15,$16,$17,$18
                    dc.w     $19,$1A,$1B,$1C,$1D,$32,$33,$34,$35,$36,$37,$38
                    dc.w     $39,$3A,$3B,0,0,0,0,0,0,0,0,0,$FFFF,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,$FFFF,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,1,2,3,4,5,6,7,8,9,$32,$33,$34,$35,$36,$37,$38
                    dc.w     $39,$3A,$3B,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     $FFFF,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0
TERRAIN3:           dc.w     0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,$10,$11,$12
                    dc.w     $13,$14,$15,$16,$17,$18,$19,$1A,$1B,$1C,$1D,$32
                    dc.w     $33,$34,$35,$36,$37,$38,$39,$3A,$3B,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,$3C,$3C,$3C,$3D
                    dc.w     $3E,$3F,$40,$41,$42,$43,$3C,$3C,0,0,0,0,$3C,$3C
                    dc.w     $3C,$44,$45,$46,$47,$48,$49,$4A,$3C,$3C,0,0,0,0,0
                    dc.w     0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,$10,$11,$12
                    dc.w     $13,$14,$15,$16,$17,$18,$19,$1A,$1B,$1C,$1D,$1E
                    dc.w     $1F,$20,$21,$22,$23,$24,$25,$26,$27,$32,$33,$34
                    dc.w     $35,$36,$37,$38,$39,$3A,$3B,0,0,0,0,0,0,0,0,0
                    dc.w     $FFFF,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,$10,$11,$12
                    dc.w     $13,$14,$15,$16,$17,$18,$19,$1A,$1B,$1C,$1D,$1E
                    dc.w     $1F,$20,$21,$22,$23,$24,$25,$26,$27,$32,$33,$34
                    dc.w     $35,$36,$37,$38,$39,$3A,$3B,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,3,4
                    dc.w     5,6,7,8,9,10,11,12,13,14,15,$10,$11,$12,$13,$14
                    dc.w     $15,$16,$17,$18,$19,$1A,$1B,$1C,$1D,$32,$33,$34
                    dc.w     $35,$36,$37,$38,$39,$3A,$3B,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
TERRAIN4:           dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,3,4
                    dc.w     5,6,7,8,9,$32,$33,$34,$35,$36,$37,$38,$39,$3A,$3B
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,$3C
                    dc.w     $3C,$3C,$3D,$3E,$3F,$40,$41,$42,$43,$3C,$3C,0,0,0
                    dc.w     0,$3C,$3C,$3C,$44,$45,$46,$47,$48,$49,$4A,$3C,$3C
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,3,4,5,6,7,8,9
                    dc.w     10,11,12,13,14,15,$10,$11,$12,$13,$32,$33,$34,$35
                    dc.w     $36,$37,$38,$39,$3A,$3B,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,3,4,5,6
                    dc.w     7,8,9,10,11,12,13,14,15,$10,$11,$12,$13,$14,$15
                    dc.w     $16,$17,$18,$19,$1A,$1B,$1C,$1D,$1E,$1F,$20,$21
                    dc.w     $22,$23,$24,$25,$26,$27,$28,$29,$2A,$2B,$2C,$2D
                    dc.w     $2E,$2F,$30,$31,$32,$33,$34,$35,$36,$37,$38,$39
                    dc.w     $3A,$3B,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,3,4,5,6,7,8,9
                    dc.w     10,11,12,13,14,15,$10,$11,$12,$13,$14,$15,$16,$17
                    dc.w     $18,$19,$1A,$1B,$1C,$1D,$1E,$1F,$20,$21,$22,$23
                    dc.w     $24,$25,$26,$27,$28,$29,$2A,$2B,$2C,$2D,$2E,$2F
                    dc.w     $30,$31,$32,$33,$34,$35,$36,$37,$38,$39,$3A,$3B,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
                    dc.w     $10,$11,$12,$13,$32,$33,$34,$35,$36,$37,$38,$39
                    dc.w     $3A,$3B,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
TERRAIN5:           dc.w     0,0,0,0,0,0,0,0,0,0,0,1,2,3,4,5,6,7,8,9,10,11,12
                    dc.w     13,14,15,$10,$11,$12,$13,$32,$33,$34,$35,$36,$37
                    dc.w     $38,$39,$3A,$3B,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,$3C,$3C,$3C,$3D,$3E,$3F,$40,$41,$42
                    dc.w     $43,$3C,$3C,0,0,0,0,$3C,$3C,$3C,$44,$45,$46,$47
                    dc.w     $48,$49,$4A,$3C,$3C,0,0,0,0,0,0,1,2,3,4,5,6,7,8,9
                    dc.w     10,11,12,13,14,15,$10,$11,$12,$13,$14,$15,$16,$17
                    dc.w     $18,$19,$1A,$1B,$1C,$1D,$1E,$1F,$20,$21,$22,$23
                    dc.w     $24,$25,$26,$27,$28,$29,$2A,$2B,$2C,$2D,$2E,$2F
                    dc.w     $30,$31,$32,$33,$34,$35,$36,$37,$38,$39,$3A,$3B,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,1,2,3,4,5,6,7,8,9,$32,$33
                    dc.w     $34,$35,$36,$37,$38,$39,$3A,$3B,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,1,2,3,4,5,6,7,8,9,$32,$33
                    dc.w     $34,$35,$36,$37,$38,$39,$3A,$3B,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,$10,$11
                    dc.w     $12,$13,$14,$15,$16,$17,$18,$19,$1A,$1B,$1C,$1D
                    dc.w     $1E,$1F,$20,$21,$22,$23,$24,$25,$26,$27,$28,$29
                    dc.w     $2A,$2B,$2C,$2D,$2E,$2F,$30,$31,$32,$33,$34,$35
                    dc.w     $36,$37,$38,$39,$3A,$3B,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,3,4,5,6
                    dc.w     7,8,9,10,11,12,13,14,15,$10,$11,$12,$13,$32,$33
                    dc.w     $34,$35,$36,$37,$38,$39,$3A,$3B,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0
TERRAIN6:           dc.w     0,1,2,3,4,5,6,7,8,9,$32,$33,$34,$35,$36,$37,$38
                    dc.w     $39,$3A,$3B,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     $3C,$3C,$3C,$3D,$3E,$3F,$40,$41,$42,$43,$3C,$3C,0
                    dc.w     0,0,0,$3C,$3C,$3C,$44,$45,$46,$47,$48,$49,$4A,$3C
                    dc.w     $3C,0,0,0,0,0,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14
                    dc.w     15,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$1A
                    dc.w     $1B,$1C,$1D,$1E,$1F,$20,$21,$22,$23,$24,$25,$26
                    dc.w     $27,$32,$33,$34,$35,$36,$37,$38,$39,$3A,$3B,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,3,4,5,6
                    dc.w     7,8,9,10,11,12,13,14,15,$10,$11,$12,$13,$14,$15
                    dc.w     $16,$17,$18,$19,$1A,$1B,$1C,$1D,$1E,$1F,$20,$21
                    dc.w     $22,$23,$24,$25,$26,$27,$32,$33,$34,$35,$36,$37
                    dc.w     $38,$39,$3A,$3B,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,1,2,3,4,5,6,7,8,9,$32,$33,$34,$35,$36
                    dc.w     $37,$38,$39,$3A,$3B,0,0,0,0,0,0,0,0,0,0,0,1,2,3,4
                    dc.w     5,6,7,8,9,10,11,12,13,14,15,$10,$11,$12,$13,$32
                    dc.w     $33,$34,$35,$36,$37,$38,$39,$3A,$3B,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,3,4,5
                    dc.w     6,7,8,9,$32,$33,$34,$35,$36,$37,$38,$39,$3A,$3B,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0
TERRAIN7:           dc.w     0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,$10,$11,$12
                    dc.w     $13,$14,$15,$16,$17,$18,$19,$1A,$1B,$1C,$1D,$32
                    dc.w     $33,$34,$35,$36,$37,$38,$39,$3A,$3B,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,$3C,$3C,$3C,$3D
                    dc.w     $3E,$3F,$40,$41,$42,$43,$3C,$3C,0,0,0,0,$3C,$3C
                    dc.w     $3C,$44,$45,$46,$47,$48,$49,$4A,$3C,$3C,0,0,0,0,0
                    dc.w     0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,$10,$11,$12
                    dc.w     $13,$14,$15,$16,$17,$18,$19,$1A,$1B,$1C,$1D,$32
                    dc.w     $33,$34,$35,$36,$37,$38,$39,$3A,$3B,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,$10,$11,$12
                    dc.w     $13,$14,$15,$16,$17,$18,$19,$1A,$1B,$1C,$1D,$1E
                    dc.w     $1F,$20,$21,$22,$23,$24,$25,$26,$27,$28,$29,$2A
                    dc.w     $2B,$2C,$2D,$2E,$2F,$30,$31,$32,$33,$34,$35,$36
                    dc.w     $37,$38,$39,$3A,$3B,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
                    dc.w     $10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$1A,$1B
                    dc.w     $1C,$1D,$1E,$1F,$20,$21,$22,$23,$24,$25,$26,$27
                    dc.w     $32,$33,$34,$35,$36,$37,$38,$39,$3A,$3B,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,1,2,3,4,5,6,7,8,9,10,11
                    dc.w     12,13,14,15,$10,$11,$12,$13,$32,$33,$34,$35,$36
                    dc.w     $37,$38,$39,$3A,$3B,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0

                    SECTION  ArcticAntics014A50,BSS,CHIP
STRENGTH_EMPTY:     ds.l     $82
STRENGTH_FULL:      ds.l     $82
SNOWSLAB:           ds.l     $280
SLEETSLAB:          ds.l     $280
DRYSLAB:            ds.l     $280
BUF11:              ds.l     $A4
lbL016EF0:          ds.l     $1EC
BUF12:              ds.l     $290
BUF13:              ds.l     $290
BUF14:              ds.l     $290
BUF15:              ds.w     1
BUF16:              ds.l     $78
BUF17:              ds.w     1
BUF18:              ds.w     1
BUF19:              ds.l     $A4
BUF1A:              ds.l     $A4
BUF1B:              ds.w     1
BUF1D:              ds.l     $A4
lbL019EF8:          ds.l     $A4
BUF21:              ds.l     $A4
lbL01A418:          ds.l     $1EC
BUF22:              ds.l     $290
BUF23:              ds.l     $290
BUF24:              ds.l     $290
BUF25:              ds.w     1
BUF26:              ds.l     $78
BUF27:              ds.w     1
BUF28:              ds.w     1
BUF29:              ds.l     $A4
BUF2A:              ds.l     $A4
BUF2B:              ds.w     1
BUF2D:              ds.l     $A4
lbL01D420:          ds.l     $A4
GRAVE:              ds.l     $A4
LAND:               ds.l     $2616
MAPS:               ds.l     $F60
RTCOVER:            ds.l     $100
MAPBOX:             ds.l     $18
S1BACK:             ds.l     $580
S2BACK:             ds.l     $580
MAPSPOT:            ds.l     $18
G_MOUND:            ds.l     14
G_CASE:             ds.l     $14
G_CARD:             ds.l     $14
G_URANIUM:          ds.l     $14
G_GYRO:             ds.l     $14
G_SAW:              ds.l     $14
G_WHITE_BUCKET:     ds.l     $14
G_BLACK_BUCKET:     ds.l     $14
G_PICKAXE:          ds.l     $16
G_SNOWSHOES:        ds.l     $12
G_TNT:              ds.l     $18
G_BLACK_PLUNGER:    ds.l     $14
G_WHITE_PLUNGER:    ds.l     $14
G_ICEHOLE:          ds.l     10
G_ICEPATCH:         ds.l     $12
G_DOORWAY:          ds.l     $3A
G_IGLOO_LEFT1:      ds.l     $46
G_IGLOO_LEFT2:      ds.l     $46
G_IGLOO_LEFT3:      ds.l     $46
G_IGLOO_LEFT4:      ds.l     $46
G_IGLOO_LEFT5:      ds.l     $46
G_IGLOO_RIGHT1:     ds.l     $46
G_IGLOO_RIGHT2:     ds.l     $46
G_IGLOO_RIGHT3:     ds.l     $46
G_IGLOO_RIGHT4:     ds.l     $46
G_IGLOO_RIGHT5:     ds.l     $46
G_POLAR_BEARA1:     ds.l     $16
G_POLAR_BEARA2:     ds.l     $16
G_POLAR_BEARA3:     ds.l     $16
G_POLAR_BEARB1:     ds.l     $3A
G_POLAR_BEARB2:     ds.l     $3A
G_POLAR_BEARB3:     ds.l     $3A
G_POLAR_BEARC1:     ds.l     $3E
G_POLAR_BEARC2:     ds.l     $3E
G_POLAR_BEARC3:     ds.l     $3E
G_EXIT_SOUTH:       ds.l     6
G_EXIT_NORTH1:      ds.l     $38
G_EXIT_NORTH2:      ds.l     $38
G_EXIT_NORTH3:      ds.l     $38
G_FIREL1:           ds.l     $1C
G_FIRER1:           ds.l     $1C
G_FIREL2:           ds.l     $1C
G_FIRER2:           ds.l     $1C
G_SNOW1:            ds.l     $40
G_SNOW2:            ds.l     $40
G_SNOW3:            ds.l     $40
G_SNOW4:            ds.l     $40
G_SNOW5:            ds.l     $40
G_ICE1:             ds.l     $40
G_ICE2:             ds.l     $40
G_CARDR:            ds.l     $12
G_SAWR:             ds.l     14
G_PICKAXER:         ds.l     $16
G_TNTR:             ds.l     $18
G_EMPTY:            ds.l     6
G_FULL:             ds.l     6
G_HOLD_BACK:        ds.l     $58
G_SNOWBALL:         ds.l     10
WSPYSAT:            ds.l     $78
BSPYSAT:            ds.l     $78
WPOURR1:            ds.l     $A4
WPOURR2:            ds.l     $A4
WPOURL1:            ds.l     $A4
WPOURL2:            ds.l     $A4
BPOURL1:            ds.l     $A4
BPOURL2:            ds.l     $A4
BPOURR1:            ds.l     $A4
BPOURR2:            ds.l     $A4
WPLUNGER1:          ds.l     $A4
WPLUNGER2:          ds.l     $A4
WPLUNGEL1:          ds.l     $A4
WPLUNGEL2:          ds.l     $A4
BPLUNGER1:          ds.l     $A4
BPLUNGER2:          ds.l     $A4
BPLUNGEL1:          ds.l     $A4
BPLUNGEL2:          ds.l     $A4
WLAUGH1:            ds.l     $A4
WLAUGH2:            ds.l     $A4
BLAUGH1:            ds.l     $A4
BLAUGH2:            ds.l     $A4
WBLUP1:             ds.l     $A4
WBLUP2:             ds.l     $A4
WBLUP3:             ds.l     $A4
BBLUP1:             ds.l     $A4
BBLUP2:             ds.l     $A4
BBLUP3:             ds.l     $A4
WPICKR1:            ds.l     $A4
WPICKR2:            ds.l     $A4
WPICKL1:            ds.l     $A4
WPICKL2:            ds.l     $A4
BPICKL1:            ds.l     $A4
BPICKL2:            ds.l     $A4
BPICKR1:            ds.l     $A4
BPICKR2:            ds.l     $A4
WSAWR1:             ds.l     $A4
WSAWR2:             ds.l     $A4
WSAWL1:             ds.l     $A4
WSAWL2:             ds.l     $A4
BSAWL1:             ds.l     $A4
BSAWL2:             ds.l     $A4
BSAWR1:             ds.l     $A4
BSAWR2:             ds.l     $A4
WHOLE1:             ds.l     $A4
HOLE2:              ds.l     $A4
HOLE3:              ds.l     $A4
HOLE4:              ds.l     $A4
HOLE5:              ds.l     $A4
BHOLE1:             ds.l     $A4
WICICLED1:          ds.l     $A4
WICICLED2:          ds.l     $A4
WICICLED3:          ds.l     $A4
BICICLED1:          ds.l     $A4
BICICLED2:          ds.l     $A4
BICICLED3:          ds.l     $A4
WROCKET1:           ds.l     $128
WROCKET2:           ds.l     $128
BROCKET1:           ds.l     $128
BROCKET2:           ds.l     $128
BUFWL1:             ds.l     $78
BUFWL2:             ds.l     $78
BUFWL3:             ds.l     $78
BUFWR1:             ds.l     $78
BUFWR2:             ds.l     $78
BUFWR3:             ds.l     $78
BUFBL1:             ds.l     $78
BUFBL2:             ds.l     $78
BUFBL3:             ds.l     $78
BUFBR1:             ds.l     $78
BUFBR2:             ds.l     $78
BUFBR3:             ds.l     $78
GO_H:               ds.l     $7E
GO_D:               ds.l     $7E
SHOW_H:             ds.l     $2A
SHOW_D:             ds.l     $2A
HIDE_H:             ds.l     $2A
HIDE_D:             ds.l     $2A
MISSILE_H:          ds.l     $70
MISSILE_D:          ds.l     $70
WSPY_D:             ds.l     $74
BSPY_D:             ds.l     $74
WSPY_H:             ds.l     $74
BSPY_H:             ds.l     $74
COMP_H:             ds.l     $78
COMP_D:             ds.l     $78
NUMS:               ds.l     $3C
SUBXW:              ds.w     1
SUBXB:              ds.w     1
SUBHTW:             ds.w     1
SUBHTB:             ds.w     1
SPLASH_BACK:        ds.w     1
SPLASH_COUNT:       ds.w     1
ALTER_PALETTE_FLAG: ds.w     1
FADE_FLAG:          ds.w     1
FADE_VALUE:         ds.w     1
TSAVE_INT:          ds.l     1
TINT_FLAG:          ds.w     1
TINT_REQ:           ds.w     1
TSCREEN1:           ds.l     1
TSCREEN2:           ds.l     1
TSCREEN3:           ds.l     1
TSCREEN4:           ds.l     1
TFILEPTR:           ds.w     1
TBACK:              ds.l     1
THANDLE:            ds.l     1
TLENGTH:            ds.l     1
TOLD_VAL_INT:       ds.l     1
TINT_VECTOR:        ds.l     1
TPALETTE:           ds.l     8
TCOUNTER:           ds.w     1
TFERDINAND:         ds.w     1
TSAVE_KEY:          ds.l     1
TSAVE_ADD:          ds.l     1
TSAVE_STACK:        ds.l     1
LOWER_LIMIT:        ds.w     1
CONTROLS:           ds.l     $302
HANDLE:             ds.l     1
LENGTH:             ds.l     1
BUPHER:             ds.l     $80
TRAPLIST:           ds.l     $96
JOY1TRIG:           ds.w     1
JOY2TRIG:           ds.w     1
RANDOM_AREA:        ds.l     $800
                    ds.w     1

                    end
