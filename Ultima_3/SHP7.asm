                        include "constants.inc"

                        org     0

start:                  BRA.S   lbC00001C

                        dc.l    $202,0,0,0,0,0
                        dc.w    $FFFF

lbC00001C:              BRA.S   lbC000096

                        include "jump_table.inc"

lbC000096:              MOVE.B  PARTY_CHARACTERS(A3),INPUT_NUMBER(A3)
                        LEA     EQUINEEMPORIU_MSG(PC),A4
                        BSR     display_message
                        MOVE.B  INPUT_NUMBER(A3),D0
                        BSR.S   draw_digit
                        LEA     Horsescost_MSG(PC),A4
                        BSR     display_message
                        MOVE.B  INPUT_NUMBER(A3),D0
                        ASL.B   #1,D0
                        MOVE.B  D0,INPUT_NUMBER(A3)
                        BSR.S   draw_digit
                        LEA     gpwillyoubuy_MSG(PC),A4
                        BSR     display_message
                        BSR     input_wait_key
                        CMP.B   #$D9,D0
                        BEQ.S   lbC0000E4
                        LEA     NAhtoobadthes_MSG(PC),A4
                        BSR     display_message
                        RTS

lbC0000E4:              BSR.S   decrease_character_money
                        BPL.S   lbC0000FE
                        LEA     YImsorrybutyo_MSG(PC),A4
                        BSR     display_message
                        MOVEQ   #$11,D0
                        BSR     play_sound
                        RTS

lbC0000FE:              LEA     YMayyouridefa_MSG(PC),A4
                        BSR     display_message
                        MOVE.B  #$14,D0
                        MOVE.B  D0,11(A3)
                        MOVE.B  D0,$4A(A3)
                        BSR     refresh_map_view
                        RTS

decrease_character_money:
                        BSR     get_character_data
                        MOVE.B  INPUT_NUMBER(A3),D3
                        MOVE.B  CHARACTER_MONEY_HI(A5),D0
                        MOVE.B  D0,D4
                        MOVE.W  #0,CCR
                        SBCD    D3,D0
                        BCS.S   not_enough_money
                        MOVE.B  D0,CHARACTER_MONEY_HI(A5)
                        CLR.B   D0
                        RTS

not_enough_money:       MOVE.B  D4,CHARACTER_MONEY_HI(A5)
                        MOVE.B  #$FF,D0
                        RTS

EQUINEEMPORIU_MSG:      dc.b    "EQUINE EMPORIUM:",$ff
                        dc.b    0
Horsescost_MSG:         dc.b    "-Horses cost,",$ff
                        dc.b    0
gpwillyoubuy_MSG:       dc.b    "00gp. will you",$ff
                        dc.b    "buy? "
                        dc.b    0
YImsorrybutyo_MSG:      dc.b    "Y",$ff,$ff
                        dc.b    "I'm sorry but",$ff,$ff
                        dc.b    "you haven't",$ff
                        dc.b    "the gold!",$ff
                        dc.b    0
NAhtoobadthes_MSG:      dc.b    "N",$ff,$ff
                        dc.b    "Ah, too bad.",$ff
                        dc.b    "these are the",$ff
                        dc.b    "best in town!",$ff
                        dc.b    0
YMayyouridefa_MSG:      dc.b    "Y",$ff,$ff
                        dc.b    "May you ride",$ff
                        dc.b    "fast and true",$ff
                        dc.b    "friend!",$ff
                        dc.b    0

                        end
