@echo off
vasmm68k -quiet -phxass -no-opt -Fhunk -o exodus.o exodus.asm
vasmm68k -quiet -phxass -no-opt -Fbin -o ultmaps/shp0.bin shp0.asm
vasmm68k -quiet -phxass -no-opt -Fbin -o ultmaps/shp1.bin shp1.asm
vasmm68k -quiet -phxass -no-opt -Fbin -o ultmaps/shp2.bin shp2.asm
vasmm68k -quiet -phxass -no-opt -Fbin -o ultmaps/shp3.bin shp3.asm
vasmm68k -quiet -phxass -no-opt -Fbin -o ultmaps/shp4.bin shp4.asm
vasmm68k -quiet -phxass -no-opt -Fbin -o ultmaps/shp5.bin shp5.asm
vasmm68k -quiet -phxass -no-opt -Fbin -o ultmaps/shp6.bin shp6.asm
vasmm68k -quiet -phxass -no-opt -Fbin -o ultmaps/shp7.bin shp7.asm
vasmm68k -quiet -phxass -no-opt -Fbin -o ultmaps/shne.bin shne.asm
vasmm68k -quiet -phxass -no-opt -Fbin -o ultmaps/endg.bin endg.asm
vlink -S -s -o exodus exodus.o
del exodus.o
