                        include "constants.inc"

                        org     0

start:                  BRA.S   lbC00001C

                        dc.l    $46E,0,0,0,0,0
                        dc.w    $FFFF

lbC00001C:              BRA.S   lbC000096

                        include "jump_table.inc"

lbC000096:              MOVEQ   #6,D0
                        BSR.S   get_encrypt_key_response
                        CMP.B   #$6B,D0
                        BNE.S   lbC0000EE
                        LEA     RADRIONPROPHE_MSG(PC),A4
                        BSR     display_message
lbC0000A8:              LEA     Howmany100gpi_MSG(PC),A4
                        BSR     display_message
lbC0000B0:              BSR.S   input_wait_key
                        CMP.B   #$B0,D0
                        BCS.S   lbC0000B0
                        CMP.B   #$BA,D0
                        BCC.S   lbC0000B0
                        MOVE.W  D0,-(SP)
                        AND.B   #$7F,D0
                        BSR     draw_letter_both_bitmap
                        MOVE.W  (SP)+,D0
                        SUB.B   #$B0,D0
                        MOVE.B  D0,INPUT_NUMBER(A3)
                        BSR     text_carriage_return
                        BSR     text_carriage_return
                        BSR.S   decrease_character_money
                        BEQ.S   lbC0000F4
                        LEA     WHATCANTPAYOU_MSG(PC),A4
lbC0000E2:              BSR     display_message
                        MOVEQ   #$11,D0
                        BSR     play_sound
                        RTS

lbC0000EE:              LEA     HonestyisaVir_MSG(PC),A4
                        BRA.S   lbC0000E2

lbC0000F4:              CLR.W   D2
                        MOVE.B  INPUT_NUMBER(A3),D2
                        ADD.W   D2,D2
                        LEA     lbW000154(PC),A4
                        ADD.W   0(A4,D2.W),A4
                        BSR     display_message
                        LEA     Moreoffering_MSG(PC),A4
                        BSR     display_message
                        BSR     input_wait_key
                        CMP.B   #$D9,D0
                        BNE.S   lbC000124
                        LEA     Y_MSG(PC),A4
                        BSR     display_message
                        BRA.S   lbC0000A8

lbC000124:              LEA     NFaretheewell_MSG(PC),A4
                        BSR     display_message
                        RTS

decrease_character_money:
                        MOVE.B  D0,D3
                        BSR     get_character_data
                        MOVE.B  CHARACTER_MONEY_HI(A5),D0
                        MOVE.B  D0,D4
                        MOVE.W  #0,CCR
                        SBCD    D3,D0
                        BCS.S   not_enough_money
                        MOVE.B  D0,CHARACTER_MONEY_HI(A5)
                        CLR.B   D0
                        RTS

not_enough_money:       MOVE.B  D4,CHARACTER_MONEY_HI(A5)
                        MOVE.B  #$FF,D0
                        RTS

lbW000154:              dc.w    Msg1-lbW000154
                        dc.w    Msg2-lbW000154
                        dc.w    Msg3-lbW000154
                        dc.w    Msg4-lbW000154
                        dc.w    Msg5-lbW000154
                        dc.w    Msg6-lbW000154
                        dc.w    Msg7-lbW000154
                        dc.w    Msg8-lbW000154
                        dc.w    Msg9-lbW000154
                        dc.w    Msg10-lbW000154

RADRIONPROPHE_MSG:      dc.b    $ff,'    RADRION:',$ff
                        dc.b    'PROPHET OF LIFE!',$ff,0
Howmany100gpi_MSG:      dc.b    $ff,'How many 100gp',$ff
                        dc.b    'is your',$ff
                        dc.b    'offering?',0
WHATCANTPAYOU_MSG:      dc.b    "WHAT? CAN'T PAY!",$ff
                        dc.b    'OUT YOU SCUM!',$ff
                        dc.b    0

Msg1:                   dc.b    'And so the sage',$ff
                        dc.b    'said unto thee',$ff
                        dc.b    "'If thou can",$ff
                        dc.b    'solve my rhyme,',$ff
                        dc.b    0

Msg2:                   dc.b    "you'll learn of",$ff
                        dc.b    'marks & playing',$ff
                        dc.b    'cards & hidden',$ff
                        dc.b    'holy shrines.',$ff
                        dc.b    0

Msg3:                   dc.b    'Of marks I say',$ff
                        dc.b    'there are but 4',$ff
                        dc.b    'of fire, force,',$ff
                        dc.b    'snake & king.',$ff
                        dc.b    0

Msg4:                   dc.b    'Learn their use',$ff
                        dc.b    'in Devil Guard,',$ff
                        dc.b    "or death you'll",$ff
                        dc.b    'surely bring.',$ff
                        dc.b    0

Msg5:                   dc.b    'Shrines there',$ff
                        dc.b    'are again but 4',$ff
                        dc.b    'to which you',$ff
                        dc.b    'go and pray.',$ff
                        dc.b    0

Msg6:                   dc.b    'There uses are',$ff
                        dc.b    'inumerable and',$ff
                        dc.b    'clues throughout',$ff
                        dc.b    'I say',$ff
                        dc.b    0

Msg7:                   dc.b    'The cards their',$ff
                        dc.b    'suits do number',$ff
                        dc.b    '4, called sol,',$ff
                        dc.b    'moon, death',$ff
                        dc.b    'and love.',$ff
                        dc.b    0

Msg8:                   dc.b    'Unto the Montors',$ff
                        dc.b    'thou must go',$ff
                        dc.b    'for guidance',$ff
                        dc.b    'from above.',$ff
                        dc.b    0

Msg9:                   dc.b    'To aid thee in',$ff
                        dc.b    'thy cryptic',$ff
                        dc.b    'search, to',$ff
                        dc.b    'dungeons thou',$ff
                        dc.b    'must fare.',$ff
                        dc.b    0

Msg10:                  dc.b    'There seek out',$ff
                        dc.b    'the Lord of Time',$ff
                        dc.b    'to help you',$ff
                        dc.b    "if he cares.'",$ff
                        dc.b    0

Moreoffering_MSG:       dc.b    $ff,'More offering?'
                        dc.b    0
Y_MSG:                  dc.b    'Y',$ff
                        dc.b    0
NFaretheewell_MSG:      dc.b    'N',$ff,$ff,'Fare thee well',$ff
                        dc.b    'and good luck!',$ff
                        dc.b    0
HonestyisaVir_MSG:      dc.b    'Honesty is a',$ff
                        dc.B    'Virtue!',$ff
                        dc.b    'I will not',$ff
                        dc.b    'help you!',$ff
                        dc.b    0

                        end
