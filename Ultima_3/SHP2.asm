                        include "constants.inc"

                        org     0

start:                  BRA.S   lbC00001C

                        dc.l    $47C,0,0,0,0,0
                        dc.w    $FFFF

lbC00001C:              BRA.S   lbC000096

                        include "jump_table.inc"

lbC000096:              MOVEQ   #2,D0
                        BSR.S   get_encrypt_key_response
                        CMP.B   #$6C,D0
                        BNE     lbC000206
                        LEA     CLERICALHEALI_MSG(PC),A4
                        BSR     display_message
lbC0000AA:              BSR.S   input_wait_key
                        CMP.B   #$B0,D0
                        BCS.S   lbC0000AA
                        CMP.B   #$B5,D0
                        BCC.S   lbC0000AA
                        SUB.B   #$B0,D0
                        CMP.B   #0,D0
                        BNE.S   lbC0000D4
                        LEA     lbW0002CE(PC),A4
lbC0000C6:              BSR     display_message
lbC0000CA:              LEA     Faretheewellm_MSG(PC),A4
                        BSR     display_message
                        RTS

lbC0000D4:              CMP.B   #1,D0
                        BNE.S   lbC0000DC
                        BRA.S   lbC0000EE

lbC0000DC:              CMP.B   #2,D0
                        BNE.S   lbC0000E4
                        BRA.S   lbC00011C

lbC0000E4:              CMP.B   #3,D0
                        BNE     lbC000194
                        BRA.S   lbC00014A

lbC0000EE:              LEA     Acuringwillco_MSG(PC),A4
                        BSR     display_message
                        MOVE.B  #1,D0
                        BSR     lbC0001C2
                        LEA     Curewhom_MSG(PC),A4
                        BSR     display_message
                        BSR     lbC00023E
                        BSR     get_character_data
                        CMP.B   #$D0,$11(A5)
                        BEQ.S   lbC000178
                        LEA     Iseenopoisonh_MSG(PC),A4
                        BRA.S   lbC0000C6

lbC00011C:              LEA     Healingscost2_MSG(PC),A4
                        BSR     display_message
                        MOVE.B  #2,D0
                        BSR     lbC0001C2
                        LEA     Healwhom_MSG(PC),A4
                        BSR     display_message
                        BSR     lbC00023E
                        BSR     get_character_data
                        MOVE.B  $1C(A5),$1A(A5)
                        MOVE.B  $1D(A5),$1B(A5)
                        BRA.S   lbC00017E

lbC00014A:              LEA     Resurrections_MSG(PC),A4
                        BSR     display_message
                        MOVE.B  #5,D0
                        BSR.S   lbC0001C2
                        LEA     Resurrectwhom_MSG(PC),A4
                        BSR     display_message
                        BSR     lbC00023E
                        BSR     get_character_data
                        CMP.B   #$C4,$11(A5)
                        BEQ.S   lbC000178
                        LEA     Thatoneisstil_MSG(PC),A4
                        BRA     lbC0000C6

lbC000178:              MOVE.B  #$C7,$11(A5)
lbC00017E:              BSR     lbC00022A
                        MOVE.B  $67(A3),$66(A3)
                        BSR     get_character_data
                        BSR     lbC000216
                        BRA     lbC0000CA

lbC000194:              LEA     Recallingscos_MSG(PC),A4
                        BSR     display_message
                        MOVE.B  #9,D0
                        BSR.S   lbC0001C2
                        LEA     Recallwhom_MSG(PC),A4
                        BSR     display_message
                        BSR     lbC00023E
                        BSR     get_character_data
                        CMP.B   #$C1,$11(A5)
                        BEQ.S   lbC000178
                        LEA     Thatoneisstil_MSG(PC),A4
                        BRA     lbC0000C6

lbC0001C2:              LEA     lbB00026C(PC),A0
                        MOVE.B  D0,(A0)
                        BSR     input_wait_key
                        CMP.B   #$D9,D0
                        BEQ.S   lbC0001DE
                        LEA     NWithoutprope_MSG(PC),A4
                        BSR     display_message
                        MOVE.L  (SP)+,D0
                        RTS

lbC0001DE:              BSR     get_character_data
                        MOVE.B  CHARACTER_MONEY_HI(A5),D0
                        LEA     lbB00026C(PC),A0
                        MOVE.B  (A0),D3
                        MOVE.W  #0,CCR
                        SBCD    D3,D0
                        BCC.S   lbC00020C
                        LEA     YImsorrybutth_MSG(PC),A4
lbC0001F8:              BSR     display_message
                        MOVEQ   #1,D0
                        BSR     play_sound
                        MOVE.L  (SP)+,D0
                        RTS

lbC000206:              LEA     HonestyisaVir_MSG(PC),A4
                        BRA.S   lbC0001F8

lbC00020C:              LEA     Y_MSG(PC),A4
                        BSR     display_message
                        RTS

lbC000216:              MOVE.B  lbB00026C(PC),D3
                        MOVE.B  CHARACTER_MONEY_HI(A5),D0
                        MOVE.W  #0,CCR
                        SBCD    D3,D0
                        MOVE.B  D0,CHARACTER_MONEY_HI(A5)
                        RTS

lbC00022A:              BSR     lbC000030
                        BSR     lbC000036
                        MOVEQ   #15,D0
                        BSR     play_sound
                        BSR     lbC000036
                        RTS

lbC00023E:              MOVE.B  #1,$79(A3)
lbC000244:              BSR     input_wait_key
                        CMP.B   #$B1,D0
                        BCS.S   lbC000244
                        CMP.B   #$B5,D0
                        BCC.S   lbC000244
                        CLR.B   $79(A3)
                        SUB.B   #$B1,D0
                        MOVE.B  D0,$66(A3)
                        ADDQ.B  #1,D0
                        BSR     draw_digit
                        BSR     text_carriage_return
                        RTS

lbB00026C:              dc.b    0
CLERICALHEALI_MSG:      dc.b    $ff
                        dc.b    'CLERICAL HEALING',$ff
                        dc.b    'SACRAMENTS:',$ff
                        dc.b    ' 1-Curing,',$ff
                        dc.b    ' 2-Healing,',$ff
                        dc.b    ' 3-Resurrection,',$ff
                        dc.b    ' 4-Recallings.',$ff
                        dc.b    'Your needs:'
                        dc.b    0
lbW0002CE:              dc.b    '0',$ff
                        dc.b    0
Faretheewellm_MSG:      dc.b    $ff,'Fare thee well,',$ff
                        dc.b    'my children.',$ff
                        dc.b    0
Acuringwillco_MSG:      dc.b    '1',$ff,$ff
                        dc.b    'A curing will',$ff
                        dc.b    'cost 100 g.p.',$ff
                        dc.b    'Wilt thou pay'
                        dc.b    '? '
                        dc.b    0
Curewhom_MSG:           dc.b    'Cure whom?-'
                        dc.b    0
Healingscost2_MSG:      dc.b    '2',$ff,$ff
                        dc.b    'Healings cost',$ff
                        dc.b    '200 g.p. Wilt',$ff
                        dc.b    'thou pay? '
                        dc.b    0
Healwhom_MSG:           dc.b    'Heal whom?-'
                        dc.b    0
Resurrections_MSG:      dc.b    '3',$ff,$ff
                        dc.b    'Resurrections',$ff
                        dc.b    'cost 500 g.p.',$ff
                        dc.b    'Wilt thou pay? '
                        dc.b    0
Resurrectwhom_MSG:      dc.b    'Resurrect',$ff
                        dc.b    'whom-'
                        dc.b    0
Recallingscos_MSG:      dc.b    '4',$ff,$ff
                        dc.b    'Recallings',$ff
                        dc.b    'cost 900 g.p.',$ff
                        dc.b    'Wilt thou pay? '
                        dc.b    0
Recallwhom_MSG:         dc.b    'Recall whom?-'
                        dc.b    0
NWithoutprope_MSG:      dc.b    'N',$ff,$ff
                        dc.b    'Without proper',$ff
                        dc.b    'offerings I',$ff
                        dc.b    'can not help!',$ff
                        dc.b    0
YImsorrybutth_MSG:      dc.b    'Y',$ff,$ff
                        dc.b    "I'm sorry but",$ff
                        dc.b    'thou hast not',$ff
                        dc.b    'gold enough.',$ff
                        dc.b    0
Y_MSG:                  dc.b    'Y',$ff
                        dc.b    0
Iseenopoisonh_MSG:      dc.b    $ff
                        dc.b    'I see no',$ff
                        dc.b    'poison here!',$ff
                        dc.b    0
Thatoneisstil_MSG:      dc.b    $ff
                        dc.b    'That one is',$ff
                        dc.b    'still kicking!!',$ff
                        dc.b    0
HonestyisaVir_MSG:      dc.b    'Honesty is a',$ff
                        dc.b    'Virtue!',$ff
                        dc.b    'I will not',$ff
                        dc.b    'help you!',$ff
                        dc.b    0

                        end
