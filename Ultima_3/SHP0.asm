                        include "constants.inc"

                        org     0

start:                  BRA.S   lbC00001C

                        dc.l    $476,0,0,0,0,0
                        dc.w    $FFFF

lbC00001C:              BRA.S   lbC000096

                        include "jump_table.inc"

lbC000096:              MOVEQ   #0,D0
                        BSR.S   get_encrypt_key_response
                        CMP.B   #$6B,D0
                        BNE.S   lbC0000D8
                        LEA     WELCOMETOTHEP_MSG(PC),A4
                        BSR     display_message
lbC0000A8:              LEA     Herefriendhav_MSG(PC),A4
                        BSR     display_message
                        BSR.S   input_get_number
                        MOVE.B  D0,INPUT_NUMBER(A3)
                        BSR     text_carriage_return
                        BSR     text_carriage_return
                        MOVE.B  INPUT_NUMBER(A3),D0
                        CMP.B   #7,D0
                        BCC.S   lbC0000DE
                        LEA     LEAVEMYSHOPYO_MSG(PC),A4
lbC0000CC:              BSR     display_message
                        MOVEQ   #1,D0
                        BSR     play_sound
                        RTS

lbC0000D8:              LEA     HonestyisaVir_MSG(PC),A4
                        BRA.S   lbC0000CC

lbC0000DE:              BSR     decrease_character_money
                        BEQ.S   lbC0000F4
                        LEA     WHATCANTPAYOU_MSG(PC),A4
                        BSR     display_message
                        MOVEQ   #1,D0
                        BSR     play_sound
                        RTS

lbC0000F4:              LEA     lbW000120(PC),A0
                        MOVE.W  (A0)+,D4
                        MOVE.W  #9,D3
                        MOVE.B  INPUT_NUMBER(A3),D0
lbC000102:              CMP.B   D3,D0
                        BHI.S   lbC000112
lbC000106:              LEA     lbW000120(PC),A4
                        ADD.W   D4,A4
                        BSR     display_message
                        BRA.S   lbC000134

lbC000112:              MOVE.W  (A0)+,D4
                        ADD.B   #$10,D3
                        CMP.B   #$99,D3
                        BNE.S   lbC000102
                        BRA.S   lbC000106

lbW000120:              dc.w    Msg1-lbW000120
                        dc.w    Msg2-lbW000120
                        dc.w    Msg3-lbW000120
                        dc.w    Msg4-lbW000120
                        dc.w    Msg5-lbW000120
                        dc.w    Msg6-lbW000120
                        dc.w    Msg7-lbW000120
                        dc.w    Msg8-lbW000120
                        dc.w    Msg9-lbW000120
                        dc.w    Msg10-lbW000120

lbC000134:              LEA     anotherdrink_MSG(PC),A4
                        BSR     display_message
                        BSR     input_wait_key
                        CMP.B   #$D9,D0
                        BNE.S   lbC000152
                        LEA     Y_MSG(PC),A4
                        BSR     display_message
                        BRA     lbC0000A8

lbC000152:              LEA     NItsbeenaplea_MSG(PC),A4
                        BSR     display_message
                        RTS

decrease_character_money:
                        MOVE.B  D0,D3
                        BSR     get_character_data
                        MOVE.B  CHARACTER_MONEY_LO(A5),D0
                        MOVE.B  D0,D4
                        MOVE.W  #0,CCR
                        SBCD    D3,D0
                        MOVE.B  D0,CHARACTER_MONEY_LO(A5)
                        MOVE.B  CHARACTER_MONEY_HI(A5),D0
                        MOVE.B  D0,D5
                        CLR.B   D3
                        SBCD    D3,D0
                        BCS.S   not_enough_money
                        MOVE.B  D0,CHARACTER_MONEY_HI(A5)
                        CLR.B   D0
                        RTS

not_enough_money:       MOVE.B  D4,CHARACTER_MONEY_LO(A5)
                        MOVE.B  D5,CHARACTER_MONEY_HI(A5)
                        MOVE.B  #$FF,D0
                        RTS

WELCOMETOTHEP_MSG:      dc.b    $ff,'   WELCOME TO',$ff
                        dc.b    '    THE PUB!',$ff
                        dc.b    0
Herefriendhav_MSG:      dc.b    $ff
                        dc.b    'Here friend,',$ff
                        dc.b    'have a drink!',$ff
                        dc.b    'IT COST 7 g.p.',$ff
                        dc.b    'YOU PAY?-'
                        dc.b    0
LEAVEMYSHOPYO_MSG:      dc.b    ' LEAVE MY SHOP!',$ff
                        dc.b    '   YOU SCUM!!',$ff
                        dc.b    0
WHATCANTPAYOU_MSG:      dc.b    "WHAT? CAN'T PAY!",$ff
                        dc.b    'OUT YOU SCUM!',$ff
                        dc.b    0

Msg1:                   dc.b    'Thank you,',$ff
                        dc.b    'kindly!',$ff
                        dc.b    0
                        
Msg2:                   dc.b    'Ambrosia,',$ff
                        dc.b    'ever heard',$ff
                        dc.b    'of it?',$ff
                        dc.b    0
        
Msg3:                   dc.b    'Dawn,',$ff
                        dc.b    'the city of',$ff
                        dc.B    'myths & magic!',$ff
                        dc.b    0

Msg4:                   dc.b    'The conjuction',$ff
                        dc.b    'of the moons',$ff
                        dc.b    'finds link!',$ff
                        dc.b    0
        
Msg5:                   dc.b    'Nasty creatures,',$ff
                        dc.b    '  nasty  dark,',$ff
                        dc.b    'sure thee ready,',$ff
                        dc.b    'fore thee embark',$ff
                        dc.b    0

Msg6:                   dc.b    'None return or',$ff
                        dc.b    " so I'm told,",$ff
                        dc.b    'from the pool,',$ff
                        dc.b    'dark and cold!',$ff
                        dc.b    0
        
Msg7:                   dc.b    '  Shrines of',$ff
                        dc.b    '  knowledge,',$ff
                        dc.b    '  shrines of',$ff
                        dc.b    '   strength,',$ff
                        dc.b    ' all are lost',$ff
                        dc.b    'into the brink!'
                        dc.b    0
        
Msg8:                   dc.b    ' Fountains fair',$ff
                        dc.b    '       &',$ff
                        dc.b    ' fountains foul',$ff
                        dc.b    'all are found in',$ff
                        dc.b    ' dungeons bowel',$ff
                        dc.b    0
        
Msg9:                   dc.b    '     EXODUS:',$ff
                        dc.b    '   Ultima III',$ff
                        dc.b    ' which is next?',$ff
                        dc.b    'Now could it be.',$ff
                        dc.b    0

Msg10:                  dc.b    'Seek ye out the',$ff
                        dc.b    ' Lord of Time,',$ff
                        dc.b    'and the one way',$ff
                        dc.b    'is a sure find!',$ff
                        dc.b    0

anotherdrink_MSG:       dc.b    $ff,'another drink?'
                        dc.b    0
Y_MSG:                  dc.b    'Y',$ff
                        dc.b    0

NItsbeenaplea_MSG:      dc.b    'N',$ff,$ff
                        dc.b    'Its been a',$ff
                        dc.b    'pleasure!!',$ff
                        dc.b    0

HonestyisaVir_MSG:      dc.b    'Honesty is a',$ff
                        dc.b    'Virtue!',$ff
                        dc.b    'I will not',$ff
                        dc.b    'help you!',$ff
                        dc.b    0

                        end
