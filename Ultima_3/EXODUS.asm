; ----------------------------------------
; Ultima 3 Exodus - Amiga version
; Disassembled/Reconstructed by Franck Charlet.
; ----------------------------------------

; ----------------------------------------
; Constants

; Exec
_LVOForbid              equ     -132
_LVOSetIntVector        equ     -162
_LVOAddIntServer        equ     -168
_LVOAllocMem            equ     -198
_LVOFreeMem             equ     -210
_LVOAvailMem            equ     -216
_LVORemTask             equ     -288
_LVOFindTask            equ     -294
_LVOSetSignal           equ     -306
_LVOGetMsg              equ     -372
_LVOReplyMsg            equ     -378
_LVOWaitPort            equ     -384
_LVOCloseLibrary        equ     -414
_LVOOpenLibrary         equ     -552

; Graphics
_LVOBltBitMap           equ     -30
_LVOInitRastPort        equ     -198
_LVOInitVPort           equ     -204
_LVOMrgCop              equ     -210
_LVOMakeVPort           equ     -216
_LVOLoadView            equ     -222
_LVOMove                equ     -240
_LVODraw                equ     -246
_LVOWaitTOF             equ     -270
_LVOBltClear            equ     -300
_LVOWritePixel          equ     -324
_LVOSetAPen             equ     -342
_LVOInitView            equ     -360
_LVOInitBitMap          equ     -390
_LVOScrollRaster        equ     -396
_LVOFreeSprite          equ     -414
_LVOGetColorMap         equ     -570

; Dos
_LVOOpen                equ     -30
_LVOClose               equ     -36
_LVORead                equ     -42
_LVOWrite               equ     -48
_LVOInput               equ     -54
_LVOOutput              equ     -60
_LVOSeek                equ     -66
_LVODeleteFile          equ     -72
_LVORename              equ     -78
_LVOLock                equ     -84
_LVOUnLock              equ     -90
_LVOExamine             equ     -102
_LVOCurrentDir          equ     -126

pr_MsgPort              equ     $5c
pr_CLI                  equ     $ac
sm_ArgList              equ     $24

INTB_VERTB              equ     5

ACCESS_READ             equ     -2
ACCESS_WRITE            equ     -1

OFFSET_BEGINNING        equ     -1

; ----------------------------------------
; Includes
                        include "constants.inc"

; ----------------------------------------
; Program start
                        SECTION start,CODE

start:                  move.l  sp,a5
                        move.l  4.w,a6
                        move.l  a6,ExecBase
                        sub.l   a1,a1
                        jsr     _LVOFindTask(a6)
                        move.l  d0,a4

                        lea     DosName,a1
                        moveq   #0,d0
                        jsr     _LVOOpenLibrary(a6)
                        move.l  d0,DOSBase

                        tst.l   pr_CLI(a4)
                        bne.b   start_from_cli

                        lea     pr_MsgPort(a4),a0
                        jsr     _LVOWaitPort(a6)
                        lea     pr_MsgPort(a4),a0
                        jsr     _LVOGetMsg(a6)
                        move.l  d0,-(sp)
                        move.l  d0,a2
                        move.l  sm_ArgList(a2),d0
                        beq.s   start_from_cli
                        move.l  DOSBase,a6
                        move.l  d0,a0
                        move.l  (a0),d1
                        jsr     _LVOCurrentDir(a6)
start_from_cli:
                        jmp     main

                        SECTION EXODUS0001CC,DATA

ExecBase:               dc.l    0
DOSBase:                dc.l    0
DosName:                dc.b    "dos.library",0

                        SECTION Files,CODE

; ----------------------------------------
; Retrieve the size of a file
file_get_size:          link    a5,#-12
                        moveq   #0,d0
                        move.l  12(a5),-(sp)
                        move.l  8(a5),-(sp)
                        move.l  d0,-8(a5)
                        move.l  d0,-4(a5)
                        jsr     _Lock
                        addq.l  #8,sp
                        move.l  d0,-12(a5)
                        tst.l   d0
                        beq.s   error_file_lock
                        move.l  FileInfoBlock,-(sp)
                        move.l  d0,-(sp)
                        jsr     _Examine
                        addq.l  #8,sp
                        move.l  d0,-4(a5)
                        tst.l   d0
                        beq.s   error_file_examine
                        move.l  FileInfoBlock,a0
                        move.l  124(a0),-8(a5)              ; fib_Size
error_file_examine:
                        move.l  -12(a5),-(sp)
                        jsr     _UnLock
                        addq.l  #4,sp
error_file_lock:
                        move.l  -8(a5),d0
                        unlk    a5
                        rts

; ----------------------------------------
; Load or save a file
file_execute_command:   link    a5,#-8
                        moveq   #-1,d0
                        move.l  d0,-8(a5)
                        move.l  16(a5),-(sp)                ; access type
                        move.l  8(a5),-(sp)                 ; filename
                        bsr     file_get_size
                        addq.l  #8,sp
                        move.l  d0,File_Size
                        tst.l   d0
                        beq     file_invalid_command
                        move.l  16(a5),d0
                        cmp.l   #ACCESS_WRITE,d0
                        beq.s   file_write_command
                        cmp.l   #ACCESS_READ,d0
                        bne     file_invalid_command
                        move.l  #$3ED,-(sp)
                        move.l  8(a5),-(sp)
                        jsr     _Open
                        addq.l  #8,sp
                        moveq   #OFFSET_BEGINNING,d1
                        move.l  d1,-(sp)
                        clr.l   -(sp)                       ; start of the file
                        move.l  d0,-(sp)
                        move.l  d0,-4(a5)
                        jsr     _Seek
                        lea     12(sp),sp
                        move.l  File_Size,-(sp)
                        move.l  12(a5),-(sp)                ; Buffer
                        move.l  -4(a5),-(sp)                ; Handle
                        jsr     _Read
                        lea     12(sp),sp
                        move.l  -4(a5),-(sp)
                        move.l  d0,-8(a5)
                        jsr     _Close
                        addq.l  #4,sp
                        bra.s   file_invalid_command

file_write_command:     move.l  #$3EE,-(sp)
                        move.l  8(a5),-(sp)
                        jsr     _Open
                        addq.l  #8,sp
                        moveq   #OFFSET_BEGINNING,d1
                        move.l  d1,-(sp)
                        clr.l   -(sp)
                        move.l  d0,-(sp)
                        move.l  d0,-4(a5)
                        jsr     _Seek
                        lea     12(sp),sp
                        move.l  File_Size,-(sp)
                        move.l  12(a5),-(sp)
                        move.l  -4(a5),-(sp)
                        jsr     _Write
                        lea     12(sp),sp
                        move.l  -4(a5),-(sp)
                        move.l  d0,-8(a5)
                        jsr     _Close
                        addq.l  #4,sp
file_invalid_command:   move.l  -8(a5),d0
                        unlk    a5
                        rts

; ----------------------------------------
; Double buffering
swap_view:              move.l  a2,-(sp)
                        move.l  CopperlistB,a0
                        move.l  CopperlistA,a1
                        move.l  a1,CopperlistB
                        move.l  a0,CopperlistA
                        move.l  Bitmap_PtrA,a0
                        move.l  Bitmap_PtrB,a2
                        move.l  a2,Bitmap_PtrA
                        move.l  a0,Bitmap_PtrB

                        move.l  a2,Rastport_Bitmap
                        move.l  a1,View_LOFCprList
                        pea     View
                        jsr     _LoadView
                        addq.l  #4,sp
                        move.l  (sp)+,a2
                        rts

; ----------------------------------------
; Program entry point
main:                   link    a5,#-$38

                        ; Stop the input from the system cursor and keyboard
                        pea     inputdevice_name
                        jsr     _FindTask
                        addq.l  #4,sp
                        move.l  d0,-(sp)
                        jsr     _RemTask
                        addq.l  #4,sp

                        move.l  #VBlank_Server,-(sp)
                        moveq   #INTB_VERTB,d0                  ; Int level
                        move.l  d0,-(sp)
                        jsr     _AddIntServer
                        addq.l  #8,sp

                        clr.l   -(sp)
                        pea     GraphicsName
                        jsr     _OpenLibrary
                        addq.l  #8,sp
                        move.l  d0,GfxBase

                        ; Remove the system mouse cursor
                        clr.l   -(sp)
                        jsr     _FreeSprite
                        addq.l  #4,sp

                        pea     View
                        jsr     _InitView
                        addq.l  #4,sp

                        pea     Viewport
                        jsr     _InitVPort
                        addq.l  #4,sp
                        move.l  #Viewport,View

                        move.l  #SCREEN_HEIGHT,-(sp)
                        move.l  #SCREEN_WIDTH,-(sp)
                        moveq   #SCREEN_BPP,d0                      ; 16 colors
                        move.l  d0,-(sp)
                        pea     BitmapA
                        jsr     _InitBitMap
                        lea     $10(sp),sp

                        move.l  #SCREEN_HEIGHT,-(sp)
                        move.l  #SCREEN_WIDTH,-(sp)
                        moveq   #SCREEN_BPP,d0
                        move.l  d0,-(sp)
                        pea     BitmapB
                        jsr     _InitBitMap
                        lea     $10(sp),sp

                        move.l  #BitmapB,RasInfo_Bitmap
                        moveq   #0,d0
                        move.w  d0,RasInfo_RxOffset
                        move.w  d0,RasInfo_RyOffset
                        clr.l   RasInfo
                        move.w  #SCREEN_WIDTH,Viewport_Width
                        move.w  #SCREEN_HEIGHT,Viewport_Height
                        move.l  #RasInfo,Viewport_RasInfo
                        
                        moveq   #16,d0
                        move.l  d0,-(sp)
                        jsr     _GetColorMap
                        addq.l  #4,sp
                        move.l  d0,Our_Colormap
                        
                        move.l  d0,a0
                        move.l  4(a0),ColorMap                  ; ColorTable
                        clr.l   setcolormap_counter
loop_setcolormap:
                        move.l  setcolormap_counter,d0
                        cmp.l   #16,d0
                        bge.s   done_setcolormap
                        asl.l   #1,d0
                        move.l  d0,a0
                        add.l   #colors_table,a0
                        move.l  ColorMap,a1
                        move.w  (a0),(a1)
                        addq.l  #2,ColorMap
                        addq.l  #1,setcolormap_counter
                        bra.s   loop_setcolormap

done_setcolormap:       move.l  Our_Colormap,Viewport_Colormap

                        moveq   #2,d0
                        move.l  d0,-(sp)
                        move.l  #SCREEN_BYTES*SCREEN_HEIGHT*SCREEN_BPP,-(sp)
                        jsr     _AllocMem
                        addq.l  #8,sp
                        move.l  d0,GfxMemA
                        clr.l   -(sp)
                        move.l  #SCREEN_BYTES*SCREEN_HEIGHT*SCREEN_BPP,-(sp)
                        move.l  d0,-(sp)
                        jsr     _BltClear
                        lea     12(sp),sp
                        
                        move.l  GfxMemA,a0
                        move.l  a0,BitmapA_Bitplane1
                        add.w   #SCREEN_BYTES*SCREEN_HEIGHT,a0
                        move.l  a0,BitmapA_Bitplane2
                        move.l  GfxMemA,a0
                        add.w   #SCREEN_BYTES*SCREEN_HEIGHT*2,a0
                        move.l  a0,BitmapA_Bitplane3
                        move.l  GfxMemA,a0
                        add.w   #SCREEN_BYTES*SCREEN_HEIGHT*3,a0
                        move.l  a0,BitmapA_Bitplane4

                        moveq   #2,d0
                        move.l  d0,-(sp)
                        move.l  #SCREEN_BYTES*SCREEN_HEIGHT*SCREEN_BPP,-(sp)
                        jsr     _AllocMem
                        addq.l  #8,sp
                        move.l  d0,GfxMemB
                        clr.l   -(sp)
                        move.l  #SCREEN_BYTES*SCREEN_HEIGHT*SCREEN_BPP,-(sp)
                        move.l  d0,-(sp)
                        jsr     _BltClear
                        lea     12(sp),sp

                        move.l  GfxMemB,a0
                        move.l  a0,BitmapB_Bitplane1
                        add.w   #SCREEN_BYTES*SCREEN_HEIGHT,a0
                        move.l  a0,BitmapB_Bitplane2
                        move.l  GfxMemB,a0
                        add.w   #SCREEN_BYTES*SCREEN_HEIGHT*2,a0
                        move.l  a0,BitmapB_Bitplane3
                        move.l  GfxMemB,a0
                        add.w   #SCREEN_BYTES*SCREEN_HEIGHT*3,a0
                        move.l  a0,BitmapB_Bitplane4

                        clr.l   -(sp)
                        move.l  #260,-(sp)
                        jsr     _AllocMem
                        addq.l  #8,sp
                        move.l  d0,FileInfoBlock

                        pea     Viewport
                        pea     View
                        jsr     _MakeVPort
                        addq.l  #8,sp

                        pea     View
                        jsr     _MrgCop
                        addq.l  #4,sp

                        move.l  View_LOFCprList,CopperlistA
                        sub.l   a0,a0
                        move.l  a0,View_LOFCprList
                        move.l  a0,View_SHFCprList
                        move.l  #BitmapA,RasInfo_Bitmap
                        
                        pea     Viewport
                        pea     View
                        jsr     _MakeVPort
                        addq.l  #8,sp

                        pea     View
                        jsr     _MrgCop
                        addq.l  #4,sp

                        move.l  View_LOFCprList,CopperlistB

                        pea     View
                        jsr     _LoadView
                        addq.l  #4,sp

                        pea     Rastport
                        jsr     _InitRastPort
                        addq.l  #4,sp

                        lea     BitmapA,a0
                        move.l  a0,Rastport_Bitmap
                        move.l  a0,Bitmap_PtrA
                        move.l  #BitmapB,Bitmap_PtrB
                        
                        jsr     main_init
                        unlk    a5
                        rts

                        SECTION misc,data_c

colors_table:           dc.w    $000,$f66,$6f6,$66f,$766,$f6f,$6ff,$f96
                        dc.w    $67f,$b66,$6b6,$ff6,$6fb,$bb6,$bbb,$fff

inputdevice_name:       dc.b    "input.device",0
intvector_name:         dc.b    "CompVBI",0
GraphicsName:           dc.b    "graphics.library",0
                        even

                        SECTION System_Structs,bss_c

View:                   ds.l    1
View_LOFCprList:        ds.l    1
View_SHFCprList:        ds.l    1
                        ds.w    2                   ; DyOffset, DxOffset
                        ds.w    1                   ; Modes

Viewport:               ds.l    1
Viewport_Colormap:      ds.l    1
                        ds.l    1                   ; DspIns
                        ds.l    1                   ; SprIns
                        ds.l    1                   ; ClrIns
                        ds.l    1                   ; UCopIns
Viewport_Width:         ds.w    1
Viewport_Height:        ds.w    1
                        ds.w    1                   ; DxOffset
                        ds.w    1                   ; DyOffset
                        ds.w    1                   ; Modes
                        ds.b    1                   ; SpritePriorities
                        ds.b    1                   ; ExtendedModes
Viewport_RasInfo:       ds.l    1

Our_Colormap:           ds.l    1

RasInfo:                ds.l    1
RasInfo_Bitmap:         ds.l    1
RasInfo_RxOffset:       ds.w    1
RasInfo_RyOffset:       ds.w    1

BitmapA:                ds.w    1                   ; BytesPerRow
                        ds.w    1                   ; Rows
                        ds.b    1                   ; Flags
                        ds.b    1                   ; Depth
                        ds.w    1                   ; pad
BitmapA_Bitplane1:      ds.l    1                   ; bitplanes
BitmapA_Bitplane2:      ds.l    1
BitmapA_Bitplane3:      ds.l    1
BitmapA_Bitplane4:      ds.l    1
                        ds.l    4

BitmapB:                ds.w    1                   ; BytesPerRow
                        ds.w    1                   ; Rows
                        ds.b    1                   ; Flags
                        ds.b    1                   ; Depth
                        ds.w    1                   ; pad
BitmapB_Bitplane1:      ds.l    1                   ; bitplanes
BitmapB_Bitplane2:      ds.l    1
BitmapB_Bitplane3:      ds.l    1
BitmapB_Bitplane4:      ds.l    1
                        ds.l    4

Bitmap_PtrA:            ds.l    1
Bitmap_PtrB:            ds.l    1

Rastport:               ds.l    1
Rastport_Bitmap:        ds.l    1
                        ds.l    23

GfxMemA:                ds.l    1
GfxMemB:                ds.l    1
FileInfoBlock:          ds.l    1
setcolormap_counter:    ds.l    1
File_Size:              ds.l    1
CopperlistB:            ds.l    1
CopperlistA:            ds.l    1
GfxBase:                ds.l    1
ColorMap:               ds.l    1
file_loading_buffer:    ds.l    15000
demo_dat_buffer:        ds.b    512
                        ds.b    34002

                        SECTION Interrupt_Server,data_c

VBlank_Server:          dc.l    0,0                             ; ln_Succ,ln_Pred
                        dc.b    2,196                           ; ln_Type,ln_Pri
                        dc.l    intvector_name                  ; ln_Name
                        dc.l    0,int_vector_server             ; is_Data,is_Code

                        SECTION EXODUS017AC8,CODE

main_init:              jsr     init_music
                        jsr     load_tiles
                        move.b  #$FF,13(a3)
                        move.b  #12,14(a3)
                        move.b  #4,15(a3)
                        move.b  #$80,$10(a3)
                        move.b  #10,$11(a3)
                        move.b  #9,$12(a3)
                        move.b  #2,$20(a3)
                        move.b  #7,$21(a3)
                        move.b  #3,$22(a3)
                        move.b  #2,$23(a3)
                        move.b  #1,$24(a3)
                        move.b  #1,$25(a3)
                        move.b  #$1E,$19(a3)
                        clr.b   $13(a3)
                        clr.b   $14(a3)
                        clr.b   $15(a3)
                        clr.b   $16(a3)
                        clr.b   $17(a3)
                        clr.b   $18(a3)
                        move.b  #1,CURRENT_WINDS(a3)
                        jsr     title_screen
                        clr.b   CURRENT_WINDS(a3)
                        move.l  #'SOSA',d0
                        lea     current_map,a0
                        bsr     file_load_cache
                        move.l  #'SOSM',d0
                        lea     lbL0225D0,a0
                        bsr     file_load_cache
                        move.l  #'PRTY',d0
                        lea     $4A(a3),a0
                        bsr     file_load_cache
                        
                        move.l  #'PLRS',d0
                        lea     characters_data,a0
                        bsr     file_load_cache
                        move.b  $4A(a3),11(a3)
                        move.b  PARTY_X_SAVE(a3),PARTY_X(a3)
                        move.b  PARTY_Y_SAVE(a3),PARTY_Y(a3)
                        move.b  #$FF,12(a3)
                        clr.b   $4C(a3)
                        jsr     draw_game_borders
                        jsr     lbC017C5E
                        jsr     lbC01BB68
                        jsr     lbC01B5C2
                        
                        jmp     lbC017CA4

lbC017C5E:              move.w  #4-1,d7
lbC017C62:              move.b  #$1E,TEXT_X(a3)
                        move.w  #3,d1
                        sub.w   d7,d1
                        move.w  d1,d0
                        add.b   #$31,d0
                        lsl.w   #2,d1
                        move.b  d1,TEXT_Y(a3)
                        move.w  d0,-(sp)
                        move.b  #$1D,d0
                        jsr     lbC020E90
                        move.w  (sp)+,d0
                        jsr     draw_letter_both_bitmap
                        addq.b  #1,TEXT_X(a3)
                        move.b  #$1F,d0
                        jsr     lbC020E90
                        dbra    d7,lbC017C62
                        rts

lbC017CA4:              clr.b   NEGATE_TIME_COUNTER(a3)
                        clr.b   CURRENT_VOLUME(a3)
                        jsr     refresh_map_view
                        clr.b   flag_music_end
                        move.b  #1,d0
                        jsr     start_new_music
                        move.b  PARTY_CHARACTERS(a3),CURRENT_CHARACTER(a3)
                        subq.b  #1,CURRENT_CHARACTER(a3)

lbC017CCE:              jsr     get_character_data
                        move.b  $11(a5),d0
                        cmp.b   #$C7,d0
                        beq     lbC017CF6
                        cmp.b   #$D0,d0
                        beq     lbC017CF6
                        sub.b   #1,CURRENT_CHARACTER(a3)
                        bpl.s   lbC017CCE
                        jmp     lbC01B966

lbC017CF6:              clr.b   CURRENT_DUNGEON_LEVEL(a3)
                        jsr     lbC01B908
                        cmp.b   #$80,$4C(a3)
                        beq     lbC017D82
                        cmp.b   #$14,11(a3)
                        beq     lbC017D1E
                        cmp.b   #$16,11(a3)
                        bne     lbC017D28
lbC017D1E:              move.b  #$D8,$78(a3)
                        bra     lbC017DA2

lbC017D28:              jsr     set_player_to_action
                        lsr.b   #1,d0
                        and.b   #$FE,d0
                        cmp.b   #10,d0
                        bcs     lbC017D82
                        cmp.b   #14,d0
                        bls     lbC017D4C
                        cmp.b   #$7C,d0
                        bne     lbC017D56
lbC017D4C:              move.b  #$C5,$78(a3)
                        bra     lbC017DA2

lbC017D56:              cmp.b   #$12,d0
                        bne     lbC017D68
                        move.b  #$C7,$78(a3)
                        bra     lbC017DA2

lbC017D68:              cmp.b   #$14,d0
                        beq     lbC017D78
                        cmp.b   #$16,d0
                        bne     lbC017D82
lbC017D78:              move.b  #$C2,$78(a3)
                        bra     lbC017DA2

lbC017D82:              move.b  #$D2,$78(a3)
                        tst.b   $4C(a3)
                        bmi     lbC017DA2
                        bne     lbC017D9E
                        move.b  #$A0,$78(a3)
                        bra     lbC017DA2

lbC017D9E:              clr.w   $78(a3)
lbC017DA2:              move.b  #$18,TEXT_X(a3)
                        move.b  #$18,TEXT_Y(a3)
                        lea     Escape_MSG,a4
                        jsr     display_message
                        move.b  #$4B,$3C(a3)
lbC017DC0:              sub.b   #1,$3C(a3)
                        beq     lbC017E54
                        jsr     lbC01C03A
                        jsr     lbC021318
                        move.b  #$19,TEXT_X(a3)
                        move.b  #$18,TEXT_Y(a3)
                        move.b  #2,$79(a3)
                        jsr     lbC021B48
                        bpl.s   lbC017DC0
                        cmp.b   #3,$79(a3)
                        beq     lbC017E66
                        clr.b   $79(a3)
                        cmp.b   #$C1,d0
                        bcs     lbC017E14
                        cmp.b   #$DB,d0
                        bcc     lbC017E14
                        jmp     handle_commands

lbC017E14:              clr.b   $79(a3)
                        cmp.b   #$81,d0
                        beq     lbC017E92
                        cmp.b   #$8A,d0
                        beq     lbC017ECC
                        cmp.b   #$95,d0
                        beq     lbC017F06
                        cmp.b   #$88,d0
                        beq     lbC017F40
                        cmp.b   #$92,d0
                        beq     sound_switch_on_off
                        cmp.b   #$93,d0
                        beq     music_switch_on_off
                        clr.b   $5E(a3)
                        cmp.b   #$A0,d0
                        bne     lbC017E70

lbC017E54:              lea     Pass_MSG0,a4
                        jsr     display_message
                        jmp     lbC01B400

lbC017E66:              clr.b   $5E(a3)
                        jmp     lbC01ADDC

lbC017E70:              lea     WHAT_MSG,a4
                        jsr     display_message
                        move.w  #0,d0
                        jsr     play_sound
                        jmp     lbC01B400

lbC017E92:              lea     North_MSG,a4
                        jsr     display_message
                        move.b  #1,d0
                        jsr     lbC021658
                        bne     invalid_move_message
                        move.b  7(a3),d0
                        jsr     lbC01B9E8
                        bne     invalid_move_message
                        subq.b  #1,PARTY_Y(a3)
                        and.b   #$3F,PARTY_Y(a3)
                        jmp     lbC01B400

lbC017ECC:              lea     South_MSG0,a4
                        jsr     display_message
                        move.b  #3,d0
                        jsr     lbC021658
                        bne     invalid_move_message
                        move.b  8(a3),d0
                        jsr     lbC01B9E8
                        bne     invalid_move_message
                        addq.b  #1,PARTY_Y(a3)
                        and.b   #$3F,PARTY_Y(a3)
                        jmp     lbC01B400

lbC017F06:              lea     East_MSG,a4
                        jsr     display_message
                        move.b  #2,d0
                        jsr     lbC021658
                        bne     invalid_move_message
                        move.b  9(a3),d0
                        jsr     lbC01B9E8
                        bne     invalid_move_message
                        addq.b  #1,PARTY_X(a3)
                        and.b   #$3F,PARTY_X(a3)
                        jmp     lbC01B400

lbC017F40:              lea     West_MSG,a4
                        jsr     display_message
                        move.b  #4,d0
                        jsr     lbC021658
                        bne     invalid_move_message
                        move.b  10(a3),d0
                        jsr     lbC01B9E8
                        bne     invalid_move_message
                        subq.b  #1,PARTY_X(a3)
                        and.b   #$3F,PARTY_X(a3)
                        jmp     lbC01B400

; ----------------------------------------
sound_switch_on_off:    lea     SoundOFF_MSG,a4
                        bchg    #0,sound_output_state
                        bne     display_sound_music_state
                        lea     SoundON_MSG,a4
display_sound_music_state:
                        jsr     display_message
                        jmp     lbC01B400

; ----------------------------------------
music_switch_on_off:    lea     MusicOFF_MSG,a4
                        bchg    #1,sound_output_state
                        bne.s   display_sound_music_state
                        lea     MusicON_MSG,a4
                        bra.s   display_sound_music_state

; ----------------------------------------
handle_commands:        clr.b   $5E(a3)
                        lea     commands_table,a0
                        sub.b   #$C1,d0
                        and.w   #$FF,d0
                        lsl.w   #2,d0
                        move.l  0(a0,d0.w),a0
                        jmp     (a0)

commands_table:         dc.l    command_Attack
                        dc.l    command_Board
                        dc.l    command_Cast
                        dc.l    command_Descend
                        dc.l    command_Enter
                        dc.l    command_Fire_ship
                        dc.l    command_GetChest
                        dc.l    command_HandEquipment
                        dc.l    command_IgniteTorch
                        dc.l    command_JoinGold
                        dc.l    command_Klimb
                        dc.l    command_Look
                        dc.l    command_ModifyOrder
                        dc.l    command_NegateTime
                        dc.l    command_OtherCommand
                        dc.l    command_PeerAtGem
                        dc.l    command_QuitAndSave
                        dc.l    command_ReadyObject
                        dc.l    command_Steal
                        dc.l    command_Transact
                        dc.l    command_Unlock
                        dc.l    command_VolumeSet
                        dc.l    command_WearObject
                        dc.l    command_Xit_drop
                        dc.l    command_Yell
                        dc.l    command_ZStats

invalid_move_message:   lea     INVALIDMOVE_MSG,a4
                        jsr     display_message

command_aborted:        move.w  #1,d0
                        jsr     play_sound
                        jmp     lbC01B400

what_message:           lea     WHAT_MSG,a4
                        jsr     display_message
                        bra.s   command_aborted

not_here_message:       lea     NOTHERE_MSG,a4
                        jsr     display_message
                        jmp     command_aborted(pc)

file_load_cache:        bsr     file_check_cache
                        bpl     file_get_cached_size        ; file is already loaded
                        move.l  d0,0(a1,d1.w)               ; write result into files_ids
                        lea     files_addresses,a1
                        move.l  file_current_loading_address,0(a1,d1.w)
                        movem.l d1/a0,-(sp)
                        move.l  file_current_loading_address,a0
                        jsr     file_load
                        move.l  File_Size,d0
                        add.l   d0,file_current_loading_address
                        movem.l (sp)+,d1/a0
                        lea     files_sizes,a1
                        move.l  d0,0(a1,d1.w)
                        bra     lbC0180CA

file_get_cached_size:   lea     files_sizes,a1
                        move.l  0(a1,d1.w),d0
lbC0180CA:              lea     files_addresses,a1
                        move.l  0(a1,d1.w),a1
                        subq.w  #1,d0
lbC0180D8:              move.b  (a1)+,(a0)+
                        dbra    d0,lbC0180D8
                        lea     files_ids,a1
                        rts

lbC0180E6:              movem.l d0/a0,-(sp)
                        jsr     file_save
                        movem.l (sp)+,d0/a0
                        bsr     file_check_cache
                        bmi     lbC01811A
                        lea     files_sizes,a1
                        move.l  0(a1,d1.w),d0
                        lea     files_addresses,a1
                        move.l  0(a1,d1.w),a1
                        subq.w  #1,d0
lbC018114:              move.b  (a0)+,(a1)+
                        dbra    d0,lbC018114
lbC01811A:              rts

; d0 contains the ID of the file
file_check_cache:       lea     files_ids,a1
                        moveq   #0,d1
lbC018124:              move.l  0(a1,d1.w),d2
                        beq     lbC018138
                        cmp.l   d2,d0
                        beq     lbC01813C
                        addq.w  #4,d1
                        bra.s   lbC018124

lbC018138:              moveq   #-1,d2
                        rts

lbC01813C:              moveq   #0,d2
                        rts

                        SECTION EXODUS018140,DATA

files_sizes:            dcb.l   80,0
files_ids:              dcb.l   80,0
files_addresses:        dcb.l   80,0

                        SECTION EXODUS018500,DATA

file_current_loading_address:
                        dc.l    file_loading_buffer
Escape_MSG:             dc.b    $1D,0                   ; escape ?
Pass_MSG0:              dc.b    "Pass",$ff,0
WHAT_MSG:               dc.b    "<-WHAT?",$ff,0
North_MSG:              dc.b    "North",$ff,0
South_MSG0:             dc.b    "South",$ff,0
East_MSG:               dc.b    "East",$ff,0
West_MSG:               dc.b    "West",$ff,0
INVALIDMOVE_MSG:        dc.b    "INVALID MOVE!",$ff,0
NOTHERE_MSG:            dc.b    "NOT HERE!",$ff,0
SoundON_MSG:            dc.b    "Sound ON",$ff,0
SoundOFF_MSG:           dc.b    "Sound OFF",$ff,0
MusicON_MSG:            dc.b    "Music ON",$ff,0
MusicOFF_MSG:           dc.b    "Music OFF",$ff,0
                        even

                        SECTION Commands,CODE

; ----------------------------------------
; (A)TTACK
command_Attack:         move.b  #$7A,$49(a3)
                        lea     Attack_MSG,a4
                        jsr     display_message
                        move.b  $77(a3),d0
                        beq     lbC01859C
                        jsr     set_direction
                        bra     lbC0185A2

lbC01859C:              jsr     input_select_direction
lbC0185A2:              jsr     lbC01C8F2
                        bpl     lbC0185B4
                        jmp     not_here_message

lbC0185B2:              nop
lbC0185B4:              move.w  d1,-(sp)
                        jsr     refresh_map_view
                        move.w  (sp)+,d1
                        lea     lbL022610,a0
                        move.b  0(a0,d1.w),PARTY_ACTION_X(a3)
                        lea     lbL022630,a0
                        move.b  0(a0,d1.w),PARTY_ACTION_Y(a3)
                        jsr     tile_get_action_datas
                        lea     lbL0225F0,a0
                        move.b  0(a0,d1.w),d0
                        beq     lbC0185F4
                        lsr.b   #2,d0
                        and.b   #3,d0
                        add.b   #$24,d0
lbC0185F4:
                        move.b  d0,(a5)
                        lea     lbL0225D0,a0
                        move.b  0(a0,d1.w),d0
                        lsr.b   #1,d0
                        move.b  d0,$5F(a3)
                        lea     lbL0225D0,a0
                        clr.b   0(a0,d1.w)
                        cmp.b   #$1E,d0
                        bne     lbC018628
                        move.b  11(a3),d0
                        cmp.b   #$16,d0
                        beq     lbC018628
                        move.b  #$2C,(a5)
lbC018628:
                        jmp     lbC01CF78

; ----------------------------------------
; (B)OARD
command_Board:          cmp.b   #$7E,11(a3)
                        beq     lbC01864C

lbC01863A:              move.l  #Board_MSG,a4
                        jsr     display_message
                        jmp     what_message

lbC01864C:              jsr     set_player_to_action
                        cmp.b   #$28,d0
                        beq     mount_horse
                        cmp.b   #$2C,d0
                        beq     board_ship
                        bra.s   lbC01863A

mount_horse:            move.b  #4,(a5)
                        move.b  #$14,d0
                        move.b  d0,11(a3)
                        move.b  d0,$4A(a3)
                        lea     MountHorse_MSG,a4
                        jsr     display_message
                        jmp     lbC01B400

board_ship:             clr.b   (a5)
                        move.b  #$16,d0
                        move.b  d0,11(a3)
                        move.b  d0,$4A(a3)
                        lea     Boardfrigate_MSG,a4
                        jsr     display_message
                        jmp     lbC01B400

; ----------------------------------------
; (C)AST
command_Cast:           move.b  #$78,$49(a3)
                        move.b  CURRENT_CHARACTER(a3),CURRENT_CHARACTER_SAVE(a3)
                        lea     Castbywhom_MSG,a4
                        jsr     display_message
                        jsr     input_select_character
                        bne     lbC0186E0
                        jmp     lbC01B400

incapacitated_message:  lea     INCAPACITATED_MSG,a4
                        jsr     display_message
                        jmp     command_aborted

lbC0186E0:              move.b  CURRENT_CHARACTER(a3),CURRENT_CHARACTER_SAVE(a3)
                        jsr     get_character_incapacitated_state
                        bne.s   incapacitated_message
lbC0186EE:
                        move.b  $17(a5),d0                  ; probably the last keystroke value
                        cmp.b   #$C3,d0
                        beq     command_cleric_spell
                        cmp.b   #$D0,d0
                        beq     command_cleric_spell
                        cmp.b   #$C9,d0
                        beq     command_cleric_spell
                        cmp.b   #$D7,d0
                        beq     command_wizard_spell
                        cmp.b   #$CC,d0
                        beq     command_wizard_spell
                        cmp.b   #$C1,d0
                        beq     command_wizard_spell
                        cmp.b   #$C4,d0
                        beq     command_select_spell_type
                        cmp.b   #$D2,d0
                        beq     command_select_spell_type
                        lea     NOTAMAGE_MSG,a4
                        jsr     display_message
                        jmp     command_aborted

command_select_spell_type:
                        lea     SPELLTYPEWC_MSG,a4
                        jsr     display_message
                        jsr     input_get_letter
                        cmp.b   #$D7,d0
                        beq     command_wizard_spell
                        cmp.b   #$C3,d0
                        bne.s   command_select_spell_type

command_cleric_spell:   lea     CLERICSPELL_MSG,a4
                        jsr     display_message
                        jsr     input_get_letter
                        cmp.b   #$C1,d0
                        bcs.s   command_cleric_spell
                        cmp.b   #$D1,d0
                        bcc.s   command_cleric_spell
                        sub.b   #$B1,d0                             ; will be key + 16
                        move.b  d0,SPELL_OR_OBJECT_NUMBER(a3)
                        bra     cast_spell

command_wizard_spell:   lea     WIZARDSPELL_MSG,a4
                        jsr     display_message
                        jsr     input_get_letter
                        cmp.b   #$C1,d0
                        bcs.s   command_wizard_spell
                        cmp.b   #$D1,d0
                        bcc.s   command_wizard_spell
                        sub.b   #$C1,d0
                        move.b  d0,SPELL_OR_OBJECT_NUMBER(a3)
                        bra     cast_spell

input_get_letter:       jsr     lbC01BD40
                        move.w  d0,-(sp)
                        and.b   #$7F,d0
                        jsr     draw_letter_both_bitmap
                        jsr     text_carriage_return
                        move.w  (sp)+,d0
                        rts

cast_spell:             jsr     get_character_data
                        move.b  SPELL_OR_OBJECT_NUMBER(a3),d1
                        and.b   #15,d1
                        move.b  #5,d0
                        jsr     lbC01B76E
                        move.b  CHARACTER_MAGICPOINTS(a5),d0
                        cmp.b   INPUT_NUMBER(a3),d0
                        bcc     enough_mp_to_cast
                        lea     MPTOOLOW_MSG,a4
                        jsr     display_message
                        jmp     command_aborted

enough_mp_to_cast:      move.b  INPUT_NUMBER(a3),d3
                        move.b  CHARACTER_MAGICPOINTS(a5),d0
                        move.w  #0,ccr
                        sbcd    d3,d0
                        move.b  d0,CHARACTER_MAGICPOINTS(a5)
                        jsr     text_carriage_return
                        move.b  SPELL_OR_OBJECT_NUMBER(a3),d0
                        add.b   #89,d0
                        jsr     get_description_message
                        jsr     text_carriage_return
                        jsr     text_carriage_return
                        clr.w   d0
                        move.b  SPELL_OR_OBJECT_NUMBER(a3),d0
                        lsl.w   #2,d0
                        lea     spells_table,a0
                        move.l  0(a0,d0.w),a0
                        jmp     (a0)

spells_table:           dc.l    spell_wiz_repond
                        dc.l    spell_wiz_mittar
                        dc.l    spell_wiz_lorum_cle_luminae
                        dc.l    spell_wiz_dor_acron_cle_rec_du
                        dc.l    spell_wiz_sur_acron_cle_rec_su
                        dc.l    spell_wiz_fulgar
                        dc.l    spell_wiz_dag_acron
                        dc.l    spell_wiz_mentar
                        dc.l    spell_wiz_dag_lorum_cle_sominae
                        dc.l    spell_wiz_fal_divi
                        dc.l    spell_wiz_noxum
                        dc.l    spell_wiz_decorp_cle_excuun
                        dc.l    spell_wiz_altair
                        dc.l    spell_wiz_dag_mentar
                        dc.l    spell_wiz_necorp
                        dc.l    spell_wiz_noname_cle_zxkuqyb

                        ; Some cleric spells have the same effects that wizards ones.
                        dc.l    spell_cle_pontori
                        dc.l    spell_cle_appar_unem
                        dc.l    spell_cle_sanctu
                        dc.l    spell_wiz_lorum_cle_luminae
                        dc.l    spell_wiz_sur_acron_cle_rec_su
                        dc.l    spell_wiz_dor_acron_cle_rec_du
                        dc.l    spell_cle_lib_rec
                        dc.l    spell_cle_alcort
                        dc.l    spell_cle_sequitu
                        dc.l    spell_wiz_dag_lorum_cle_sominae
                        dc.l    spell_cle_sanctu_mani
                        dc.l    spell_cle_vieda
                        dc.l    spell_wiz_decorp_cle_excuun
                        dc.l    spell_cle_surmandum
                        dc.l    spell_wiz_noname_cle_zxkuqyb
                        dc.l    spell_cle_anju_sermani

spell_wiz_repond:       cmp.b   #$80,$4C(a3)
                        bne     spell_failed
                        cmp.b   #$30,$5F(a3)
                        bne     spell_failed
                        tst.b   $17(a3)
                        bne     spell_failed
                        move.b  #$FF,$17(a3)
                        jsr     lbC02167A
                        bpl     spell_failed
                        bra     spell_wiz_noname_cle_zxkuqyb

spell_wiz_mittar:       move.b  #$28,d0
                        jsr     lbC0212F8
                        or.b    #$10,d0
                        move.b  d0,INPUT_NUMBER(a3)
lbC018912:
                        cmp.b   #$80,$4C(a3)
                        bne     spell_failed
                        lea     DIRECT_MSG,a4
                        jsr     display_message
                        jsr     input_select_direction
                        bsr     lbC018DC6
                        clr.w   d2
                        move.b  CURRENT_CHARACTER(a3),d2
                        lea     lbL0224C0,a0
                        move.b  0(a0,d2.w),PARTY_ACTION_X(a3)
                        lea     lbL0224C4,a0
                        move.b  0(a0,d2.w),PARTY_ACTION_Y(a3)
                        jsr     lbC01CB9E
                        bmi     spell_failed
                        move.b  d0,$74(a3)
                        move.w  #2,d0
                        jsr     play_sound
                        jsr     lbC01D8D2
                        bra     lbC018DC0
                        
spell_wiz_lorum_cle_luminae:    
                        move.b  #10,LIGHT_COUNTER(a3)
                        bsr     lbC018DC6
                        bra     lbC018DC0

spell_wiz_dor_acron_cle_rec_du: 
                        cmp.b   #1,$4C(a3)
                        bne     spell_failed
                        bsr     lbC018DC6
                        cmp.b   #7,CURRENT_DUNGEON_LEVEL(a3)
                        bcs     lbC01899C
                        bra     spell_failed

lbC01899C:              addq.b  #1,CURRENT_DUNGEON_LEVEL(a3)
                        bra     lbC018BEA

spell_wiz_sur_acron_cle_rec_su: 
                        cmp.b   #1,$4C(a3)
                        bne     spell_failed
                        bsr     lbC018DC6
                        tst.b   CURRENT_DUNGEON_LEVEL(a3)
                        beq     lbC018C88
                        sub.b   #1,CURRENT_DUNGEON_LEVEL(a3)
                        bra     lbC018BEA

spell_wiz_fulgar:       move.b  #$4B,INPUT_NUMBER(a3)
                        bra     lbC018912

spell_wiz_dag_acron:    bsr     lbC018DC6
                        tst.b   $4C(a3)
                        bne     spell_failed
                        cmp.b   #$16,11(a3)
                        beq     spell_failed

lbC0189E6:              move.b  #$40,d0
                        jsr     lbC0212F8
                        move.b  d0,PARTY_ACTION_X(a3)
                        move.b  #$40,d0
                        jsr     lbC0212F8
                        move.b  d0,PARTY_ACTION_Y(a3)
                        jsr     tile_get_action_datas
                        cmp.b   #4,d0
                        bne.s   lbC0189E6
                        move.b  PARTY_ACTION_X(a3),PARTY_X(a3)
                        move.b  PARTY_ACTION_Y(a3),PARTY_Y(a3)
                        bra     lbC018DC0

spell_wiz_mentar:       jsr     get_character_data
                        move.b  $14(a5),d0
                        jsr     lbC01B888
                        add.b   d0,d0
                        move.b  d0,INPUT_NUMBER(a3)
                        bra     lbC018912

spell_wiz_dag_lorum_cle_sominae:
                        move.b  #$FA,LIGHT_COUNTER(a3)
                        bsr     lbC018DC6
                        bra     lbC018DC0

spell_wiz_fal_divi:     bsr     lbC018DC6
                        jmp     command_cleric_spell(pc)

spell_wiz_noxum:        move.b  #$4B,INPUT_NUMBER(a3)
lbC018A54:              cmp.b   #$80,$4C(a3)
                        bne     spell_failed
                        bsr     lbC018DC6
                        move.b  #8,$74(a3)
lbC018A68:              subq.b  #1,$74(a3)
                        bpl     lbC018A78
                        jmp     lbC01B400

lbC018A78:              jsr     lbC02167A
                        and.b   #3,d0
                        beq.s   lbC018A68
                        clr.w   d1
                        move.b  $74(a3),d1
                        lea     lbL0224B8,a0
                        tst.b   0(a0,d1.w)
                        beq.s   lbC018A68
                        bsr     lbC018B16
                        jsr     lbC01D8D2
                        bra.s   lbC018A68

spell_wiz_decorp_cle_excuun:
                        move.b  #$FF,INPUT_NUMBER(a3)
                        bra     lbC018912

spell_wiz_altair:       bsr     lbC018DC6
                        move.b  #$14,NEGATE_TIME_COUNTER(a3)
                        bra     lbC018DC0

spell_wiz_dag_mentar:   jsr     get_character_data
                        move.b  $14(a5),d0
                        jsr     lbC01B888
                        add.b   d0,d0
                        move.b  d0,INPUT_NUMBER(a3)
                        bra.s   lbC018A54

spell_wiz_necorp:       cmp.b   #$80,$4C(a3)
                        bne     spell_failed
                        bsr     lbC018DC6
                        move.b  #8,$74(a3)
lbC018AE6:              subq.b  #1,$74(a3)
                        bpl     lbC018AF6
                        jmp     lbC01B400

lbC018AF6:              clr.w   d1
                        move.b  $74(a3),d1
                        lea     lbL0224B8,a0
                        tst.b   0(a0,d1.w)
                        beq.s   lbC018AE6
                        move.b  #5,0(a0,d1.w)
                        bsr     lbC018B16
                        bra.s   lbC018AE6

lbC018B16:              lea     lbL0224A0,a0
                        move.b  0(a0,d1.w),PARTY_ACTION_X(a3)
                        lea     lbL0224A8,a0
                        move.b  0(a0,d1.w),PARTY_ACTION_Y(a3)
                        jsr     lbC01CCC2
                        move.b  #$78,(a5)
                        jsr     display_coded_map_page
                        move.w  #2,d0
                        jsr     play_sound
                        move.b  $5F(a3),(a5)
                        jsr     display_coded_map_page
                        rts

spell_wiz_noname_cle_zxkuqyb:
                        move.b  #$FF,INPUT_NUMBER(a3)
                        bra     lbC018A54

spell_cle_pontori:      cmp.b   #$80,$4C(a3)
                        bne     spell_failed
                        cmp.b   #$32,$5F(a3)
                        bne     spell_failed
                        tst.b   $18(a3)
                        bne     spell_failed
                        move.b  #$FF,$18(a3)
                        jsr     lbC02167A
                        bpl     spell_failed
                        bra.s   spell_wiz_noname_cle_zxkuqyb

spell_cle_sanctu:       move.b  #$14,d0
                        jsr     lbC0212F8
                        add.b   #10,d0
                        jsr     lbC01B8A4
                        move.b  d0,INPUT_NUMBER(a3)
                        bra     lbC018BA8

lbC018BA8:              lea     HEALWHOM_MSG,a4
                        jsr     display_message
                        jsr     input_select_character
                        beq     spell_failed
                        move.b  INPUT_NUMBER(a3),d0
                        jsr     lbC01B7F0
                        jsr     lbC021124
                        bsr     lbC018DC6
                        jsr     lbC021124
                        bra     lbC018DC0

spell_cle_lib_rec:      bsr     lbC018DC6
                        cmp.b   #1,$4C(a3)
                        bne     spell_failed
lbC018BEA:              move.b  #$10,d0
                        jsr     lbC0212F8
                        move.b  d0,PARTY_ACTION_X(a3)
                        move.b  #$10,d0
                        jsr     lbC0212F8
                        move.b  d0,PARTY_ACTION_Y(a3)
                        jsr     tile_dungeon_get_action_datas
                        bne.s   lbC018BEA
                        move.b  PARTY_ACTION_X(a3),PARTY_X(a3)
                        move.b  PARTY_ACTION_Y(a3),PARTY_Y(a3)
                        bra     lbC018DC0

spell_cle_alcort:       lea     CUREWHOM_MSG,a4
                        jsr     display_message
                        jsr     input_select_character
                        beq     spell_failed
                        jsr     lbC021124
                        bsr     lbC018DC6
                        jsr     lbC021124
                        jsr     get_character_data
                        cmp.b   #$D0,$11(a5)
                        bne     spell_failed
                        move.b  #$C7,$11(a5)
                        bra     lbC018DC0

spell_cle_appar_unem:   bsr     lbC018DC6
                        jsr     lbC02167A
                        and.b   #3,d0
                        beq     spell_failed
                        clr.b   13(a3)
                        jmp     lbC019318

spell_cle_sequitu:      cmp.b   #1,$4C(a3)
                        bne     spell_failed
                        bsr     lbC018DC6
lbC018C88:              jmp     lbC01B454

spell_cle_sanctu_mani:  move.b  #$50,d0
                        jsr     lbC0212F8
                        add.b   #20,d0
                        jsr     lbC01B8A4
                        move.b  d0,INPUT_NUMBER(a3)
                        bra     lbC018BA8

spell_cle_vieda:        bsr     lbC018DC6
                        cmp.b   #1,$4C(a3)
                        beq     lbC018CBE
                        jmp     lbC01A32E

lbC018CBE:              jmp     lbC01E46C

spell_cle_surmandum:    cmp.b   #$80,$4C(a3)
                        beq     spell_failed
                        lea     RESURRECTWHOM_MSG,a4
                        jsr     display_message
                        jsr     input_select_character
                        beq     spell_failed
                        jsr     lbC021124
                        bsr     lbC018DC6
                        jsr     lbC021124
                        jsr     get_character_data
                        cmp.b   #$C4,$11(a5)
                        bne     spell_failed
                        jsr     lbC02167A
                        and.b   #3,d0
                        bne     lbC018D1C
                        move.b  #$C1,$11(a5)
                        bra     spell_failed

lbC018D1C:              move.b  #$C7,$11(a5)
                        bra     lbC018DC0

spell_cle_anju_sermani: cmp.b   #$80,$4C(a3)
                        beq     spell_failed
                        lea     RECALLWHOM_MSG,a4
                        jsr     display_message
                        jsr     input_select_character
                        beq     spell_failed
                        jsr     lbC021124
                        bsr     lbC018DC6
                        jsr     lbC021124
                        jsr     get_character_data
                        cmp.b   #$C1,$11(a5)
                        bne     spell_failed
                        move.b  CURRENT_CHARACTER(a3),d0
                        move.w  d0,-(sp)
                        move.b  CURRENT_CHARACTER_SAVE(a3),d0
                        move.b  d0,CURRENT_CHARACTER(a3)
                        jsr     get_character_data
                        move.b  #5,d3
                        move.b  $15(a5),d0
                        move.w  #0,ccr
                        sbcd    d3,d0
                        move.b  d0,$15(a5)
                        move.w  (sp)+,d0
                        move.b  d0,CURRENT_CHARACTER(a3)
                        jsr     get_character_data
                        move.b  #$C7,$11(a5)
                        bra     lbC018DC0

spell_failed:           lea     FAILED_MSG,a4
                        jsr     display_message
                        move.w  #3,d0
                        jsr     play_sound
                        jmp     lbC01B400

lbC018DC0:              jmp     lbC01B400

lbC018DC6:              jsr     lbC018DE0(pc)
                        jsr     lbC018DEC(pc)
                        jsr     lbC018DE0(pc)
                        jsr     lbC018DEC(pc)
                        rts

lbC018DE0:              move.w  #19,d0
                        jsr     play_sound
                        rts

lbC018DEC:              jmp     lbC021B1E

                        SECTION EXODUS018DF4,DATA

Attack_MSG:             dc.b    "Attack-",0
Board_MSG:              dc.b    "Board",0
MountHorse_MSG:         dc.b    "Mount Horse!",$ff,0
Boardfrigate_MSG:       dc.b    "Board frigate!",$ff,0
Castbywhom_MSG:         dc.b    "Cast by whom?-",0
INCAPACITATED_MSG:      dc.b    "INCAPACITATED!",$ff,0
NOTAMAGE_MSG:           dc.b    "NOT A MAGE!",$ff,0
SPELLTYPEWC_MSG:        dc.b    "SPELL TYPE W/C-",0
CLERICSPELL_MSG:        dc.b    "CLERIC SPELL-",0
WIZARDSPELL_MSG:        dc.b    "WIZARD SPELL-",0
MPTOOLOW_MSG:           dc.b    "M.P. TOO LOW!",$ff,0
DIRECT_MSG:             dc.b    "DIRECT-",0
HEALWHOM_MSG:           dc.b    "HEAL WHOM?-",0
CUREWHOM_MSG:           dc.b    "CURE WHOM?-",0
RESURRECTWHOM_MSG:      dc.b    "RESURRECT",$ff
                        dc.b    "WHOM?-",0
RECALLWHOM_MSG:         dc.b    "RECALL WHOM?-",0
FAILED_MSG:             dc.b    "FAILED!",$ff,0
                        even

                        SECTION EXODUS018ED0,CODE

; ----------------------------------------
; (D)ESCEND
command_Descend:        lea     Descend_MSG,a4
                        jsr     display_message
                        jmp     what_message

; ----------------------------------------
; (E)NTER
command_Enter:          lea     Enter_MSG,a4
                        jsr     display_message
                        tst.b   $4C(a3)
                        beq     lbC018F06
                        cmp.b   #$FF,$4C(a3)
                        beq     lbC0190A8
lbC018F00:              jmp     lbC017E70

lbC018F06:              move.w  #$13,d1
                        lea     lbB026992,a0
                        lea     lbB0269A5,a1
                        move.b  PARTY_X(a3),d4
                        move.b  PARTY_Y(a3),d5
lbC018F1E:
                        subq.w  #1,d1
                        bpl     lbC018F2C
                        jmp     lbC018FF0

lbC018F2C:              cmp.b   0(a0,d1.w),d4
                        bne.s   lbC018F1E
                        cmp.b   0(a1,d1.w),d5
                        bne.s   lbC018F1E
                        move.b  d1,$74(a3)
                        move.b  11(a3),$4A(a3)
                        move.b  PARTY_X(a3),PARTY_X_SAVE(a3)
                        move.b  PARTY_Y(a3),PARTY_Y_SAVE(a3)
                        cmp.b   #10,6(a3)
                        bne     lbC018F82
                        lea     dungeon_MSG,a4
                        jsr     display_message
                        clr.b   CURRENT_DUNGEON_LEVEL(a3)
                        move.b  #1,d0
                        move.b  d0,$4C(a3)
                        move.b  d0,PARTY_X(a3)
                        move.b  d0,PARTY_Y(a3)
                        move.b  d0,$3E(a3)
                        jmp     lbC018FF6

lbC018F82:              cmp.b   #12,6(a3)
                        bne     lbC018FB6
                        lea     towne_MSG,a4
                        jsr     display_message
                        move.b  #$20,PARTY_Y(a3)
                        move.b  #1,PARTY_X(a3)
                        move.b  #2,d0
                        move.b  d0,$4C(a3)
                        move.b  d0,12(a3)
                        jmp     lbC018FF6

lbC018FB6:              cmp.b   #14,6(a3)
                        bne     lbC018FF0
                        move.b  #$1E,$19(a3)
                        lea     castle_MSG,a4
                        jsr     display_message
                        move.b  #3,$4C(a3)
                        move.b  #$20,PARTY_X(a3)
                        move.b  #$3E,PARTY_Y(a3)
                        move.b  #2,12(a3)
                        jmp     lbC018FF6

lbC018FF0:              jmp     what_message

lbC018FF6:              jsr     reset_music
                        jsr     lbC01A3E6
                        add.b   #$41,$74(a3)
                        move.b  $74(a3),lbB019CB2
                        move.l  #'MAPA',d0
                        move.b  $74(a3),d0                  ; Map number
                        lea     current_map,a0
                        jsr     file_load_cache
                        move.l  #'TLKA',d0                  ; Text
                        move.b  $74(a3),d0
                        lea     lbL0226D0,a0
                        jsr     file_load_cache
                        cmp.b   #1,$4C(a3)
                        beq     lbC0190A2
                        move.l  #'MONA',d0
                        move.b  $74(a3),d0
                        lea     lbL0225D0,a0
                        jsr     file_load_cache
                        cmp.b   #2,$4C(a3)
                        bne     lbC019074
                        move.b  $4C(a3),lbB028FDC
                        jmp     lbC01B400

lbC019074:              move.b  PARTY_X_SAVE(a3),d0
                        lea     lbB026992,a0
                        cmp.b   1(a0),d0
                        bne     lbC019094
                        move.b  #7,lbB028FDC
                        jmp     lbC01B400

lbC019094:              move.b  #3,lbB028FDC
                        jmp     lbC01B400

lbC0190A2:              jmp     lbC01E068

lbC0190A8:              move.b  PARTY_X(a3),PARTY_ACTION_X(a3)
                        move.b  PARTY_Y(a3),PARTY_ACTION_Y(a3)
                        jsr     tile_get_action_datas
                        cmp.b   #$F8,d0
                        bne     lbC018F00
                        lea     shrineWHOENTE_MSG,a4
                        jsr     display_message
                        jsr     input_select_character
                        bne     lbC0190DE
                        jmp     command_aborted

lbC0190DE:              jsr     get_character_incapacitated_state
                        beq     lbC0190EE
                        jmp     incapacitated_message

lbC0190EE:              jsr     reset_music
                        move.l  #'SHNE',d0
                        jsr     file_load_encrypted
                    
                        jsr     generate_jumps_table
                        move.l  #'SHRN',d0
                        lea     lbL022420,a0
                        jsr     file_load_cache
                        jsr     display_coded_map_page
                        move.b  #10,d0
                        jsr     start_new_music
                        jsr     lbB0237D0
lbC01915A:              move.b  #11,lbB028FDC
                        jmp     lbC01B400

; ----------------------------------------
; (F)IRE (only available when on board of a ship)
command_Fire_ship:      lea     Fire_MSG,a4
                        jsr     display_message
                        cmp.b   #22,11(a3)
                        beq     lbC019184
                        jmp     what_message

lbC019184:              tst.b   $77(a3)
                        beq     lbC0191A6
                        lea     ascii_MSG16,a4
                        jsr     display_message
                        move.b  $77(a3),d0
                        jsr     set_direction
                        bra     lbC0191B8

lbC0191A6:              lea     DIRECT_MSG1,a4
                        jsr     display_message
                        jsr     input_select_direction
lbC0191B8:      
                        move.w  #5,d0
                        jsr     play_sound
                        move.b  PARTY_X(a3),PARTY_ACTION_X(a3)
                        move.b  PARTY_Y(a3),PARTY_ACTION_Y(a3)
                        move.b  #4,$65(a3)
lbC0191D4:
                        subq.b  #1,$65(a3)
                        beq     lbC019224
                        move.b  PARTY_DIRECTION_X(a3),d0
                        add.b   d0,PARTY_ACTION_X(a3)
                        and.b   #$3F,PARTY_ACTION_X(a3)
                        move.b  PARTY_DIRECTION_Y(a3),d0
                        add.b   d0,PARTY_ACTION_Y(a3)
                        and.b   #$3F,PARTY_ACTION_Y(a3)
                        jsr     lbC01C8F2
                        bpl     lbC019230
                        jsr     tile_get_action_datas
                        move.w  d0,-(sp)
                        move.b  #$F4,(a5)
                        jsr     refresh_map_view
                        jsr     tile_get_action_datas
                        move.w  (sp)+,d0
                        move.b  d0,(a5)
                        jmp     lbC0191D4(pc)

lbC019224:              jsr     refresh_map_view
                        jmp     lbC01B400

lbC019230:
                        move.b  d0,$5F(a3)
                        jsr     tile_get_action_datas
                        move.w  d0,-(sp)
                        move.b  #$F4,(a5)
                        jsr     refresh_map_view
                        move.w  #4,d0
                        jsr     play_sound
                        jsr     tile_get_action_datas
                        clr.w   d1
                        move.b  $5F(a3),d1
                        lea     lbL0225D0,a0
                        cmp.b   #$3C,0(a0,d1.w)
                        bne     lbC01927C
                        jsr     lbC02167A
                        bmi     lbC01927C
                        jmp     lbC0192D0

lbC01927C:              jsr     lbC02167A
                        bpl     lbC0192D0
                        lea     lbL0225D0,a0
                        move.b  0(a0,d1.w),d0
                        lsr.b   #2,d0
                        addq.b  #1,d0
                        jsr     get_description_message
                        lea     SDESTROYED_MSG,a4
                        jsr     display_message
                        move.w  (sp)+,d0
                        jsr     tile_get_action_datas
                        clr.w   d1
                        move.b  $5F(a3),d1
                        lea     lbL0225F0,a0
                        move.b  0(a0,d1.w),(a5)
                        lea     lbL0225D0,a0
                        clr.b   0(a0,d1.w)
                        jmp     lbC01B400

lbC0192D0:              jsr     tile_get_action_datas
                        move.w  (sp)+,d0
                        move.b  d0,(a5)
                        jsr     display_coded_map_page
                        jmp     lbC01B400

; ----------------------------------------
; (G)ET CHEST (open chest)
command_GetChest:       move.b  #$FF,13(a3)
                        lea     GETCHESTPLRTO_MSG,a4
                        jsr     display_message
                        jsr     input_select_character
                        bne     lbC019308
                        jmp     lbC01B400

lbC019308:              jsr     get_character_incapacitated_state
                        beq     lbC019318
                        jmp     incapacitated_message

lbC019318:              move.b  PARTY_X(a3),PARTY_ACTION_X(a3)
                        move.b  PARTY_Y(a3),PARTY_ACTION_Y(a3)
                        cmp.b   #1,$4C(a3)
                        beq     lbC019360
                        jsr     tile_get_action_datas
                        cmp.b   #$24,d0
                        bcs     lbC01935A
                        cmp.b   #$28,d0
                        bcc     lbC01935A
                        and.b   #3,d0
                        lsl.b   #2,d0
                        bne     lbC019352
                        move.b  #$20,d0
lbC019352:
                        move.b  d0,(a5)
                        jmp     lbC01936E

lbC01935A:              jmp     not_here_message

lbC019360:              jsr     tile_dungeon_get_action_datas
                        cmp.b   #$40,d0
                        bne.s   lbC01935A
                        clr.b   (a5)
lbC01936E:              tst.b   13(a3)
                        beq     chest_gold
                        jsr     lbC02167A
                        bpl     chest_gold
                        jsr     lbC02167A
                        move.b  d0,$74(a3)
                        jsr     lbC02167A
                        and.b   $74(a3),d0
                        and.b   #3,d0
                        beq     chest_acid_trap
                        cmp.b   #1,d0
                        beq     chest_poison_trap
                        cmp.b   #2,d0
                        beq     chest_bomb_trap
                        jmp     chest_gas_trap

chest_acid_trap:        lea     ACIDTRAP_MSG,a4
                        jsr     display_message
                        jsr     lbC019518
                        beq     chest_gold
                        jsr     lbC021124
                        move.w  #4,d0
                        jsr     play_sound
                        jsr     lbC021124
                        jsr     lbC02167A
                        and.b   #$37,d0
                        jsr     lbC01B8C2
                        jmp     chest_gold

chest_poison_trap:      lea     POISONTRAP_MSG,a4
                        jsr     display_message
                        jsr     lbC019518
                        beq     chest_gold
                        jsr     lbC021124
                        move.w  #4,d0
                        jsr     play_sound
                        jsr     lbC021124
                        jsr     get_character_data
                        move.b  #$D0,$11(a5)
                        jmp     chest_gold

chest_bomb_trap:        lea     BOMBTRAP_MSG,a4
                        jsr     display_message
                        jsr     lbC019518
                        beq     chest_gold
                        jsr     lbC019454
                        jmp     lbC01B400

lbC019454:              move.b  PARTY_CHARACTERS(a3),CURRENT_CHARACTER(a3)
                        subq.b  #1,CURRENT_CHARACTER(a3)

lbC019460:              jsr     get_character_incapacitated_state
                        bne     lbC0194A0
                        jsr     lbC021124
                        move.w  #4,d0
                        jsr     play_sound
                        jsr     lbC021124
                        jsr     lbC02167A
                        and.b   #$77,d0
                        jsr     lbC01B8C2
                        move.b  CURRENT_DUNGEON_LEVEL(a3),d0
                        addq.b  #1,d0
                        lsl.b   #3,d0
                        jsr     lbC01B8C2

lbC0194A0:              subq.b  #1,CURRENT_CHARACTER(a3)
                        bpl.s   lbC019460
                        jsr     lbC01BB68
                        rts

chest_gas_trap:         move.b  CURRENT_CHARACTER(a3),CURRENT_CHARACTER_SAVE(a3)
                        lea     GASTRAP_MSG,a4
                        jsr     display_message
                        jsr     lbC019518
                        beq     chest_gold
                        move.b  PARTY_CHARACTERS(a3),CURRENT_CHARACTER(a3)
                        subq.b  #1,CURRENT_CHARACTER(a3)

lbC0194D8:              jsr     get_character_incapacitated_state
                        bne     lbC019504
                        jsr     lbC021124
                        move.w  #4,d0
                        jsr     play_sound
                        jsr     lbC021124
                        jsr     get_character_data
                        move.b  #$D0,$11(a5)
lbC019504:
                        subq.b  #1,CURRENT_CHARACTER(a3)
                        bpl.s   lbC0194D8
                        move.b  CURRENT_CHARACTER_SAVE(a3),CURRENT_CHARACTER(a3)
                        jmp     chest_gold

lbC019518:              jsr     lbC01BF78
                        beq     lbC019524
                        rts

lbC019524:              lea     TRAPEVADED_MSG,a4
                        jsr     display_message
                        move.w  #1,d0
                        jsr     play_sound
                        clr.b   d0
                        rts

chest_gold:             lea     GOLD_MSG,a4
                        jsr     display_message
                        move.b  #$64,d0
                        jsr     lbC0212F8
                        or.b    #$30,d0
                        jsr     lbC01B8A4
                        move.b  d0,$74(a3)
                        jsr     draw_2_digits
                        jsr     text_carriage_return
                        move.b  $74(a3),d0
                        jsr     lbC01B7BE
                        jsr     lbC02167A
                        cmp.b   #$40,d0
                        bcs     lbC01958C
                        jmp     lbC01B400

lbC01958C:              jsr     lbC02167A
                        bmi     lbC0195E8
                        move.b  d0,$74(a3)
                        jsr     lbC02167A
                        and.b   $74(a3),d0
                        and.b   #7,d0
                        beq     lbC0195E8
                        move.b  d0,$74(a3)
                        lea     ANDA_MSG,a4
                        jsr     display_message
                        move.b  $74(a3),d0
                        add.b   #$41,d0
                        jsr     get_description_message
                        jsr     text_carriage_return
                        move.b  $74(a3),d2
                        add.b   #$30,d2
                        move.b  #1,d0
                        jsr     set_character_byte_value
                        jmp     lbC01B400

lbC0195E8:              jsr     lbC02167A
                        bmi     lbC019644
                        move.b  d0,$74(a3)
                        jsr     lbC02167A
                        and.b   $74(a3),d0
                        and.b   #3,d0
                        beq     lbC019644
                        move.b  d0,$74(a3)
                        lea     AND_MSG,a4
                        jsr     display_message
                        move.b  $74(a3),d0
                        add.b   #$51,d0
                        jsr     get_description_message
                        jsr     text_carriage_return
                        move.b  $74(a3),d2
                        add.b   #$28,d2
                        move.b  #1,d0
                        jsr     set_character_byte_value
                        jmp     lbC01B400

lbC019644:              jmp     lbC01B400

; ----------------------------------------
; (H)AND EQUIPMENT
command_HandEquipment:  lea     Handequipment_MSG,a4
                        jsr     display_message
                        jsr     input_select_character
                        beq     lbC0196E6
                        jsr     get_character_data
                        tst.b   $11(a5)
                        beq     lbC0196E6
                        move.b  CURRENT_CHARACTER(a3),CURRENT_CHARACTER_SAVE(a3)
                        lea     TOPLR_MSG,a4
                        jsr     display_message
                        jsr     input_select_character
                        beq     lbC0196E6
                        jsr     get_character_data
                        tst.b   $11(a5)
                        beq     lbC0196E6
                        move.b  CURRENT_CHARACTER(a3),d0
                        move.b  CURRENT_CHARACTER_SAVE(a3),CURRENT_CHARACTER(a3)
                        move.b  d0,CURRENT_CHARACTER_SAVE(a3)
                        lea     FGEWA_MSG,a4
                        jsr     display_message
                        jsr     input_get_letter
                        cmp.b   #$C6,d0
                        beq     lbC0196EC
                        cmp.b   #$C7,d0
                        beq     lbC019758
                        cmp.b   #$C5,d0
                        beq     lbC0197A0
                        cmp.b   #$D7,d0
                        beq     lbC01985A
                        cmp.b   #$C1,d0
                        beq     lbC0198E2
                        jmp     lbC0198D0

lbC0196E6:              jmp     display_invalid_character_msg

lbC0196EC:              lea     AMOUNT_MSG,a4
                        jsr     display_message
                        jsr     input_get_number
                        jsr     text_carriage_return
                        move.b  #$20,d2
                        move.b  INPUT_NUMBER(a3),d0
                        jsr     lbC019964
                        beq     lbC019732

lbC019716:              lea     NOTENOUGH_MSG,a4
                        jsr     display_message
                        move.w  #1,d0
                        jsr     play_sound
                        jmp     lbC01B400

lbC019732:              move.b  CURRENT_CHARACTER_SAVE(a3),CURRENT_CHARACTER(a3)
                        move.b  INPUT_NUMBER(a3),d0
                        move.b  #$20,d2
                        jsr     increase_character_food

done_message:           lea     DONE_MSG,a4
                        jsr     display_message
                        jmp     lbC01B400

lbC019758:              lea     AMOUNT_MSG,a4
                        jsr     display_message
                        jsr     input_get_number
                        jsr     text_carriage_return
                        move.b  #$23,d2
                        move.b  INPUT_NUMBER(a3),d0
                        jsr     lbC019964
                        beq     lbC019786
                        jmp     lbC019716(pc)

lbC019786:              move.b  CURRENT_CHARACTER_SAVE(a3),CURRENT_CHARACTER(a3)
                        move.b  INPUT_NUMBER(a3),d0
                        move.b  #$23,d2
                        jsr     lbC01B7BE
                        jmp     done_message(pc)

lbC0197A0:              lea     GKPT_MSG,a4
                        jsr     display_message
                        jsr     input_get_letter
                        cmp.b   #$C7,d0
                        bne     lbC0197C4
                        move.b  #$25,d2
                        jmp     lbC019800

lbC0197C4:              cmp.b   #$CB,d0
                        bne     lbC0197D6
                        move.b  #$26,d2
                        jmp     lbC019800

lbC0197D6:              cmp.b   #$D0,d0
                        bne     lbC0197E8
                        move.b  #$27,d2
                        jmp     lbC019800

lbC0197E8:              cmp.b   #$D4,d0
                        bne     lbC0197FA
                        move.b  #15,d2
                        jmp     lbC019800

lbC0197FA:              jmp     lbC0198D0

lbC019800:              move.b  d2,$74(a3)
                        lea     HOWMANY_MSG,a4
                        jsr     display_message
                        jsr     input_get_number
                        jsr     text_carriage_return
                        jsr     get_character_data
                        clr.w   d2
                        move.b  $74(a3),d2
                        move.b  0(a5,d2.w),d0
                        move.b  INPUT_NUMBER(a3),d3
                        move.w  #0,ccr
                        sbcd    d3,d0
                        bcc     lbC01983E
                        jmp     lbC019716(pc)

lbC01983E:              move.b  d0,0(a5,d2.w)
                        move.b  CURRENT_CHARACTER_SAVE(a3),CURRENT_CHARACTER(a3)
                        move.b  $74(a3),d2
                        move.b  INPUT_NUMBER(a3),d0
                        jsr     set_character_byte_value
                        jmp     done_message(pc)

lbC01985A:              lea     WHICHWEAPON_MSG,a4
                        jsr     display_message
                        jsr     input_get_letter
                        cmp.b   #$C2,d0
                        bcs     lbC0198D0
                        cmp.b   #$D1,d0
                        bcc     lbC0198D0
                        sub.b   #$C1,d0
                        move.b  d0,$69(a3)
                        jsr     get_character_data
                        move.b  $30(a5),d0
                        cmp.b   $69(a3),d0
                        bne     lbC01989C
                        jmp     lbC019952

lbC01989C:              clr.w   d2
                        move.b  $69(a3),d2
                        add.w   #$30,d2
                        move.b  0(a5,d2.w),d0
                        beq     lbC0198D0
                        move.b  #1,d3
                        move.w  #0,ccr
                        sbcd    d3,d0
                        move.b  d0,0(a5,d2.w)
                        move.b  CURRENT_CHARACTER_SAVE(a3),CURRENT_CHARACTER(a3)
                        move.b  #1,d0
                        jsr     set_character_byte_value
                        jmp     done_message(pc)

lbC0198D0:              lea     NONE_MSG0,a4
                        jsr     display_message
                        jmp     command_aborted

lbC0198E2:              lea     WHICHARMOUR_MSG,a4
                        jsr     display_message
                        jsr     input_get_letter
                        cmp.b   #$C2,d0
                        bcs.s   lbC0198D0
                        cmp.b   #$C9,d0
                        bcc.s   lbC0198D0
                        sub.b   #$C1,d0
                        move.b  d0,$69(a3)
                        jsr     get_character_data
                        move.b  $28(a5),d0
                        cmp.b   $69(a3),d0
                        beq     lbC019952
                        clr.w   d2
                        move.b  $69(a3),d2
                        add.w   #$28,d2
                        move.b  0(a5,d2.w),d0
                        bne     lbC019930
                        jmp     lbC0198D0(pc)

lbC019930:              move.b  #1,d3
                        move.w  #0,ccr
                        sub.b   d3,d0
                        move.b  d0,0(a5,d2.w)
                        move.b  CURRENT_CHARACTER_SAVE(a3),CURRENT_CHARACTER(a3)
                        move.b  #1,d0
                        jsr     set_character_byte_value
                        jmp     done_message(pc)

lbC019952:              lea     INUSE_MSG,a4
                        jsr     display_message
                        jmp     command_aborted

lbC019964:              move.b  d0,$69(a3)
                        move.b  d2,$6B(a3)
                        jsr     get_character_data
                        clr.w   d2
                        move.b  $6B(a3),d2
                        move.b  $69(a3),d3
                        move.b  1(a5,d2.w),d4
                        move.w  #0,ccr
                        sbcd    d3,d4
                        move.b  0(a5,d2.w),d0
                        clr.b   d3
                        sbcd    d3,d0
                        bcs     lbC0199A0
                        move.b  d0,0(a5,d2.w)
                        move.b  d4,1(a5,d2.w)
                        move.b  #0,d0
                        rts

lbC0199A0:              move.b  #$FF,d0
                        rts

; ----------------------------------------
; (I)GNITE TORCH
command_IgniteTorch:    lea     Igniteatorch_MSG,a4
                        jsr     display_message
                        cmp.b   #1,$4C(a3)
                        beq.b   select_torch_character
                        jmp     not_here_message

select_torch_character: lea     WHOSETORCH_MSG,a4
                        jsr     display_message
                        jsr     input_select_character
                        bne.b   set_torch_used
                        jmp     lbC01B400

set_torch_used:         jsr     get_character_data
                        move.b  CHARACTER_TORCHES(a5),d0
                        bne.b   decrease_number_torchs
none_left_message:      lea     NONELEFT_MSG,a4
                        jsr     display_message
                        jmp     lbC01B400

decrease_number_torchs: move.b  #1,d3
                        move.w  #0,ccr
                        sbcd    d3,d0
                        move.b  d0,CHARACTER_TORCHES(a5)
                        move.b  #$FF,LIGHT_COUNTER(a3)
                        jmp     lbC01B400

; ----------------------------------------
; (J)OIN GOLD
command_JoinGold:       lea     Joingoldto_MSG,a4
                        jsr     display_message
                        jsr     input_select_character
                        bne     lbC019A36
                        jmp     lbC01B400

lbC019A36:              cmp.b   PARTY_CHARACTERS(a3),d0
                        bls     lbC019A44
                        jmp     display_invalid_character_msg

lbC019A44:              move.b  d0,$74(a3)
                        clr.b   TEMP_NUMBER_W_HI(a3)
                        clr.b   TEMP_NUMBER_W_LO(a3)
                        clr.b   CURRENT_CHARACTER(a3)

lbC019A54:              jsr     get_character_data
                        move.b  CHARACTER_MONEY_LO(a5),d0
                        clr.b   CHARACTER_MONEY_LO(a5)
                        move.b  TEMP_NUMBER_W_LO(a3),d3
                        move.w  #0,ccr
                        abcd    d3,d0
                        move.b  d0,TEMP_NUMBER_W_LO(a3)
                        move.b  CHARACTER_MONEY_HI(a5),d0
                        clr.b   CHARACTER_MONEY_HI(a5)
                        move.b  TEMP_NUMBER_W_HI(a3),d3
                        abcd    d3,d0
                        bcc     lbC019A8A
                        move.b  #$99,d0
                        move.b  d0,TEMP_NUMBER_W_LO(a3)
lbC019A8A:
                        move.b  d0,TEMP_NUMBER_W_HI(a3)
                        addq.b  #1,CURRENT_CHARACTER(a3)
                        move.b  CURRENT_CHARACTER(a3),d0
                        cmp.b   PARTY_CHARACTERS(a3),d0
                        bcs.s   lbC019A54
                        move.b  $74(a3),CURRENT_CHARACTER(a3)
                        subq.b  #1,CURRENT_CHARACTER(a3)
                        jsr     get_character_data
                        move.b  TEMP_NUMBER_W_HI(a3),CHARACTER_MONEY_HI(a5)
                        move.b  TEMP_NUMBER_W_LO(a3),CHARACTER_MONEY_LO(a5)
                        jmp     lbC01B400

; ----------------------------------------
; (K)LIMB
command_Klimb:          lea     KlimbWHAT_MSG,a4
                        jsr     display_message
                        jmp     command_aborted

; ----------------------------------------
; (L)OOK
command_Look:           lea     Look_MSG,a4
                        jsr     display_message
                        jsr     input_select_direction
                        lea     ascii_MSG1,a4
                        jsr     display_message
                        jsr     tile_get_action_datas
                        lsr.b   #2,d0
                        addq.b  #1,d0
                        jsr     get_description_message
                        jsr     text_carriage_return
                        jmp     lbC01B400

                        SECTION EXODUS019B10,DATA

Descend_MSG:            dc.b    "Descend",0
Enter_MSG:              dc.b    "Enter ",0
dungeon_MSG:            dc.b    "dungeon!",$ff,0
towne_MSG:              dc.b    "towne!",$ff,0
castle_MSG:             dc.b    "castle!",$ff,0
shrineWHOENTE_MSG:      dc.b    "shrine!",$ff
                        dc.b    "WHO ENTERS?-",0
Fire_MSG:               dc.b    "Fire!",0
ascii_MSG16:            dc.b    "-",0
DIRECT_MSG1:            dc.b    $ff
                        dc.b    "DIRECT-",0
SDESTROYED_MSG:         dc.b    "S",$ff
                        dc.b    "DESTROYED!",$ff,0
GETCHESTPLRTO_MSG:      dc.b    "GET CHEST!",$ff
                        dc.b    "PLR TO SEARCH-",0
ACIDTRAP_MSG:           dc.b    "ACID TRAP!",$ff,0
POISONTRAP_MSG:         dc.b    "POISON TRAP!",$ff,0
BOMBTRAP_MSG:           dc.b    "BOMB TRAP!",$ff,0
GASTRAP_MSG:            dc.b    "GAS TRAP!",$ff,0
TRAPEVADED_MSG:         dc.b    "TRAP EVADED!",$ff,0
GOLD_MSG:               dc.b    "GOLD+",0
ANDA_MSG:               dc.b    "AND A ",0
AND_MSG:                dc.b    "AND ",0
Handequipment_MSG:      dc.b    "Hand equipment",$ff
                        dc.b    "FROM PLR-",0
TOPLR_MSG:              dc.b    "  TO PLR-",0
FGEWA_MSG:              dc.b    "F,G,E,W,A:",0
AMOUNT_MSG:             dc.b    "AMOUNT-",0
NOTENOUGH_MSG:          dc.b    "NOT ENOUGH!",$ff,0
DONE_MSG:               dc.b    "DONE!",$ff,0
GKPT_MSG:               dc.b    "G,K,P,T:",0
HOWMANY_MSG:            dc.b    "HOW MANY-",0
WHICHWEAPON_MSG:        dc.b    "WHICH WEAPON:",0
NONE_MSG0:              dc.b    "NONE!",$ff,0
WHICHARMOUR_MSG:        dc.b    "WHICH ARMOUR:",0
INUSE_MSG:              dc.b    "IN USE!",$ff,0
Igniteatorch_MSG:       dc.b    "Ignite a torch",$ff,0
WHOSETORCH_MSG:         dc.b    "WHOSE TORCH-",0
NONELEFT_MSG:           dc.b    "NONE LEFT!",$ff,0
Joingoldto_MSG:         dc.b    "Join gold to-",0
KlimbWHAT_MSG:          dc.b    "Klimb <-WHAT?",$ff,0
Look_MSG:               dc.b    "Look-",0
ascii_MSG1:             dc.b    "->",0,0
lbB019CB2:              dcb.b   2,0

                        SECTION EXODUS019CB4,CODE

; ----------------------------------------
; (M)ODIFY ORDER
command_ModifyOrder:    lea     ModifyorderPL_MSG,a4
                        jsr     display_message
                        jsr     input_select_character
                        bne     lbC019CD0
                        jmp     aborted_message

lbC019CD0:              subq.b  #1,d0
                        move.b  d0,TEMP_NUMBER_W_HI(a3)
                        cmp.b   PARTY_CHARACTERS(a3),d0
                        bcc     aborted_message
                        lea     PLR_MSG,a4
                        jsr     display_message
                        jsr     input_select_character
                        beq     aborted_message
                        subq.b  #1,d0
                        move.b  d0,TEMP_NUMBER_W_LO(a3)
                        
                        cmp.b   PARTY_CHARACTERS(a3),d0
                        bcc     aborted_message
                        cmp.b   TEMP_NUMBER_W_HI(a3),d0
                        beq     aborted_message
                        move.b  TEMP_NUMBER_W_HI(a3),CURRENT_CHARACTER(a3)
                        jsr     get_character_data
                        move.l  a5,a4
                        move.b  TEMP_NUMBER_W_LO(a3),CURRENT_CHARACTER(a3)
                        jsr     get_character_data
                        clr.w   d2
                        move.b  TEMP_NUMBER_W_HI(a3),d2
                        clr.w   d1
                        move.b  TEMP_NUMBER_W_LO(a3),d1
                        lea     $50(a3),a0
                        move.b  0(a0,d2.w),d0
                        move.b  0(a0,d1.w),0(a0,d2.w)
                        move.b  d0,0(a0,d1.w)
                        move.w  #16-1,d2
lbC019D4A:
                        move.l  (a5),d0
                        move.l  (a4),(a5)+
                        move.l  d0,(a4)+
                        dbra    d2,lbC019D4A
                        move.b  #1,TEXT_Y(a3)
                        bsr     lbC019DB2
                        move.b  #5,TEXT_Y(a3)
                        bsr     lbC019DB2
                        move.b  #9,TEXT_Y(a3)
                        bsr     lbC019DB2
                        move.b  #13,TEXT_Y(a3)
                        bsr     lbC019DB2
                        jsr     lbC01BB68
                        move.b  #$18,TEXT_Y(a3)
                        move.b  #$18,TEXT_X(a3)
                        lea     EXCHANGED_MSG,a4
                        jsr     display_message
                        jmp     lbC01B400

aborted_message:        lea     ABORTED_MSG,a4
                        jsr     display_message
                        jmp     lbC01B400

lbC019DB2:              move.b  #$18,TEXT_X(a3)
                        lea     ascii_MSG2,a4
                        jmp     display_message

; ----------------------------------------
; (N)EGATE TIME
command_NegateTime:     lea     NegatetimeWHO_MSG,a4
                        jsr     display_message
                        jsr     input_select_character
                        bne     lbC019DE2
                        jmp     lbC01B400

lbC019DE2:              jsr     get_character_data
                        move.b  CHARACTER_POWDERS(a5),d0
                        bne.b   decrease_number_powders
                        jmp     none_left_message

decrease_number_powders:
                        move.b  #1,d3
                        move.w  #0,ccr
                        sbcd    d3,d0
                        move.b  d0,CHARACTER_POWDERS(a5)
                        move.b  #10,NEGATE_TIME_COUNTER(a3)
                        jmp     lbC01B400

; ----------------------------------------
; (O)THER COMMAND
command_OtherCommand:   move.b  #$FF,$76(a3)
                        lea     OthercommandW_MSG,a4
                        jsr     display_message
lbC019E22:
                        jsr     input_select_character
                        bne     lbC019E32
                        jmp     lbC01B400

lbC019E32:              jsr     get_character_incapacitated_state
                        beq     lbC019E42
                        jmp     incapacitated_message

lbC019E42:              lea     CMD_MSG,a4
                        jsr     display_message
                        move.b  #8,OTHER_COMMAND_LENGTH(a3)

lbC019E54:              jsr     input_wait_key
                        cmp.b   #$8D,d0
                        beq     lbC019EB4
                        cmp.b   #$88,d0
                        bne     lbC019E8C
                        cmp.b   #8,OTHER_COMMAND_LENGTH(a3)
                        bcc     lbC019E8C
                        sub.b   #1,TEXT_X(a3)
                        addq.b  #1,OTHER_COMMAND_LENGTH(a3)
                        move.b  #$20,d0
                        jsr     draw_letter_both_bitmap
                        bra.s   lbC019E54

lbC019E8C:              cmp.b   #$C0,d0
                        bcs.s   lbC019E54
                        cmp.b   #$DB,d0
                        bcc.s   lbC019E54
                        move.b  d0,$1F(a3)
                        and.b   #$7F,d0
                        jsr     draw_letter_both_bitmap
                        addq.b  #1,TEXT_X(a3)
                        subq.b  #1,OTHER_COMMAND_LENGTH(a3)
                        bne.s   lbC019E54
lbC019EB4:
                        jsr     text_carriage_return
                        move.b  $1F(a3),d0
                        move.b  OTHER_COMMAND_LENGTH(a3),d1
                        cmp.b   #$D4,d0
                        bne     lbC019ED8
                        cmp.b   #2,d1
                        bne     lbC019ED8
                        jmp     lbC019F94

lbC019ED8:              cmp.b   #$C7,d0
                        bne     lbC019EEE
                        cmp.b   #5,d1
                        bne     lbC019EEE
                        jmp     lbC01A18A

lbC019EEE:              cmp.b   #$C8,d0
                        bne     lbC019F04
                        cmp.b   #2,d1
                        bne     lbC019F04
                        jmp     lbC01A142

lbC019F04:              cmp.b   #$C5,d0
                        bne     lbC019F56
                        cmp.b   #3,d1
                        bne     lbC019F1A
                        jmp     lbC01A202

lbC019F1A:              cmp.b   #4,d1
                        bne     lbC019F56
                        jsr     text_carriage_return
                        move.l  #2,-(sp)
                        jsr     _AvailMem
                        addq.l  #4,sp
                        move.l  d0,d6
                        move.w  #3,d7
lbC019F3C:
                        move.b  d6,d0
                        jsr     draw_2_digits
                        lsr.l   #8,d6
                        dbra    d7,lbC019F3C
                        jsr     text_carriage_return
                        jmp     lbC01B400

lbC019F56:              cmp.b   #$D9,d0
                        bne     lbC019F6C
                        cmp.b   #4,d1
                        bne     lbC019F6C
                        jmp     lbC01A2A8

lbC019F6C:              cmp.b   #$C5,d0
                        bne     lbC019F82
                        cmp.b   #1,d1
                        bne     lbC019F82
                        jmp     lbC01AD72

lbC019F82:              lea     NOEFFECT_MSG,a4
                        jsr     display_message
                        jmp     lbC01B400

lbC019F94:              lea     DIR_MSG,a4
                        jsr     display_message
                        jsr     input_select_direction
                        jsr     tile_get_action_datas
                        cmp.b   #$7C,d0
                        beq     lbC019FBA
                        jmp     not_here_message

lbC019FBA:              lea     DSLM_MSG,a4
                        jsr     display_message
                        jsr     input_get_letter
                        move.b  #$1E,d1
                        cmp.b   #$CC,d0
                        beq     lbC01A002
                        addq.b  #1,d1
                        cmp.b   #$D3,d0
                        beq     lbC01A002
                        addq.b  #1,d1
                        cmp.b   #$CD,d0
                        beq     lbC01A002
                        addq.b  #1,d1
                        cmp.b   #$C4,d0
                        beq     lbC01A002
                        jmp     what_message

lbC01A002:              move.b  d1,$6A(a3)
                        move.b  d1,d0
                        sub.b   #$1E,d0
                        move.b  d0,d2
                        jsr     lbC01E922
                        move.b  d0,$69(a3)
                        jsr     get_character_data
                        move.b  14(a5),d0
                        and.b   $69(a3),d0
                        bne     lbC01A030
                        jmp     none_left_message

lbC01A030:              move.b  $6A(a3),d1
                        cmp.b   PARTY_ACTION_X(a3),d1
                        beq     lbC01A06C
lbC01A03C:              jsr     lbC021124
                        move.w  #4,d0
                        jsr     play_sound
                        jsr     lbC021124
                        jsr     get_character_data
                        clr.b   CHARACTER_HITPOINTS_HI(a5)
                        move.b  #$FF,d0
                        jsr     lbC01B8C2
                        jmp     lbC01B400

lbC01A06C:              cmp.b   $19(a3),d1
                        bne.s   lbC01A03C
                        addq.b  #1,$19(a3)
                        move.b  #4,INPUT_NUMBER(a3)
lbC01A07E:
                        jsr     tile_get_action_datas
                        move.b  #$F0,(a5)
                        jsr     refresh_map_view
                        move.w  #4,d0
                        jsr     play_sound
                        jsr     tile_get_action_datas
                        move.b  #$7C,(a5)
                        jsr     refresh_map_view
                        move.w  #4,d0
                        jsr     play_sound
                        subq.b  #1,INPUT_NUMBER(a3)
                        bpl.s   lbC01A07E
                        move.b  #$20,(a5)
                        jsr     refresh_map_view
                        cmp.b   #$22,$19(a3)
                        beq     lbC01A0D4
                        jmp     lbC01B400

lbC01A0D4:              jsr     reset_music
                        jsr     lbC021F0E
                        move.l  #'ENDG',d0
                        jsr     file_load_encrypted
                        
                        jsr     generate_jumps_table
                        jsr     lbB0237D0
                        jsr     lbC021A78
                        move.b  #10,d0
                        jsr     start_new_music
                        move.w  #$1388,d0
                        jsr     temporize
lbC01A136:              move.w  #5,d0
                        jsr     display_requester
                        bra.s   lbC01A136

lbC01A142:              cmp.b   #$7C,6(a3)
                        beq     lbC01A152
                        jmp     not_here_message

lbC01A152:              move.b  PARTY_X(a3),d0
                        and.b   #3,d0
                        move.b  d0,d2
                        jsr     lbC01E922
                        move.b  d0,$69(a3)
                        jsr     get_character_data
                        move.b  14(a5),d0
                        or.b    $69(a3),d0
                        move.b  d0,14(a5)
                        lea     ACARDWITHSTRA_MSG,a4
                        jsr     display_message
                        jmp     lbC01B400

lbC01A18A:              tst.b   $4C(a3)
                        beq     lbC01A198
                        jmp     not_here_message

lbC01A198:              cmp.b   #$21,PARTY_X(a3)
                        bne     lbC01A1C4
                        cmp.b   #3,PARTY_Y(a3)
                        beq     lbC01A1B2
                        jmp     not_here_message

lbC01A1B2:              jsr     get_character_data
                        move.b  #1,$3F(a5)
                        jmp     lbC01A1F0

lbC01A1C4:              cmp.b   #$13,PARTY_X(a3)
                        beq     lbC01A1D4
                        jmp     not_here_message

lbC01A1D4:              cmp.b   #$2C,PARTY_Y(a3)
                        beq     lbC01A1E4
                        jmp     not_here_message

lbC01A1E4:              jsr     get_character_data
                        move.b  #1,$2F(a5)
lbC01A1F0:
                        lea     EXOTICS_MSG,a4
                        jsr     display_message
                        jmp     lbC01B400

lbC01A202:
                        lea     DIR_MSG,a4
                        jsr     display_message
                        jsr     input_select_direction
                        jsr     lbC01C8F2
                        bpl     lbC01A224
                        jmp     not_here_message

lbC01A224:              move.b  d0,$5F(a3)
                        jsr     get_character_data
                        move.b  CHARACTER_MONEY_HI(a5),d0
                        bne     lbC01A248
                        lea     NOTENOUGHGOLD_MSG,a4
                        jsr     display_message
                        jmp     command_aborted

lbC01A248:              move.b  #1,d3
                        move.w  #0,ccr
                        sbcd    d3,d0
                        move.b  d0,CHARACTER_MONEY_HI(a5)
                        clr.w   d1
                        move.b  $5F(a3),d1
                        lea     lbL0225D0,a0
                        cmp.b   #$48,0(a0,d1.w)
                        beq     lbC01A270
                        jmp     lbC019F82(pc)

lbC01A270:              lea     lbL022610,a0
                        move.b  0(a0,d1.w),PARTY_ACTION_X(a3)
                        lea     lbL022630,a0
                        move.b  0(a0,d1.w),PARTY_ACTION_Y(a3)
                        jsr     tile_get_action_datas
                        lea     lbL0225F0,a0
                        move.b  0(a0,d1.w),(a5)
                        lea     lbL0225D0,a0
                        clr.b   0(a0,d1.w)
                        jmp     lbC01B400

lbC01A2A8:              cmp.b   #2,$4C(a3)
                        bne     lbC01A2EC
                        move.b  PARTY_X_SAVE(a3),d0
                        lea     lbB026992,a0
                        cmp.b   4(a0),d0
                        bne     lbC01A2EC
                        move.b  #$30,d0
                        cmp.b   PARTY_X(a3),d0
                        bne     lbC01A2EC
                        cmp.b   PARTY_Y(a3),d0
                        bne     lbC01A2EC
                        lea     YELLEVOCARE_MSG,a4
                        jsr     display_message
                        jmp     lbC01B400

lbC01A2EC:              jmp     lbC019F82(pc)

; ----------------------------------------
; (P)EER AT GEM
command_PeerAtGem:      lea     PeeratgemWHOS_MSG,a4
                        jsr     display_message
                        jsr     input_select_character
                        bne     lbC01A30C
                        jmp     lbC01B400

lbC01A30C:              jsr     get_character_data
                        move.b  CHARACTER_GEMS(a5),d0
                        bne     lbC01A320
                        jmp     none_left_message

lbC01A320:              move.b  #1,d3
                        move.w  #0,ccr
                        sbcd    d3,d0
                        move.b  d0,CHARACTER_GEMS(a5)
lbC01A32E:              clr.b   PARTY_ACTION_X(a3)
                        clr.b   PARTY_ACTION_Y(a3)
                        move.b  lbB028FDC,$58(a3)
                        jsr     reset_music
                        move.b  #10,current_music_number
                        move.b  $58(a3),lbB028FDC
                        jsr     lbC0276B4
                        jmp     lbC01B400

; ----------------------------------------
; (Q)UIT AND SAVE
command_QuitAndSave:    lea     Quitsave_MSG,a4
                        jsr     display_message
                        tst.b   $4C(a3)
                        beq     lbC01A390
                        lea     ONLYONSURFACE_MSG,a4
                        jsr     display_message
                        move.w  #0,d0
                        jsr     play_sound
                        jmp     lbC01B400

lbC01A390:              bsr     lbC01A39E
                        bsr     lbC01A3F2
                        jmp     lbC01B400

lbC01A39E:              move.b  11(a3),$4A(a3)
                        move.b  PARTY_X(a3),PARTY_X_SAVE(a3)
                        move.b  PARTY_Y(a3),PARTY_Y_SAVE(a3)
                        move.b  PARTY_MOVES(a3),d0
                        jsr     draw_2_digits
                        move.b  PARTY_MOVES+1(a3),d0
                        jsr     draw_2_digits
                        move.b  PARTY_MOVES+2(a3),d0
                        jsr     draw_2_digits
                        move.b  PARTY_MOVES+3(a3),d0
                        jsr     draw_2_digits
                        lea     MOVES_MSG,a4
                        jsr     display_message
                        rts

lbC01A3E6:              lea     PLEASEWAIT_MSG,a4
                        jsr     display_message
lbC01A3F2:
                        move.l  #'SOSA',d0
                        lea     current_map,a0
                        jsr     lbC0180E6                   ; DIFF
                        move.l  #'SOSM',d0
                        lea     lbL0225D0,a0
                        jsr     lbC0180E6

lbC01A416:              move.l  #'PLRS',d0
                        lea     characters_data,a0
                        jsr     lbC0180E6
                        move.l  #'PRTY',d0
                        lea     $4A(a3),a0
                        jsr     lbC0180E6
                        rts

; ----------------------------------------
; (R)EADY (object)
command_ReadyObject:    lea     Readyfor_MSG,a4
                        jsr     display_message
                        move.b  #1,$79(a3)
                        jsr     lbC021E16
                        jsr     lbC01B6F4
                        bne     lbC01A4C4

; ----------------------------------------
display_invalid_character_msg:
                        clr.b   $79(a3)
                        jsr     lbC021E16
                        lea     NOSUCHPLAYER_MSG,a4
                        jsr     display_message
                        jmp     command_aborted

lbC01A4C4:              cmp.b   #5,d0
                        bcc.s   display_invalid_character_msg
                        move.b  d0,CURRENT_CHARACTER(a3)
                        subq.b  #1,CURRENT_CHARACTER(a3)
                        clr.b   $79(a3)
                        jsr     lbC021E16
lbC01A4DE:
                        lea     WEAPON_MSG,a4
                        jsr     display_message
                        jsr     input_get_letter
                        cmp.b   #$C1,d0
                        bcs.s   lbC01A4DE
                        cmp.b   #$D1,d0
                        bcc.s   lbC01A4DE
                        bsr     lbC01A578
                        bpl     lbC01A516
lbC01A504:
                        lea     NOTALLOWED_MSG,a4
                        jsr     display_message
                        jmp     command_aborted

lbC01A516:              sub.b   #$C1,d0
                        move.b  d0,SPELL_OR_OBJECT_NUMBER(a3)
                        beq     lbC01A548
                        jsr     get_character_data
                        clr.w   d2
                        move.b  SPELL_OR_OBJECT_NUMBER(a3),d2
                        tst.b   $30(a5,d2.w)
                        bne     lbC01A548
lbC01A536:
                        lea     NOTOWNED_MSG,a4
                        jsr     display_message
                        jmp     command_aborted

lbC01A548:              move.b  SPELL_OR_OBJECT_NUMBER(a3),d0
                        add.b   #65,d0
                        jsr     get_description_message
                        lea     READY_MSG0,a4
                        jsr     display_message
                        jsr     get_character_data
                        move.b  #$30,d2
                        move.b  SPELL_OR_OBJECT_NUMBER(a3),$30(a5)
                        jmp     lbC01B400

lbC01A578:              move.b  d0,$69(a3)
                        jsr     get_character_data
                        move.b  $17(a5),d0
                        lea     lbB026930,a0
                        lea     lbB02693B,a1
                        clr.w   d1
lbC01A594:
                        cmp.b   0(a0,d1.w),d0
                        beq     lbC01A5A6
                        addq.w  #1,d1
                        cmp.w   #11,d1
                        bcs.s   lbC01A594
lbC01A5A6:
                        move.b  $69(a3),d0
                        cmp.b   #$D0,d0
                        beq     lbC01A5C0
                        cmp.b   0(a1,d1.w),d0
                        bcs     lbC01A5C0
                        move.w  #$FFFF,d1
                        rts

lbC01A5C0:              clr.w   d1
                        rts

                        SECTION EXODUS01A5C4,DATA

ascii_MSG2:             dc.b    "              ",0
ModifyorderPL_MSG:      dc.b    "Modify order!",$ff
                        dc.b    "PLR:-",0
PLR_MSG:                dc.b    "PLR:-",0
EXCHANGED_MSG:          dc.b    "EXCHANGED!",$ff,0
ABORTED_MSG:            dc.b    "ABORTED!",$ff,0
NegatetimeWHO_MSG:      dc.b    "Negate time!",$ff
                        dc.b    "WHOSE POWD?-",0
OthercommandW_MSG:      dc.b    "Other command!",$ff
                        dc.b    "WHOSE ACTION-",0
CMD_MSG:                dc.b    "CMD:-",0
NOEFFECT_MSG:           dc.b    "NO EFFECT!",$ff,0
DIR_MSG:                dc.b    "DIR-",0
DSLM_MSG:               dc.b    "D, S, L, M-",0
ACARDWITHSTRA_MSG:      dc.b    "A CARD, WITH",$ff
                        dc.b    "STRANGE MARKS!",$ff,0
EXOTICS_MSG:            dc.b    "EXOTICS!",$ff,0
NOTENOUGHGOLD_MSG:      dc.b    "NOT ENOUGH GOLD!",$ff,0
YELLEVOCARE_MSG:        dc.b    $ff
                        dc.b    "YELL 'EVOCARE'",$ff,$ff,0
PeeratgemWHOS_MSG:      dc.b    "Peer at gem!",$ff
                        dc.B    "WHOSE GEM-",0
Quitsave_MSG:           dc.b    "Quit & save...",$ff,0
ONLYONSURFACE_MSG:      dc.b    "ONLY ON SURFACE!",$ff,0
MOVES_MSG:              dc.b    " MOVES",$ff,0
PLEASEWAIT_MSG:         dc.b    "PLEASE WAIT...",$ff,0
Readyfor_MSG:           dc.b    "Ready for # - ",0
NOSUCHPLAYER_MSG:       dc.b    "NO SUCH PLAYER!",$ff,0
WEAPON_MSG:             dc.b    "WEAPON-",0
NOTALLOWED_MSG:         dc.b    "NOT ALLOWED!",$ff,0
NOTOWNED_MSG:           dc.b    "NOT OWNED!",$ff,0
READY_MSG0:             dc.b    " READY",$ff,0

                        SECTION EXODUS01A744,CODE

; ----------------------------------------
; (S)TEAL
command_Steal:          lea     Plrtosteal_MSG,a4
                        jsr     display_message
                        jsr     input_select_character
                        bne     lbC01A760
                        jmp     lbC01B400

lbC01A760:              jsr     get_character_incapacitated_state
                        beq     lbC01A770
                        jmp     incapacitated_message

lbC01A770:              lea     DIRECT_MSG0,a4
                        jsr     display_message
                        jsr     input_select_direction
                        jsr     lbC01BF78
                        beq     lbC01A7EC
lbC01A78C:              jsr     lbC02167A
                        and.b   #3,d0
                        beq     lbC01A7AC
                        lea     FAILED_MSG0,a4
                        jsr     display_message
                        jmp     lbC01B400

lbC01A7AC:              move.w  #64-1,d1
lbC01A7B0:              lea     lbL0225D0,a0
                        cmp.b   #$48,0(a0,d1.w)
                        bne     lbC01A7CC
                        lea     lbL022650,a0
                        move.b  #$C0,0(a0,d1.w)
lbC01A7CC:              dbra    d1,lbC01A7B0
                        lea     WATCHOUT_MSG,a4
                        jsr     display_message
                        move.w  #6,d0
                        jsr     play_sound
                        jmp     lbC01B400

lbC01A7EC:              jsr     tile_get_action_datas
                        cmp.b   #$94,d0
                        bcs.s   lbC01A78C
                        cmp.b   #$E8,d0
                        bcc.s   lbC01A78C
                        move.b  PARTY_DIRECTION_X(a3),d0
                        add.b   d0,d0
                        add.b   d0,PARTY_ACTION_X(a3)
                        move.b  PARTY_DIRECTION_Y(a3),d0
                        add.b   d0,d0
                        add.b   d0,PARTY_ACTION_Y(a3)
                        jsr     tile_get_action_datas
                        cmp.b   #$24,d0
                        bne     lbC01A78C
                        move.b  #$20,(a5)
                        jmp     chest_gold

; ----------------------------------------
; (T)RANSACT
command_Transact:       lea     Transact_MSG,a4
                        jsr     display_message
                        tst.b   $77(a3)
                        beq     lbC01A85A
                        lea     ascii_MSG3,a4
                        jsr     display_message
                        move.b  $77(a3),d0
                        jsr     set_direction
                        bra     lbC01A872

lbC01A85A:              jsr     text_carriage_return
                        lea     DIRECT_MSG0,a4
                        jsr     display_message
                        jsr     input_select_direction

lbC01A872:              jsr     lbC01C8F2
                        bpl     lbC01A9A8
                        jsr     tile_get_action_datas
                        cmp.b   #$94,d0
                        bcc     lbC01A890
                        jmp     not_here_message

lbC01A890:              cmp.b   #$E8,d0
                        bcs     lbC01A89E
                        jmp     not_here_message

lbC01A89E:              move.b  PARTY_DIRECTION_X(a3),d0
                        add.b   d0,PARTY_ACTION_X(a3)
                        move.b  PARTY_DIRECTION_Y(a3),d0
                        add.b   d0,PARTY_ACTION_Y(a3)
                        jsr     tile_get_action_datas
                        cmp.b   #$40,d0
                        beq     lbC01A8C2
                        jmp     not_here_message

lbC01A8C2:              cmp.b   #$49,lbB019CB2
                        bne     lbC01A8F4
                        cmp.b   #44,PARTY_ACTION_X(a3)
                        bne     lbC01A8F4
                        cmp.b   #20,PARTY_ACTION_Y(a3)
                        bne     lbC01A8F4
                        lea     Pleasegotothe_MSG,a4
                        jsr     display_message
                        jmp     lbC01B400

lbC01A8F4:              lea     WhowillTransa_MSG,a4
                        jsr     display_message
                        jsr     input_select_character
                        bne     lbC01A910
                        jmp     lbC01B400

lbC01A910:              jsr     get_character_incapacitated_state
                        beq     lbC01A920
                        jmp     incapacitated_message

lbC01A920:              move.b  CURRENT_CHARACTER(a3),CURRENT_CHARACTER_SAVE(a3)
                        jsr     lbC0210C6
                        jsr     reset_music
                        ; the only explanation for this is that shops
                        ; are placed at specific y positions on the map
                        ; to reflect their ordinal file numbers
                        move.b  PARTY_Y(a3),d1          ; not sure
                        and.b   #7,d1                   ; Shop number from 0 to 7
                        add.b   #'0',d1
                        move.l  #'SHP0',d0
                        move.b  d1,d0                   ; shop number
                        jsr     file_load_encrypted

                        jsr     generate_jumps_table
                        move.b  #6,d0
                        jsr     start_new_music
                        jsr     lbB0237D0               ; Run the shop's code
lbC01A98E:              move.b  $4C(a3),lbB028FDC
                        move.b  CURRENT_CHARACTER_SAVE(a3),CURRENT_CHARACTER(a3)
                        jsr     lbC0210C6
                        jmp     lbC01B400

lbC01A9A8:              lea     lbL0225D0,a0
                        cmp.b   #$4C,0(a0,d1.w)
                        bne     lbC01A9BE
                        jmp     transact_lord_brit

lbC01A9BE:              lea     lbL022650,a0
                        move.b  0(a0,d1.w),d0
                        and.b   #15,d0
                        beq     lbC01A9DC
                        jsr     lbC01DF3C
                        jmp     lbC01B400

lbC01A9DC:              lea     EATDEATHSCUM_MSG,a4
                        tst.b   $4C(a3)
                        beq     lbC01A9F4
                        bmi     lbC01A9F4
                        lea     GOODDAY_MSG,a4

lbC01A9F4:              jsr     display_message
                        jmp     lbC01B400

transact_lord_brit:     lea     WhowillTransa_MSG,a4
                        jsr     display_message
                        jsr     input_select_character
                        bne     lbC01AA1C
                        jmp     lbC01B400

lbC01AA1C:              jsr     get_character_incapacitated_state
                        beq     lord_brit_messages
                        jmp     incapacitated_message

lord_brit_messages:     move.b  #8,current_music_number
                        lea     WELCOMEMYCHIL_MSG,a4
                        jsr     display_message
                        jsr     get_character_data
                        move.b  CHARACTER_MAXPOINTS_HI(a5),d0
                        cmp.b   CHARACTER_EXPERIENCE_HI(a5),d0
                        bls     lord_brit_no_more_exp
                        jmp     lord_brit_need_more_exp

lord_brit_no_more_exp:  cmp.b   #37,d0
                        bcs     lbC01AA74
                        lea     NOMORE_MSG,a4
                        jsr     display_message
                        jmp     lbC01B400

lbC01AA74:              cmp.b   #5,d0
                        bcs     lbC01AA9A
                        move.b  14(a5),d0
                        and.b   #$80,d0
                        bne     lbC01AA9A
                        lea     SEEKYETHEMARK_MSG,a4
                        jsr     display_message
                        jmp     lbC01B400

lbC01AA9A:              move.b  CHARACTER_MAXPOINTS_HI(a5),d0
                        move.b  #1,d3
                        move.w  #0,ccr
                        abcd    d3,d0
                        cmp.b   #37,d0
                        bcs     lbC01AAB4
                        move.b  #37,d0

lbC01AAB4:              move.b  d0,CHARACTER_MAXPOINTS_HI(a5)
                        lea     THOUARTGREATE_MSG,a4
                        jsr     display_message
                        jsr     lbC018DEC
                        move.w  #7,d0
                        jsr     play_sound
                        jsr     lbC018DEC
                        jmp     lbC01B400

lord_brit_need_more_exp:
                        lea     EXPERIENCEMOR_MSG,a4
                        jsr     display_message
                        jmp     lbC01B400

; ----------------------------------------
; (U)NLOCK
command_Unlock:         cmp.b   #1,$4C(a3)
                        bne     lbC01AB02
error_unlock:           jmp     not_here_message

lbC01AB02:              lea     Unlock_MSG,a4
                        jsr     display_message
                        jsr     input_select_direction
                        tst.b   PARTY_DIRECTION_Y(a3)
                        bne.s   error_unlock
                        jsr     tile_get_action_datas
                        cmp.b   #$B8,d0
                        bne.s   error_unlock
                        lea     WHOSEKEY_MSG,a4
                        jsr     display_message
                        jsr     input_select_character
                        bne     lbC01AB42
                        jmp     lbC01B400

lbC01AB42:              jsr     get_character_data
                        move.b  CHARACTER_KEYS(a5),d0
                        bne     lbC01AB56
                        jmp     none_left_message

lbC01AB56:              move.b  #1,d3
                        move.w  #0,ccr
                        sbcd    d3,d0
                        move.b  d0,CHARACTER_KEYS(a5)
                        jsr     tile_get_action_datas
                        move.b  6(a3),d0
                        add.b   d0,d0
                        move.b  d0,(a5)
                        move.w  #21,d0
                        jsr     play_sound
                        jsr     refresh_map_view
                        jmp     lbC01B400

; ----------------------------------------
; (V)OLUME SET
command_VolumeSet:      addq.b  #1,CURRENT_VOLUME(a3)
                        and.b   #3,CURRENT_VOLUME(a3)
                        moveq   #0,d0
                        move.b  CURRENT_VOLUME(a3),d0
                        add.w   d0,d0                           ; * 2
                        lea     volume_levels_table,a0
                        move.w  (a0,d0.w),sound_volume
                        move.w  8(a0,d0.w),music_volume
                        move.w  (a0,d0.w),$DFF0D8
                        lea     volume_levels_messages_table,a0
                        add.w   d0,d0                           ; * 4
                        move.l  0(a0,d0.w),a4
                        jsr     display_message
                        lea     Volume_MSG,a4
                        jsr     display_message
                        jmp     lbC01B400

; ----------------------------------------
; (W)EAR (object)
command_WearObject:     lea     Wearfor_MSG,a4
                        jsr     display_message
                        move.b  #1,$79(a3)
                        jsr     lbC021E16
                        jsr     lbC01B6F4
                        bne     lbC01AC06
lbC01AC00:              jmp     display_invalid_character_msg

lbC01AC06:              cmp.b   #5,d0
                        bcc.s   lbC01AC00
                        move.b  d0,CURRENT_CHARACTER(a3)
                        subq.b  #1,CURRENT_CHARACTER(a3)
                        clr.b   $79(a3)
                        jsr     lbC021E16
lbC01AC20:              lea     ARMOUR_MSG,a4
                        jsr     display_message
                        jsr     input_get_letter
                        cmp.b   #$C1,d0
                        bcs.s   lbC01AC20
                        cmp.b   #$C9,d0
                        bcc.s   lbC01AC20
                        bsr     lbC01AC9E
                        bpl     lbC01AC4C
                        jmp     lbC01A504

lbC01AC4C:              sub.b   #$C1,d0
                        move.b  d0,SPELL_OR_OBJECT_NUMBER(a3)
                        beq     lbC01AC72
                        jsr     get_character_data
                        clr.w   d2
                        move.b  SPELL_OR_OBJECT_NUMBER(a3),d2
                        tst.b   $28(a5,d2.w)
                        bne     lbC01AC72
                        jmp     lbC01A536

lbC01AC72:              move.b  SPELL_OR_OBJECT_NUMBER(a3),d0
                        add.b   #$51,d0
                        jsr     get_description_message
                        lea     READY_MSG,a4
                        jsr     display_message
                        jsr     get_character_data
                        move.b  SPELL_OR_OBJECT_NUMBER(a3),$28(a5)
                        jmp     lbC01B400

lbC01AC9E:              move.b  d0,$69(a3)
                        jsr     get_character_data
                        move.b  $17(a5),d0
                        lea     lbB026930,a0
                        lea     lbB026946,a1
                        clr.w   d1
lbC01ACBA:
                        cmp.b   0(a0,d1.w),d0
                        beq     lbC01ACCC
                        addq.w  #1,d1
                        cmp.w   #11,d1
                        bcs.s   lbC01ACBA
lbC01ACCC:
                        move.b  $69(a3),d0
                        cmp.b   #$C8,d0
                        beq     lbC01ACE6
                        cmp.b   0(a1,d1.w),d0
                        bcs     lbC01ACE6
                        move.w  #-1,d1
                        rts

lbC01ACE6:              clr.w   d1
                        rts

; ----------------------------------------
; (X)IT (drop object)
command_Xit_drop:       cmp.b   #$7E,11(a3)
                        bne     lbC01AD06
                        lea     Xit_MSG,a4
                        jsr     display_message
                        jmp     what_message

lbC01AD06:              lea     DismountHorse_MSG,a4
                        cmp.b   #$14,11(a3)
                        beq     lbC01AD28
                        lea     Xit_MSG,a4
                        jsr     display_message
                        lea     craft_MSG,a4

lbC01AD28:              jsr     display_message
                        jsr     set_player_to_action
                        cmp.b   #5,d0
                        bcs     lbC01AD42
                        jmp     not_here_message

lbC01AD42:              move.b  11(a3),d0
                        add.b   d0,d0
                        move.b  d0,(a5)
                        move.b  #$7E,11(a3)
                        jmp     lbC01B400

; ----------------------------------------
; (Y)ELL
command_Yell:           clr.b   $76(a3)
                        lea     Yellwhom_MSG,a4
                        jsr     display_message
                        jmp     lbC019E22

lbC01AD6C:              jmp     lbC019F82

lbC01AD72:              tst.b   $76(a3)
                        bne.s   lbC01AD6C
                        jsr     get_character_data
                        move.b  14(a5),d0
                        and.b   #$40,d0
                        beq.s   lbC01AD6C
                        tst.b   $4C(a3)
                        bne.s   lbC01AD6C
                        cmp.b   #10,PARTY_X(a3)
                        bne.s   lbC01AD6C
                        cmp.b   #$3B,PARTY_Y(a3)
                        beq     lbC01ADAE
                        cmp.b   #$38,PARTY_Y(a3)
                        beq     lbC01ADBA
                        jmp     lbC01AD6C(pc)

lbC01ADAE:              move.b  #$38,PARTY_Y(a3)
                        jmp     lbC01ADC0

lbC01ADBA:              move.b  #$3B,PARTY_Y(a3)

lbC01ADC0:              jsr     lbC020FF2
                        move.w  #8,d0
                        jsr     play_sound
                        jsr     refresh_map_view
                        jmp     lbC01B400

lbC01ADDC:              and.b   #$7F,d0
                        move.b  d0,CURRENT_CHARACTER(a3)
                        bra     lbC01AE04

; ----------------------------------------
; (Z)TATS
command_ZStats:         lea     Ztatsfor_MSG,a4
                        jsr     display_message
                        jsr     input_select_character
                        bne     lbC01AE04
                        jmp     lbC01B400

lbC01AE04:              jsr     get_character_data
                        tst.b   0(a5)
                        bne     lbC01AE18
                        jmp     lbC01B400

lbC01AE18:              jsr     blit_front_to_back_buffer
                        jsr     lbC01AED0
                        jsr     lbC021F0E

lbC01AE2A:              nop

lbC01AE2C:              jsr     lbC01F972
                        cmp.w   #2,d0
                        bne.s   lbC01AE2C
                        move.w  lbW01FD92,d0
                        beq     lbC01AEA8
                        cmp.w   #$14,d0
                        bcc     lbC01AE78
                        sub.w   #10,d0
                        add.b   #$C1,d0
                        bsr     lbC01AC9E
                        bpl     lbC01AE66
                        move.w  #1,d0
                        jsr     play_sound
                        bra.s   lbC01AE2C

lbC01AE66:              sub.b   #$C1,d0
                        move.b  d0,$28(a5)
                        lea     lbC01AF92,a4
                        bra     lbC01AEA4

lbC01AE78:              sub.w   #$14,d0
                        add.b   #$C1,d0
                        jsr     lbC01A578
                        bpl     lbC01AE96
                        move.w  #1,d0
                        jsr     play_sound
                        bra.s   lbC01AE2C

lbC01AE96:              sub.b   #$C1,d0
                        move.b  d0,$30(a5)
                        lea     lbC01AFD6,a4
lbC01AEA4:              jsr     (a4)
                        bra.s   lbC01AE2A

lbC01AEA8:              jsr     blit_back_to_front_buffer
                        jsr     lbC021E16
                        move.b  #$18,TEXT_X(a3)
                        move.b  #$18,TEXT_Y(a3)
                        move.l  #$FF0FFFF,letter_mask
                        jmp     lbC01B400

lbC01AED0:              move.w  #1,lbW01FD90
                        jsr     lbC01FB22
                        move.l  #$FF0FFFF,letter_mask
                        lea     lbW020CFE,a4
                        jsr     lbC01F764
                        lea     lbW01B190,a4
                        bsr     lbC01AF6A
                        move.b  #12,TEXT_X(a3)
                        move.b  #13,TEXT_Y(a3)
                        move.b  #1,d3
                        move.b  CHARACTER_EXPERIENCE_HI(a5),d0
                        move.w  #0,ccr
                        abcd    d3,d0
                        bsr     lbC01B0F6
                        move.b  #2,TEXT_X(a3)
                        move.b  #$11,TEXT_Y(a3)
                        move.b  $11(a5),d1
                        clr.w   d0
                        lea     lbL01B1CE,a0
                        lea     Good_MSG,a4
                        jsr     lbC01F7F8
                        lea     lbW01B1D2,a4
                        bsr     lbC01B122
                        lea     lbW01B1E4,a4
                        bsr     lbC01B122
                        bsr     lbC01AFD6
                        bsr     lbC01AF92
                        lea     lbW020C9A,a4
                        jsr     lbC01B160
                        rts

lbC01AF6A:              move.w  (a4)+,d2
                        bmi     lbC01AF90
                        move.w  (a4)+,d7
                        move.b  (a4)+,TEXT_X(a3)
                        move.b  (a4)+,TEXT_Y(a3)
                        subq.w  #1,d7
lbC01AF7E:
                        move.b  0(a5,d2.w),d0
                        bsr     lbC01B0F6
                        addq.w  #1,d2
                        dbra    d7,lbC01AF7E
                        bra.s   lbC01AF6A

lbC01AF90:              rts

lbC01AF92:              move.l  #SKIN_MSG,lbL01B188
                        move.w  #8,lbW01B18C
                        move.b  #$10,lbB01B18F
                        move.b  #14,lbB01B18E
                        lea     lbW020172,a0
                        lea     lbL020B02,a1
                        lea     SKIN_MSG0,a4
                        move.b  #1,d0
                        move.w  #$28,d1
                        move.b  #$19,d6
                        bra     lbC01B016

lbC01AFD6:              move.l  #HAND_MSG,lbL01B188
                        move.w  #$10,lbW01B18C
                        move.b  #8,lbB01B18F
                        move.b  #$1C,lbB01B18E
                        lea     lbW0201D2,a0
                        lea     lbL020B22,a1
                        lea     HANDS_MSG,a4
                        move.b  #2,d0
                        move.w  #$30,d1
                        move.b  #$25,d6
lbC01B016:              movem.l a5,-(sp)
                        add.w   d1,a5
                        moveq   #0,d7
                        move.b  lbB01B18F,TEXT_Y(a3)
                        bra     lbC01B03C

lbC01B02A:              move.b  0(a5,d7.w),d0
                        bne     lbC01B03C
                        move.l  #$FFFFFFFF,(a1)+
                        bra     lbC01B0D4

lbC01B03C:              move.w  d7,d3
                        mulu    #12,d3
                        move.l  a0,a2
                        add.l   d3,a2
                        move.l  a2,(a1)+
                        move.b  lbB01B18E,TEXT_X(a3)
                        move.l  #$FF0FFFF,d1
                        cmp.b   (a5),d7
                        bne     lbC01B062
                        move.l  #$FF0000FF,d1

lbC01B062:              move.l  d1,letter_mask
                        movem.l a0/a1,-(sp)
                        move.w  d0,-(sp)
                        move.b  d7,d0
                        move.l  letter_mask,-(sp)
                        move.l  #$FFFF,letter_mask
                        add.b   #$41,d0
                        move.l  Bitmap_PtrA,a1
                        jsr     draw_letter
                        addq.b  #1,TEXT_X(a3)
                        move.b  #$29,d0
                        move.l  Bitmap_PtrA,a1
                        jsr     draw_letter
                        addq.b  #1,TEXT_X(a3)
                        move.l  (sp)+,letter_mask
                        tst.w   d7
                        beq     lbC01B0BE
                        move.l  lbL01B188,a4
lbC01B0BE:
                        move.w  d7,d0
                        jsr     lbC020E6E
                        move.b  d6,TEXT_X(a3)
                        move.w  (sp)+,d0
                        bsr     lbC01B0F6
                        movem.l (sp)+,a0/a1

lbC01B0D4:              addq.b  #1,TEXT_Y(a3)
                        addq.w  #1,d7
                        cmp.w   lbW01B18C,d7
                        bne     lbC01B02A
                        movem.l (sp)+,a5
                        move.l  #$FF0FFFF,letter_mask
                        rts

lbC01B0F6:              move.w  d0,-(sp)
                        and.b   #$F0,d0
                        lsr.b   #4,d0
                        jsr     lbC01B10A
                        move.w  (sp)+,d0
                        and.b   #15,d0

lbC01B10A:              move.l  Bitmap_PtrA,a1
                        add.b   #$30,d0
                        jsr     draw_letter
                        addq.b  #1,TEXT_X(a3)
                        rts

lbC01B122:              move.b  (a4)+,d6
                        move.b  (a4)+,TEXT_Y(a3)
                        move.w  #4-1,d7
lbC01B12C:              movem.w (a4)+,d0/d1
                        move.b  14(a5),d2
                        btst    d0,d2
                        beq     lbC01B15A
                        move.b  d6,TEXT_X(a3)
                        movem.l a4,-(sp)
                        move.w  d1,d0
                        lea     Death_MSG,a4
                        jsr     get_text_address_and_display
                        movem.l (sp)+,a4
                        addq.b  #1,TEXT_Y(a3)
lbC01B15A:              dbra    d7,lbC01B12C
                        rts

lbC01B160:              moveq   #15,d0
                        jsr     draw_setapen
                        move.w  (a4)+,d6
                        subq.w  #1,d6
lbC01B16E:              move.w  (a4)+,d0
                        move.w  (a4)+,d1
                        jsr     draw_move
                        move.w  (a4)+,d0
                        move.w  (a4)+,d1
                        jsr     draw_draw
                        dbra    d6,lbC01B16E
                        rts

                        SECTION EXODUS01B188,BSS
lbL01B188:              ds.l    1
lbW01B18C:              ds.w    1
lbB01B18E:              ds.b    1
lbB01B18F:              ds.b    1

                        SECTION EXODUS01B190,DATA
lbW01B190:              dc.w    $23,2,$1706,$20,2,$1707,$26,1,$1908,$25,1,$1909
                        dc.w    15,1,$190A,$27,1,$190B,$1A,2,$C0A,$1C,2,$C0B,$1E
                        dc.w    2,$C0C,$19,1,$C0E,$FFFF
lbL01B1CE:              dc.l    $C7C4C1D0
lbW01B1D2:              dc.w    $114,3,0,1,1,0,2,2,3
lbW01B1E4:              dc.w    $714,4,4,5,5,6,6,7,7
volume_levels_table:    dc.w    $3F,$1F,15,$1F
                        dc.w    2,1,0,1
volume_levels_messages_table:
                        dc.l    Full_MSG
                        dc.l    ascii_MSG4
                        dc.l    ascii_MSG5
                        dc.l    ascii_MSG4
Good_MSG:               dc.b    "  Good",0
                        dc.b    "  Dead",0
                        dc.b    " Ashes",0
                        dc.b    "Poisoned",0
Death_MSG:              dc.b    "Death",0
                        dc.b    "Sol",0
                        dc.b    "Love",0
                        dc.b    "Moons",0
                        dc.b    "Force",0
                        dc.b    "Fire",0
                        dc.b    "Snake",0
                        dc.b    "Kings",0
Plrtosteal_MSG:         dc.b    "Plr to steal-",0
Transact_MSG:           dc.b    "Transact!",0
ascii_MSG3:             dc.b    "-",0
DIRECT_MSG0:            dc.b    "DIRECT-",0
FAILED_MSG0:            dc.b    "FAILED!",$ff,0
WATCHOUT_MSG:           dc.b    "WATCH OUT!",$ff,0
WhowillTransa_MSG:      dc.b    "Who will",$ff
                        dc.b    "Transact-",0
Pleasegotothe_MSG:      dc.b    $ff
                        dc.b    "Please go to",$ff
                        dc.b    "the front!",$ff,0
GOODDAY_MSG:            dc.b    $ff
                        dc.b    "GOOD DAY!",$ff,$ff,0
EATDEATHSCUM_MSG:       dc.b    $ff
                        dc.b    "EAT DEATH, SCUM!",$ff,0
WELCOMEMYCHIL_MSG:      dc.b    $ff
                        dc.b    "WELCOME MY CHILD",$ff,0
NOMORE_MSG:             dc.b    "NO MORE!",$ff,0
SEEKYETHEMARK_MSG:      dc.b    "SEEK YE, THE",$ff
                        dc.b    "MARK OF KINGS!",$ff,0
THOUARTGREATE_MSG:      dc.b    "THOU ART GREATER",$ff,$ff,0
EXPERIENCEMOR_MSG:      dc.b    "EXPERIENCE MORE!",$ff,$ff,0
Unlock_MSG:             dc.b    "Unlock-",0
WHOSEKEY_MSG:           dc.b    "WHOSE KEY-",0
Volume_MSG:             dc.b    "Volume",$ff,0
Full_MSG:               dc.b    "Full ",0
ascii_MSG4:             dc.b    "1/2 ",0
ascii_MSG5:             dc.b    "1/4 ",0
Wearfor_MSG:            dc.b    "Wear for # - ",0
ARMOUR_MSG:             dc.b    "ARMOUR-",0
READY_MSG:              dc.b    " READY",$ff,0
Xit_MSG:                dc.b    "X-it ",0
craft_MSG:              dc.b    "craft",$ff,0
DismountHorse_MSG:      dc.b    "Dismount Horse",$ff,0
Yellwhom_MSG:           dc.b    "Yell, whom?-",0
Ztatsfor_MSG:           dc.b    "Ztats for # - ",0
HANDS_MSG:              dc.b    "HANDS",0
SKIN_MSG0:              dc.b    "SKIN",0

                        SECTION EXODUS01B400,CODE

lbC01B400:              jsr     lbC0216B8
                        tst.b   $4C(a3)
                        bne     lbC01B414
                        jsr     lbC01B5C2
lbC01B414:              cmp.b   #1,$4C(a3)
                        bne     lbC01B42A
                        jsr     lbC026A74
                        jmp     lbC01E48E

lbC01B42A:              cmp.b   #$80,$4C(a3)
                        bne     lbC01B43A
                        jmp     lbC01D9F2

lbC01B43A:              cmp.b   #2,$4C(a3)
                        bcs     lbC01B4C4
                        tst.b   PARTY_X(a3)
                        beq     lbC01B454
                        tst.b   PARTY_Y(a3)
                        bne     lbC01B4C4

lbC01B454:              move.b  PARTY_X_SAVE(a3),PARTY_X(a3)
                        move.b  PARTY_Y_SAVE(a3),PARTY_Y(a3)
                        move.b  #$FF,12(a3)
                        clr.b   $4C(a3)
                        jsr     reset_music
                        lea     EXITTOSOSARIA_MSG,a4
                        jsr     display_message
                        lea     PLEASEWAIT_MSG0,a4
                        jsr     display_message
                        move.l  #'SOSA',d0
                        lea     current_map,a0
                        jsr     file_load_cache
                        move.l  #'SOSM',d0
                        lea     lbL0225D0,a0
                        jsr     file_load_cache
                        move.b  11(a3),$4A(a3)
                        jsr     lbC01A416
                        bsr     lbC01B62C
                        move.b  #1,lbB028FDC
lbC01B4C4:
                        jsr     lbC01BDBC
                        jsr     lbC01BB68
                        jsr     set_player_to_action
                        cmp.b   #$88,d0
                        bne     lbC01B4E4
                        jsr     lbC01C340
lbC01B4E4:
                        jsr     set_player_to_action
                        cmp.b   #$30,d0
                        bne     lbC01B4F8
                        jsr     lbC01C172
lbC01B4F8:
                        jsr     lbC01B58E
                        beq     lbC01B50A
                        jmp     lbC01B588

lbC01B50A:              clr.b   NEGATE_TIME_COUNTER(a3)
                        move.b  #11,d0
                        jsr     lbC0212F8
                        move.b  d0,PARTY_ACTION_X(a3)
                        move.b  #11,d0
                        jsr     lbC0212F8
                        move.b  d0,PARTY_ACTION_Y(a3)
                        cmp.b   #5,d0
                        bne     lbC01B53E
                        move.b  PARTY_ACTION_X(a3),d0
                        cmp.b   #5,d0
                        beq     lbC01B56C
lbC01B53E:
                        jsr     lbC01CCC2
                        cmp.b   #$10,d0
                        bne     lbC01B566
                        move.b  #$7A,(a5)
                        jsr     display_coded_map_page
                        move.w  #4,d0
                        jsr     play_sound
                        jsr     refresh_map_view
lbC01B566:              jmp     lbC01C39E

lbC01B56C:              jsr     lbC01CCC2
                        move.b  #$7A,(a5)
                        jsr     display_coded_map_page
                        jsr     lbC019454
                        jsr     refresh_map_view
lbC01B588:              jmp     lbC01C39E

lbC01B58E:              move.b  $4C(a3),d0
                        cmp.b   #$80,d0
                        bne     lbC01B59E
                        move.b  $15(a3),d0
lbC01B59E:
                        cmp.b   #3,d0
                        bne     lbC01B5BC
                        move.b  PARTY_X_SAVE(a3),d0
                        lea     lbB026992,a0
                        cmp.b   1(a0),d0
                        bne     lbC01B5BC
                        clr.b   d0
                        rts

lbC01B5BC:              move.b  #-1,d0
                        rts

lbC01B5C2:              tst.b   $4C(a3)
                        beq     lbC01B5CC
                        rts

lbC01B5CC:              subq.b  #1,14(a3)
                        bne     lbC01B5EE
                        move.b  #12,14(a3)
                        move.b  ascii_MSG,d0
                        jsr     lbC01B61A
                        move.b  d0,ascii_MSG
lbC01B5EE:
                        subq.b  #1,15(a3)
                        bne     lbC01B610
                        move.b  #4,15(a3)
                        move.b  ascii_MSG0,d0
                        jsr     lbC01B61A
                        move.b  d0,ascii_MSG0
lbC01B610:
                        bsr     lbC01B62C
                        jmp     lbC01B664

lbC01B61A:              addq.b  #1,d0
                        cmp.b   #$B8,d0
                        bcs     lbC01B62A
                        move.b  #$B0,d0
lbC01B62A:              rts

lbC01B62C:              jsr     text_save_position
                        move.b  #8,TEXT_X(a3)
                        clr.b   TEXT_Y(a3)
                        move.b  #$1D,d0
                        jsr     lbC020E90
                        lea     ascii_MSG10,a4
                        jsr     display_message
                        move.b  #$1F,d0
                        jsr     lbC020E90
                        jsr     text_restore_position
                        rts

lbC01B664:              tst.b   $4C(a3)
                        beq     lbC01B66E
                        rts

lbC01B66E:              cmp.b   #$B0,ascii_MSG
                        bne     lbC01B694
                        cmp.b   #$B0,ascii_MSG0
                        bne     lbC01B694
                        move.b  #$18,lbB023535
                        jmp     lbC01B6A2

lbC01B694:              move.b  #12,lbB023535
                        jmp     lbC01B6A2

lbC01B6A2:              lea     lbW026972,a0
                        lea     lbW02697A,a1
                        move.w  #8-1,d1
lbC01B6B2:              move.b  0(a0,d1.w),PARTY_ACTION_X(a3)
                        move.b  0(a1,d1.w),PARTY_ACTION_Y(a3)
                        jsr     tile_get_action_datas
                        move.b  #4,(a5)
                        dbra    d1,lbC01B6B2
                        
                        move.b  ascii_MSG,d0
                        sub.b   #$B0,d0
                        clr.w   d1
                        move.b  d0,d1
                        move.b  0(a0,d1.w),PARTY_ACTION_X(a3)
                        move.b  0(a1,d1.w),PARTY_ACTION_Y(a3)
                        jsr     tile_get_action_datas
                        move.b  #$88,(a5)
                        rts

lbC01B6F4:              jsr     lbC01BD40
                        cmp.b   #$B0,d0
                        bcs.s   lbC01B6F4
                        cmp.b   #$BA,d0
                        bcc.s   lbC01B6F4
                        move.w  d0,-(sp)
                        and.b   #$7F,d0
                        jsr     draw_letter_both_bitmap
                        jsr     text_carriage_return
                        move.w  (sp)+,d0
                        sub.b   #$B0,d0
                        rts

; ----------------------------------------
input_select_character:
                        move.b  #1,$79(a3)
                        jsr     lbC021E16

lbC01B72C:              jsr     lbC01BD40
                        cmp.b   #$B0,d0
                        bcs.s   lbC01B72C
                        cmp.b   #$B5,d0
                        bcc.s   lbC01B72C
                        move.w  d0,-(sp)
                        and.b   #$7F,d0
                        jsr     draw_letter_both_bitmap
                        jsr     text_carriage_return
                        clr.b   $79(a3)
                        jsr     lbC021E16
                        move.w  (sp)+,d0
                        sub.b   #$B0,d0
                        move.b  d0,CURRENT_CHARACTER(a3)
                        sub.b   #1,CURRENT_CHARACTER(a3)
                        tst.b   d0
                        rts

lbC01B76E:              move.b  d0,$69(a3)
                        move.b  $69(a3),d3
                        clr.b   d0
                        tst.b   d1
                        bne     lbC01B784
                        move.b  d0,INPUT_NUMBER(a3)
                        rts

lbC01B784:              move.w  #0,ccr
                        abcd    d3,d0
                        sub.b   #1,d1
                        bne.s   lbC01B784
                        move.b  d0,INPUT_NUMBER(a3)
                        rts

lbC01B796:              move.w  #$1E,d2
                        jsr     lbC01B834
                        bcs     lbC01B7B2
                        cmp.b   #$98,d0
                        bhi     lbC01B7B2
                        move.b  d0,0(a5,d2.w)
                        rts

lbC01B7B2:              move.w  #$9899,d3
                        jsr     set_character_word_value
                        rts

lbC01B7BE:              move.w  #$23,d2
                        jsr     lbC01B834
                        bcs     lbC01B7D2
                        move.b  d0,0(a5,d2.w)
                        rts

lbC01B7D2:              move.w  #$9999,d3
                        jsr     set_character_word_value
                        rts

lbC01B7F0:              move.w  #$1A,d2
                        jsr     lbC01B834
                        bcs     lbC01B822
                        move.b  d0,0(a5,d2.w)
                        move.w  #$1C,d4
                        move.b  0(a5,d4.w),d0
                        cmp.b   0(a5,d2.w),d0
                        bhi     lbC01B832
                        bcs     lbC01B822
                        move.b  1(a5,d4.w),d0
                        cmp.b   1(a5,d2.w),d0
                        bhi     lbC01B832
lbC01B822:              move.w  #$1C,d4
                        move.b  1(a5,d4.w),1(a5,d2.w)
                        move.b  0(a5,d4.w),0(a5,d2.w)
lbC01B832:              rts

lbC01B834:              move.b  d0,d3
                        jsr     get_character_data
                        move.b  1(a5,d2.w),d0
                        move.w  #0,ccr
                        abcd    d3,d0
                        move.b  d0,1(a5,d2.w)
                        clr.b   d3
                        move.b  0(a5,d2.w),d0
                        abcd    d3,d0
                        rts

set_character_word_value:
                        move.b  d3,1(a5,d2.w)
                        lsr.w   #8,d3
                        move.b  d3,0(a5,d2.w)
                        rts

set_character_byte_value:
                        and.w   #$FF,d2
                        move.b  d0,d3
                        jsr     get_character_data
                        move.b  0(a5,d2.w),d0
                        move.w  #0,ccr
                        abcd    d3,d0
                        bcs     character_max_byte_value
                        move.b  d0,0(a5,d2.w)
                        rts

character_max_byte_value:
                        move.b  #$99,0(a5,d2.w)
                        rts

lbC01B888:              tst.b   d0
                        beq     lbC01B8A2
                        move.b  #1,d3
                        clr.b   d1
lbC01B894:              addq.b  #1,d1
                        move.w  #4,ccr
                        sbcd    d3,d0
                        bne.s   lbC01B894
                        move.b  d1,d0
lbC01B8A2:              rts

lbC01B8A4:              move.b  d0,$6C(a3)
                        beq     lbC01B8C0
                        clr.b   d0
                        move.b  #1,d3
lbC01B8B2:
                        move.w  #0,ccr
                        abcd    d3,d0
                        sub.b   #1,$6C(a3)
                        bne.s   lbC01B8B2
lbC01B8C0:              rts

lbC01B8C2:              move.b  d0,d3
                        jsr     get_character_data
                        move.b  CHARACTER_HITPOINTS_LO(a5),d0
                        move.w  #0,ccr
                        sbcd    d3,d0
                        move.b  d0,CHARACTER_HITPOINTS_LO(a5)
                        move.b  CHARACTER_HITPOINTS_HI(a5),d0
                        clr.b   d3
                        sbcd    d3,d0
                        bcs     lbC01B8EE
                        move.b  d0,CHARACTER_HITPOINTS_HI(a5)
                        move.b  #0,d0
                        rts

lbC01B8EE:              move.b  #$C4,$11(a5)
                        clr.b   CHARACTER_HITPOINTS_HI(a5)
                        clr.b   CHARACTER_HITPOINTS_LO(a5)
                        jsr     lbC01B980
                        move.b  #$FF,d0
                        rts

lbC01B908:              move.b  #3,CURRENT_CHARACTER(a3)
lbC01B90E:              jsr     get_character_data
                        move.b  $11(a5),d0
                        cmp.b   #$C7,d0
                        beq     lbC01B97E
                        cmp.b   #$D0,d0
                        beq     lbC01B97E
                        sub.b   #1,CURRENT_CHARACTER(a3)
                        bpl.s   lbC01B90E
                        jsr     lbC01BB68
                        cmp.b   #1,$4C(a3)
                        bne     lbC01B94A
                        jsr     lbC026A74
                        bra     lbC01B950

lbC01B94A:              jsr     display_decoded_map_page
lbC01B950:              lea     ALLPLAYERSOUT_MSG,a4
                        jsr     display_message
                        jsr     lbC01B980
                        bra     lbC01B972

lbC01B966:              lea     ALLPLAYERSOUT_MSG,a4
                        jsr     display_message
lbC01B972:              jsr     lbC021F0E
                        jmp     lbC01A136

lbC01B97E:              rts

lbC01B980:              move.b  $4C(a3),d0
                        clr.b   $4C(a3)
                        move.w  d0,-(sp)
                        tst.b   d0
                        bne     lbC01B9AE
lbC01B990:              move.b  PARTY_X(a3),PARTY_X_SAVE(a3)
                        move.b  PARTY_Y(a3),PARTY_Y_SAVE(a3)
                        move.b  11(a3),$4A(a3)
                        jsr     lbC01A3F2
                        jmp     lbC01B9E0

lbC01B9AE:              cmp.b   #$80,d0
                        bne     lbC01B9CA
                        tst.b   $15(a3)
                        beq.s   lbC01B990
                        bpl     lbC01B9CA
                        move.b  #$16,$4A(a3)
                        bra     lbC01B9DA

lbC01B9CA:              cmp.b   #$16,11(a3)
                        bne     lbC01B9DA
                        move.b  #$7E,$4A(a3)
lbC01B9DA:              jsr     lbC01A416
lbC01B9E0:              move.w  (sp)+,d0
                        move.b  d0,$4C(a3)
                        rts

lbC01B9E8:              move.b  d0,$6C(a3)
                        move.b  11(a3),d0
                        cmp.b   #$16,d0
                        bne     lbC01B9FE
                        jmp     lbC01BB32

lbC01B9FE:              move.b  $6C(a3),d0
                        cmp.b   #$40,d0
                        beq     lbC01BA68
                        cmp.b   #$42,d0
                        beq     lbC01BA62
                        cmp.b   #$44,d0
                        beq     lbC01BA3A
                        cmp.b   #$7C,d0
                        beq     lbC01BA3A
                        cmp.b   #$18,d0
                        bcc     lbC01BA5C
                        cmp.b   #0,d0
                        beq     lbC01BA5C
                        cmp.b   #8,d0
                        beq     lbC01BA5C
lbC01BA3A:              cmp.b   #$14,11(a3)
                        bne     lbC01BA4C
                        move.w  #9,d0
                        bra     lbC01BA50

lbC01BA4C:              move.w  #10,d0
lbC01BA50:              jsr     play_sound
                        move.b  #0,d0
                        rts

lbC01BA5C:              move.b  #$FF,d0
                        rts

lbC01BA62:              jmp     lbC01BAD4

lbC01BA68:              jsr     lbC018DEC
                        move.w  #11,d0
                        jsr     play_sound
                        clr.b   CURRENT_CHARACTER(a3)
lbC01BA7C:              jsr     get_character_data
                        btst    #4,14(a5)
                        beq     lbC01BAA6
                        addq.b  #1,CURRENT_CHARACTER(a3)
                        move.b  CURRENT_CHARACTER(a3),d0
                        cmp.b   PARTY_CHARACTERS(a3),d0
                        bcs.s   lbC01BA7C
                        jsr     lbC018DEC
                        jmp     lbC01BA3A(pc)

lbC01BAA6:              move.b  #$99,d0
                        jsr     lbC01B8C2(pc)
                        jsr     display_decoded_map_page
                        jsr     lbC021124
                        move.w  #4,d0
                        jsr     play_sound
                        jsr     lbC021124
                        jsr     lbC018DEC
                        jmp     lbC01BA5C(pc)

lbC01BAD4:              move.w  #12,d0
                        jsr     play_sound
                        clr.b   CURRENT_CHARACTER(a3)

lbC01BAE2:              jsr     get_character_incapacitated_state
                        bne     lbC01BAF6
                        btst    #5,14(a5)
                        beq     lbC01BB0A
lbC01BAF6:              addq.b  #1,CURRENT_CHARACTER(a3)
                        move.b  CURRENT_CHARACTER(a3),d0
                        cmp.b   PARTY_CHARACTERS(a3),d0
                        bcs.s   lbC01BAE2
                        jmp     lbC01BA3A(pc)

lbC01BB0A:              move.b  #$50,d0
                        jsr     lbC01B8C2(pc)
                        jsr     display_decoded_map_page
                        jsr     lbC021124
                        move.w  #4,d0
                        jsr     play_sound
                        jsr     lbC021124
                        jmp     lbC01BAF6(pc)

lbC01BB32:              move.b  $6C(a3),d0
                        beq     lbC01BB46
                        cmp.b   #$18,d0
                        beq     lbC01BB46
                        jmp     lbC01BA5C(pc)

lbC01BB46:              move.b  #0,d0
                        rts

text_save_position:     move.b  TEXT_X(a3),SAVE_TEXT_X(a3)
                        move.b  TEXT_Y(a3),SAVE_TEXT_Y(a3)
                        rts

text_restore_position:  move.b  SAVE_TEXT_X(a3),TEXT_X(a3)
                        move.b  SAVE_TEXT_Y(a3),TEXT_Y(a3)
                        rts

lbC01BB68:              move.w  #2,lbW01BB76
                        bsr     lbC01BB78
                        rts

lbW01BB76:              dc.w    0

lbC01BB78:              bsr.s   text_save_position
                        move.w  #4-1,d7
                        lea     characters_data,a5
lbC01BB84:              tst.b   0(a5)
                        bne     lbC01BB92
                        jmp     lbC01BC64

lbC01BB92:              bsr     lbC01BCB8
                        move.b  #$26,TEXT_X(a3)
                        move.b  $11(a5),d0
                        and.b   #$7F,d0
                        jsr     lbC020E36
                        addq.b  #1,TEXT_Y(a3)
                        move.b  #$19,TEXT_X(a3)
                        move.b  $18(a5),d0
                        and.b   #$7F,d0
                        jsr     lbC020E36
                        addq.b  #1,TEXT_X(a3)
                        move.b  $16(a5),d0
                        and.b   #$7F,d0
                        jsr     lbC020E36
                        addq.b  #1,TEXT_X(a3)
                        move.b  $17(a5),d0
                        and.b   #$7F,d0
                        jsr     lbC020E36
                        addq.b  #2,TEXT_X(a3)
                        move.b  #$4D,d0
                        bsr     lbC01BC72
                        move.b  CHARACTER_MAGICPOINTS(a5),d0
                        bsr     lbC01BC94
                        addq.b  #1,TEXT_X(a3)
                        move.b  #$4C,d0
                        bsr     lbC01BC72
                        move.b  CHARACTER_EXPERIENCE_HI(a5),d0
                        move.b  #1,d3
                        move.w  #0,ccr
                        abcd    d3,d0
                        bsr     lbC01BC94
                        addq.b  #1,TEXT_Y(a3)
                        move.b  #$19,TEXT_X(a3)
                        move.b  #$48,d0
                        bsr     lbC01BC72
                        move.b  CHARACTER_HITPOINTS_HI(a5),d0
                        bsr     lbC01BC94
                        move.b  CHARACTER_HITPOINTS_LO(a5),d0
                        bsr     lbC01BC94
                        addq.b  #1,TEXT_X(a3)
                        move.b  #$46,d0
                        bsr     lbC01BC72
                        move.b  CHARACTER_FOOD_HI(a5),d0
                        bsr     lbC01BC94
                        move.b  CHARACTER_FOOD_LO(a5),d0
                        bsr     lbC01BC94
lbC01BC64:              add.w   #64,a5
                        dbra    d7,lbC01BB84
                        bsr     text_restore_position
                        rts

lbC01BC72:              lea     F_MSG,a4
                        move.b  d0,(a4)
lbC01BC7A:
                        move.b  (a4)+,d0
                        beq     lbC01BC92
                        and.b   #$7F,d0
                        jsr     lbC020E36
                        addq.b  #1,TEXT_X(a3)
                        bra.s   lbC01BC7A

lbC01BC92:              rts

lbC01BC94:              move.w  d0,-(sp)
                        and.b   #$F0,d0
                        lsr.b   #4,d0
                        bsr     lbC01BCA6
                        move.w  (sp)+,d0
                        and.b   #15,d0
lbC01BCA6:              add.b   #$30,d0
                        jsr     lbC020E36
                        addq.b  #1,TEXT_X(a3)
                        rts

lbC01BCB8:              move.l  a5,a2
                        clr.w   d2
                        move.b  0(a2,d2.w),d0
                        beq     lbC01BD08
lbC01BCC4:              addq.w  #1,d2
                        tst.b   0(a2,d2.w)
                        bne.s   lbC01BCC4
                        move.b  d2,d0
                        lsr.b   #1,d0
                        move.b  d0,d3
                        move.b  #$1F,d0
                        sub.b   d3,d0
                        move.b  d0,TEXT_X(a3)
                        move.b  #3,d0
                        sub.b   d7,d0
                        lsl.b   #2,d0
                        addq.b  #1,d0
                        move.b  d0,TEXT_Y(a3)
lbC01BCEE:
                        move.b  (a2)+,d0
                        beq     lbC01BD08
                        and.b   #$7F,d0
                        jsr     lbC020E36
                        addq.b  #1,TEXT_X(a3)
                        jmp     lbC01BCEE(pc)

lbC01BD08:              rts

increase_character_food:
                        move.b  d0,d3
                        jsr     get_character_data
                        move.b  CHARACTER_FOOD_LO(a5),d0
                        move.w  #0,ccr
                        abcd    d3,d0
                        move.b  d0,CHARACTER_FOOD_LO(a5)
                        move.b  CHARACTER_FOOD_HI(a5),d0
                        clr.b   d3
                        abcd    d3,d0
                        bcs     lbC01BD32
                        move.b  d0,CHARACTER_FOOD_HI(a5)
                        rts

lbC01BD32:              move.b  #$99,d0
                        move.b  d0,CHARACTER_FOOD_LO(a5)
                        move.b  d0,CHARACTER_FOOD_HI(a5)
                        rts

lbC01BD40:              move.b  d1,$1A(a3)
                        move.b  d2,$1B(a3)
lbC01BD48:              cmp.b   #1,$4C(a3)
                        beq     lbC01BD6A
                        jsr     lbC02133E
                        jsr     lbC021496
                        jsr     lbC021386
                        jsr     display_coded_map_page
lbC01BD6A:              jsr     lbC021B48
                        bpl.s   lbC01BD48
                        move.b  $1A(a3),d1
                        move.b  $1B(a3),d2
                        rts

                        SECTION EXODUS01BD7C,DATA

EXITTOSOSARIA_MSG:      dc.b    "EXIT TO SOSARIA!",$ff,0
PLEASEWAIT_MSG0:        dc.b    "PLEASE WAIT...",$ff,0
ascii_MSG10:            dc.b    "�"
ascii_MSG:              dc.b    "���"
ascii_MSG0:             dc.b    "��",0
ALLPLAYERSOUT_MSG:      dc.b    $ff,$ff
                        dc.b    "ALL PLAYERS OUT!",$ff,0
F_MSG:                  dc.b    "F:",0

                        SECTION EXODUS01BDBC,CODE

lbC01BDBC:              tst.b   $4C(a3)
                        beq     lbC01BDD6
                        subq.b  #1,$11(a3)
                        beq     lbC01BDD0
                        rts

lbC01BDD0:              move.b  #4,$11(a3)
lbC01BDD6:              move.b  #4,CURRENT_CHARACTER(a3)
                        subq.b  #1,$12(a3)
                        bpl     lbC01BDEC
                        move.b  #9,$12(a3)
lbC01BDEC:              subq.b  #1,CURRENT_CHARACTER(a3)
                        bpl     lbC01BDFE
                        jsr     lbC01BB68
                        rts

lbC01BDFE:              jsr     get_character_data
                        cmp.b   #$D7,$17(a5)
                        bne     lbC01BE20
                        move.b  CHARACTER_MAGICPOINTS(a5),d0
                        cmp.b   $14(a5),d0
                        bcc     lbC01BE20
                        jsr     lbC01BF40

lbC01BE20:              cmp.b   #$C3,$17(a5)
                        bne     lbC01BE3C
                        move.b  CHARACTER_MAGICPOINTS(a5),d0
                        cmp.b   $15(a5),d0
                        bcc     lbC01BE3C
                        jsr     lbC01BF40

lbC01BE3C:              move.b  $17(a5),d0
                        cmp.b   #$CC,d0
                        beq     lbC01BE5E
                        cmp.b   #$C4,d0
                        beq     lbC01BE5E
                        cmp.b   #$C1,d0
                        beq     lbC01BE5E
                        jmp     lbC01BE68

lbC01BE5E:              move.w  #$14,d2
                        jsr     lbC01BF1E

lbC01BE68:              move.b  $17(a5),d0
                        cmp.b   #$D0,d0
                        beq     lbC01BE8A
                        cmp.b   #$C9,d0
                        beq     lbC01BE8A
                        cmp.b   #$C4,d0
                        beq     lbC01BE8A
                        jmp     lbC01BE94

lbC01BE8A:              move.w  #$15,d2
                        jsr     lbC01BF1E

lbC01BE94:              move.b  $17(a5),d0
                        cmp.b   #$D2,d0
                        bne     lbC01BEB4
                        move.w  #$15,d2
                        jsr     lbC01BF1E
                        move.w  #$14,d2
                        jsr     lbC01BF1E

lbC01BEB4:              move.b  $11(a5),d0
                        beq     lbC01BF1A
                        cmp.b   #$C4,d0
                        beq     lbC01BF1A
                        cmp.b   #$C1,d0
                        beq     lbC01BF1A
                        move.b  #$10,d0
                        jsr     lbC01BFDC
                        cmp.b   #$D0,$11(a5)
                        bne     lbC01BF08
                        move.b  #1,d0
                        jsr     lbC01B8C2
                        jsr     lbC01BB68
                        jsr     lbC021124
                        move.l  #POISON_MSG,a4
                        jsr     display_message
                        jsr     lbC021124

lbC01BF08:              tst.b   $12(a3)
                        bne     lbC01BF1A
                        move.b  #1,d0
                        jsr     lbC01B7F0
lbC01BF1A:              jmp     lbC01BDEC(pc)

lbC01BF1E:              move.b  0(a5,d2.w),d0
                        jsr     lbC01B888
                        lsr.b   #1,d0
                        jsr     lbC01B8A4
                        cmp.b   CHARACTER_MAGICPOINTS(a5),d0
                        bls     lbC01BF3E
                        jsr     lbC01BF40
lbC01BF3E:              rts

lbC01BF40:              move.b  #1,d3
                        move.b  CHARACTER_MAGICPOINTS(a5),d0
                        move.w  #0,ccr
                        abcd    d3,d0
                        move.b  d0,CHARACTER_MAGICPOINTS(a5)
                        rts

get_character_incapacitated_state:
                        jsr     get_character_data
                        move.b  $11(a5),d0
                        cmp.b   #$C7,d0
                        beq     lbC01BF74
                        cmp.b   #$D0,d0
                        beq     lbC01BF74
                        move.b  #-1,d0
                        rts

lbC01BF74:              clr.b   d0
                        rts

lbC01BF78:              jsr     get_character_data
                        move.b  $13(a5),d0
                        jsr     lbC01B888
                        move.b  d0,$64(a3)
                        move.b  $17(a5),d0
                        cmp.b   #$D4,d0
                        beq     lbC01BFB4
                        cmp.b   #$C2,d0
                        beq     lbC01BFBE
                        cmp.b   #$C9,d0
                        beq     lbC01BFBE
                        cmp.b   #$D2,d0
                        beq     lbC01BFBE
                        bne     lbC01BFC4
lbC01BFB4:              add.b   #$80,$64(a3)
                        bra     lbC01BFC4

lbC01BFBE:              add.b   #$40,$64(a3)
lbC01BFC4:              jsr     lbC02167A
                        cmp.b   $64(a3),d0
                        bcc     lbC01BFD6
                        clr.b   d0
                        rts

lbC01BFD6:              move.b  #$FF,d0
                        rts

lbC01BFDC:              move.b  d0,d3
                        move.b  $22(a5),d0
                        sbcd    d3,d0
                        move.b  d0,$22(a5)
                        move.b  CHARACTER_FOOD_LO(a5),d0
                        clr.b   d3
                        sbcd    d3,d0
                        move.b  d0,CHARACTER_FOOD_LO(a5)
                        move.b  CHARACTER_FOOD_HI(a5),d0
                        sbcd    d3,d0
                        bcs     lbC01C004
                        move.b  d0,CHARACTER_FOOD_HI(a5)
                        rts

lbC01C004:              clr.b   CHARACTER_FOOD_HI(a5)
                        clr.b   CHARACTER_FOOD_LO(a5)
                        move.b  #5,d0
                        jsr     lbC01B8C2
                        move.w  #4,d0
                        jsr     play_sound
                        jsr     lbC021124
                        move.l  #STARVING_MSG,a4
                        jsr     display_message
                        jsr     lbC021124
                        rts

lbC01C03A:              tst.b   $4C(a3)
                        beq     lbC01C044
                        rts

lbC01C044:              subq.b  #1,$13(a3)
                        beq     lbC01C050
                        rts

lbC01C050:              move.b  #4,$13(a3)
                        move.b  #8,d0
                        jsr     lbC0212F8
                        beq     lbC01C0EC
                        move.b  lbB0226CC,d0
                        add.b   lbB0226CE,d0
                        and.b   #$3F,d0
                        move.b  d0,PARTY_ACTION_X(a3)
                        move.b  lbB0226CD,d0
                        add.b   lbB0226CF,d0
                        and.b   #$3F,d0
                        move.b  d0,PARTY_ACTION_Y(a3)
                        jsr     tile_get_action_datas
                        beq     lbC01C11C
                        cmp.b   #$2C,d0
                        bne     lbC01C0EC
                        move.b  #$30,(a5)
                        move.b  lbB0226CC,d0
                        move.b  PARTY_ACTION_X(a3),lbB0226CC
                        move.b  d0,PARTY_ACTION_X(a3)
                        move.b  lbB0226CD,d0
                        move.b  PARTY_ACTION_Y(a3),lbB0226CD
                        move.b  d0,PARTY_ACTION_Y(a3)
                        jsr     tile_get_action_datas
                        clr.b   (a5)
                        jsr     refresh_map_view
                        jsr     lbC01C196
                        move.l  #ASHIPWASDESTR_MSG,a4
                        jsr     display_message
                        jmp     lbC01C170

lbC01C0EC:              move.b  #8,d0
                        jsr     lbC0212F8
                        clr.w   d1
                        move.b  d0,d1
                        move.l  #lbB026982,a1
                        move.l  #lbB02698A,a2
                        move.b  0(a1,d1.w),lbB0226CE
                        move.b  0(a2,d1.w),lbB0226CF
                        jmp     lbC01C14E

lbC01C11C:              move.b  #$30,d0
                        move.b  d0,(a5)
                        move.b  lbB0226CC,d0
                        move.b  PARTY_ACTION_X(a3),lbB0226CC
                        move.b  d0,PARTY_ACTION_X(a3)
                        move.b  lbB0226CD,d0
                        move.b  PARTY_ACTION_Y(a3),lbB0226CD
                        move.b  d0,PARTY_ACTION_Y(a3)
                        jsr     tile_get_action_datas
                        clr.b   (a5)
lbC01C14E:              move.b  lbB0226CC,d0
                        cmp.b   PARTY_X(a3),d0
                        bne     lbC01C170
                        move.b  lbB0226CD,d0
                        cmp.b   PARTY_Y(a3),d0
                        bne     lbC01C170
                        jsr     lbC01C172
lbC01C170:              rts

lbC01C172:              move.b  #$18,11(a3)
                        jsr     refresh_map_view
                        move.l  #AHUGESWIRLING_MSG,a4
                        jsr     display_message
                        jsr     lbC01C196
                        jmp     lbC01C1CE

lbC01C196:              tst.b   CURRENT_VOLUME(a3)
                        bmi     lbC01C1CC
                        move.b  #$40,d0
                        move.b  d0,$6C(a3)
lbC01C1A6:              move.b  #$14,d2
lbC01C1AA:              move.b  $6C(a3),d1
lbC01C1AE:              move.w  d0,-(sp)
                        move.w  (sp)+,d0
                        subq.b  #1,d1
                        bne.s   lbC01C1AE
                        subq.b  #1,d2
                        bne.s   lbC01C1AA
                        addq.b  #1,$6C(a3)
                        cmp.b   #$C0,$6C(a3)
                        bcs.s   lbC01C1A6
lbC01C1CC:              rts

lbC01C1CE:              tst.b   $4C(a3)
                        beq     lbC01C20A
                        bpl     lbC01C1E0
                        jmp     lbC01C2BA

lbC01C1E0:              move.b  #$40,d0
                        jsr     lbC0212F8
                        move.b  d0,PARTY_X(a3)
                        move.b  #$40,d0
                        jsr     lbC0212F8
                        move.b  d0,PARTY_Y(a3)
                        jsr     set_player_to_action
                        bne.s   lbC01C1E0
                        jmp     lbC01C326

lbC01C20A:              move.b  PARTY_X(a3),PARTY_X_SAVE(a3)
                        move.b  PARTY_X(a3),PARTY_ACTION_X(a3)
                        move.b  PARTY_Y(a3),PARTY_Y_SAVE(a3)
                        move.b  PARTY_Y(a3),PARTY_ACTION_Y(a3)
                        jsr     tile_get_action_datas
                        clr.b   (a5)
                        move.b  #2,lbB0226CC
                        move.b  #$3E,lbB0226CD
                        move.b  #$16,d0
                        move.b  d0,11(a3)
                        move.b  d0,$4A(a3)
                        jsr     reset_music
                        jsr     lbC01A3F2
                        jsr     lbC020FF2
                        move.l  #ASTHEWATERENT_MSG,a4
                        jsr     display_message
                        move.l  #'MAPZ',d0
                        lea     current_map,a0
                        jsr     file_load_cache
                        move.l  #'MONZ',d0
                        lea     lbL0225D0,a0
                        jsr     file_load_cache
                        move.b  #11,lbB028FDC
                        move.b  #$7E,$4A(a3)
                        move.b  #$20,PARTY_X(a3)
                        move.b  #$36,PARTY_Y(a3)
                        move.b  #$FF,$4C(a3)
                        move.l  #YOUAWAKENONTH_MSG,a4
                        jsr     display_message
                        jmp     lbC01C326

lbC01C2BA:              jsr     reset_music
                        move.l  #'SOSA',d0
                        lea     current_map,a0
                        jsr     file_load_cache
                        move.l  #ALLISDARK_MSG,a4
                        jsr     display_message
                        jsr     lbC020FF2
                        move.l  #'SOSM',d0
                        move.l  #lbL0225D0,a0
                        jsr     file_load_cache
                        move.l  #YOUMADEIT_MSG,a4
                        jsr     display_message
                        move.b  PARTY_X_SAVE(a3),PARTY_X(a3)
                        move.b  PARTY_Y_SAVE(a3),PARTY_Y(a3)
                        clr.b   $4C(a3)
                        move.b  #$16,$4A(a3)
                        jsr     lbC01A416
                        move.b  #1,lbB028FDC
lbC01C326:              move.b  $4A(a3),11(a3)
                        jsr     refresh_map_view
                        move.l  #lbB01C9E4,a4
                        jsr     display_message
                        rts

lbC01C340:              jsr     refresh_map_view
                        jsr     lbC018DEC
                        move.w  #13,d0
                        jsr     play_sound
                        jsr     lbC018DEC
                        clr.w   d0
                        move.b  ascii_MSG0,d0
                        sub.b   #$B0,d0
                        move.l  #lbW026972,a0
                        move.l  #lbW02697A,a1
                        move.b  0(a0,d0.w),PARTY_X(a3)
                        move.b  0(a1,d0.w),PARTY_Y(a3)
                        jsr     refresh_map_view
                        jsr     lbC018DEC
                        move.w  #13,d0
                        jsr     play_sound
                        jsr     lbC018DEC
                        rts

lbC01C39E:              move.b  #$18,TEXT_X(a3)
                        move.b  #$18,TEXT_Y(a3)
                        move.b  11(a3),d0
                        cmp.b   #$14,d0
                        bcs     lbC01C3CC
                        cmp.b   #$18,d0
                        bcc     lbC01C3CC
                        not.b   $5E(a3)
                        bmi     lbC01C3CC
                        jmp     lbC017CF6

lbC01C3CC:              tst.b   NEGATE_TIME_COUNTER(a3)
                        beq     lbC01C3E0
                        subq.b  #1,NEGATE_TIME_COUNTER(a3)
                        jmp     lbC017CF6

lbC01C3E0:              jsr     lbC01C3F2
                        jsr     lbC01C4F2
                        jmp     lbC017CF6

lbC01C3F2:              tst.b   $4C(a3)
                        beq     lbC01C3FC
                        rts

lbC01C3FC:              move.b  #$86,d0
                        jsr     lbC0212F8
                        bmi     lbC01C40C
                        rts

lbC01C40C:              move.w  #$1F,d1
lbC01C410:              dbra    d1,lbC01C416
                        rts

lbC01C416:              lea     lbL0225D0,a0
                        tst.b   0(a0,d1.w)
                        bne.s   lbC01C410
                        move.b  #13,d0
                        jsr     lbC0212F8
                        move.b  d0,$74(a3)
                        move.b  #13,d0
                        jsr     lbC0212F8
                        and.b   $74(a3),d0
                        clr.w   d2
                        move.b  d0,d2
                        move.l  #lbB0269B8,a1
                        move.b  0(a1,d2.w),d0
                        lsl.b   #2,d0
                        lea     lbL0225D0,a0
                        move.b  d0,0(a0,d1.w)
                        move.l  #lbB0269C5,a2
                        move.l  #lbL0225F0,a0
                        move.b  0(a2,d2.w),0(a0,d1.w)
                        move.b  #$40,d0
                        jsr     lbC0212F8
                        cmp.b   PARTY_X(a3),d0
                        beq     lbC01C4E4
                        move.b  d0,PARTY_ACTION_X(a3)
                        move.b  #$40,d0
                        jsr     lbC0212F8
                        cmp.b   PARTY_Y(a3),d0
                        beq     lbC01C4E4
                        move.b  d0,PARTY_ACTION_Y(a3)
                        jsr     tile_get_action_datas
                        move.l  #lbL0225F0,a0
                        cmp.b   0(a0,d1.w),d0
                        bne     lbC01C4E4
                        move.l  #lbL022610,a0
                        move.b  PARTY_ACTION_X(a3),0(a0,d1.w)
                        move.l  #lbL022630,a0
                        move.b  PARTY_ACTION_Y(a3),0(a0,d1.w)
                        lea     lbL022650,a0
                        move.b  #$C0,0(a0,d1.w)
                        lea     lbL0225D0,a0
                        move.b  0(a0,d1.w),(a5)
                        rts

lbC01C4E4:              lea     lbL0225D0,a0
                        clr.b   0(a0,d1.w)
                        jmp     lbC01C410(pc)

lbC01C4F2:              move.w  #$1F,d1
lbC01C4F6:              lea     lbL0225D0,a0
                        tst.b   0(a0,d1.w)
                        beq     lbC01C7C8
                        tst.b   $4C(a3)
                        beq     lbC01C512
                        jmp     lbC01C742

lbC01C512:              jsr     lbC01C826
                        move.b  PARTY_ACTION_X(a3),d0
                        cmp.b   PARTY_X(a3),d0
                        bne     lbC01C536
                        move.b  PARTY_ACTION_Y(a3),d0
                        cmp.b   PARTY_Y(a3),d0
                        bne     lbC01C536
                        jmp     lbC0185B2

lbC01C536:              jsr     tile_get_action_datas
                        jsr     lbC01C7CE
                        beq     lbC01C5B8
                        move.l  #lbL022610,a0
                        move.b  0(a0,d1.w),PARTY_ACTION_X(a3)
                        jsr     tile_get_action_datas
                        jsr     lbC01C7CE
                        beq     lbC01C5B8
                        lea     lbL022610,a0
                        move.b  0(a0,d1.w),d0
                        add.b   PARTY_DIRECTION_X(a3),d0
                        and.b   #$3F,d0
                        move.b  d0,PARTY_ACTION_X(a3)
                        move.l  #lbL022630,a0
                        move.b  0(a0,d1.w),PARTY_ACTION_Y(a3)
                        jsr     tile_get_action_datas
                        jsr     lbC01C7CE
                        beq     lbC01C5B8
                        lea     lbL0225D0,a0
                        move.b  0(a0,d1.w),d0
                        cmp.b   #$3C,d0
                        beq     lbC01C5B2
                        cmp.b   #$74,d0
                        beq     lbC01C5B2
                        bra     lbC01C7C8

lbC01C5B2:              jmp     lbC01C65C

lbC01C5B8:              move.b  PARTY_ACTION_X(a3),d0
                        cmp.b   PARTY_X(a3),d0
                        bne     lbC01C5D4
                        move.b  PARTY_ACTION_Y(a3),d0
                        cmp.b   PARTY_Y(a3),d0
                        bne     lbC01C5D4
                        bra     lbC01C7C8

lbC01C5D4:              move.b  PARTY_ACTION_X(a3),$6A(a3)
                        move.b  PARTY_ACTION_Y(a3),$6B(a3)
                        move.l  #lbL022610,a0
                        move.b  0(a0,d1.w),PARTY_ACTION_X(a3)
                        move.l  #lbL022630,a0
                        move.b  0(a0,d1.w),PARTY_ACTION_Y(a3)
                        jsr     tile_get_action_datas
                        move.l  #lbL0225F0,a0
                        move.b  0(a0,d1.w),(a5)
                        move.b  $6A(a3),d0
                        move.b  d0,PARTY_ACTION_X(a3)
                        move.l  #lbL022610,a0
                        move.b  d0,0(a0,d1.w)
                        move.b  $6B(a3),d0
                        move.b  d0,PARTY_ACTION_Y(a3)
                        move.l  #lbL022630,a0
                        move.b  d0,0(a0,d1.w)
                        jsr     tile_get_action_datas
                        move.l  #lbL0225F0,a0
                        move.b  (a5),0(a0,d1.w)
                        lea     lbL0225D0,a0
                        move.b  0(a0,d1.w),d0
                        move.b  d0,(a5)
                        cmp.b   #$3C,d0
                        beq     lbC01C65C
                        cmp.b   #$74,d0
                        beq     lbC01C65C
                        bra     lbC01C7C8

lbC01C65C:              jsr     lbC02167A
                        bmi     lbC01C732
                        jsr     lbC01C826
                        move.b  #5,d0
                        sub.b   $6E(a3),d0
                        move.b  d0,PARTY_ACTION_X(a3)
                        cmp.b   #11,d0
                        bcc     lbC01C732
                        move.b  #5,d0
                        sub.b   $6F(a3),d0
                        move.b  d0,PARTY_ACTION_Y(a3)
                        cmp.b   #11,d0
                        bcc     lbC01C732
                        jsr     refresh_map_view
                        move.w  #$10,d0
                        lea     lbL0225D0,a0
                        cmp.b   #$74,0(a0,d1.w)
                        beq     lbC01C6B2
                        move.w  #5,d0
lbC01C6B2:              jsr     play_sound
                        move.b  #3,d0
                        move.b  d0,$74(a3)
lbC01C6C0:              move.b  PARTY_DIRECTION_X(a3),d0
                        add.b   d0,PARTY_ACTION_X(a3)
                        cmp.b   #11,PARTY_ACTION_X(a3)
                        bcc     lbC01C732
                        move.b  PARTY_DIRECTION_Y(a3),d0
                        add.b   d0,PARTY_ACTION_Y(a3)
                        cmp.b   #11,PARTY_ACTION_Y(a3)
                        bcc     lbC01C732
                        jsr     lbC01CCC2
                        cmp.b   #8,d0
                        beq     lbC01C732
                        cmp.b   #$46,d0
                        beq     lbC01C732
                        cmp.b   #$48,d0
                        beq     lbC01C732
                        move.w  d0,-(sp)
                        move.b  #$7A,(a5)
                        jsr     display_coded_map_page
                        jsr     lbC01CCC2
                        move.w  (sp)+,d0
                        move.b  d0,(a5)
                        cmp.b   #5,PARTY_ACTION_X(a3)
                        bne     lbC01C736
                        cmp.b   #5,PARTY_ACTION_Y(a3)
                        bne     lbC01C736
                        jsr     lbC019454
lbC01C732:              bra     lbC01C7C8

lbC01C736:              subq.b  #1,$74(a3)
                        bne.s   lbC01C6C0
                        bra     lbC01C7C8

lbC01C742:              lea     lbL022650,a0
                        move.b  0(a0,d1.w),d0
                        and.b   #$C0,d0
                        bne     lbC01C758
                        bra     lbC01C7C8

lbC01C758:              cmp.b   #$40,d0
                        beq     lbC01C76C
                        cmp.b   #$80,d0
                        beq     lbC01C7BE
                        jmp     lbC01C512(pc)

lbC01C76C:              jsr     lbC02167A
                        bmi     lbC01C77A
lbC01C776:              bra     lbC01C7C8

lbC01C77A:              jsr     lbC02167A
                        jsr     lbC01CC9A
                        move.l  #lbL022610,a0
                        add.b   0(a0,d1.w),d0
                        and.b   #$3F,d0
                        move.b  d0,PARTY_ACTION_X(a3)
                        beq.s   lbC01C776
                        jsr     lbC02167A
                        jsr     lbC01CC9A
                        move.l  #lbL022630,a0
                        add.b   0(a0,d1.w),d0
                        and.b   #$3F,d0
                        move.b  d0,PARTY_ACTION_Y(a3)
                        beq.s   lbC01C776
                        jmp     lbC01C536(pc)

lbC01C7BE:              jsr     lbC01C826
                        jmp     lbC01C536(pc)

lbC01C7C8:              dbra    d1,lbC01C4F6
                        rts

lbC01C7CE:              move.w  d0,-(sp)
                        lea     lbL0225D0,a0
                        move.b  0(a0,d1.w),d0
                        cmp.b   #$40,d0
                        bcc     lbC01C7FA
                        cmp.b   #$2C,d0
                        bcs     lbC01C7FA
                        move.w  (sp)+,d0
                        tst.b   d0
                        bne     lbC01C81C
                        jmp     lbC01C822

lbC01C7FA:              move.w  (sp)+,d0
                        cmp.b   #4,d0
                        beq     lbC01C822
                        cmp.b   #8,d0
                        beq     lbC01C822
                        cmp.b   #12,d0
                        beq     lbC01C822
                        cmp.b   #$20,d0
                        beq     lbC01C822
lbC01C81C:              move.b  #$FF,d0
                        rts

lbC01C822:              clr.b   d0
                        rts

lbC01C826:              tst.b   $4C(a3)
                        beq     lbC01C884
                        move.b  PARTY_X(a3),d0
                        move.l  #lbL022610,a0
                        sub.b   0(a0,d1.w),d0
                        move.b  d0,$6E(a3)
                        jsr     lbC01CC9A
                        move.b  d0,PARTY_DIRECTION_X(a3)
                        add.b   0(a0,d1.w),d0
                        and.b   #$3F,d0
                        move.b  d0,PARTY_ACTION_X(a3)
                        move.b  PARTY_Y(a3),d0
                        move.l  #lbL022630,a0
                        sub.b   0(a0,d1.w),d0
                        move.b  d0,$6F(a3)
                        jsr     lbC01CC9A
                        move.b  d0,PARTY_DIRECTION_Y(a3)
                        add.b   0(a0,d1.w),d0
                        and.b   #$3F,d0
                        move.b  d0,PARTY_ACTION_Y(a3)
                        jmp     lbC01C8D4

lbC01C884:              move.b  PARTY_X(a3),d0
                        move.l  #lbL022610,a0
                        sub.b   0(a0,d1.w),d0
                        move.b  d0,$6E(a3)
                        jsr     lbC01CCB0
                        move.b  d0,PARTY_DIRECTION_X(a3)
                        add.b   0(a0,d1.w),d0
                        and.b   #$3F,d0
                        move.b  d0,PARTY_ACTION_X(a3)
                        move.b  PARTY_Y(a3),d0
                        move.l  #lbL022630,a0
                        sub.b   0(a0,d1.w),d0
                        move.b  d0,$6F(a3)
                        jsr     lbC01CCB0
                        move.b  d0,PARTY_DIRECTION_Y(a3)
                        add.b   0(a0,d1.w),d0
                        and.b   #$3F,d0
                        move.b  d0,PARTY_ACTION_Y(a3)
lbC01C8D4:              move.b  $6E(a3),d0
                        jsr     lbC01CCB6
                        move.b  d0,$74(a3)
                        move.b  $6F(a3),d0
                        jsr     lbC01CCB6
                        add.b   d0,$74(a3)
                        rts

lbC01C8F2:              move.w  #$1F,d1
lbC01C8F6:              lea     lbL0225D0,a0
                        tst.b   0(a0,d1.w)
                        beq     lbC01C92C
                        move.l  #lbL022610,a0
                        move.b  0(a0,d1.w),d0
                        cmp.b   PARTY_ACTION_X(a3),d0
                        bne     lbC01C92C
                        move.l  #lbL022630,a0
                        move.b  0(a0,d1.w),d0
                        cmp.b   PARTY_ACTION_Y(a3),d0
                        bne     lbC01C92C
                        move.w  d1,d0
                        rts

lbC01C92C:              dbra    d1,lbC01C8F6
                        move.b  #$FF,d0
                        rts

lbC01C938:              clr.b   TEXT_Y(a3)
                        move.b  #8,TEXT_X(a3)
                        move.b  #$1D,d0
                        jsr     lbC020E90
                        move.b  CURRENT_DUNGEON_LEVEL(a3),d0
                        add.b   #$B1,d0
                        move.l  #LVL00_MSG,a4
                        move.b  d0,5(a4)
                        jsr     display_message
                        bsr     lbC01C9AE
                        move.b  #$18,TEXT_Y(a3)
                        move.b  #6,TEXT_X(a3)
                        move.b  #$1D,d0
                        jsr     lbC020E90
                        move.l  #HEAD_MSG,a4
                        jsr     display_message
                        move.b  $3E(a3),d0
                        beq     lbC01C9BA
                        cmp.b   #1,d0
                        beq     lbC01C9C8
                        cmp.b   #2,d0
                        beq     lbC01C9D6
                        move.l  #WEST_MSG2,a4
                        jsr     display_message
lbC01C9AE:              move.b  #$1F,d0
                        jsr     lbC020E90
                        rts

lbC01C9BA:              move.l  #NORTH_MSG2,a4
                        jsr display_message
                        bra.s   lbC01C9AE

lbC01C9C8:              move.l  #EAST_MSG2,a4
                        jsr display_message
                        bra.s   lbC01C9AE

lbC01C9D6:              move.l  #SOUTH_MSG2,a4
                        jsr display_message
                        bra.s   lbC01C9AE

                        SECTION EXODUS01C9E4,DATA

lbB01C9E4:              dc.b    $1D,0
POISON_MSG:             dc.b    "POISON!",$ff,0
STARVING_MSG:           dc.b    $ff, "STARVING!",$ff,0
ASHIPWASDESTR_MSG:      dc.b    "A SHIP WAS",$ff
                        dc.b    "DESTROYED!",$ff
                        dc.b    $1D
                        dc.b    0
AHUGESWIRLING_MSG:      dc.b    $ff, "A HUGE SWIRLING",$ff
                        dc.b    " --WHIRLPOOL--",$ff
                        dc.B    "  ENGULFS YOU",$ff
                        dc.b    " AND YOUR SHIP",$ff
                        dc.b    " DRAGGING BOTH",$ff
                        dc.b    "     TO A",$ff
                        dc.b    " WATERY GRAVE!",0
ASTHEWATERENT_MSG:
                        dc.b    $ff,$ff, " AS THE WATER",$ff
                        dc.b    " ENTERS  YOUR",$ff
                        dc.b    "LUNGS YOU PASS"
                        dc.b    $ff,"INTO DARKNESS!",$ff,$ff,0
YOUAWAKENONTH_MSG:
                        dc.b    $ff," YOU AWAKEN ON",$ff
                        dc.B    " THE SHORES OF",$ff
                        dc.b    "  A FORGOTTEN",$ff
                        dc.b    "LAND. YOUR SHIP",$ff
                        dc.b    " AND CREW LOST",$ff
                        dc.b    "  TO THE SEA!",$ff,0

ALLISDARK_MSG:          dc.b    $ff,$ff,$ff,$ff,$ff, " ALL IS DARK!",$ff,$ff,0
YOUMADEIT_MSG:          dc.b    " YOU MADE IT!",$ff,0
LVL00_MSG:              dc.b    "LVL:00",0
HEAD_MSG:               dc.b    "HEAD-",0
WEST_MSG2:              dc.b    "-WEST",0
NORTH_MSG2:             dc.b    "NORTH",0
EAST_MSG2:              dc.b    "-EAST",0
SOUTH_MSG2:             dc.b    "SOUTH",0,0
                        dc.b    0

                        SECTION EXODUS01CB5C,CODE

lbC01CB5C:              move.w  #8,d1
lbC01CB60:              sub.w   #1,d1
                        bpl     lbC01CB6E
                        move.b  #$FF,d0
                        rts

lbC01CB6E:              lea     lbL0224B8,a0
                        tst.b   0(a0,d1.w)
                        beq.s   lbC01CB60
                        lea     lbL0224A0,a0
                        move.b  0(a0,d1.w),d0
                        cmp.b   PARTY_ACTION_X(a3),d0
                        bne.s   lbC01CB60
                        lea     lbL0224A8,a0
                        move.b  0(a0,d1.w),d0
                        cmp.b   PARTY_ACTION_Y(a3),d0
                        bne.s   lbC01CB60
                        move.b  d1,d0
                        rts

lbC01CB9E:              move.b  PARTY_DIRECTION_X(a3),d0
                        add.b   d0,PARTY_ACTION_X(a3)
                        cmp.b   #11,PARTY_ACTION_X(a3)
                        bcc     lbC01CBE0
                        move.b  PARTY_DIRECTION_Y(a3),d0
                        add.b   d0,PARTY_ACTION_Y(a3)
                        cmp.b   #11,PARTY_ACTION_Y(a3)
                        bcc     lbC01CBE0
                        jsr     lbC01CCC2
                        move.w  d0,-(sp)
                        move.b  $49(a3),(a5)
                        jsr     display_coded_map_page
                        move.w  (sp)+,d0
                        move.b  d0,(a5)
                        jsr     lbC01CB5C(pc)
                        bmi.s   lbC01CB9E
                        rts

lbC01CBE0:              jsr     display_coded_map_page
                        move.b  #$FF,d0
                        rts

; ----------------------------------------
input_select_direction: jsr     lbC01BD40
set_direction:          cmp.b   #$81,d0
                        beq     key_direction_north
                        cmp.b   #$8A,d0
                        beq     key_direction_south
                        cmp.b   #$88,d0
                        beq     key_direction_west
                        cmp.b   #$95,d0
                        beq     key_direction_east
                        jmp     input_select_direction(pc)

key_direction_north:    lea     North_MSG1,a4
                        jsr     display_message
                        clr.b   PARTY_DIRECTION_X(a3)
                        move.b  #-1,PARTY_DIRECTION_Y(a3)
                        jmp     lbC01CC80

key_direction_south:    lea     South_MSG,a4
                        jsr     display_message
                        clr.b   PARTY_DIRECTION_X(a3)
                        move.b  #1,PARTY_DIRECTION_Y(a3)
                        jmp     lbC01CC80

key_direction_west:     lea     West_MSG1,a4
                        jsr     display_message
                        move.b  #-1,PARTY_DIRECTION_X(a3)
                        clr.b   PARTY_DIRECTION_Y(a3)
                        jmp     lbC01CC80

key_direction_east:     lea     East_MSG1,a4
                        jsr     display_message
                        move.b  #1,PARTY_DIRECTION_X(a3)
                        clr.b   PARTY_DIRECTION_Y(a3)
lbC01CC80:
                        move.b  PARTY_X(a3),d0         ; Calculate the next position from the current one
                        add.b   PARTY_DIRECTION_X(a3),d0
                        move.b  d0,PARTY_ACTION_X(a3)         ; Next X
                        move.b  PARTY_Y(a3),d0
                        add.b   PARTY_DIRECTION_Y(a3),d0
                        move.b  d0,PARTY_ACTION_Y(a3)         ; Next Y
                        rts

lbC01CC9A:              tst.b   d0
                        beq     lbC01CCA8
                        bmi     lbC01CCAA
                        move.b  #1,d0
lbC01CCA8:              rts

lbC01CCAA:              move.b  #$FF,d0
                        rts

lbC01CCB0:              lsl.b   #2,d0
                        jmp     lbC01CC9A(pc)

lbC01CCB6:              btst    #7,d0
                        beq     lbC01CCC0
                        neg.b   d0
lbC01CCC0:              rts

lbC01CCC2:              clr.w   d0
                        move.b  PARTY_ACTION_Y(a3),d0
                        mulu    #11,d0
                        add.b   PARTY_ACTION_X(a3),d0
                        lea     lbL022420,a5
                        add.w   d0,a5
                        move.b  (a5),d0
                        rts

lbC01CCDC:              move.w  d0,-(sp)
                        move.b  $5F(a3),d0
                        cmp.b   #$20,d0
                        bcc     lbC01CD02
                        cmp.b   #$16,d0
                        bcs     lbC01CD02
                        move.w  (sp)+,d0
                        cmp.b   #0,d0
                        bne     lbC01CD24
                        jmp     lbC01CD2A

lbC01CD02:              move.w  (sp)+,d0
                        cmp.b   #2,d0
                        beq     lbC01CD2A
                        cmp.b   #4,d0
                        beq     lbC01CD2A
                        cmp.b   #6,d0
                        beq     lbC01CD2A
                        cmp.b   #$10,d0
                        beq     lbC01CD2A
lbC01CD24:              move.b  #$FF,d0
                        rts

lbC01CD2A:              clr.b   d0
                        rts

lbC01CD2E:              cmp.b   #2,d0
                        beq     lbC01CD54
                        cmp.b   #4,d0
                        beq     lbC01CD54
                        cmp.b   #6,d0
                        beq     lbC01CD54
                        cmp.b   #$10,d0
                        beq     lbC01CD54
                        move.b  #$FF,d0
                        rts

lbC01CD54:              move.w  #10,d0
                        jsr     play_sound
                        clr.b   d0
                        rts

lbC01CD62:              move.b  #$FF,d0
                        move.b  d0,CURRENT_CHARACTER_SAVE(a3)
                        move.b  d0,CURRENT_CHARACTER(a3)
                        move.b  d0,INPUT_NUMBER(a3)
lbC01CD72:              addq.b  #1,CURRENT_CHARACTER(a3)
                        move.b  CURRENT_CHARACTER(a3),d0
                        cmp.b   PARTY_CHARACTERS(a3),d0
                        bcs     lbC01CD8A
                        jmp     lbC01CEEC

lbC01CD8A:              jsr     get_character_data
                        cmp.b   #$C4,$11(a5)
                        beq.s   lbC01CD72
                        cmp.b   #$C1,$11(a5)
                        beq.s   lbC01CD72
                        clr.w   d1
                        move.b  $5E(a3),d1
                        clr.w   d2
                        move.b  CURRENT_CHARACTER(a3),d2
                        lea     lbL0224C0,a0
                        move.b  0(a0,d2.w),d0
                        lea     lbL0224A0,a0
                        sub.b   0(a0,d1.w),d0
                        move.b  d0,PARTY_DIRECTION_X(a3)
                        jsr     lbC01CCB6(pc)
                        move.b  d0,TEMP_NUMBER_W_HI(a3)
                        lea     lbL0224C4,a0
                        move.b  0(a0,d2.w),d0
                        lea     lbL0224A8,a0
                        sub.b   0(a0,d1.w),d0
                        move.b  d0,PARTY_DIRECTION_Y(a3)
                        jsr     lbC01CCB6(pc)
                        move.b  d0,TEMP_NUMBER_W_LO(a3)
                        or.b    TEMP_NUMBER_W_HI(a3),d0
                        cmp.b   #2,d0
                        bcs     lbC01CED0
                        move.b  TEMP_NUMBER_W_HI(a3),d0
                        add.b   TEMP_NUMBER_W_LO(a3),d0
                        cmp.b   INPUT_NUMBER(a3),d0
                        beq     lbC01CE0C
                        bcc     lbC01CD72
lbC01CE0C:              move.b  d0,TEMP_NUMBER_W_HI(a3)
                        move.b  PARTY_DIRECTION_X(a3),d0
                        jsr     lbC01CC9A(pc)
                        move.b  d0,PARTY_DIRECTION_X(a3)
                        lea     lbL0224A0,a0
                        add.b   0(a0,d1.w),d0
                        move.b  d0,PARTY_ACTION_X(a3)
                        move.b  PARTY_DIRECTION_Y(a3),d0
                        jsr     lbC01CC9A(pc)
                        move.b  d0,PARTY_DIRECTION_Y(a3)
                        lea     lbL0224A8,a0
                        add.b   0(a0,d1.w),d0
                        move.b  d0,PARTY_ACTION_Y(a3)
                        jsr     lbC01CCC2(pc)
                        jsr     lbC01CCDC(pc)
                        bpl     lbC01CEA8
                        lea     lbL0224A8,a0
                        move.b  0(a0,d1.w),d0
                        add.b   PARTY_DIRECTION_Y(a3),d0
                        move.b  d0,PARTY_ACTION_Y(a3)
                        lea     lbL0224A0,a0
                        move.b  0(a0,d1.w),PARTY_ACTION_X(a3)
                        jsr     lbC01CCC2(pc)
                        jsr     lbC01CCDC(pc)
                        bpl     lbC01CEA8
                        lea     lbL0224A0,a0
                        move.b  0(a0,d1.w),d0
                        add.b   PARTY_DIRECTION_X(a3),d0
                        move.b  d0,PARTY_ACTION_X(a3)
                        lea     lbL0224A8,a0
                        move.b  0(a0,d1.w),PARTY_ACTION_Y(a3)
                        jsr     lbC01CCC2(pc)
                        jsr     lbC01CCDC(pc)
                        bpl     lbC01CEA8
                        jmp     lbC01CD72(pc)

lbC01CEA8:              move.b  CURRENT_CHARACTER(a3),CURRENT_CHARACTER_SAVE(a3)
                        move.b  TEMP_NUMBER_W_HI(a3),INPUT_NUMBER(a3)
                        move.b  PARTY_ACTION_X(a3),$70(a3)
                        move.b  PARTY_ACTION_Y(a3),$71(a3)
                        move.b  PARTY_DIRECTION_X(a3),$6E(a3)
                        move.b  PARTY_DIRECTION_Y(a3),$6F(a3)
                        jmp     lbC01CD72(pc)

lbC01CED0:              clr.b   INPUT_NUMBER(a3)
                        move.b  d2,CURRENT_CHARACTER_SAVE(a3)
                        move.b  TEMP_NUMBER_W_HI(a3),d0
                        add.b   TEMP_NUMBER_W_LO(a3),d0
                        cmp.b   #2,d0
                        bcs     lbC01CEEC
                        jmp     lbC01CD72(pc)

lbC01CEEC:              move.b  $5E(a3),d1
                        move.b  CURRENT_CHARACTER_SAVE(a3),d2
                        move.b  d2,CURRENT_CHARACTER(a3)
                        rts

lbC01CEFA:              jsr     get_character_data
lbC01CF00:              move.b  $17(a5),d0
                        cmp.b   #$C6,d0
                        beq     lbC01CF5A
                        cmp.b   #$C3,d0
                        beq     lbC01CF60
                        cmp.b   #$D7,d0
                        beq     lbC01CF66
                        cmp.b   #$D4,d0
                        beq     lbC01CF6C
                        cmp.b   #$D0,d0
                        beq     lbC01CF5A
                        cmp.b   #$C2,d0
                        beq     lbC01CF5A
                        cmp.b   #$CC,d0
                        beq     lbC01CF72
                        cmp.b   #$C9,d0
                        beq     lbC01CF66
                        cmp.b   #$C4,d0
                        beq     lbC01CF60
                        cmp.b   #$C1,d0
                        beq     lbC01CF66
                        move.b  #$7E,d0
                        rts

lbC01CF5A:              move.b  #$28,d0
                        rts

lbC01CF60:              move.b  #$2A,d0
                        rts

lbC01CF66:              move.b  #$2C,d0
                        rts

lbC01CF6C:              move.b  #$2E,d0
                        rts

lbC01CF72:              move.b  #$22,d0
                        rts

lbC01CF78:              move.b  current_music_number,$16(a3)
                        clr.b   $17(a3)
                        clr.b   $18(a3)
                        jsr     reset_music
                        move.b  #$D2,$78(a3)
                        move.w  #$20,d1
lbC01CF98:
                        subq.w  #1,d1
                        bmi     lbC01CFC8
                        lea     lbL0225D0,a0
                        move.b  0(a0,d1.w),d0
                        cmp.b   #$4C,d0
                        beq     lbC01CFB8
                        cmp.b   #$48,d0
                        bne.s   lbC01CF98
lbC01CFB8:
                        lea     lbL022650,a0
                        move.b  #$C0,0(a0,d1.w)
                        jmp     lbC01CF98(pc)

lbC01CFC8:              lea     CONFLICT_MSG,a4
                        jsr     display_message
                        jsr     lbC01D81A
                        lea     S_MSG,a4
                        jsr     display_message
                        cmp.b   #1,$4C(a3)
                        bne     lbC01CFFA
                        move.b  #$C3,d0
                        jmp     lbC01D0CA

lbC01CFFA:              cmp.b   #$1E,$5F(a3)
                        bne     lbC01D028
                        move.b  #$2E,$5F(a3)
                        cmp.b   #$16,11(a3)
                        bne     lbC01D01E
                        move.b  #$D3,d0
                        jmp     lbC01D0CA

lbC01D01E:              move.b  #$C1,d0
                        jmp     lbC01D0CA

lbC01D028:              cmp.b   #$16,11(a3)
                        bne     lbC01D050
                        cmp.b   #$20,$5F(a3)
                        bcc     lbC01D046
                        move.b  #$D1,d0
                        jmp     lbC01D0CA

lbC01D046:              move.b  #$D2,d0
                        jmp     lbC01D0CA

lbC01D050:              move.b  $5F(a3),d0
                        cmp.b   #$20,d0
                        bcc     lbC01D06E
                        cmp.b   #$16,d0
                        bcs     lbC01D06E
                        move.b  #$CD,d0
                        jmp     lbC01D0CA

lbC01D06E:              move.b  6(a3),d0
                        cmp.b   #2,d0
                        bne     lbC01D084
                        move.b  #$C7,d0
                        jmp     lbC01D0CA

lbC01D084:              cmp.b   #4,d0
                        bne     lbC01D096
                        move.b  #$C2,d0
                        jmp     lbC01D0CA

lbC01D096:              cmp.b   #6,d0
                        bne     lbC01D0A8
                        move.b  #$C6,d0
                        jmp     lbC01D0CA

lbC01D0A8:              cmp.b   #$10,d0
lbC01D0AC:              bne     lbC01D0BA
                        move.b  #$C3,d0
                        jmp     lbC01D0CA

lbC01D0BA:              cmp.b   #$40,d0
                        beq.s   lbC01D0AC
                        cmp.b   #$42,d0
                        beq.s   lbC01D0AC
                        move.b  #$C7,d0

lbC01D0CA:              move.l  #'CONA',d1
                        move.b  d0,d1
                        exg     d0,d1
                        lea     lbL022420,a0
                        jsr     file_load_cache
                        move.b  $4C(a3),$15(a3)
                        move.b  #$80,$4C(a3)
                        clr.w   d1
                        move.b  PARTY_CHARACTERS(a3),d1
lbC01D0F2:
                        sub.w   #1,d1
                        bmi     lbC01D176
                        move.b  d1,CURRENT_CHARACTER(a3)
                        jsr     get_character_data
                        move.b  $11(a5),d0
                        cmp.b   #$C7,d0
                        beq     lbC01D134
                        cmp.b   #$D0,d0
                        beq     lbC01D134
                        move.b  #$FF,d0
                        lea     lbL0224C0,a0
                        move.b  d0,0(a0,d1.w)
                        lea     lbL0224C4,a0
                        move.b  d0,0(a0,d1.w)
                        jmp     lbC01D0F2(pc)

lbC01D134:              lea     lbL0224C0,a0
                        move.b  0(a0,d1.w),PARTY_ACTION_X(a3)
                        lea     lbL0224C4,a0
                        move.b  0(a0,d1.w),PARTY_ACTION_Y(a3)
                        jsr     lbC01CEFA(pc)
                        lea     lbL0224CC,a0
                        move.b  d0,0(a0,d1.w)
                        jsr     lbC01CCC2(pc)
                        lea     lbL0224C8,a0
                        move.b  d0,0(a0,d1.w)
                        lea     lbL0224CC,a0
                        move.b  0(a0,d1.w),(a5)
                        jmp     lbC01D0F2(pc)

lbC01D176:              move.w  #7,d1
                        lea     lbL0224B8,a0
lbC01D180:              clr.b   0(a0,d1.w)
                        dbra    d1,lbC01D180
                        jsr     lbC01B58E
                        beq     lbC01D1BC
                        move.b  $15(a3),d0
                        cmp.b   #2,d0
                        bcs     lbC01D1C8
                        cmp.b   #4,d0
                        bcc     lbC01D1C8
                        cmp.b   #$24,$5F(a3)
                        beq     lbC01D1BC
                        clr.b   $74(a3)
                        jmp     lbC01D1D6

lbC01D1BC:              move.b  #7,$74(a3)
                        jmp     lbC01D1D6

lbC01D1C8:              jsr     lbC02167A
                        and.b   #7,d0
                        move.b  d0,$74(a3)
lbC01D1D6:              jsr     lbC02167A
                        move.b  d0,d1
                        and.w   #7,d1
                        lea     lbL0224B8,a0
                        tst.b   0(a0,d1.w)
                        bne.s   lbC01D1D6
                        clr.w   d2
                        move.b  $5F(a3),d2
                        lsr.b   #1,d2
                        and.w   #15,d2
                        lea     lbB0269E2,a0
                        move.b  0(a0,d2.w),d0
                        jsr     lbC0212F8
                        or.b    #15,d0
                        lea     lbL0224B8,a0
                        move.b  d0,0(a0,d1.w)
                        lea     lbL0224A0,a0
                        move.b  0(a0,d1.w),PARTY_ACTION_X(a3)
                        lea     lbL0224A8,a0
                        move.b  0(a0,d1.w),PARTY_ACTION_Y(a3)
                        jsr     lbC01CCC2(pc)
                        lea     lbL0224B0,a0
                        move.b  d0,0(a0,d1.w)
                        move.b  $5F(a3),(a5)
                        subq.b  #1,$74(a3)
                        bpl.s   lbC01D1D6
                        jsr     display_coded_map_page
                        move.w  #14,d0
                        jsr     play_sound
                        move.b  #5,lbB028FDC
                        jmp     lbC01D2F4

                        SECTION EXODUS01D2BC,DATA

North_MSG1:             dc.b    "North",$ff,0
South_MSG:              dc.b    "South",$ff,0
East_MSG1:              dc.b    "East",$ff,0
West_MSG1:              dc.b    "West",$ff,0
CONFLICT_MSG:           dc.b    $ff, "---CONFLICT!!---",$ff
                        dc.b    "->",0
S_MSG:                  dc.b    "S",$ff,$ff,0,0
                        dcb.b   2,0
                        
                        SECTION EXODUS01D2F4,CODE

lbC01D2F4:              jsr     lbC01BDBC
                        jsr     lbC01BB68
                        clr.b   $14(a3)
lbC01D304:              move.b  $14(a3),CURRENT_CHARACTER(a3)
                        jsr     get_character_incapacitated_state
                        beq     lbC01D328
                        addq.b  #1,$14(a3)
                        move.b  $14(a3),d0
                        cmp.b   PARTY_CHARACTERS(a3),d0
                        bcc     lbC01D9FE
                        bra.s   lbC01D304

lbC01D328:              jsr     lbC0210C6
                        move.b  #$2F,$3C(a3)
                        lea     PLAYER_MSG,a4
                        jsr     display_message
                        move.b  CURRENT_CHARACTER(a3),d0
                        addq.b  #1,d0
                        jsr     draw_digit
                        lea     ascii_MSG11,a4
                        jsr     display_message
                        clr.b   PARTY_DIRECTION_X(a3)
                        clr.b   PARTY_DIRECTION_Y(a3)
                        clr.w   d1
                        move.b  CURRENT_CHARACTER(a3),d1
                        lea     lbL0224C0,a0
                        move.b  0(a0,d1.w),PARTY_ACTION_X(a3)
                        lea     lbL0224C4,a0
                        move.b  0(a0,d1.w),PARTY_ACTION_Y(a3)
                        jsr     lbC01CCC2
                        move.b  #$C0,$5E(a3)

lbC01D38C:              clr.b   $79(a3)
                        add.b   #$40,$5E(a3)
                        bne     lbC01D3B6
                        jsr     lbC01CCC2
                        clr.w   d1
                        move.b  CURRENT_CHARACTER(a3),d1
                        lea     lbL0224C8,a0
                        move.b  0(a0,d1.w),(a5)
                        jmp     lbC01D3CC

lbC01D3B6:              jsr     lbC01CCC2
                        clr.w   d1
                        move.b  CURRENT_CHARACTER(a3),d1
                        lea     lbL0224CC,a0
                        move.b  0(a0,d1.w),(a5)
lbC01D3CC:
                        jsr     lbC02133E
                        jsr     lbC021496
                        jsr     display_coded_map_page
                        subq.b  #1,$3C(a3)
                        bne     lbC01D3F2
                        move.b  #$A0,d0
                        jmp     lbC01D400

lbC01D3F2:              move.b  #2,$79(a3)
                        jsr     lbC021B48
                        bpl.s   lbC01D38C

lbC01D400:              move.w  d0,-(sp)
                        jsr     lbC01CCC2
                        clr.w   d1
                        move.b  CURRENT_CHARACTER(a3),d1
                        lea     lbL0224CC,a0
                        move.b  0(a0,d1.w),(a5)
                        jsr     display_coded_map_page
                        move.w  (sp)+,d0
                        cmp.b   #3,$79(a3)
                        beq     lbC01D682
                        clr.b   $79(a3)
                        cmp.b   #$92,d0
                        bne     lbC01D43C
                        jmp     sound_switch_on_off

lbC01D43C:              cmp.b   #$93,d0
                        bne     lbC01D44A
                        jmp     music_switch_on_off

lbC01D44A:              cmp.b   #$C1,d0
                        bcs     lbC01D460
                        cmp.b   #$DB,d0
                        bcc     lbC01D460
                        jmp     lbC01D5E4

lbC01D460:              cmp.b   #$81,d0
                        beq     lbC01D48E
                        cmp.b   #$8A,d0
                        beq     lbC01D4A6
                        cmp.b   #$95,d0
                        beq     lbC01D4BE
                        cmp.b   #$88,d0
                        beq     lbC01D4D6
                        cmp.b   #$A0,d0
                        beq     lbC01D4EE
                        jmp     lbC017E70

lbC01D48E:              lea     North_MSG0,a4
                        jsr     display_message
                        move.b  #-1,PARTY_DIRECTION_Y(a3)
                        jmp     lbC01D522

lbC01D4A6:              lea     South_MSG1,a4
                        jsr     display_message
                        move.b  #1,PARTY_DIRECTION_Y(a3)
                        jmp     lbC01D522

lbC01D4BE:              lea     East_MSG0,a4
                        jsr     display_message
                        move.b  #1,PARTY_DIRECTION_X(a3)
                        jmp     lbC01D522

lbC01D4D6:              lea     West_MSG0,a4
                        jsr     display_message
                        move.b  #-1,PARTY_DIRECTION_X(a3)
                        jmp     lbC01D522

lbC01D4EE:              lea     Pass_MSG,a4
                        jsr     display_message
                        jmp     lbC01D9F2

lbC01D500:              lea     INVALIDMOVE_MSG0,a4
                        jsr     display_message
                        move.w  #1,d0
                        jsr     play_sound
                        jmp     lbC01D9F2

lbC01D522:              clr.w   d1
                        move.b  CURRENT_CHARACTER(a3),d1
                        lea     lbL0224C0,a0
                        move.b  0(a0,d1.w),d0
                        add.b   PARTY_DIRECTION_X(a3),d0
                        move.b  d0,PARTY_ACTION_X(a3)
                        move.b  d0,$6A(a3)
                        bmi.s   lbC01D500
                        cmp.b   #11,d0
                        bcc.s   lbC01D500
                        lea     lbL0224C4,a0
                        move.b  0(a0,d1.w),d0
                        add.b   PARTY_DIRECTION_Y(a3),d0
                        move.b  d0,PARTY_ACTION_Y(a3)
                        move.b  d0,$6B(a3)
                        bmi.s   lbC01D500
                        cmp.b   #11,d0
                        bcc.s   lbC01D500
                        jsr     lbC01CCC2
                        jsr     lbC01CD2E
                        bne.s   lbC01D500
                        clr.w   d1
                        move.b  CURRENT_CHARACTER(a3),d1
                        lea     lbL0224C0,a0
                        move.b  0(a0,d1.w),PARTY_ACTION_X(a3)
                        lea     lbL0224C4,a0
                        move.b  0(a0,d1.w),PARTY_ACTION_Y(a3)
                        jsr     lbC01CCC2
                        lea     lbL0224C8,a0
                        move.b  0(a0,d1.w),(a5)
                        move.b  $6A(a3),d0
                        move.b  d0,PARTY_ACTION_X(a3)
                        lea     lbL0224C0,a0
                        move.b  d0,0(a0,d1.w)
                        move.b  $6B(a3),d0
                        move.b  d0,PARTY_ACTION_Y(a3)
                        lea     lbL0224C4,a0
                        move.b  d0,0(a0,d1.w)
                        jsr     lbC01CCC2
                        lea     lbL0224C8,a0
                        move.b  d0,0(a0,d1.w)
                        lea     lbL0224CC,a0
                        move.b  0(a0,d1.w),(a5)
                        jmp     lbC01D9F2

lbC01D5E4:              cmp.b   #$C1,d0
                        bne     lbC01D5F2
                        jmp     lbC01D688

lbC01D5F2:              cmp.b   #$C3,d0
                        bne     lbC01D600
                        jmp     lbC01D9D4

lbC01D600:              cmp.b   #$CE,d0
                        bne     lbC01D61A
                        lea     NEGATETIME_MSG,a4
                        jsr     display_message
                        jmp     lbC019DE2

lbC01D61A:              cmp.b   #$D2,d0
                        bne     lbC01D634
                        lea     READYAWEAPON_MSG,a4
                        jsr     display_message
                        jmp     lbC01A4DE

lbC01D634:              cmp.b   #$D6,d0
                        bne     lbC01D642
                        jmp     command_VolumeSet

lbC01D642:              cmp.b   #$DA,d0
                        bne     lbC01D668
                        lea     ZTATS_MSG,a4
                        jsr     display_message
                        move.b  $14(a3),CURRENT_CHARACTER(a3)
                        jsr     get_character_data
                        jmp     lbC01AE18

lbC01D668:              lea     NOTUSABLECMD_MSG,a4
                        jsr     display_message
                        move.w  #0,d0
                        jsr     play_sound
                        jmp     lbC01D38C(pc)

lbC01D682:              jmp     lbC01ADDC

lbC01D688:              move.b  #$7A,$49(a3)
                        jsr     get_character_data
                        move.b  $30(a5),d0
                        add.b   #$41,d0
                        jsr     get_description_message
                        lea     ATTACKDIR_MSG,a4
                        jsr     display_message
                        move.b  $77(a3),d0
                        bne     lbC01D6D8
lbC01D6B6:
                        jsr     lbC021B48
                        bpl.s   lbC01D6B6
                        cmp.b   #$A0,d0
                        bne     lbC01D6D8
                        lea     NONE_MSG,a4
                        jsr     display_message
                        jmp     lbC01D9F2

lbC01D6D8:              jsr     set_direction
                        move.w  #8,d0
                        jsr     play_sound
                        jsr     get_character_data
                        move.b  $30(a5),d0
                        cmp.b   #3,d0
                        beq     lbC01D718
                        cmp.b   #5,d0
                        beq     lbC01D718
                        cmp.b   #9,d0
                        beq     lbC01D718
                        cmp.b   #13,d0
                        beq     lbC01D718
                        jmp     lbC01D74A

lbC01D718:              clr.w   d1
                        move.b  CURRENT_CHARACTER(a3),d1
                        lea     lbL0224C0,a0
                        move.b  0(a0,d1.w),PARTY_ACTION_X(a3)
                        lea     lbL0224C4,a0
                        move.b  0(a0,d1.w),PARTY_ACTION_Y(a3)
                        jsr     lbC01CB9E
                        bmi     lbC01D7AE
                        move.b  d0,$74(a3)
                        jmp     lbC01D7C6

lbC01D74A:              clr.w   d1
                        move.b  CURRENT_CHARACTER(a3),d1
                        lea     lbL0224C0,a0
                        move.b  0(a0,d1.w),d0
                        add.b   PARTY_DIRECTION_X(a3),d0
                        move.b  d0,PARTY_ACTION_X(a3)
                        lea     lbL0224C4,a0
                        move.b  0(a0,d1.w),d0
                        add.b   PARTY_DIRECTION_Y(a3),d0
                        move.b  d0,PARTY_ACTION_Y(a3)
                        jsr     lbC01CB5C
                        bpl     lbC01D7C6
                        jsr     get_character_data
                        move.b  $30(a5),d0
                        cmp.b   #1,d0
                        bne     lbC01D7AE
                        move.b  #1,d3
                        move.b  $31(a5),d0
                        move.w  #0,ccr
                        sbcd    d3,d0
                        move.b  d0,$31(a5)
                        bne     lbC01D718
                        clr.b   $30(a5)
                        jmp     lbC01D718(pc)

lbC01D7AE:              lea     MISSED_MSG,a4
                        jsr     display_message
                        jsr     display_coded_map_page
                        jmp     lbC01D9F2

lbC01D7C6:              move.b  d0,$74(a3)
                        jsr     lbC01B58E
                        bne     lbC01D7E8
                        jsr     get_character_data
                        cmp.b   #15,$30(a5)
                        beq     lbC01D7E8
                        jmp     lbC01D7AE(pc)

lbC01D7E8:              jsr     lbC02167A
                        bmi     lbC01D80E
                        move.b  #$63,d0
                        jsr     lbC0212F8
                        jsr     lbC01B8A4
                        jsr     get_character_data
                        cmp.b   $13(a5),d0
                        bcc.s   lbC01D7AE
lbC01D80E:              jsr     lbC01D81A
                        jmp     lbC01D860

lbC01D81A:              btst    #0,PARTY_Y(a3)
                        beq     lbC01D84E
                        move.b  $5F(a3),d0
                        sub.b   #$2E,d0
                        bcs     lbC01D84E
                        and.b   #$FE,d0
                        btst    #0,PARTY_X(a3)
                        beq     lbC01D842
                        bset    #0,d0
lbC01D842:              add.b   #$79,d0
                        jsr     get_description_message
                        rts

lbC01D84E:              move.b  $5F(a3),d0
                        lsr.b   #1,d0
                        addq.b  #1,d0
                        jsr     get_description_message
                        rts

lbC01D860:              lea     HIT_MSG,a4
                        jsr     display_message
                        jsr     lbC01CCC2
                        move.b  $49(a3),(a5)
                        jsr     display_coded_map_page
                        move.w  #4,d0
                        jsr     play_sound
                        move.b  $5F(a3),(a5)
                        jsr     get_character_data
                        move.b  $12(a5),d0
                        jsr     lbC01B888
                        or.b    #1,d0
                        jsr     lbC0212F8
                        move.b  d0,INPUT_NUMBER(a3)
                        move.b  $12(a5),d0
                        lsr.b   #1,d0
                        add.b   d0,INPUT_NUMBER(a3)
                        move.b  $30(a5),d0
                        add.b   d0,d0
                        addq.b  #4,d0
                        add.b   d0,INPUT_NUMBER(a3)
                        jsr     lbC01D8D2
                        jsr     display_coded_map_page
                        jmp     lbC01D9F2

lbC01D8D2:              cmp.b   #$26,$5F(a3)
                        beq     lbC01D8F8
                        clr.w   d1
                        move.b  $74(a3),d1
                        lea     lbL0224B8,a0
                        move.b  0(a0,d1.w),d0
                        sub.b   INPUT_NUMBER(a3),d0
                        bls     lbC01D8FA
                        move.b  d0,0(a0,d1.w)
lbC01D8F8:              rts

lbC01D8FA:              lea     KILLEDEXP_MSG,a4
                        jsr     display_message
                        clr.w   d1
                        move.b  $5F(a3),d1
                        lsr.w   #1,d1
                        and.w   #15,d1
                        lea     lbB0269D2,a0
                        move.b  0(a0,d1.w),d0
                        move.b  d0,$65(a3)
                        jsr     draw_2_digits
                        jsr     text_carriage_return
                        move.b  $65(a3),d0
                        jsr     lbC01B796
                        clr.w   d1
                        move.b  $74(a3),d1
                        lea     lbL0224A0,a0
                        move.b  0(a0,d1.w),PARTY_ACTION_X(a3)
                        lea     lbL0224A8,a0
                        move.b  0(a0,d1.w),PARTY_ACTION_Y(a3)
                        jsr     lbC01CCC2
                        lea     lbL0224B0,a0
                        move.b  0(a0,d1.w),(a5)
                        lea     lbL0224B8,a0
                        clr.b   0(a0,d1.w)
                        jsr     display_coded_map_page
                        rts

lbC01D976:              clr.b   NEGATE_TIME_COUNTER(a3)
                        jsr     lbC01BB68
                        jsr     reset_music
                        lea     VICTORY_MSG,a4
                        jsr     display_message
                        move.w  #18,d0
                        jsr     play_sound
                        move.b  $16(a3),d0
                        jsr     start_new_music
                        move.b  $15(a3),$4C(a3)
                        cmp.b   #1,$4C(a3)
                        bne     lbC01D9C8
                        jsr     lbC026A74
                        jsr     lbC026A74
                        jmp     lbC01E08C

lbC01D9C8:              jsr     refresh_map_view
                        jmp     lbC01B400

lbC01D9D4:              move.b  #$78,$49(a3)
                        lea     CASTSPELL_MSG,a4
                        jsr     display_message
                        jsr     get_character_data
                        jmp     lbC0186EE

lbC01D9F2:              move.b  $14(a3),CURRENT_CHARACTER(a3)
                        jsr     lbC0210C6
lbC01D9FE:              move.b  $16(a3),d0
                        cmp.b   #7,d0
                        bne     lbC01DA0E
                        clr.b   NEGATE_TIME_COUNTER(a3)
lbC01DA0E:              move.w  #7,d1
                        lea     lbL0224B8,a0
lbC01DA18:              tst.b   0(a0,d1.w)
                        bne     lbC01DA28
                        dbra    d1,lbC01DA18
                        jmp     lbC01D976(pc)

lbC01DA28:              jsr     lbC01BB68
                        move.b  #$18,TEXT_Y(a3)
                        move.b  #$18,TEXT_X(a3)
                        addq.b  #1,$14(a3)
                        move.b  $14(a3),d0
                        cmp.b   PARTY_CHARACTERS(a3),d0
                        bcc     lbC01DA50
                        jmp     lbC01D304(pc)

lbC01DA50:              tst.b   NEGATE_TIME_COUNTER(a3)
                        beq     lbC01DA62
                        subq.b  #1,NEGATE_TIME_COUNTER(a3)
                        jmp     lbC01D2F4(pc)

lbC01DA62:              move.b  #$FF,$5E(a3)
lbC01DA68:              addq.b  #1,$5E(a3)
                        clr.w   d1
                        move.b  $5E(a3),d1
                        cmp.b   #8,d1
                        bcs     lbC01DA80
                        jmp     lbC01D2F4(pc)

lbC01DA80:              lea     lbL0224B8,a0
                        tst.b   0(a0,d1.w)
                        beq.s   lbC01DA68
                        jsr     lbC01CD62
                        move.b  #$7A,$49(a3)
                        tst.b   INPUT_NUMBER(a3)
                        bne     lbC01DAA6
                        jmp     lbC01DC62

lbC01DAA6:              tst.b   CURRENT_CHARACTER(a3)
                        bmi     lbC01DAC8
                        jsr     lbC02167A
                        bmi     lbC01DAC8
                        cmp.b   #$3A,$5F(a3)
                        bne     lbC01DAC8
                        jmp     lbC01DBB4

lbC01DAC8:              move.b  #$C0,d0
                        jsr     lbC0212F8
                        bpl     lbC01DB12
                        move.b  $5F(a3),d0
                        cmp.b   #$1A,d0
                        beq     lbC01DB1E
                        cmp.b   #$1C,d0
                        beq     lbC01DB1E
                        cmp.b   #$2C,d0
                        beq     lbC01DB1E
                        cmp.b   #$36,d0
                        beq     lbC01DB1E
                        cmp.b   #$3A,d0
                        beq     lbC01DB1E
                        cmp.b   #$3C,d0
                        beq     lbC01DB1E
                        cmp.b   #$26,d0
                        beq     lbC01DB5C

lbC01DB12:              tst.b   INPUT_NUMBER(a3)
                        bpl     lbC01DB5C
                        jmp     lbC01DA68(pc)

lbC01DB1E:              jsr     lbC02167A
                        and.b   #3,d0
                        move.b  d0,CURRENT_CHARACTER(a3)
                        jsr     get_character_data
                        cmp.b   #$C7,$11(a5)
                        bne.s   lbC01DB12
                        jsr     lbC018DEC
                        move.w  #15,d0
                        jsr     play_sound
                        jsr     lbC018DEC
                        move.b  #$78,$49(a3)
                        jmp     lbC01DC62

lbC01DB5C:              lea     lbL0224A0,a0
                        lea     lbL0224A8,a1
                        lea     lbL0224B0,a2
                        move.b  0(a0,d1.w),PARTY_ACTION_X(a3)
                        move.b  0(a1,d1.w),PARTY_ACTION_Y(a3)
                        jsr     lbC01CCC2
                        move.b  0(a2,d1.w),(a5)
                        move.b  $70(a3),d0
                        move.b  d0,PARTY_ACTION_X(a3)
                        move.b  d0,0(a0,d1.w)
                        move.b  $71(a3),d0
                        move.b  d0,PARTY_ACTION_Y(a3)
                        move.b  d0,0(a1,d1.w)
                        jsr     lbC01CCC2
                        move.b  (a5),0(a2,d1.w)
                        move.b  $5F(a3),(a5)
                        jsr     display_coded_map_page
                        jmp     lbC01DA68(pc)

lbC01DBB4:              move.w  #16,d0
                        jsr     play_sound
                        clr.w   d1
                        move.b  $5E(a3),d1
                        lea     lbL0224A0,a0
                        move.b  0(a0,d1.w),PARTY_ACTION_X(a3)
                        lea     lbL0224A8,a0
                        move.b  0(a0,d1.w),PARTY_ACTION_Y(a3)
lbC01DBDC:
                        move.b  $6E(a3),d0
                        add.b   d0,PARTY_ACTION_X(a3)
                        cmp.b   #11,PARTY_ACTION_X(a3)
                        bcc     lbC01DC58
                        move.b  $6F(a3),d0
                        add.b   d0,PARTY_ACTION_Y(a3)
                        cmp.b   #11,PARTY_ACTION_Y(a3)
                        bcc     lbC01DC58
                        clr.w   d2
                        move.b  PARTY_CHARACTERS(a3),d2
                        subq.w  #1,d2
lbC01DC0A:
                        move.b  PARTY_ACTION_X(a3),d0
                        lea     lbL0224C0,a0
                        cmp.b   0(a0,d2.w),d0
                        bne     lbC01DC38
                        move.b  PARTY_ACTION_Y(a3),d0
                        lea     lbL0224C4,a0
                        cmp.b   0(a0,d2.w),d0
                        bne     lbC01DC38
                        move.b  d2,CURRENT_CHARACTER(a3)
                        jmp     lbC01DD2A

lbC01DC38:              sub.b   #1,d2
                        bpl.s   lbC01DC0A
                        jsr     lbC01CCC2
                        move.w  d0,-(sp)
                        move.b  #$7A,(a5)
                        jsr     display_coded_map_page
                        move.w  (sp)+,d0
                        move.b  d0,(a5)
                        jmp     lbC01DBDC(pc)

lbC01DC58:              jsr     display_coded_map_page
                        jmp     lbC01DA68(pc)

lbC01DC62:              move.b  $5F(a3),d0
                        cmp.b   #$1C,d0
                        beq     lbC01DC84
                        cmp.b   #$3C,d0
                        beq     lbC01DC84
                        cmp.b   #$38,d0
                        beq     lbC01DC84
                        jmp     lbC01DC90

lbC01DC84:              jsr     lbC01DEE6
                        jmp     lbC01DC9E

lbC01DC90:              cmp.b   #$2E,d0
                        bne     lbC01DC9E
                        jsr     lbC01DE4E
lbC01DC9E:
                        lea     PLR_MSG0,a4
                        jsr     display_message
                        move.b  CURRENT_CHARACTER(a3),d0
                        addq.b  #1,d0
                        jsr     draw_digit
                        move.w  #8,d0
                        jsr     play_sound
                        cmp.b   #3,$15(a3)
                        bne     lbC01DCF0
                        move.b  PARTY_X_SAVE(a3),d0
                        cmp.b   lbW026993,d0
                        bne     lbC01DCF0
                        jsr     get_character_data
                        cmp.b   #7,$28(a5)
                        beq     lbC01DCF0
                        jmp     lbC01DD1E

lbC01DCF0:              jsr     get_character_data
                        move.b  $28(a5),d0
                        add.b   #$10,d0
                        jsr     lbC0212F8
                        cmp.b   #8,d0
                        bcs     lbC01DD1E
                        lea     MISSED_MSG0,a4
                        jsr     display_message
                        jmp     lbC01DA68(pc)

lbC01DD1E:              lea     HIT_MSG0,a4
                        jsr     display_message
lbC01DD2A:              jsr     get_character_data
                        move.b  $5F(a3),d1
                        lsr.b   #1,d1
                        and.w   #15,d1
                        lea     lbB0269E2,a0
                        move.b  0(a0,d1.w),d0
                        lsr.b   #3,d0
                        add.b   CHARACTER_MAXPOINTS_HI(a5),d0
                        or.b    #1,d0
                        jsr     lbC0212F8
                        addq.b  #1,d0
                        jsr     lbC01B8A4
                        jsr     lbC01B8C2
                        bmi     lbC01DD78
                        move.b  $15(a3),d0
                        and.b   #3,d0
                        lsl.b   #4,d0
                        jsr     lbC01B8C2
lbC01DD78:
                        move.w  #4,d0
                        jsr     play_sound
                        jsr     lbC01BB78
                        clr.w   lbW01BB76
                        clr.w   d2
                        move.b  CURRENT_CHARACTER(a3),d2
                        lea     lbL0224C0,a0
                        move.b  0(a0,d2.w),PARTY_ACTION_X(a3)
                        lea     lbL0224C4,a0
                        move.b  0(a0,d2.w),PARTY_ACTION_Y(a3)
                        jsr     lbC01CCC2
                        move.b  $49(a3),(a5)
                        move.l  a5,-(sp)
                        jsr     display_coded_map_page
                        jsr     lbC01BB78
                        jsr     lbC021124
                        move.l  (sp)+,a5
                        moveq   #0,d1
                        move.b  CURRENT_CHARACTER(a3),d1
                        lea     lbL0224CC,a0
                        move.b  0(a0,d1.w),(a5)
                        jsr     display_coded_map_page
                        jsr     lbC021124
                        jsr     get_character_data
                        cmp.b   #$C4,$11(a5)
                        bne     lbC01DA68
                        lea     KILLED_MSG,a4
                        jsr     display_message
                        clr.w   d1
                        move.b  CURRENT_CHARACTER(a3),d1
                        lea     lbL0224C0,a0
                        lea     lbL0224C4,a1
                        lea     lbL0224C8,a2
                        move.b  0(a0,d1.w),PARTY_ACTION_X(a3)
                        move.b  0(a1,d1.w),PARTY_ACTION_Y(a3)
                        jsr     lbC01CCC2
                        move.b  0(a2,d1.w),(a5)
                        move.b  #$FF,d0
                        move.b  d0,0(a0,d1.w)
                        move.b  d0,0(a1,d1.w)
                        jsr     display_coded_map_page
                        jsr     lbC01B908
                        bra     lbC01DA68

lbC01DE4E:              jsr     get_character_data
                        jsr     lbC02167A
                        bmi     lbC01DE8A
                        jsr     lbC02167A
                        and.b   #15,d0
                        beq     lbC01DEE4
                        cmp.b   $30(a5),d0
                        beq     lbC01DEE4
                        clr.w   d2
                        move.b  d0,d2
                        move.b  $30(a5,d2.w),d0
                        beq     lbC01DEE4
                        clr.b   $30(a5,d2.w)
                        jmp     lbC01DEB4

lbC01DE8A:              jsr     lbC02167A
                        and.b   #7,d0
                        beq     lbC01DEE4
                        cmp.b   $28(a5),d0
                        beq     lbC01DEE4
                        clr.w   d2
                        move.b  d0,d2
                        move.b  $28(a5,d2.w),d0
                        beq     lbC01DEE4
                        move.b  #0,d0
                        clr.b   $28(a5,d2.w)
lbC01DEB4:
                        lea     PLR_MSG0,a4
                        jsr     display_message
                        move.b  CURRENT_CHARACTER(a3),d0
                        addq.b  #1,d0
                        jsr     draw_digit
                        lea     PILFERED_MSG,a4
                        jsr     display_message
                        move.w  #8,d0
                        jsr     play_sound
lbC01DEE4:
                        rts

lbC01DEE6:              jsr     lbC02167A
                        and.b   #3,d0
                        beq     lbC01DEF6
lbC01DEF4:              rts

lbC01DEF6:              jsr     get_character_data
                        cmp.b   #$C7,$11(a5)
                        bne.s   lbC01DEF4
                        move.b  #$D0,$11(a5)
                        lea     PLR_MSG0,a4
                        jsr     display_message
                        move.b  CURRENT_CHARACTER(a3),d0
                        addq.b  #1,d0
                        jsr     draw_digit
                        lea     POISONED_MSG,a4
                        jsr     display_message
                        move.w  #8,d0
                        jsr     play_sound
                        rts

lbC01DF3C:              lea     lbL0226D0,a4
                        jmp     lbC01DF4E

get_description_message:    
                        lea     descriptions_messages,a4
lbC01DF4E:              tst.b   (a4)+
                        bne.s   lbC01DF4E
                        subq.b  #1,d0
                        bne.s   lbC01DF4E
                        jsr     display_message
                        rts

                        SECTION EXODUS01DF60,DATA

PLAYER_MSG:             dc.b    "----PLAYER-",0
ascii_MSG11:            dc.b    "----",$ff
                        dc.b    $1D
                        dc.b    0
North_MSG0:             dc.b    "North",$ff,0
South_MSG1:             dc.b    "South",$ff,0
East_MSG0:              dc.b    "East",$ff,0
West_MSG0:              dc.b    "West",$ff,0
Pass_MSG:               dc.b    "Pass",$ff,0
INVALIDMOVE_MSG0:       dc.b    "INVALID MOVE!",$ff,0
NEGATETIME_MSG:         dc.b    "NEGATE TIME!",$ff,0
READYAWEAPON_MSG:       dc.b    "READY A WEAPON!",$ff,0
ZTATS_MSG:              dc.b    "ZTATS",$ff,0
NOTUSABLECMD_MSG:       dc.b    "NOT USABLE CMD!",$ff
                        dc.b    $1D
                        dc.b    0
ATTACKDIR_MSG:          dc.b    " ATTACK",$ff
                        dc.b    "DIR-",0
NONE_MSG:               dc.b    "NONE",$ff,0
MISSED_MSG:             dc.b    "MISSED!",$ff,0
HIT_MSG:                dc.b    $ff, "HIT!",$ff,0
KILLEDEXP_MSG:          dc.b    "KILLED! EXP.-",0
VICTORY_MSG:            dc.b    $ff, "****VICTORY!****",$ff,$ff,0
CASTSPELL_MSG:          dc.b    "CAST SPELL!",$ff,0
PLR_MSG0:               dc.b    "PLR-",0,0
MISSED_MSG0:            dc.b    "-MISSED!",$ff,0
HIT_MSG0:               dc.b    "-HIT!",$ff,0
KILLED_MSG:             dc.b    "KILLED!!!",$ff,0
PILFERED_MSG:           dc.b    "-PILFERED!",$ff,0
POISONED_MSG:           dc.b    "-POISONED!",$ff,0

                        SECTION EXODUS01E068,CODE

lbC01E068:              jsr     reset_music
                        clr.b   $78(a3)
                        clr.b   LIGHT_COUNTER(a3)
                        jsr     lbC026A74
                        jsr     lbC026A74
                        move.b  #4,d0
                        jsr     start_new_music

lbC01E08C:              jsr     lbC01B908
                        jsr     lbC01C938
                        jsr     lbC026A74
                        move.b  PARTY_X(a3),PARTY_ACTION_X(a3)
                        move.b  PARTY_Y(a3),PARTY_ACTION_Y(a3)
                        jsr     tile_dungeon_get_action_datas
                        clr.b   d1
                        cmp.b   #$40,d0
                        bne     lbC01E0BE
                        move.b  #$C7,d1
lbC01E0BE:              cmp.b   $78(a3),d1
                        beq     lbC01E0D0
                        move.b  d1,$78(a3)
                        jsr     lbC021E16
lbC01E0D0:
                        move.b  #$18,TEXT_Y(a3)
                        move.b  #$18,TEXT_X(a3)
                        tst.b   LIGHT_COUNTER(a3)
                        bne.b   lbC01E0F6
                        lea     ITSDARK_MSG,a4
                        jsr     display_message
                        jsr     lbC020FF2
lbC01E0F6:
                        lea     lbW01E9D8,a4
                        jsr     display_message
                        move.l  #$1C03E,dungeon_pass_counter
lbC01E10C:
                        clr.b   $79(a3)
                        subq.l  #1,dungeon_pass_counter
                        beq     dungeon_pass_message
                        move.b  #2,$79(a3)
                        jsr     lbC021B48
                        bpl.s   lbC01E10C
                        cmp.b   #3,$79(a3)
                        beq     lbC01E1B0
                        clr.b   $79(a3)
                        cmp.b   #$81,d0
                        beq     lbC01E234
                        cmp.b   #$8A,d0
                        beq     lbC01E29E
                        cmp.b   #$95,d0
                        beq     lbC01E304
                        cmp.b   #$88,d0
                        beq     lbC01E342
                        cmp.b   #$92,d0
                        bne     lbC01E168
                        jmp     sound_switch_on_off

lbC01E168:              cmp.b   #$93,d0
                        bne     lbC01E176
                        jmp     music_switch_on_off

lbC01E176:              cmp.b   #$A0,d0
                        beq     dungeon_pass_message
                        cmp.b   #$C1,d0
                        bcc     lbC01E18C
                        jmp     lbC017E70

lbC01E18C:              cmp.b   #$DB,d0
                        bcs     handle_dungeon_commands
                        jmp     lbC017E70

handle_dungeon_commands:
                        sub.b   #$C1,d0
                        clr.w   d3
                        move.b  d0,d3
                        lsl.w   #2,d3
                        lea     dungeon_commands_table,a0
                        move.l  0(a0,d3.w),a0
                        jmp     (a0)

lbC01E1B0:              jmp     lbC01ADDC

dungeon_pass_counter:   dc.l    0

dungeon_commands_table: dc.l    command_dungeon_unknown_command
                        dc.l    command_dungeon_unknown_command
                        dc.l    command_Cast
                        dc.l    command_dungeon_Descend
                        dc.l    command_dungeon_unknown_command
                        dc.l    command_dungeon_unknown_command
                        dc.l    command_GetChest
                        dc.l    command_HandEquipment
                        dc.l    command_IgniteTorch
                        dc.l    command_JoinGold
                        dc.l    command_dungeon_Klimb
                        dc.l    command_dungeon_unknown_command
                        dc.l    command_ModifyOrder
                        dc.l    command_NegateTime
                        dc.l    command_OtherCommand
                        dc.l    command_dungeon_PeerAtGem
                        dc.l    command_dungeon_unknown_command
                        dc.l    command_ReadyObject
                        dc.l    command_dungeon_unknown_command
                        dc.l    command_dungeon_unknown_command
                        dc.l    command_dungeon_unknown_command
                        dc.l    command_VolumeSet
                        dc.l    command_WearObject
                        dc.l    command_dungeon_unknown_command
                        dc.l    command_Yell
                        dc.l    command_ZStats

dungeon_pass_message:   lea     Pass_MSG1,a4
                        jsr     display_message
                        jmp     lbC01E48E

lbC01E234:              lea     Advance_MSG,a4
                        jsr     display_message
                        clr.w   d1
                        move.b  $3E(a3),d1
                        lea     lbW0269F2,a0
                        move.b  0(a0,d1.w),d0
                        add.b   PARTY_X(a3),d0
                        and.b   #15,d0
                        move.b  d0,PARTY_ACTION_X(a3)
                        lea     lbW0269F6,a0
                        move.b  0(a0,d1.w),d0
                        add.b   PARTY_Y(a3),d0
                        and.b   #15,d0
                        move.b  d0,PARTY_ACTION_Y(a3)
                        jsr     tile_dungeon_get_action_datas
                        cmp.b   #$80,d0
                        bne     lbC01E286
                        jmp     lbC01E380

lbC01E286:              move.b  PARTY_ACTION_X(a3),PARTY_X(a3)
                        move.b  PARTY_ACTION_Y(a3),PARTY_Y(a3)
                        jsr     lbC026A74
                        jmp     lbC01E48E

lbC01E29E:              lea     Retreat_MSG,a4
                        jsr     display_message
                        move.b  $3E(a3),d1
                        addq.b  #2,d1
                        and.w   #3,d1
                        lea     lbW0269F2,a0
                        move.b  0(a0,d1.w),d0
                        add.b   PARTY_X(a3),d0
                        and.b   #15,d0
                        move.b  d0,PARTY_ACTION_X(a3)
                        lea     lbW0269F6,a0
                        move.b  0(a0,d1.w),d0
                        add.b   PARTY_Y(a3),d0
                        and.b   #15,d0
                        move.b  d0,PARTY_ACTION_Y(a3)
                        jsr     tile_dungeon_get_action_datas
                        bmi     lbC01E380
                        move.b  PARTY_ACTION_X(a3),PARTY_X(a3)
                        move.b  PARTY_ACTION_Y(a3),PARTY_Y(a3)
                        jsr     lbC026A74
                        jmp     lbC01E48E

lbC01E304:              lea     Turnright_MSG,a4
                        jsr     display_message
                        move.b  PARTY_X(a3),PARTY_ACTION_X(a3)
                        move.b  PARTY_Y(a3),PARTY_ACTION_Y(a3)
                        jsr     tile_dungeon_get_action_datas
                        cmp.b   #$A0,d0
                        bcc     lbC01E380
                        addq.b  #1,$3E(a3)
                        and.b   #3,$3E(a3)
                        jsr     lbC026A74
                        jmp     lbC01E48E

lbC01E342:              lea     Turnleft_MSG,a4
                        jsr     display_message
                        move.b  PARTY_X(a3),PARTY_ACTION_X(a3)
                        move.b  PARTY_Y(a3),PARTY_ACTION_Y(a3)
                        jsr     tile_dungeon_get_action_datas
                        cmp.b   #$A0,d0
                        bcc     lbC01E380
                        subq.b  #1,$3E(a3)
                        and.b   #3,$3E(a3)
                        jsr     lbC026A74
                        jmp     lbC01E48E

lbC01E380:              lea     INVALIDMOVE_MSG1,a4
lbC01E386:              jsr     display_message
                        move.w  #1,d0
                        jsr     play_sound
                        jmp     lbC01E48E

lbC01E3A2:              lea     INVALIDCMD_MSG,a4
                        jmp     lbC01E386(pc)

; ----------------------------------------
; Unknown dungeon command
command_dungeon_unknown_command:
                        lea     NOTADNGCMD_MSG,a4
                        jmp     lbC01E386(pc)

; ----------------------------------------
; (D)ESCEND (dungeon)
command_dungeon_Descend:
                        lea     Descend_MSG0,a4
                        jsr     display_message
                        move.b  PARTY_X(a3),PARTY_ACTION_X(a3)
                        move.b  PARTY_Y(a3),PARTY_ACTION_Y(a3)
                        jsr     tile_dungeon_get_action_datas
                        bmi.s   lbC01E3A2
                        and.b   #$20,d0
                        beq.s   lbC01E3A2
                        addq.b  #1,CURRENT_DUNGEON_LEVEL(a3)
                        jsr     lbC026A74
                        jmp     lbC01E48E

; ----------------------------------------
; (K)LIMB (dungeon)
command_dungeon_Klimb:
                        lea     Klimb_MSG,a4
                        jsr     display_message
                        move.b  PARTY_X(a3),PARTY_ACTION_X(a3)
                        move.b  PARTY_Y(a3),PARTY_ACTION_Y(a3)
                        jsr     tile_dungeon_get_action_datas
                        and.b   #$10,d0
                        beq.s   lbC01E3A2
                        subq.b  #1,CURRENT_DUNGEON_LEVEL(a3)
                        bmi     lbC01E428
                        jsr     lbC026A74
                        jmp     lbC01E48E

lbC01E428:              jmp     lbC01B454

; ----------------------------------------
; (P)EER AT GEM (dungeon)
command_dungeon_PeerAtGem:
                        lea     PeeratagemWHO_MSG,a4
                        jsr     display_message
                        jsr     input_select_character
                        bne     lbC01E44A
                        jmp     lbC01B400

lbC01E44A:              jsr     get_character_data
                        move.b  CHARACTER_GEMS(a5),d0
                        bne     lbC01E45E
                        jmp     none_left_message

lbC01E45E:              move.b  #1,d3
                        move.w  #0,ccr
                        sbcd    d3,d0
                        move.b  d0,CHARACTER_GEMS(a5)
lbC01E46C:              jsr     reset_music
                        move.b  #10,current_music_number
                        move.b  #4,lbB028FDC
                        jsr     lbC027522
                        jmp     lbC01E48E

lbC01E48E:              jsr     lbC0216B8
                        jsr     lbC01BDBC
                        jsr     lbC01BB68
                        move.b  #$18,TEXT_Y(a3)
                        move.b  #$18,TEXT_X(a3)
                        tst.b   LIGHT_COUNTER(a3)
                        beq     decrease_light_counter
                        subq.b  #1,LIGHT_COUNTER(a3)
decrease_light_counter: move.b  PARTY_X(a3),PARTY_ACTION_X(a3)
                        move.b  PARTY_Y(a3),PARTY_ACTION_Y(a3)
                        jsr     tile_dungeon_get_action_datas
                        beq     lbC01E4D6
                        jmp     lbC01E522

lbC01E4D6:              move.b  #$82,d0
                        add.b   CURRENT_DUNGEON_LEVEL(a3),d0
                        jsr     lbC0212F8
                        bmi     lbC01E4EC
                        jmp     lbC01E08C(pc)

lbC01E4EC:              move.b  CURRENT_DUNGEON_LEVEL(a3),d0
                        addq.b  #2,d0
                        jsr     lbC0212F8
                        cmp.b   #7,d0
                        bcs     lbC01E506
                        move.b  #6,d0
lbC01E506:              add.b   #24,d0
                        add.b   d0,d0
                        move.b  d0,$5F(a3)
                        jsr     tile_dungeon_get_action_datas
                        move.b  #$40,d0
                        move.b  d0,(a5)
                        jmp     lbC01CF78

lbC01E522:              cmp.b   #1,d0
                        bne     lbC01E532
                        clr.b   (a5)
                        jmp     lbC01E592

lbC01E532:              cmp.b   #2,d0
                        beq     lbC01E650
                        cmp.b   #3,d0
                        beq     lbC01E7DA
                        cmp.b   #4,d0
                        beq     lbC01E60A
                        cmp.b   #5,d0
                        beq     lbC01E854
                        cmp.b   #6,d0
                        beq     lbC01E7EE
                        cmp.b   #8,d0
                        beq     lbC01E566
                        jmp     lbC01E08C(pc)

lbC01E566:              jsr     tile_dungeon_get_action_datas
                        clr.b   (a5)
                        lea     MISTYWRITING_MSG,a4
                        jsr     display_message
                        move.b  CURRENT_DUNGEON_LEVEL(a3),d0
                        addq.b  #1,d0
                        jsr     lbC01DF3C
                        jsr     text_carriage_return
                        jmp     lbC01E08C(pc)

lbC01E592:              jsr     reset_music
                        move.l  #'TIME',d0
                        lea     lbL022420,a0
                        jsr     file_load_cache
                        jsr     display_coded_map_page
                        move.b  #10,d0
                        jsr     start_new_music
                        lea     YOUSEEAVISION_MSG,a4
                        jsr     display_message
lbC01E5C6:
                        move.w  #$1100,d2
                        jsr     lbC021AAA
                        move.w  #$1000,d2
                        jsr     lbC021AAA
                        move.w  #$1000,d2
                        jsr     lbC021AAA
                        jsr     display_coded_map_page
                        jsr     lbC021B48
                        bpl.s   lbC01E5C6
                        jsr     text_carriage_return
                        jsr     lbC026A74
                        move.b  #4,lbB028FDC
                        jmp     lbC01E08C(pc)

lbC01E60A:              jsr     tile_dungeon_get_action_datas
                        clr.b   (a5)
                        lea     ARGHATRAP_MSG,a4
                        jsr     display_message
                        move.w  #4,d0
                        jsr     play_sound
                        clr.b   CURRENT_CHARACTER(a3)
                        jsr     lbC01BF78
                        bne     lbC01E646
                        lea     EVADED_MSG,a4
                        jsr     display_message
                        jmp     lbC01E08C(pc)

lbC01E646:              jsr     lbC019454
                        jmp     lbC01E08C(pc)

lbC01E650:              jsr     reset_music
                        move.l  #'FNTN',d0
                        lea     lbL022420,a0
                        jsr     file_load_cache
                        jsr     display_coded_map_page
                        move.b  #10,d0
                        jsr     start_new_music
lbC01E678:
                        move.b  #24,TEXT_Y(a3)
                        move.b  #24,TEXT_X(a3)
                        lea     AFOUNTAINWHOW_MSG,a4
                        jsr     display_message
                        clr.b   $4C(a3)
                        jsr     input_select_character
                        move.b  #1,$4C(a3)
                        tst.b   d0
                        bne     lbC01E6BA
                        jsr     lbC026A74
                        move.b  #4,d0
                        jsr     start_new_music
                        jmp     lbC01E08C(pc)

lbC01E6BA:              jsr     get_character_incapacitated_state
                        beq     lbC01E6DE
                        lea     CANT_MSG,a4
                        jsr     display_message
                        move.w  #1,d0
                        jsr     play_sound
                        jmp     lbC01E678(pc)

lbC01E6DE:              move.b  PARTY_X(a3),d0
                        and.b   #3,d0
                        beq     lbC01E732
                        cmp.b   #1,d0
                        bne     lbC01E6F8
                        jmp     lbC01E76A

lbC01E6F8:              cmp.b   #2,d0
                        bne     lbC01E706
                        jmp     lbC01E792

lbC01E706:              lea     AHTHATSNICE_MSG,a4
                        jsr     display_message
                        jsr     get_character_data
                        cmp.b   #$D0,$11(a5)
                        bne     lbC01E728
                        move.b  #$C7,$11(a5)
lbC01E728:              jsr     lbC01BB68
                        jmp     lbC01E678(pc)

lbC01E732:              jsr     get_character_data
                        move.b  #$D0,$11(a5)
                        lea     YUCKHORRIBLE_MSG,a4
                        jsr     display_message
                        jsr     lbC021124
                        move.w  #4,d0
                        jsr     play_sound
                        jsr     lbC021124
                        jsr     lbC01BB68
                        jmp     lbC01E678(pc)

lbC01E76A:              jsr     get_character_data
                        move.b  CHARACTER_MAXPOINTS_HI(a5),CHARACTER_HITPOINTS_HI(a5)
                        move.b  CHARACTER_MAXPOINTS_LO(a5),CHARACTER_HITPOINTS_LO(a5)
                        lea     HOWWONDERFUL_MSG,a4
                        jsr     display_message
                        jsr     lbC01BB68
                        jmp     lbC01E678(pc)

lbC01E792:              lea     ARGHBLAHYUK_MSG,a4
                        jsr     display_message
                        jsr     get_character_data
                        move.b  #$25,d0
                        jsr     lbC01B8C2
                        jsr     lbC018DEC
                        jsr     lbC021124
                        move.w  #4,d0
                        jsr     play_sound
                        jsr     lbC021124
                        jsr     lbC018DEC
                        jsr     lbC01BB68
                        jmp     lbC01E678(pc)

lbC01E7DA:              lea     STRANGEWIND_MSG,a4
                        jsr     display_message
                        clr.b   LIGHT_COUNTER(a3)
                        jmp     lbC01E08C(pc)

lbC01E7EE:              jsr     tile_dungeon_get_action_datas
                        clr.b   (a5)
                        move.b  PARTY_CHARACTERS(a3),d0
                        jsr     lbC0212F8
                        move.b  d0,CURRENT_CHARACTER(a3)
                        jsr     get_character_incapacitated_state
                        beq     lbC01E812
                        jmp     lbC01E08C(pc)

lbC01E812:              move.b  CHARACTER_FOOD_HI(a5),d0
                        bne     lbC01E826
                        jsr     lbC01C004
                        jmp     lbC01E834

lbC01E826:              move.b  #1,d3
                        move.w  #0,ccr
                        sbcd    d3,d0
                        move.b  d0,CHARACTER_FOOD_HI(a5)
lbC01E834:
                        move.w  #17,d0
                        jsr     play_sound
                        lea     GREMLINS_MSG,a4
                        jsr     display_message
                        jsr     lbC01BB68
                        jmp     lbC01E08C(pc)

lbC01E854:              jsr     reset_music
                        move.l  #'BRND',d0
                        lea     lbL022420,a0
                        jsr     file_load_cache
                        jsr     display_coded_map_page
                        move.b  #10,d0
                        jsr     start_new_music
                        move.b  #24,TEXT_Y(a3)
                        move.b  #24,TEXT_X(a3)
                        lea     AREDHOTRODINT_MSG,a4
                        jsr     display_message
                        clr.b   $4C(a3)
                        jsr     input_select_character
                        move.w  d0,-(sp)
                        move.b  #1,$4C(a3)
                        move.w  (sp)+,d0
                        tst.b   d0
                        bne     lbC01E8C8
lbC01E8AE:              jsr     lbC026A74
                        jsr     lbC01BB68
                        move.b  #4,d0
                        jsr     start_new_music
                        jmp     lbC01E08C(pc)

lbC01E8C8:              move.b  PARTY_X(a3),d0
                        and.b   #3,d0
                        addq.b  #4,d0
                        move.b  d0,d2
                        jsr     lbC01E922
                        move.b  d0,INPUT_NUMBER(a3)
                        jsr     get_character_data
                        move.b  14(a5),d0
                        or.b    INPUT_NUMBER(a3),d0
                        move.b  d0,14(a5)
                        jsr     lbC021124
                        move.w  #4,d0
                        jsr     play_sound
                        jsr     lbC021124
                        move.b  #$50,d0
                        jsr     lbC01B8C2
                        lea     ITLEFTAMARK_MSG,a4
                        jsr     display_message
                        jmp     lbC01E8AE(pc)

lbC01E922:              move.b  #1,d0
                        tst.b   d2
                        beq     lbC01E936
lbC01E92E:              add.b   d0,d0
                        subq.b  #1,d2
                        bne.s   lbC01E92E
lbC01E936:              rts

tile_dungeon_get_action_datas:  
                        lea     current_map,a5
                        clr.w   d0
                        move.b  PARTY_ACTION_Y(a3),d0
                        lsl.w   #4,d0
                        add.b   PARTY_ACTION_X(a3),d0
                        add.w   d0,a5
                        clr.w   d0
                        move.b  CURRENT_DUNGEON_LEVEL(a3),d0
                        lsl.w   #8,d0
                        add.w   d0,a5
                        move.b  (a5),d0
                        rts

reset_music:            clr.b   lbB028FDC
                        clr.b   current_music_number
wait_music_change:      tst.b   flag_music_end
                        bne.s   wait_music_change
                        rts

start_new_music:        move.b  d0,current_music_number
                        move.b  d0,lbB028FDC
                        rts

                        SECTION EXODUS01E9CC,DATA

ITSDARK_MSG:            dc.b    "IT'S DARK!",$ff,0
lbW01E9D8:              dc.w    $1D00
Pass_MSG1:              dc.b    "Pass",$ff,0
Advance_MSG:            dc.b    "Advance",$ff,0
Retreat_MSG:            dc.b    "Retreat",$ff,0
Turnright_MSG:          dc.b    "Turn right",$ff,0
Turnleft_MSG:           dc.b    "Turn left",$ff,0
INVALIDMOVE_MSG1:       dc.b    "INVALID MOVE!",$ff,0
INVALIDCMD_MSG:         dc.b    "INVALID CMD!",$ff,0
NOTADNGCMD_MSG:         dc.b    "NOT A DNG CMD!",$ff,0
Descend_MSG0:           dc.b    "Descend",$ff,0
Klimb_MSG:              dc.b    "Klimb",$ff,0
PeeratagemWHO_MSG:      dc.b    "Peer at a gem!",$ff
                        dc.b    "WHOSE GEM-",0
MISTYWRITING_MSG:       dc.b    "MISTY WRITING:",$ff,0
YOUSEEAVISION_MSG:      dc.b    $ff, "YOU SEE A VISION",$ff
                        dc.b    "OF THE TIME LORD",$ff
                        dc.b    "  HE TELLS YOU",$ff
                        dc.b    " THE ONE WAY IS",$ff
                        dc.b    "   LOVE, SOL,",$ff
                        dc.b    " MOONS & DEATH,",$ff
                        dc.b    " ALL ELSE FAILS.",0
ARGHATRAP_MSG:          dc.b    "ARGH!! A TRAP!!",$ff,0
EVADED_MSG:             dc.b    "EVADED!!",$ff,0
AFOUNTAINWHOW_MSG:      dc.b    $ff, "A FOUNTAIN, WHO",$ff
                        dc.b    "WILL DRINK? - ",0
CANT_MSG:               dc.b    "CAN'T!",$ff,0
AHTHATSNICE_MSG:        dc.b    "AH! THAT'S NICE!",$ff,0
YUCKHORRIBLE_MSG:       dc.b    "YUCK! HORRIBLE!",$ff,0
HOWWONDERFUL_MSG:       dc.b    "HOW WONDERFUL!",$ff,0
ARGHBLAHYUK_MSG:        dc.b    "ARGH! BLAH! YUK!",$ff,0
STRANGEWIND_MSG:        dc.b    "STRANGE WIND!",$ff,0
GREMLINS_MSG:           dc.b    "GREMLINS!",$ff,0,0
AREDHOTRODINT_MSG:      dc.b    "A RED HOT ROD",$ff
                        dc.b    "IN THE WALL, WHO",$ff
                        dc.b    "WILL TOUCH?-",0
ITLEFTAMARK_MSG:        dc.b    "IT LEFT A MARK!",$ff,0
descriptions_messages:  dc.b    0
                        dc.b    "WATER",0
                        dc.b    "GRASS",0
                        dc.b    "BRUSH",0
                        dc.b    "FOREST",0
                        dc.b    "MOUNTAINS",0
                        dc.b    "DUNGEON",0
                        dc.b    "TOWNE",0
                        dc.b    "CASTLE",0
                        dc.b    "FLOOR",0
                        dc.b    "CHEST",0
                        dc.b    "HORSE",0
                        dc.b    "FRIGATE",0
                        dc.b    "WHIRLPOOL",0
                        dc.b    "SERPENT",0
                        dc.b    "MAN-O-WAR",0
                        dc.b    "PIRATE",0
                        dc.b    "MERCHANT",0
                        dc.b    "JESTER",0
                        dc.b    "GUARD",0
                        dc.b    "LORD BRITISH",0
                        dc.b    "FIGHTER",0
                        dc.b    "CLERIC",0
                        dc.b    "WIZARD",0
                        dc.b    "THIEF",0
                        dc.b    "ORC",0
                        dc.b    "SKELETON",0
                        dc.b    "GIANT",0
                        dc.b    "DAEMON",0
                        dc.b    "PINCHER",0
                        dc.b    "DRAGON",0
                        dc.b    "BALRON",0
                        dc.b    "EXODUS",0
                        dc.b    "FORCE FIELD",0
                        dc.b    "LAVA",0
                        dc.b    "MOON GATE",0
                        dc.b    "WALL",0
                        dc.b    "VOID",0
                        dc.b    "WALL",0
                        dc.b    "A",0
                        dc.b    "B",0
                        dc.b    "C",0
                        dc.b    "D",0
                        dc.b    "E",0
                        dc.b    "F",0
                        dc.b    "G",0
                        dc.b    "H",0
                        dc.b    "I",0
                        dc.b    "U",0
                        dc.b    "Y",0
                        dc.b    "L",0
                        dc.b    "M",0
                        dc.b    "N",0
                        dc.b    "O",0
                        dc.b    "P",0
                        dc.b    "W",0
                        dc.b    "R",0
                        dc.b    "S",0
                        dc.b    "T",0
                        dc.b    "SNAKE",0
                        dc.b    "SNAKE",0
                        dc.b    "MAGIC",0
                        dc.b    "FIRE",0
                        dc.b    "SHRINE",0
                        dc.b    "RANGER",0

                        ; weapons (16)
HAND_MSG:               dc.b    "HAND",0
                        dc.b    "DAGGER",0
                        dc.b    "MACE",0
                        dc.b    "SLING",0
                        dc.b    "AXE",0
                        dc.b    "BOW",0
                        dc.b    "SWORD",0
                        dc.b    "2H-SWD",0
                        dc.b    "+2 AXE",0
                        dc.b    "+2 BOW",0
                        dc.b    "+2 SWD",0
                        dc.b    "GLOVES",0
                        dc.b    "+4 AXE",0
                        dc.b    "+4 BOW",0
                        dc.b    "+4 SWD",0
                        dc.b    "EXOTIC",0

                        ; armours (8)
SKIN_MSG:               dc.b    "SKIN",0
                        dc.b    "CLOTH",0
                        dc.b    "LEATHER",0
                        dc.b    "CHAIN",0
                        dc.b    "PLATE",0
                        dc.b    "+2 CHAIN",0
                        dc.b    "+2 PLATE",0
                        dc.b    "EXOTIC",0
                        
                        ; wizards spells (16)
                        dc.b    "REPOND",0              ; A
                        dc.b    "MITTAR",0              ; B
                        dc.b    "LORUM",0               ; C
                        dc.b    "DOR ACRON",0           ; D
                        dc.b    "SUR ACRON",0           ; E
                        dc.b    "FULGAR",0              ; F
                        dc.b    "DAG ACRON",0           ; G
                        dc.b    "MENTAR",0              ; H
                        dc.b    "DAG LORUM",0           ; I
                        dc.b    "FAL DIVI",0            ; J
                        dc.b    "NOXUM",0               ; K
                        dc.b    "DECORP",0              ; L
                        dc.b    "ALTAIR",0              ; M
                        dc.b    "DAG MENTAR",0          ; N
                        dc.b    "NECORP",0              ; O
                        dc.b    0                       ; P
                        
                        ; cleric spells (16)
                        dc.b    "PONTORI",0             ; A
                        dc.b    "APPAR UNEM",0          ; B
                        dc.b    "SANCTU",0              ; C
                        dc.b    "LUMINAE",0             ; D
                        dc.b    "REC SU",0              ; E
                        dc.b    "REC DU",0              ; F
                        dc.b    "LIB REC",0             ; G
                        dc.b    "ALCORT",0              ; H
                        dc.b    "SEQUITU",0             ; I
                        dc.b    "SOMINAE",0             ; J
                        dc.b    "SANCTU MANI",0         ; K
                        dc.b    "VIEDA",0               ; L
                        dc.b    "EXCUUN",0              ; M
                        dc.b    "SURMANDUM",0           ; N
                        dc.b    "ZXKUQYB",0             ; O
                        dc.b    "ANJU SERMANI",0        ; P
                        
                        ; monsters
                        dc.b    "BRIGAND",0
                        dc.b    "CUTPURSE",0
                        dc.b    "GOBLIN",0
                        dc.b    "TROLL",0
                        dc.b    "GHOUL",0
                        dc.b    "ZOMBIE",0
                        dc.b    "GOLEM",0
                        dc.b    "TITAN",0
                        dc.b    "GARGOYLE",0
                        dc.b    "MANE",0
                        dc.b    "SNATCH",0
                        dc.b    "BRADLE",0
                        dc.b    "GRIFFON",0
                        dc.b    "WYVERN",0
                        dc.b    "ORCUS",0
                        dc.b    "DEVIL",0
                        dc.b    0

                        dc.b    0,0

                        SECTION EXODUS01EF34,CODE

; ----------------------------------------
; The demo screen
demo_screen:            jsr     draw_demo_screen
                        jsr     lbC021F0E
                        bra     lbC01EF4A

lbC01EF44:              jsr     lbC027F40
lbC01EF4A:              bsr     lbC01EF56
                        cmp.w   #1,d0
                        beq.s   lbC01EF44
                        rts

lbC01EF56:              move.w  #4,lbW01FD90
                        bsr     lbC01FB22
lbC01EF62:
                        bsr     lbC01F972
                        cmp.w   #2,d0
                        bne.s   lbC01EF62
                        move.w  lbW01FD92,d0
                        cmp.w   #1,d0
                        bne     lbC01EF7C
                        rts

lbC01EF7C:              cmp.w   #2,d0
                        bne     lbC01EF8A
                        bsr     lbC01EFD0
                        bra.s   lbC01EF56

lbC01EF8A:              cmp.w   #3,d0
                        bne.s   lbC01EF62
                        bset    #0,lbB01FD68
                        bne     lbC01EFAC
                        move.l  #'PRTY',d0
                        lea     $4A(a3),a0
                        jsr     file_load_cache
lbC01EFAC:
                        tst.b   PARTY_CHARACTERS(a3)
                        bne     lbC01EFBE
                        move.w  #3,d0
                        bsr     display_requester
                        bra.s   lbC01EF62

lbC01EFBE:              move.w  #9,d0
                        bsr     display_requester
                        tst.w   d0
                        beq.s   lbC01EF62
                        move.w  #3,d0
                        rts

lbC01EFD0:              bset    #0,lbB01FD68
                        bne     lbC01EFEC
                        move.l  #'PRTY',d0
                        lea     $4A(a3),a0
                        jsr     file_load_cache
lbC01EFEC:              bset    #1,lbB01FD68
                        bne     lbC01F00A

                        move.l  #'ROST',d0
                        lea     lbB0237D0,a0
                        jsr     file_load
lbC01F00A:              tst.b   PARTY_CHARACTERS(a3)
                        beq     lbC01F036
                        bset    #2,lbB01FD68
                        bne     lbC01F036
                        move.l  #'PLRS',d0
                        lea     characters_data,a0
                        jsr     file_load_cache
                        jsr     lbC021202
lbC01F036:              clr.w   lbW01FD6A
lbC01F03C:              move.w  #2,lbW01FD90
                        bsr     lbC01FB22
lbC01F048:              nop

lbC01F04A:              bsr     lbC01F972
                        cmp.w   #2,d0
                        bne.s   lbC01F04A
                        move.w  lbW01FD92,d0
                        cmp.w   #1,d0
                        bne     lbC01F0C0
                        move.w  #$39,d0
                        bsr     lbC01F36E
                        bne     lbC01F078
                        bra     lbC01F0D4

lbC01F072:              bsr     lbC01F38A
                        beq.s   lbC01F03C

lbC01F078:              bsr     lbC01F328
                        beq     lbC01F0A6
                        jsr     blit_front_to_back_buffer
                        jsr     lbC01AED0

lbC01F08C:              bsr     lbC01F972
                        cmp.w   #2,d0
                        bne.s   lbC01F08C
                        jsr     blit_back_to_front_buffer
                        move.w  #3,lbW01FD90
                        bra.s   lbC01F072

lbC01F0A6:              moveq   #1,d0
                        bsr     display_requester
                        bra.s   lbC01F072

lbC01F0C0:              cmp.w   #2,d0
                        bne     lbC01F118
                        move.w  #$3A,d0
                        bsr     lbC01F36E
                        bne     lbC01F0E6
lbC01F0D4:              jsr     blit_back_to_front_buffer
                        move.w  #2,lbW01FD90
                        bra     lbC01F048

lbC01F0E6:              bsr     lbC01F328
                        beq     lbC01F0F8
                        moveq   #2,d0
                        bsr     display_requester
                        bra     lbC01F10E

lbC01F0F8:              bsr     lbC01F4A2
                        jsr     blit_back_to_front_buffer
                        bsr     lbC01F39C
                        move.w  #3,lbW01FD90
lbC01F10E:
                        bsr     lbC01F38A
                        bne.s   lbC01F0E6
                        bra     lbC01F03C

lbC01F118:              cmp.w   #3,d0
                        bne     lbC01F1CE
                        move.w  #$3F,d0
                        bsr     lbC01F36E
lbC01F128:              beq.s   lbC01F0D4
                        bsr     lbC01F328
                        bne     lbC01F138
lbC01F132:              bsr     lbC01F38A
                        bra.s   lbC01F128

lbC01F138:              tst.b   $10(a5)
                        bne     lbC01F172
                        cmp.b   #4,PARTY_CHARACTERS(a3)
                        bcc     lbC01F1C2
                        not.b   $10(a5)
                        addq.b  #1,PARTY_CHARACTERS(a3)
                        moveq   #0,d0
                        move.b  PARTY_CHARACTERS(a3),d0
                        subq.w  #1,d0
                        lea     $50(a3),a0
                        move.b  lbB01FD99,0(a0,d0.w)
                        bsr     lbC01F33A
lbC01F16C:              bsr     lbC01F39C
                        bra.s   lbC01F132

lbC01F172:              not.b   $10(a5)
                        subq.b  #1,PARTY_CHARACTERS(a3)
                        lea     $50(a3),a0
                        moveq   #0,d0
                        move.w  lbB01FD98,d1
lbC01F188:              cmp.b   0(a0,d0.w),d1
                        beq     lbC01F194
                        addq.w  #1,d0
                        bra.s   lbC01F188

lbC01F194:              cmp.b   #3,d0
                        bcc     lbC01F1A6
                        move.b  1(a0,d0.w),0(a0,d0.w)
                        addq.w  #1,d0
                        bra.s   lbC01F194

lbC01F1A6:              move.b  #$FF,0(a0,d0.w)
                        tst.b   PARTY_CHARACTERS(a3)
                        beq     lbC01F1B8
                        bsr     lbC01F33A
lbC01F1B8:              move.w  #1,lbW01FD6A
                        bra.s   lbC01F16C

lbC01F1C2:              move.w  #4,d0
                        bsr     display_requester
                        bra     lbC01F132

lbC01F1CE:              cmp.w   #4,d0
                        bne     lbC01F278
                        move.w  #8,d0
                        bsr     display_requester
                        tst.w   d0
                        beq     lbC01F04A
                        tst.b   PARTY_CHARACTERS(a3)
                        beq     lbC01F04A
                        lea     current_map,a0
                        move.l  #'SSAV',d0
                        jsr     file_load_cache
                        move.l  #'SOSA',0(a1,d1.w)
                        lea     lbL0225D0,a0
                        move.l  #'SSMV',d0
                        jsr     file_load_cache
                        move.l  #'SOSM',0(a1,d1.w)
                        lea     lbL0225D0,a0
                        move.l  #'SOSM',d0
                        jsr     lbC0180E6
                        lea     current_map,a0
                        move.l  #'SOSA',d0
                        jsr     lbC0180E6
                        moveq   #-1,d0
                        move.b  d0,$50(a3)
                        move.b  d0,$51(a3)
                        move.b  d0,$52(a3)
                        move.b  d0,$53(a3)
                        clr.b   PARTY_CHARACTERS(a3)

                        lea     lbB0237D0,a0
                        moveq   #20-1,d0
lbC01F264:              move.w  d0,d1
                        lsl.w   #6,d1
                        clr.b   $10(a0,d1.w)
                        dbra    d0,lbC01F264

                        bsr     lbC01F33A
                        bra     lbC01F04A

lbC01F278:              cmp.w   #5,d0
                        bne     lbC01F2E0
                        move.w  #$3B,d0
                        bsr     lbC01F36E
lbC01F288:              beq     lbC01F0D4
                        bsr     lbC01F328
                        bne     lbC01F29A
lbC01F294:              bsr     lbC01F38A
                        bra.s   lbC01F288

lbC01F29A:              tst.b   $10(a5)
                        beq     lbC01F2AA
                        moveq   #0,d0
                        bsr     display_requester
                        bra.s   lbC01F294

lbC01F2AA:              move.w  #7,d0
                        bsr     display_requester
                        tst.w   d0
                        beq.s   lbC01F294
                        clr.b   0(a5)
                        bsr     lbC01F39C
                        move.w  #1,lbW01FD6A
                        move.w  #3,lbW01FD90
lbC01F2D0:
                        bsr     lbC01F38A
                        beq     lbC01F03C
                        bsr     lbC01F328
                        bne.s   lbC01F29A
                        bra.s   lbC01F2D0

lbC01F2E0:              cmp.w   #6,d0
                        bne     lbC01F04A
                        tst.w   lbW01FD6A
                        beq     lbC01F326
                        move.l  #'PRTY',d0
                        lea     $4A(a3),a0
                        jsr     lbC0180E6
                        move.l  #'PLRS',d0
                        lea     characters_data,a0
                        jsr     lbC0180E6
                        move.l  #'ROST',d0
                        lea     lbB0237D0,a0
                        jsr     file_save
lbC01F326:
                        rts

lbC01F328:              subq.w  #1,d0
                        move.w  d0,lbB01FD98
                        bsr     lbC01F8D4
                        tst.b   0(a5)
                        rts

lbC01F33A:              clr.b   $4C(a3)
                        move.b  #$7E,$4A(a3)
                        move.b  #$FF,$4F(a3)
                        move.b  #$2C,PARTY_X_SAVE(a3)
                        move.b  #$14,PARTY_Y_SAVE(a3)
                        tst.b   PARTY_CHARACTERS(a3)
                        beq     lbC01F364
                        jsr     lbC0211D8
lbC01F364:
                        move.w  #1,lbW01FD6A
                        rts

lbC01F36E:              move.w  #3,lbW01FD90
                        move.w  d0,lbW020348
                        jsr     blit_front_to_back_buffer
                        bsr     lbC01FB22
                        bsr     lbC01F39C
lbC01F38A:              bsr     lbC01F972
                        cmp.w   #2,d0
                        bne.s   lbC01F38A
                        move.w  lbW01FD92,d0
                        rts

lbC01F39C:              move.b  #1,CURRENT_CHARACTER(a3)
                        move.b  #1,CURRENT_CHARACTER_SAVE(a3)
lbC01F3A8:              move.b  #1,TEXT_X(a3)
                        move.b  CURRENT_CHARACTER(a3),TEXT_Y(a3)
                        add.b   #12,TEXT_Y(a3)
                        cmp.b   #$17,TEXT_Y(a3)
                        bcs     lbC01F3D0
                        sub.b   #10,TEXT_Y(a3)
                        move.b  #$14,TEXT_X(a3)
lbC01F3D0:
                        move.l  #$FF,letter_mask
                        move.b  CURRENT_CHARACTER_SAVE(a3),d0
                        jsr     lbC01B0F6
                        addq.b  #1,TEXT_X(a3)
                        move.b  CURRENT_CHARACTER(a3),d0
                        subq.b  #1,d0
                        bsr     lbC01F8D4
                        tst.b   (a5)
                        bne     lbC01F416
                        move.l  #$FF00FFFF,letter_mask
                        lea     Empty_MSG,a4
                        bsr     lbC01F756
                        bra     lbC01F480

lbC01F416:              move.b  #$2D,d0
                        tst.b   $10(a5)
                        beq     lbC01F426
                        move.b  #$2A,d0
lbC01F426:
                        bsr     lbC01F8BC
                        move.l  #$FFFF000,letter_mask
                        addq.b  #1,TEXT_X(a3)
                        move.b  $18(a5),d0
                        bsr     lbC01F8BC
                        move.b  $16(a5),d0
                        bsr     lbC01F8BC
                        move.b  $17(a5),d0
                        bsr     lbC01F8BC
                        move.b  $11(a5),d0
                        bsr     lbC01F8BC
                        move.b  #$20,d0
                        bsr     lbC01F8BC
                        move.l  #$FF00,letter_mask
                        moveq   #7,d7
                        lea     (a5),a4
lbC01F472:
                        move.b  (a4)+,d0
                        beq     lbC01F480
                        bsr     lbC01F8BC
                        dbra    d7,lbC01F472
lbC01F480:
                        moveq   #1,d3
                        move.b  CURRENT_CHARACTER_SAVE(a3),d0
                        move.w  #0,ccr
                        abcd    d3,d0
                        move.b  d0,CURRENT_CHARACTER_SAVE(a3)
                        addq.b  #1,CURRENT_CHARACTER(a3)
                        cmp.b   #$15,CURRENT_CHARACTER(a3)
                        bne     lbC01F3A8
                        rts

lbC01F4A2:              clr.l   lbL020A8A
                        clr.w   lbW01FD90
                        lea     lbW020D30,a0
                        lea     lbL01FDA4,a1
                        bsr     lbC01F8E4
                        move.w  #$1E,lbW01FDA0
                        lea     lbL01FDA4,a5
                        jsr     blit_front_to_back_buffer
                        bsr     lbC01FB22
                        move.l  #$FF,letter_mask
                        lea     lbW020CE4,a4
                        jsr     lbC01F764
                        bsr     lbC01F732
                        clr.w   lbW01F8BA
                        clr.b   lbB01FD96
lbC01F4FC:
                        lea     lbL020A8A,a0
                        lea     lbW020090,a4
                        cmp.b   #15,lbB01FD96
                        bne     lbC01F524
                        tst.l   (a0)
                        bne     lbC01F534
                        move.l  a4,(a0)
                        bsr     lbC01FBDC
                        bra     lbC01F534

lbC01F524:              tst.l   (a0)
                        beq     lbC01F534
                        clr.l   (a0)
                        lea     4(a4),a0
                        bsr     lbC01FC7E
lbC01F534:
                        subq.w  #1,lbW01FDA2
                        bpl     lbC01F54E
                        move.w  #3,lbW01FDA2
                        jsr     scroll_input_char
lbC01F54E:
                        move.w  lbW01F8BA,d0
                        add.w   #14,d0
                        move.b  d0,TEXT_X(a3)
                        move.b  #1,TEXT_Y(a3)
                        move.l  #$FF00FFFF,letter_mask
                        clr.b   d0
                        bsr     lbC01F8BC
                        bsr     lbC01F972
                        cmp.w   #1,d0
                        bne     lbC01F586
                        bsr     lbC01F812
                        bra     lbC01F4FC

lbC01F586:              cmp.w   #2,d0
                        bne     lbC01F4FC
                        move.w  lbW01FD92,d1
                        cmp.w   #5,d1
                        bcc     lbC01F5B6
                        move.b  #3,TEXT_Y(a3)
                        move.w  #$16,d0
                        bset    #0,lbB01FD96
                        bsr     lbC01F6B2
                        bra     lbC01F4FC

lbC01F5B6:              cmp.w   #8,d1
                        bcc     lbC01F5D8
                        move.b  #2,TEXT_Y(a3)
                        move.w  #$18,d0
                        bset    #1,lbB01FD96
                        bsr     lbC01F6B2
                        bra     lbC01F4FC

lbC01F5D8:              cmp.w   #$13,d1
                        bcc     lbC01F600
                        move.b  #4,TEXT_Y(a3)
                        move.w  #$17,d0
                        bset    #2,lbB01FD96
                        bsr     lbC01F6B2
                        jsr     lbC028162
                        bra     lbC01F4FC

lbC01F600:              cmp.w   #$3C,d1
                        bcc     lbC01F678
                        move.w  #$12,d2
                        move.w  #$32,d0
lbC01F610:
                        move.b  0(a5,d2.w),d4
                        move.b  #1,d3
                        cmp.w   d0,d1
                        beq     lbC01F660
                        cmp.b   #$25,d4
                        bcc     lbC01F4FC
                        tst.w   lbW01FDA0
                        beq     lbC01F4FC
                        subq.w  #1,lbW01FDA0
                        move.w  #0,ccr
                        abcd    d3,d4
lbC01F63E:
                        move.b  d4,0(a5,d2.w)
                        move.l  #$FF,letter_mask
                        lea     lbW020CE4,a4
                        jsr     lbC01AF6A
                        bsr     lbC01F732
                        bra     lbC01F4FC

lbC01F660:              cmp.b   #5,d4
                        bls     lbC01F4FC
                        addq.w  #1,lbW01FDA0
                        move.w  #0,ccr
                        sbcd    d3,d4
                        bra.s   lbC01F63E

lbC01F678:              cmp.w   #$46,d1
                        bcc     lbC01F68A
                        move.w  #$13,d2
                        move.w  #$3C,d0
                        bra.s   lbC01F610

lbC01F68A:              cmp.w   #$50,d1
                        bcc     lbC01F69E
                        move.w  #$14,d2
                        move.w  #$46,d0
                        bra     lbC01F610

lbC01F69E:              cmp.w   #$5A,d1
                        bcc     lbC01F6FC
                        move.w  #$15,d2
                        move.w  #$50,d0
                        bra     lbC01F610

lbC01F6B2:              move.w  d0,-(sp)
                        move.l  #$FF,letter_mask
                        move.b  #14,TEXT_X(a3)
                        move.w  #12,d6

lbC01F6C8:              move.b  #$20,d0
                        bsr     lbC01F8BC
                        dbra    d6,lbC01F6C8
                        move.b  #14,TEXT_X(a3)
                        move.w  lbW01FD94,d0
                        lea     CreateCharact_MSG,a4
                        bsr     get_text_address
                        move.w  (sp)+,d0
                        move.b  (a4),0(a5,d0.w)
                        or.b    #$80,0(a5,d0.w)
                        bsr     lbC01F756
                        rts

lbC01F6FC:              cmp.w   #$64,d1
                        bne     lbC01F728
                        lea     lbL01FDA4,a0
                        lea     lbB0237D0,a1
                        move.w  lbB01FD98,d0
                        lsl.w   #6,d0
                        add.w   d0,a1
                        bsr     lbC01F8E4
                        move.w  #1,lbW01FD6A
                        rts

lbC01F728:              cmp.w   #$65,d1
                        bne     lbC01F4FC
                        rts

lbC01F732:              move.w  lbW01FDA0,d0
                        jsr     lbC01B8A4
                        move.b  #17,TEXT_X(a3)
                        move.b  #9,TEXT_Y(a3)
                        jsr     lbC01B0F6
                        rts

get_text_address_and_display:
                        bsr     get_text_address
lbC01F756:              move.b  (a4)+,d0
                        beq     lbC01F762
                        bsr     lbC01F8BC
                        bra.s   lbC01F756

lbC01F762:              rts

lbC01F764:              move.l  a4,-(sp)
                        jsr     lbC028162
                        move.b  #14,TEXT_X(a3)
                        move.b  #1,TEXT_Y(a3)
                        lea     0(a5),a4
                        bsr.s   lbC01F756
                        move.b  #14,TEXT_X(a3)
                        addq.b  #1,TEXT_Y(a3)
                        move.b  $18(a5),d1
                        move.w  #17,d0                  ; male
                        lea     lbB01F80A,a0
                        bsr     lbC01F7E2
                        move.b  #14,TEXT_X(a3)
                        addq.b  #1,TEXT_Y(a3)
                        move.b  $16(a5),d1
                        move.w  #12,d0                  ; human
                        lea     lbB01F80D,a0
                        bsr     lbC01F7E2
                        move.b  #14,TEXT_X(a3)
                        addq.b  #1,TEXT_Y(a3)
                        move.b  $17(a5),d1
                        move.w  #20,d0                  ; fighter
                        lea     lbB026930,a0
                        bsr     lbC01F7E2
                        move.l  (sp)+,a4
                        jsr     lbC01AF6A
                        rts

lbC01F7E2:              lea     CreateCharact_MSG,a4
                        tst.b   d1
                        bne     lbC01F7F8
                        lea     Select_MSG,a4
                        bra     lbC01F756

lbC01F7F8:              cmp.b   (a0)+,d1
                        beq     lbC01F804
                        addq.w  #1,d0
                        bra.s   lbC01F7F8

lbC01F804:              jsr     get_text_address_and_display(pc)
                        rts

lbB01F80A:              dc.b    $CD,$C6,$CF
lbB01F80D:              dc.b    $C8,$C5,$C4,$C2,$C6

lbC01F812:              move.l  #$FF00FFFF,letter_mask
                        move.b  #1,TEXT_Y(a3)
                        move.w  lbW01F8BA,d0
                        add.w   #14,d0
                        move.b  d0,TEXT_X(a3)
                        move.w  lbW01FD9E,d0
                        cmp.b   #$8D,d0
                        beq     lbC01F882
                        cmp.b   #$88,d0
                        bne     lbC01F884
                        tst.w   lbW01F8BA
                        beq     lbC01F882
                        move.b  #$20,d0
                        bsr     lbC01F8BC
                        subq.b  #2,TEXT_X(a3)
                        subq.w  #1,lbW01F8BA
                        move.w  lbW01F8BA,d1
                        bne     lbC01F878
                        bclr    #3,lbB01FD96
lbC01F878:              clr.b   0(a5,d1.w)
                        moveq   #0,d0
                        bsr     lbC01F8BC
lbC01F882:              rts

lbC01F884:              cmp.w   #12,lbW01F8BA
                        beq.s   lbC01F882
                        cmp.b   #$BF,d0
                        bcs.s   lbC01F882
                        cmp.b   #$DB,d0
                        bcc.s   lbC01F882
                        move.w  lbW01F8BA,d1
                        move.b  d0,0(a5,d1.w)
                        bsr     lbC01F8BC
                        addq.w  #1,lbW01F8BA
                        bset    #3,lbB01FD96
                        rts

lbW01F8BA:              dc.w    0

lbC01F8BC:              move.l  Bitmap_PtrA,a1
                        and.b   #$7F,d0
                        jsr     draw_letter
                        addq.b  #1,TEXT_X(a3)
                        rts

lbC01F8D4:              lea     lbB0237D0,a5
                        and.w   #$FF,d0
                        lsl.w   #6,d0
                        add.w   d0,a5
                        rts

lbC01F8E4:              move.w  #16-1,d0
lbC01F8E8:              move.l  (a0)+,(a1)+
                        dbra    d0,lbC01F8E8
                        rts

; ----------------------------------------
; Requesters
; d0 = message number
display_requester:      move.w  lbW01FD90,-(sp)
                        lea     requester_coords_table,a0
                        cmp.w   #7,d0
                        bcc     lbC01F90E
                        move.l  #lbW0204A4,(a0)+                ; single ok button
                        bra     lbC01F91A

lbC01F90E:              move.l  #lbW0204BA,(a0)+                ; ok / cancel buttons
                        move.l  #lbW0204D0,(a0)+

lbC01F91A:              clr.l   (a0)
                        add.w   d0,d0
                        add.w   #71,d0
                        move.w  d0,idx_requester_msg_line1
                        addq.w  #1,d0
                        move.w  d0,idx_requester_msg_line2
                        move.w  #5,lbW01FD90
                        jsr     lbC021F0E
                        bsr     lbC01FB22

lbC01F942:              bsr     lbC01F972
                        cmp.w   #2,d0
                        bne.s   lbC01F942
                        move.w  (sp)+,lbW01FD90
                        ; Restore the background behind the requester
                        ; (which is probably displayed on BitmapA only)
                        move.l  Bitmap_PtrB,a0
                        move.l  Bitmap_PtrA,a1
                        lea     restore_requester_bkgnd_blitdat,a2
                        jsr     blit_bitmap
                        move.w  lbW01FD92,d0
                        rts

lbC01F972:              move.l  key_ascii,d0
                        btst    #7,d0
                        bne     lbC01F9A4
                        move.w  mouse_x,save_mouse_x
                        move.w  mouse_y,save_mouse_y
                        btst    #6,$BFE001
                        beq     requester_lbutton_pressed
                        bra     lbC01F9DE

lbC01F9A4:              and.l   #$7F,d0
                        move.l  d0,key_ascii
                        jsr     translate_key
                        beq     lbC01F9DE
                        cmp.b   #$61,d0
                        bcs     lbC01F9CE
                        cmp.b   #$7A,d0
                        bhi     lbC01F9CE
                        and.b   #$DF,d0

lbC01F9CE:              or.b    #$80,d0
                        move.w  d0,lbW01FD9E
                        move.w  #1,d0
                        rts

lbC01F9DE:              clr.w   d0
                        rts

requester_lbutton_pressed:
                        lea     lbL020C72,a0
                        move.w  lbW01FD90,d0
                        lsl.w   #2,d0
                        move.l  0(a0,d0.w),a0               ; lbL020C4E
                        lea     12(a0),a1
                        move.w  save_mouse_x,d4
                        move.w  save_mouse_y,d5
                        clr.w   d0
lbC01FA08:              move.l  (a1)+,a0                    ; lbW0204E6 / lbW0204F0 / lbW0204A4 / lbW0204BA / lbW0204D0 / 0                 
                        cmp.l   #0,a0
                        beq.s   lbC01F9DE
                        cmp.l   #-1,a0
                        beq     lbC01FA4E
                        move.w  (a0),d1
                        and.w   #$FF,d1
                        cmp.w   #2,d1
                        beq     lbC01FA4E
                        move.w  2(a0),d2
                        cmp.w   #3,d1
                        beq     lbC01FA40
                        move.w  20(a0),lbW01FD94
lbC01FA40:              lea     4(a0),a0
                        jsr     lbC021DC2
                        beq     lbC01FA54
lbC01FA4E:              addq.w  #1,d0
                        bra.s   lbC01FA08

lbC01FA54:              move.l  a0,a4
                        move.w  d2,lbW01FD92
                        move.w  #-1,lbW01FD9C
lbC01FA64:              bsr     lbC01FAD8

lbC01FA68:              move.w  mouse_x,save_mouse_x
                        move.w  mouse_y,save_mouse_y
                        move.w  save_mouse_x,d4
                        move.w  save_mouse_y,d5
                        move.l  a4,a0
                        btst    #6,$BFE001
                        beq     lbC01FAB2
                        jsr     lbC021DC2
                        beq     lbC01FAA8
                        bsr     lbC01FAC0
                        bra     lbC01F9DE

; Clicked gadget ?
lbC01FAA8:              bsr     lbC01FAC0
                        move.w  #2,d0
                        rts

lbC01FAB2:              jsr     lbC021DC2
                        beq.s   lbC01FA64
                        bsr     lbC01FAC0
                        bra.s   lbC01FA68

lbC01FAC0:              tst.w   lbW01FD9C
                        bne     lbC01FAD6
                        move.w  #-1,lbW01FD9C
lbC01FAD2:              bsr     lbC01FAE8
lbC01FAD6:              rts

lbC01FAD8:              tst.w   lbW01FD9C
                        beq.s   lbC01FAD6
                        clr.w   lbW01FD9C
                        bra.s   lbC01FAD2

lbC01FAE8:              move.l  Bitmap_PtrA,a0
                        moveq   #-1,d0
lbC01FAF0:              move.l  d0,-(sp)
                        move.l  a4,a1
                        lea     lbL01FD88,a2
                        movem.w (a1)+,d0-d3
                        addq.w  #1,d0
                        addq.w  #1,d1
                        subq.w  #1,d2
                        subq.w  #1,d3
                        movem.w d0-d3,-(a2)
                        move.l  (a2),8(a2)
                        move.l  a0,a1
                        move.l  (sp)+,d0
                        jsr     lbC027E9E
                        rts

lbC01FB22:              lea     lbL020C72,a0
                        move.w  lbW01FD90,d0
                        lsl.w   #2,d0
                        move.l  (a0,d0.w),a0
                        move.w  10(a0),lbW01FD9A
                        move.w  (a0),d0
                        bsr     lbC01FB60
                        lea     12(a0),a0
lbC01FB48:              move.l  (a0)+,d0
                        beq     lbC01FB5E
                        cmp.l   #-1,d0
                        beq.s   lbC01FB48
                        move.l  d0,a4
                        bsr     lbC01FBDC
                        bra.s   lbC01FB48

lbC01FB5E:              rts

lbC01FB60:              move.l  a0,a4
                        move.w  d0,-(sp)
                        btst    #1,d0
                        beq     lbC01FB74
                        lea     2(a4),a0
                        bsr     lbC01FC7E
lbC01FB74:              move.w  (sp)+,d0
                        btst    #0,d0
                        beq     lbC01FBAA
                        move.w  #15,d0
                        lea     2(a4),a0
                        bsr     lbC01FCA6
                        move.w  #1,d0
                        bsr     lbC01FBAE
                        move.w  #15,d0
                        bsr     lbC01FCB0
                        move.w  #2,d0
                        bsr     lbC01FBAE
                        move.w  #15,d0
                        bsr     lbC01FCB0
lbC01FBAA:              move.l  a4,a0
                        rts

lbC01FBAE:              lea     lbL01FD6C,a0
                        add.w   d0,(a0)
                        add.w   d0,2(a0)
                        sub.w   d0,4(a0)
                        add.w   d0,6(a0)
                        sub.w   d0,8(a0)
                        sub.w   d0,10(a0)
                        add.w   d0,12(a0)
                        sub.w   d0,14(a0)
                        move.l  (a0),$10(a0)
                        rts

lbC01FBDC:              move.l  a0,-(sp)
                        move.w  (a4),d0
                        and.w   #$FF,d0
                        cmp.w   #1,d0
                        beq     lbC01FC7C
                        cmp.w   #2,d0
                        beq     lbC01FC26
                        cmp.w   #3,d0
                        beq     lbC01FC64
                        move.w  12(a4),d0
                        lea     4(a4),a0
                        bsr     lbC01FCA6
                        move.w  (a4),d0
                        and.w   #$FF00,d0
                        beq     lbC01FC22
                        move.w  #$FFFE,d0
                        bsr.s   lbC01FBAE
                        bsr     lbC01FCB0
lbC01FC22:              add.w   #12,a4
lbC01FC26:              addq.l  #2,a4
                        move.b  (a4),TEXT_X(a3)
                        move.b  1(a4),TEXT_Y(a3)
                        move.l  2(a4),letter_mask
                        move.w  6(a4),d0
                        lea     CreateCharact_MSG,a4
                        bsr     get_text_address

lbC01FC4A:              move.b  (a4)+,d0
                        beq     lbC01FC64
                        move.l  Bitmap_PtrA,a1
                        jsr     draw_letter
                        addq.b  #1,TEXT_X(a3)
                        bra.s   lbC01FC4A

lbC01FC64:              move.l  (sp)+,a0
                        rts

get_text_address:       tst.w   d0
                        beq     lbC01FC7A
                        subq.w  #1,d0
lbC01FC72:              tst.b   (a4)+
                        bne.s   lbC01FC72
                        dbra    d0,lbC01FC72
lbC01FC7A:              rts

lbC01FC7C:              rts

lbC01FC7E:              lea     lbL01FD80,a2
                        move.l  (a0),(a2)
                        move.l  4(a0),4(a2)
                        move.l  (a0),8(a2)
                        move.l  4(a0),12(a2)
                        move.l  Bitmap_PtrA,a0
                        move.l  a0,a1
                        jsr     lbC027EA8
                        rts

lbC01FCA6:              ext.l   d0
                        bsr     draw_setapen
                        bsr     lbC01FCDA
lbC01FCB0:              lea     lbL01FD6C,a0
                        movem.w (a0),d0/d1
                        ;move.w 2(a0),d1
                        bsr     draw_move
                        lea     lbL01FD70,a0
                        move.w  #4-1,d7
lbC01FCCC:              movem.w (a0)+,d0/d1
                        ;move.w (a0)+,d1
                        bsr     draw_draw
                        dbra    d7,lbC01FCCC
                        rts

lbC01FCDA:              lea     lbL01FD6C,a1
                        move.l  (a0),(a1)
                        move.w  4(a0),4(a1)
                        move.w  2(a0),6(a1)
                        move.w  4(a0),8(a1)
                        move.w  6(a0),10(a1)
                        move.w  (a0),12(a1)
                        move.l  6(a0),14(a1)
                        move.l  (a0),$10(a1)
                        rts

draw_draw:              move.l  a0,-(sp)
                        ext.l   d1
                        move.l  d1,-(sp)
                        ext.l   d0
                        move.l  d0,-(sp)
                        pea     Rastport
                        jsr     _Draw
                        add.w   #12,sp
                        move.l  (sp)+,a0
                        rts

draw_move:              ext.l   d1
                        move.l  d1,-(sp)
                        ext.l   d0
                        move.l  d0,-(sp)
                        pea     Rastport
                        jsr     _Move
                        add.w   #12,sp
                        rts

draw_setapen:           move.l  a0,-(sp)
                        move.l  d0,-(sp)
                        pea     Rastport
                        jsr     _SetAPen
                        addq.l  #8,sp
                        move.l  (sp)+,a0
                        rts

                        SECTION EXODUS01FD68,BSS

lbB01FD68:              ds.b    2
lbW01FD6A:              ds.w    1
lbL01FD6C:              ds.l    1
lbL01FD70:              ds.l    4
lbL01FD80:              ds.l    2
lbL01FD88:              ds.l    2
lbW01FD90:              ds.w    1
lbW01FD92:              ds.w    1
lbW01FD94:              ds.w    1
lbB01FD96:              ds.b    2
lbB01FD98:              ds.b    1
lbB01FD99:              ds.b    1
lbW01FD9A:              ds.w    1
lbW01FD9C:              ds.w    1
lbW01FD9E:              ds.w    1
lbW01FDA0:              ds.w    1
lbW01FDA2:              ds.w    1
lbL01FDA4:              ds.l    $10

                        SECTION EXODUS01FDE4,DATA

lbW01FDE4:              dc.w    2,$50B,0,$FF00,0
lbW01FDEE:              dc.w    2,$901,$FFF,$F000,1
lbW01FDF8:              dc.w    2,$A02,$FFF,$F000,2
lbW01FE02:              dc.w    2,$903,$FFF,$F000,3
lbW01FE0C:              dc.w    2,$304,$FFF,$F000,4
lbW01FE16:              dc.w    2,$505,$FFF,$F000,5
lbW01FE20:              dc.w    2,$406,$FFF,$F000,6
lbW01FE2A:              dc.w    2,$107,$FFF,$F000,7
lbW01FE34:              dc.w    2,$708,$FFF,$F000,8
lbW01FE3E:              dc.w    2,$709,$FFF,$F000,$3E
lbW01FE48:              dc.w    2,$50D,0,$FF00,9
lbW01FE52:              dc.w    2,$120D,0,$FF00,10
lbW01FE5C:              dc.w    2,$1B01,0,$FF00,11
lbW01FE66:              dc.w    2,$F05,0,$FF00,$30
lbW01FE70:              dc.w    2,$F06,0,$FF00,$30
lbW01FE7A:              dc.w    2,$F07,0,$FF00,$30
lbW01FE84:              dc.w    2,$F08,0,$FF00,$30
lbW01FE8E:              dc.w    3,$32,$77,$27,$87,$31
lbW01FE9A:              dc.w    3,$33,$98,$27,$A8,$31
lbW01FEA6:              dc.w    3,$3C,$77,$2F,$87,$39
lbW01FEB2:              dc.w    3,$3D,$98,$2F,$A8,$39
lbW01FEBE:              dc.w    3,$46,$77,$37,$87,$41
lbW01FECA:              dc.w    3,$47,$98,$37,$A8,$41
lbW01FED6:              dc.w    3,$50,$77,$3F,$87,$49
lbW01FEE2:              dc.w    3,$51,$98,$3F,$A8,$49
lbW01FEEE:              dc.w    0,0,$1E,$76,$5A,$82,15,$50F,$FF00,$FFFF,12
lbW01FF04:              dc.w    0,1,$1E,$86,$5A,$92,15,$611,$FF00,$FFFF,13
lbW01FF1A:              dc.w    0,2,$1E,$96,$5A,$A2,15,$513,$FF00,$FFFF,14
lbW01FF30:              dc.w    0,3,$1E,$A6,$5A,$B2,15,$515,$FF00,$FFFF,15
lbW01FF46:              dc.w    0,4,$1E,$B6,$5A,$C2,15,$517,$FF00,$FFFF,$10
lbW01FF5C:              dc.w    0,5,$86,$76,$BA,$82,15,$120F,$FF00,$FFFF,$11
lbW01FF72:              dc.w    0,6,$86,$86,$BA,$92,15,$1111,$FF00,$FFFF,$12
lbW01FF88:              dc.w    0,7,$86,$96,$BA,$A2,15,$1113,$FF00,$FFFF,$13
lbW01FF9E:              dc.w    0,8,$DE,$16,$13A,$22,15,$1E03,$FF00,$FFFF,$14
lbW01FFB4:              dc.w    0,9,$DE,$26,$13A,$32,15,$1E05,$FF00,$FFFF,$15
lbW01FFCA:              dc.w    0,10,$DE,$36,$13A,$42,15,$1E07,$FF00,$FFFF,$16
lbW01FFE0:              dc.w    0,11,$DE,$46,$13A,$52,15,$1F09,$FF00,$FFFF,$17
lbW01FFF6:              dc.w    0,12,$DE,$56,$13A,$62,15,$1E0B,$FF00,$FFFF,$18
lbW02000C:              dc.w    0,13,$DE,$66,$13A,$72,15,$1D0D,$FF00,$FFFF,$19
lbW020022:              dc.w    0,14,$DE,$76,$13A,$82,15,$1F0F,$FF00,$FFFF,$1A
lbW020038:              dc.w    0,15,$DE,$86,$13A,$92,15,$1C11,$FF00,$FFFF,$1B
lbW02004E:              dc.w    0,$10,$DE,$96,$13A,$A2,15,$1F13,$FF00,$FFFF,$1C
lbW020064:              dc.w    0,$11,$DE,$A6,$13A,$B2,15,$1D15,$FF00,$FFFF,$1D
lbW02007A:              dc.w    0,$12,$DE,$B6,$13A,$C2,15,$1E17,$FF00,$FFFF,$1E
lbW020090:              dc.w    0,$64,$66,$AF,$96,$B9,15,$E16,$FF00,$FFFF,$3C
lbW0200A6:              dc.w    $300,$65,$9F,$AF,$D0,$B9,15,$1416,$FF00,$FFFF,$3D
lbW0200BC:              dc.w    $300,0,$104,12,$12C,$1C,7,$2102,$FF00,$FFFF,$2F
lbW0200D2:              dc.w    2,$1D06,0,$FF00,$1F
lbW0200DC:              dc.w    2,$1206,$FFF,$F000,$20
lbW0200E6:              dc.w    2,$1207,$FFF,$F000,$21
lbW0200F0:              dc.w    2,$1408,$FFF,$F000,$22
lbW0200FA:              dc.w    2,$1409,$FFF,$F000,$23
lbW020104:              dc.w    2,$110A,$FFF,$F000,$24
lbW02010E:              dc.w    2,$110B,$FFF,$F000,$25
lbW020118:              dc.w    2,$10A,$FFF,$F000,$27
lbW020122:              dc.w    2,$40B,$FFF,$F000,$28
lbW02012C:              dc.w    2,$10C,$FFF,$F000,$29
lbW020136:              dc.w    2,$60D,$FFF,$F000,$2A
lbW020140:              dc.w    2,$60E,$FFF,$F000,$2B
lbW02014A:              dc.w    2,$210,0,$FF00,$26
lbW020154:              dc.w    2,$110E,0,$FF00,$2C
lbW02015E:              dc.w    2,$113,0,$FF00,$2D
lbW020168:              dc.w    2,$713,0,$FF00,$2E
lbW020172:              dc.w    3,10,$6E,$7F,$DA,$89
lbW02017E:              dc.w    3,11,$6E,$87,$DA,$91
lbW02018A:              dc.w    3,12,$6E,$8F,$DA,$99
lbW020196:              dc.w    3,13,$6E,$97,$DA,$A1
lbW0201A2:              dc.w    3,14,$6E,$9F,$DA,$A9
lbW0201AE:              dc.w    3,15,$6E,$A7,$DA,$B1
lbW0201BA:              dc.w    3,$10,$6E,$AF,$DA,$B9
lbW0201C6:              dc.w    3,$11,$6E,$B7,$DA,$C1
lbW0201D2:              dc.w    3,$14,$DE,$3F,$13A,$49
lbW0201DE:              dc.w    3,$15,$DE,$47,$13A,$51
lbW0201EA:              dc.w    3,$16,$DE,$4F,$13A,$59
lbW0201F6:              dc.w    3,$17,$DE,$57,$13A,$61
lbW020202:              dc.w    3,$18,$DE,$5F,$13A,$69
lbW02020E:              dc.w    3,$19,$DE,$67,$13A,$71
lbW02021A:              dc.w    3,$1A,$DE,$6F,$13A,$79
lbW020226:              dc.w    3,$1B,$DE,$77,$13A,$81
lbW020232:              dc.w    3,$1C,$DE,$7F,$13A,$89
lbW02023E:              dc.w    3,$1D,$DE,$87,$13A,$91
lbW02024A:              dc.w    3,$1E,$DE,$8F,$13A,$99
lbW020256:              dc.w    3,$1F,$DE,$97,$13A,$A1
lbW020262:              dc.w    3,$20,$DE,$9F,$13A,$A9
lbW02026E:              dc.w    3,$21,$DE,$A7,$13A,$B1
lbW02027A:              dc.w    3,$22,$DE,$AF,$13A,$B9
lbW020286:              dc.w    3,$23,$DE,$B7,$13A,$C1
lbW020292:              dc.w    $300,0,$10E,$57,$132,$61,15,$220B,$FF00,$FFFF,$2F
lbW0202A8:              dc.w    2,$B0B,0,$FF00,$31
lbW0202B2:              dc.w    2,$100D,0,$FF,$32
lbW0202BC:              dc.w    2,$A0F,$FFF,$F000,$33
lbW0202C6:              dc.w    2,$B10,$FFF,$F000,$34
lbW0202D0:              dc.w    2,$E11,$FFF,$F000,$35
lbW0202DA:              dc.w    2,$C12,$FFF,$F000,$36
lbW0202E4:              dc.w    2,$A13,$FFF,$F000,$37
lbW0202EE:              dc.w    2,$F15,$FFF,$F000,$38
lbW0202F8:              dc.w    3,1,$4F,$77,$EF,$81
lbW020304:              dc.w    3,2,$57,$7F,$E7,$89
lbW020310:              dc.w    3,3,$6F,$87,$CF,$91
lbW02031C:              dc.w    3,4,$5F,$8F,$DF,$99
lbW020328:              dc.w    3,5,$4F,$97,$F7,$A1
lbW020334:              dc.w    3,6,$77,$A7,$C7,$B1
lbW020340:              dc.w    2,$30B,0,$FF00
lbW020348:              dc.w    $39
lbW02034A:              dc.w    3,1,8,$67,$97,$71
lbW020356:              dc.w    3,2,8,$6F,$97,$79
lbW020362:              dc.w    3,3,8,$77,$97,$81
lbW02036E:              dc.w    3,4,8,$7F,$97,$89
lbW02037A:              dc.w    3,5,8,$87,$97,$91
lbW020386:              dc.w    3,6,8,$8F,$97,$99
lbW020392:              dc.w    3,7,8,$97,$97,$A1
lbW02039E:              dc.w    3,8,8,$9F,$97,$A9
lbW0203AA:              dc.w    3,9,8,$A7,$97,$B1
lbW0203B6:              dc.w    3,10,8,$AF,$97,$B9
lbW0203C2:              dc.w    3,11,$9F,$67,$12F,$71
lbW0203CE:              dc.w    3,12,$9F,$6F,$12F,$79
lbW0203DA:              dc.w    3,13,$9F,$77,$12F,$81
lbW0203E6:              dc.w    3,14,$9F,$7F,$12F,$89
lbW0203F2:              dc.w    3,15,$9F,$87,$12F,$91
lbW0203FE:              dc.w    3,$10,$9F,$8F,$12F,$99
lbW02040A:              dc.w    3,$11,$9F,$97,$12F,$A1
lbW020416:              dc.w    3,$12,$9F,$9F,$12F,$A9
lbW020422:              dc.w    3,$13,$9F,$A7,$12F,$B1
lbW02042E:              dc.w    3,$14,$9F,$AF,$12F,$B9
lbW02043A:              dc.w    2,$70C,0,$FF00,$40
lbW020444:              dc.w    2,$70D,0,$FF00,$41
lbW02044E:              dc.w    2,$1010,0,$FF,$32
lbW020458:              dc.w    2,$B12,$FFF,$F000,$42
lbW020462:              dc.w    2,$C13,$FFF,$F000,$43
lbW02046C:              dc.w    2,$D14,$FFF,$F000,$44
lbW020476:              dc.w    2,$617,$FF00,$FFFF,$45
lbW020480:              dc.w    3,1,$57,$8F,$E7,$99
lbW02048C:              dc.w    3,2,$5F,$97,$DF,$A1
lbW020498:              dc.w    3,3,$67,$9F,$D7,$A9
lbW0204A4:              dc.w    $300,0,$88,$3E,$B8,$4A,1,$1308,$FF00,$FFFF,$46
lbW0204BA:              dc.w    $300,0,$A8,$3E,$E8,$4A,15,$1608,$FF00,$FFFF,$3D
lbW0204D0:              dc.w    $300,1,$58,$3E,$88,$4A,15,$D08,0,$FF,$46
lbW0204E6:              dc.w    2,$903,$FF00,$FFFF
idx_requester_msg_line1:
                        dc.w    $47
lbW0204F0:              dc.w    2,$905,$FF00,$FFFF
idx_requester_msg_line2:
                        dc.w    $48

CreateCharact_MSG:      dc.b    "-Create Character-",0
                        dc.b    "Name:",0
                        dc.b    "Sex:",0
                        dc.b    "Race:",0
                        dc.b    "Profession:",0
                        dc.b    "Strength:",0
                        dc.b    "Dexterity:",0
                        dc.b    "Intelligence:",0
                        dc.b    "Wisdom:",0
                        dc.b    "-Race-",0
                        dc.b    "-Sex-",0
                        dc.b    "-Profession-",0
                        dc.b    "Human",0
                        dc.b    "Elf",0
                        dc.b    "Dwarf",0
                        dc.b    "Bobbit",0
                        dc.b    "Fuzzy",0
                        dc.b    "Male",0
                        dc.b    "Female",0
                        dc.b    "Other",0
                        dc.b    "Fighter",0
                        dc.b    "Cleric",0
                        dc.b    "Wizard",0
                        dc.b    "Thief",0
                        dc.b    "Paladin",0
                        dc.b    "Barbarian",0
                        dc.b    "Lark",0
                        dc.b    "Illusionist",0
                        dc.b    "Druid",0
                        dc.b    "Alchemist",0
                        dc.b    "Ranger",0
                        dc.b    "-Weapons-",0
                        dc.b    "Gold:",0
                        dc.b    "Food:",0
                        dc.b    "Keys:",0
                        dc.b    "Gems:",0
                        dc.b    "Torches:",0
                        dc.b    "Powders:",0
                        dc.b    "-Status-",0
                        dc.b    "Hit Points:",0
                        dc.b    "Maximum:",0
                        dc.b    "Experience:",0
                        dc.b    "Level:",0
                        dc.b    "Magic:",0
                        dc.b    "-Armour-",0
                        dc.b    "Cards-",0
                        dc.b    "Marks",0
                        dc.b    "Exit",0
                        dc.b    $7F,"~05|}",0
                        dc.b    "PARTY ORGANIZATION",0
                        dc.b    "OPTIONS:",0
                        dc.b    "Examine the register",0
                        dc.b    "Create a character",0
                        dc.b    "Form a Party",0
                        dc.b    "Disperse a Party",0
                        dc.b    "Terminate a character",0
                        dc.b    "Main  menu",0
                        dc.b    "Select character to EXAMINE ",0
                        dc.b    "Select character to CREATE",0
                        dc.b    "Select character to TERMINATE",0
                        dc.b    "Save",0
                        dc.b    "Cancel",0
                        dc.b    "Points:",0
                        dc.b    "Select/Deselect party (Max=4)",0
                        dc.b    "FROM THE DEPTHS OF HELL...",0
                        dc.b    "...HE COMES FOR VENGEANCE!",0
                        dc.b    "Return to the view",0
                        dc.b    "Organize a Party",0
                        dc.b    "Journey onward",0
                        dc.b    "(C)-1983,86 By Lord British",0
                        dc.b    "Ok",0
Requesters_Messages:
                        dc.b    "Thou Cannot Terminate",0
                        dc.b    "a Player With a Party!",0

                        dc.b    "Thou Cannot Examine a",0
                        dc.b    " Nonexistant Player!",0

                        dc.b    "  That Player Already",0
                        dc.b    "        Exists!",0

                        dc.b    "  Thou Cannot Journey",0
                        dc.b    "    Without a Party!",0

                        dc.b    "Thy Party Cannot Have",0
                        dc.b    "More Than (4) Players!",0

                        dc.b    "Reset Thy Machine When",0
                        dc.b    "Thy Disk Light is Off.",0

                        dc.b    "Problem with Thy Disk!",0
                        dc.b    "(Is It In Thy Drive?)",0

                        dc.b    "Dost Thou Really Want",0
                        dc.b    "to Terminate This One?",0

                        dc.b    "Dost Thou Really Want",0
                        dc.b    "to Disperse the Party?",0

                        dc.b    "Dost Thou Really Want",0
                        dc.b    " to Journey Onward?",0

lbL0209CA:              dc.w    $3,$0,$0,$13F,$C7,$FFFF
                        dc.l    lbW01FDE4
                        dc.l    lbW01FDEE
                        dc.l    lbW01FDF8
                        dc.l    lbW01FE02
                        dc.l    lbW01FE0C
                        dc.l    lbW01FE16
                        dc.l    lbW01FE20
                        dc.l    lbW01FE2A
                        dc.l    lbW01FE34
                        dc.l    lbW01FE3E
                        dc.l    lbW01FE48
                        dc.l    lbW01FE52
                        dc.l    lbW01FE5C
                        dc.l    lbW01FE8E
                        dc.l    lbW01FE9A
                        dc.l    lbW01FEA6
                        dc.l    lbW01FEB2
                        dc.l    lbW01FEBE
                        dc.l    lbW01FECA
                        dc.l    lbW01FED6
                        dc.l    lbW01FEE2
                        dc.l    lbW01FEEE
                        dc.l    lbW01FF04
                        dc.l    lbW01FF1A
                        dc.l    lbW01FF30
                        dc.l    lbW01FF46
                        dc.l    lbW01FF5C
                        dc.l    lbW01FF72
                        dc.l    lbW01FF88
                        dc.l    lbW01FF9E
                        dc.l    lbW01FFB4
                        dc.l    lbW01FFCA
                        dc.l    lbW01FFE0
                        dc.l    lbW01FFF6
                        dc.l    lbW02000C
                        dc.l    lbW020022
                        dc.l    lbW020038
                        dc.l    lbW02004E
                        dc.l    lbW020064
                        dc.l    lbW02007A
                        dc.l    lbW01FE66
                        dc.l    lbW01FE70
                        dc.l    lbW01FE7A
                        dc.l    lbW01FE84
                        dc.l    lbW0200A6

lbL020A8A:              dc.l    lbW020090,0
lbL020A92:              dc.w    $3,$0,$0,$13F,$C7,$FFFF
                        dc.l    lbW0200BC
                        dc.l    lbW01FDEE
                        dc.l    lbW01FDF8
                        dc.l    lbW01FE02
                        dc.l    lbW01FE0C
                        dc.l    lbW01FE16
                        dc.l    lbW01FE20
                        dc.l    lbW01FE2A
                        dc.l    lbW01FE34
                        dc.l    lbW0200D2
                        dc.l    lbW0200DC
                        dc.l    lbW0200E6
                        dc.l    lbW0200F0
                        dc.l    lbW0200FA
                        dc.l    lbW020104
                        dc.l    lbW02010E
                        dc.l    lbW020118
                        dc.l    lbW020122
                        dc.l    lbW02012C
                        dc.l    lbW020136
                        dc.l    lbW020140
                        dc.l    lbW02014A
                        dc.l    lbW020154
                        dc.l    lbW02015E
                        dc.l    lbW020168

lbL020B02:              dc.l    lbW020172
                        dc.l    lbW02017E
                        dc.l    lbW02018A
                        dc.l    lbW020196
                        dc.l    lbW0201A2
                        dc.l    lbW0201AE
                        dc.l    lbW0201BA
                        dc.l    lbW0201C6

lbL020B22:              dc.l    lbW0201D2
                        dc.l    lbW0201DE
                        dc.l    lbW0201EA
                        dc.l    lbW0201F6
                        dc.l    lbW020202
                        dc.l    lbW02020E
                        dc.l    lbW02021A
                        dc.l    lbW020226
                        dc.l    lbW020232
                        dc.l    lbW02023E
                        dc.l    lbW02024A
                        dc.l    lbW020256
                        dc.l    lbW020262
                        dc.l    lbW02026E
                        dc.l    lbW02027A
                        dc.l    lbW020286,0

lbL020B66:              dc.w    $2,$8,$50,$137,$BF,$FFFF
                        dc.l    lbW0202A8
                        dc.l    lbW0202B2
                        dc.l    lbW0202BC
                        dc.l    lbW0202C6
                        dc.l    lbW0202D0
                        dc.l    lbW0202DA
                        dc.l    lbW0202E4
                        dc.l    lbW0202EE
                        dc.l    lbW0202F8
                        dc.l    lbW020304
                        dc.l    lbW020310
                        dc.l    lbW02031C
                        dc.l    lbW020328
                        dc.l    lbW020334,0

lbL020BAE:              dc.w    $2,$8,$50,$137,$BF,$FFFF
                        dc.l    lbW020292
                        dc.l    lbW020340
                        dc.l    lbW02034A
                        dc.l    lbW020356
                        dc.l    lbW020362
                        dc.l    lbW02036E
                        dc.l    lbW02037A
                        dc.l    lbW020386
                        dc.l    lbW020392
                        dc.l    lbW02039E
                        dc.l    lbW0203AA
                        dc.l    lbW0203B6
                        dc.l    lbW0203C2
                        dc.l    lbW0203CE
                        dc.l    lbW0203DA
                        dc.l    lbW0203E6
                        dc.l    lbW0203F2
                        dc.l    lbW0203FE
                        dc.l    lbW02040A
                        dc.l    lbW020416
                        dc.l    lbW020422
                        dc.l    lbW02042E,0

lbL020C16:              dc.w    $2,$8,$50,$137,$BF,$FFFF
                        dc.l    lbW02043A
                        dc.l    lbW020444
                        dc.l    lbW02044E
                        dc.l    lbW020458
                        dc.l    lbW020462
                        dc.l    lbW02046C
                        dc.l    lbW020476
                        dc.l    lbW020480
                        dc.l    lbW02048C
                        dc.l    lbW020498,0

lbL020C4E:              dc.w    $3,$40,$A,$FF,$56,$FFFF
                        dc.l    lbW0204E6
                        dc.l    lbW0204F0

requester_coords_table: dc.l    lbW0204A4
                        dc.l    lbW0204BA
                        dc.l    lbW0204D0
                        dc.l    0

lbL020C72:              dc.l    lbL0209CA
                        dc.l    lbL020A92
                        dc.l    lbL020B66
                        dc.l    lbL020BAE
                        dc.l    lbL020C16
                        dc.l    lbL020C4E

restore_requester_bkgnd_blitdat:
                        dc.w    $40,10,$FF,$56,$40,10

lbW020C9A:              dc.w    9,2,$4C,$84,$4C,$84,$2C,$13E,$2C,2,$7C,$74,$7C
                        dc.w    $74,$6C,$DC,$6C,2,$94,$68,$94,$74,$6C,$74,$7C,$84
                        dc.w    $2C,$84,$6C,$68,$7C,$68,$C6,$DC,$2C,$DC,$C6
lbW020CE4:              dc.w    $12,1,$1105,$13,1,$1106,$14,1,$1107,$15,1,$1108
                        dc.w    $FFFF
lbW020CFE:              dc.w    $12,1,$E05,$13,1,$E06,$14,1,$E07,$15,1,$E08,$FFFF
Select_MSG:             dc.b    "Select",0
Empty_MSG:              dc.b    "- Empty -      ",0,0
lbW020D30:              dc.w    0,0,0,0,0,0,0,0
                        dc.w    $C7,$505,$505,0,0,$150,$150,0,$150,1,$5000,0,1,0
                        dc.w    0,0,1,0,0,0,0,0,0,0

                        SECTION EXODUS020D70,CODE

load_tiles:             lea     game_state,a3
                        lea     tiles_buffer,a0
                        move.l  #'ULTS',d0
                        bsr     file_load
                        rts

display_message:        move.b  (a4)+,d0
                        beq     lbC020DAE
                        cmp.b   #$FF,d0
                        beq     lbC020DA8
                        and.b   #$7F,d0
                        bsr     draw_letter_both_bitmap
                        addq.b  #1,TEXT_X(a3)
                        bra.s   display_message

lbC020DA8:              bsr     text_carriage_return
                        bra.s   display_message

lbC020DAE:              rts

draw_letter_both_bitmap:
                        move.w  d0,-(sp)
                        move.l  Bitmap_PtrA,a1
                        bsr     draw_letter
                        move.w  (sp)+,d0
                        move.l  Bitmap_PtrB,a1
                        bsr     draw_letter
                        rts

draw_letter:            movem.l d1/a0-a4,-(sp)
                        moveq   #0,d5
                        move.b  TEXT_Y(a3),d5
                        mulu    #320,d5
                        moveq   #0,d4
                        move.b  TEXT_X(a3),d4
                        add.w   d4,d5
                        lea     8(a1),a4
                        movem.l (a4)+,a0-a3
                        lea     font,a4
                        ext.w   d0
                        lsl.w   #3,d0
                        add.w   d0,a4
                        moveq   #7,d0
draw_letter_loop:       move.b  (a4)+,d3
                        move.b  letter_mask+3,d4
                        and.b   d3,d4
                        move.b  d4,0(a0,d5.w)
                        move.b  letter_mask+2,d4
                        and.b   d3,d4
                        move.b  d4,0(a1,d5.w)
                        move.b  letter_mask+1,d4
                        and.b   d3,d4
                        move.b  d4,0(a2,d5.w)
                        move.b  letter_mask,d4
                        and.b   d3,d4
                        move.b  d4,0(a3,d5.w)
                        add.w   #SCREEN_BYTES,d5
                        dbra    d0,draw_letter_loop
                        movem.l (sp)+,d1/a0-a4
                        rts

lbC020E36:              move.l  Bitmap_PtrB,a1
                        move.l  12(a1),a1
                        moveq   #0,d5
                        move.b  TEXT_Y(a3),d5
                        mulu    #320,d5
                        moveq   #0,d4
                        move.b  TEXT_X(a3),d4
                        add.w   d4,d5
                        add.w   d5,a1
                        lea     font,a0
                        ext.w   d0
                        lsl.w   #3,d0
                        add.w   d0,a0
                        moveq   #8-1,d0
lbC020E62:              move.b  (a0)+,(a1)
                        add.w   #SCREEN_BYTES,a1
                        dbra    d0,lbC020E62
                        rts

lbC020E6E:              jmp     get_text_address_and_display

text_carriage_return:   movem.l d0-d2/a0-a3,-(sp)
                        jsr     scroll_text
                        movem.l (sp)+,d0-d2/a0-a3
                        move.b  #$18,TEXT_X(a3)
                        move.b  #$18,TEXT_Y(a3)
                        rts

lbC020E90:              movem.l a2-a5,-(sp)
                        moveq   #0,d5
                        move.b  TEXT_Y(a3),d5
                        mulu    #320,d5
                        moveq   #0,d4
                        move.b  TEXT_X(a3),d4
                        add.w   d4,d5
                        lea     lbW0269FA,a5
                        cmp.b   #$1D,d0
                        beq     lbC020EBA
                        lea     lbB026A1A,a5
lbC020EBA:              move.l  a5,-(sp)
                        move.l  Bitmap_PtrA,a4
                        bsr     lbC020EE2
                        move.l  (sp)+,a5
                        move.l  Bitmap_PtrB,a4
                        bsr     lbC020EE2
                        movem.l (sp)+,a2-a5
                        addq.b  #1,TEXT_X(a3)
                        rts

lbC020EE2:              move.w  d5,-(sp)
                        lea     8(a4),a4
                        movem.l (a4)+,a0-a3
                        moveq   #8-1,d0
lbC020EEE:              move.b  (a5),0(a0,d5.w)
                        move.b  8(a5),0(a1,d5.w)
                        move.b  $10(a5),0(a2,d5.w)
                        move.b  $18(a5),0(a3,d5.w)
                        addq.w  #1,a5
                        add.w   #SCREEN_BYTES,d5
                        dbra    d0,lbC020EEE
                        move.w  (sp)+,d5
                        rts

draw_game_borders:      jsr     lbC02109A
                        lea     game_borders_blitdat,a4
                        moveq   #9-1,d7
lbC020F22:              move.l  Bitmap_PtrA,a0
                        move.l  Bitmap_PtrB,a1
                        move.l  a4,a2
                        jsr     blit_bitmap
                        add.w   #6*2,a4
                        dbra    d7,lbC020F22
                        jsr     blit_back_to_front_buffer
                        move.l  #$FF0FFFF,letter_mask
                        rts

game_borders_blitdat:   dc.w    0,0,$13F,7,0,0
                        dc.w    0,$C0,$B7,$C7,0,$C0
                        dc.w    0,8,7,$BF,0,8
                        dc.w    $138,8,$13F,$7F,$138,8
                        dc.w    $138,7,$13F,$C7,$B8,7
                        dc.w    $BF,$C0,$13F,$C7,$BF,$80
                        dc.w    $BF,$C0,$138,$C7,$BF,$60
                        dc.w    $BF,$C0,$138,$C7,$BF,$40
                        dc.w    $BF,$C0,$138,$C7,$BF,$20

lbC020FE0:              movem.l d1/a0-a4,-(sp)
                        move.l  Bitmap_PtrA,a4
                        bsr     lbC021006
                        bra     lbC020FF6

lbC020FF2:              movem.l d1/a0-a4,-(sp)
lbC020FF6:              move.l  Bitmap_PtrB,a4
                        bsr     lbC021006
                        movem.l (sp)+,d1/a0-a4
                        rts

lbC021006:              lea     8(a4),a4
                        movem.l (a4)+,a0-a3
                        move.w  #480,d0
                        move.w  #176-1,d1
lbC021016:              clr.b   1(a0,d0.w)
                        clr.b   1(a1,d0.w)
                        clr.b   1(a2,d0.w)
                        clr.b   1(a3,d0.w)
                        clr.l   2(a0,d0.w)
                        clr.l   2(a1,d0.w)
                        clr.l   2(a2,d0.w)
                        clr.l   2(a3,d0.w)
                        clr.l   6(a0,d0.w)
                        clr.l   6(a1,d0.w)
                        clr.l   6(a2,d0.w)
                        clr.l   6(a3,d0.w)
                        clr.l   10(a0,d0.w)
                        clr.l   10(a1,d0.w)
                        clr.l   10(a2,d0.w)
                        clr.l   10(a3,d0.w)
                        clr.l   14(a0,d0.w)
                        clr.l   14(a1,d0.w)
                        clr.l   14(a2,d0.w)
                        clr.l   14(a3,d0.w)
                        clr.l   18(a0,d0.w)
                        clr.l   18(a1,d0.w)
                        clr.l   18(a2,d0.w)
                        clr.l   18(a3,d0.w)
                        clr.b   22(a0,d0.w)
                        clr.b   22(a1,d0.w)
                        clr.b   22(a2,d0.w)
                        clr.b   22(a3,d0.w)
                        add.w   #SCREEN_BYTES,d0
                        dbra    d1,lbC021016
                        rts

lbC02109A:              move.l  Bitmap_PtrB,a0
                        bsr     screen_clear
                        rts

screen_clear:           movem.l d0/d1/a0/a1,-(sp)
                        clr.l   -(sp)
                        move.l  #SCREEN_BYTES*SCREEN_HEIGHT*SCREEN_BPP,-(sp)
                        move.l  8(a0),-(sp)
                        jsr     _BltClear
                        add.w   #12,sp
                        movem.l (sp)+,d0/d1/a0/a1
                        rts

lbC0210C6:              lea     lbW02117A,a0
                        clr.w   d0
                        move.b  CURRENT_CHARACTER(a3),d0
                        add.w   d0,d0
                        move.w  0(a0,d0.w),d0
                        move.w  d0,-(sp)
                        bsr     lbC0210F8
                        cmp.b   #1,$4C(a3)
                        bne     lbC0210F2
                        jsr     lbC026A74
                        bra     lbC0210F6

lbC0210F2:              bsr     display_decoded_map_page
lbC0210F6:              move.w  (sp)+,d0
lbC0210F8:              move.l  Bitmap_PtrB,a0
                        move.l  8(a0),a0
                        add.w   d0,a0
                        not.b   (a0)
                        not.b   $28(a0)
                        not.b   $50(a0)
                        not.b   $78(a0)
                        not.b   $A0(a0)
                        not.b   $C8(a0)
                        not.b   $F0(a0)
                        not.b   $118(a0)
                        rts

lbC021124:              clr.w   d0
                        move.b  CURRENT_CHARACTER(a3),d0
                        moveq   #1,d1
                        bra     lbC021132

lbC021130:              moveq   #12,d1
lbC021132:              lea     lbW021D9A,a0
                        lsl.w   #3,d0
                        add.w   d0,a0
                        move.w  d1,d0
                        bsr     lbC021162
                        cmp.b   #1,$4C(a3)
                        bne     lbC02115E
                        movem.l d0/d1/a0,-(sp)
                        jsr     lbC026A74
                        movem.l (sp)+,d0/d1/a0
                        bra     lbC021162

lbC02115E:              bsr     display_decoded_map_page
lbC021162:              movem.l d0-d7/a0-a5,-(sp)
                        move.l  a0,a4
                        move.l  Bitmap_PtrB,a0
                        jsr     lbC01FAF0
                        movem.l (sp)+,d0-d7/a0-a5
                        rts

lbW02117A:              dc.w    $1F,$51F,$A1F,$F1F

draw_digit:             add.b   #'0',d0
                        jsr     draw_letter_both_bitmap(pc)
                        addq.b  #1,TEXT_X(a3)
                        rts

draw_2_digits:          move.w  d0,-(sp)
                        and.b   #$F0,d0
                        lsr.b   #4,d0
                        jsr     lbC0211A6
                        move.w  (sp)+,d0
                        and.b   #$f,d0
lbC0211A6:              add.b   #'0',d0
                        jsr     draw_letter_both_bitmap(pc)
                        addq.b  #1,TEXT_X(a3)
                        rts

lbC0211B6:              lea     $50(a3),a0
                        moveq   #0,d0
                        move.b  0(a0,d1.w),d0
                        lsl.w   #6,d0
                        lea     lbB0237D0,a1
                        add.w   d0,a1
                        move.w  d1,d0
                        lsl.w   #6,d0
                        lea     characters_data,a0
                        add.w   d0,a0
                        rts

lbC0211D8:              lea     characters_data,a0
                        move.w  #$3F,d0
                        moveq   #0,d1
lbC0211E4:              move.l  d1,(a0)+
                        dbra    d0,lbC0211E4
                        move.b  PARTY_CHARACTERS(a3),d1
                        subq.w  #1,d1
lbC0211F0:              bsr.s   lbC0211B6
                        move.w  #16-1,d2
lbC0211F6:              move.l  (a1)+,(a0)+
                        dbra    d2,lbC0211F6
                        dbra    d1,lbC0211F0
                        rts

lbC021202:              moveq   #0,d1
                        move.b  PARTY_CHARACTERS(a3),d1
                        subq.w  #1,d1
lbC02120A:              bsr.s   lbC0211B6
                        move.w  #16-1,d2
lbC021210:              move.l  (a0)+,(a1)+
                        dbra    d2,lbC021210
                        dbra    d1,lbC02120A
                        rts

input_get_number:       clr.b   TEMP_NUMBER_W_HI(a3)
                        clr.b   TEMP_NUMBER_W_LO(a3)
loop_input_get_number:  jsr     input_wait_key
                        cmp.b   #$B0,d0
                        bcs.s   loop_input_get_number
                        cmp.b   #$BA,d0
                        bcc.s   loop_input_get_number
                        sub.b   #$B0,d0
                        move.b  d0,TEMP_NUMBER_W_HI(a3)
                        add.b   #'0',d0
                        jsr     draw_letter_both_bitmap(pc)
                        addq.b  #1,TEXT_X(a3)
lbC02124C:
                        jsr     input_wait_key
                        cmp.b   #$8D,d0
                        bne     lbC02126A
                        move.b  TEMP_NUMBER_W_HI(a3),TEMP_NUMBER_W_LO(a3)
                        clr.b   TEMP_NUMBER_W_HI(a3)
                        jmp     lbC0212CC

lbC02126A:              cmp.b   #$88,d0
                        bne     lbC021284
                        subq.b  #1,TEXT_X(a3)
                        move.b  #$20,d0
                        jsr     draw_letter_both_bitmap(pc)
                        jmp     input_get_number(pc)

lbC021284:              cmp.b   #$B0,d0
                        bcs.s   lbC02124C
                        cmp.b   #$BA,d0
                        bcc.s   lbC02124C
                        sub.b   #$B0,d0
                        move.b  d0,TEMP_NUMBER_W_LO(a3)
                        add.b   #'0',d0
                        jsr     draw_letter_both_bitmap(pc)
                        addq.b  #1,TEXT_X(a3)
lbC0212A6:
                        jsr     input_wait_key
                        cmp.b   #$8D,d0
                        beq     lbC0212CC
                        cmp.b   #$88,d0
                        bne.s   lbC0212A6
                        subq.b  #1,TEXT_X(a3)
                        move.b  #' ',d0
                        jsr     draw_letter_both_bitmap(pc)
                        jmp     lbC02124C(pc)

lbC0212CC:              clr.b   d0
                        move.b  TEMP_NUMBER_W_HI(a3),d1
                        beq     lbC0212E0
lbC0212D6:
                        add.b   #10,d0
                        subq.b  #1,d1
                        bne.s   lbC0212D6
lbC0212E0:
                        add.b   TEMP_NUMBER_W_LO(a3),d0
                        move.b  d0,$64(a3)
                        move.b  TEMP_NUMBER_W_HI(a3),d0
                        lsl.b   #4,d0
                        add.b   TEMP_NUMBER_W_LO(a3),d0
                        move.b  d0,INPUT_NUMBER(a3)
                        rts

lbC0212F8:              move.b  d0,$6C(a3)
                        jsr     lbC02167A
lbC021302:
                        cmp.b   $6C(a3),d0
                        bcs     lbC021312
                        sub.b   $6C(a3),d0
                        jmp     lbC021302(pc)

lbC021312:              move.b  d0,$6C(a3)
                        rts

lbC021318:              jsr     lbC0215EA
                        jsr     lbC02133E
                        jsr     lbC021386
                        jsr     lbC021464
                        jsr     lbC021496
                        jsr     refresh_map_view
                        rts

lbC02133E:              subq.b  #1,$20(a3)
                        bne     lbC021356
                        move.b  #2,$20(a3)
                        clr.w   d2
                        jsr     lbC021AAA

lbC021356:              move.w  #$1000,d2
                        jsr     lbC021AAA
                        subq.b  #1,$21(a3)
                        bne     lbC02137A
                        move.b  #2,$21(a3)
                        move.w  #$1080,d2
                        jsr     lbC021AAA
lbC02137A:              move.w  #$1100,d2
                        jsr     lbC021AAA
                        rts

lbC021386:              jsr     lbC02139A
                        jsr     lbC0213B2
                        jsr     lbC0213CC
                        rts

lbC02139A:              subq.b  #1,$22(a3)
                        bne     lbC0213B0
                        move.b  #3,$22(a3)
                        clr.w   d0
                        bsr     lbC0213EE
lbC0213B0:              rts

lbC0213B2:              subq.b  #1,$23(a3)
                        bne     lbC0213CA
                        move.b  #2,$23(a3)
                        move.w  #10,d0
                        bsr     lbC0213EE
lbC0213CA:              rts

lbC0213CC:              subq.b  #1,$24(a3)
                        bne     lbC0213EC
                        move.b  #1,$24(a3)
                        move.w  #$14,d0
                        bsr     lbC0213EE
                        move.w  #$1E,d0
                        bsr     lbC0213EE
lbC0213EC:              rts

lbC0213EE:              tst.b   CURRENT_WINDS(a3)
                        beq.s   lbC0213EC
                        lea     lbW02143C,a0
                        add.w   d0,a0
                        movem.w (a0)+,d0-d4
                        lea     lbL0240F0,a0
                        add.w   d0,a0
                        lea     0,a1
lbC02140C:              move.w  0(a0,d1.w),d5
                        move.w  d5,d6
                        and.w   d3,d5
                        and.w   d4,d6
                        move.w  0(a0,d2.w),d0
                        move.w  d0,d7
                        and.w   d3,d0
                        and.w   d4,d7
                        or.w    d5,d7
                        move.w  d7,0(a0,d2.w)
                        or.w    d0,d6
                        move.w  d6,0(a0,d1.w)
                        add.w   #$20,a0
                        addq.w  #1,a1
                        cmp.w   #4,a1
                        bne.s   lbC02140C
                        rts

lbW02143C:              dc.w    $380,0,4,$30,$FFCF
                        dc.w    $300,4,8,$300,$FCFF
                        dc.w    $580,2,6,$C0,$FF3F
                        dc.w    $780,2,6,$C0,$FF3F

lbC021464:              lea     lbL025050,a0
                        bsr     lbC021474
                        lea     lbL026850,a0
lbC021474:              rol.w   4(a0)
                        rol.w   36(a0)
                        rol.w   68(a0)
                        rol.w   100(a0)
                        rol.w   6(a0)
                        rol.w   38(a0)
                        rol.w   70(a0)
                        rol.w   102(a0)
                        rts

lbC021496:              lea     lbB0214D0,a2
                        move.w  #$78,d2
lbC0214A0:              move.b  0(a2,d2.w),d0
                        move.b  d0,d1
                        and.b   #$80,d1
                        and.b   #$7F,d0
                        subq.b  #1,d0
                        bpl     lbC0214C4
                        jsr     lbC02167A
                        and.b   #15,d0
                        bchg    #7,d1
lbC0214C4:              or.b    d1,d0
                        move.b  d0,0(a2,d2.w)
                        dbra    d2,lbC0214A0
                        rts

lbB0214D0:              dc.b    0,1,2,3,4,5,6,7,8,9,10
                        dc.b    0,1,2,3,4,5,6,7,8,9,10
                        dc.b    0,1,2,3,4,5,6,7,8,9,10
                        dc.b    0,1,2,3,4,5,6,7,8,9,10
                        dc.b    0,1,2,3,4,5,6,7,8,9,10
                        dc.b    0,1,2,3,4,5,6,7,8,9,10
                        dc.b    0,1,2,3,4,5,6,7,8,9,10
                        dc.b    0,1,2,3,4,5,6,7,8,9,10
                        dc.b    0,1,2,3,4,5,6,7,8,9,10
                        dc.b    0,1,2,3,4,5,6,7,8,9,10
                        dc.b    0,1,2,3,4,5,6,7,8,9,10
                        dc.b    0

display_message_at:     move.b  d1,TEXT_X(a3)
                        move.b  d2,TEXT_Y(a3)
                        jmp     display_message(pc)

get_character_data:     lea     characters_data,a5
                        clr.w   d0
                        move.b  CURRENT_CHARACTER(a3),d0
                        lsl.w   #6,d0                       ; * 64 bytes / character
                        add.w   d0,a5
                        rts

set_player_to_action:   move.b  PARTY_X(a3),PARTY_ACTION_X(a3)
                        move.b  PARTY_Y(a3),PARTY_ACTION_Y(a3)
                        jmp     tile_get_action_datas

; ----------------------------------------
; Retrieve the map datas near the player
tile_get_action_datas:  lea     current_map,a5
                        clr.w   d0
                        move.b  PARTY_ACTION_Y(a3),d0
                        lsl.w   #6,d0
                        add.w   d0,a5
                        clr.w   d0
                        move.b  PARTY_ACTION_X(a3),d0
                        add.w   d0,a5
                        move.b  (a5),d0
                        rts

lbC0215EA:              subq.b  #1,$25(a3)
                        bpl     lbC021618
                        move.b  #8,$25(a3)
lbC0215FA:              move.b  #9,d0
                        jsr     lbC0212F8(pc)
                        cmp.b   #5,d0
                        bcs     lbC021614
                        subq.b  #4,d0
                        cmp.b   CURRENT_WINDS(a3),d0
                        beq.s   lbC0215FA
lbC021614:              move.b  d0,CURRENT_WINDS(a3)
lbC021618:              move.b  TEXT_X(a3),d0
                        move.w  d0,-(sp)
                        move.b  #6,TEXT_X(a3)
                        move.b  #$18,TEXT_Y(a3)
                        move.b  #$1D,d0
                        bsr     lbC020E90
                        clr.w   d0
                        move.b  CURRENT_WINDS(a3),d0
                        lsl.w   #2,d0
                        lea     winds_table,a4
                        move.l  0(a4,d0.w),a4
                        jsr     display_message(pc)
                        move.b  #$1F,d0
                        bsr     lbC020E90
                        move.w  (sp)+,d0
                        move.b  d0,TEXT_X(a3)
                        rts

lbC021658:              cmp.b   CURRENT_WINDS(a3),d0
                        bne     lbC021670
lbC021660:              cmp.b   #$16,11(a3)
                        bne     lbC021676
                        move.b  #-1,d0
                        rts

lbC021670:              tst.b   CURRENT_WINDS(a3)
                        beq.s   lbC021660
lbC021676:              clr.b   d0
                        rts

lbC02167A:              movem.l d1/d2/a0,-(sp)
                        lea     lbB0221FC,a0
                        moveq   #14,d1
                        move.b  15(a0),d2
                        move.w  #0,ccr
lbC02168E:              move.b  0(a0,d1.w),d0
                        addx.b  d0,d2
                        move.b  d2,0(a0,d1.w)
                        subq.w  #1,d1
                        bpl.s   lbC02168E
                        moveq   #15,d1
lbC02169E:              addq.b  #1,0(a0,d1.w)
                        bne     lbC0216AC
                        subq.w  #1,d1
                        bpl.s   lbC02169E
lbC0216AC:              movem.l (sp)+,d1/d2/a0
                        move.b  lbB0221FC,d0
                        rts

lbC0216B8:              move.b  PARTY_CHARACTERS(a3),d3
                        move.b  PARTY_MOVES+3(a3),d0
                        move.w  #0,ccr
                        abcd    d3,d0
                        move.b  d0,PARTY_MOVES+3(a3)
                        clr.b   d3
                        move.b  PARTY_MOVES+2(a3),d0
                        abcd    d3,d0
                        move.b  d0,PARTY_MOVES+2(a3)
                        move.b  PARTY_MOVES+1(a3),d0
                        abcd    d3,d0
                        move.b  d0,PARTY_MOVES+1(a3)
                        move.b  PARTY_MOVES(a3),d0
                        abcd    d3,d0
                        bcc     lbC0216FA
                        move.b  #$99,d0                         ; BCD
                        move.b  d0,PARTY_MOVES+3(a3)
                        move.b  d0,PARTY_MOVES+2(a3)
                        move.b  d0,PARTY_MOVES+1(a3)
lbC0216FA:              move.b  d0,PARTY_MOVES(a3)
                        rts

input_wait_key:         move.b  d1,$1A(a3)
                        move.b  d2,$1B(a3)
lbC021708:              jsr     scroll_input_char
                        clr.b   d0
                        jsr     draw_letter_both_bitmap(pc)
                        jsr     lbC021B48
                        bpl.s   lbC021708
                        move.w  d0,-(sp)
                        move.b  #$20,d0
                        jsr     draw_letter_both_bitmap(pc)
                        move.w  (sp)+,d0
                        move.b  $1A(a3),d1
                        move.b  $1B(a3),d2
                        rts

scroll_input_char:      subq.b  #1,speed_scroll_input_char
                        bne     wait_scroll_input_char
                        move.b  #45,speed_scroll_input_char
                        lea     font,a0
                        move.b  7(a0),d0
                        move.b  6(a0),7(a0)
                        move.b  5(a0),6(a0)
                        move.b  4(a0),5(a0)
                        move.b  3(a0),4(a0)
                        move.b  2(a0),3(a0)
                        move.b  1(a0),2(a0)
                        move.b  (a0),1(a0)
                        move.b  d0,(a0)
wait_scroll_input_char:
                        rts

refresh_map_view:       movem.l d0-d7/a0-a5,-(sp)
                        lea     current_map,a0
                        lea     lbL0224D0,a1
                        clr.w   d0
                        move.b  PARTY_X(a3),d0
                        subq.w  #5,d0
                        clr.w   d1
                        move.b  PARTY_Y(a3),d1
                        subq.w  #5,d1
                        clr.w   d2
                        clr.w   d3
lbC0217A4:              tst.b   12(a3)
                        bmi     lbC0217CC
                        move.w  d2,d6
                        add.w   d0,d6
                        cmp.w   #$40,d6
                        bcc     lbC0217C4
                        move.w  d3,d7
                        add.w   d1,d7
                        cmp.w   #$40,d7
                        bcs     lbC0217DC
lbC0217C4:              move.b  12(a3),d6
                        bra     lbC0217EC

lbC0217CC:              move.w  d2,d6
                        add.w   d0,d6
                        and.w   #$3F,d6
                        move.w  d3,d7
                        add.w   d1,d7
                        and.w   #$3F,d7
lbC0217DC:              lsl.w   #6,d7
                        add.w   d6,d7
                        clr.w   d6
                        move.b  0(a0,d7.w),d6
                        asr.b   #1,d6
                        and.b   #$7E,d6
lbC0217EC:              move.b  d6,(a1)+
                        add.w   #1,d2
                        cmp.w   #11,d2
                        bne.s   lbC0217A4
                        clr.w   d2
                        add.w   #1,d3
                        cmp.w   #11,d3
                        bne.s   lbC0217A4
                        lea     lbL0224D0,a0
                        move.b  $3C(a0),6(a3)
                        move.b  $31(a0),7(a3)
                        move.b  $47(a0),8(a3)
                        move.b  $3D(a0),9(a3)
                        move.b  $3B(a0),10(a3)
                        move.b  11(a3),$3C(a0)
                        lea     lbL022420,a0
                        move.w  #$78,d0
                        move.w  #$48,d1
lbC02183C:              move.b  d1,(a0)+
                        dbra    d0,lbC02183C
                        lea     ascii_MSG20,a0
                        lea     ascii_MSG21,a1
                        lea     lbL0224D0,a2
                        lea     lbL022420,a4
                        move.w  #$78,d6
                        move.w  #10,d4
                        move.w  d4,d5
lbC021864:              move.w  d4,d2
                        move.w  d5,d3
                        move.w  d6,d7
lbC02186A:              move.w  d7,d0
                        add.b   0(a0,d2.w),d0
                        add.b   0(a1,d3.w),d0
                        cmp.b   #$3C,d0
                        beq     lbC0218AC
                        move.b  d0,d7
                        move.b  0(a2,d7.w),d0
                        cmp.b   #6,d0
                        beq     lbC0218B2
                        cmp.b   #8,d0
                        beq     lbC0218B2
                        cmp.b   #$46,d0
                        beq     lbC0218B2
                        cmp.b   #$48,d0
                        beq     lbC0218B2
                        add.b   0(a0,d2.w),d2
                        add.b   0(a0,d3.w),d3
                        bra.s   lbC02186A

lbC0218AC:              move.b  0(a2,d6.w),0(a4,d6.w)
lbC0218B2:              subq.w  #1,d6
                        dbra    d4,lbC021864
                        move.w  #10,d4
                        dbra    d5,lbC021864
                        lea     ascii_MSG22,a5
                        move.w  #$78,d6
                        move.w  #10,d4
                        move.w  d4,d5

lbC0218D2:              move.w  d4,d2
                        move.w  d5,d3
                        move.w  d6,d7
                        cmp.b   #$48,0(a4,d7.w)
                        bne     lbC021956
lbC0218E2:              move.b  0(a5,d2.w),d0
                        cmp.b   0(a5,d3.w),d0
                        bcs     lbC021916
                        bhi     lbC021908
                        move.w  d7,d0
                        add.b   0(a0,d2.w),d0
                        add.b   0(a1,d3.w),d0
                        add.b   0(a0,d2.w),d2
                        add.b   0(a0,d3.w),d3
                        bra     lbC021920

lbC021908:              move.w  d7,d0
                        add.b   0(a0,d2.w),d0
                        add.b   0(a0,d2.w),d2
                        bra     lbC021920

lbC021916:              move.w  d7,d0
                        add.b   0(a1,d3.w),d0
                        add.b   0(a0,d3.w),d3
lbC021920:              cmp.b   #$3C,d0
                        beq     lbC021950
                        move.b  d0,d7
                        move.b  0(a2,d7.w),d0
                        cmp.b   #6,d0
                        beq     lbC021956
                        cmp.b   #8,d0
                        beq     lbC021956
                        cmp.b   #$46,d0
                        beq     lbC021956
                        cmp.b   #$48,d0
                        beq     lbC021956
                        bra.s   lbC0218E2

lbC021950:              move.b  0(a2,d6.w),0(a4,d6.w)
lbC021956:              subq.w  #1,d6
                        dbra    d4,lbC0218D2
                        move.w  #10,d4
                        dbra    d5,lbC0218D2
                        bra     lbC021976

display_decoded_map_page:
                        movem.l d0-d7/a0-a5,-(sp)
                        bra.b   lbC0219B4

display_coded_map_page:
                        movem.l d0-d7/a0-a5,-(sp)
lbC021976:              lea     lbL022420,a0
                        lea     lbL0224D0,a3
                        lea     lbB0214D0(pc),a1
                        move.w  #121-1,d0
lbC02198A:              move.b  0(a0,d0.w),d1
                        cmp.b   #' ',d1
                        bcs     lbC0219AC
                        cmp.b   #'A'-1,d1
                        bcc     lbC0219AC
                        btst    #7,0(a1,d0.w)
                        beq     lbC0219AC
                        add.b   #'a'-1,d1
lbC0219AC:              move.b  d1,0(a3,d0.w)
                        dbra    d0,lbC02198A

lbC0219B4:              move.l  Bitmap_PtrB,a4
                        lea     8(a4),a4
                        movem.l (a4)+,a0-a3
                        lea     480(a0),a0
                        lea     480(a1),a1
                        lea     480(a2),a2
                        lea     480(a3),a3
                        lea     lbL0224D0,a4
                        lea     lbL0240F0,a5
                        move.w  #$28,d3
                        moveq   #11-1,d6
                        clr.w   d4
lbC0219E6:              clr.w   d5
                        moveq   #11-1,d7
lbC0219EA:              move.w  d4,d1
                        add.w   d5,d1
                        clr.w   d0
                        move.b  (a4)+,d0
                        lsl.w   #6,d0
                        moveq   #16-1,d2                                        ; Y
lbC0219F6:
                        move.b  0(a5,d0.w),1(a0,d1.w)
                        move.b  1(a5,d0.w),2(a0,d1.w)
                        move.b  $20(a5,d0.w),1(a1,d1.w)
                        move.b  $21(a5,d0.w),2(a1,d1.w)
                        move.b  $40(a5,d0.w),1(a2,d1.w)
                        move.b  $41(a5,d0.w),2(a2,d1.w)
                        move.b  $60(a5,d0.w),1(a3,d1.w)
                        move.b  $61(a5,d0.w),2(a3,d1.w)
                        addq.w  #2,d0
                        add.w   d3,d1
                        dbra    d2,lbC0219F6
                        addq.w  #2,d5
                        dbra    d7,lbC0219EA
                        add.w   #640,d4
                        dbra    d6,lbC0219E6
                        bra     lbC021A7C

get_encrypt_key_response:   
                        lea     encrypt_key,a0
                        move.b  0(a0,d0.w),d0
                        rts

; Some sort of protection
encrypt_key:            dc.b    $6b         ; k
                        dc.b    $79         ; y
                        dc.b    $6c         ; l
                        dc.b    $65         ; e
                        dc.b    $6a         ; j
                        dc.b    $61         ; a
                        dc.b    $6b         ; k
                        dc.b    $65         ; e
                        dc.b    $62         ; b
                        dc.B    0

lbC021A78:              movem.l d0-d7/a0-a5,-(sp)
lbC021A7C:              tst.w   lbW01BB76
                        beq     lbC021A98
                        subq.w  #1,lbW01BB76
                        bne     lbC021A98
                        jsr     lbC01BB78
lbC021A98:              jsr     swap_view
                        jsr     _WaitTOF
                        movem.l (sp)+,d0-d7/a0-a5
                        rts

lbC021AAA:              lea     lbL0240F0,a0            ; scroll something
                        add.w   d2,a0
                        move.w  #3,d1
lbC021AB6:              move.w  $1E(a0),d0
                        move.w  $1C(a0),$1E(a0)
                        move.w  $1A(a0),$1C(a0)
                        move.w  $18(a0),$1A(a0)
                        move.w  $16(a0),$18(a0)
                        move.w  $14(a0),$16(a0)
                        move.w  $12(a0),$14(a0)
                        move.w  $10(a0),$12(a0)
                        move.w  14(a0),$10(a0)
                        move.w  12(a0),14(a0)
                        move.w  10(a0),12(a0)
                        move.w  8(a0),10(a0)
                        move.w  6(a0),8(a0)
                        move.w  4(a0),6(a0)
                        move.w  2(a0),4(a0)
                        move.w  (a0),2(a0)
                        move.w  d0,(a0)
                        lea     32(a0),a0
                        dbra    d1,lbC021AB6
                        rts

lbC021B1E:              movem.l d0-d7/a0-a5,-(sp)
                        jsr     blit_front_to_back_buffer
                        lea     lbL026914,a4
                        moveq   #-1,d0
                        move.l  Bitmap_PtrB,a0
                        jsr     lbC01FAF0
                        jsr     blit_back_to_front_buffer
                        movem.l (sp)+,d0-d7/a0-a5
                        rts

lbC021B48:              clr.b   $77(a3)
                        move.l  key_ascii,d0
                        btst    #7,d0
                        bne     lbC021B9C
                        move.w  mouse_x,save_mouse_x
                        move.w  mouse_y,save_mouse_y
                        
                        clr.w   d0
                        btst    #6,$BFE001
                        bne     lbC021B80
                        bset    #0,d0

lbC021B80:              move.w  $DFF016,d1
                        btst    #10,d1
                        bne     lbC021B92
                        bset    #1,d0
lbC021B92:              tst.w   d0
                        bne     lbC021BFC
                        bra     lbC021BF4

lbC021B9C:              and.l   #$7F,d0
                        move.l  d0,key_ascii
                        move.l  d0,-(sp)
                        bsr     lbC021DEA
                        move.l  (sp)+,d0
                        jsr     translate_key
                        beq     lbC021BF8
                        cmp.b   #$81,d0
                        beq     lbC021BEE
                        cmp.b   #$8A,d0
                        beq     lbC021BEE
                        cmp.b   #$95,d0
                        beq     lbC021BEE
                        cmp.b   #$88,d0
                        beq     lbC021BEE
                        cmp.b   #$61,d0
                        bcs     lbC021BEE
                        cmp.b   #$7A,d0
                        bhi     lbC021BEE
                        and.b   #$DF,d0
lbC021BEE:              or.b    #$80,d0
                        rts

lbC021BF4:              bsr     lbC021DEA
lbC021BF8:              clr.b   d0
                        rts

lbC021BFC:              move.w  d0,-(sp)
                        bsr     lbC021DEA
                        move.w  (sp)+,d0
                        cmp.w   #3,d0                   ; Both buttons pressed ?
                        beq.s   lbC021BF8
                        cmp.b   #1,$79(a3)
                        beq     lbC021CAA
                        btst    #0,d0                   ; Left button pressed
                        bne     lbC021C6C
                        move.w  lbW021F32,d0
                        cmp.w   #4,d0
                        bcc     lbC021C94
                        bsr     lbC021C9A
                        move.b  d0,$77(a3)
                        move.b  #$C1,d0
                        cmp.b   #$80,$4C(a3)
                        beq     lbC021C68
                        cmp.b   #$16,11(a3)
                        bne     lbC021C50
                        move.b  #$C6,d0
                        rts

lbC021C50:              cmp.b   #2,$4C(a3)
                        beq     lbC021C64
                        cmp.b   #3,$4C(a3)
                        bne     lbC021C68
lbC021C64:              move.b  #$D4,d0
lbC021C68:              tst.b   d0
                        rts

lbC021C6C:              move.w  lbW021F32,d0
                        cmp.w   #4,d0
                        bcs     lbC021C9A
                        beq     lbC021C94
                        cmp.w   #5,d0
                        bne     lbC021BF8
                        cmp.b   #2,$79(a3)
                        bne     lbC021BF8
                        bra     lbC021CAA

lbC021C94:              move.b  $78(a3),d0
                        rts

lbC021C9A:              lea     lbL021CA6,a0
                        move.b  0(a0,d0.w),d0
                        rts

lbL021CA6:              dc.b    $81,$8A,$88,$95

lbC021CAA:              cmp.w   #5,lbW021F32
                        bne     lbC021CDC
                        move.w  save_mouse_x,d4
                        move.w  save_mouse_y,d5
                        lea     lbW021D9A,a0
                        clr.w   d0
lbC021CCA:              bsr     lbC021DC2
                        beq     lbC021CE0
                        addq.l  #8,a0
                        addq.l  #1,d0
                        cmp.w   #4,d0
                        bne.s   lbC021CCA
lbC021CDC:              clr.b   d0
                        rts

lbC021CE0:              move.l  a0,lbL021DBA
                        move.w  d0,lbW021DBE
                        move.w  #$FFFF,lbW021DC0
lbC021CF4:              bsr     lbC021D8A
lbC021CF8:              move.w  mouse_x,save_mouse_x
                        move.w  mouse_y,save_mouse_y
                        move.w  save_mouse_x,d4
                        move.w  save_mouse_y,d5
                        move.l  lbL021DBA,a0
                        btst    #6,$BFE001
                        beq     lbC021D60
                        bsr     lbC021DC2
                        beq     lbC021D3A
                        bsr     lbC021D6C
                        clr.b   d0
                        rts
    
lbC021D3A:              bsr     lbC021D6C
                        move.w  lbW021DBE,d0
                        cmp.b   #2,$79(a3)
                        bne     lbC021D58
                        move.b  #3,$79(a3)
                        bra     lbC021BEE

lbC021D58:              add.b   #$31,d0
                        bra     lbC021BEE

lbC021D60:              bsr     lbC021DC2
                        beq.s   lbC021CF4
                        bsr     lbC021D6C
                        bra.s   lbC021CF8

lbC021D6C:              tst.w   lbW021DC0
                        bne     lbC021D88
                        move.w  #$FFFF,lbW021DC0
lbC021D7E:              move.w  lbW021DBE,d0
                        bsr     lbC021130
lbC021D88:              rts

lbC021D8A:              tst.w   lbW021DC0
                        beq.s   lbC021D88
                        clr.w   lbW021DC0
                        bra.s   lbC021D7E

lbW021D9A:              dc.w    $BF,7,312,32
                        dc.w    $BF,$27,312,64
                        dc.w    $BF,$47,312,96
                        dc.w    $BF,$67,312,128
lbL021DBA:              dc.l    0
lbW021DBE:              dc.w    0
lbW021DC0:              dc.w    0

lbC021DC2:              cmp.w   (a0),d4
                        bcs     lbC021DE4
                        cmp.w   4(a0),d4
                        bcc     lbC021DE4
                        cmp.w   2(a0),d5
                        bcs     lbC021DE4
                        cmp.w   6(a0),d5
                        bcc     lbC021DE4
                        clr.w   d1
                        rts

lbC021DE4:              move.w  #$FFFF,d1
                        rts

lbC021DEA:              move.w  save_mouse_x,d0
                        move.w  save_mouse_y,d1
                        cmp.b   #$80,$4C(a3)
                        beq     lbC021E40
                        cmp.w   lbW021F2C,d0
                        bne     lbC021E40
                        cmp.w   lbW021F2E,d1
                        bne     lbC021E40
                        rts

lbC021E16:              move.w  #-1,lbW021F30
                        move.w  save_mouse_x,d0
                        move.w  save_mouse_y,d1
                        bsr     lbC021E4C
                        cmp.b   #1,$4C(a3)
                        bne     lbC021F0C
                        jsr     lbC026A74
                        rts

lbC021E40:              move.w  d0,lbW021F2C
                        move.w  d1,lbW021F2E
lbC021E4C:              cmp.b   #1,$79(a3)
                        beq     lbC021F0E
                        cmp.w   #$B8,d0
                        bcc     lbC021F0E
                        cmp.w   #8,d0
                        bcc     lbC021E6C
                        clr.w   d0
                        bra     lbC021E72

lbC021E6C:              subq.w  #8,d0
                        lsr.w   #4,d0
lbC021E72:              cmp.w   #$BC,d1
                        bcs     lbC021E82
                        move.w  #10,d1
                        bra     lbC021E96

lbC021E82:              cmp.w   #12,d1
                        bcc     lbC021E90
                        clr.w   d1
                        bra     lbC021E96

lbC021E90:              sub.w   #12,d1
                        lsr.w   #4,d1
lbC021E96:              cmp.b   #$80,$4C(a3)
                        bne     lbC021EB4
                        move.w  #10,d2
                        move.w  #10,d3
                        sub.b   PARTY_ACTION_X(a3),d2
                        sub.b   PARTY_ACTION_Y(a3),d3
                        bra     lbC021ED2

lbC021EB4:              cmp.b   #1,$4C(a3)
                        bne     lbC021ECA
                        move.w  #5,d2
                        move.w  #2,d3
                        bra     lbC021ED2

lbC021ECA:              move.w  #5,d2
                        move.w  #5,d3
lbC021ED2:              add.w   d3,d1
                        mulu    #$15,d1
                        add.w   d2,d0
                        add.w   d0,d1
                        cmp.w   lbW021F30,d1
                        beq     lbC021F0C
                        move.w  d1,lbW021F30
                        lea     lbB021F34,a0
                        moveq   #0,d0
                        move.b  0(a0,d1.w),d0
                        move.w  d0,lbW021F32
                        cmp.w   #4,d0
                        beq     lbC021F26
lbC021F06:              jsr     mouse_set_new_cursor
lbC021F0C:              rts

lbC021F0E:              cmp.w   #5,lbW021F32
                        beq.s   lbC021F0C
                        move.w  #5,lbW021F32
                        move.w  #4,d0
                        bra.s   lbC021F06

lbC021F26:              move.w  #5,d0
                        bra.s   lbC021F06

lbW021F2C:              dc.w    0
lbW021F2E:              dc.w    0
lbW021F30:              dc.w    0
lbW021F32:              dc.w    0
lbB021F34:              dc.b    2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,2,2,0,0
                        dcb.b   15,0
                        dcb.b   2,3
                        dcb.b   3,2
                        dcb.b   15,0
                        dcb.b   3,3
                        dcb.b   4,2
                        dcb.b   13,0
                        dcb.b   4,3
                        dcb.b   5,2
                        dcb.b   11,0
                        dcb.b   5,3
                        dcb.b   6,2
                        dcb.b   9,0
                        dcb.b   6,3
                        dcb.b   7,2
                        dcb.b   7,0
                        dcb.b   7,3
                        dcb.b   8,2
                        dcb.b   5,0
                        dcb.b   8,3
                        dcb.b   9,2
                        dcb.b   3,0
                        dcb.b   9,3
                        dcb.b   10,2
                        dc.b    0,3,3,3,3,3,3,3,3,3,3,2,2,2,2,2,2,2,2,2,2,4,3,3,3
                        dcb.b   7,3
                        dcb.b   10,2
                        dc.b    1,3,3,3,3,3,3,3,3,3,3,2,2,2,2,2,2,2,2,2,1,1,1,3,3
                        dcb.b   7,3
                        dcb.b   8,2
                        dcb.b   5,1
                        dcb.b   8,3
                        dcb.b   7,2
                        dcb.b   7,1
                        dcb.b   7,3
                        dcb.b   6,2
                        dcb.b   9,1
                        dcb.b   6,3
                        dcb.b   5,2
                        dcb.b   11,1
                        dcb.b   5,3
                        dcb.b   4,2
                        dcb.b   13,1
                        dcb.b   4,3
                        dcb.b   3,2
                        dcb.b   15,1
                        dcb.b   3,3
                        dcb.b   2,2
                        dcb.b   $11,1
                        dcb.b   2,3
                        dc.b    2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,0

file_load_encrypted:    lea     lbB0237D0,a0
                        jsr     file_load_cache
                        rts

generate_jumps_table:   lea     jumps_table,a0
                        lea     lbL0237EE,a1
                        move.w  #20-1,d0
gen_jumps_table_loop:   move.w  #$4EF9,(a1)+
                        move.l  (a0)+,(a1)+
                        dbra    d0,gen_jumps_table_loop
                        rts

; Table provided to the sub programs
jumps_table:            dc.l    display_message
                        dc.l    text_carriage_return
                        dc.l    draw_letter_both_bitmap
                        dc.l    lbC018DEC
                        dc.l    lbC021124
                        dc.l    input_get_number
                        dc.l    play_sound
                        dc.l    input_wait_key
                        dc.l    get_character_data
                        dc.l    increase_character_food
                        dc.l    draw_digit
                        dc.l    lbC021B48
                        dc.l    refresh_map_view
                        dc.l    set_character_byte_value
                        dc.l    lbC021AAA
                        dc.l    display_coded_map_page
                        dc.l    display_message_at
                        dc.l    draw_2_digits
                        dc.l    lbC020FE0
                        dc.l    get_encrypt_key_response
                        dc.l    $12345678

; ----------------------------------------
; Save a file
file_save:              move.l  #ACCESS_WRITE,d2
                        bra     Retry_File_Op

; ----------------------------------------
; Load a file
file_load:              move.l  #ACCESS_READ,d2
Retry_File_Op:          movem.l d0/d2/a0,-(sp)
                        lea     PathName,a1
                        and.b   #$7F,d0
                        move.l  d0,8(a1)            ; Set the name of the file to load (4 chars)
                        move.l  d2,-(sp)            ; Command
                        move.l  a0,-(sp)            ; Buffer
                        move.l  a1,-(sp)            ; PathName
                        jsr     file_execute_command
                        add.w   #12,sp
                        cmp.l   #-1,d0
                        beq     lbC0221B2
                        tst.l   File_Size
                        bne     lbC0221BC
lbC0221B2:              bsr     lbC0221C2
                        movem.l (sp)+,d0/d2/a0
                        bra.s   Retry_File_Op

lbC0221BC:              movem.l (sp)+,d0/d2/a0
                        rts

lbC0221C2:              move.l  letter_mask,-(sp)
                        move.b  TEXT_X(a3),SAVE_TEXT_X(a3)
                        move.b  TEXT_Y(a3),SAVE_TEXT_Y(a3)
                        movem.l d0-d7/a0-a5,-(sp)
                        move.w  #6,d0
                        jsr     display_requester
                        movem.l (sp)+,d0-d7/a0-a5
                        move.b  SAVE_TEXT_Y(a3),TEXT_Y(a3)
                        move.b  SAVE_TEXT_X(a3),TEXT_X(a3)
                        move.l  (sp)+,letter_mask
                        rts

                        SECTION EXODUS0221FC,BSS

lbB0221FC:              ds.b    36
game_state:             ds.l    128
lbL022420:              ds.l    32
lbL0224A0:              ds.l    2
lbL0224A8:              ds.l    2
lbL0224B0:              ds.l    2
lbL0224B8:              ds.l    2
lbL0224C0:              ds.l    1
lbL0224C4:              ds.l    1
lbL0224C8:              ds.l    1
lbL0224CC:              ds.l    1
lbL0224D0:              ds.l    64
lbL0225D0:              ds.l    8
lbL0225F0:              ds.l    8
lbL022610:              ds.l    8
lbL022630:              ds.l    8
lbL022650:              ds.l    31
lbB0226CC:              ds.b    1
lbB0226CD:              ds.b    1
lbB0226CE:              ds.b    1
lbB0226CF:              ds.b    1
lbL0226D0:              ds.b    256
current_map:            ds.l    857
                        ds.b    1
lbB023535:              ds.b    667

lbB0237D0:              ds.b    30              ; Code header
lbL0237EE:              ds.b    2016            ; Jump table start here
                        
characters_data:        ds.b    256
tiles_buffer:           ds.b    32              ; (seems to be unused)
lbL0240F0:              ds.b    3936
lbL025050:              ds.b    6144
lbL026850:              ds.b    160

                        SECTION EXODUS0268F0,DATA

PathName:               dc.b    "ULTMAPS/SOSA.BIN",0,0
                        dcb.b   2,0
letter_mask:            dc.l    0
                        ; may not be used
                        dc.b    $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,0,0,0,0
lbL026914:              dc.w    $8,$C,$B8,$BC           ; Some coords
winds_table:            dc.l    CALMWINDS_MSG
                        dc.l    NORTHWIND_MSG
                        dc.l    EASTWIND_MSG
                        dc.l    SOUTHWIND_MSG
                        dc.l    WESTWIND_MSG
lbB026930:              dc.b    $C6,$C3,$D7,$D4,$D0,$C2,$CC,$C9,$C4,$C1,$D2
lbB02693B:              dc.b    $D1,$C4,$C3,$C8,$D1,$D1,$D1,$C4,$C4,$C3,$CC
lbB026946:              dc.b    $C9,$C5,$C3,$C4,$C6,$C4,$C3,$C4,$C3,$C3,$C8

ascii_MSG20:            dcb.b   2,1
                        dcb.b   2,1
                        dc.b    1
                        dc.b    0
                        dcb.b   5,$FF

ascii_MSG21:            dcb.b   2,11
                        dcb.b   2,11
                        dc.b    11
                        dc.b    0
                        dcb.b   5,$F5

ascii_MSG22:            dc.b    5
                        dc.b    4
                        dc.b    3
                        dc.b    2
                        dc.b    1
                        dc.b    0
                        dc.b    1
                        dc.b    2
                        dc.b    3
                        dc.b    4
                        dc.b    5

lbW026972:              dc.w    $839,$F24,$F0C,$1F3A
lbW02697A:              dc.w    $82E,$1B3A,$1D37,$1F1F
lbB026982:              dc.b    0,1,1,1,0,$FF,$FF,$FF
lbB02698A:              dc.b    1,1,0,$FF,$FF,$FF,0,1
lbB026992:              dc.b    $2D
lbW026993:              dc.b    $A,$2E,$6,$22,$31,$2F,$7,$25,$12,$1E,$38,$13,$31,$3A,$3A,$38,$9,$2E
lbB0269A5:              dc.b    $12,$35,$13,13,$10,$3A,$3A,$2C,$35,$1F,2,$1F,$39
                        dc.b    $22,$1E,$2C,6,$1C,7
lbB0269B8:              dc.b    $18,$17,$19,$14,$1A,$1B,13,$1C,$16,14,15,$1D,$1E
lbB0269C5:              dc.b    4,4,4,4,4,4,0,4,4,0,0,4,4
lbB0269D2:              dc.b    1,2,$15,$20,8,6,$10,5,3,4,6,8,$10,$15,$20,5
lbB0269E2:              dc.b    $20,$20,$f0,$F0,$C0,$60,$A0,$80,$30,$50,$70,$A0,$C0,$E0,$F0,$F0
lbW0269F2:              dc.b    $00,$01,$00,$FF
lbW0269F6:              dc.b    $FF,$00,$01,$00
lbW0269FA:              dc.b    $E0,$30,$0C,$06,$06,$0C,$30,$E0,$00,$00,$00,$00,$00,$00,$00,$00,$E0,$F0,$FC,$FE,$FE,$FC
                        dc.b    $F0,$E0,$E0,$30,$0C,$06,$06,$0C,$30,$E0
lbB026A1A:              dc.b    7,12,$30,$60,$60,$30
                        dc.b    12,7,0,0,0,0,0,0,0,0,7,15,$3F,$7F,$7F,$3F,15,7,7
                        dc.b    12,$30,$60,$60,$30,12,7
CALMWINDS_MSG:          dc.b    "CALM WINDS",0
NORTHWIND_MSG:          dc.b    "NORTH WIND",0
EASTWIND_MSG:           dc.b    "EAST  WIND",0
SOUTHWIND_MSG:          dc.b    "SOUTH WIND",0
WESTWIND_MSG:           dc.b    "WEST  WIND",0
speed_scroll_input_char:    
                        dc.b    10
                        even

                        SECTION EXODUS026A74,CODE

lbC026A74:              jsr     lbC020FF2
                        tst.b   LIGHT_COUNTER(a3)
                        beq     lbC026A86
                        bsr     lbC026A8E
lbC026A86:              jsr     lbC021A78
                        rts

lbC026A8E:              clr.b   d0
                        jsr     lbC026C38
                        beq     lbC026AA0
                        bpl     lbC026B40
                        rts

lbC026AA0:              move.b  #1,d0
                        jsr     lbC026C38
                        bmi     lbC026AF0
                        move.b  #3,d0
                        jsr     lbC026C38
                        bmi     lbC026AF0
                        move.b  #6,d0
                        jsr     lbC026C38
                        bmi     lbC026AF0
                        move.b  #10,d0
                        jsr     lbC026C38
                        bmi     lbC026AF0
                        move.b  #15,d0
                        jsr     lbC026C38
                        bmi     lbC026AF0
                        move.b  #$15,d0
                        jsr     lbC026C38

lbC026AF0:              move.b  #2,d0
                        jsr     lbC026C38
                        bmi     lbC026B40
                        move.b  #5,d0
                        jsr     lbC026C38
                        bmi     lbC026B40
                        move.b  #9,d0
                        jsr     lbC026C38
                        bmi     lbC026B40
                        move.b  #14,d0
                        jsr     lbC026C38
                        bmi     lbC026B40
                        move.b  #$14,d0
                        jsr     lbC026C38
                        bmi     lbC026B40
                        move.b  #$1B,d0
                        jsr     lbC026C38

lbC026B40:              move.b  #4,d0
                        jsr     lbC026C38
                        beq     lbC026B50
                        rts

lbC026B50:              move.b  #7,d0
                        jsr     lbC026C38
                        bmi     lbC026B84
                        move.b  #11,d0
                        jsr     lbC026C38
                        bmi     lbC026B84
                        move.b  #$10,d0
                        jsr     lbC026C38
                        bmi     lbC026B84
                        move.b  #$16,d0
                        jsr     lbC026C38
lbC026B84:
                        move.b  #8,d0
                        jsr     lbC026C38
                        bmi     lbC026BB8
                        move.b  #13,d0
                        jsr     lbC026C38
                        bmi     lbC026BB8
                        move.b  #$13,d0
                        jsr     lbC026C38
                        bmi     lbC026BB8
                        move.b  #$1A,d0
                        jsr     lbC026C38
lbC026BB8:
                        move.b  #12,d0
                        jsr     lbC026C38
                        beq     lbC026BC8
                        rts

lbC026BC8:              move.b  #$11,d0
                        jsr     lbC026C38
                        bmi     lbC026BEE
                        move.b  #$17,d0
                        jsr     lbC026C38
                        bmi     lbC026BEE
                        move.b  #$1C,d0
                        jsr     lbC026C38
lbC026BEE:              move.b  #$12,d0
                        jsr     lbC026C38
                        bmi     lbC026C14
                        move.b  #$19,d0
                        jsr     lbC026C38
                        bmi     lbC026C14
                        move.b  #$1F,d0
                        jsr     lbC026C38
lbC026C14:              move.b  #$18,d0
                        jsr     lbC026C38
                        bmi     lbC026C36
                        move.b  #$1D,d0
                        jsr     lbC026C38
                        move.b  #$1E,d0
                        jsr     lbC026C38
lbC026C36:              rts

lbC026C38:              move.b  d0,$3F(a3)
                        clr.w   d1
                        move.b  d0,d1
                        lea     lbL026D2A,a0
                        move.b  0(a0,d1.w),PARTY_DIRECTION_X(a3)
                        lea     lbL026D4A,a0
                        move.b  0(a0,d1.w),PARTY_DIRECTION_Y(a3)
                        move.b  $3E(a3),d2
                        beq     lbC026C7E
lbC026C60:
                        move.b  PARTY_DIRECTION_X(a3),d1
                        move.b  PARTY_DIRECTION_Y(a3),PARTY_DIRECTION_X(a3)
                        not.b   PARTY_DIRECTION_X(a3)
                        addq.b  #1,PARTY_DIRECTION_X(a3)
                        move.b  d1,PARTY_DIRECTION_Y(a3)
                        sub.b   #1,d2
                        bne.s   lbC026C60
lbC026C7E:              move.b  PARTY_X(a3),d0
                        add.b   PARTY_DIRECTION_X(a3),d0
                        and.b   #15,d0
                        move.b  d0,PARTY_ACTION_X(a3)
                        move.b  PARTY_Y(a3),d0
                        add.b   PARTY_DIRECTION_Y(a3),d0
                        and.b   #15,d0
                        move.b  d0,PARTY_ACTION_Y(a3)
                        jsr     tile_dungeon_get_action_datas
                        bmi     lbC026CE8
                        move.w  d0,-(sp)
                        and.b   #$10,d0
                        beq     lbC026CBC
                        clr.b   $49(a3)
                        jsr     lbC026F7A
lbC026CBC:
                        move.w  (sp)+,d0
                        move.w  d0,-(sp)
                        and.b   #$20,d0
                        beq     lbC026CD4
                        move.b  #1,$49(a3)
                        jsr     lbC026F7A
lbC026CD4:
                        move.w  (sp)+,d0
                        and.b   #$40,d0
                        beq     lbC026CE4
                        jsr     lbC026F0C
lbC026CE4:              clr.b   d0
                        rts

lbC026CE8:              cmp.b   #$C0,d0
                        bcc     lbC026CFA
                        move.b  $3F(a3),d0
                        jmp     lbC026D6A

lbC026CFA:              move.b  $3F(a3),d0
                        jsr     lbC026D6A
                        add.b   #$20,$3F(a3)
                        move.b  $3F(a3),d0
                        jsr     lbC026D6A
                        cmp.b   #$20,$3F(a3)
                        beq     lbC026D24
                        move.b  #$FF,d0
                        rts

lbC026D24:              move.b  #1,d0
                        rts

lbL026D2A:              dc.b    $00,$FF,$01,$FF,$00,$01,$FE,$FF,$01,$02,$FE,$FF,$00,$01,$02,$FD,$FE,$FF,$01,$02,$03,$FD,$FE,$FF
                        dc.b    $00,$01,$02,$03,$FE,$FF,$01,$02
lbL026D4A:              dc.b    $00,$00,$00,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FE,$FE,$FE,$FE,$FE,$FE,$FE,$FE,$FE,$FE
                        dc.b    $FE,$FD,$FD,$FD,$FD,$FD,$FD,$FD,$FD,$FD,$FD,$FD

lbC026D6A:              lea     lbW027222,a0
                        clr.w   d3
                        move.b  d0,d3
                        lsl.w   #3,d3
                        add.w   d3,a0
                        move.b  0(a0),$40(a3)
                        move.b  1(a0),$41(a3)
                        move.b  2(a0),$42(a3)
                        move.b  3(a0),$43(a3)
                        move.b  4(a0),$44(a3)
                        move.b  4(a0),$45(a3)
                        move.b  5(a0),$46(a3)
                        move.b  5(a0),$47(a3)
                        move.b  6(a0),$48(a3)
                        move.w  #$FFFF,d7
                        cmp.b   #$20,d0
                        bhi     lbC026DD6
                        move.w  #$5DC0,d6
                        clr.w   d7
                        btst    #0,$3E(a3)
                        beq     lbC026DCC
                        exg     d6,d7
lbC026DCC:
                        tst.b   7(a0)
                        beq     lbC026DD6
                        exg     d6,d7
lbC026DD6:
                        jsr     lbC026E58
                        subq.b  #1,$48(a3)
                        beq     lbC026E52
                        move.b  $43(a3),d0
                        add.b   d0,$40(a3)
                        subq.b  #1,$45(a3)
                        bne     lbC026E04
                        move.b  $44(a3),$45(a3)
                        addq.b  #1,$41(a3)
lbC026E04:
                        subq.b  #1,$47(a3)
                        bne     lbC026E1A
                        move.b  $46(a3),$47(a3)
                        subq.b  #1,$42(a3)
lbC026E1A:
                        cmp.b   #1,$43(a3)
                        beq.s   lbC026DD6
                        subq.b  #1,$45(a3)
                        bne     lbC026E38
                        move.b  $44(a3),$45(a3)
                        addq.b  #1,$41(a3)
lbC026E38:
                        sub.b   #1,$47(a3)
                        bne     lbC026E4E
                        move.b  $46(a3),$47(a3)
                        sub.b   #1,$42(a3)
lbC026E4E:              jmp     lbC026DD6(pc)

lbC026E52:              move.b  #$FF,d0
                        rts

lbC026E58:              cmp.b   #$20,$3F(a3)
                        beq     lbC026EA4
                        bsr     lbC026ECC
                        tst.w   d7
                        bmi     lbC026E82
                        add.w   d7,d2
                        add.w   d2,a0
lbC026E70:
                        or.b    d0,(a0)
                        add.w   #$28,a0
                        addq.b  #1,d1
                        cmp.b   $42(a3),d1
                        bcs.s   lbC026E70
                        rts

lbC026E82:              add.w   d2,a0
                        move.b  $42(a3),d2
lbC026E88:              or.b    d0,(a0)
                        or.b    d0,8000(a0)
                        or.b    d0,16000(a0)
                        or.b    d0,24000(a0)
                        add.w   #SCREEN_BYTES,a0
                        addq.b  #1,d1
                        cmp.b   d2,d1
                        bcs.s   lbC026E88
                        rts

lbC026EA4:              bsr     lbC026ECC
                        not.b   d0
                        add.w   d2,a0
                        move.b  $42(a3),d2
lbC026EB0:              and.b   d0,(a0)
                        and.b   d0,8000(a0)
                        and.b   d0,16000(a0)
                        and.b   d0,24000(a0)
                        add.w   #SCREEN_BYTES,a0
                        addq.b  #1,d1
                        cmp.b   d2,d1
                        bcs.s   lbC026EB0
                        rts

lbC026ECC:              clr.w   d1
                        move.b  $40(a3),d1
                        add.w   #$1E,d1
                        lsr.w   #3,d1
                        move.w  d1,d2
                        move.b  $40(a3),d0
                        add.b   #$1E,d0
                        and.w   #7,d0
                        move.l  #lbB0278F8,a0
                        move.b  0(a0,d0.w),d0
                        move.b  $41(a3),d1
                        move.w  d1,d3
                        add.w   #$20,d3
                        mulu    #40,d3
                        move.l  Bitmap_PtrB,a0
                        move.l  8(a0),a0
                        add.w   d3,d2
                        rts

lbC026F0C:              clr.w   d2
                        move.b  $3F(a3),d2
                        cmp.b   #$15,d2
                        bcs     lbC026F1C
                        rts

lbC026F1C:              lsl.w   #2,d2
                        move.l  #lbB0274CE,a0
                        move.b  0(a0,d2.w),d0
                        bpl     lbC026F2E
                        rts

lbC026F2E:              move.b  d0,$40(a3)
                        move.b  1(a0,d2.w),$43(a3)
                        move.b  2(a0,d2.w),$41(a3)
                        move.b  3(a0,d2.w),$42(a3)
lbC026F44:
                        jsr     lbC026ECC(pc)
                        add.w   d2,a0
lbC026F4A:
                        or.b    d0,0(a0)
                        or.b    d0,8000(a0)
                        or.b    d0,16000(a0)
                        or.b    d0,24000(a0)
                        add.w   #SCREEN_BYTES,a0
                        addq.b  #1,d1
                        cmp.b   $42(a3),d1
                        bcs.s   lbC026F4A
                        addq.b  #1,$40(a3)
                        move.b  $40(a3),d0
                        cmp.b   $43(a3),d0
                        bcs.s   lbC026F44
                        rts

lbC026F7A:              move.b  $3F(a3),d0
                        cmp.b   #$15,d0
                        bcs     lbC026F88
                        rts

lbC026F88:              move.l  #-1,lbB0278F4
                        lsl.w   #3,d0
                        clr.w   d2
                        move.b  d0,d2
                        move.l  #lbB027422,a0
                        move.b  0(a0,d2.w),d0
                        bpl     lbC026FA8
                        rts

lbC026FA8:
                        move.b  d0,$7C(a3)
                        move.b  d0,$40(a3)
                        move.b  2(a0,d2.w),d0
                        beq     lbC026FF2
                        tst.b   $49(a3)
                        beq     lbC026FC8
                        move.b  #$7F,d0
                        sub.b   2(a0,d2.w),d0
lbC026FC8:
                        move.b  d0,$7D(a3)
                        move.w  d0,-(sp)
                        move.b  #$40,$7E(a3)
                        jsr     lbC027098
                        addq.b  #1,$7C(a3)
                        addq.b  #1,$40(a3)
                        move.w  (sp)+,d0
                        move.b  d0,$7D(a3)
                        jsr     lbC027098
lbC026FF2:
                        move.b  1(a0,d2.w),d0
                        move.b  d0,$7C(a3)
                        move.b  d0,$40(a3)
                        move.b  3(a0,d2.w),d0
                        beq     lbC027040
                        tst.b   $49(a3)
                        beq     lbC027016
                        move.b  #$7F,d0
                        sub.b   3(a0,d2.w),d0
lbC027016:
                        move.b  d0,$7D(a3)
                        move.w  d0,-(sp)
                        move.b  #$40,$7E(a3)
                        jsr     lbC027098
                        subq.b  #1,$7C(a3)
                        subq.b  #1,$40(a3)
                        move.w  (sp)+,d0
                        move.b  d0,$7D(a3)
                        jsr     lbC027098
lbC027040:
                        move.b  0(a0,d2.w),$7C(a3)
                        move.b  1(a0,d2.w),$40(a3)
                        move.b  4(a0,d2.w),d0
                        tst.b   $49(a3)
                        beq     lbC027060
                        move.b  #$7F,d0
                        sub.b   4(a0,d2.w),d0
lbC027060:
                        move.b  d0,$7D(a3)
                        move.b  d0,$7E(a3)
                        jsr     lbC027098
                        move.b  0(a0,d2.w),$7C(a3)
                        move.b  5(a0,d2.w),d0
                        tst.b   $49(a3)
                        beq     lbC027088
                        move.b  #$7F,d0
                        sub.b   5(a0,d2.w),d0
lbC027088:              move.b  d0,$7D(a3)
                        move.b  d0,$7E(a3)
                        jsr     lbC027098
                        rts

lbC027098:              move.b  $40(a3),PARTY_DIRECTION_X(a3)
                        move.b  $7C(a3),d0
                        sub.b   d0,PARTY_DIRECTION_X(a3)
                        bcc     lbC0270C0
                        not.b   PARTY_DIRECTION_X(a3)
                        addq.b  #1,PARTY_DIRECTION_X(a3)
                        move.b  #$FF,$7F(a3)
                        jmp     lbC0270C6

lbC0270C0:              move.b  #1,$7F(a3)
lbC0270C6:              move.b  $7E(a3),PARTY_DIRECTION_Y(a3)
                        move.b  $7D(a3),d0
                        sub.b   d0,PARTY_DIRECTION_Y(a3)
                        bcc     lbC0270EE
                        not.b   PARTY_DIRECTION_Y(a3)
                        addq.b  #1,PARTY_DIRECTION_Y(a3)
                        move.b  #$FF,$80(a3)
                        jmp     lbC0270F4

lbC0270EE:              move.b  #1,$80(a3)
lbC0270F4:              move.b  PARTY_DIRECTION_X(a3),d0
                        cmp.b   PARTY_DIRECTION_Y(a3),d0
                        bcc     lbC027106
                        jmp     lbC02714C

lbC027106:
                        move.b  PARTY_DIRECTION_X(a3),d0
                        move.b  d0,$7B(a3)
                        lsr.b   #1,d0
                        move.b  d0,$7A(a3)
lbC027114:
                        move.b  $7A(a3),d0
                        add.b   PARTY_DIRECTION_Y(a3),d0
                        move.b  d0,$7A(a3)
                        sub.b   PARTY_DIRECTION_X(a3),d0
                        bcs     lbC027134
                        move.b  d0,$7A(a3)
                        move.b  $80(a3),d0
                        add.b   d0,$7D(a3)
lbC027134:
                        move.b  $7F(a3),d0
                        add.b   d0,$7C(a3)
                        jsr     lbC027192
                        subq.b  #1,$7B(a3)
                        bne.s   lbC027114
                        rts

lbC02714C:              move.b  PARTY_DIRECTION_Y(a3),d0
                        move.b  d0,$7B(a3)
                        lsr.b   #1,d0
                        move.b  d0,$7A(a3)
lbC02715A:
                        move.b  $7A(a3),d0
                        add.b   PARTY_DIRECTION_X(a3),d0
                        move.b  d0,$7A(a3)
                        sub.b   PARTY_DIRECTION_Y(a3),d0
                        bcs     lbC02717A
                        move.b  d0,$7A(a3)
                        move.b  $7F(a3),d0
                        add.b   d0,$7C(a3)
lbC02717A:
                        move.b  $80(a3),d0
                        add.b   d0,$7D(a3)
                        jsr     lbC027192
                        subq.b  #1,$7B(a3)
                        bne.s   lbC02715A
                        rts

lbC027192:              clr.w   d1
                        clr.w   d3
                        move.b  $7C(a3),d3
                        add.w   #$1E,d3
                        move.w  d3,d4
                        lsr.w   #3,d3
                        and.w   #7,d4
                        lea     lbB027900,a1
                        move.b  0(a1,d4.w),d4
                        move.b  $7D(a3),d1
                        add.w   #$20,d1
                        mulu    #$28,d1
                        move.l  Bitmap_PtrB,a1
                        move.l  8(a1),a1
                        add.w   d1,d3
                        add.w   d3,a1
                        tst.b   lbB0278F7
                        beq     lbC0271DC
                        bset    d4,0(a1)
                        bra     lbC0271E0

lbC0271DC:              bclr    d4,0(a1)
lbC0271E0:              tst.b   lbB0278F6
                        beq     lbC0271F2
                        bset    d4,8000(a1)
                        bra     lbC0271F6

lbC0271F2:              bclr    d4,8000(a1)
lbC0271F6:              tst.b   lbB0278F5
                        beq     lbC027208
                        bset    d4,16000(a1)
                        bra     lbC02720C

lbC027208:              bclr    d4,16000(a1)
lbC02720C:              tst.b   lbB0278F4
                        beq     lbC02721C
                        bset    d4,24000(a1)
                        rts

lbC02721C:              bclr    d4,24000(a1)
                        rts

lbW027222:              dc.w    0,$8001,$FFFF,$8102,0,$8001,$101,$2000,$8000
                        dc.w    $80FF,$202,$2000,$20,$6001,$FFFF,$2002,$2020
                        dc.w    $6001,$FFFF,$4102,$8020,$60FF,$FFFF,$2002,$20
                        dc.w    $6001,$202,$2000,$2020,$6001,$101,$1000,$6020
                        dc.w    $60FF,$202,$1000,$8020,$60FF,$404,$2000,$30,$5001
                        dc.w    $FFFF,$2002,$2030,$5001,$FFFF,$1002,$3030,$5001
                        dc.w    $FFFF,$2102,$6030,$50FF,$FFFF,$1002,$8030,$50FF
                        dc.w    $FFFF,$2002,$30,$5001,$404,$2000,$2030,$5001,$202
                        dc.w    $1000,$3030,$5001,$101,$800,$5030,$50FF,$202,$800
                        dc.w    $6030,$50FF,$404,$1000,$8030,$50FF,$808,$2000,$38
                        dc.w    $4801,$FFFF,$2002,$2038,$4801,$FFFF,$1002,$3038
                        dc.w    $4801,$FFFF,$802,$3838,$4801,$FFFF,$1102,$5038
                        dc.w    $48FF,$FFFF,$802,$6038,$48FF,$FFFF,$1002,$8038
                        dc.w    $48FF,$FFFF,$2002,$3038,$4801,$202,$800,$3838
                        dc.w    $4801,$101,$800,$4838,$48FF,$202,$800,$5038,$48FF
                        dc.w    $404,$800,$2020,$8001,$FFFF,$4101,$20,$8001,$201
                        dc.w    $1000,$8020,$80FF,$402,$1000,$30,$6001,$FFFF
                        dc.w    $1001,$3030,$6001,$FFFF,$2001,$8030,$60FF,$FFFF
                        dc.w    $1001,$A30,$5B01,$402,$C00,$2530,$5B01,$201,$600
                        dc.w    $5B30,$5BFF,$402,$600,$7630,$5BFF,$804,$C00,$1038
                        dc.w    $5001,$FFFF,$A01,$2238,$5001,$FFFF,$A01,$3A38
                        dc.w    $5001,$FFFF,$C01,$5E38,$50FF,$FFFF,$A01,$7038
                        dc.w    $50FF,$FFFF,$A01,$D38,$4D01,$804,$800,$2638,$4D01
                        dc.w    $402,$400,$3338,$4D01,$201,$200,$4D38,$4DFF,$402
                        dc.w    $200,$5A38,$4DFF,$804,$400,$7338,$4DFF,$1008,$800
                        dc.w    $103C,$4801,$FFFF,$601,$243C,$4801,$FFFF,$601
                        dc.w    $323C,$4801,$FFFF,$601,$3C3C,$4801,$FFFF,$801
                        dc.w    $4E3C,$48FF,$FFFF,$601,$5C3C,$48FF,$FFFF,$601
                        dc.w    $803C,$48FF,$FFFF,$601,$333C,$4601,$102,$200
                        dc.w    $3A3C,$4601,$101,$200,$463C,$46FF,$202,$200,$4D3C
                        dc.w    $46FF,$204,$200

lbB027422:              dc.b    $30,$50,$10,$10,$20,$34,$4C,$60,0,$10,0,$10,$20
                        dc.b    $34,$4C,$60,$70,$80,$10,0,$20,$34,$4C,$60,$18,$20
                        dc.b    $28,0,$30,$3A,$46,$50,$38,$48,$28,$28,$30,$3A,$46
                        dc.b    $50,$60,$68,0,$28,$30,$3A,$46,$50,0,8,0,$28,$30
                        dc.b    $3A,$46,$50,$20,$28,0,$28,$30,$3A,$46,$50,$58,$60
                        dc.b    $28,0,$30,$3A,$46,$50,$78,$80,$28,0,$30,$3A,$46
                        dc.b    $50,$1C,$20,$34,0,$38,$3D,$42,$48,$2C,$30,$34,0
                        dc.b    $38,$3D,$42,$48,$3C,$44,$34,$34,$38,$3D,$42,$48
                        dc.b    $50,$54,0,$34,$38,$3D,$42,$48,$60,$64,0,$34,$38
                        dc.b    $3D,$42,$48,0,4,0,$34,$38,$3D,$42,$48,$20,$24,0
                        dc.b    $34,$38,$3D,$42,$48,$30,$34,0,$34,$38,$3D,$42,$48
                        dc.b    $4C,$50,$34,0,$38,$3D,$42,$48,$5C,$60,$34,0,$38
                        dc.b    $3D,$42,$48,$7C,$80,$34,0,$38,$3D,$42,$48,$FF,0,0
                        dc.b    0

lbB0274CE:              dc.b    $30,$50,$68,$78,0,$10,$68,$78,$70,$80,$68,$78,$18
                        dc.b    $20,$54,$5C,$38,$48,$54,$5C,$60,$68,$54,$5C,0,8
                        dc.b    $54,$5C,$20,$28,$54,$5C,$58,$60,$54,$5C,$78,$80
                        dc.b    $54,$5C,$1C,$20,$4A,$4E,$2C,$30,$4A,$4E,$3C,$44
                        dc.b    $4A,$4E,$50,$54,$4A,$4E,$60,$64,$4A,$4E,12,$14
                        dc.b    $4A,$4E,$20,$24,$4A,$4E,$30,$34,$4A,$4E,$4C,$50
                        dc.b    $4A,$4E,$5C,$60,$4A,$4E,$6C,$74,$4A,$4E

lbC027522:              jsr     lbC020FF2
                        clr.b   PARTY_ACTION_X(a3)
                        clr.b   PARTY_ACTION_Y(a3)
lbC027530:              move.b  PARTY_ACTION_X(a3),d0
                        addq.b  #4,d0
                        move.b  d0,TEXT_X(a3)
                        move.b  PARTY_ACTION_Y(a3),d0
                        addq.b  #4,d0
                        move.b  d0,TEXT_Y(a3)
                        jsr     tile_dungeon_get_action_datas
                        cmp.b   #$C0,d0
                        bne     lbC02756E
                        move.l  #$FF,letter_mask
                        move.b  #$5F,d0
                        jsr     draw_letter_both_bitmap
                        bra     lbC02762A

lbC02756E:              cmp.b   #$A0,d0
                        bne     lbC02758E
                        move.l  #$FFFF,letter_mask
                        move.b  #$5E,d0
                        jsr     draw_letter_both_bitmap
                        bra     lbC02762A

lbC02758E:              cmp.b   #$80,d0
                        bne     lbC0275AE
                        move.l  #$FFFF,letter_mask
                        move.b  #$40,d0
                        jsr     draw_letter_both_bitmap
                        bra     lbC02762A

lbC0275AE:              cmp.b   #$30,d0
                        bne     lbC0275CE
                        move.l  #$FF0000,letter_mask
                        move.b  #$5B,d0
                        jsr     draw_letter_both_bitmap
                        bra     lbC02762A

lbC0275CE:              cmp.b   #$20,d0
                        bne     lbC0275EE
                        move.l  #$FF00FF,letter_mask
                        move.b  #$5C,d0
                        jsr     draw_letter_both_bitmap
                        bra     lbC02762A

lbC0275EE:              cmp.b   #$10,d0
                        bne     lbC02760E
                        move.l  #$FFFF00,letter_mask
                        move.b  #$5D,d0
                        jsr     draw_letter_both_bitmap
                        bra     lbC02762A

lbC02760E:              tst.b   d0
                        beq     lbC02762A
                        move.l  #$FFFFFF,letter_mask
                        move.b  #$3F,d0
                        jsr     draw_letter_both_bitmap
lbC02762A:
                        addq.b  #1,PARTY_ACTION_X(a3)
                        and.b   #15,PARTY_ACTION_X(a3)
                        bne     lbC027530
                        addq.b  #1,PARTY_ACTION_Y(a3)
                        move.b  PARTY_ACTION_Y(a3),d0
                        cmp.b   #$10,PARTY_ACTION_Y(a3)
                        bcc     lbC027652
                        bra     lbC027530

lbC027652:
                        move.b  PARTY_X(a3),TEXT_X(a3)
                        addq.b  #4,TEXT_X(a3)
                        move.b  PARTY_Y(a3),TEXT_Y(a3)
                        addq.b  #4,TEXT_Y(a3)
                        jsr     lbC021A78
lbC027670:
                        move.l  Bitmap_PtrA,a0
                        move.b  #$2A,d0
                        jsr     draw_letter
                        bsr     lbC0278A4
                        bmi     lbC02769E
                        move.l  Bitmap_PtrA,a0
                        move.b  #$20,d0
                        jsr     draw_letter
                        bsr     lbC0278A4
                        bpl.s   lbC027670
lbC02769E:              jsr     text_carriage_return
                        bsr     lbC026A74
                        move.l  #$FF0FFFF,letter_mask
                        rts

lbC0276B4:              jsr     lbC020FF2
lbC0276BA:              jsr     tile_get_action_datas
                        bne     lbC0276CA
                        jmp     lbC027832

lbC0276CA:              cmp.b   #4,d0
                        beq     lbC027708
                        cmp.b   #8,d0
                        beq     lbC02772C
                        cmp.b   #12,d0
                        beq     lbC02774A
                        cmp.b   #$10,d0
                        beq     lbC0277A2
                        cmp.b   #$20,d0
                        beq     lbC027776
                        cmp.b   #$84,d0
                        beq     lbC0277FE
                        cmp.b   #$8C,d0
                        beq     lbC0277B0
                        jmp     lbC02780C

lbC027708:              move.l  #$FF0000,lbB0278F4
                        move.b  PARTY_ACTION_X(a3),d0
                        and.b   #1,d0
                        move.b  d0,d2
                        move.b  #1,d1
                        jsr     lbC0278C0
                        jmp     lbC027832

lbC02772C:              move.l  #$FF00,lbB0278F4
                        move.b  #1,d1
                        move.b  #1,d2
                        jsr     lbC0278C0
                        jmp     lbC027832

lbC02774A:              move.l  #$FF00FF00,lbB0278F4
                        move.b  #1,d1
                        move.b  #0,d2
                        jsr     lbC0278C0
                        move.b  #1,d1
                        move.b  #1,d2
                        jsr     lbC0278C0
                        jmp     lbC027832

lbC027776:              move.l  #$FFFFFF00,lbB0278F4
                        move.b  #0,d1
                        move.b  #0,d2
                        jsr     lbC0278C0
                        move.b  #0,d1
                        move.b  #1,d2
                        jsr     lbC0278C0
                        jmp     lbC027832

lbC0277A2:              move.l  #$FFFFFFFF,lbB0278F4
                        bra     lbC0277BA

lbC0277B0:              move.l  #$FF00FFFF,lbB0278F4
lbC0277BA:              bsr     lbC0277C4
                        jmp     lbC027832

lbC0277C4:              move.b  #0,d1
                        move.b  #0,d2
                        jsr     lbC0278C0
                        move.b  #0,d1
                        move.b  #1,d2
                        jsr     lbC0278C0
                        move.b  #1,d1
                        move.b  #0,d2
                        jsr     lbC0278C0
                        move.b  #1,d1
                        move.b  #1,d2
                        jsr     lbC0278C0
                        rts

lbC0277FE:              move.l  #$FF,lbB0278F4
                        bra     lbC027816

lbC02780C:              move.l  #$FF00FFFF,lbB0278F4
lbC027816:
                        move.b  #0,d1
                        move.b  #0,d2
                        jsr     lbC0278C0
                        move.b  #1,d1
                        move.b  #0,d2
                        jsr     lbC0278C0
lbC027832:              addq.b  #1,PARTY_ACTION_X(a3)
                        and.b   #$3F,PARTY_ACTION_X(a3)
                        beq     lbC027846
lbC027842:              jmp     lbC0276BA(pc)

lbC027846:              addq.b  #1,PARTY_ACTION_Y(a3)
                        cmp.b   #64,PARTY_ACTION_Y(a3)
                        bcs.s   lbC027842
                        jsr     lbC021A78
                        move.b  PARTY_X(a3),PARTY_ACTION_X(a3)
                        move.b  PARTY_Y(a3),PARTY_ACTION_Y(a3)
                        move.l  Bitmap_PtrB,-(sp)
                        move.l  Bitmap_PtrA,Bitmap_PtrB
lbC027876:
                        move.l  #$FF00FF,lbB0278F4
                        bsr     lbC0277C4
                        bsr     lbC0278A4
                        bmi     lbC02789C
                        clr.l   lbB0278F4
                        bsr     lbC0277C4
                        bsr     lbC0278A4
                        bpl.s   lbC027876
lbC02789C:              move.l  (sp)+,Bitmap_PtrB
                        rts

lbC0278A4:              move.w  #$95F,d6
lbC0278A8:              move.w  d6,-(sp)
                        jsr     lbC021B48
                        move.w  (sp)+,d6
                        tst.b   d0
                        bmi     lbC0278BE
                        dbra    d6,lbC0278A8
                        clr.b   d0
lbC0278BE:              rts

lbC0278C0:              move.b  d1,$6E(a3)
                        move.b  d2,$6F(a3)
                        move.b  PARTY_ACTION_X(a3),d0
                        add.b   d0,d0
                        add.b   $6E(a3),d0
                        move.b  d0,$70(a3)
                        move.b  PARTY_ACTION_Y(a3),d0
                        add.b   d0,d0
                        add.b   $6F(a3),d0
                        move.b  d0,$71(a3)
                        move.b  $70(a3),$7C(a3)
                        move.b  $71(a3),$7D(a3)
                        bra     lbC027192

lbB0278F4:              dc.b    0
lbB0278F5:              dc.b    0
lbB0278F6:              dc.b    0
lbB0278F7:              dc.b    0
lbB0278F8:              dc.b    $80,$40,$20,$10,8,4,2,1
lbB027900:              dc.b    7,6,5,4,3,2,1,0

                        SECTION EXODUS027908,CODE

; ----------------------------------------
; The title screen
title_screen:           lea     current_map,a0
                        move.l  #'NAME',d0
                        jsr     file_load                   ; lord british gfx coordinates
                        move.l  Bitmap_PtrB,a0
                        move.l  8(a0),a0                    ; first bitplane address
                        move.l  #'EXP0',d0                  ; Load the title pic (40*200*4)
                        jsr     file_load                   ; containing titles, various sprites + borders gfx
                        
                        lea     title_borders_blitdat,a4
                        moveq   #7-1,d7
draw_title_borders:     move.l  Bitmap_PtrB,a0
                        move.l  Bitmap_PtrA,a1
                        move.l  a4,a2
                        bsr     blit_bitmap
                        add.w   #6*2,a4                     ; (6 * 2)
                        dbra    d7,draw_title_borders

                        move.b  #$E3,lbB0281C9
                        move.b  #$9D,lbB0281C8
                        bsr     display_exodus_gfx
                        move.w  #9,d0
                        bsr     temporize
                        bmi     lbC0279B6
                        bsr     display_ultima3_gfx
                        move.w  #4,d0
                        bsr     temporize
                        bmi     lbC0279BC
                        bsr     display_by_gfx
                        move.w  #$31,d0
                        bsr     temporize
                        bmi     lbC0279BC
                        bsr     display_lord_british_gfx
                        bmi     lbC0279BC
                        move.w  #$63,d0
                        bsr     temporize
                        bmi     lbC0279BC
                        bsr     display_origin_copyright
                        bsr     title_dragon_animation
                        bsr     display_amiga_version_copyright
                        bra     lbC0279BC

lbC0279B6:              jsr     lbC02899C
lbC0279BC:
                        jsr     file_load_sounds
                        jmp     demo_screen

; ----------------------------------------
; Mute first audio channel
sound_mute_channel:     lea     $DFF000,a0
                        move.w  #1,$A4(a0)                  ; len
                        move.w  #124,$A6(a0)                ; period
                        move.w  #64,$A8(a0)
                        clr.w   empty_sound
                        move.l  #empty_sound,$A0(a0)
                        move.w  #$8001,$96(a0)
                        rts

display_exodus_gfx:     bsr.s   sound_mute_channel
                        move.w  #12,lbW0281C0
                        move.w  #4,lbW0281BE
                        move.b  #-1,lbB0281CB
                        clr.b   lbB0281CA
lbC027A1C:
                        move.w  #12,d0
                        move.w  #32,d1
                        move.w  #66,d2
                        bsr     draw_animated_titles
                        addq.b  #4,lbB0281CA
                        bne.s   lbC027A1C
                        move.w  #1,$DFF096
                        clr.b   lbB0281CB
                        move.w  #12,d0
                        move.w  #32,d1
                        move.w  #66,d2
                        bra     draw_animated_titles                    ; gfx last step (plain)

display_ultima3_gfx:    bsr     sound_mute_channel
                        move.w  #88,lbW0281C0
                        move.w  #7,lbW0281BE
                        move.b  #$FF,lbB0281CB
                        clr.b   lbB0281CA
lbC027A78:
                        move.w  #88,d0
                        move.w  #25,d1
                        move.w  #42,d2
                        bsr     draw_animated_titles
                        addq.b  #2,lbB0281CA
                        bne.s   lbC027A78
                        move.w  #1,$DFF096
                        jsr     lbC02899C
                        clr.b   lbB0281CB
                        move.w  #88,d0
                        move.w  #25,d1
                        move.w  #42,d2
                        bra     draw_animated_titles                    ; gfx last step (plain)

display_by_gfx:         move.l  Bitmap_PtrB,a0
                        move.l  Bitmap_PtrA,a1
                        lea     by_gfx_blitdat,a2
                        bra     blit_bitmap

by_gfx_blitdat:         dc.w    0,12,$18,$15,$50,$88

display_origin_copyright:
                        move.l  Bitmap_PtrB,a0
                        move.l  Bitmap_PtrA,a1
                        lea     origin_copyright_blitdat,a2
                        bra     blit_bitmap

origin_copyright_blitdat:
                        dc.w    8,$4F,$137,$55,8,$97

display_amiga_version_copyright:
                        move.l  Bitmap_PtrB,a0
                        move.l  Bitmap_PtrA,a1
                        lea     amiga_version_copyright_blitdat,a2
                        bra     blit_bitmap

amiga_version_copyright_blitdat:
                        dc.w    8,$9E,$137,$A3,8,$97

; src x1
; scr y1
; src x2
; src y2
; dest x
; dest y

title_borders_blitdat:  dc.w    $130,$C0,$137,$C7,0,$C0
                        dc.w    8,0,$137,7,8,$C0
                        dc.w    $138,8,$13F,$BF,0,8
                        dc.w    0,0,$13F,7,0,0
                        dc.w    $138,8,$13F,$C7,$138,8
                        dc.w    8,$83,$137,$97,8,$9E
                        dc.w    0,$A3,$5A,$B3,$72,$AF

display_lord_british_gfx:
                        move.l  #1,-(sp)
                        pea     Rastport
                        jsr     _SetAPen
                        addq.l  #8,sp
                        lea     current_map,a4
                        move.w  #266-1,d7
display_lord_british_gfx_loop:
                        moveq   #0,d0
                        move.l  d0,d2
                        move.l  #$C3,d1
                        move.b  (a4)+,d0
                        add.l   #23,d0
                        move.b  (a4)+,d2
                        sub.l   d2,d1
                        movem.l d0/d1,-(sp)
                        bsr     write_pixel
                        movem.l (sp)+,d0/d1
                        addq.l  #1,d0
                        bsr     write_pixel                 ; Double it on x
                        move.w  #3,d0
                        bsr     temporize
                        bmi     lbC027BFC
                        dbra    d7,display_lord_british_gfx_loop
                        clr.w   d0
lbC027BFC:              rts

; ----------------------------------------
; Draw a pixel on the screen
write_pixel:            move.l  d1,-(sp)
                        move.l  d0,-(sp)
                        pea     Rastport
                        jsr     _WritePixel
                        add.w   #12,sp
                        rts

; ----------------------------------------
; The title animation with the dragon
title_dragon_animation: moveq   #2,d6
                        moveq   #6,d5
                        swap    d5
                        moveq   #3-1,d2
lbC027C1C:
                        movem.w d2,-(sp)
                        lea     lbB028230,a0
                        move.w  d2,d0
                        add.w   d0,d0
                        move.w  0(a0,d0.w),d7
                        lea     title_dragon_blitdat,a0
lbC027C34:
                        moveq   #4-1,d4
                        move.w  d5,d3
lbC027C38:
                        bsr     draw_title_dragon_animation
                        move.w  #$19,d0
                        bsr     temporize
                        add.w   d6,d3
                        lea     title_dragon_blitdat,a0
                        dbra    d4,lbC027C38
                        add.w   d6,8(a0)
                        dbra    d7,lbC027C34
                        swap    d5                                  ; Swap between animation 0 and 6
                        neg.w   d6
                        movem.w (sp)+,d2
                        dbra    d2,lbC027C1C

                        moveq   #0,d3
                        bsr     draw_title_dragon_animation
                        move.w  #$C8,d0
                        bsr     temporize
                        lea     title_dragon_blitdat,a0
                        subq.w  #2,8(a0)
                        moveq   #4*2,d3
                        bsr     draw_title_dragon_animation
                        move.w  #20,d0
                        jsr     play_sound
                        move.w  #$C8,d0
                        bsr     temporize
                        moveq   #5*2,d3
                        bsr     draw_title_dragon_animation
                        move.w  #$C8,d0
                        bsr     temporize
                        rts

draw_title_dragon_animation:
                        lea     title_dragon_blitdat,a2
                        lea     title_dragon_animdat,a1
                        move.w  d3,d0
                        add.w   d0,d0
                        move.w  0(a1,d0.w),(a2)
                        move.w  (a2),4(a2)
                        add.w   #$5A,4(a2)
                        move.w  2(a1,d0.w),2(a2)
                        move.w  2(a2),6(a2)
                        add.w   #16,6(a2)
                        move.l  Bitmap_PtrB,a0
                        move.l  Bitmap_PtrA,a1
                        bsr     blit_bitmap
                        rts

temporize:              mulu    #300,d0
lbC027CF6:              move.l  d0,-(sp)
                        move.l  key_ascii,d0
                        btst    #7,d0
                        bne     lbC027D14
                        move.l  (sp)+,d0
                        subq.l  #1,d0
                        bpl.s   lbC027CF6
                        clr.w   d0
                        rts

lbC027D14:              and.l   #$7F,d0
                        move.l  d0,key_ascii
                        move.l  (sp)+,d0
                        move.w  #-1,d0
                        rts

draw_animated_titles:   movem.l a2/a3,-(sp)
                        move.w  d0,lbW0281C2
                        move.w  d1,lbW0281CC
                        move.w  lbW0281C0,-(sp)
                        move.w  d2,d6
lbC027D40:
                        move.w  lbW0281CC,d7
                        move.w  lbW0281C0,d2
                        mulu    #40,d2
                        move.l  Bitmap_PtrA,a2
                        move.l  8(a2),a2
                        add.l   d2,a2
                        move.w  lbW0281C2,d2
                        mulu    #40,d2
                        move.l  Bitmap_PtrB,a3
                        move.l  8(a3),a3
                        add.l   d2,a3
                        move.w  lbW0281BE,d4
lbC027D78:
                        move.l  a3,a1
                        add.w   d4,a1
                        move.b  (a1),lbB0281D2
                        move.b  8000(a1),lbB0281D3
                        move.b  16000(a1),lbB0281D4
                        move.b  24000(a1),lbB0281D5
                        tst.l   lbB0281D2
                        beq     lbC027E18
                        btst    #7,lbB0281CB
                        beq     lbC027E18
                        move.l  lbB0281D2,d5
                        move.b  lbB0281CA,d0
                        and.w   #$FF,d0
                        lsl.w   #2,d0
                        add.w   #$7C,d0
                        move.w  d0,$DFF0A6
                        move.b  lbB0281C9,d0
                        add.b   #$1D,d0
                        move.b  d0,d1
                        add.b   lbB0281C8,d0
                        move.b  d0,lbB0281C9
                        move.b  d1,lbB0281C8
                        move.b  d0,empty_sound
                        move.b  d1,lbB028239
                        cmp.b   lbB0281CA,d0
                        bcs     lbC027E12
                        beq     lbC027E12
                        clr.l   lbB0281D2
                        jmp     lbC027E18

lbC027E12:              move.l  d5,lbB0281D2
lbC027E18:              move.l  a2,a0
                        add.w   d4,a0
                        move.b  lbB0281D2,(a0)
                        move.b  lbB0281D3,8000(a0)
                        move.b  lbB0281D4,16000(a0)
                        move.b  lbB0281D5,24000(a0)
                        addq.w  #1,d4
                        dbra    d7,lbC027D78
                        addq.w  #1,lbW0281C0
                        addq.w  #1,lbW0281C2
                        dbra    d6,lbC027D40
                        move.w  (sp)+,lbW0281C0
                        movem.l (sp)+,a2/a3
                        rts

blit_front_to_back_buffer:
                        movem.l d0-d7/a0-a5,-(sp)
                        move.l  Bitmap_PtrA,a0
                        move.l  Bitmap_PtrB,a1
                        lea     blit_back_front_buffer_blitdat,a2
                        bra     lbC027E94

blit_back_to_front_buffer:
                        movem.l d0-d7/a0-a5,-(sp)
                        move.l  Bitmap_PtrB,a0
                        move.l  Bitmap_PtrA,a1
                        lea     blit_back_front_buffer_blitdat,a2
lbC027E94:              bsr     blit_bitmap
                        movem.l (sp)+,d0-d7/a0-a5
                        rts

lbC027E9E:              move.l  #$50,d1
                        bra     lbC027EBC

lbC027EA8:              move.l  #3,d1
                        moveq   #-1,d0
                        bra     lbC027EBC

blit_bitmap:            move.l  #$C0,d1
                        moveq   #-1,d0
lbC027EBC:              clr.l   -(sp)                           ; custom temporary array
                        move.l  d0,-(sp)                        ; mask
                        move.l  d1,-(sp)                        ; minterm
                        move.w  6(a2),d0
                        sub.w   2(a2),d0
                        addq.w  #1,d0
                        move.l  d0,-(sp)                        ; size y
                        move.w  4(a2),d0
                        sub.w   (a2),d0
                        addq.w  #1,d0
                        move.l  d0,-(sp)                        ; size x
                        move.w  10(a2),d0
                        move.l  d0,-(sp)                        ; dst y
                        move.w  8(a2),d0
                        move.l  d0,-(sp)                        ; dst x
                        move.l  a1,-(sp)                        ; dst bitmap
                        move.w  2(a2),d0
                        move.l  d0,-(sp)                        ; src y
                        move.w  (a2),d0
                        move.l  d0,-(sp)                        ; src x
                        move.l  a0,-(sp)                        ; src bitmap
                        jsr     _BltBitMap
                        add.w   #$2C,sp
                        jsr     _WaitTOF
                        rts

; ----------------------------------------
; Demo sequence 
draw_demo_screen:       move.l  #'DEMO',d0
                        lea     lbL022420,a0
                        jsr     file_load
                        move.l  #'DEM1',d0
                        lea     demo_dat_buffer,a0
                        jsr     file_load
                        move.w  #511,lbW0281BC
                        or.b    #$82,sound_output_state

lbC027F40:              jsr     turn_cursor_sprite_off
                        move.l  Bitmap_PtrA,a0
                        move.l  a0,a1
                        lea     demo_screen_middle_border_blitdat,a2
                        bsr     blit_bitmap
                        move.l  #$FF00FFFF,letter_mask
                        move.b  #14,TEXT_X(a3)
                        move.b  #11,TEXT_Y(a3)
                        lea     ULTIMAIII_MSG,a4
                        bsr     display_flat_message
                        move.b  #12,TEXT_X(a3)
                        move.b  #24,TEXT_Y(a3)
                        lea     PRESSSPACE_MSG,a4
                        bsr     display_flat_message
                        bsr     blit_front_to_back_buffer
                        move.w  lbW0281BC,d7
demo_screen_loop:
                        addq.w  #1,d7
                        cmp.w   #512,d7
                        bcs     lbC027FA4
                        moveq   #0,d7
lbC027FA4:
                        lea     demo_dat_buffer,a0
                        lea     demo_dat_buffer+512,a1
                        lea     lbL022420,a2
                        moveq   #0,d1
                        move.b  0(a0,d7.w),d1
                        move.b  0(a1,d7.w),d0
                        cmp.w   #$FF,d1
                        bne     lbC027FCE
                        move.b  d0,d5
                        bra     lbC027FDC

lbC027FCE:              move.b  d0,0(a2,d1.w)
                        cmp.b   #16,d0
                        bcs.s   demo_screen_loop
                        move.b  #1,d5
lbC027FDC:
                        bsr     demo_screen_display
                        bsr     demo_screen_display
                        bsr     demo_screen_display
                        bsr     demo_screen_display
                        subq.b  #1,d5
                        bne.s   lbC027FDC

                        tst.b   flag_music_end
                        bne     lbC028022

                        move.w  current_demo_screen_music,d0
                        and.w   #$f,d0
                        lea     demo_screen_music_sequence,a0
                        move.b  0(a0,d0.w),d0
                        jsr     start_new_music
                        clr.b   lbB028FDC
                        addq.w  #1,current_demo_screen_music
lbC028022:
                        moveq   #0,d0
                        bsr     temporize
                        bpl     demo_screen_loop

                        move.w  d7,lbW0281BC
                        jsr     reset_music
                        move.l  Bitmap_PtrA,a0
                        move.l  a0,a1
                        ; Remove the "<press space>" message
                        lea     demo_screen_bottom_border_blitdat,a2
                        bsr     blit_bitmap
                        jsr     turn_cursor_sprite_on
                        rts

display_flat_message:   move.b  #$1D,d0
                        jsr     lbC020E90
                        jsr     lbC01F756
                        move.b  #$1F,d0
                        jsr     lbC020E90
                        rts

demo_screen_display:    movem.l d0-d7/a0-a5,-(sp)
                        jsr     lbC02133E
                        jsr     lbC021496
                        jsr     lbC021386
                        lea     lbL022420,a0
                        lea     lbL0224D0,a3
                        lea     lbB0214D0,a1
                        move.w  #$78,d0
lbC02809A:
                        move.b  0(a0,d0.w),d1
                        cmp.b   #$20,d1
                        bcs     lbC0280BC
                        cmp.b   #$40,d1
                        bcc     lbC0280BC
                        btst    #7,0(a1,d0.w)
                        beq     lbC0280BC
                        add.b   #$60,d1
lbC0280BC:
                        move.b  d1,0(a3,d0.w)
                        dbra    d0,lbC02809A
                        move.l  Bitmap_PtrB,a4
                        lea     8(a4),a4
                        movem.l (a4)+,a0-a3
                        add.w   #$F00,a0
                        add.w   #$F00,a1
                        add.w   #$F00,a2
                        add.w   #$F00,a3
                        lea     lbL0224D0,a4
                        lea     lbL0240F0,a5
                        move.w  #40,d3
                        moveq   #6-1,d6
                        clr.w   d4
lbC0280F6:
                        clr.w   d5
                        moveq   #19-1,d7
lbC0280FA:
                        move.w  d4,d1
                        add.w   d5,d1
                        clr.w   d0
                        move.b  (a4)+,d0
                        lsl.w   #6,d0
                        moveq   #15,d2
lbC028106:
                        move.b  0(a5,d0.w),1(a0,d1.w)
                        move.b  1(a5,d0.w),2(a0,d1.w)
                        move.b  $20(a5,d0.w),1(a1,d1.w)
                        move.b  $21(a5,d0.w),2(a1,d1.w)
                        move.b  $40(a5,d0.w),1(a2,d1.w)
                        move.b  $41(a5,d0.w),2(a2,d1.w)
                        move.b  $60(a5,d0.w),1(a3,d1.w)
                        move.b  $61(a5,d0.w),2(a3,d1.w)
                        addq.w  #2,d0
                        add.w   d3,d1
                        dbra    d2,lbC028106
                        addq.w  #2,d5
                        dbra    d7,lbC0280FA
                        add.w   #$280,d4
                        dbra    d6,lbC0280F6
                        jsr     swap_view
                        jsr     _WaitTOF
                        movem.l (sp)+,d0-d7/a0-a5
                        rts

lbC028162:              movem.l d1/a2-a4,-(sp)
                        jsr     lbC01CF00
                        and.w   #$FF,d0
                        lsl.w   #6,d0
                        move.l  Bitmap_PtrA,a4
                        lea     8(a4),a4
                        movem.l (a4)+,a0-a3
                        move.w  #$146,d1
                        lea     lbL0240F0,a4
                        add.w   d0,a4
                        move.w  #15,d0
lbC028190:
                        move.w  0(a4),0(a0,d1.w)
                        move.w  32(a4),0(a1,d1.w)
                        move.w  64(a4),0(a2,d1.w)
                        move.w  96(a4),0(a3,d1.w)
                        addq.w  #2,a4
                        add.w   #$28,d1
                        dbra    d0,lbC028190
                        movem.l (sp)+,d1/a2-a4
                        rts

                        SECTION EXODUS0281BC,bss_c

lbW0281BC:              ds.w    1
lbW0281BE:              ds.w    1
lbW0281C0:              ds.w    1
lbW0281C2:              ds.w    3
lbB0281C8:              ds.b    1
lbB0281C9:              ds.b    1
lbB0281CA:              ds.b    1
lbB0281CB:              ds.b    1
lbW0281CC:              ds.w    3
lbB0281D2:              ds.b    1
lbB0281D3:              ds.b    1
lbB0281D4:              ds.b    1
lbB0281D5:              ds.b    1
current_demo_screen_music:
                        ds.w    1

                        SECTION EXODUS0281D8,data_c

blit_back_front_buffer_blitdat:
                        dc.w    0,0,319,199,0,0
demo_screen_middle_border_blitdat:  
                        dc.w    8,0,311,8,8,88
demo_screen_bottom_border_blitdat:
                        dc.w    8,0,311,7,8,192

title_dragon_blitdat:   dc.w    0,0,90,16,114,175
title_dragon_animdat:   dc.w    0,163
                        dc.w    0,183
                        dc.w    96,163
                        dc.w    96,183
                        
                        dc.w    192,183                     ; dragon's flame
                        dc.w    192,163                     ; pack of adventurers
lbB028230:              dc.b    0,10,0,5,0,7,0,0
empty_sound:            dc.b    0
lbB028239:              dc.b    0
ULTIMAIII_MSG:          dc.b    "ULTIMA III",0
PRESSSPACE_MSG:         dc.b    "PRESS <SPACE>",0
demo_screen_music_sequence:
                        dc.b    1,2,3,3,4,4,5,5,6,6,7,7,8,10,10,11,0

                        SECTION EXODUS028264,CODE

scroll_text:            bsr     scroll_1_text_line
                        move.l  Bitmap_PtrB,Rastport_Bitmap
                        bsr     scroll_1_text_line
                        move.l  Bitmap_PtrA,Rastport_Bitmap
                        rts

scroll_1_text_line:     move.l  #199,-(sp)
                        move.l  #319,-(sp)
                        move.l  #136,-(sp)
                        move.l  #192,-(sp)
                        move.l  #8,-(sp)
                        clr.l   -(sp)
                        pea     Rastport
                        jsr     _ScrollRaster
                        add.w   #$1C,sp
                        rts

; ----------------------------------------
; Vertical blank interrupt
int_vector_server:      move.l  mouse_pointer,a0
                        move.l  mouse_current_pointer,d0
                        beq     mouse_no_new_cursor
                        move.l  d0,a1
                        move.l  4(a0),4(a1)
                        move.l  a1,a0
                        move.l  a0,mouse_pointer
                        clr.l   mouse_current_pointer

mouse_no_new_cursor:    move.w  $DFF00A,d0
                        move.w  old_mouse_y,d1
                        move.b  d0,old_mouse_y+1
                        sub.b   d0,d1
                        beq     lbC028356
                        bpl     lbC028346
                        neg.b   d1
                        add.w   d1,mouse_x
                        cmp.w   #319,mouse_x
                        bls     lbC028356
                        move.w  #319,mouse_x
                        bra     lbC028356

lbC028346:              sub.w   d1,mouse_x
                        bpl     lbC028356
                        clr.w   mouse_x
lbC028356:
                        move.w  mouse_x,d1
                        add.w   #$80,d1
                        sub.w   (a0),d1
                        lsr.w   #1,d1
                        move.b  d1,5(a0)
                        and.b   #$FE,7(a0)
                        move.w  mouse_x,d1
                        sub.w   (a0),d1
                        and.b   #1,d1
                        or.b    d1,7(a0)
                        lsr.w   #8,d0
                        move.w  old_mouse_x,d1
                        move.b  d0,old_mouse_x+1
                        sub.b   d0,d1
                        beq     lbC0283C6
                        bpl     lbC0283B6
                        neg.b   d1
                        add.w   d1,mouse_y
                        cmp.w   #199,mouse_y
                        bls     lbC0283C6
                        move.w  #199,mouse_y
                        bra     lbC0283C6

lbC0283B6:              sub.w   d1,mouse_y
                        bpl     lbC0283C6
                        clr.w   mouse_y
lbC0283C6:              move.w  mouse_y,d0
                        add.w   #$2C,d0
                        sub.w   2(a0),d0
                        move.b  d0,4(a0)
                        add.b   #$10,d0
                        move.b  d0,6(a0)
                        tst.w   cursor_sprite_onoff
                        beq     lbC0283F4
                        addq.w  #4,a0
                        move.l  a0,$DFF120
lbC0283F4:
                        move.l  key_ascii,d0
                        btst    #7,d0
                        bne     lbC028448
                        moveq   #0,d0
                        move.b  $BFEC01,d0
                        tst.w   key_state
                        bne     lbC028452
                        btst    #0,d0
                        beq     lbC028448
                        move.w  #1,key_state
                        move.w  #15,key_repeat_pause
lbC02842C:              move.w  d0,key_value
                        not.b   d0
                        lsr.b   #1,d0
                        cmp.b   #$62,d0
                        beq     lbC02845A
                        or.b    #$80,d0
                        move.l  d0,key_ascii
lbC028448:              jsr     lbC0289E8
                        moveq   #0,d0
                        rts

lbC028452:              btst    #0,d0
                        bne     lbC028462
lbC02845A:              clr.w   key_state
                        bra.s   lbC028448

lbC028462:              cmp.w   key_value,d0
                        bne.s   lbC02842C
                        subq.w  #1,key_repeat_pause
                        bne.s   lbC028448
                        move.w  #8,key_repeat_pause
                        bra.s   lbC02842C
; ----------------------------------------

turn_cursor_sprite_on:  move.w  #-1,cursor_sprite_onoff
                        rts

turn_cursor_sprite_off: clr.w   cursor_sprite_onoff
                        rts

translate_key:          lea     input_keys_table,a0
                        move.w  #62-1,d1
lbC02849A:              cmp.b   (a0)+,d0
                        dbeq    d1,lbC02849A
                        beq     translate_valid_key
                        clr.w   d0
                        rts

translate_valid_key:    move.w  #61,d0
                        sub.w   d1,d0
                        lea     output_keys_table,a0
                        move.b  0(a0,d0.w),d0
                        rts

mouse_set_new_cursor:   lea     mouse_cursor_directions,a0
                        mulu    #76,d0
                        add.w   d0,a0
                        move.l  a0,mouse_current_pointer
                        ; Wait until the pointer has been set in the irq
synchro_mouse_pointer:  tst.l   mouse_current_pointer
                        bne.s   synchro_mouse_pointer
                        rts

                        SECTION mouse_key_bss,bss_c

key_ascii:              ds.l    1
key_value:              ds.w    1
key_state:              ds.w    1
save_mouse_x:           ds.w    1
save_mouse_y:           ds.w    1

                        SECTION mouse_key_dats,data_c

key_repeat_pause:       dc.w    15
cursor_sprite_onoff:    dc.w    0
old_mouse_x:            dc.w    100
old_mouse_y:            dc.w    100

mouse_x:                dc.w    100
mouse_y:                dc.w    100
mouse_pointer:          dc.l    mouse_cursor_hand
mouse_current_pointer:  dc.l    0
mouse_cursor_directions:
                        dc.w    8,8
                        dc.w    $90A4,$A000
                        dc.w    %00000000000000000,%00000000000000000
                        dc.w    %00000000000000000,%00000000000000000
                        dc.w    %00000000110000000,%00000000110000000
                        dc.w    %00000001111000000,%00000001111000000
                        dc.w    %00000011001100000,%00000011111100000
                        dc.w    %00000110000110000,%00000111111110000
                        dc.w    %00001101001011000,%00001101111011000
                        dc.w    %00011001001001100,%00011001111001100
                        dc.w    %00000001001000000,%00000001111000000
                        dc.w    %00000001001000000,%00000001111000000
                        dc.w    %00000001001000000,%00000001111000000
                        dc.w    %00000001001000000,%00000001111000000
                        dc.w    %00000001001000000,%00000001111000000
                        dc.w    %00000001111000000,%00000001111000000
                        dc.w    %00000000000000000,%00000000000000000
                        dc.w    %00000000000000000,%00000000000000000
                        dc.w    %00000000000000000,%00000000000000000
                        
                        dc.w    8,8
                        dc.w    $90A4,$A000
                        dc.w    %00000000000000000,%00000000000000000
                        dc.w    %00000000000000000,%00000000000000000
                        dc.w    %00000001111000000,%00000001111000000
                        dc.w    %00000001001000000,%00000001111000000
                        dc.w    %00000001001000000,%00000001111000000
                        dc.w    %00000001001000000,%00000001111000000
                        dc.w    %00000001001000000,%00000001111000000
                        dc.w    %00000001001000000,%00000001111000000
                        dc.w    %00011001001001100,%00011001111001100
                        dc.w    %00001101001011000,%00001101111011000
                        dc.w    %00000110000110000,%00000111111110000
                        dc.w    %00000011001100000,%00000011111100000
                        dc.w    %00000001111000000,%00000001111000000
                        dc.w    %00000000110000000,%00000000110000000
                        dc.w    %00000000000000000,%00000000000000000
                        dc.w    %00000000000000000,%00000000000000000
                        dc.w    %00000000000000000,%00000000000000000

                        dc.w    8,8
                        dc.w    $90A4,$A000
                        dc.w    %00000000000000000,%00000000000000000
                        dc.w    %00000000000000000,%00000000000000000
                        dc.w    %00000000100000000,%00000000100000000
                        dc.w    %00000001100000000,%00000001100000000
                        dc.w    %00000011000000000,%00000011000000000
                        dc.w    %00000110000000000,%00000110000000000
                        dc.w    %00001101111111100,%00001111111111100
                        dc.w    %00011000000000100,%00011111111111100
                        dc.w    %00011000000000100,%00011111111111100
                        dc.w    %00001101111111100,%00001111111111100
                        dc.w    %00000110000000000,%00000110000000000
                        dc.w    %00000011000000000,%00000011000000000
                        dc.w    %00000001100000000,%00000001100000000
                        dc.w    %00000000100000000,%00000000100000000
                        dc.w    %00000000000000000,%00000000000000000
                        dc.w    %00000000000000000,%00000000000000000
                        dc.w    %00000000000000000,%00000000000000000
                        
                        dc.w    8,8
                        dc.w    $90A4,$A000
                        dc.w    %00000000000000000,%00000000000000000
                        dc.w    %00000000000000000,%00000000000000000
                        dc.w    %00000000010000000,%00000000010000000
                        dc.w    %00000000011000000,%00000000011000000
                        dc.w    %00000000001100000,%00000000001100000
                        dc.w    %00000000000110000,%00000000000110000
                        dc.w    %00011111111011000,%00011111111111000
                        dc.w    %00010000000001100,%00011111111111100
                        dc.w    %00010000000001100,%00011111111111100
                        dc.w    %00011111111011000,%00011111111111000
                        dc.w    %00000000000110000,%00000000000110000
                        dc.w    %00000000001100000,%00000000001100000
                        dc.w    %00000000011000000,%00000000011000000
                        dc.w    %00000000010000000,%00000000010000000
                        dc.w    %00000000000000000,%00000000000000000
                        dc.w    %00000000000000000,%00000000000000000
                        dc.w    %00000000000000000,%00000000000000000

mouse_cursor_hand:      dc.w    0,0
                        dc.w    $90A4,$A000
                        dc.w    %00000000000000000,%00011000000000000
                        dc.w    %00011000000000000,%00111110000000000
                        dc.w    %00001110000000000,%00111111000000000
                        dc.w    %00000011000000000,%00001111110000000
                        dc.w    %00000001110000000,%00000111111000000
                        dc.w    %00000110100000000,%00011111111111000
                        dc.w    %00001011011111000,%00011111111111100
                        dc.w    %00001100111011000,%00111111111111100
                        dc.w    %00110110000111100,%01111111111111110
                        dc.w    %00011000010111100,%01111111111111110
                        dc.w    %00000001110111100,%00111111111111111
                        dc.w    %00001111111011110,%00011111111111111
                        dc.w    %00000111111111110,%00001111111111111
                        dc.w    %00000001110111110,%00000111111111111
                        dc.w    %00000000001111111,%00000001111111111
                        dc.w    %00000000000111111,%00000000011111111
                        dc.w    %00000000000000000,%00000000000000000
                        
                        dc.w    8,8
                        dc.w    $90A4,$A000
                        dc.w    %00000000000000000,%00000000000000000
                        dc.w    %00000001110000000,%00000001110000000
                        dc.w    %00000001010000000,%00000001110000000
                        dc.w    %00000001010000000,%00000001110000000
                        dc.w    %00000001010000000,%00000001110000000
                        dc.w    %00000001010000000,%00000001110000000
                        dc.w    %00000001010000000,%00000001110000000
                        dc.w    %01111111011111110,%01111111111111110
                        dc.w    %01000000000000010,%01111111111111110
                        dc.w    %01111111011111110,%01111111111111110
                        dc.w    %00000001010000000,%00000001110000000
                        dc.w    %00000001010000000,%00000001110000000
                        dc.w    %00000001010000000,%00000001110000000
                        dc.w    %00000001010000000,%00000001110000000
                        dc.w    %00000001010000000,%00000001110000000
                        dc.w    %00000001110000000,%00000001110000000
                        dc.w    %00000000000000000,%00000000000000000

input_keys_table:       dc.b    $4C,$4D,$4F,$4E,$50,$51,$10,$11,$12,$13,$14,$15
                        dc.b    $16,$17,$18,$19,$20,$21,$22,$23,$24,$25,$26,$27
                        dc.b    $28,$29,$31,$32,$33,$34,$35,$36,$37,$38,$39,$40
                        dc.b    $41,$44,$46,$45,$43,1,2,3,4,5,6,7,8,9,10,$1D,$1E
                        dc.b    $1F,$2D,$2E,$2F,$3D,$3E,$3F,15

output_keys_table:      dc.b    $81,$8A,$88,$95,$12,$13,$51,$57,$45,$52,$54,$59
                        dc.b    $55,$49,$4F,$50,$41,$53,$44,$46,$47,$48,$4A,$4B
                        dc.b    $4C,$3B,$5A,$58,$43,$56,$42,$4E,$4D,$2C,$2E,$20,8
                        dc.b    13,8,$1B,13,$31,$32,$33,$34,$35,$36,$37,$38,$39
                        dc.b    $30,$31,$32,$33,$34,$35,$36,$37,$38,$39,$30,0,0

; Audio related
                        section Audio,CODE

play_sound:             btst    #0,sound_output_state
                        beq     lbC0287D2
                        cmp.w   #14,d0
                        beq     lbC0287D4
                        cmp.w   #18,d0
                        beq     lbC0287E2
                        cmp.w   #21,d0
                        bhi     lbC0287D2
                        movem.l d0/d1/a0/a1,-(sp)
                        move.w  #$96,d1
                        cmp.w   #10,d0
                        beq     lbC028780
                        move.w  #$7C,d1
                        cmp.w   #15,d0
                        beq     lbC028780
                        move.w  #$166,d1

lbC028780:              lea     $DFF000,a1
                        move.w  #8,$96(a1)
                        move.w  #$400,$9A(a1)
                        move.w  #$400,$9C(a1)
                        lsl.w   #2,d0
                        lea     lbL028F02,a0
                        move.l  0(a0,d0.w),a0
                        move.l  (a0)+,d0
                        beq     lbC0287CE
                        lsr.w   #1,d0
                        move.w  d0,$D4(a1)
                        move.w  d1,$D6(a1)
                        addq.l  #2,a0
                        move.l  a0,$D0(a1)
                        bclr    #0,sound_finished_state
                        move.w  #$8400,$9A(a1)
                        move.w  #$8008,$96(a1)
lbC0287CE:              movem.l (sp)+,d0/d1/a0/a1
lbC0287D2:              rts

lbC0287D4:              movem.l d0-d2/a0-a2,-(sp)
                        lea     lbW02888A,a2
                        bra     lbC0287EC

lbC0287E2:              movem.l d0-d2/a0-a2,-(sp)
                        lea     lbW0288A2,a2
lbC0287EC:              lea     $DFF000,a1
                        move.w  #8,$96(a1)
                        move.w  #$400,$9A(a1)
                        move.w  #$400,$9C(a1)
                        move.w  #0,$D8(a1)
                        move.w  sound_output_state,-(sp)
                        bclr    #1,sound_output_state
                        move.l  lbL028EA2,$D0(a1)
                        move.w  lbW028E92,$D4(a1)
                        lea     audio_notes,a0
                        move.w  4(a0),$D6(a1)
                        move.w  #$8008,$96(a1)
                        move.w  #4-1,d0
lbC02883E:              move.w  (a2)+,d1
                        move.w  0(a0,d1.w),$D6(a1)
                        move.w  (a2)+,d2
                        move.w  sound_volume,d1
                        bsr     lbC028878
                        move.w  (a2)+,d2
                        clr.w   d1
                        bsr     lbC028878
                        dbra    d0,lbC02883E
                        move.w  #8,$96(a1)
                        move.w  (sp)+,sound_output_state
                        move.w  sound_volume,$D8(a1)
                        movem.l (sp)+,d0-d2/a0-a2
                        rts

lbC028878:              move.w  d1,$D8(a1)
lbC02887C:              move.w  #10000,d1
lbC028880:              dbra    d1,lbC028880
                        dbra    d2,lbC02887C
                        rts

lbW02888A:              dc.w    4,$32,5,4,$32,5,4,$14,3,4,$3C,1
lbW0288A2:              dc.w    4,$28,5,4,$28,5,4,$1E,3,14,$50,1

Audio_Interrupt:        bclr    #0,sound_finished_state
                        bne     sound_dma_off
                        bset    #0,sound_finished_state
                        move.w  #$400,$9C(a0)
                        rts

sound_dma_off:          move.w  #8,$96(a0)
                        move.w  #$400,$9A(a0)
                        move.w  #$400,$9C(a0)
                        rts

Audio_Int_Vector:       dc.l    0,0
                        dc.b    2,0
                        dc.l    0
                        dc.l    0
                        dc.l    Audio_Interrupt

init_music:             pea     Audio_Int_Vector(pc)
                        move.l  #10,-(sp)
                        jsr     _SetIntVector
                        addq.l  #8,sp
                        
                        move.l  #'FLAM',d0
                        lea     sound_flame,a0
                        jsr     file_load
                        move.l  #'MDTA',d0
                        lea     music_datas,a0
                        jsr     file_load
                        lea     music_datas,a4
                        moveq   #0,d2
                        clr.b   flag_music_end
                        clr.b   current_music_number
                        clr.b   lbB028FDC
                        moveq   #0,d1
                        move.b  0(a4,d2.w),d1           ; 16 in the file
                        move.b  d1,max_music_number
                        subq.w  #1,d1
                        lea     lbL029028,a0
                        addq.w  #1,d2
lbC028966:              move.b  1(a4,d2.w),d0
                        lsl.w   #8,d0
                        move.b  0(a4,d2.w),d0
                        move.w  d0,(a0)+
                        addq.w  #2,d2
                        dbra    d1,lbC028966
                        bset    #0,sound_output_state
                        rts

file_load_sounds:       lea     samples_files_table,a4
                        move.w  #16-1,d7
lbC02898C:              move.l  (a4)+,a0
                        move.l  (a4)+,d0
                        jsr     file_load
                        dbra    d7,lbC02898C
                        rts

lbC02899C:              lea     $DFF000,a0
                        lea     lbL034B9E,a1
                        move.l  a1,$A0(a0)
                        move.l  a1,$B0(a0)
                        move.l  a1,$C0(a0)
                        move.w  #128,$A4(a0)
                        move.w  #128,$B4(a0)
                        move.w  #128,$C4(a0)
                        move.w  #0,$A8(a0)
                        move.w  #0,$B8(a0)
                        move.w  #0,$C8(a0)
                        move.w  #63,$D8(a0)
                        move.w  #63,sound_volume
                        rts

; ----------------------------------------
; Probably the replay routine
lbC0289E8:              btst    #7,sound_output_state
                        beq     lbC028A00
                        movem.l d0-d7/a0-a5,-(sp)
                        bsr     lbC028A02
                        movem.l (sp)+,d0-d7/a0-a5
lbC028A00:              rts

lbC028A02:              moveq   #0,d0
                        move.b  current_music_number,d0
                        cmp.b   max_music_number,d0
                        bcs     lbC028A16
                        moveq   #0,d0
lbC028A16:              cmp.b   flag_music_end,d0
                        bne     lbC028A24
                        bra     lbC028AF6

lbC028A24:              move.b  d0,flag_music_end
                        add.w   d0,d0
                        lea     lbL029028,a1
                        move.w  0(a1,d0.w),d2
                        move.w  d2,d3
                        lea     music_datas,a4
                        moveq   #0,d0
                        move.b  0(a4,d2.w),d0
                        move.w  d0,lbW0290AA
                        subq.w  #1,d0
                        move.w  d0,d1
                        add.w   d0,d0
                        move.w  d0,lbW0290AC
                        lea     lbL028FE8,a1
                        addq.w  #1,d2
lbC028A5E:              move.b  1(a4,d2.w),d0
                        lsl.w   #8,d0
                        move.b  0(a4,d2.w),d0
                        add.w   d3,d0
                        move.w  d0,(a1)+
                        addq.w  #2,d2
                        dbra    d1,lbC028A5E
                        moveq   #0,d0
                        move.b  0(a4,d2.w),d0
                        move.w  d0,lbW0290AE
                        beq     lbC028AAE
                        add.w   d0,d0
                        move.w  d0,lbW0290B0
                        lea     lbL029068,a1
                        moveq   #0,d1
                        addq.w  #1,d2

lbC028A94:              move.b  1(a4,d2.w),d0
                        lsl.w   #8,d0
                        move.b  0(a4,d2.w),d0
                        add.w   d3,d0
                        move.w  d0,(a1)+
                        addq.w  #2,d2
                        addq.w  #2,d1
                        cmp.w   lbW0290B0,d1
                        bne.s   lbC028A94
lbC028AAE:              move.w  lbW0290AC,d1
                        lea     lbW028EB2,a0
                        moveq   #0,d0
lbC028ABC:              move.b  d0,1(a0,d1.w)
                        move.b  d0,$39(a0,d1.w)
                        move.b  #1,$38(a0,d1.w)
                        subq.b  #2,d1
                        bpl.s   lbC028ABC
                        move.w  lbW0290AA,d2
                        lea     lbL028E6C,a1
                        move.b  0(a1,d2.w),d0
                        lea     $DFF000,a1
                        move.w  #7,$96(a1)
                        and.w   #$FF,d0
                        or.w    #$8000,d0
                        move.w  d0,$96(a1)
lbC028AF6:              lea     lbW028EB2,a0
                        move.w  lbW0290AC,d1
lbC028B02:              cmp.b   #$10,$31(a0,d1.w)
                        bcs     lbC028B10
                        bra     lbC028C26

lbC028B10:              move.b  $41(a0,d1.w),d0
                        beq     lbC028C26
                        cmp.b   #3,d0
                        beq     lbC028BEA
                        bhi     lbC028BB8
                        cmp.b   #1,d0
                        bne     lbC028B72
                        move.b  8(a0,d1.w),d2
                        move.b  $30(a0,d1.w),d0
                        move.w  #0,ccr
                        addx.b  d2,d0
                        move.b  d0,$30(a0,d1.w)
                        move.b  9(a0,d1.w),d2
                        move.b  $31(a0,d1.w),d0
                        addx.b  d2,d0
                        move.b  d0,$31(a0,d1.w)
                        cmp.b   $29(a0,d1.w),d0
                        bne     lbC028B5C
                        move.b  $30(a0,d1.w),d0
                        cmp.b   $28(a0,d1.w),d0
lbC028B5C:              bcs     lbC028BEA
                        beq     lbC028B6A
                        move.w  $28(a0,d1.w),$30(a0,d1.w)
lbC028B6A:              addq.b  #1,$41(a0,d1.w)
                        bra     lbC028BEA

lbC028B72:              move.b  $10(a0,d1.w),d2
                        move.b  $30(a0,d1.w),d0
                        move.w  #0,ccr
                        subx.b  d2,d0
                        move.b  d0,$30(a0,d1.w)
                        move.b  $11(a0,d1.w),d2
                        move.b  $31(a0,d1.w),d0
                        subx.b  d2,d0
                        move.b  d0,$31(a0,d1.w)
                        cmp.b   $19(a0,d1.w),d0
                        bne     lbC028BA6
                        move.b  $30(a0,d1.w),d0
                        cmp.b   $18(a0,d1.w),d0
                        beq     lbC028BB0
lbC028BA6:              bhi     lbC028BEA
                        move.w  $18(a0,d1.w),$30(a0,d1.w)
lbC028BB0:              addq.b  #1,$41(a0,d1.w)
                        bra     lbC028BEA

lbC028BB8:              move.b  $20(a0,d1.w),d2
                        move.b  $30(a0,d1.w),d0
                        move.w  #0,ccr
                        subx.b  d2,d0
                        move.b  d0,$30(a0,d1.w)
                        move.b  $21(a0,d1.w),d2
                        move.b  $31(a0,d1.w),d0
                        subx.b  d2,d0
                        bcc     lbC028BE6
                        moveq   #0,d0
                        move.w  d0,$30(a0,d1.w)
                        move.b  d0,$41(a0,d1.w)
                        bra     lbC028C26

lbC028BE6:              move.b  d0,$31(a0,d1.w)
lbC028BEA:              move.b  $38(a0,d1.w),d0
                        cmp.b   0(a0,d1.w),d0
                        bcc     lbC028BFC
                        move.b  #4,$41(a0,d1.w)
lbC028BFC:              move.w  d1,d3
                        lsl.w   #3,d3
                        lea     $DFF0A8,a2
                        moveq   #0,d0
                        move.b  $31(a0,d1.w),d0
                        move.w  music_volume,d4
                        lsl.w   d4,d0
                        btst    #1,sound_output_state
                        bne     lbC028C22
                        clr.w   d0
lbC028C22:              move.w  d0,0(a2,d3.w)
lbC028C26:              subq.w  #2,d1
                        bmi     lbC028C30
                        bra     lbC028B02

lbC028C30:              move.w  lbW0290AC,d1
                        move.b  #$FF,lbB0290B6
lbC028C3E:              tst.b   $39(a0,d1.w)
                        bne     lbC028CEA
                        move.b  #0,lbB0290B6
                        subq.b  #1,$38(a0,d1.w)
                        bne     lbC028CEA
lbC028C56:              bsr     lbC028CFC
                        tst.b   d0
                        beq     lbC028D28
                        bpl     lbC028C7C
                        and.b   #$7F,d0
                        beq     lbC028C72
                        move.b  d0,$40(a0,d1.w)
                        bne.s   lbC028C56
lbC028C72:              move.b  #4,$41(a0,d1.w)
                        bra     lbC028CE4

lbC028C7C:              move.w  d1,d3
                        lsl.w   #3,d3
                        add.b   1(a0,d1.w),d0
                        sub.b   #12,d0
                        and.l   #$FF,d0
                        divu    #12,d0
                        lea     audio_notes,a1
                        lea     $DFF0A6,a2
                        swap    d0
                        add.w   d0,d0
                        move.w  0(a1,d0.w),0(a2,d3.w)
                        swap    d0
                        cmp.w   $48(a0,d1.w),d0
                        beq     lbC028CDE
                        move.w  d0,$48(a0,d1.w)
                        lea     lbW028E8E,a1
                        add.w   d0,d0
                        lea     $DFF0A4,a2
                        move.w  0(a1,d0.w),0(a2,d3.w)
                        add.w   d0,d0
                        lea     lbL028E9A,a1
                        lea     $DFF0A0,a2
                        move.l  0(a1,d0.w),0(a2,d3.w)
lbC028CDE:              move.b  #1,$41(a0,d1.w)
lbC028CE4:              move.b  $40(a0,d1.w),$38(a0,d1.w)
lbC028CEA:              subq.w  #2,d1
                        bpl     lbC028C3E
                        tst.b   lbB0290B6
                        bne     lbC028D16
                        rts

lbC028CFC:              lea     lbL028FE8,a1
                        lea     music_datas,a2
                        move.w  0(a1,d1.w),d2
                        move.b  0(a2,d2.w),d0
                        addq.w  #1,0(a1,d1.w)
                        rts

lbC028D16:              moveq   #0,d0
                        move.b  lbB028FDC,d0
                        move.b  d0,current_music_number
                        bra     lbC028A24

lbC028D28:              bsr.s   lbC028CFC
                        tst.b   d0
                        beq.s   lbC028D16
                        bmi     lbC028D66
                        move.b  d0,lbB0290B2
                        cmp.b   #$10,d0
                        bcs     lbC028C56
                        cmp.b   #$3A,d0
                        bcc     lbC028C56
                        move.w  d1,d7
                        add.b   lbB0290B2,d7
                        bsr.s   lbC028CFC
                        move.b  d0,-$10(a0,d7.w)
                        cmp.w   #$18,d7
                        bcs.s   lbC028D28
                        addq.w  #1,d7
                        bsr.s   lbC028CFC
                        move.b  d0,-$10(a0,d7.w)
                        bra.s   lbC028D28

lbC028D66:              cmp.b   #$80,d0
                        bne     lbC028DBC
                        bsr.s   lbC028CFC
                        moveq   #0,d2
                        move.b  d0,d2
                        cmp.w   lbW0290AE,d2
                        bcs     lbC028D82
                        bra     lbC028C56

lbC028D82:              add.w   d2,d2
                        lea     lbL029068,a1
                        move.w  0(a1,d2.w),d3
                        add.w   d1,d3
                        lea     lbL028FE8,a2
                        lea     music_datas,a3
                        move.b  0(a2,d1.w),0(a3,d3.w)
                        move.b  1(a2,d1.w),1(a3,d3.w)
                        move.w  lbW0290AA,d0
                        add.w   d0,d0
                        add.w   0(a1,d2.w),d0
                        move.w  d0,0(a2,d1.w)
                        bra     lbC028C56

lbC028DBC:              cmp.b   #$81,d0
                        bne     lbC028DF6
                        bsr     lbC028CFC
                        moveq   #0,d2
                        move.b  d0,d2
                        add.w   d2,d2
                        lea     lbL029068,a1
                        move.w  0(a1,d2.w),d0
                        add.w   d1,d0
                        lea     music_datas,a1
                        lea     lbL028FE8,a2
                        move.b  0(a1,d0.w),0(a2,d1.w)
                        move.b  1(a1,d0.w),1(a2,d1.w)
                        bra     lbC028C56

lbC028DF6:              cmp.b   #$82,d0
                        bne     lbC028E0E
                        move.b  #$FF,$39(a0,d1.w)
                        move.b  #4,$41(a0,d1.w)
                        bra     lbC028CEA

lbC028E0E:              cmp.b   #$83,d0
                        bne     lbC028E18
lbC028E16:              bra.s   lbC028E16

lbC028E18:              cmp.b   #$84,d0
                        bne     lbC028E2C
                        bsr     lbC028CFC
                        bsr     lbC028CFC
                        bra     lbC028C56

lbC028E2C:              cmp.b   #$85,d0
                        bne     lbC028E62
                        bsr     lbC028CFC
                        lea     ascii_MSG28,a1
                        and.b   0(a1,d1.w),d0
                        move.b  d0,lbB0290B2
                        move.b  lbB0290A8,d0
                        lea     lbB028E71,a1
                        and.b   0(a1,d1.w),d0
                        or.b    lbB0290B2,d0
                        bsr     lbC028E66
lbC028E62:              bra     lbC028C56

lbC028E66:              and.w   #$FF,d0
                        rts

                        SECTION EXODUS028E6C,data_c

lbL028E6C:              dc.l    $10307
ascii_MSG28:            dc.b    9
lbB028E71:              dc.b    $F6,$12,$ED,$24,$DB
audio_notes:            dc.w    428,404,382,360,340,321,303,286,270,254,240,226
lbW028E8E:              dc.w    128,64
lbW028E92:              dc.w    32,16,8,4
lbL028E9A:
                        dc.l    lbL034B9E
                        dc.l    lbL034C9E
lbL028EA2:
                        dc.l    lbL034D1E
                        dc.l    lbL034D5E
                        dc.l    lbL034D7E
                        dc.l    lbL034D8E

lbW028EB2:              dcb.w   4,$A00
                        dcb.w   4,15
                        dcb.w   4,1
                        dcb.w   4,8
                        dcb.w   4,$8000
                        dcb.w   4,10
                        dcb.w   8,0
                        dcb.w   4,$3C00
                        dcb.w   4,0

lbL028F02:              dc.l    lbL0290BA
                        dc.l    lbL031EBC
                        dc.l    lbL02974A
                        dc.l    lbL031EBC
                        dc.l    lbL0320CE
                        dc.l    lbL02A8C0
                        dc.l    lbL02CB5E
                        dc.l    lbL02DFEC
                        dc.l    lbL02F01E
                        dc.l    lbL02F936
                        dc.l    lbL02F936
                        dc.l    lbL02F01E
                        dc.l    sound_flame
                        dc.l    lbL02DFEC
                        dc.l    0
                        dc.l    sound_flame
                        dc.l    lbL02974A
                        dc.l    lbL02974A
                        dc.l    0
                        dc.l    lbL02DFEC
                        dc.l    sound_flame
                        dc.l    lbL032C14

; memory location to load / PathName
samples_files_table:    dc.l    lbL0290BA
                        dc.b    "WHAT"
                        dc.l    lbL02974A
                        dc.b    "GREM"
                        dc.l    lbL02A8C0
                        dc.b    "FIRE"
                        dc.l    lbL02CB5E
                        dc.b    "ALRM"
                        dc.l    lbL02DFEC
                        dc.b    "MOON"
                        dc.l    lbL02F01E
                        dc.b    "SWSH"
                        dc.l    lbL02F936
                        dc.b    "HRSE"
                        dc.l    lbL031EBC
                        dc.b    "BUMP"
                        dc.l    lbL0320CE
                        dc.b    "HITS"
                        dc.l    lbL032C14
                        dc.b    "SQEK"
                        
                        dc.l    lbL034D5E
                        dc.b    "ACC1"
                        dc.l    lbL034D4E
                        dc.b    "ACC2"
                        dc.l    lbL034D2E
                        dc.b    "ACC3"
                        dc.l    lbL034CEE
                        dc.b    "ACC4"
                        dc.l    lbL034C6E
                        dc.b    "ACC5"
                        dc.l    lbL034B6E
                        dc.b    "ACC6"

flag_music_end:         dcb.b   2,0
lbB028FDC:              dcb.b   2,0
current_music_number:   dcb.b   2,0
sound_output_state:     dcb.b   2,0
sound_finished_state:   dcb.b   2,0
music_volume:           dc.w    2,0

                        SECTION EXODUS028FE8,bss_c

lbL028FE8:              ds.l    16
lbL029028:              ds.l    16
lbL029068:              ds.l    16
lbB0290A8:              ds.b    2
lbW0290AA:              ds.w    1
lbW0290AC:              ds.w    1
lbW0290AE:              ds.w    1
lbW0290B0:              ds.w    1
lbB0290B2:              ds.b    2
max_music_number:       ds.b    2
lbB0290B6:              ds.b    2
sound_volume:           ds.w    1
lbL0290BA:              ds.l    332
                        ds.w    1
                        ds.b    1
lbB0295ED:              ds.b    349
lbL02974A:              ds.l    49
                        ds.w    1
                        ds.b    1
lbB029811:              ds.b    1846
lbB029F47:              ds.b    498
lbB02A139:              ds.b    472
lbB02A311:              ds.b    872
lbB02A679:              ds.b    302
lbB02A7A7:              ds.b    256
lbB02A8A7:              ds.b    25
lbL02A8C0:              ds.l    677
                        ds.w    1
                        ds.b    1
lbB02B357:              ds.b    490
lbB02B541:              ds.b    4358
lbB02C647:              ds.b    318
lbB02C785:              ds.b    985
lbL02CB5E:              ds.l    298
                        ds.w    1
                        ds.b    1
lbB02D009:              ds.b    284
lbB02D125:              ds.b    2630
lbB02DB6B:              ds.b    790
lbB02DE81:              ds.b    363
lbL02DFEC:              ds.l    486
                        ds.w    1
                        ds.b    1
lbB02E787:              ds.b    588
lbB02E9D3:              ds.b    1611
lbL02F01E:              ds.l    582
lbL02F936:              ds.l    555
sound_flame:            ds.l    1846
                        ds.w    1
lbL031EBC:              ds.l    132
                        ds.w    1
lbL0320CE:              ds.l    721
                        ds.w    1
lbL032C14:              ds.l    982
                        ds.w    1
music_datas:            ds.l    1024
lbL034B6E:              ds.l    12
lbL034B9E:              ds.l    52
lbL034C6E:              ds.l    12
lbL034C9E:              ds.l    20
lbL034CEE:              ds.l    12
lbL034D1E:              ds.l    4
lbL034D2E:              ds.l    8
lbL034D4E:              ds.l    4
lbL034D5E:              ds.l    8
lbL034D7E:              ds.l    4
lbL034D8E:              ds.l    2
                        ds.w    1

                        SECTION Font,CODE

font:                   incbin  "font.bin"

                        SECTION Libraries_Calls,CODE

; -----------------------------
; Dos calls
_Open:                  movem.l d2/a6,-(sp)
                        movem.l 12(sp),d1/d2
                        move.l  DOSBase,a6
                        jsr     _LVOOpen(a6)
                        movem.l (sp)+,d2/a6
                        rts
                
_Close:                 move.l  a6,-(sp)
                        move.l  8(sp),d1
                        move.l  DOSBase,a6
                        jsr     _LVOClose(a6)
                        move.l  (sp)+,a6
                        rts

_Read:                  movem.l d2/d3/a6,-(sp)
                        movem.l $10(sp),d1-d3
                        move.l  DOSBase,a6
                        jsr     _LVORead(a6)
                        movem.l (sp)+,d2/d3/a6
                        rts
                
_Write:                 movem.l d2/d3/a6,-(sp)
                        movem.l $10(sp),d1-d3
                        move.l  DOSBase,a6
                        jsr     _LVOWrite(a6)
                        movem.l (sp)+,d2/d3/a6
                        rts
                
_Seek:                  movem.l d2/d3/a6,-(sp)
                        movem.l $10(sp),d1-d3
                        move.l  DOSBase,a6
                        jsr     _LVOSeek(a6)
                        movem.l (sp)+,d2/d3/a6
                        rts
                
_Lock:                  movem.l d2/a6,-(sp)
                        movem.l 12(sp),d1/d2
                        move.l  DOSBase,a6
                        jsr     _LVOLock(a6)
                        movem.l (sp)+,d2/a6
                        rts
                
_UnLock:                move.l  a6,-(sp)
                        move.l  8(sp),d1
                        move.l  DOSBase,a6
                        jsr     _LVOUnLock(a6)
                        move.l  (sp)+,a6
                        rts

_Examine:               movem.l d2/a6,-(sp)
                        movem.l 12(sp),d1/d2
                        move.l  DOSBase,a6
                        jsr     _LVOExamine(a6)
                        movem.l (sp)+,d2/a6
                        rts
                
; -----------------------------
; Exec calls
_SetIntVector:          move.l  a6,-(sp)
                        move.l  ExecBase,a6
                        movem.l 8(sp),d0/a1
                        jsr     _LVOSetIntVector(a6)
                        move.l  (sp)+,a6
                        rts

_AddIntServer:          move.l  a6,-(sp)
                        move.l  ExecBase,a6
                        movem.l 8(sp),d0/a1
                        jsr     _LVOAddIntServer(a6)
                        move.l  (sp)+,a6
                        rts

_AllocMem:              move.l  a6,-(sp)
                        move.l  ExecBase,a6
                        movem.l 8(sp),d0/d1
                        jsr     _LVOAllocMem(a6)
                        move.l  (sp)+,a6
                        rts

_FreeMem:               move.l  a6,-(sp)
                        move.l  ExecBase,a6
                        move.l  8(sp),a1
                        move.l  12(sp),d0
                        jsr     _LVOFreeMem(a6)
                        move.l  (sp)+,a6
                        rts

_AvailMem:              move.l  a6,-(sp)
                        move.l  ExecBase,a6
                        move.l  8(sp),d1
                        jsr     _LVOAvailMem(a6)
                        move.l  (sp)+,a6
                        rts

_RemTask:               move.l  a6,-(sp)
                        move.l  ExecBase,a6
                        move.l  8(sp),a1
                        jsr     _LVORemTask(a6)
                        move.l  (sp)+,a6
                        rts

_FindTask:              move.l  a6,-(sp)
                        move.l  ExecBase,a6
                        move.l  8(sp),a1
                        jsr     _LVOFindTask(a6)
                        move.l  (sp)+,a6
                        rts

_CloseLibrary:          move.l  a6,-(sp)
                        move.l  ExecBase,a6
                        move.l  8(sp),a1
                        jsr     _LVOCloseLibrary(a6)
                        move.l  (sp)+,a6
                        rts

_OpenLibrary:           move.l  a6,-(sp)
                        move.l  ExecBase,a6
                        move.l  8(sp),a1
                        move.l  12(sp),d0
                        jsr     _LVOOpenLibrary(a6)
                        move.l  (sp)+,a6
                        rts

; -----------------------------
; Graphic calls
_BltBitMap:             movem.l d2-d7/a2/a6,-(sp)
                        move.l  $24(sp),a0
                        movem.l $28(sp),d0/d1
                        move.l  $30(sp),a1
                        movem.l $34(sp),d2-d7/a2
                        move.l  GfxBase,a6
                        jsr     _LVOBltBitMap(a6)
                        movem.l (sp)+,d2-d7/a2/a6
                        rts

_InitRastPort:          move.l  a6,-(sp)
                        move.l  8(sp),a1
                        move.l  GfxBase,a6
                        jsr     _LVOInitRastPort(a6)
                        move.l  (sp)+,a6
                        rts

_InitVPort:             move.l  a6,-(sp)
                        move.l  8(sp),a0
                        move.l  GfxBase,a6
                        jsr     _LVOInitVPort(a6)
                        move.l  (sp)+,a6
                        rts

_MrgCop:                move.l  a6,-(sp)
                        move.l  8(sp),a1
                        move.l  GfxBase,a6
                        jsr     _LVOMrgCop(a6)
                        move.l  (sp)+,a6
                        rts

_MakeVPort:             move.l  a6,-(sp)
                        movem.l 8(sp),a0/a1
                        move.l  GfxBase,a6
                        jsr     _LVOMakeVPort(a6)
                        move.l  (sp)+,a6
                        rts

_LoadView:              move.l  a6,-(sp)
                        move.l  8(sp),a1
                        move.l  GfxBase,a6
                        jsr     _LVOLoadView(a6)
                        move.l  (sp)+,a6
                        rts

_Move:                  move.l  a6,-(sp)
                        move.l  8(sp),a1
                        movem.l 12(sp),d0/d1
                        move.l  GfxBase,a6
                        jsr     _LVOMove(a6)
                        move.l  (sp)+,a6
                        rts

_Draw:                  move.l  a6,-(sp)
                        move.l  8(sp),a1
                        movem.l 12(sp),d0/d1
                        move.l  GfxBase,a6
                        jsr     _LVODraw(a6)
                        move.l  (sp)+,a6
                        rts

_WaitTOF:               move.l  a6,-(sp)
                        move.l  GfxBase,a6
                        jsr     _LVOWaitTOF(a6)
                        move.l  (sp)+,a6
                        rts

_BltClear:              move.l  a6,-(sp)
                        move.l  8(sp),a1
                        movem.l 12(sp),d0/d1
                        move.l  GfxBase,a6
                        jsr     _LVOBltClear(a6)
                        move.l  (sp)+,a6
                        rts

_WritePixel:            move.l  a6,-(sp)
                        move.l  8(sp),a1
                        movem.l 12(sp),d0/d1
                        move.l  GfxBase,a6
                        jsr     _LVOWritePixel(a6)
                        move.l  (sp)+,a6
                        rts

_SetAPen:               move.l  a6,-(sp)
                        move.l  8(sp),a1
                        move.l  12(sp),d0
                        move.l  GfxBase,a6
                        jsr     _LVOSetAPen(a6)
                        move.l  (sp)+,a6
                        rts

_InitView:              move.l  a6,-(sp)
                        move.l  8(sp),a1
                        move.l  GfxBase,a6
                        jsr     _LVOInitView(a6)
                        move.l  (sp)+,a6
                        rts

_InitBitMap:            movem.l d2/a6,-(sp)
                        move.l  12(sp),a0
                        movem.l $10(sp),d0-d2
                        move.l  GfxBase,a6
                        jsr     _LVOInitBitMap(a6)
                        movem.l (sp)+,d2/a6
                        rts

_ScrollRaster:          movem.l d2-d5/a6,-(sp)
                        move.l  $18(sp),a1
                        movem.l $1C(sp),d0-d5
                        move.l  GfxBase,a6
                        jsr     _LVOScrollRaster(a6)
                        movem.l (sp)+,d2-d5/a6
                        rts

_FreeSprite:            move.l  a6,-(sp)
                        move.l  8(sp),d0
                        move.l  GfxBase,a6
                        jsr     _LVOFreeSprite(a6)
                        move.l  (sp)+,a6
                        rts

_GetColorMap:           move.l  a6,-(sp)
                        move.l  8(sp),d0
                        move.l  GfxBase,a6
                        jsr     _LVOGetColorMap(a6)
                        move.l  (sp)+,a6
                        rts

                        end
