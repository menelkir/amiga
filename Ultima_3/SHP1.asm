                        include "constants.inc"

                        org     0

start:                  bra.b   lbC00001C

                        dc.l    $1EA,0,0,0,0,0
                        dc.w    $FFFF

lbC00001C:              bra.b   lbC000096

                        include "jump_table.inc"

lbC000096:              lea     YELOCALGROCER_MSG(PC),A4
                        bsr     display_message
loop:                   lea     RATIONS1gpEAC_MSG(PC),A4
                        bsr     display_message
                        bsr.b   input_get_number
                        bsr.b   decrease_character_money
                        bpl.b   lbC0000CA
                        lea     WHATCANTPAYOU_MSG(PC),A4
lbC0000BA:              bsr     display_message
                        moveq   #1,D0
                        bsr.b   play_sound
                        rts

lbC0000CA:              move.b  INPUT_NUMBER(A3),D0
                        bsr.b   increase_character_food
                        lea     Thankyouanyth_MSG(PC),A4
                        bsr     display_message
                        bsr     input_wait_key
                        cmp.b   #$D9,D0
                        beq.b   lbC0000EC
                        lea     Nverywellcome_MSG(PC),A4
                        bsr     display_message
                        rts

lbC0000EC:              lea     Y_MSG(PC),A4
                        bsr     display_message
                        bra.b   loop

decrease_character_money:
                        move.b  D0,D3
                        bsr     get_character_data
                        move.b  CHARACTER_MONEY_LO(A5),D0
                        move.b  D0,D4
                        move.w  #0,CCR
                        sbcd    D3,D0
                        move.b  D0,CHARACTER_MONEY_LO(A5)
                        move.b  CHARACTER_MONEY_HI(A5),D0
                        move.b  D0,D5
                        clr.b   D3
                        sbcd    D3,D0
                        bcs.b   not_enough_money
                        move.b  D0,CHARACTER_MONEY_HI(A5)
                        clr.b   D0
                        rts

not_enough_money:       move.b  D4,CHARACTER_MONEY_LO(A5)
                        move.b  D5,CHARACTER_MONEY_HI(A5)
                        move.b  #$FF,D0
                        rts

YELOCALGROCER_MSG:      dc.b    $ff, "    YE LOCAL",$ff
                        dc.b    "     GROCER",$ff,$ff
                        dc.b    0
RATIONS1gpEAC_MSG:      dc.b    "RATIONS:",$ff
                        dc.b    "1 g.p. EACH,",$ff
                        dc.b    "How many would",$ff
                        dc.b    "you like?-"
                        dc.b    0
WHATCANTPAYOU_MSG:      dc.b    $ff,$ff,"WHAT? CAN'T PAY!",$ff
                        dc.b    "OUT! YOU SCUM!!!",$ff
                        dc.b    0
Thankyouanyth_MSG:      dc.b    $ff,$ff,"Thank you,",$ff
                        dc.b    "anything else?"
                        dc.b    0
Nverywellcome_MSG:      dc.b    "N",$ff,$ff
                        dc.b    "very well,",$ff
                        dc.b    "come again!",$ff
                        dc.b    0
Y_MSG:                  dc.b    "Y",$ff,$ff
                        dc.b    0

                        end
