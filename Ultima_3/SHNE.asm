                        include "constants.inc"

                        org     0

start:                  BRA.S   lbC00001C

                        dc.l    $306,0,0,0,0,0
                        dc.w    $FFFF

lbC00001C:              BRA.S   lbC000096

                        include "jump_table.inc"

lbC000096:              MOVEQ   #8,D0
                        BSR.S   get_encrypt_key_response
                        CMP.B   #$62,D0
                        BNE     lbC0001F0
                        LEA     WELCOMETOTHES_MSG(PC),A4
                        BSR     display_message
                        MOVE.B  0(A3),D0
                        AND.B   #3,D0
                        BEQ.S   lbC0000DC
                        CMP.B   #1,D0
                        BEQ.S   lbC0000F8
                        CMP.B   #2,D0
                        BEQ.S   lbC000114
                        LEA     WISDOM_MSG(PC),A4
                        BSR     display_message
                        BSR     lbC0001F6
                        LEA     lbB000318(PC),A0
                        MOVE.B  0(A0,D1.W),TEMP_NUMBER_W_HI(A3)
                        MOVE.B  #$15,D2
                        BRA.S   lbC00012E

lbC0000DC:              LEA     STRENGTH_MSG(PC),A4
                        BSR     display_message
                        BSR     lbC0001F6
                        LEA     lbB00030E(PC),A0
                        MOVE.B  0(A0,D1.W),TEMP_NUMBER_W_HI(A3)
                        MOVE.B  #$12,D2
                        BRA.S   lbC00012E

lbC0000F8:              LEA     DEXTERITY_MSG(PC),A4
                        BSR     display_message
                        BSR     lbC0001F6
                        LEA     lbB000313(PC),A0
                        MOVE.B  0(A0,D1.W),TEMP_NUMBER_W_HI(A3)
                        MOVE.B  #$13,D2
                        BRA.S   lbC00012E

lbC000114:              LEA     INTELLIGENCE_MSG(PC),A4
                        BSR     display_message
                        BSR     lbC0001F6
                        LEA     lbB00031D(PC),A0
                        MOVE.B  0(A0,D1.W),TEMP_NUMBER_W_HI(A3)
                        MOVE.B  #$14,D2
lbC00012E:              MOVE.B  D2,$6B(A3)
                        LEA     OFFERING100_MSG(PC),A4
                        BSR     display_message
lbC00013A:              CLR.W   D2
                        BSR     lbC000072
                        MOVE.W  #$1000,D2
                        BSR     lbC000072
                        BSR     display_coded_map_page
                        BSR     lbC000060
                        BPL.S   lbC00013A
                        CMP.B   #$B0,D0
                        BCS.S   lbC00013A
                        CMP.B   #$BA,D0
                        BCC.S   lbC00013A
                        SUB.B   #$B0,D0
                        MOVE.B  D0,INPUT_NUMBER(A3)
                        BSR     draw_digit
                        MOVE.B  INPUT_NUMBER(A3),D0
                        BNE.S   lbC00017A
                        LEA     THENBEOFF_MSG(PC),A4
                        BSR     display_message
                        RTS

lbC00017A:              BSR     get_character_data
                        MOVE.B  $23(A5),D0
                        CMP.B   INPUT_NUMBER(A3),D0
                        BCC.S   lbC000198
                        LEA     YOUCANTCHEATT_MSG(PC),A4
lbC00018C:              BSR     display_message
                        MOVEQ   #1,D0
                        BSR     play_sound
                        RTS

lbC000198:              BSR     get_character_data
                        MOVE.B  $23(A5),D0
                        MOVE.B  INPUT_NUMBER(A3),D3
                        MOVE.W  #0,CCR
                        SBCD    D3,D0
                        MOVE.B  D0,$23(A5)
                        LEA     SHAZAM_MSG(PC),A4
                        BSR     display_message
                        BSR     lbC000036
                        MOVE.B  #15,$68(A3)
                        BSR     lbC00023E
                        BSR     lbC000036
                        MOVE.B  INPUT_NUMBER(A3),D0
                        CLR.W   D2
                        MOVE.B  $6B(A3),D2
                        BSR.S   increase_character_attribute
                        BSR     get_character_data
                        CLR.W   D2
                        MOVE.B  $6B(A3),D2
                        MOVE.B  TEMP_NUMBER_W_HI(A3),D0
                        CMP.B   0(A5,D2.W),D0
                        BCC.S   lbC0001EC
                        MOVE.B  D0,0(A5,D2.W)
lbC0001EC:              BRA     lbC00012E

lbC0001F0:              LEA     HonestyisaVir_MSG(PC),A4
                        BRA.S   lbC00018C

lbC0001F6:              BSR     get_character_data
                        MOVE.B  $16(A5),D0
                        CLR.W   D1
                        LEA     lbB000309(PC),A0
lbC000204:              CMP.B   0(A0,D1.W),D0
                        BEQ.S   lbC000212
                        ADDQ.W  #1,D1
                        CMP.W   #5,D1
                        BCS.S   lbC000204
lbC000212:              RTS

increase_character_attribute:
                        MOVE.B  D0,D3
                        MOVE.B  D2,$6B(A3)
                        BSR     get_character_data
                        CLR.W   D2
                        MOVE.B  $6B(A3),D2
                        MOVE.B  0(A5,D2.W),D0
                        MOVE.W  #0,CCR
                        ABCD    D3,D0
                        BCS.S   attribute_max_value
                        MOVE.B  D0,0(A5,D2.W)
                        RTS

attribute_max_value:    MOVE.B  #$99,0(A5,D2.W)
                        RTS

lbC00023E:              MOVEQ   #13,D0
                        BSR     play_sound
                        BSR     lbC000030
                        MOVEQ   #$11,D0
                        BSR     play_sound
                        BSR     lbC000030
                        RTS

WELCOMETOTHES_MSG:      dc.b    $ff
                        dc.b    ' WELCOME TO THE',$ff
                        dc.b    '   SHRINE  OF',$ff
                        dc.b    0
WISDOM_MSG:             dc.b    '     WISDOM',$ff,$ff,0
STRENGTH_MSG:           dc.b    '    STRENGTH',$ff,$ff,0
DEXTERITY_MSG:          dc.b    '   DEXTERITY',$ff,$ff,0
INTELLIGENCE_MSG:       dc.b    '  INTELLIGENCE',$ff,$ff,0
OFFERING100_MSG:        dc.b    'OFFERING*100-',0
THENBEOFF_MSG:          dc.b    $ff,'THEN BE OFF!',$ff,0
HonestyisaVir_MSG:      dc.b    'Honesty is a',$ff
                        dc.b    'Virtue!',$ff
YOUCANTCHEATT_MSG:      dc.b    $ff
                        dc.b    "YOU CAN'T CHEAT",$ff
                        dc.b    'THE GODS!',$ff,0
SHAZAM_MSG:             dc.b    $ff,'SHAZAM!',$ff,0
lbB000309:              dc.b    $C8,$C5,$C4,$C2,$C6
lbB00030E:              dc.b    $75,$75,$99,$75,$25
lbB000313:              dc.b    $75,$99,$75,$50,$99
lbB000318:              dc.b    $75,$50,$75,$99,$75
lbB00031D:              dc.b    $75,$75,$50,$75,$99

                        end
