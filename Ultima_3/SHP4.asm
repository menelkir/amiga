                        include "constants.inc"

                        org     0

start:                  BRA.S   lbC00001C

                        dc.l    $43C,0,0,0,0,0
                        dc.w    $FFFF

lbC00001C:              BRA.S   lbC000096

                        include "jump_table.inc"

lbC000096:              MOVEQ   #4,D0
                        BSR.S   get_encrypt_key_response
                        CMP.B   #$6A,D0
                        BNE     lbC00014A
                        LEA     lbB00042C(PC),A0
                        MOVE.B  #$C6,(A0)
                        CMP.B   #$25,$4D(A3)
                        BNE.S   lbC0000B6
                        MOVE.B  #$C8,(A0)
lbC0000B6:              LEA     WelcometotheA_MSG(PC),A4
                        BSR     display_message
                        BSR.S   input_wait_key
                        CMP.B   #$D9,D0
                        BNE.S   lbC0000D0
                        BSR     lbC000206
                        LEA     BuyorSell_MSG(PC),A4
                        BRA.S   lbC0000D4

lbC0000D0:              LEA     NBuyorSell_MSG(PC),A4
lbC0000D4:              BSR     display_message
                        BSR     input_wait_key
                        CMP.B   #$C2,D0
                        BNE     lbC000180
                        LEA     B_MSG(PC),A4
                        BSR     display_message
lbC0000EC:              LEA     Yourinterest_MSG(PC),A4
                        BSR     display_message
                        BSR     input_wait_key
                        CMP.B   #$C2,D0
                        BCS.S   lbC000140
                        LEA     lbB00042C(PC),A0
                        CMP.B   (A0),D0
                        BCC.S   lbC000140
                        MOVE.W  D0,-(SP)
                        AND.B   #$7F,D0
                        BSR     draw_letter_both_bitmap
                        MOVE.W  (SP)+,D0
                        SUB.B   #$C2,D0
                        MOVE.B  D0,INPUT_NUMBER(A3)
                        LSL.B   #1,D0
                        CLR.W   D2
                        MOVE.B  D0,D2
                        LEA     table_prices(PC),A0
                        MOVE.B  0(A0,D2.W),TEMP_NUMBER_W_HI(A3)
                        MOVE.B  1(A0,D2.W),TEMP_NUMBER_W_LO(A3)
                        BSR     decrease_character_money
                        BEQ.S   lbC000150
                        LEA     Imverysorrybu_MSG(PC),A4
                        BSR     display_message
                        RTS

lbC000140:              LEA     ohwellmaybene_MSG(PC),A4
lbC000144:              BSR     display_message
                        RTS

lbC00014A:              LEA     HonestyisaVir_MSG(PC),A4
                        BRA.S   lbC000144

lbC000150:              BSR     get_character_data
                        MOVE.W  #$29,D2
                        ADD.B   INPUT_NUMBER(A3),D2
                        MOVE.B  0(A5,D2.W),D0
                        MOVE.B  #1,D3
                        MOVE.W  #0,CCR
                        ABCD    D3,D0
                        BCC.S   lbC000170
                        MOVE.B  #$99,D0
lbC000170:              MOVE.B  D0,0(A5,D2.W)
                        LEA     Hereyouaremay_MSG(PC),A4
                        BSR     display_message
                        BRA     lbC0000EC

lbC000180:              LEA     S_MSG(PC),A4
                        BSR     display_message
lbC000188:              LEA     forsale_MSG(PC),A4
                        BSR     display_message
                        BSR     input_wait_key
                        CMP.B   #$C2,D0
                        BCS.S   lbC000140
                        LEA     lbB00042C(PC),A0
                        CMP.B   (A0),D0
                        BCC.S   lbC000140
                        MOVE.W  D0,-(SP)
                        AND.B   #$7F,D0
                        BSR     draw_letter_both_bitmap
                        MOVE.W  (SP)+,D0
                        SUB.B   #$C2,D0
                        MOVE.B  D0,INPUT_NUMBER(A3)
                        LSL.B   #1,D0
                        CLR.W   D2
                        MOVE.B  D0,D2
                        LEA     table_prices(PC),A0
                        MOVE.B  0(A0,D2.W),TEMP_NUMBER_W_HI(A3)
                        MOVE.B  1(A0,D2.W),TEMP_NUMBER_W_LO(A3)
                        BSR     get_character_data
                        MOVE.W  #$29,D2
                        ADD.B   INPUT_NUMBER(A3),D2
                        MOVE.B  0(A5,D2.W),D0
                        BNE.S   lbC0001E8
                        LEA     Youdontownone_MSG(PC),A4
                        BSR     display_message
                        RTS

lbC0001E8:              MOVE.B  #1,D3
                        MOVE.W  #0,CCR
                        SBCD    D3,D0
                        MOVE.B  D0,0(A5,D2.W)
                        CLR.B   $28(A5)
                        BSR.S   increase_character_money
                        LEA     Thankyou_MSG(PC),A4
                        BSR     display_message
                        BRA.S   lbC000188

lbC000206:              LEA     YAVAILABLEBCL_MSG(PC),A4
                        BSR.S   display_item
                        LEA     CLEATHER195gp_MSG(PC),A4
                        BSR.S   display_item
                        LEA     DCHAIN575gp_MSG(PC),A4
                        BSR.S   display_item
                        LEA     EPLATE2500gp_MSG(PC),A4
                        BSR.S   display_item
                        LEA     lbB00042C(PC),A0
                        CMP.B   #$C8,(A0)
                        BEQ.S   lbC00022A
                        RTS

lbC00022A:              LEA     F2CHAIN6130gp_MSG(PC),A4
                        BSR.S   display_item
                        LEA     G2PLATE8250gp_MSG(PC),A4
                        BSR.S   display_item
                        RTS

display_item:           BSR     display_message
lbC00023C:              BSR     lbC000060
                        BPL.S   lbC00023C
                        RTS

increase_character_money:
                        MOVE.B  CHARACTER_MONEY_LO(A5),D0
                        MOVE.B  TEMP_NUMBER_W_LO(A3),D3
                        MOVE.W  #0,CCR
                        ABCD    D3,D0
                        MOVE.B  D0,CHARACTER_MONEY_LO(A5)
                        MOVE.B  CHARACTER_MONEY_HI(A5),D0
                        MOVE.B  TEMP_NUMBER_W_HI(A3),D3
                        ABCD    D3,D0
                        BCS.S   max_character_money
                        MOVE.B  D0,CHARACTER_MONEY_HI(A5)
                        RTS

max_character_money:    MOVE.B  #$99,CHARACTER_MONEY_HI(A5)
                        MOVE.B  #$99,CHARACTER_MONEY_LO(A5)
                        RTS

decrease_character_money:        
                        BSR     get_character_data
                        MOVE.B  CHARACTER_MONEY_LO(A5),D0
                        MOVE.B  D0,D4
                        MOVE.B  TEMP_NUMBER_W_LO(A3),D3
                        MOVE.W  #0,CCR
                        SBCD    D3,D0
                        MOVE.B  D0,CHARACTER_MONEY_LO(A5)
                        MOVE.B  CHARACTER_MONEY_HI(A5),D0
                        MOVE.B  D0,D5
                        MOVE.B  TEMP_NUMBER_W_HI(A3),D3
                        SBCD    D3,D0
                        BCS.S   not_enough_money
                        MOVE.B  D0,CHARACTER_MONEY_HI(A5)
                        MOVE.B  #0,D0
                        RTS

not_enough_money:       MOVE.B  D4,CHARACTER_MONEY_LO(A5)
                        MOVE.B  D5,CHARACTER_MONEY_HI(A5)
                        MOVE.B  #$FF,D0
                        RTS

table_prices:           dc.w    $75
                        dc.w    $195
                        dc.w    $575
                        dc.w    $2500
                        dc.w    $6130
                        dc.w    $8250

WelcometotheA_MSG:      dc.b    $ff
                        dc.b    'Welcome to the',$ff
                        dc.b    'ARMOUR SHOP!',$ff,$ff
                        dc.b    'list (Y/N)?-'
                        dc.b    0
NBuyorSell_MSG:         dc.b    'N'
BuyorSell_MSG:          dc.b    $ff,$ff,'Buy or Sell?-',0
Yourinterest_MSG:       dc.b    $ff,$ff,'Your interest?-',0
Imverysorrybu_MSG:      dc.b    $ff,$ff
                        dc.b    "I'm very sorry",$ff
                        dc.b    "but you haven't",$ff
                        dc.b    'the gold!'
                        dc.b    $ff,0
ohwellmaybene_MSG:      dc.b    '0',$ff
                        dc.b    'oh well, maybe',$ff
                        dc.b    'next time.',$ff,0
Hereyouaremay_MSG:      dc.b    $ff,$ff
                        dc.b    'Here you are',$ff
                        dc.b    'may it serve',$ff
                        dc.b    'you well!',$ff,0
forsale_MSG:            dc.b    $ff,$ff
                        dc.b    'for sale?-',0
Youdontownone_MSG:      dc.b    $ff,$ff
                        dc.b    "You don't own",$ff
                        dc.b    'one of those!',$ff,0
Thankyou_MSG:           dc.b    $ff,$ff
                        dc.b    'Thank you!',$ff,0
YAVAILABLEBCL_MSG:      dc.b    'Y'
                        dc.b    $ff,$ff
                        dc.b    'AVAILABLE:',$ff
                        dc.b    'B:CLOTH-75gp',$ff,0
CLEATHER195gp_MSG:      dc.b    'C:LEATHER-195gp',$ff,0
DCHAIN575gp_MSG:        dc.b    'D:CHAIN-575gp',$ff,0
EPLATE2500gp_MSG:       dc.b    'E:PLATE-2500gp',$ff,0
F2CHAIN6130gp_MSG:      dc.b    'F:+2CHAIN-6130gp',$ff,0
G2PLATE8250gp_MSG:      dc.b    'G:+2PLATE-8250gp',$ff,0
B_MSG:                  dc.b    'B',0
S_MSG:                  dc.b    'S',0
lbB00042C:              dc.b    0
HonestyisaVir_MSG:      dc.b    'Honesty is a',$ff
                        dc.b    'Virtue!',$ff
                        dc.b    'I will not',$ff
                        dc.b    'help you!',$ff,0

                        end
