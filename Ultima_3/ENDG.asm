                        include "constants.inc"

                        org     0

start:                  BRA.S   lbC00001C

                        dc.l    $300,0,0,0,0,0
                        dc.w    $FFFF

lbC00001C:              BRA.S   lbC000096

                        include "jump_table.inc"

lbC000096:              LEA     CONGRATULATIO_MSG(PC),A4
                        BSR.S   display_message
                        MOVE.B  PARTY_MOVES(A3),D0
                        BSR.S   draw_2_digits
                        MOVE.B  PARTY_MOVES+1(A3),D0
                        BSR.S   draw_2_digits
                        MOVE.B  PARTY_MOVES+2(A3),D0
                        BSR.S   draw_2_digits
                        MOVE.B  PARTY_MOVES+3(A3),D0
                        BSR.S   draw_2_digits
                        LEA     MOVESREPORTTH_MSG(PC),A4
                        BSR     display_message
                        MOVEQ   #15,D0
                        BSR     lbC0001DA
                        MOVEQ   #13,D0
                        BSR     lbC0001DA
                        MOVEQ   #15,D0
                        BSR     lbC0001DA
                        MOVEQ   #13,D0
                        BSR     lbC0001DA
                        MOVEQ   #15,D0
                        BSR     lbC0001DA
                        MOVEQ   #13,D0
                        BSR     lbC0001DA
                        MOVEQ   #15,D0
                        BSR     lbC0001DA
                        MOVEQ   #13,D0
                        BSR     lbC0001DA
                        MOVEQ   #15,D0
                        BSR     lbC0001DA
                        MOVEQ   #13,D0
                        BSR     lbC0001DA
                        MOVEQ   #15,D0
                        BSR     lbC0001DA
                        MOVEQ   #13,D0
                        BSR     lbC0001DA
                        MOVEQ   #15,D0
                        BSR     lbC0001DA
                        MOVEQ   #13,D0
                        BSR     lbC0001DA
                        MOVEQ   #15,D0
                        BSR     lbC0001DA
                        MOVEQ   #13,D0
                        BSR     lbC0001DA
                        MOVEQ   #15,D0
                        BSR     lbC0001DA
                        MOVEQ   #13,D0
                        BSR     lbC0001DA
                        MOVEQ   #15,D0
                        BSR     lbC0001DA
                        MOVEQ   #13,D0
                        BSR     lbC0001DA
                        BSR     lbC00008A
                        MOVE.B  #5,D1
                        MOVE.B  #3,D2
                        LEA     Andsoitcameto_MSG(PC),A4
                        BSR     display_message_at
                        MOVE.B  #2,D1
                        MOVE.B  #5,D2
                        LEA     passthatonthi_MSG(PC),A4
                        BSR     display_message_at
                        MOVE.B  #2,D1
                        MOVE.B  #7,D2
                        LEA     dayEXODUShell_MSG(PC),A4
                        BSR     display_message_at
                        MOVE.B  #2,D1
                        MOVE.B  #9,D2
                        LEA     incarnateofev_MSG(PC),A4
                        BSR     display_message_at
                        MOVE.B  #2,D1
                        MOVE.B  #11,D2
                        LEA     wasvanquished_MSG(PC),A4
                        BSR     display_message_at
                        MOVE.B  #2,D1
                        MOVE.B  #13,D2
                        LEA     SOSARIAWhatno_MSG(PC),A4
                        BSR     display_message_at
                        MOVE.B  #2,D1
                        MOVE.B  #15,D2
                        LEA     liesaheadinth_MSG(PC),A4
                        BSR     display_message_at
                        MOVE.B  #2,D1
                        MOVE.B  #$11,D2
                        LEA     ULTIMAsagacan_MSG(PC),A4
                        BSR     display_message_at
                        MOVE.B  #2,D1
                        MOVE.B  #$13,D2
                        LEA     bepurespecula_MSG(PC),A4
                        BSR     display_message_at
                        MOVE.B  #2,D1
                        MOVE.B  #$15,D2
                        LEA     OnwardtoULTIM_MSG(PC),A4
                        BSR     display_message_at
                        RTS

lbC0001DA:              BSR     play_sound
                        BSR     lbC000030
                        MOVEQ   #-1,D0
lbC0001E4:              DBRA    D0,lbC0001E4
                        RTS

CONGRATULATIO_MSG:      dc.b    $ff
                        dc.b    'CONGRATULATIONS!',$ff
                        dc.b    '   THOU  HAST',$ff
                        dc.b    '   COMPLEATED',$ff
                        dc.b    'EXODUS: ULTIMA 3',$ff
                        dc.b    '       IN',$ff
                        dc.b    ' '
                        dc.b    0
MOVESREPORTTH_MSG:      dc.b    ' MOVES',$ff
                        dc.b    'REPORT THY FEAT!'
                        dc.b    0
Andsoitcameto_MSG:      dc.b    'And so it came to'
                        dc.b    0
passthatonthi_MSG:      dc.b    'pass  that  on  this'
                        dc.b    0
dayEXODUShell_MSG:      dc.b    'day EXODUS,hell-born'
                        dc.b    0
incarnateofev_MSG:      dc.b    'incarnate  of  evil,'
                        dc.b    0
wasvanquished_MSG:      dc.b    'was vanquished  from'
                        dc.b    0
SOSARIAWhatno_MSG:      dc.b    'SOSARIA.   What  now'
                        dc.b    0
liesaheadinth_MSG:      dc.b    'lies  ahead  in  the'
                        dc.b    0
ULTIMAsagacan_MSG:      dc.b    'ULTIMA saga can only'
                        dc.b    0
bepurespecula_MSG:      dc.b    'be pure speculation!'
                        dc.b    0
OnwardtoULTIM_MSG:      dc.b    'Onward to ULTIMA IV!'
                        dc.b    0

                        end
