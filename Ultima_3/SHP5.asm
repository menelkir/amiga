                        include "constants.inc"

                        org     0

start:                  BRA.S   lbC00001C

                        dc.l    $27A,0,0,0,0,0
                        dc.w    $FFFF

lbC00001C:              BRA.S   lbC000096

                        include "jump_table.inc"

lbC000096:              LEA     THEGUILDSHOPW_MSG(PC),A4
                        BSR     display_message
                        BSR.S   input_wait_key
                        CMP.B   #$D4,D0
                        BEQ.S   lbC0000CE
                        CMP.B   #$CB,D0
                        BEQ.S   lbC0000EE
                        CMP.B   #$D0,D0
                        BEQ.S   lbC00010E
                        CMP.B   #$C7,D0
                        BEQ.S   lbC00012C
lbC0000C4:              LEA     NThankyoucome_MSG(PC),A4
                        BSR     display_message
                        RTS

lbC0000CE:              LEA     T_MSG(PC),A4
                        BSR     display_message
                        MOVE.B  #$30,D0
                        BSR     decrease_character_money
                        BMI.S   lbC00014A
                        MOVE.W  #15,D2
                        MOVE.B  #5,D0
                        BSR.S   set_character_byte_value
                        BRA     lbC000160

lbC0000EE:              LEA     K_MSG(PC),A4
                        BSR     display_message
                        MOVE.B  #$50,D0
                        BSR     decrease_character_money
                        BMI.S   lbC00014A
                        MOVE.W  #$26,D2
                        MOVE.B  #1,D0
                        BSR     set_character_byte_value
                        BRA.S   lbC000160

lbC00010E:              LEA     P_MSG(PC),A4
                        BSR     display_message
                        MOVE.B  #$90,D0
                        BSR.S   decrease_character_money
                        BMI.S   lbC00014A
                        MOVE.W  #$27,D2
                        MOVE.B  #1,D0
                        BSR     set_character_byte_value
                        BRA.S   lbC000160

lbC00012C:              LEA     G_MSG(PC),A4
                        BSR     display_message
                        MOVE.B  #$75,D0
                        BSR.S   decrease_character_money
                        BMI.S   lbC00014A
                        MOVE.W  #$25,D2
                        MOVE.B  #1,D0
                        BSR     set_character_byte_value
                        BRA.S   lbC000160

lbC00014A:              LEA     Imsorrybutyou_MSG(PC),A4
lbC00014E:              BSR     display_message
                        MOVEQ   #1,D0
                        BSR     play_sound
                        RTS

lbC000160:              LEA     Anythingelse_MSG(PC),A4
                        BSR     display_message
                        BSR     input_wait_key
                        CMP.B   #$D9,D0
                        BEQ     lbC000096
                        BRA     lbC0000C4

decrease_character_money:
                        MOVE.B  D0,D3
                        BSR     get_character_data
                        MOVE.B  CHARACTER_MONEY_LO(A5),D0
                        MOVE.B  D0,D4
                        MOVE.W  #0,CCR
                        SBCD    D3,D0
                        MOVE.B  D0,CHARACTER_MONEY_LO(A5)
                        MOVE.B  CHARACTER_MONEY_HI(A5),D0
                        MOVE.B  D0,D5
                        CLR.B   D3
                        SBCD    D3,D0
                        BCS.S   not_enough_money
                        MOVE.B  D0,CHARACTER_MONEY_HI(A5)
                        CLR.B   D0
                        RTS

not_enough_money:       MOVE.B  D4,CHARACTER_MONEY_LO(A5)
                        MOVE.B  D5,CHARACTER_MONEY_HI(A5)
                        MOVE.B  #$FF,D0
                        RTS

THEGUILDSHOPW_MSG:      dc.b    $ff,"THE GUILD SHOP",$ff
                        dc.b    "WE HAVE:",$ff
                        dc.b    " Keys-50gp",$ff
                        dc.b    " Torches-5/30gp",$ff
                        dc.b    " Powders-90gp",$ff
                        dc.b    " Gems-75gp",$ff
                        dc.b    "YOUR NEED: "
                        dc.b    0
NThankyoucome_MSG:      dc.b    "N",$ff,$ff
                        dc.b    "Thank you,",$ff
                        dc.b    "come again!",$ff,0
T_MSG:                  dc.b    'T',$ff,0
K_MSG:                  dc.b    'K',$ff,0
P_MSG:                  dc.b    'P',$ff,0
G_MSG:                  dc.b    'G',$ff,0
Imsorrybutyou_MSG:      dc.b    $ff,"I'm sorry,",$ff
                        dc.b    "but you have",$ff
                        dc.b    "not the funds!",$ff,0
Anythingelse_MSG:       dc.b    $ff,'Anything else? ',0

                        end
