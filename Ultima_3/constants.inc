SCREEN_WIDTH            equ     320
SCREEN_HEIGHT           equ     200
SCREEN_BPP              equ     4
SCREEN_BYTES            equ     SCREEN_WIDTH/8

PARTY_X                 equ     0
PARTY_Y                 equ     1

PARTY_ACTION_X          equ     2
PARTY_ACTION_Y          equ     3

PARTY_DIRECTION_X       equ     4
PARTY_DIRECTION_Y       equ     5

SAVE_TEXT_X             equ     $1c
SAVE_TEXT_Y             equ     $1d
OTHER_COMMAND_LENGTH    equ     $1e

CURRENT_VOLUME          equ     $3a
CURRENT_WINDS           equ     $3b
CURRENT_DUNGEON_LEVEL   equ     $3d
PARTY_MOVES             equ     $54             ; .l
PARTY_CHARACTERS        equ     $4b
PARTY_X_SAVE            equ     $4d
PARTY_Y_SAVE            equ     $4e
NEGATE_TIME_COUNTER     equ     $5c
LIGHT_COUNTER           equ     $5d
INPUT_NUMBER            equ     $61
TEMP_NUMBER_W_HI        equ     $62
TEMP_NUMBER_W_LO        equ     $63
CURRENT_CHARACTER       equ     $66
CURRENT_CHARACTER_SAVE  equ     $67
SPELL_OR_OBJECT_NUMBER  equ     $68

TEXT_X                  equ     $72
TEXT_Y                  equ     $73

; Characters values
CHARACTER_TORCHES       equ     $f
CHARACTER_MAGICPOINTS   equ     $19
CHARACTER_HITPOINTS_HI  equ     $1a
CHARACTER_HITPOINTS_LO  equ     $1b
CHARACTER_MAXPOINTS_HI  equ     $1c
CHARACTER_MAXPOINTS_LO  equ     $1d
CHARACTER_EXPERIENCE_HI equ     $1e
CHARACTER_EXPERIENCE_LO equ     $1f             ; Doesn't seem to be used
CHARACTER_FOOD_HI       equ     $20
CHARACTER_FOOD_LO       equ     $21
CHARACTER_MONEY_HI      equ     $23
CHARACTER_MONEY_LO      equ     $24
CHARACTER_GEMS          equ     $25
CHARACTER_KEYS          equ     $26
CHARACTER_POWDERS       equ     $27
