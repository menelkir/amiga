                        include "constants.inc"

                        org     0

start:                  BRA.S   lbC00001C

                        dc.l    $500,0,0,0,0,0
                        dc.w    $FFFF

lbC00001C:              BRA.S   lbC000096

                        include "jump_table.inc"

lbC000096:              LEA     lbB0004EF(PC),A0
                        MOVE.B  #$C9,(A0)
                        CMP.B   #$25,PARTY_X_SAVE(A3)
                        BNE.S   lbC0000B6
                        MOVE.B  #$D0,(A0)
lbC0000B6:              LEA     WelcometotheW_MSG(PC),A4
                        BSR     display_message
                        BSR.S   input_wait_key
                        CMP.B   #$D9,D0
                        BNE.S   ask_buy_or_sell
                        BSR     lbC000208
                        LEA     BuyorSell_MSG(PC),A4
                        BRA.S   lbC0000D4

ask_buy_or_sell:        LEA     NBuyorSell_MSG(PC),A4
lbC0000D4:              BSR     display_message
                        BSR     input_wait_key
                        CMP.B   #$C2,D0
                        BNE     sell_item
                        LEA     B_MSG(PC),A4
                        BSR     display_message
lbC0000EC:              LEA     Yourinterest_MSG(PC),A4
                        BSR     display_message
                        BSR     input_wait_key
                        CMP.B   #$C2,D0
                        BCS.S   lbC000140
                        LEA     lbB0004EF(PC),A0
                        CMP.B   (A0),D0
                        BCC.S   lbC000140
                        MOVE.W  D0,-(SP)
                        AND.B   #$7F,D0
                        BSR     draw_letter_both_bitmap
                        MOVE.W  (SP)+,D0
                        SUB.B   #$C2,D0
                        MOVE.B  D0,INPUT_NUMBER(A3)
                        ADD.B   D0,D0
                        CLR.W   D2
                        MOVE.B  D0,D2
                        LEA     table_prices(PC),A0
                        MOVE.B  0(A0,D2.W),TEMP_NUMBER_W_HI(A3)
                        MOVE.B  1(A0,D2.W),TEMP_NUMBER_W_LO(A3)
                        BSR     decrease_character_money
                        BEQ.S   add_item
                        LEA     Imverysorrybu_MSG(PC),A4
                        BSR     display_message
                        RTS

lbC000140:              LEA     ohwellmaybene_MSG(PC),A4
lbC000144:              BSR     display_message
                        RTS

add_item:               BSR     get_character_data
                        MOVE.W  #$31,D2
                        ADD.B   INPUT_NUMBER(A3),D2
                        MOVE.B  0(A5,D2.W),D0
                        MOVE.B  #1,D3
                        MOVE.W  #0,CCR
                        ABCD    D3,D0
                        BCC.S   max_items
                        MOVE.B  #$99,D0
max_items:              MOVE.B  D0,0(A5,D2.W)
                        LEA     Hereyouaremay_MSG(PC),A4
                        BSR     display_message
                        BRA     lbC0000EC

sell_item:              LEA     S_MSG(PC),A4
                        BSR     display_message
lbC000188:              LEA     forsale_MSG(PC),A4
                        BSR     display_message
                        BSR     input_wait_key
                        CMP.B   #$C2,D0
                        BCS.S   lbC000140
                        LEA     lbB0004EF(PC),A0
                        CMP.B   (A0),D0
                        BCC.S   lbC000140
                        MOVE.W  D0,-(SP)
                        AND.B   #$7F,D0
                        BSR     draw_letter_both_bitmap
                        MOVE.W  (SP)+,D0
                        SUB.B   #$C2,D0
                        MOVE.B  D0,INPUT_NUMBER(A3)
                        ADD.B   D0,D0
                        CLR.W   D2
                        MOVE.B  D0,D2
                        LEA     table_prices(PC),A0
                        MOVE.B  0(A0,D2.W),TEMP_NUMBER_W_HI(A3)
                        MOVE.B  1(A0,D2.W),TEMP_NUMBER_W_LO(A3)
                        BSR     get_character_data
                        MOVE.W  #$31,D2
                        ADD.B   INPUT_NUMBER(A3),D2
                        MOVE.B  0(A5,D2.W),D0                   ; from $31(a5)
                        BNE.S   remove_item
                        LEA     Youdontownone_MSG(PC),A4
                        BSR     display_message
                        RTS

remove_item:            MOVE.B  #1,D3
                        MOVE.W  #0,CCR
                        SBCD    D3,D0
                        MOVE.B  D0,0(A5,D2.W)
                        CLR.B   $30(A5)
                        BSR     increase_character_money
                        LEA     Thankyou_MSG(PC),A4
                        BSR     display_message
                        BRA.S   lbC000188

lbC000208:              LEA     YAVAILABLEBDA_MSG(PC),A4
                        BSR.S   display_item
                        LEA     CMACES30gp_MSG(PC),A4
                        BSR.S   display_item
                        LEA     DSLINGS60gp_MSG(PC),A4
                        BSR.S   display_item
                        LEA     EAXES125gp_MSG(PC),A4
                        BSR.S   display_item
                        LEA     FBOWS350gp_MSG(PC),A4
                        BSR.S   display_item
                        LEA     GSWORDS200gp_MSG(PC),A4
                        BSR.S   display_item
                        LEA     H2HSWD250gp_MSG(PC),A4
                        BSR.S   display_item
                        LEA     lbB0004EF(PC),A0
                        CMP.B   #$D0,(A0)
                        BEQ.S   lbC00023E
                        RTS

lbC00023E:              LEA     I2AXE400gp_MSG(PC),A4
                        BSR.S   display_item
                        LEA     J2BOWS1050gp_MSG(PC),A4
                        BSR.S   display_item
                        LEA     K2SWDS800gp_MSG(PC),A4
                        BSR.S   display_item
                        LEA     LGLOVES1200gp_MSG(PC),A4
                        BSR.S   display_item
                        LEA     M4AXES2700gp_MSG(PC),A4
                        BSR.S   display_item
                        LEA     N4BOWS6550gp_MSG(PC),A4
                        BSR.S   display_item
                        LEA     O4SWDS4550gp_MSG(PC),A4
                        BSR.S   display_item
                        RTS

display_item:           BSR     display_message
lbC00026E:              BSR     lbC000060
                        BPL.S   lbC00026E
                        RTS

increase_character_money:
                        MOVE.B  CHARACTER_MONEY_LO(A5),D0
                        MOVE.B  TEMP_NUMBER_W_LO(A3),D3
                        MOVE.W  #0,CCR
                        ABCD    D3,D0
                        MOVE.B  D0,CHARACTER_MONEY_LO(A5)
                        MOVE.B  CHARACTER_MONEY_HI(A5),D0
                        MOVE.B  TEMP_NUMBER_W_HI(A3),D3
                        ABCD    D3,D0
                        BCS.S   max_character_money
                        MOVE.B  D0,CHARACTER_MONEY_HI(A5)
                        RTS

max_character_money:    MOVE.B  #$99,CHARACTER_MONEY_HI(A5)
                        MOVE.B  #$99,CHARACTER_MONEY_LO(A5)
                        RTS

decrease_character_money:
                        BSR     get_character_data
                        MOVE.B  CHARACTER_MONEY_LO(A5),D0
                        MOVE.B  D0,D4
                        MOVE.B  TEMP_NUMBER_W_LO(A3),D3
                        MOVE.W  #0,CCR
                        SBCD    D3,D0
                        MOVE.B  D0,CHARACTER_MONEY_LO(A5)
                        MOVE.B  CHARACTER_MONEY_HI(A5),D0
                        MOVE.B  D0,D5
                        MOVE.B  TEMP_NUMBER_W_HI(A3),D3
                        SBCD    D3,D0
                        BCS.S   not_enough_money
                        MOVE.B  D0,CHARACTER_MONEY_HI(A5)
                        MOVE.B  #0,D0
                        RTS

not_enough_money:       MOVE.B  D4,CHARACTER_MONEY_LO(A5)
                        MOVE.B  D5,CHARACTER_MONEY_HI(A5)
                        MOVE.B  #$FF,D0
                        RTS

table_prices:           dc.w    $5
                        dc.w    $30
                        dc.w    $60
                        dc.w    $125
                        dc.w    $350
                        dc.w    $200
                        dc.w    $250
                        dc.w    $400
                        dc.w    $1050
                        dc.w    $800
                        dc.w    $1200
                        dc.w    $2700
                        dc.w    $6550
                        dc.w    $4550

WelcometotheW_MSG:      dc.b    $ff,"Welcome to the",$ff
                        dc.b    "WEAPONS SHOP!",$ff,$ff
                        dc.b    "list (Y/N)?-"
                        dc.b    0
NBuyorSell_MSG:         dc.b    'N'
BuyorSell_MSG:          dc.b    $ff,$ff,"Buy or Sell?-"
                        dc.b    0
Yourinterest_MSG:       dc.b    $ff,$ff,"Your interest?-"
                        dc.b    0
Imverysorrybu_MSG:      dc.b    $ff,$ff,"I'm very sorry",$ff
                        dc.b    "but you haven't",$ff
                        dc.b    "the gold!",$ff
                        dc.b    0
ohwellmaybene_MSG:      dc.b    "0",$ff
                        dc.b    "oh well, maybe",$ff
                        dc.b    "next time.",$ff
                        dc.b    0
Hereyouaremay_MSG:      dc.b    $ff,$ff,"Here you are",$ff
                        dc.b    "may it serve",$ff
                        dc.b    "you well!",$ff
                        dc.b    0
forsale_MSG:            dc.b    $ff,$ff,"for sale?-"
                        dc.b    0
Youdontownone_MSG:      dc.b    $ff,$ff,"You don't own",$ff
                        dc.b    "one of those!",$ff
                        dc.b    0
Thankyou_MSG:           dc.b    $ff,$ff,"Thank you!",$ff
                        dc.b    0
YAVAILABLEBDA_MSG:      dc.b    "Y",$ff,$ff
                        dc.b    "AVAILABLE:",$ff
                        dc.b    "B:DAGGERS-5gp",$ff,0
CMACES30gp_MSG:         dc.b    "C:MACES-30gp",$ff,0
DSLINGS60gp_MSG:        dc.b    "D:SLINGS-60gp",$ff,0
EAXES125gp_MSG:         dc.b    "E:AXES-125gp",$ff,0
FBOWS350gp_MSG:         dc.b    "F:BOWS-350gp",$ff,0
GSWORDS200gp_MSG:       dc.b    "G:SWORDS-200gp",$ff,0
H2HSWD250gp_MSG:        dc.b    "H:2H SWD-250gp",$ff,0
I2AXE400gp_MSG:         dc.b    "I:+2 AXE-400gp",$ff,0
J2BOWS1050gp_MSG:       dc.b    "J:+2 BOWS-1050gp",$ff,0
K2SWDS800gp_MSG:        dc.b    "K:+2 SWDS-800gp",$ff,0
LGLOVES1200gp_MSG:      dc.b    "L:GLOVES-1200gp",$ff,0
M4AXES2700gp_MSG:       dc.b    "M:+4 AXES-2700gp",$ff,0
N4BOWS6550gp_MSG:       dc.b    "N:+4 BOWS-6550gp",$ff,0
O4SWDS4550gp_MSG:       dc.b    "O:+4 SWDS-4550gp",$ff,0
B_MSG:                  dc.b    "B",0
S_MSG:                  dc.b    "S",0
lbB0004EF:              dc.b    0

                        end
