; -------------------------------------------
; Thexder.
; Game.
; Disassembled by Franck Charlet.
; -------------------------------------------

LH_TAIL             equ     4
LH_TAILPRED         equ     8
LH_HEAD             equ     0

; exec.library
_LVOSupervisor      equ     -30
_LVOAddIntServer    equ     -168
_LVORemIntServer    equ     -174
_LVOCause           equ     -180
_LVOAllocMem        equ     -198
_LVOFreeMem         equ     -210
_LVOFindTask        equ     -294
_LVOAllocSignal     equ     -330
_LVOFreeSignal      equ     -336
_LVOAddPort         equ     -354
_LVORemPort         equ     -360
_LVOOpenDevice      equ     -444
_LVOCloseDevice     equ     -450
_LVODoIO            equ     -456
_LVOSendIO          equ     -462
_LVOCheckIO         equ     -468
_LVOWaitIO          equ     -474
_LVOOpenLibrary     equ     -552

; dos.library
_LVOOpen            equ     -30
_LVOClose           equ     -36
_LVORead            equ     -42
_LVOSeek            equ     -66

; intuition.library
_LVOCloseScreen     equ     -66
_LVOOpenScreen      equ     -198 

; graphics.library
_LVOSetRGB4         equ     -288

; -------------------------------------------
                    section prog,code

ProgStart:
                    lea      DT,a4

                    move.l   sp,Save_SP-DT(a4)
                    move.l   a5,Save_A5-DT(a4)
                    move.l   a4,_RegisterA4-DT(a4)
                    jsr      InitVBL(pc)
                    jsr      InitInputHandler(pc)
                    jsr      OpenAll(pc)
                    jsr      InitChar(pc)
                    clr.w    fAnyKey-DT(a4)
                    move.w   #1,fDemo-DT(a4)
                    jsr      _InitSoundIntr(pc)
                    jsr      _InitSounds(pc)

lbC000C74:          move.w   #-1,fJoy-DT(a4)
                    jsr      DisplayTitle(pc)
                    move.l   prgbMoonMusic-DT(a4),_prgbSongCur-DT(a4)
                    jsr      _StartMusic(pc)
                    move.w   #$12C,d0
                    jsr      WaitSome(pc)
                    bne.s    lbC000CAC
                    jsr      DisplayCredits(pc)
                    move.w   #$12C,d0
                    jsr      WaitSome(pc)
                    bne.s    lbC000CAC
                    tst.w    fAnyKey-DT(a4)
                    bne.s    lbC000CAC
                    tst.w    fDemo-DT(a4)
                    bne.s    lbC000CB2
lbC000CAC:          move.w   #0,fDemo-DT(a4)
lbC000CB2:          move.l   #$25C7,d0
                    move.l   prgbSpWarn-DT(a4),a0
                    jsr      Speak(pc)
                    move.l   prgbThemeMusic-DT(a4),_prgbSongCur-DT(a4)
                    jsr      _StartMusic(pc)
                    jsr      InitKey(pc)
                    clr.w    cLevel-DT(a4)
                    move.w   #-1,frmTThex-DT(a4)
                    move.w   #-1,Garbage-DT(a4)
                    jsr      InitGame(pc)
_InitLevel:         jsr      InitLevel(pc)
                    tst.w    cLevel-DT(a4)
                    beq.s    lbC000CF0
                    jsr      DoBlockAide(pc)
lbC000CF0:          move.w   #-1,d0
                    move.w   d0,cFrame-DT(a4)
                    clr.w    cVBL-DT(a4)
lbC000CFC:          cmp.w    #3,cVBL-DT(a4)
                    blt.s    lbC000CFC
                    tst.w    fDemo-DT(a4)
                    beq.s    lbC000D10
                    tst.w    fAnyKey-DT(a4)
                    bne.s    lbC000CAC
lbC000D10:          clr.w    cVBL-DT(a4)
                    addq.w   #1,iShoot-DT(a4)
                    move.w   iShoot-DT(a4),d0
                    cmp.w    cTekiOnScrn-DT(a4),d0
                    ble.s    lbC000D28
                    move.w   #1,iShoot-DT(a4)
lbC000D28:          addq.w   #1,cFrame-DT(a4)
                    move.w   cFrame-DT(a4),d0
                    and.w    #7,d0
                    bne.s    _ReadKey
                    tst.w    fShields-DT(a4)
                    beq.s    _ReadKey
                    subq.w   #1,fShields-DT(a4)
                    bcc.s    _UpDateShields
                    clr.w    fShields-DT(a4)
_UpDateShields:     jsr      UpDateShields(pc)
                    tst.w    fShields-DT(a4)
                    bne.s    _ReadKey
                    jsr      PrintShields(pc)
_ReadKey:           jsr      ReadKey(pc)
                    tst.w    _fPause-DT(a4)
                    bne.s    _ReadKey
                    clr.w    cVBL-DT(a4)
                    tst.w    fQuit-DT(a4)
                    bne      Quit
                    tst.w    wEnergy-DT(a4)
                    bne.s    _EraseBeam
                    clr.w    xJoyCur-DT(a4)
                    clr.w    yJoyCur-DT(a4)
                    clr.w    fireCur-DT(a4)
_EraseBeam:         jsr      EraseBeam(pc)
                    tst.w    wEnergy-DT(a4)
                    beq.s    _EraseBomb
                    jsr      MoveMan(pc)
_EraseBomb:         jsr      EraseBomb(pc)
                    jsr      MoveTeki(pc)
                    jsr      DrawMap(pc)
                    jsr      DrawBeam(pc)
                    jsr      DrawBomb(pc)
                    tst.w    wEnergy-DT(a4)
                    bne      _DrawThex
                    move.w   #$11,fState-DT(a4)
                    jsr      EraseBeam(pc)
                    jsr      RemoveThexFromMap(pc)
                    addq.w   #1,Garbage-DT(a4)
                    jsr      KillThex(pc)
                    cmp.w    #2,frmTThex-DT(a4)
                    bne.s    lbC000DC6
                    bra.s    lbC000DCC

lbC000DC6:          and.w    #3,Garbage-DT(a4)
lbC000DCC:          bne.s    lbC000DD2
                    addq.w   #1,frmTThex-DT(a4)
lbC000DD2:          move.w   frmTThex-DT(a4),d0
                    cmp.w    #3,d0
                    bne.s    lbC000E44
                    move.w   #0,frmThex-DT(a4)
                    jsr      PrintGameOver(pc)
                    move.w   #300,d0
                    jsr      WaitSome(pc)
                    tst.w    fDemo-DT(a4)
                    bne      lbC000C74
                    move.l   prgbMoonMusic-DT(a4),_prgbSongCur-DT(a4)
                    jsr      _StartMusic(pc)
                    jsr      DisplayCredits(pc)
                    move.w   #480,d0
                    jsr      WaitSome(pc)
                    bne.s    lbC000E22
                    lea      gdInput-DT(a4),a0
                    clr.w    8(a0)
_CkKey:             jsr      CkKey(pc)
                    bne.s    lbC000E22
                    tst.w    _fMusicDone-DT(a4)
                    bne.s    _CkKey
lbC000E22:          move.w   #0,fThex-DT(a4)
                    jsr      OpenAll(pc)
                    jsr      InitChar(pc)
                    jsr      InitMan(pc)
                    move.w   #0,frmThex-DT(a4)
                    move.w   #-1,Garbage-DT(a4)
                    bra      lbC000CAC

lbC000E44:          move.w   d0,frmThex-DT(a4)
                    jsr      DrawThexDying(pc)
                    bra.s    lbC000E52

_DrawThex:          jsr      DrawThex(pc)
lbC000E52:          move.w   xMapMax-DT(a4),d0
                    cmp.w    xMap-DT(a4),d0
                    bne      lbC000CFC
                    jsr      EraseBomb(pc)
                    jsr      EraseBeam(pc)
                    jsr      NextLevel(pc)
                    jsr      PrintLevelCompleted(pc)
                    clr.l    Garbage-DT(a4)
                    bra      _InitLevel

Quit:               jsr      LaserSound(pc)
                    jsr      _SoundOff(pc)
                    jsr      _CleanUpAudio(pc)
                    jsr      RemoveVBL(pc)
                    jsr      RemoveInputHandler(pc)
                    jsr      _StopAllSnd(pc)
                    jsr      Bye(pc)
                    move.l   Save_A5-DT(a4),a5
                    move.l   Save_SP-DT(a4),sp
                    rts

_InitSoundIntr:     link     a5,#0
                    pea      $10001
                    pea      $16
                    jsr      _AllocMem(pc)
                    addq.w   #8,sp
                    move.l   d0,_soundInt-DT(a4)
                    bne.s    lbC00002E
lbC00002A:          unlk     a5
                    rts

lbC00002E:          move.l   _soundInt-DT(a4),a0
                    move.b   #2,8(a0)
                    move.l   _soundInt-DT(a4),a0
                    clr.b    9(a0)
                    move.l   _soundInt-DT(a4),a0
                    clr.l    10(a0)
                    move.l   _soundInt-DT(a4),a0
                    lea      _PlayIt(pc),a1
                    move.l   _RegisterA4-DT(a4),14(a0)
                    move.l   a1,$12(a0)
                    bra.s    lbC00002A

_InitSounds:        link     a5,#0
                    movem.l  d4/d5,-(sp)
                    pea      2
                    pea      4
                    jsr      _AllocMem(pc)
                    addq.w   #8,sp
                    move.l   d0,_waveForm-DT(a4)
                    bne.s    lbC000080
lbC000078:          movem.l  (sp)+,d4/d5
                    unlk     a5
                    rts

lbC000080:          pea      2
                    pea      $1000
                    jsr      _AllocMem(pc)
                    addq.w   #8,sp
                    move.l   d0,_noiseForm-DT(a4)
                    bne.s    lbC0000A4
                    pea      4
                    move.l   _waveForm-DT(a4),-(sp)
                    jsr      _FreeMem(pc)
                    addq.w   #8,sp
                    bra.s    lbC000078

lbC0000A4:          move.l   _waveForm-DT(a4),a0
                    clr.b    (a0)
                    move.l   _waveForm-DT(a4),a0
                    move.b   #$80,1(a0)
                    move.l   _waveForm-DT(a4),a0
                    clr.b    2(a0)
                    move.l   _waveForm-DT(a4),a0
                    move.b   #$80,3(a0)
                    moveq    #1,d5
                    moveq    #0,d4
                    bra.s    lbC0000FE

lbC0000CC:          btst     #0,d5
                    beq.s    lbC0000E2
                    moveq    #0,d0
                    move.w   d5,d0
                    lsr.l    #1,d0
                    eor.l    #$CA0,d0
                    move.w   d0,d5
                    bra.s    lbC0000EA

lbC0000E2:          moveq    #0,d0
                    move.w   d5,d0
                    lsr.l    #1,d0
                    move.w   d0,d5
lbC0000EA:          moveq    #0,d0
                    move.w   d5,d0
                    and.l    #$FF,d0
                    move.l   _noiseForm-DT(a4),a0
                    move.b   d0,(a0,d4.w)
                    addq.w   #1,d4
lbC0000FE:          cmp.w    #$1000,d4
                    bcs.s    lbC0000CC
                    pea      $10003
                    pea      $44
                    jsr      _AllocMem(pc)
                    addq.w   #8,sp
                    move.l   d0,_allocIOB-DT(a4)
                    beq.s    __CleanUpAudio
                    pea      $10003
                    pea      $44
                    jsr      _AllocMem(pc)
                    addq.w   #8,sp
                    move.l   d0,_lockIOB-DT(a4)
                    beq.s    __CleanUpAudio
                    clr.l    -(sp)
                    pea      AGIsoundport_MSG(pc)
                    jsr      _CreatePort(pc)
                    addq.w   #8,sp
                    move.l   d0,_soundPort-DT(a4)
                    bne.s    lbC00014A
__CleanUpAudio:     jsr      _CleanUpAudio(pc)
                    bra      lbC000078

lbC00014A:          bra      lbC000078

AGIsoundport_MSG:   dc.b     'AGI-sound-port',0
                    even

_StartSound:        link     a5,#0
                    movem.l  d4/a2/a3,-(sp)
                    move.l   8(a5),a2
                    moveq    #0,d4
                    bra.s    lbC00019C

lbC00016E:          move.w   d4,d0
                    muls     #20,d0
                    lea      _chan-DT(a4),a0
                    move.l   d0,a3
                    add.l    a0,a3
                    move.w   d4,4(a3)
                    move.w   d4,d0
                    ext.l    d0
                    asl.l    #2,d0
                    move.l   d0,a0
                    add.l    a2,a0
                    move.l   10(a0),(a3)
                    move.w   #1,6(a3)
                    move.w   #1,12(a3)
                    addq.w   #1,d4
lbC00019C:          cmp.w    #3,d4
                    blt.s    lbC00016E
                    tst.l    _waveForm-DT(a4)
                    beq.s    __StopSnd
                    tst.l    _noiseForm-DT(a4)
                    beq.s    __StopSnd
                    jsr      _AllocateChannels(pc)
                    tst.w    d0
                    bne.s    lbC0001C2
__StopSnd:          jsr      _StopSnd(pc)
lbC0001BA:          movem.l  (sp)+,d4/a2/a3
                    unlk     a5
                    rts

lbC0001C2:          move.w   #3,_channelsPlaying-DT(a4)
                    move.w   #1,_fMusicDone-DT(a4)
                    bra.s    lbC0001BA

_PlayIt:            link     a5,#0
                    move.l   a4,-(sp)
                    move.l   a1,a4
                    bsr.s    _PlayIt1
                    move.l   (sp)+,a4
                    unlk     a5
                    rts

_PlayIt1:           link     a5,#0
                    movem.l  d4/a2/a3,-(sp)
                    tst.w    _fMusic-DT(a4)
                    beq.s    lbC0001FA
                    jsr      _StopSnd(pc)
lbC0001F2:          movem.l  (sp)+,d4/a2/a3
                    unlk     a5
                    rts

lbC0001FA:          tst.w    _fPause-DT(a4)
                    beq.s    lbC000206
                    jsr      _StopSnd(pc)
                    bra.s    lbC0001F2

lbC000206:          lea      _chan-DT(a4),a0
                    move.l   a0,a2
lbC00020C:          tst.w    12(a2)
                    beq      lbC000312
                    move.l   (a2),a3
                    subq.w   #1,6(a2)
                    tst.w    6(a2)
                    bne      lbC000312
                    move.l   a3,a0
                    addq.l   #1,a3
                    moveq    #0,d0
                    move.b   (a0),d0
                    move.w   d0,6(a2)
                    move.l   a3,a0
                    addq.l   #1,a3
                    moveq    #0,d0
                    move.b   (a0),d0
                    asl.l    #8,d0
                    add.w    d0,6(a2)
                    cmp.w    #-1,6(a2)
                    bne.s    lbC000254
                    subq.w   #1,_channelsPlaying-DT(a4)
                    move.l   a2,-(sp)
                    jsr      _ChannelOff(pc)
                    addq.w   #4,sp
                    bra      lbC000312

lbC000254:          cmp.w    #3,4(a2)
                    bne.s    lbC0002AA
                    addq.l   #1,a3
                    move.l   a3,a0
                    addq.l   #1,a3
                    moveq    #0,d0
                    move.b   (a0),d0
                    and.l    #3,d0
                    bra.s    lbC000296

lbC00026E:          move.w   #$200,8(a2)
                    bra.s    lbC0002A8

lbC000276:          move.w   #$400,8(a2)
                    bra.s    lbC0002A8

lbC00027E:          move.w   #$800,8(a2)
                    bra.s    lbC0002A8

lbC000286:          move.w   #$1000,8(a2)
                    bra.s    lbC0002A8

lbW00028E:          dc.w     lbC00026E-lbC0002A6
                    dc.w     lbC000276-lbC0002A6
                    dc.w     lbC00027E-lbC0002A6
                    dc.w     lbC000286-lbC0002A6

lbC000296:          cmp.l    #4,d0
                    bcc.s    lbC0002A8
                    asl.l    #1,d0
                    move.w   lbW00028E(pc,d0.w),d0
                    jmp      lbC0002A6(pc,d0.w)
lbC0002A6           equ      *-2

lbC0002A8:          bra.s    lbC0002E6

lbC0002AA:          move.l   a3,a0
                    addq.l   #1,a3
                    moveq    #0,d0
                    move.b   (a0),d0
                    and.l    #$3F,d0
                    moveq    #10,d1
                    asl.l    d1,d0
                    move.w   d0,8(a2)
                    move.l   a3,a0
                    addq.l   #1,a3
                    moveq    #0,d0
                    move.b   (a0),d0
                    and.l    #15,d0
                    asl.l    #6,d0
                    or.w     d0,8(a2)
                    move.w   8(a2),d0
                    swap     d0
                    clr.w    d0
                    swap     d0
                    divu     #4,d0
                    move.w   d0,8(a2)
lbC0002E6:          move.l   a3,a0
                    addq.l   #1,a3
                    moveq    #0,d0
                    move.b   (a0),d0
                    and.l    #15,d0
                    move.w   d0,d4
                    moveq    #15,d0
                    move.w   d4,d1
                    ext.l    d1
                    sub.l    d1,d0
                    asl.l    #4,d0
                    moveq    #15,d1
                    jsr      _divs(pc)
                    move.w   d0,10(a2)
                    move.l   a3,(a2)
                    move.l   a2,-(sp)
                    bsr.s    _PlayNote
                    addq.w   #4,sp
lbC000312:          add.l    #20,a2
                    lea      __devtab-DT(a4),a0
                    cmp.l    a0,a2
                    bcs      lbC00020C
                    tst.w    _channelsPlaying-DT(a4)
                    bne.s    lbC000346
                    move.l   prgbThemeMusic-DT(a4),a0
                    move.l   _prgbSongCur-DT(a4),a1
                    move.b   (a0),d0
                    cmp.b    (a1),d0
                    bne.s    __StopSnd0
                    jsr      _StartMusic(pc)
                    bra      lbC0001F2

__StopSnd0:         jsr      _StopSnd(pc)
                    clr.w    _fMusicDone-DT(a4)
lbC000346:          bra      lbC0001F2

_SoundOff:          link     a5,#0
                    move.l   a2,-(sp)
                    clr.w    _fMusicDone-DT(a4)
                    lea      _chan-DT(a4),a0
                    move.l   a0,a2
lbC00035A:          move.l   a2,-(sp)
                    jsr      _ChannelOff(pc)
                    addq.w   #4,sp
                    add.l    #20,a2
                    lea      __devtab-DT(a4),a0
                    cmp.l    a0,a2
                    bcs.s    lbC00035A
                    jsr      _FreeChannels(pc)
                    move.l   (sp)+,a2
                    unlk     a5
                    rts

_PlayNote:          link     a5,#0
                    movem.l  a2/a3,-(sp)
                    move.l   8(a5),a2
                    move.l   $10(a2),a3
                    cmp.w    #3,4(a2)
                    bne.s    lbC00039E
                    move.l   _noiseForm-DT(a4),(a3)
                    move.w   #$800,4(a3)
                    bra.s    lbC0003A8

lbC00039E:          move.l   _waveForm-DT(a4),(a3)
                    move.w   #2,4(a3)
lbC0003A8:          move.w   8(a2),6(a3)
                    move.w   10(a2),8(a3)
                    moveq    #0,d0
                    move.w   14(a2),d0
                    bset     #15,d0
                    move.w   d0,$DFF096
                    movem.l  (sp)+,a2/a3
                    unlk     a5
                    rts

_ErrBeep:           link     a5,#0
                    unlk     a5
                    rts

_AllocateChannels:  link     a5,#0
                    move.l   a2,-(sp)
                    clr.l    -(sp)
                    move.l   _allocIOB-DT(a4),-(sp)
                    clr.l    -(sp)
                    pea      AudioName(pc)
                    jsr      _OpenDevice(pc)
                    lea      $10(sp),sp
                    tst.l    d0
                    beq.s    lbC0003FA
                    moveq    #0,d0
lbC0003F4:          move.l   (sp)+,a2
                    unlk     a5
                    rts

lbC0003FA:          move.l   _allocIOB-DT(a4),a0
                    move.l   $14(a0),_audioDevice-DT(a4)
                    move.l   _allocIOB-DT(a4),a0
                    move.b   #$D8,9(a0)
                    move.l   _allocIOB-DT(a4),a0
                    move.l   _soundPort-DT(a4),14(a0)
                    move.l   _allocIOB-DT(a4),a0
                    move.w   #$20,$1C(a0)
                    move.l   _allocIOB-DT(a4),a0
                    move.b   #$40,$1E(a0)
                    lea      __H0_end-DT(a4),a0
                    move.l   _allocIOB-DT(a4),a1
                    move.l   a0,$22(a1)
                    move.l   _allocIOB-DT(a4),a0
                    move.l   #1,$26(a0)
                    move.l   _allocIOB-DT(a4),-(sp)
                    jsr      _BeginIO(pc)
                    addq.w   #4,sp
                    move.l   _allocIOB-DT(a4),-(sp)
                    jsr      _WaitIO(pc)
                    addq.w   #4,sp
                    tst.l    d0
                    beq.s    lbC000464
                    jsr      _FreeChannels(pc)
                    moveq    #0,d0
                    bra.s    lbC0003F4

lbC000464:          move.l   _lockIOB-DT(a4),a0
                    move.l   _soundPort-DT(a4),14(a0)
                    move.l   _lockIOB-DT(a4),a0
                    move.l   _audioDevice-DT(a4),$14(a0)
                    move.l   _allocIOB-DT(a4),a0
                    move.l   _lockIOB-DT(a4),a1
                    move.l   $18(a0),$18(a1)
                    move.l   _lockIOB-DT(a4),a0
                    move.w   #13,$1C(a0)
                    move.l   _allocIOB-DT(a4),a0
                    move.l   _lockIOB-DT(a4),a1
                    move.w   $20(a0),$20(a1)
                    move.l   _lockIOB-DT(a4),-(sp)
                    jsr      _SendIO(pc)
                    addq.w   #4,sp
                    move.l   _lockIOB-DT(a4),-(sp)
                    jsr      _CheckIO(pc)
                    addq.w   #4,sp
                    tst.l    d0
                    beq.s    lbC0004BE
                    bsr.s    _FreeChannels
                    moveq    #0,d0
                    bra      lbC0003F4

lbC0004BE:          lea      _chan-DT(a4),a0
                    move.l   a0,a2
lbC0004C4:          moveq    #0,d0
                    move.w   4(a2),d0
                    moveq    #1,d1
                    asl.l    d0,d1
                    move.w   d1,14(a2)
                    add.l    #20,a2
                    lea      __devtab-DT(a4),a0
                    cmp.l    a0,a2
                    bcs.s    lbC0004C4
                    move.l   #$DFF0A0,chan1_addr-DT(a4)
                    move.l   #$DFF0B0,chan2_addr-DT(a4)
                    move.l   #$DFF0C0,chan3_addr-DT(a4)
                    moveq    #1,d0
                    bra      lbC0003F4

AudioName:          dc.b     'audio.device',0
                    even

_ChannelOff:        link     a5,#0
                    move.l   8(a5),a0
                    tst.w    12(a0)
                    beq.s    lbC00052E
                    move.l   8(a5),a0
                    clr.w    12(a0)
                    move.l   8(a5),a0
                    move.w   14(a0),$DFF096
lbC00052E:          unlk     a5
                    rts

_FreeChannels:      link     a5,#0
                    tst.l    _audioDevice-DT(a4)
                    beq.s    lbC00055A
                    move.l   _allocIOB-DT(a4),a0
                    move.w   #9,$1C(a0)
                    move.l   _allocIOB-DT(a4),-(sp)
                    jsr      _DoIO(pc)
                    addq.w   #4,sp
                    move.l   _allocIOB-DT(a4),-(sp)
                    jsr      _CloseDevice(pc)
                    addq.w   #4,sp
lbC00055A:          clr.l    _audioDevice-DT(a4)
                    clr.l    -(sp)
                    pea      $44
                    move.l   _allocIOB-DT(a4),-(sp)
                    jsr      _setmem(pc)
                    lea      12(sp),sp
                    clr.l    -(sp)
                    pea      $44
                    move.l   _lockIOB-DT(a4),-(sp)
                    jsr      _setmem(pc)
                    lea      12(sp),sp
                    unlk     a5
                    rts

_CleanUpAudio:      link     a5,#0
                    tst.l    _waveForm-DT(a4)
                    beq.s    lbC0005A2
                    pea      4
                    move.l   _waveForm-DT(a4),-(sp)
                    jsr      _FreeMem(pc)
                    addq.w   #8,sp
                    clr.l    _waveForm-DT(a4)
lbC0005A2:          tst.l    _noiseForm-DT(a4)
                    beq.s    lbC0005BA
                    pea      $1000
                    move.l   _noiseForm-DT(a4),-(sp)
                    jsr      _FreeMem(pc)
                    addq.w   #8,sp
                    clr.l    _noiseForm-DT(a4)
lbC0005BA:          tst.l    _soundPort-DT(a4)
                    beq.s    lbC0005CE
                    move.l   _soundPort-DT(a4),-(sp)
                    jsr      _DeletePort(pc)
                    addq.w   #4,sp
                    clr.l    _soundPort-DT(a4)
lbC0005CE:          tst.l    _lockIOB-DT(a4)
                    beq.s    lbC0005E6
                    pea      $44
                    move.l   _lockIOB-DT(a4),-(sp)
                    jsr      _FreeMem(pc)
                    addq.w   #8,sp
                    clr.l    _lockIOB-DT(a4)
lbC0005E6:          tst.l    _allocIOB-DT(a4)
                    beq.s    lbC0005FE
                    pea      $44
                    move.l   _allocIOB-DT(a4),-(sp)
                    jsr      _FreeMem(pc)
                    addq.w   #8,sp
                    clr.l    _allocIOB-DT(a4)
lbC0005FE:          unlk     a5
                    rts

_StopSnd:           link     a5,#0
                    move.w   #7,$DFF096
                    unlk     a5
                    rts

_StopAllSnd:        link     a5,#0
                    move.w   #15,$DFF096
                    unlk     a5
                    rts

_StartMusic:        movem.l  d0-d7/a0-a6,-(sp)
                    lea      _sNode-DT(a4),a0
                    move.l   _prgbSongCur-DT(a4),a1
                    clr.w    (a0)
                    move.w   #0,4(a0)
                    move.l   a1,6(a0)
                    move.l   a1,d1
                    move.l   d1,d2
                    move.b   1(a1),d0
                    lsl.w    #8,d0
                    add.b    (a1),d0
                    add.w    d0,d2
                    move.l   d2,10(a0)
                    move.l   d1,d2
                    move.b   3(a1),d0
                    lsl.w    #8,d0
                    add.b    2(a1),d0
                    add.w    d0,d2
                    move.l   d2,14(a0)
                    move.l   d1,d2
                    move.b   5(a1),d0
                    lsl.w    #8,d0
                    add.b    4(a1),d0
                    add.w    d0,d2
                    move.l   d2,18(a0)
                    move.l   a0,-(sp)
                    jsr      _StartSound(pc)
                    addq.w   #4,sp
                    movem.l  (sp)+,d0-d7/a0-a6
                    rts

DoBlockAide:        moveq    #0,d0
                    move.l   prgbBuffer1-DT(a4),a0
                    add.w    #$C0,a0
                    move.l   prgbDispMap-DT(a4),a1
                    move.l   #$2C0C,d1
                    add.l    d1,a1
                    move.b   6(a0),d0
                    move.w   d0,(a1)
                    move.b   7(a0),d0
                    move.w   d0,$400(a1)
                    move.b   8(a0),d0
                    move.w   d0,$800(a1)
                    rts

ClearScreen:        move.l   pBitPlane0-DT(a4),a0
                    move.l   pBitPlane1-DT(a4),a1
                    move.l   pBitPlane2-DT(a4),a2
                    moveq    #0,d0
                    move.w   #4000-1,d1
lbC000F38:          move.l   d0,(a0)+
                    move.l   d0,(a1)+
                    move.l   d0,(a2)+
                    dbra     d1,lbC000F38
                    rts

InitGame:           clr.w    _fPause-DT(a4)
                    clr.w    fQuit-DT(a4)
                    clr.w    fShields-DT(a4)
                    clr.w    fShieldsUsed-DT(a4)
                    clr.w    fDead-DT(a4)
                    bsr.s    ClearScreen
                    jsr      SetColor(pc)
                    jsr      InitScore(pc)
                    jsr      InitEnergy(pc)
                    move.w   #1,wBCDLevel-DT(a4)
                    move.w   #$11,fState-DT(a4)
                    rts

InitLevel:          clr.w    xCur-DT(a4)
                    clr.w    yCur-DT(a4)
                    clr.w    xMap-DT(a4)
                    clr.w    yMap-DT(a4)
                    clr.w    _fPause-DT(a4)
                    clr.w    fFlash-DT(a4)
                    clr.w    fQuit-DT(a4)
                    clr.w    fShields-DT(a4)
                    clr.w    fShieldsUsed-DT(a4)
                    move.w   #$13,xThex-DT(a4)
                    move.w   #11,yThex-DT(a4)
                    move.w   #$13,xThexScrOffSet-DT(a4)
                    move.w   #11,yThexScrOffSet-DT(a4)
                    move.w   #$2C26,iMapThexOffSetCur-DT(a4)
                    jsr      InitTeki(pc)
                    jsr      InitMap(pc)
                    jsr      DrawThex(pc)
                    jsr      PrintEnergyMax(pc)
                    jsr      PrintEnergy(pc)
                    jsr      PrintShields(pc)
                    jsr      DrawEnergyBar(pc)
                    rts

InitKey:            moveq    #0,d0
                    lea      gdInput-DT(a4),a0
                    move.w   d0,18(a0)
                    move.w   d0,20(a0)
                    ;lea      lbW00585A-DT(a4),a0
                    ;move.l   d0,(a0)+
                    ;move.l   d0,(a0)
                    rts

WaitKey:            movem.l  d0/a0,-(sp)
                    lea      gdInput-DT(a4),a0
                    clr.w    8(a0)
WaitingKey:         btst     #7,$BFE001
                    beq.s    ExitJoyButton
                    tst.w    8(a0)
                    beq.s    WaitingKey
                    clr.w    8(a0)
ExitJoyButton:      movem.l  (sp)+,d0/a0
                    rts

CkKey:              movem.l  d0/a0,-(sp)
                    lea      gdInput-DT(a4),a0
                    tst.w    8(a0)
                    bne.s    lbC00102C
                    move.b   $BFE001,d0
                    not.w    d0
                    btst     #7,d0
lbC00102C:          movem.l  (sp)+,d0/a0
                    rts

ReadKey:            movem.l  d0-d3/a0,-(sp)
                    clr.w    xJoy2-DT(a4)
                    clr.w    yJoy2-DT(a4)
                    clr.w    joy2But1-DT(a4)
                    lea      gdInput-DT(a4),a0
                    jsr      DoShields(pc)
                    move.w   10(a0),d2
                    move.w   8(a0),d0
                    beq      lbC0010E8
                    move.w   #1,fAnyKey-DT(a4)
                    clr.w    8(a0)
                    clr.w    10(a0)
                    moveq    #-1,d1
                    cmp.w    #$26,d0
                    bne.s    lbC001074
                    clr.w    fJoy-DT(a4)
                    bra      lbC00117C

lbC001074:          cmp.w    #$27,d0
                    bne.s    lbC001080
                    move.w   d1,fJoy-DT(a4)
                    bra.s    lbC0010F0

lbC001080:          cmp.w    #$45,d0
                    bne.s    lbC001096
                    eor.w    d1,_fPause-DT(a4)
                    eor.w    d1,12(a0)
                    jsr      LaserSound(pc)
                    bra      lbC0011BA

lbC001096:          cmp.w    #$10,d0
                    bne.s    lbC0010AA
                    cmp.w    #$48,d2
                    bne.s    lbC0010AA
                    move.w   d1,fQuit-DT(a4)
                    bra      lbC0011BA

lbC0010AA:          cmp.w    #$58,d0
                    bne.s    lbC0010B8
                    eor.w    d1,12(a0)
                    bra      lbC0011BA

lbC0010B8:          cmp.w    #$42,d0
                    bne.s    lbC0010CC
                    cmp.w    #$68,d2
                    bne.s    lbC0010CC
                    eor.w    d1,fGod-DT(a4)
                    bra      lbC0011BA

lbC0010CC:          cmp.w    #$37,d0
                    bne.s    lbC0010DA
                    eor.w    d1,_fMusic-DT(a4)
                    bra      lbC0011BA

lbC0010DA:          cmp.w    #$21,d0
                    bne.s    lbC0010E8
                    eor.w    d1,_fSound-DT(a4)
                    bra      lbC0011BA

lbC0010E8:          tst.w    fJoy-DT(a4)
                    beq      lbC00117C
lbC0010F0:          lea      gdInput-DT(a4),a0
                    moveq    #0,d0
                    tst.w    18(a0)
                    beq.s    lbC0010FE
                    moveq    #1,d0
lbC0010FE:          move.w   d0,joy2But1-DT(a4)
                    lea      rgKeyPad-DT(a4),a0
                    moveq    #0,d0
lbC001108:          tst.w    (a0)+
                    bmi.s    lbC001124
                    bne.s    lbC001112
                    addq.w   #4,d0
                    bra.s    lbC001108

lbC001112:          lea      lbW001128(pc),a0
                    move.l   0(a0,d0.w),d0
                    move.w   d0,yJoy2-DT(a4)
                    swap     d0
                    move.w   d0,xJoy2-DT(a4)
lbC001124:          bra      lbC0011BA

lbW001128:          dc.w     0,-1,1,-1,1,0,1,1,0,1,-1,1,-1,0,-1,-1

DoShields:          tst.w    _fPause-DT(a4)
                    bne.s    lbC00117A
                    tst.w    $14(a0)
                    beq.s    lbC00117A
                    cmp.w    #20,fShields-DT(a4)
                    bge.s    lbC00117A
                    cmp.w    #10,wEnergy-DT(a4)
                    blt.s    lbC00117A
                    move.w   #10,d0
                    jsr      DoSubEnergy(pc)
                    move.w   #80,fShields-DT(a4)
                    move.w   d1,fShieldsUsed-DT(a4)
                    jsr      PrintShields(pc)
lbC00117A:          rts

lbC00117C:          move.l   $DFF00A,d0
                    move.l   d0,d1
                    asl.l    #1,d1
                    eor.l    d0,d1
                    moveq    #1,d2
                    moveq    #9,d3
                    btst     d2,d0
                    beq.s    lbC001194
                    addq.w   #1,xJoy2-DT(a4)
lbC001194:          btst     d3,d0
                    beq.s    lbC00119C
                    subq.w   #1,xJoy2-DT(a4)
lbC00119C:          btst     d2,d1
                    beq.s    lbC0011A4
                    addq.w   #1,yJoy2-DT(a4)
lbC0011A4:          btst     d3,d1
                    beq.s    lbC0011AC
                    subq.w   #1,yJoy2-DT(a4)
lbC0011AC:          btst     #7,$BFE001
                    bne.s    lbC0011BA
                    addq.w   #1,joy2But1-DT(a4)
lbC0011BA:          tst.w    fDemo-DT(a4)
                    beq.s    lbC0011D8
                    jsr      Random(pc)
                    and.w    #1,d0
                    move.w   #1,xJoy2-DT(a4)
                    move.w   d0,yJoy2-DT(a4)
                    move.w   #1,joy2But1-DT(a4)
lbC0011D8:          move.w   xJoy2-DT(a4),xJoyCur-DT(a4)
                    move.w   yJoy2-DT(a4),yJoyCur-DT(a4)
                    move.w   joy2But1-DT(a4),fireCur-DT(a4)
                    movem.l  (sp)+,d0-d3/a0
                    rts

InitChar:           move.l   pbmAll-DT(a4),a0
                    move.l   a0,pbmThex34-DT(a4)
                    clr.b    (a0)
                    move.w   #$17,d7
lbC0011FE:          move.w   #$1F,d6
lbC001202:          move.w   (a0),d3
                    move.w   2(a0),d4
                    move.w   4(a0),d5
                    move.b   11(a0),d0
                    lsl.w    #8,d0
                    move.b   10(a0),d0
                    move.b   9(a0),d1
                    lsl.w    #8,d1
                    move.b   8(a0),d1
                    move.b   7(a0),d2
                    lsl.w    #8,d2
                    move.b   6(a0),d2
                    move.w   d0,(a0)+
                    move.w   d1,(a0)+
                    move.w   d2,(a0)+
                    move.w   d3,(a0)+
                    move.w   d4,(a0)+
                    move.w   d5,(a0)+
                    dbra     d6,lbC001202
                    dbra     d7,lbC0011FE
                    move.l   a0,pbmThex33-DT(a4)
                    move.w   #$17,d7
lbC001246:          move.w   #$17,d6
lbC00124A:          move.w   (a0),d3
                    move.w   2(a0),d4
                    move.w   4(a0),d5
                    move.b   11(a0),d0
                    lsl.w    #8,d0
                    move.b   10(a0),d0
                    move.b   9(a0),d1
                    lsl.w    #8,d1
                    move.b   8(a0),d1
                    move.b   7(a0),d2
                    lsl.w    #8,d2
                    move.b   6(a0),d2
                    move.w   d0,(a0)+
                    move.w   d1,(a0)+
                    move.w   d2,(a0)+
                    move.w   d3,(a0)+
                    move.w   d4,(a0)+
                    move.w   d5,(a0)+
                    dbra     d6,lbC00124A
                    dbra     d7,lbC001246
                    move.l   a0,pbmMap-DT(a4)
                    clr.l    d0
                    move.l   d0,(a0)
                    move.l   d0,4(a0)
                    move.l   d0,8(a0)
                    move.l   d0,12(a0)
                    move.l   d0,$10(a0)
                    move.l   d0,$14(a0)
                    move.l   d0,$18(a0)
                    move.l   d0,$1C(a0)
                    move.l   d0,$E00(a0)
                    move.l   d0,$E04(a0)
                    move.l   d0,$E08(a0)
                    move.l   d0,$E0C(a0)
                    move.l   d0,$E10(a0)
                    move.l   d0,$E14(a0)
                    move.l   d0,$E18(a0)
                    move.l   d0,$E1C(a0)
                    move.w   #$7F,d1
lbC0012CE:          move.w   #3,d2
lbC0012D2:          move.l   (a0),d0
                    swap     d0
                    move.l   d0,(a0)+
                    move.b   1(a0),d0
                    lsl.w    #8,d0
                    move.b   (a0),d0
                    swap     d0
                    move.b   3(a0),d0
                    lsl.w    #8,d0
                    move.b   2(a0),d0
                    move.l   d0,(a0)+
                    dbra     d2,lbC0012D2
                    dbra     d1,lbC0012CE
                    move.l   a0,pbmTeki-DT(a4)
                    move.w   #$3F,d7
lbC0012FE:          move.w   #15,d6
lbC001302:          move.w   (a0),d2
                    move.w   2(a0),d3
                    move.b   7(a0),d0
                    lsl.w    #8,d0
                    move.b   6(a0),d0
                    move.b   5(a0),d1
                    lsl.w    #8,d1
                    move.b   4(a0),d1
                    move.w   d0,(a0)+
                    move.w   d1,(a0)+
                    move.w   d2,(a0)+
                    move.w   d3,(a0)+
                    dbra     d6,lbC001302
                    dbra     d7,lbC0012FE
                    add.w    #$180,a0
                    move.l   a0,pbmDying-DT(a4)
                    move.w   #2,d7
lbC001338:          move.w   #$17,d6
lbC00133C:          move.w   (a0),d3
                    move.w   2(a0),d4
                    move.w   4(a0),d5
                    move.b   11(a0),d0
                    lsl.w    #8,d0
                    move.b   10(a0),d0
                    move.b   9(a0),d1
                    lsl.w    #8,d1
                    move.b   8(a0),d1
                    move.b   7(a0),d2
                    lsl.w    #8,d2
                    move.b   6(a0),d2
                    move.w   d0,(a0)+
                    move.w   d1,(a0)+
                    move.w   d2,(a0)+
                    move.w   d3,(a0)+
                    move.w   d4,(a0)+
                    move.w   d5,(a0)+
                    dbra     d6,lbC00133C
                    dbra     d7,lbC001338
                    move.l   pbmMap-DT(a4),a0
                    moveq    #0,d0
                    move.l   d0,$BC0(a0)
                    move.l   d0,$BC4(a0)
                    move.l   d0,$BC8(a0)
                    move.l   d0,$BCC(a0)
                    move.l   #$3C003C00,$BD0(a0)
                    move.l   #$3C003C00,$BD4(a0)
                    move.l   #$C000C00,$BD8(a0)
                    move.l   #$30003000,$BDC(a0)
                    move.l   d0,$BE0(a0)
                    move.l   d0,$BE4(a0)
                    move.l   d0,$BE8(a0)
                    move.l   #$3FFC3FFC,$BEC(a0)
                    move.l   #$3FFC3FFC,$BF0(a0)
                    move.l   d0,$BF4(a0)
                    move.l   d0,$BF8(a0)
                    move.l   d0,$BFC(a0)
                    rts

InitMap:            move.w   #1,iShoot-DT(a4)
                    move.l   prgbBuffer1-DT(a4),a0
                    add.w    #$C0,a0
                    move.b   (a0)+,bShootableWall1-DT(a4)
                    move.b   (a0)+,bShootableWall2-DT(a4)
                    move.l   prgbBuffer1-DT(a4),a0
                    add.w    #640,a0
                    move.l   prgbDispMap-DT(a4),a1
                    lea      lbW005F86-DT(a4),a2
                    move.w   cLevel-DT(a4),d0
                    and.w    #15,d0
                    add.w    d0,d0
                    move.w   0(a2,d0.w),d5
                    add.b    d5,bShootableWall1-DT(a4)
                    add.b    d5,bShootableWall2-DT(a4)
                    lea      rgobjTeki-DT(a4),a2
                    moveq    #0,d6
                    clr.l    d4
lbC00141C:          move.l   d4,d1
                    add.l    d1,d1
                    clr.l    d3
lbC001422:          move.w   #1,d2
                    clr.l    d0
                    move.b   (a0)+,d0
                    bpl      lbC0014B6
                    cmp.b    #$FF,d0
                    beq      lbC0014C8
                    btst     #6,d0
                    beq.s    lbC001448
                    move.w   -2(a1,d1.l),d0
                    or.w     #$40,d0
                    bra      lbC0014C8

lbC001448:          move.w   d4,2(a2)
                    move.w   d3,4(a2)
                    and.w    #$3F,d0
                    move.w   d0,8(a2)
                    move.w   d0,10(a2)
                    move.l   prgbBuffer1-DT(a4),a6
                    add.w    #$40,a6
                    moveq    #0,d7
                    move.b   0(a6,d0.w),d7
                    lsr.b    #4,d7
                    and.w    #7,d7
                    lea      lbB00163A(pc),a6
                    move.b   0(a6,d7.w),d7
                    move.w   d7,(a2)
                    move.l   prgbBuffer1-DT(a4),a6
                    add.w    #$80,a6
                    moveq    #0,d7
                    move.b   0(a6,d0.w),d7
                    and.w    #15,d7
                    move.w   d7,6(a2)
                    lea      lbB00162E(pc),a6
                    move.b   0(a6,d7.w),d7
                    move.w   d7,12(a2)
                    move.w   d7,14(a2)
                    or.w     #$80,d0
                    move.w   d6,d7
                    lsl.w    #8,d7
                    or.w     d7,d0
                    clr.w    $10(a2)
                    add.w    #$16,a2
                    addq.w   #1,d6
                    bra.s    lbC0014C8

lbC0014B6:          move.w   d0,d2
                    and.w    #15,d0
                    and.w    #$70,d2
                    lsr.w    #4,d2
                    bne.s    lbC0014C8
                    move.w   #8,d2
lbC0014C8:          subq.w   #1,d2
                    btst     #7,d0
                    bne.s    lbC0014D6
                    tst.b    d0
                    beq.s    lbC0014D6
                    add.w    d5,d0
lbC0014D6:          move.w   d0,0(a1,d1.l)
                    add.l    #$400,d1
                    addq.w   #1,d3
                    dbra     d2,lbC0014D6
                    cmp.w    #$2C,d3
                    blt      lbC001422
                    bgt.s    lbC0014FC
                    addq.l   #1,d4
                    cmp.w    #$1E0,d4
                    beq.s    lbC0014FC
                    bra      lbC00141C

lbC0014FC:          tst.w    cLevel-DT(a4)
                    beq.s    lbC00152E
                    lea      lbL0058A6-DT(a4),a0
                    move.l   prgbDispMap-DT(a4),a1
                    move.w   #$15,d2
lbC00150E:          move.w   #$27,d1
lbC001512:          move.w   (a0)+,d0
                    cmp.w    #-1,d0
                    beq.s    lbC001520
                    tst.b    d0
                    bpl.s    lbC001520
                    moveq    #0,d0
lbC001520:          move.w   d0,(a1)+
                    dbra     d1,lbC001512
                    add.w    #$3B0,a1
                    dbra     d2,lbC00150E
lbC00152E:          sub.w    #$28,d4
                    move.w   d4,xMapMax-DT(a4)
                    move.w   #$230,d4
                    move.w   #$3B0,d5
                    move.l   prgbDispMap-DT(a4),a0
                    lea      lbL0058A6-DT(a4),a1
                    move.l   pBitPlane0-DT(a4),a2
                    move.l   pBitPlane1-DT(a4),a3
                    move.l   pbmMap-DT(a4),a6
                    move.w   #$15,d2
lbC001556:          move.w   #$27,d1
lbC00155A:          moveq    #0,d0
                    move.w   (a0)+,d0
                    move.w   d0,(a1)+
                    move.l   a6,a5
                    cmp.w    #$FFFF,d0
                    bne.s    lbC00156C
                    move.w   #0,d0
lbC00156C:          tst.b    d0
                    bmi.s    _DrawTeki
                    lsl.w    #5,d0
                    add.w    d0,a5
                    move.w   (a5)+,(a2)+
                    move.w   (a5)+,(a3)+
                    move.w   (a5)+,$4E(a2)
                    move.w   (a5)+,$4E(a3)
                    move.w   (a5)+,$9E(a2)
                    move.w   (a5)+,$9E(a3)
                    move.w   (a5)+,$EE(a2)
                    move.w   (a5)+,$EE(a3)
                    move.w   (a5)+,$13E(a2)
                    move.w   (a5)+,$13E(a3)
                    move.w   (a5)+,$18E(a2)
                    move.w   (a5)+,$18E(a3)
                    move.w   (a5)+,$1DE(a2)
                    move.w   (a5)+,$1DE(a3)
                    move.w   (a5)+,$22E(a2)
                    move.w   (a5)+,$22E(a3)
lbC0015B0:          dbra     d1,lbC00155A
                    add.w    d4,a2
                    add.w    d4,a3
                    add.w    d5,a0
                    dbra     d2,lbC001556
                    bra.s    lbC0015C6

_DrawTeki:          jsr      DrawTeki(pc)
                    bra.s    lbC0015B0

lbC0015C6:          move.l   prgbBuffer1-DT(a4),a0
                    add.w    #$26E0,a0
                    move.w   #2,d2
lbC0015D2:          move.w   #$27,d1
lbC0015D6:          clr.l    d0
                    move.b   (a0)+,d0
                    move.l   a6,a5
                    lsl.w    #5,d0
                    add.w    d0,a5
                    move.w   (a5)+,(a2)+
                    move.w   (a5)+,(a3)+
                    move.w   (a5)+,$4E(a2)
                    move.w   (a5)+,$4E(a3)
                    move.w   (a5)+,$9E(a2)
                    move.w   (a5)+,$9E(a3)
                    move.w   (a5)+,$EE(a2)
                    move.w   (a5)+,$EE(a3)
                    move.w   (a5)+,$13E(a2)
                    move.w   (a5)+,$13E(a3)
                    move.w   (a5)+,$18E(a2)
                    move.w   (a5)+,$18E(a3)
                    move.w   (a5)+,$1DE(a2)
                    move.w   (a5)+,$1DE(a3)
                    move.w   (a5)+,$22E(a2)
                    move.w   (a5)+,$22E(a3)
                    dbra     d1,lbC0015D6
                    add.w    d4,a2
                    add.w    d4,a3
                    dbra     d2,lbC0015D2
                    jsr      PrintScore(pc)
                    rts

lbB00162E:          dc.b     0,7,0,0,0,3,1,1,0,0,3,0
lbB00163A:          dc.b     1,3,5,7,9,11,13,100

DrawMap:            clr.l    d3
                    move.w   #-1,iTekiShoot-DT(a4)
                    move.w   #$230,d4
                    move.w   #$3B0,d5
                    move.l   prgbDispMap-DT(a4),a0
                    add.w    iMapOffSetCur-DT(a4),a0
                    lea      lbL0058A6-DT(a4),a1
                    move.l   pBitPlane0-DT(a4),a2
                    move.l   pBitPlane1-DT(a4),a3
                    move.w   #$15,d2
lbC00166A:          move.w   #$27,d1
lbC00166E:          moveq    #0,d0
                    moveq    #0,d6
                    move.w   (a1),d6
                    move.w   (a0)+,d0
                    move.w   d0,(a1)+
                    cmp.w    #-1,d0
                    beq.s    lbC001686
                    tst.b    d0
                    bmi.s    lbC0016E8
                    cmp.w    d0,d6
                    bne.s    lbC00168C
lbC001686:          addq.l   #2,a2
                    addq.l   #2,a3
                    bra.s    lbC0016D4

lbC00168C:          move.l   pbmMap-DT(a4),a5
                    tst.b    d0
                    beq.s    lbC001698
                    lsl.w    #5,d0
                    add.w    d0,a5
lbC001698:          move.w   (a5)+,(a2)+
                    move.w   (a5)+,(a3)+
                    move.w   (a5)+,$4E(a2)
                    move.w   (a5)+,$4E(a3)
                    move.w   (a5)+,$9E(a2)
                    move.w   (a5)+,$9E(a3)
                    move.w   (a5)+,$EE(a2)
                    move.w   (a5)+,$EE(a3)
                    move.w   (a5)+,$13E(a2)
                    move.w   (a5)+,$13E(a3)
                    move.w   (a5)+,$18E(a2)
                    move.w   (a5)+,$18E(a3)
                    move.w   (a5)+,$1DE(a2)
                    move.w   (a5)+,$1DE(a3)
                    move.w   (a5)+,$22E(a2)
                    move.w   (a5)+,$22E(a3)
lbC0016D4:          dbra     d1,lbC00166E
                    add.w    d4,a2
                    add.w    d4,a3
                    add.w    d5,a0
                    dbra     d2,lbC00166A
                    move.w   d3,cTekiOnScrn-DT(a4)
                    rts

lbC0016E8:          tst.w    wQuad-DT(a4)
                    beq.s    _DrawTeki0
                    move.w   #$15,d6
                    sub.w    d2,d6
                    move.w   d6,lbW005874-DT(a4)
                    move.w   #$27,d6
                    sub.w    d1,d6
                    move.w   d6,lbW005872-DT(a4)
                    move.w   xThexScrOffSet-DT(a4),d7
                    cmp.w    #2,wQuad-DT(a4)
                    bne.s    lbC001716
                    addq.w   #2,d7
                    cmp.w    d7,d6
                    blt.s    _DrawTeki0
                    bra.s    lbC00171C

lbC001716:          addq.w   #1,d7
                    cmp.w    d7,d6
                    bge.s    _DrawTeki0
lbC00171C:          bsr.s    lbC001740
                    tst.w    (a6)
                    beq.s    _DrawTeki0
                    addq.w   #1,d3
                    cmp.w    iShoot-DT(a4),d3
                    bne.s    _DrawTeki0
                    move.w   d0,iTekiShoot-DT(a4)
                    move.w   lbW005872-DT(a4),xTekiShoot-DT(a4)
                    move.w   lbW005874-DT(a4),yTekiShoot-DT(a4)
_DrawTeki0:         jsr      DrawTeki(pc)
                    bra.s    lbC0016D4

lbC001740:          movem.l  d0-d3,-(sp)
                    lea      rgobjTeki-DT(a4),a6
                    moveq    #0,d3
                    cmp.b    #-1,d0
                    bne.s    lbC00175A
                    move.w   #2,d3
                    moveq    #0,d0
                    move.w   -$402(a0),d0
lbC00175A:          btst     #6,d0
                    beq.s    lbC001762
                    addq.w   #1,d3
lbC001762:          move.w   d0,d1
                    move.w   d0,d2
                    lsr.w    #8,d2
                    mulu     #$16,d2
                    add.w    d2,a6
                    movem.l  (sp)+,d0-d3
                    rts

DrawThexDying:      jsr      InsertThexInMap(pc)
                    lea      table_y_lines(pc),a0
                    move.w   xThexScrOffSet-DT(a4),d1
                    add.w    d1,d1
                    move.w   yThexScrOffSet-DT(a4),d0
                    cmp.w    #9,fState-DT(a4)
                    add.w    d0,d0
                    add.w    0(a0,d0.w),d1
                    move.w   frmThex-DT(a4),d0
                    mulu     #288,d0
                    move.l   pbmDying-DT(a4),a3
                    add.w    d0,a3
                    move.l   pBitPlane0-DT(a4),a1
                    add.w    d1,a1
                    move.l   pBitPlane1-DT(a4),a2
                    add.w    d1,a2
                    jsr      lbC001976(pc)
                    move.l   pBitPlane2-DT(a4),a2
                    add.w    d1,a2
                    jsr      lbC0018B8(pc)
                    move.l   pBitPlane2-DT(a4),a2
                    add.w    d1,a2
                    sub.w    #640,a2
                    jmp      lbC0018B8(pc)

DrawThex:           lea      table_y_lines(pc),a0
                    move.w   xThexScrOffSet-DT(a4),d1
                    add.w    d1,d1
                    move.w   yThexScrOffSet-DT(a4),d0
                    add.w    d0,d0
                    add.w    0(a0,d0.w),d1
                    move.w   frmThex-DT(a4),d0
                    cmp.w    #$18,d0
                    blt.s    lbC0017F0
                    lea      lbC001976(pc),a0
                    lea      lbC0018B8(pc),a6
                    bra.s    lbC0017F8

lbC0017F0:          lea      lbC001AF0(pc),a0
                    lea      lbC001CEA(pc),a6
lbC0017F8:          lea      lbW00182C(pc),a1
                    add.w    d0,d0
                    move.l   pbmThex34-DT(a4),a3
                    add.w    0(a1,d0.w),a3
                    move.l   pBitPlane0-DT(a4),a1
                    add.w    d1,a1
                    move.w   fFlash-DT(a4),d0
                    beq.s    lbC001824
                    subq.w   #1,d0
                    move.w   d0,fFlash-DT(a4)
                    move.l   pBitPlane1-DT(a4),a2
                    add.w    d1,a2
                    jsr      (a6)
                    move.l   a1,a2
                    bra.s    lbC00182A

lbC001824:          move.l   pBitPlane1-DT(a4),a2
                    add.w    d1,a2
lbC00182A:          jmp      (a0)

lbW00182C:          dc.w     0,$180,$300,$480,$600,$780,$900,$A80,$C00,$D80
                    dc.w     $F00,$1080,$1200,$1380,$1500,$1680,$1800,$1980
                    dc.w     $1B00,$1C80,$1E00,$1F80,$2100,$2280,$2400,$2520
                    dc.w     $2640,$2760,$2880,$29A0,$2AC0,$2BE0,$2D00,$2E20
                    dc.w     $2F40,$3060,$3180,$32A0,$33C0,$34E0,$3600,$3720
                    dc.w     $3840,$3960,$3A80,$3BA0,$3CC0,$3DE0

table_y_lines:      dc.w     0,640,640*2,640*3,640*4,640*5,640*6,640*7,640*8,640*9
                    dc.w     640*10,640*11,640*12,640*13,640*14,640*15,640*16,640*17
                    dc.w     640*18,640*19,640*20,640*21

lbC0018B8:          clr.l    (a2)+
                    clr.w    (a2)+
                    clr.l    $4A(a2)
                    clr.w    $4E(a2)
                    clr.l    $9A(a2)
                    clr.w    $9E(a2)
                    clr.l    $EA(a2)
                    clr.w    $EE(a2)
                    clr.l    $13A(a2)
                    clr.w    $13E(a2)
                    clr.l    $18A(a2)
                    clr.w    $18E(a2)
                    clr.l    $1DA(a2)
                    clr.w    $1DE(a2)
                    clr.l    $22A(a2)
                    clr.w    $22E(a2)
                    clr.l    $27A(a2)
                    clr.w    $27E(a2)
                    clr.l    $2CA(a2)
                    clr.w    $2CE(a2)
                    clr.l    $31A(a2)
                    clr.w    $31E(a2)
                    clr.l    $36A(a2)
                    clr.w    $36E(a2)
                    clr.l    $3BA(a2)
                    clr.w    $3BE(a2)
                    clr.l    $40A(a2)
                    clr.w    $40E(a2)
                    clr.l    $45A(a2)
                    clr.w    $45E(a2)
                    clr.l    $4AA(a2)
                    clr.w    $4AE(a2)
                    clr.l    $4FA(a2)
                    clr.w    $4FE(a2)
                    clr.l    $54A(a2)
                    clr.w    $54E(a2)
                    clr.l    $59A(a2)
                    clr.w    $59E(a2)
                    clr.l    $5EA(a2)
                    clr.w    $5EE(a2)
                    clr.l    $63A(a2)
                    clr.w    $63E(a2)
                    clr.l    $68A(a2)
                    clr.w    $68E(a2)
                    clr.l    $6DA(a2)
                    clr.w    $6DE(a2)
                    clr.l    $72A(a2)
                    clr.w    $72E(a2)
                    rts

lbC001976:          move.l   (a3)+,(a1)+
                    move.w   (a3)+,(a1)+
                    move.l   (a3)+,(a2)+
                    move.w   (a3)+,(a2)+
                    move.l   (a3)+,$4A(a1)
                    move.w   (a3)+,$4E(a1)
                    move.l   (a3)+,$4A(a2)
                    move.w   (a3)+,$4E(a2)
                    move.l   (a3)+,$9A(a1)
                    move.w   (a3)+,$9E(a1)
                    move.l   (a3)+,$9A(a2)
                    move.w   (a3)+,$9E(a2)
                    move.l   (a3)+,$EA(a1)
                    move.w   (a3)+,$EE(a1)
                    move.l   (a3)+,$EA(a2)
                    move.w   (a3)+,$EE(a2)
                    move.l   (a3)+,$13A(a1)
                    move.w   (a3)+,$13E(a1)
                    move.l   (a3)+,$13A(a2)
                    move.w   (a3)+,$13E(a2)
                    move.l   (a3)+,$18A(a1)
                    move.w   (a3)+,$18E(a1)
                    move.l   (a3)+,$18A(a2)
                    move.w   (a3)+,$18E(a2)
                    move.l   (a3)+,$1DA(a1)
                    move.w   (a3)+,$1DE(a1)
                    move.l   (a3)+,$1DA(a2)
                    move.w   (a3)+,$1DE(a2)
                    move.l   (a3)+,$22A(a1)
                    move.w   (a3)+,$22E(a1)
                    move.l   (a3)+,$22A(a2)
                    move.w   (a3)+,$22E(a2)
                    move.l   (a3)+,$27A(a1)
                    move.w   (a3)+,$27E(a1)
                    move.l   (a3)+,$27A(a2)
                    move.w   (a3)+,$27E(a2)
                    move.l   (a3)+,$2CA(a1)
                    move.w   (a3)+,$2CE(a1)
                    move.l   (a3)+,$2CA(a2)
                    move.w   (a3)+,$2CE(a2)
                    move.l   (a3)+,$31A(a1)
                    move.w   (a3)+,$31E(a1)
                    move.l   (a3)+,$31A(a2)
                    move.w   (a3)+,$31E(a2)
                    move.l   (a3)+,$36A(a1)
                    move.w   (a3)+,$36E(a1)
                    move.l   (a3)+,$36A(a2)
                    move.w   (a3)+,$36E(a2)
                    move.l   (a3)+,$3BA(a1)
                    move.w   (a3)+,$3BE(a1)
                    move.l   (a3)+,$3BA(a2)
                    move.w   (a3)+,$3BE(a2)
                    move.l   (a3)+,$40A(a1)
                    move.w   (a3)+,$40E(a1)
                    move.l   (a3)+,$40A(a2)
                    move.w   (a3)+,$40E(a2)
                    move.l   (a3)+,$45A(a1)
                    move.w   (a3)+,$45E(a1)
                    move.l   (a3)+,$45A(a2)
                    move.w   (a3)+,$45E(a2)
                    move.l   (a3)+,$4AA(a1)
                    move.w   (a3)+,$4AE(a1)
                    move.l   (a3)+,$4AA(a2)
                    move.w   (a3)+,$4AE(a2)
                    move.l   (a3)+,$4FA(a1)
                    move.w   (a3)+,$4FE(a1)
                    move.l   (a3)+,$4FA(a2)
                    move.w   (a3)+,$4FE(a2)
                    move.l   (a3)+,$54A(a1)
                    move.w   (a3)+,$54E(a1)
                    move.l   (a3)+,$54A(a2)
                    move.w   (a3)+,$54E(a2)
                    move.l   (a3)+,$59A(a1)
                    move.w   (a3)+,$59E(a1)
                    move.l   (a3)+,$59A(a2)
                    move.w   (a3)+,$59E(a2)
                    move.l   (a3)+,$5EA(a1)
                    move.w   (a3)+,$5EE(a1)
                    move.l   (a3)+,$5EA(a2)
                    move.w   (a3)+,$5EE(a2)
                    move.l   (a3)+,$63A(a1)
                    move.w   (a3)+,$63E(a1)
                    move.l   (a3)+,$63A(a2)
                    move.w   (a3)+,$63E(a2)
                    move.l   (a3)+,$68A(a1)
                    move.w   (a3)+,$68E(a1)
                    move.l   (a3)+,$68A(a2)
                    move.w   (a3)+,$68E(a2)
                    move.l   (a3)+,$6DA(a1)
                    move.w   (a3)+,$6DE(a1)
                    move.l   (a3)+,$6DA(a2)
                    move.w   (a3)+,$6DE(a2)
                    move.l   (a3)+,$72A(a1)
                    move.w   (a3)+,$72E(a1)
                    move.l   (a3)+,$72A(a2)
                    move.w   (a3)+,$72E(a2)
                    rts

lbC001AF0:          move.l   (a3)+,(a1)+
                    move.w   (a3)+,(a1)+
                    move.l   (a3)+,(a2)+
                    move.w   (a3)+,(a2)+
                    move.l   (a3)+,$4A(a1)
                    move.w   (a3)+,$4E(a1)
                    move.l   (a3)+,$4A(a2)
                    move.w   (a3)+,$4E(a2)
                    move.l   (a3)+,$9A(a1)
                    move.w   (a3)+,$9E(a1)
                    move.l   (a3)+,$9A(a2)
                    move.w   (a3)+,$9E(a2)
                    move.l   (a3)+,$EA(a1)
                    move.w   (a3)+,$EE(a1)
                    move.l   (a3)+,$EA(a2)
                    move.w   (a3)+,$EE(a2)
                    move.l   (a3)+,$13A(a1)
                    move.w   (a3)+,$13E(a1)
                    move.l   (a3)+,$13A(a2)
                    move.w   (a3)+,$13E(a2)
                    move.l   (a3)+,$18A(a1)
                    move.w   (a3)+,$18E(a1)
                    move.l   (a3)+,$18A(a2)
                    move.w   (a3)+,$18E(a2)
                    move.l   (a3)+,$1DA(a1)
                    move.w   (a3)+,$1DE(a1)
                    move.l   (a3)+,$1DA(a2)
                    move.w   (a3)+,$1DE(a2)
                    move.l   (a3)+,$22A(a1)
                    move.w   (a3)+,$22E(a1)
                    move.l   (a3)+,$22A(a2)
                    move.w   (a3)+,$22E(a2)
                    move.l   (a3)+,$27A(a1)
                    move.w   (a3)+,$27E(a1)
                    move.l   (a3)+,$27A(a2)
                    move.w   (a3)+,$27E(a2)
                    move.l   (a3)+,$2CA(a1)
                    move.w   (a3)+,$2CE(a1)
                    move.l   (a3)+,$2CA(a2)
                    move.w   (a3)+,$2CE(a2)
                    move.l   (a3)+,$31A(a1)
                    move.w   (a3)+,$31E(a1)
                    move.l   (a3)+,$31A(a2)
                    move.w   (a3)+,$31E(a2)
                    move.l   (a3)+,$36A(a1)
                    move.w   (a3)+,$36E(a1)
                    move.l   (a3)+,$36A(a2)
                    move.w   (a3)+,$36E(a2)
                    move.l   (a3)+,$3BA(a1)
                    move.w   (a3)+,$3BE(a1)
                    move.l   (a3)+,$3BA(a2)
                    move.w   (a3)+,$3BE(a2)
                    move.l   (a3)+,$40A(a1)
                    move.w   (a3)+,$40E(a1)
                    move.l   (a3)+,$40A(a2)
                    move.w   (a3)+,$40E(a2)
                    move.l   (a3)+,$45A(a1)
                    move.w   (a3)+,$45E(a1)
                    move.l   (a3)+,$45A(a2)
                    move.w   (a3)+,$45E(a2)
                    move.l   (a3)+,$4AA(a1)
                    move.w   (a3)+,$4AE(a1)
                    move.l   (a3)+,$4AA(a2)
                    move.w   (a3)+,$4AE(a2)
                    move.l   (a3)+,$4FA(a1)
                    move.w   (a3)+,$4FE(a1)
                    move.l   (a3)+,$4FA(a2)
                    move.w   (a3)+,$4FE(a2)
                    move.l   (a3)+,$54A(a1)
                    move.w   (a3)+,$54E(a1)
                    move.l   (a3)+,$54A(a2)
                    move.w   (a3)+,$54E(a2)
                    move.l   (a3)+,$59A(a1)
                    move.w   (a3)+,$59E(a1)
                    move.l   (a3)+,$59A(a2)
                    move.w   (a3)+,$59E(a2)
                    move.l   (a3)+,$5EA(a1)
                    move.w   (a3)+,$5EE(a1)
                    move.l   (a3)+,$5EA(a2)
                    move.w   (a3)+,$5EE(a2)
                    move.l   (a3)+,$63A(a1)
                    move.w   (a3)+,$63E(a1)
                    move.l   (a3)+,$63A(a2)
                    move.w   (a3)+,$63E(a2)
                    move.l   (a3)+,$68A(a1)
                    move.w   (a3)+,$68E(a1)
                    move.l   (a3)+,$68A(a2)
                    move.w   (a3)+,$68E(a2)
                    move.l   (a3)+,$6DA(a1)
                    move.w   (a3)+,$6DE(a1)
                    move.l   (a3)+,$6DA(a2)
                    move.w   (a3)+,$6DE(a2)
                    move.l   (a3)+,$72A(a1)
                    move.w   (a3)+,$72E(a1)
                    move.l   (a3)+,$72A(a2)
                    move.w   (a3)+,$72E(a2)
                    move.l   (a3)+,$77A(a1)
                    move.w   (a3)+,$77E(a1)
                    move.l   (a3)+,$77A(a2)
                    move.w   (a3)+,$77E(a2)
                    move.l   (a3)+,$7CA(a1)
                    move.w   (a3)+,$7CE(a1)
                    move.l   (a3)+,$7CA(a2)
                    move.w   (a3)+,$7CE(a2)
                    move.l   (a3)+,$81A(a1)
                    move.w   (a3)+,$81E(a1)
                    move.l   (a3)+,$81A(a2)
                    move.w   (a3)+,$81E(a2)
                    move.l   (a3)+,$86A(a1)
                    move.w   (a3)+,$86E(a1)
                    move.l   (a3)+,$86A(a2)
                    move.w   (a3)+,$86E(a2)
                    move.l   (a3)+,$8BA(a1)
                    move.w   (a3)+,$8BE(a1)
                    move.l   (a3)+,$8BA(a2)
                    move.w   (a3)+,$8BE(a2)
                    move.l   (a3)+,$90A(a1)
                    move.w   (a3)+,$90E(a1)
                    move.l   (a3)+,$90A(a2)
                    move.w   (a3)+,$90E(a2)
                    move.l   (a3)+,$95A(a1)
                    move.w   (a3)+,$95E(a1)
                    move.l   (a3)+,$95A(a2)
                    move.w   (a3)+,$95E(a2)
                    move.l   (a3)+,$9AA(a1)
                    move.w   (a3)+,$9AE(a1)
                    move.l   (a3)+,$9AA(a2)
                    move.w   (a3)+,$9AE(a2)
                    rts

lbC001CEA:          clr.l    (a2)+
                    clr.w    (a2)+
                    clr.l    $4A(a2)
                    clr.w    $4E(a2)
                    clr.l    $9A(a2)
                    clr.w    $9E(a2)
                    clr.l    $EA(a2)
                    clr.w    $EE(a2)
                    clr.l    $13A(a2)
                    clr.w    $13E(a2)
                    clr.l    $18A(a2)
                    clr.w    $18E(a2)
                    clr.l    $1DA(a2)
                    clr.w    $1DE(a2)
                    clr.l    $22A(a2)
                    clr.w    $22E(a2)
                    clr.l    $27A(a2)
                    clr.w    $27E(a2)
                    clr.l    $2CA(a2)
                    clr.w    $2CE(a2)
                    clr.l    $31A(a2)
                    clr.w    $31E(a2)
                    clr.l    $36A(a2)
                    clr.w    $36E(a2)
                    clr.l    $3BA(a2)
                    clr.w    $3BE(a2)
                    clr.l    $40A(a2)
                    clr.w    $40E(a2)
                    clr.l    $45A(a2)
                    clr.w    $45E(a2)
                    clr.l    $4AA(a2)
                    clr.w    $4AE(a2)
                    clr.l    $4FA(a2)
                    clr.w    $4FE(a2)
                    clr.l    $54A(a2)
                    clr.w    $54E(a2)
                    clr.l    $59A(a2)
                    clr.w    $59E(a2)
                    clr.l    $5EA(a2)
                    clr.w    $5EE(a2)
                    clr.l    $63A(a2)
                    clr.w    $63E(a2)
                    clr.l    $68A(a2)
                    clr.w    $68E(a2)
                    clr.l    $6DA(a2)
                    clr.w    $6DE(a2)
                    clr.l    $72A(a2)
                    clr.w    $72E(a2)
                    clr.l    $77A(a2)
                    clr.w    $77E(a2)
                    clr.l    $7CA(a2)
                    clr.w    $7CE(a2)
                    clr.l    $81A(a2)
                    clr.w    $81E(a2)
                    clr.l    $86A(a2)
                    clr.w    $86E(a2)
                    clr.l    $8BA(a2)
                    clr.w    $8BE(a2)
                    clr.l    $90A(a2)
                    clr.w    $90E(a2)
                    clr.l    $95A(a2)
                    clr.w    $95E(a2)
                    clr.l    $9AA(a2)
                    clr.w    $9AE(a2)
                    rts

Random:             moveq    #0,d0
                    move.w   RandomNum-DT(a4),d0
                    add.w    #$9248,d0
                    ror.w    #3,d0
                    move.w   d0,RandomNum-DT(a4)
                    rts

OpenAll:            tst.l    pGfxBase-DT(a4)
                    bne      lbC0020C8
                    move.l   4.w,a6
                    lea      GraphicsName-DT(a4),a1
                    moveq    #0,d0
                    jsr      _LVOOpenLibrary(a6)
                    move.l   d0,pGfxBase-DT(a4)
                    lea      IntuitionName-DT(a4),a1
                    moveq    #0,d0
                    jsr      _LVOOpenLibrary(a6)
                    move.l   d0,pIntuitionBase-DT(a4)
                    move.l   d0,a6
                    lea      nscrGame-DT(a4),a0
                    jsr      _LVOOpenScreen(a6)
                    move.l   d0,pscrGame-DT(a4)
                    beq      Bye
                    move.l   d0,a0
                    move.l   192(a0),pBitPlane0-DT(a4)
                    move.l   196(a0),pBitPlane1-DT(a4)
                    move.l   200(a0),pBitPlane2-DT(a4)
                    move.l   4.w,a6
                    lea      DosName-DT(a4),a1
                    moveq    #0,d0
                    jsr      _LVOOpenLibrary(a6)
                    move.l   d0,pDosBase-DT(a4)
                    move.l   4.w,a6
                    move.l   #$10000,d1
                    move.l   #$B000,d0
                    jsr      _LVOAllocMem(a6)
                    tst.l    d0
                    beq      Bye
                    move.l   d0,prgbDispMap-DT(a4)
                    move.l   #$10000,d1
                    move.l   #$73FA,d0
                    jsr      _LVOAllocMem(a6)
                    tst.l    d0
                    beq      Bye
                    move.l   d0,pbmAll-DT(a4)
                    move.l   #$10000,d1
                    move.l   #$2780,d0
                    jsr      _LVOAllocMem(a6)
                    tst.l    d0
                    beq      Bye
                    move.l   d0,prgbBuffer1-DT(a4)
                    move.l   #$10000,d1
                    move.l   #$2760,d0
                    jsr      _LVOAllocMem(a6)
                    tst.l    d0
                    beq      Bye
                    move.l   d0,lbL006334-DT(a4)
                    move.l   #$10000,d1
                    move.l   #$1680,d0
                    jsr      _LVOAllocMem(a6)
                    tst.l    d0
                    beq      Bye
                    move.l   d0,prgbMoonMusic-DT(a4)
                    move.l   #$10000,d1
                    move.l   #$F00,d0
                    jsr      _LVOAllocMem(a6)
                    tst.l    d0
                    beq      Bye
                    move.l   d0,prgbThemeMusic-DT(a4)
                    moveq    #2,d1
                    move.l   #$78B4,d0
                    jsr      _LVOAllocMem(a6)
                    tst.l    d0
                    beq      Bye
                    move.l   d0,prgbaudWaveForms-DT(a4)
                    clr.l    -(sp)
                    pea      szSdWaveForms-DT(a4)
                    move.l   #0,-(sp)
                    move.l   prgbaudWaveForms-DT(a4),-(sp)
                    move.l   #$78B4,-(sp)
                    jsr      LoadFile(pc)
                    move.l   (sp)+,d0
                    bmi      Bye
                    moveq    #2,d1
                    move.l   #$4D30,d0
                    jsr      _LVOAllocMem(a6)
                    tst.l    d0
                    beq      Bye
                    move.l   d0,prgbSpPres-DT(a4)
                    clr.l    -(sp)
                    pea      szSpPres-DT(a4)
                    move.l   #0,-(sp)
                    move.l   prgbSpPres-DT(a4),-(sp)
                    move.l   #$4D30,-(sp)
                    jsr      LoadFile(pc)
                    move.l   (sp)+,d0
                    bmi      Bye
                    moveq    #2,d1
                    move.l   #$4B8F,d0
                    jsr      _LVOAllocMem(a6)
                    tst.l    d0
                    beq      Bye
                    move.l   d0,prgbSpWarn-DT(a4)
                    clr.l    -(sp)
                    pea      szSpWarn-DT(a4)
                    move.l   #0,-(sp)
                    move.l   prgbSpWarn-DT(a4),-(sp)
                    move.l   #$4B8F,-(sp)
                    jsr      LoadFile(pc)
                    move.l   (sp)+,d0
                    bmi      Bye
                    moveq    #2,d1
                    moveq    #$40,d0
                    jsr      _LVOAllocMem(a6)
                    tst.l    d0
                    beq      Bye
                    move.l   d0,prgbaudNullWaveForm-DT(a4)
                    move.l   d0,a0
                    moveq    #0,d0
                    moveq    #15,d1
lbC001FB4:          move.l   d0,(a0)+
                    dbra     d1,lbC001FB4
                    move.l   #$10000,d1
                    move.l   #$3E80,d0
                    jsr      _LVOAllocMem(a6)
                    tst.l    d0
                    beq      Bye
                    move.l   d0,pRedBuff-DT(a4)
                    move.l   #$10000,d1
                    move.l   #$3E80,d0
                    jsr      _LVOAllocMem(a6)
                    tst.l    d0
                    beq      Bye
                    move.l   d0,pGreenBuff-DT(a4)
                    move.l   #$10000,d1
                    move.l   #$3E80,d0
                    jsr      _LVOAllocMem(a6)
                    tst.l    d0
                    beq      Bye
                    move.l   d0,pBlueBuff-DT(a4)
                    clr.l    -(sp)
                    pea      dataBluePlane_MSG-DT(a4)
                    move.l   #0,-(sp)
                    move.l   pBlueBuff-DT(a4),-(sp)
                    move.l   #$3E80,-(sp)
                    jsr      LoadFile(pc)
                    move.l   (sp)+,d0
                    bmi      Bye
                    clr.l    -(sp)
                    pea      dataGreenPlan_MSG-DT(a4)
                    move.l   #0,-(sp)
                    move.l   pGreenBuff-DT(a4),-(sp)
                    move.l   #$3E80,-(sp)
                    jsr      LoadFile(pc)
                    move.l   (sp)+,d0
                    bmi      Bye
                    clr.l    -(sp)
                    pea      dataRedPlane_MSG-DT(a4)
                    move.l   #0,-(sp)
                    move.l   pRedBuff-DT(a4),-(sp)
                    move.l   #$3E80,-(sp)
                    jsr      LoadFile(pc)
                    move.l   (sp)+,d0
                    bmi      Bye
                    clr.l    -(sp)
                    pea      dataLogo_MSG-DT(a4)
                    move.l   #0,-(sp)
                    move.l   lbL006334-DT(a4),-(sp)
                    move.l   #$2760,-(sp)
                    jsr      LoadFile(pc)
                    move.l   (sp)+,d0
                    bmi      Bye
                    clr.l    -(sp)
                    pea      dataMoonMusic_MSG-DT(a4)
                    move.l   #0,-(sp)
                    move.l   prgbMoonMusic-DT(a4),-(sp)
                    move.l   #$1680,-(sp)
                    jsr      LoadFile(pc)
                    move.l   (sp)+,d0
                    bmi      Bye
                    clr.l    -(sp)
                    pea      dataThexMusic_MSG-DT(a4)
                    move.l   #0,-(sp)
                    move.l   prgbThemeMusic-DT(a4),-(sp)
                    move.l   #$F00,-(sp)
                    jsr      LoadFile(pc)
                    move.l   (sp)+,d0
                    bmi      Bye
lbC0020C8:          clr.l    -(sp)
                    pea      datat1_MSG-DT(a4)
                    move.l   #6,-(sp)
                    move.l   pbmAll-DT(a4),-(sp)
                    move.l   #$73FA,-(sp)
                    jsr      LoadFile(pc)
                    move.l   (sp)+,d0
                    bmi      Bye
                    clr.l    -(sp)
                    pea      datatx_MSG-DT(a4)
                    move.l   #0,-(sp)
                    move.l   prgbBuffer1-DT(a4),-(sp)
                    move.l   #$2780,-(sp)
                    jsr      LoadFile(pc)
                    move.l   (sp)+,d0
                    bmi      Bye
                    rts

DisplayTitle:       movem.l  d0-d7/a0-a6,-(sp)
                    jsr      SetTitleColor(pc)
                    move.l   pBlueBuff-DT(a4),a0
                    move.l   pBitPlane0-DT(a4),a1
                    move.l   pGreenBuff-DT(a4),a2
                    move.l   pBitPlane1-DT(a4),a3
                    move.l   pRedBuff-DT(a4),a5
                    move.l   pBitPlane2-DT(a4),a6
                    move.l   #$F9F,d0
lbC002130:          move.l   (a0)+,(a1)+
                    move.l   (a2)+,(a3)+
                    move.l   (a5)+,(a6)+
                    dbra     d0,lbC002130
                    lea      COPYRIGHT1987_MSG-DT(a4),a0
                    move.l   pBitPlane1-DT(a4),a2
                    move.l   pBitPlane2-DT(a4),a3
                    move.w   #$3C00,d1
                    add.w    d1,a2
                    add.w    d1,a3
                    bsr.s    lbC002152
                    bra.s    lbC002160

lbC002152:          moveq    #0,d0
                    move.b   (a0)+,d0
                    beq.s    lbC00215E
                    jsr      PrintAChar(pc)
                    bra.s    lbC002152

lbC00215E:          rts

lbC002160:          move.l   #$2698,d0
                    move.l   prgbSpPres-DT(a4),a0
                    jsr      Speak(pc)
                    moveq    #10,d0
                    bsr.s    DisplayLogo
                    movem.l  (sp)+,d0-d7/a0-a6
                    rts

DisplayLogo:        movem.l  d0-d7/a0-a6,-(sp)
                    mulu     #$50,d0
                    add.l    #$F05,d0
                    move.l   pBitPlane1-DT(a4),a5
                    add.w    d0,a5
                    move.l   pBitPlane2-DT(a4),a3
                    add.w    d0,a3
                    move.l   pBitPlane0-DT(a4),a6
                    add.w    d0,a6
                    move.l   #$D20,d0
                    move.l   lbL006334-DT(a4),a0
                    add.w    d0,a0
                    move.l   #$1A40,d0
                    move.l   lbL006334-DT(a4),a1
                    add.w    d0,a1
                    move.l   #$2760,d0
                    move.l   lbL006334-DT(a4),a2
                    add.w    d0,a2
                    moveq    #$2F,d3
lbC0021BE:          moveq    #$45,d2
lbC0021C0:          move.b   -(a2),d1
                    move.b   -(a0),d0
                    move.b   0(a5,d2.w),d4
                    eor.b    d4,d0
                    and.b    d1,d0
                    eor.b    d4,d0
                    move.b   d0,0(a5,d2.w)
                    moveq    #0,d0
                    move.b   0(a3,d2.w),d4
                    eor.b    d4,d0
                    and.b    d1,d0
                    eor.b    d4,d0
                    move.b   d0,0(a3,d2.w)
                    move.b   -(a1),d0
                    move.b   0(a6,d2.w),d4
                    eor.b    d4,d0
                    and.b    d1,d0
                    eor.b    d4,d0
                    move.b   d0,0(a6,d2.w)
                    dbra     d2,lbC0021C0
                    sub.w    #$50,a5
                    sub.w    #$50,a3
                    sub.w    #$50,a6
                    move.w   #5,d0
                    jsr      WaitSome(pc)
                    dbra     d3,lbC0021BE
                    movem.l  (sp)+,d0-d7/a0-a6
                    rts

DisplayCredits:     movem.l  d0-d7/a0-a6,-(sp)
                    jsr      ClearScreen(pc)
                    jsr      SetTitleColor(pc)
                    lea      PRESENTEDBYSI_MSG-DT(a4),a0
                    move.l   pBitPlane1-DT(a4),a2
                    move.w   #$1400,d1
                    add.w    d1,a2
                    move.l   a2,a3
                    jsr      lbC002304(pc)
                    lea      GAMEDESIGNHGO_MSG-DT(a4),a0
                    move.l   pBitPlane1-DT(a4),a2
                    move.w   #$1680,d1
                    add.w    d1,a2
                    move.l   a2,a3
                    jsr      lbC002304(pc)
                    lea      AMIGAVERSIONS_MSG-DT(a4),a0
                    move.l   pBitPlane0-DT(a4),a2
                    move.w   #$1B80,d1
                    add.w    d1,a2
                    move.l   a2,a3
                    jsr      lbC002304(pc)
                    lea      PROGRAMMEDBYL_MSG-DT(a4),a0
                    move.l   pBitPlane2-DT(a4),a2
                    move.w   #$2080,d1
                    add.w    d1,a2
                    move.l   a2,a3
                    jsr      lbC002304(pc)
                    lea      THANKSTOMICHA_MSG-DT(a4),a0
                    move.l   pBitPlane1-DT(a4),a2
                    move.w   #$2580,d1
                    add.w    d1,a2
                    move.l   a2,a3
                    jsr      lbC002304(pc)
                    lea      ROBERTCCLARDY_MSG-DT(a4),a0
                    move.l   pBitPlane1-DT(a4),a2
                    move.w   #$2800,d1
                    add.w    d1,a2
                    move.l   a2,a3
                    bsr.s    lbC002304
                    lea      JOHNPCONLEY_MSG-DT(a4),a0
                    move.l   pBitPlane1-DT(a4),a2
                    move.w   #$2A80,d1
                    add.w    d1,a2
                    move.l   a2,a3
                    bsr.s    lbC002304
                    lea      HAYESHAUGEN_MSG-DT(a4),a0
                    move.l   pBitPlane1-DT(a4),a2
                    move.w   #$2D00,d1
                    add.w    d1,a2
                    move.l   a2,a3
                    bsr.s    lbC002304
                    lea      GAMEMUSIC_MSG-DT(a4),a0
                    move.l   pBitPlane1-DT(a4),a2
                    move.w   #$3200,d1
                    add.w    d1,a2
                    move.l   a2,a3
                    bsr.s    lbC002304
                    lea      THEXDERHGODAI_MSG-DT(a4),a0
                    move.l   pBitPlane1-DT(a4),a2
                    move.w   #$3480,d1
                    add.w    d1,a2
                    move.l   a2,a3
                    bsr.s    lbC002304
                    lea      MOONLIGHTSONA_MSG-DT(a4),a0
                    move.l   pBitPlane1-DT(a4),a2
                    move.w   #$3700,d1
                    add.w    d1,a2
                    move.l   a2,a3
                    bsr.s    lbC002304
                    lea      BROKENBYBLACK_MSG-DT(a4),a0
                    move.l   pBitPlane1-DT(a4),a2
                    move.w   #$3C00,d1
                    add.w    d1,a2
                    move.l   a2,a3
                    bsr.s    lbC002304
                    bra.s    lbC002312

lbC002304:          moveq    #0,d0
                    move.b   (a0)+,d0
                    beq.s    lbC002310
                    jsr      PrintAChar(pc)
                    bra.s    lbC002304

lbC002310:          rts

lbC002312:          moveq    #10,d0
                    jsr      DisplayLogo(pc)
                    movem.l  (sp)+,d0-d7/a0-a6
                    rts

SetColor:           movem.l  d0-d7/a0-a6,-(sp)
                    lea      palette_game-DT(a4),a5
                    bra.s    lbC002330

SetTitleColor:      movem.l  d0-d7/a0-a6,-(sp)
                    lea      palette_title-DT(a4),a5
lbC002330:          move.l   pGfxBase-DT(a4),a6
                    moveq    #0,d4
lbC002336:          move.l   pscrGame-DT(a4),a0
                    lea      $2C(a0),a0
                    move.w   d4,d0
                    move.w   (a5),d1
                    move.w   2(a5),d2
                    move.w   4(a5),d3
                    jsr      _LVOSetRGB4(a6)
                    addq.w   #6,a5
                    addq.w   #1,d4
                    cmp.w    #16,d4
                    blt.s    lbC002336
                    movem.l  (sp)+,d0-d7/a0-a6
                    rts

NextLevel:          move.l   #9671,d0
                    move.l   prgbSpWarn-DT(a4),a0
                    jsr      Speak(pc)
                    move.l   prgbDispMap-DT(a4),a0
                    add.w    iMapOffSetCur-DT(a4),a0
                    move.w   (a0),charBlank-DT(a4)
                    move.w   #$10,d0
                    tst.w    fShieldsUsed-DT(a4)
                    bne.s    _AddEnergyMax
                    move.w   #48,d0
_AddEnergyMax:      jsr      AddEnergyMax(pc)
                    tst.w    fShieldsUsed-DT(a4)
                    bne.s    lbC0023C4
                    move.w   #256,d0
                    jsr      AddEnergy(pc)
lbC0023C4:          move.w   cLevel-DT(a4),d0
lbC0023C8:          addq.w   #1,d0
                    and.w    #15,d0
                    beq.s    lbC0023C8
                    move.w   d0,cLevel-DT(a4)
                    add.w    d0,d0
                    lea      lbW006386-DT(a4),a0
                    move.w   0(a0,d0.w),d5
                    move.l   prgbBuffer1-DT(a4),a0
                    add.w    #$2748,a0
                    move.l   d0,-(sp)
                    move.w   (a0),wOldLevel-DT(a4)
                    jsr      IncLevel(pc)
                    move.w   d0,(a0)
                    move.l   (sp)+,d0
                    lea      datatexmp01_MSG-DT(a4),a0
                    lea      ascii_MSG0-DT(a4),a1
                    move.w   (a1,d0.w),10(a0)
                    lea      datateki01_MSG-DT(a4),a0
                    lea      ascii_MSG1-DT(a4),a1
                    move.w   (a1,d0.w),9(a0)
                    clr.l    -(sp)
                    pea      datatexmp01_MSG-DT(a4)
                    move.l   #10,-(sp)
                    move.l   prgbBuffer1-DT(a4),a0
                    add.w    d5,a0
                    move.l   a0,-(sp)
                    move.l   #$2780,-(sp)
                    bsr.s    LoadFile
                    move.l   (sp)+,d0
                    bmi      Bye
                    clr.l    -(sp)
                    pea      datateki01_MSG-DT(a4)
                    move.l   #0,-(sp)
                    move.l   pbmTeki-DT(a4),-(sp)
                    move.l   #$2000,-(sp)
                    bsr.s    LoadFile
                    move.l   (sp)+,d0
                    bmi      Bye
                    rts

LoadFile:           link     a5,#-4
                    movem.l  d0-d7/a0-a6,-(sp)
                    lea      -4(a5),a3
                    move.l   pDosBase-DT(a4),a6
                    move.l   $14(a5),d1
                    move.l   #$3ED,d2
                    jsr      _LVOOpen(a6)
                    tst.l    d0
                    beq.s    lbC0024B2
                    move.l   d0,(a3)
                    move.l   d0,d1
                    move.l   $10(a5),d2
                    beq.s    lbC002488
                    moveq    #-1,d3
                    jsr      _LVOSeek(a6)
                    tst.l    d0
                    bmi.s    lbC0024C6
lbC002488:          move.l   (a3),d1
                    move.l   12(a5),d2
                    move.l   8(a5),d3
                    jsr      _LVORead(a6)
                    tst.l    d0
                    bmi.s    lbC0024BC
                    move.l   d0,$18(a5)
lbC00249E:          move.l   (a3),d1
                    jsr      _LVOClose(a6)
lbC0024A4:          movem.l  (sp)+,d0-d7/a0-a6
                    unlk     a5
                    move.l   (sp)+,a0
                    add.w    #$10,sp
                    jmp      (a0)

lbC0024B2:          move.l   #-1,$18(a5)
                    bra.s    lbC0024A4

lbC0024BC:          move.l   #-3,$18(a5)
                    bra.s    lbC00249E

lbC0024C6:          move.l   #-2,$18(a5)
                    bra.s    lbC00249E

WaitSome:           move.l   a0,-(sp)
                    lea      gdInput-DT(a4),a0
                    clr.w    8(a0)
                    neg.w    d0
                    move.w   d0,cVBL-DT(a4)
lbC0024E0:          move.w   #7,d0
                    btst     d0,$BFE001
                    bne.s    lbC0024F2
                    clr.w    fJoy-DT(a4)
                    bra.s    lbC002506

lbC0024F2:          move.w   8(a0),d0
                    beq.s    lbC002500
                    move.w   #-1,fJoy-DT(a4)
                    bra.s    lbC002506

lbC002500:          move.w   cVBL-DT(a4),d0
                    bne.s    lbC0024E0
lbC002506:          clr.w    8(a0)
                    tst.w    d0
                    move.l   (sp)+,a0
                    rts

Speak:              move.l   a1,-(sp)
                    move.l   #$DFF000,a1
                    move.l   a0,$D0(a1)
                    move.w   d0,$D4(a1)
                    move.w   #$DC,$D6(a1)
                    move.w   #$40,$D8(a1)
                    move.w   #$8208,$96(a1)
                    move.w   cVBL-DT(a4),d0
lbC002536:          cmp.w    cVBL-DT(a4),d0
                    beq.s    lbC002536
                    move.l   prgbaudNullWaveForm-DT(a4),a0
                    move.l   a0,$D0(a1)
                    move.w   #$20,$D4(a1)
                    move.l   (sp)+,a1
                    rts

LaserSound:         movem.l  d0-d2/a0/a1,-(sp)
                    move.l   #$DFF000,a1
                    tst.w    fQuit-DT(a4)
                    bne.s    lbC002586
                    tst.w    fireCur-DT(a4)
                    beq.s    lbC002586
                    tst.w    _fPause-DT(a4)
                    bne.s    lbC002586
                    move.l   prgbaudWaveForms-DT(a4),a0
                    add.l    #$1800,a0
                    move.w   #$4E2,d0
                    move.w   #$DC,d1
                    move.w   #$20,d2
                    tst.w    _fSound-DT(a4)
                    beq.s    lbC002596
lbC002586:          move.l   prgbaudNullWaveForm-DT(a4),a0
                    move.w   #$20,d0
                    move.w   #$DC,d1
                    move.w   #$20,d2
lbC002596:          move.l   a0,$D0(a1)
                    move.w   d0,$D4(a1)
                    move.w   d1,$D6(a1)
                    move.w   d2,$D8(a1)
                    move.w   #$8208,$96(a1)
                    movem.l  (sp)+,d0-d2/a0/a1
                    rts

ThexHitSound:       movem.l  d0/a0/a1,-(sp)
                    move.w   #3,fFlash-DT(a4)
                    move.l   #$DFF000,a1
                    move.w   #8,$96(a1)
                    move.w   #$400,$9A(a1)
                    move.w   #$C8,d0
lbC0025D2:          dbra     d0,lbC0025D2
                    tst.w    _fSound-DT(a4)
                    bne.s    lbC002618
                    move.l   prgbaudWaveForms-DT(a4),a0
                    add.l    #$6820,a0
                    move.l   a0,$D0(a1)
                    move.w   #$2C3,$D4(a1)
                    move.w   #$DC,$D6(a1)
                    move.w   #$40,$D8(a1)
                    move.w   #$8208,$96(a1)
                    move.w   #$C8,d0
lbC002606:          dbra     d0,lbC002606
                    move.l   prgbaudNullWaveForm-DT(a4),a0
                    move.l   a0,$D0(a1)
                    move.w   #$20,$D4(a1)
lbC002618:          movem.l  (sp)+,d0/a0/a1
                    rts

TekiHitSound:       movem.l  d0/a0/a1,-(sp)
                    move.l   #$DFF000,a1
                    move.w   #8,$96(a1)
                    move.w   #$400,$9A(a1)
                    move.w   #$C8,d0
lbC002638:          dbra     d0,lbC002638
                    tst.w    _fSound-DT(a4)
                    bne.s    lbC00267E
                    move.l   prgbaudWaveForms-DT(a4),a0
                    add.l    #$6620,a0
                    move.l   a0,$D0(a1)
                    move.w   #$100,$D4(a1)
                    move.w   #$DC,$D6(a1)
                    move.w   #$40,$D8(a1)
                    move.w   #$8208,$96(a1)
                    move.w   #$C8,d0
lbC00266C:          dbra     d0,lbC00266C
                    move.l   prgbaudNullWaveForm-DT(a4),a0
                    move.l   a0,$D0(a1)
                    move.w   #$20,$D4(a1)
lbC00267E:          movem.l  (sp)+,d0/a0/a1
                    rts

Bye:                move.l   _RegisterA4-DT(a4),a4
                    move.l   pIntuitionBase-DT(a4),a6
                    move.l   pscrGame-DT(a4),d0
                    beq.s    lbC002698
                    move.l   d0,a0
                    jsr      _LVOCloseScreen(a6)
lbC002698:          move.l   4.w,a6
                    move.l   prgbDispMap-DT(a4),d0
                    beq.s    lbC0026AE
                    move.l   d0,a1
                    move.l   #$B000,d0
                    jsr      _LVOFreeMem(a6)
lbC0026AE:          move.l   4.w,a6
                    move.l   prgbBuffer1-DT(a4),d0
                    beq.s    lbC0026C4
                    move.l   d0,a1
                    move.l   #$2780,d0
                    jsr      _LVOFreeMem(a6)
lbC0026C4:          move.l   pbmAll-DT(a4),d0
                    beq.s    lbC0026D6
                    move.l   d0,a1
                    move.l   #$73FA,d0
                    jsr      _LVOFreeMem(a6)
lbC0026D6:          move.l   lbL006334-DT(a4),d0
                    beq.s    lbC0026E8
                    move.l   d0,a1
                    move.l   #$2760,d0
                    jsr      _LVOFreeMem(a6)
lbC0026E8:          move.l   prgbMoonMusic-DT(a4),d0
                    beq.s    lbC0026FA
                    move.l   d0,a1
                    move.l   #$1680,d0
                    jsr      _LVOFreeMem(a6)
lbC0026FA:          move.l   prgbThemeMusic-DT(a4),d0
                    beq.s    lbC00270C
                    move.l   d0,a1
                    move.l   #$F00,d0
                    jsr      _LVOFreeMem(a6)
lbC00270C:          move.l   pRedBuff-DT(a4),d0
                    beq.s    lbC00271E
                    move.l   d0,a1
                    move.l   #$3E80,d0
                    jsr      _LVOFreeMem(a6)
lbC00271E:          move.l   pGreenBuff-DT(a4),d0
                    beq.s    lbC002730
                    move.l   d0,a1
                    move.l   #$3E80,d0
                    jsr      _LVOFreeMem(a6)
lbC002730:          move.l   pBlueBuff-DT(a4),d0
                    beq.s    lbC002742
                    move.l   d0,a1
                    move.l   #$3E80,d0
                    jsr      _LVOFreeMem(a6)
lbC002742:          move.l   prgbSpPres-DT(a4),d0
                    beq.s    lbC002754
                    move.l   d0,a1
                    move.l   #$4D30,d0
                    jsr      _LVOFreeMem(a6)
lbC002754:          move.l   prgbSpWarn-DT(a4),d0
                    beq.s    lbC002766
                    move.l   d0,a1
                    move.l   #$4B8F,d0
                    jsr      _LVOFreeMem(a6)
lbC002766:          move.l   prgbaudNullWaveForm-DT(a4),d0
                    beq.s    lbC002774
                    move.l   d0,a1
                    moveq    #$40,d0
                    jsr      _LVOFreeMem(a6)
lbC002774:          move.l   prgbaudWaveForms-DT(a4),d0
                    beq.s    lbC002786
                    move.l   d0,a1
                    move.l   #$78B4,d0
                    jsr      _LVOFreeMem(a6)
lbC002786:          moveq    #0,d0
                    rts

InitMan:            clr.w    frmThex-DT(a4)
                    clr.w    frmTThex-DT(a4)
                    clr.w    fState-DT(a4)
                    clr.l    lbL0065F0-DT(a4)
                    clr.w    fThex-DT(a4)
                    clr.w    lbW006600-DT(a4)
                    clr.w    lbW006602-DT(a4)
                    clr.w    lbW006608-DT(a4)
                    clr.l    lbL00660C-DT(a4)
                    clr.w    lbW006610-DT(a4)
                    clr.w    lbW006612-DT(a4)
                    move.w   #1,lbW006616-DT(a4)
                    clr.w    wQuad-DT(a4)
                    clr.w    lbW00661A-DT(a4)
                    clr.w    cJump-DT(a4)
                    clr.w    fDead-DT(a4)
                    clr.w    cFrame-DT(a4)
                    clr.w    fJumpLevel-DT(a4)
                    clr.w    fFlash-DT(a4)
                    clr.w    xJoyCur-DT(a4)
                    clr.w    yJoyCur-DT(a4)
                    move.w   #$11,d0
                    move.w   d0,lbW00661C-DT(a4)
                    move.w   d0,lbW00661E-DT(a4)
                    move.w   d0,fState-DT(a4)
                    rts

MoveMan:            jsr      RemoveThexFromMap(pc)
                    jsr      CkCollision(pc)
                    jsr      GetDir(pc)
                    jsr      DoUpDate(pc)
                    clr.l    d0
                    lea      lbB002988(pc),a0
                    move.w   fState-DT(a4),d0
                    move.b   0(a0,d0.w),d0
                    move.w   d0,wQuad-DT(a4)
                    beq.s    lbC00283A
                    move.w   d0,lbW006616-DT(a4)
lbC00283A:          move.w   xThex-DT(a4),d1
                    move.w   yThex-DT(a4),d2
                    clr.l    d0
                    move.w   d1,d3
                    move.w   d2,d0
                    lsl.w    #8,d0
                    lsl.w    #2,d0
                    add.w    d3,d3
                    add.w    d3,d0
                    move.w   d0,iMapThexOffSetCur-DT(a4)
                    move.l   prgbDispMap-DT(a4),a0
                    tst.w    wEnergy-DT(a4)
                    beq.s    lbC002884
                    moveq    #3,d7
                    cmp.w    #$18,frmThex-DT(a4)
                    blt.s    lbC00286A
                    moveq    #2,d7
lbC00286A:          move.w   #$FFFF,0(a0,d0.l)
                    move.w   #$FFFF,2(a0,d0.l)
                    move.w   #$FFFF,4(a0,d0.l)
                    add.w    #$400,d0
                    dbra     d7,lbC00286A
lbC002884:          cmp.w    #9,fState-DT(a4)
                    blt.s    _UpDateShields0
                    move.w   0(a0,d0.l),d7
                    tst.b    d7
                    bmi.s    _ThexHitSound
                    and.w    #15,d7
                    cmp.w    #1,d7
                    beq.s    _ThexHitSound
                    move.w   2(a0,d0.l),d7
                    tst.b    d7
                    bmi.s    _ThexHitSound
                    and.w    #15,d7
                    cmp.w    #1,d7
                    beq.s    _ThexHitSound
                    move.w   4(a0,d0.l),d7
                    tst.b    d7
                    bmi.s    _ThexHitSound
                    and.w    #15,d7
                    cmp.w    #1,d7
                    bne.s    _UpDateShields0
_ThexHitSound:      jsr      ThexHitSound(pc)
                    tst.w    fShields-DT(a4)
                    beq.s    lbC0028DC
                    subq.w   #2,fShields-DT(a4)
                    bcc.s    lbC0028DC
                    clr.w    fShields-DT(a4)
                    jsr      PrintShields(pc)
                    bra.s    _UpDateShields0

lbC0028DC:          move.w   #2,d0
                    jsr      SubEnergy(pc)
_UpDateShields0:    jsr      UpDateShields(pc)
                    move.w   xMapMax-DT(a4),d0
                    move.w   xThex-DT(a4),d3
                    sub.w    #$13,d3
                    bmi.s    lbC0028FE
                    cmp.w    d0,d3
                    blt.s    lbC002900
                    move.w   d0,d3
                    bra.s    lbC002900

lbC0028FE:          clr.w    d3
lbC002900:          move.w   #$16,d0
                    move.w   yThex-DT(a4),d4
                    sub.w    #11,d4
                    bmi.s    lbC002916
                    cmp.w    d0,d4
                    blt.s    lbC002918
                    move.w   d0,d4
                    bra.s    lbC002918

lbC002916:          clr.w    d4
lbC002918:          move.w   d3,xMap-DT(a4)
                    move.w   d4,yMap-DT(a4)
                    move.w   d1,d5
                    move.w   d2,d6
                    sub.w    d3,d5
                    sub.w    d4,d6
                    move.w   d5,xThexScrOffSet-DT(a4)
                    move.w   d6,yThexScrOffSet-DT(a4)
                    move.w   d4,d0
                    lsl.w    #8,d0
                    lsl.w    #2,d0
                    add.w    d3,d3
                    add.w    d3,d0
                    move.w   d0,iMapOffSetCur-DT(a4)
                    tst.w    fThex-DT(a4)
                    bmi.s    lbC002960
                    move.w   fState-DT(a4),d0
                    cmp.w    #12,d0
                    beq.s    lbC002962
                    cmp.w    #11,d0
                    beq.s    lbC002962
                    cmp.w    #$12,d0
                    beq.s    lbC002962
                    cmp.w    #$11,d0
                    beq.s    lbC002962
lbC002960:          rts

lbC002962:          move.w   frmThex-DT(a4),d0
                    move.w   lbW006600-DT(a4),d1
                    bpl.s    lbC00296E
                    neg.w    d1
lbC00296E:          cmp.w    #$10,d0
                    bge.s    lbC002986
                    add.w    d1,lbW006612-DT(a4)
                    move.w   lbW006612-DT(a4),d1
                    and.w    #7,d1
                    add.w    d1,d0
                    move.w   d0,frmThex-DT(a4)
lbC002986:          rts

lbB002988:          dc.b     0,0,2,2,2,0,1,1,1,1,2,1,2,1,2,1,2,1,2,1,2,0

RemoveThexFromMap:  tst.w    wEnergy-DT(a4)
                    beq.s    lbC0029D0
                    move.l   prgbDispMap-DT(a4),a0
                    clr.l    d0
                    move.w   iMapThexOffSetCur-DT(a4),d0
                    move.w   #3,d1
                    cmp.w    #$18,frmThex-DT(a4)
                    blt.s    lbC0029BC
                    moveq    #2,d1
lbC0029BC:          clr.w    0(a0,d0.l)
                    clr.w    2(a0,d0.l)
                    clr.w    4(a0,d0.l)
                    add.w    #$400,d0
                    dbra     d1,lbC0029BC
lbC0029D0:          rts

InsertThexInMap:    move.l   prgbDispMap-DT(a4),a0
                    moveq    #0,d0
                    move.w   iMapThexOffSetCur-DT(a4),d0
                    moveq    #2,d1
lbC0029DE:          move.w   #$FFFF,0(a0,d0.l)
                    move.w   #$FFFF,2(a0,d0.l)
                    move.w   #$FFFF,4(a0,d0.l)
                    add.w    #$400,d0
                    dbra     d1,lbC0029DE
                    move.w   0(a0,d0.l),d1
                    cmp.w    #$FFFF,d1
                    bne.s    lbC002A0E
                    clr.w    0(a0,d0.l)
                    clr.w    2(a0,d0.l)
                    clr.w    4(a0,d0.l)
lbC002A0E:          rts

DoUpDate:           tst.w    fThex-DT(a4)
                    bmi      DoTrans
                    lea      lbW002FCA(pc),a0
                    move.w   lbW006608-DT(a4),d0
                    add.w    d0,d0
                    move.w   0(a0,d0.w),d1
                    lea      lbB0063A6-DT(a4),a1
                    lea      lbW006570-DT(a4),a2
                    move.w   fState-DT(a4),d2
                    move.w   d2,lbW00661E-DT(a4)
                    move.l   a1,a0
                    move.w   d2,d0
                    mulu     #9,d0
                    add.w    d0,a0
                    move.b   0(a0,d1.w),d3
                    move.w   d3,lbW00661C-DT(a4)
                    move.w   #-1,lbW00661A-DT(a4)
LoopBack:           addq.w   #1,lbW00661A-DT(a4)
                    cmp.w    #3,lbW00661A-DT(a4)
                    beq.s    lbC002AC0
                    tst.b    d3
                    bmi      InitTrans
                    and.w    #$1F,d3
                    beq.s    lbC002A98
                    ext.w    d3
                    move.w   d3,fState-DT(a4)
                    jsr      CkIfCollision(pc)
                    tst.b    d3
                    bmi      InitTrans
                    and.w    #$7F,d3
                    beq.s    lbC002A98
                    cmp.w    fState-DT(a4),d3
                    bne.s    LoopBack
                    move.w   d3,d0
                    add.w    d0,d0
                    move.w   d0,d4
                    move.w   0(a2,d4.w),d0
                    move.w   d0,lbW006600-DT(a4)
                    move.w   $2A(a2,d4.w),d0
                    move.w   d0,lbW006602-DT(a4)
lbC002A98:          move.w   xThex-DT(a4),d0
                    add.w    lbW006600-DT(a4),d0
                    bmi.s    lbC002AAC
                    cmp.w    #$1FF,d0
                    bgt.s    lbC002AAC
                    move.w   d0,xThex-DT(a4)
lbC002AAC:          move.w   yThex-DT(a4),d0
                    add.w    lbW006602-DT(a4),d0
                    bmi.s    lbC002AC0
                    cmp.w    #$28,d0
                    bgt.s    lbC002AC0
                    move.w   d0,yThex-DT(a4)
lbC002AC0:          move.w   d3,d4
                    add.w    d4,d4
                    move.w   $54(a2,d4.w),d0
                    cmp.w    #9,fState-DT(a4)
                    bge.s    lbC002B2A
                    move.w   fState-DT(a4),d0
                    cmp.w    #3,lbW00661A-DT(a4)
                    bne.s    lbC002AF8
                    move.w   lbW00661E-DT(a4),d0
                    move.w   d0,fState-DT(a4)
                    cmp.w    #9,lbW00661C-DT(a4)
                    bgt.s    lbC002AF8
                    subq.w   #1,d0
                    bne.s    lbC002AF4
                    move.w   #8,d0
lbC002AF4:          move.w   d0,fState-DT(a4)
lbC002AF8:          lea      lbW006622-DT(a4),a0
                    move.b   0(a0,d0.w),d0
                    move.w   frmThex-DT(a4),d1
lbC002B04:          add.w    #$10,d0
                    cmp.w    d0,d1
                    beq.s    lbC002B2E
                    bcc.s    lbC002B04
                    sub.w    d1,d0
                    move.w   #1,d2
                    cmp.w    #8,d0
                    blt.s    lbC002B1E
                    move.w   #-1,d2
lbC002B1E:          add.w    d2,d1
                    and.w    #15,d1
                    or.w     #$20,d1
                    move.w   d1,d0
lbC002B2A:          move.w   d0,frmThex-DT(a4)
lbC002B2E:          rts

InitTrans:          and.w    #$FF,d3
                    clr.w    lbW006612-DT(a4)
                    cmp.w    #$8A,d3
                    ble      lbC002BDA
                    cmp.w    #$8D,d3
                    beq      lbC002BDA
                    cmp.w    #$8E,d3
                    beq      lbC002BDA
                    cmp.w    #$D1,d3
                    beq      lbC002BDA
                    cmp.w    #$D2,d3
                    beq.s    lbC002BDA
                    cmp.w    #$CC,d3
                    beq.s    lbC002BDA
                    cmp.w    #$CB,d3
                    beq.s    lbC002BDA
                    moveq    #14,d5
                    move.l   #$1C0000,d1
                    cmp.w    #$92,d3
                    beq.s    lbC002BA0
                    cmp.w    #$91,d3
                    beq.s    lbC002BA0
                    cmp.w    #$93,d3
                    beq.s    lbC002BA0
                    cmp.w    #$94,d3
                    beq.s    lbC002BA0
                    moveq    #$1E,d5
                    move.l   #$1C0100,d1
                    cmp.w    #$8C,d3
                    beq.s    lbC002BA0
                    moveq    #15,d5
                    move.l   #$1C4000,d1
lbC002BA0:          move.l   lbL0065F0-DT(a4),d0
                    and.l    d1,d0
                    beq.s    lbC002BDA
                    move.l   lbL0065F0-DT(a4),d0
                    and.l    d5,d0
                    bne.s    lbC002BB6
                    jsr      lbC002FDC(pc)
                    bra.s    lbC002BDA

lbC002BB6:          cmp.w    #$8C,d3
                    bne.s    lbC002BC4
                    move.w   #7,d3
                    bra      LoopBack

lbC002BC4:          cmp.w    #$8B,d3
                    bne.s    lbC002BD2
                    move.w   #3,d3
                    bra      LoopBack

lbC002BD2:          move.w   lbW00661E-DT(a4),d3
                    bra      LoopBack

lbC002BDA:          moveq    #0,d0
                    move.b   d3,d0
                    and.w    #$1F,d0
                    move.w   d0,fState-DT(a4)
                    move.w   #-1,fThex-DT(a4)
                    move.w   #0,lbW006610-DT(a4)
                    cmp.w    #$CB,d3
                    bne.s    lbC002BFE
                    move.w   #$15,d0
                    bra.s    lbC002C20

lbC002BFE:          cmp.w    #$CC,d3
                    bne.s    lbC002C0A
                    move.w   #$16,d0
                    bra.s    lbC002C20

lbC002C0A:          cmp.w    #$D1,d3
                    bne.s    lbC002C16
                    move.w   #$17,d0
                    bra.s    lbC002C20

lbC002C16:          cmp.w    #$D2,d3
                    bne.s    lbC002C20
                    move.w   #$18,d0
lbC002C20:          add.w    d0,d0
                    add.w    d0,d0
                    lea      lbL00650C-DT(a4),a0
                    move.l   0(a0,d0.w),d0
                    beq.s    NoTrans
                    move.l   d0,lbL00660C-DT(a4)
                    move.l   d0,a0
                    move.w   (a0),d0
                    move.w   d0,frmThex-DT(a4)
                    rts

DoTrans:            addq.w   #1,lbW006610-DT(a4)
                    move.w   lbW006610-DT(a4),d0
                    add.w    d0,d0
                    move.l   lbL00660C-DT(a4),a0
                    move.w   0(a0,d0.w),d0
                    bmi.s    NoTrans
                    move.w   d0,frmThex-DT(a4)
                    rts

NoTrans:            move.w   #0,fThex-DT(a4)
                    rts

lbW002C5E:          dc.w     6,$10,-1
lbW002C64:          dc.w     14,$11,-1
lbW002C6A:          dc.w     $10,6,-1
lbW002C70:          dc.w     $11,14,-1
lbW002C76:          dc.w     14,$13,$12,0,-1
lbW002C80:          dc.w     0,$12,$13,14,-1
lbW002C8A:          dc.w     0,$14,$15,$18,$19,$1A,$1B,$20,-1
lbW002C9C:          dc.w     14,$16,$17,$1C,$1D,$1E,$1F,$28,-1
lbW002CAE:          dc.w     $28,$1F,$1E,$1D,$1C,$17,$16,14,-1
lbW002CC0:          dc.w     $20,$1B,$1A,$19,$18,$15,$14,0,-1

CkIfCollision:      move.w   fState-DT(a4),d0
                    cmp.w    #9,d0
                    beq.s    lbC002CEE
                    cmp.w    #10,d0
                    beq.s    lbC002CEE
                    cmp.w    #13,d0
                    beq.s    lbC002CEE
                    cmp.w    #14,d0
                    bne.s    lbC002CFC
lbC002CEE:          addq.w   #1,cJump-DT(a4)
                    cmp.w    #17,cJump-DT(a4)
                    blt.s    lbC002D02
                    bra.s    lbC002D16

lbC002CFC:          move.w   #0,cJump-DT(a4)
lbC002D02:          lea      lbW006464-DT(a4),a0
                    move.w   d3,d0
                    add.w    d0,d0
                    add.w    d0,d0
                    move.l   0(a0,d0.w),d0
                    and.l    lbL0065F0-DT(a4),d0
                    beq.s    lbC002D2E
lbC002D16:          lea      lbL0064B8-DT(a4),a0
                    move.w   d3,d0
                    add.w    d0,d0
                    add.w    d0,d0
                    move.l   0(a0,d0.w),d0
                    beq.s    lbC002D46
                    move.l   d0,a0
                    move.l   lbL0065F0-DT(a4),d0
                    jmp      (a0)

lbC002D2E:          cmp.w    #$11,d3
                    beq.s    lbC002D16
                    cmp.w    #$12,d3
                    beq.s    lbC002D16
                    cmp.w    #11,d3
                    beq.s    lbC002D16
                    cmp.w    #12,d3
                    beq.s    lbC002D16
lbC002D46:          rts

P_UP_UP_Collisions: cmp.w    #1,lbW006616-DT(a4)
                    bne.s    lbC002D56
                    move.w   #$93,d3
                    bra.s    lbC002D5A

lbC002D56:          move.w   #$94,d3
lbC002D5A:          btst     #2,d0
                    bne.s    lbC002D6E
                    move.w   #2,d3
                    btst     #1,d0
                    bne.s    lbC002D6E
                    move.w   #8,d3
lbC002D6E:          rts

P_DN_DN_Collisions: move.w   d0,d5
                    and.l    #$1C0000,d0
                    beq.s    lbC002DA0
                    cmp.w    #1,lbW006616-DT(a4)
                    bne.s    lbC002D88
                    move.w   #$93,d3
                    bra.s    lbC002D8C

lbC002D88:          move.w   #$94,d3
lbC002D8C:          btst     #$13,d0
                    bne.s    lbC002DA0
                    move.w   #4,d3
                    btst     #$14,d0
                    bne.s    lbC002DA0
                    move.w   #6,d3
lbC002DA0:          rts

P_LF_LF_Collisions: move.w   #$93,d3
                    btst     #$10,d0
                    bne.s    lbC002DBC
                    move.w   #8,d3
                    btst     #15,d0
                    bne.s    lbC002DBA
                    move.w   #6,d3
lbC002DBA:          rts

lbC002DBC:          move.l   d0,d5
                    and.l    #$1C0000,d0
                    beq.s    lbC002DDA
                    move.l   d5,d0
                    and.l    #14,d0
                    bne.s    lbC002DD6
                    jsr      lbC002FDC(pc)
                    bra.s    lbC002DDA

lbC002DD6:          move.w   #3,d3
lbC002DDA:          rts

P_RT_RT_Collisions: move.w   #$94,d3
                    btst     #6,d0
                    bne.s    lbC002DF6
                    move.w   #2,d3
                    btst     #7,d0
                    bne.s    lbC002DF4
                    move.w   #4,d3
lbC002DF4:          rts

lbC002DF6:          move.l   d0,d5
                    and.l    #$1C0000,d0
                    beq.s    lbC002E14
                    move.l   d5,d0
                    and.l    #14,d0
                    bne.s    lbC002E10
                    jsr      lbC002FDC(pc)
                    bra.s    lbC002E14

lbC002E10:          move.w   #7,d3
lbC002E14:          rts

P_UL_UL_Collisions: move.l   d0,d5
                    move.w   #7,d3
                    and.l    #$38000,d0
                    beq.s    lbC002E36
                    move.w   #1,d3
                    move.l   d5,d0
                    and.l    #14,d0
                    beq.s    lbC002E36
                    move.w   #$93,d3
lbC002E36:          rts

P_UR_UR_Collisions: move.l   d0,d5
                    move.w   #3,d3
                    and.l    #$E0,d0
                    beq.s    lbC002E58
                    move.w   #1,d3
                    move.l   d5,d0
                    and.l    #14,d0
                    beq.s    lbC002E58
                    move.w   #$94,d3
lbC002E58:          rts

P_DL_DL_Collisions: move.l   d0,d5
                    move.w   #7,d3
                    and.l    #$38000,d0
                    beq.s    lbC002E7A
                    move.w   #5,d3
                    move.l   d5,d0
                    and.l    #$1C0000,d0
                    beq.s    lbC002E7A
                    move.w   #$93,d3
lbC002E7A:          rts

P_DR_DR_Collisions: move.l   d0,d5
                    move.w   #3,d3
                    and.l    #$E0,d0
                    beq.s    lbC002E9C
                    move.w   #5,d3
                    move.l   d5,d0
                    and.l    #$1C0000,d0
                    beq.s    lbC002E9C
                    move.w   #$94,d3
lbC002E9C:          rts

M_UP_LF_Collisions: cmp.w    #17,cJump-DT(a4)
                    bge.s    lbC002EAE
                    and.l    #14,d0
                    beq.s    lbC002EB2
lbC002EAE:          move.w   #$13,d3
lbC002EB2:          rts

M_UP_RT_Collisions: cmp.w    #17,cJump-DT(a4)
                    bge.s    lbC002EC4
                    and.l    #14,d0
                    beq.s    lbC002EC8
lbC002EC4:          move.w   #$14,d3
lbC002EC8:          rts

M_LF_LF_Collisions: move.l   d0,d5
                    and.l    #$1C00,d0
                    bne.s    lbC002EDA
                    move.w   #15,d3
                    rts

lbC002EDA:          move.l   d5,d0
                    and.l    #$3C000,d0
                    beq.s    lbC002EE8
                    move.w   #$11,d3
lbC002EE8:          rts

M_RT_RT_Collisions: move.l   d0,d5
                    and.l    #$1C00,d0
                    bne.s    lbC002EFA
                    move.w   #$10,d3
                    rts

lbC002EFA:          move.l   d5,d0
                    and.l    #$1E0,d0
                    beq.s    lbC002F08
                    move.w   #$12,d3
lbC002F08:          rts

M_UL_LF_Collisions: move.l   d0,d5
                    move.w   #15,d3
                    and.l    #14,d0
                    bne.s    lbC002F32
                    cmp.w    #17,cJump-DT(a4)
                    bge.s    lbC002F32
                    move.l   d5,d0
                    move.w   #9,d3
                    and.l    #$38001,d0
                    bne.s    lbC002F32
                    move.w   #13,d3
lbC002F32:          rts

M_UR_RT_Collisions: move.l   d0,d5
                    move.w   #$10,d3
                    and.l    #14,d0
                    bne.s    lbC002F5C
                    cmp.w    #17,cJump-DT(a4)
                    bge.s    lbC002F5C
                    move.l   d5,d0
                    move.w   #10,d3
                    and.l    #$F0,d0
                    bne.s    lbC002F5C
                    move.w   #14,d3
lbC002F5C:          rts

F_DL_LF_Collisions: move.l   d0,d5
                    move.w   #$CB,d3
                    and.l    #$1C00,d0
                    bne.s    lbC002F7E
                    move.l   d5,d0
                    move.w   #$13,d3
                    and.l    #$1E000,d0
                    bne.s    lbC002F7E
                    move.w   #$11,d3
lbC002F7E:          rts

F_DR_RT_Collisions: move.l   d0,d5
                    move.w   #$CC,d3
                    and.l    #$1C00,d0
                    bne.s    lbC002FA0
                    move.l   d5,d0
                    move.w   #$14,d3
                    and.l    #$3C0,d0
                    bne.s    lbC002FA0
                    move.w   #$12,d3
lbC002FA0:          rts

M_SD_LF_Collisions: and.l    #$1C00,d0
                    bne.s    lbC002FAE
                    move.w   #$13,d3
lbC002FAE:          rts

M_SD_RT_Collisions: and.l    #$1C00,d0
                    bne.s    lbC002FBC
                    move.w   #$14,d3
lbC002FBC:          rts

F_DN_LF_Collisons:  move.w   #$D1,d3
                    rts

F_DN_RT_Collisions: move.w   #$D2,d3
                    rts

lbW002FCA:          dc.w     8,0,5,3,7,1,6,2,4

lbC002FDC:          subq.w   #1,yThex-DT(a4)
                    sub.w    #$400,iMapThexOffSetCur-DT(a4)
                    bsr.s    CkCollision
                    move.w   lbL0065F0-DT(a4),d0
                    rts

CkCollision:        movem.l  d0-d3/a0,-(sp)
                    clr.l    lbL0065F0-DT(a4)
                    move.l   prgbDispMap-DT(a4),a0
                    clr.l    d0
                    move.w   iMapThexOffSetCur-DT(a4),d0
                    sub.w    #$400,d0
                    tst.w    -2(a0,d0.l)
                    beq.s    lbC003012
                    or.l     #1,lbL0065F0-DT(a4)
lbC003012:          tst.w    0(a0,d0.l)
                    beq.s    lbC003020
                    or.l     #2,lbL0065F0-DT(a4)
lbC003020:          tst.w    2(a0,d0.l)
                    beq.s    lbC00302E
                    or.l     #4,lbL0065F0-DT(a4)
lbC00302E:          tst.w    4(a0,d0.l)
                    beq.s    lbC00303C
                    or.l     #8,lbL0065F0-DT(a4)
lbC00303C:          tst.w    6(a0,d0.l)
                    beq.s    lbC00304A
                    or.l     #$10,lbL0065F0-DT(a4)
lbC00304A:          add.w    #$400,d0
                    tst.w    6(a0,d0.l)
                    beq.s    lbC00305C
                    or.l     #$20,lbL0065F0-DT(a4)
lbC00305C:          add.w    #$400,d0
                    tst.w    6(a0,d0.l)
                    beq.s    lbC00306E
                    or.l     #$40,lbL0065F0-DT(a4)
lbC00306E:          add.w    #$400,d0
                    tst.w    6(a0,d0.l)
                    beq.s    lbC003080
                    or.l     #$80,lbL0065F0-DT(a4)
lbC003080:          add.w    #$400,d0
                    tst.w    6(a0,d0.l)
                    beq.s    lbC003092
                    or.l     #$100,lbL0065F0-DT(a4)
lbC003092:          add.w    #$400,d0
                    tst.w    6(a0,d0.l)
                    beq.s    lbC0030A4
                    or.l     #$200,lbL0065F0-DT(a4)
lbC0030A4:          tst.w    4(a0,d0.l)
                    beq.s    lbC0030B2
                    or.l     #$400,lbL0065F0-DT(a4)
lbC0030B2:          tst.w    2(a0,d0.l)
                    beq.s    lbC0030C0
                    or.l     #$800,lbL0065F0-DT(a4)
lbC0030C0:          tst.w    0(a0,d0.l)
                    beq.s    lbC0030CE
                    or.l     #$1000,lbL0065F0-DT(a4)
lbC0030CE:          tst.w    -2(a0,d0.l)
                    beq.s    lbC0030DC
                    or.l     #$2000,lbL0065F0-DT(a4)
lbC0030DC:          sub.w    #$400,d0
                    tst.w    -2(a0,d0.l)
                    beq.s    lbC0030EE
                    or.l     #$4000,lbL0065F0-DT(a4)
lbC0030EE:          sub.w    #$400,d0
                    tst.w    -2(a0,d0.l)
                    beq.s    lbC003100
                    or.l     #$8000,lbL0065F0-DT(a4)
lbC003100:          sub.w    #$400,d0
                    tst.w    -2(a0,d0.l)
                    beq.s    lbC003112
                    or.l     #$10000,lbL0065F0-DT(a4)
lbC003112:          sub.w    #$400,d0
                    tst.w    -2(a0,d0.l)
                    beq.s    lbC003124
                    or.l     #$20000,lbL0065F0-DT(a4)
lbC003124:          add.w    #$C00,d0
                    tst.w    4(a0,d0.l)
                    beq.s    lbC003136
                    or.l     #$40000,lbL0065F0-DT(a4)
lbC003136:          tst.w    2(a0,d0.l)
                    beq.s    lbC003144
                    or.l     #$80000,lbL0065F0-DT(a4)
lbC003144:          tst.w    0(a0,d0.l)
                    beq.s    lbC003152
                    or.l     #$100000,lbL0065F0-DT(a4)
lbC003152:          movem.l  (sp)+,d0-d3/a0
                    rts

GetDir:             move.w   xJoyCur-DT(a4),d0
                    move.w   yJoyCur-DT(a4),d1
                    lea      lbL0031B8(pc),a0
                    add.w    d1,d1
                    add.w    d1,d1
                    move.l   0(a0,d1.w),a0
                    add.w    d0,d0
                    move.w   0(a0,d0.w),d0
                    move.w   d0,lbW006608-DT(a4)
                    rts

                    dc.l     lbW0031C2
lbL0031B8:          dc.l     lbW0031C8
                    dc.l     lbW0031CE
                    dc.w     8
lbW0031C2:          dc.w     1,2,7
lbW0031C8:          dc.w     0,3,6
lbW0031CE:          dc.w     5,4

InitTeki:           lea      rgobjTeki-DT(a4),a0
                    move.w   #$B09,d1
                    moveq    #0,d0
lbC0031DC:          move.w   d0,(a0)+
                    dbra     d1,lbC0031DC
                    rts

MoveTeki:           lea      rgobjTeki-DT(a4),a0
                    move.l   prgbDispMap-DT(a4),a1
                    move.l   prgbBuffer1-DT(a4),a2
                    add.w    #$40,a2
                    lea      lbL006744-DT(a4),a3
                    moveq    #0,d7
lbC0031FA:          move.w   (a0),d0
                    bmi.s    lbC00327A
                    beq.s    lbC00327C
                    move.w   6(a0),d0
                    cmp.w    #4,d0
                    beq.s    lbC003240
                    cmp.w    #6,d0
                    beq.s    lbC003240
                    cmp.w    #9,d0
                    beq.s    lbC003240
                    move.w   10(a0),d0
                    move.w   d0,d1
                    move.b   0(a2,d0.w),d2
                    btst     #7,d2
                    bne.s    lbC00322E
                    btst     #0,cFrame+1-DT(a4)
                    bne.s    lbC003240
lbC00322E:          and.w    #15,d2
                    addq.w   #1,d1
                    and.w    d2,d1
                    not.w    d2
                    and.w    d2,d0
                    or.w     d1,d0
                    move.w   d0,10(a0)
lbC003240:          move.w   xMap-DT(a4),d0
                    move.w   2(a0),d1
                    subq.w   #1,d0
                    cmp.w    d0,d1
                    blt.s    lbC003270
                    add.w    #$29,d0
                    cmp.w    d0,d1
                    bgt.s    lbC003270
                    move.w   6(a0),d0
                    add.w    d0,d0
                    add.w    d0,d0
                    move.l   0(a3,d0.w),d0
                    beq.s    lbC003270
                    move.l   d0,a5
                    bsr.s    RemoveTekiFromMap
                    jsr      (a5)
                    tst.w    (a0)
                    beq.s    lbC00327C
lbC00326E:          bsr.s    lbC0032D2
lbC003270:          add.w    #$100,d7
                    add.w    #$16,a0
                    bra.s    lbC0031FA

lbC00327A:          rts

lbC00327C:          cmp.w    #$FFFF,8(a0)
                    beq.s    lbC003270
                    tst.w    4(a0)
                    beq.s    lbC00328C
                    bsr.s    RemoveTekiFromMap
lbC00328C:          subq.w   #1,8(a0)
                    cmp.w    #-1,8(a0)
                    beq.s    lbC003270
                    bra.s    lbC00326E

lbC00329A:          moveq    #0,d0
                    move.w   2(a0),d1
                    move.w   4(a0),d0
                    lsl.w    #8,d0
                    lsl.w    #2,d0
                    add.w    d1,d1
                    add.w    d1,d0
                    move.l   d0,d1
                    rts

RemoveTekiFromMap:  bsr.s    lbC00329A
                    moveq    #0,d0
                    move.w   d0,0(a1,d1.l)
                    move.w   d0,2(a1,d1.l)
                    add.l    #$400,d1
                    move.w   d0,0(a1,d1.l)
                    move.w   d0,2(a1,d1.l)
                    sub.l    #$400,d1
                    rts

lbC0032D2:          bsr.s    lbC00329A
                    move.w   8(a0),d0
                    or.w     #$80,d0
                    or.w     d7,d0
                    move.w   d0,0(a1,d1.l)
                    or.w     #$40,d0
                    move.w   d0,2(a1,d1.l)
                    add.l    #$400,d1
                    or.w     #$FF,d0
                    move.w   d0,0(a1,d1.l)
                    move.w   d0,2(a1,d1.l)
                    rts

lbC0032FE:          move.w   #8,$10(a0)
                    bra      lbC003596

lbC003308:          move.w   14(a0),d0
                    move.w   12(a0),d2
                    addq.w   #1,d2
                    and.w    d0,d2
                    move.w   d2,12(a0)
                    bne.s    lbC003350
                    clr.l    d2
                    move.w   iMapThexOffSetCur-DT(a4),d2
                    move.w   2(a0),d3
                    sub.w    xMap-DT(a4),d3
                    cmp.w    #$14,d3
                    bge.s    lbC003340
                    move.w   #4,$10(a0)
                    cmp.l    d2,d1
                    bge.s    lbC003350
                    move.w   #6,$10(a0)
                    bra.s    lbC003350

lbC003340:          move.w   #2,$10(a0)
                    cmp.l    d2,d1
                    bge.s    lbC003350
                    move.w   #0,$10(a0)
lbC003350:          bra      lbC003596

lbC003354:          move.w   14(a0),d0
                    move.w   12(a0),d2
                    addq.w   #1,d2
                    and.w    d0,d2
                    move.w   d2,12(a0)
                    move.w   #7,$10(a0)
                    bra      lbC003596

lbC00336E:          move.w   10(a0),d0
                    move.b   0(a2,d0.w),d2
                    btst     #7,d2
                    beq.s    lbC003384
                    btst     #0,cFrame+1-DT(a4)
                    beq.s    lbC0033C4
lbC003384:          move.w   14(a0),d0
                    move.w   12(a0),d2
                    addq.w   #1,d2
                    and.w    d0,d2
                    move.w   d2,12(a0)
                    bne.s    lbC0033C4
                    move.w   2(a0),d3
                    sub.w    xMap-DT(a4),d3
                    cmp.w    #$12,d3
                    bge.s    lbC0033AC
                    move.w   #5,$10(a0)
                    bra.s    lbC0033C0

lbC0033AC:          cmp.w    #$16,d3
                    blt.s    lbC0033BA
                    move.w   #1,$10(a0)
                    bra.s    lbC0033C0

lbC0033BA:          move.w   #8,$10(a0)
lbC0033C0:          bra      lbC003596

lbC0033C4:          rts

lbC0033C6:          move.w   10(a0),d0
                    move.b   0(a2,d0.w),d2
                    btst     #7,d2
                    beq.s    lbC0033DE
                    btst     #0,cFrame+1-DT(a4)
                    beq      lbC003490
lbC0033DE:          move.w   14(a0),d0
                    move.w   12(a0),d2
                    addq.w   #1,d2
                    and.w    d0,d2
                    move.w   d2,12(a0)
                    bne      lbC0034F2
                    move.w   2(a0),d3
                    sub.w    xMap-DT(a4),d3
                    cmp.w    #$13,d3
                    bge.s    lbC003422
                    move.w   4(a0),d3
                    sub.w    yThex-DT(a4),d3
                    bpl.s    lbC003410
                    move.w   #6,d5
                    bra.s    lbC00345C

lbC003410:          cmp.w    #4,d3
                    bge.s    lbC00341C
                    move.w   #5,d5
                    bra.s    lbC00345C

lbC00341C:          move.w   #4,d5
                    bra.s    lbC00345C

lbC003422:          cmp.w    #$15,d3
                    blt.s    lbC00344A
                    move.w   4(a0),d3
                    sub.w    yThex-DT(a4),d3
                    bpl.s    lbC003438
                    move.w   #0,d5
                    bra.s    lbC00345C

lbC003438:          cmp.w    #4,d3
                    bge.s    lbC003444
                    move.w   #1,d5
                    bra.s    lbC00345C

lbC003444:          move.w   #2,d5
                    bra.s    lbC00345C

lbC00344A:          move.w   #7,d5
                    moveq    #0,d3
                    move.w   iMapThexOffSetCur-DT(a4),d3
                    cmp.l    d3,d1
                    blt.s    lbC00345C
                    move.w   #3,d5
lbC00345C:          move.w   #7,d4
                    move.w   $10(a0),d0
                    cmp.w    d0,d5
                    beq.s    lbC003470
                    move.w   d0,d5
                    addq.w   #1,d5
                    and.w    #7,d5
lbC003470:          move.w   d5,$10(a0)
                    move.w   8(a0),d0
                    or.w     d5,d0
                    move.w   d0,10(a0)
                    jsr      lbC003596(pc)
                    beq.s    lbC003490
                    subq.w   #1,d4
                    bmi.s    lbC003490
                    addq.w   #1,d5
                    and.w    #7,d5
                    bra.s    lbC003470

lbC003490:          rts

lbC003492:          clr.l    d2
                    clr.l    d3
                    jsr      lbC0036A6(pc)
                    beq.s    lbC0034F6
                    move.w   10(a0),d0
                    move.b   0(a2,d0.w),d2
                    btst     #7,d2
                    beq.s    lbC0034B2
                    btst     #0,cFrame+1-DT(a4)
                    beq.s    lbC0034F6
lbC0034B2:          move.w   14(a0),d0
                    move.w   12(a0),d2
                    addq.w   #1,d2
                    and.w    d0,d2
                    move.w   d2,12(a0)
                    bne.s    lbC0034F2
lbC0034C4:          move.w   2(a0),d3
                    sub.w    xMap-DT(a4),d3
                    cmp.w    #$12,d3
                    bge.s    lbC0034DC
                    move.w   #5,$10(a0)
                    bra      lbC003596

lbC0034DC:          cmp.w    #$16,d3
                    blt.s    lbC0034EC
                    move.w   #1,$10(a0)
                    bra      lbC0033C0

lbC0034EC:          move.w   #8,$10(a0)
lbC0034F2:          bra      lbC003596

lbC0034F6:          rts

lbC0034F8:          move.w   14(a0),d0
                    move.w   12(a0),d2
                    addq.w   #1,d2
                    and.w    d0,d2
                    move.w   d2,12(a0)
                    bne.s    lbC003530
                    move.w   10(a0),d0
                    cmp.w    8(a0),d0
                    beq.s    lbC003530
                    move.w   d0,d1
                    move.b   0(a2,d0.w),d2
                    and.w    #15,d2
                    addq.w   #1,d1
                    and.w    d2,d1
                    bne.s    lbC003526
                    addq.w   #1,d1
lbC003526:          not.w    d2
                    and.w    d2,d0
                    or.w     d1,d0
                    move.w   d0,10(a0)
lbC003530:          rts

MovePattern7:       move.w   14(a0),d0
                    move.w   12(a0),d2
                    addq.w   #1,d2
                    and.w    d0,d2
                    move.w   d2,12(a0)
                    bne.s    lbC0034F2
                    move.w   8(a0),d0
                    cmp.w    10(a0),d0
                    beq.s    lbC0034F2
                    bra      lbC0034C4

lbC003554:          clr.l    d2
                    clr.l    d3
                    jsr      lbC0036A6(pc)
                    beq.s    lbC003594
                    jsr      Random(pc)
                    and.w    #1,d0
                    bne.s    lbC00357E
                    clr.l    d2
                    clr.l    d3
                    jsr      lbC003716(pc)
                    beq.s    lbC003594
                    jsr      lbC00329A(pc)
                    clr.l    d2
                    clr.l    d3
                    jmp      lbC003640(pc)

lbC00357E:          clr.l    d2
                    clr.l    d3
                    jsr      lbC003640(pc)
                    beq.s    lbC003594
                    jsr      lbC00329A(pc)
                    clr.l    d2
                    clr.l    d3
                    jmp      lbC003716(pc)

lbC003594:          rts

lbC003596:          jsr      lbC00329A(pc)
                    move.w   $10(a0),d0
                    moveq    #0,d2
                    moveq    #0,d3
                    cmp.w    #3,d0
                    beq.s    lbC0035E0
                    cmp.w    #4,d0
                    beq.s    lbC003606
                    cmp.w    #5,d0
                    beq      lbC003640
                    cmp.w    #6,d0
                    beq      lbC003666
                    cmp.w    #7,d0
                    beq      lbC0036A6
                    cmp.w    #0,d0
                    beq      lbC0036E2
                    cmp.w    #1,d0
                    beq      lbC003716
                    cmp.w    #2,d0
                    beq      lbC003734
                    rts

lbC0035E0:          sub.l    #$400,d1
                    move.w   0(a1,d1.l),d0
                    jsr      CkHitThex(pc)
                    move.w   2(a1,d1.l),d0
                    jsr      CkHitThex(pc)
                    tst.w    d2
                    bne.s    lbC003602
                    subq.w   #1,4(a0)
                    bra      lbC00375E

lbC003602:          bra      lbC003776

lbC003606:          move.w   2(a1,d1.l),d0
                    jsr      CkHitThex(pc)
                    move.w   4(a1,d1.l),d0
                    jsr      CkHitThex(pc)
                    sub.l    #$400,d1
                    move.w   2(a1,d1.l),d0
                    jsr      CkHitThex(pc)
                    move.w   4(a1,d1.l),d0
                    jsr      CkHitThex(pc)
                    tst.w    d2
                    bne.s    lbC00363C
                    subq.w   #1,4(a0)
                    addq.w   #1,2(a0)
                    bra      lbC00375E

lbC00363C:          bra      lbC003776

lbC003640:          move.w   4(a1,d1.l),d0
                    jsr      CkHitThex(pc)
                    add.l    #$400,d1
                    move.w   4(a1,d1.l),d0
                    jsr      CkHitThex(pc)
                    tst.w    d2
                    bne.s    lbC003662
                    addq.w   #1,2(a0)
                    bra      lbC00375E

lbC003662:          bra      lbC003776

lbC003666:          add.l    #$400,d1
                    move.w   2(a1,d1.l),d0
                    jsr      CkHitThex(pc)
                    move.w   4(a1,d1.l),d0
                    jsr      CkHitThex(pc)
                    add.l    #$400,d1
                    move.w   2(a1,d1.l),d0
                    jsr      CkHitThex(pc)
                    move.w   4(a1,d1.l),d0
                    jsr      CkHitThex(pc)
                    tst.w    d2
                    bne.s    lbC0036A2
                    addq.w   #1,4(a0)
                    addq.w   #1,2(a0)
                    bra      lbC00375E

lbC0036A2:          bra      lbC003776

lbC0036A6:          add.l    #$400,d1
                    move.w   0(a1,d1.l),d0
                    jsr      CkHitThex(pc)
                    move.w   2(a1,d1.l),d0
                    jsr      CkHitThex(pc)
                    add.l    #$400,d1
                    move.w   0(a1,d1.l),d0
                    jsr      CkHitThex(pc)
                    move.w   2(a1,d1.l),d0
                    jsr      CkHitThex(pc)
                    tst.w    d2
                    bne.s    lbC0036DE
                    addq.w   #1,4(a0)
                    bra      lbC00375E

lbC0036DE:          bra      lbC003776

lbC0036E2:          add.l    #$400,d1
                    move.w   -2(a1,d1.l),d0
                    bsr.s    CkHitThex
                    move.w   0(a1,d1.l),d0
                    bsr.s    CkHitThex
                    add.l    #$400,d1
                    move.w   -2(a1,d1.l),d0
                    bsr.s    CkHitThex
                    move.w   0(a1,d1.l),d0
                    bsr.s    CkHitThex
                    tst.w    d2
                    bne.s    lbC003714
                    addq.w   #1,4(a0)
                    subq.w   #1,2(a0)
                    bra.s    lbC00375E

lbC003714:          bra.s    lbC003776

lbC003716:          move.w   -2(a1,d1.l),d0
                    bsr.s    CkHitThex
                    add.l    #$400,d1
                    move.w   -2(a1,d1.l),d0
                    bsr.s    CkHitThex
                    tst.w    d2
                    bne.s    lbC003732
                    subq.w   #1,2(a0)
                    bra.s    lbC00375E

lbC003732:          bra.s    lbC003776

lbC003734:          move.w   0(a1,d1.l),d0
                    bsr.s    CkHitThex
                    move.w   -2(a1,d1.l),d0
                    bsr.s    CkHitThex
                    sub.l    #$400,d1
                    move.w   0(a1,d1.l),d0
                    bsr.s    CkHitThex
                    move.w   -2(a1,d1.l),d0
                    bsr.s    CkHitThex
                    tst.w    d2
                    bne.s    lbC003776
                    subq.w   #1,2(a0)
                    subq.w   #1,4(a0)
lbC00375E:          jsr      lbC00329A(pc)
                    moveq    #0,d0
                    rts

CkHitThex:          tst.w    d0
                    beq.s    lbC003774
                    addq.w   #1,d2
                    cmp.w    #-1,d0
                    bne.s    lbC003774
                    addq.w   #1,d3
lbC003774:          rts

lbC003776:          tst.w    d3
                    beq      lbC00380C
                    jsr      ThexHitSound(pc)
                    tst.w    fShields-DT(a4)
                    beq.s    lbC003804
                    btst     #6,$12(a0)
                    bne.s    lbC0037CC
                    btst     #7,$12(a0)
                    bne.s    lbC0037CC
                    bset     #6,$12(a0)
                    move.b   #3,$13(a0)
                    move.w   2(a0),d0
                    sub.w    xMap-DT(a4),d0
                    cmp.w    xThex-DT(a4),d0
                    blt.s    lbC0037B2
                    addq.w   #1,d0
lbC0037B2:          move.b   d0,$14(a0)
                    move.w   4(a0),d0
                    sub.w    yMap-DT(a4),d0
                    lsl.w    #3,d0
                    cmp.w    yThex-DT(a4),d0
                    blt.s    lbC0037C8
                    addq.w   #1,d0
lbC0037C8:          move.b   d0,$15(a0)
lbC0037CC:          subq.w   #1,(a0)
                    beq.s    lbC0037D2
                    bpl.s    lbC0037E2
lbC0037D2:          clr.w    (a0)
                    move.w   #3,8(a0)
                    move.w   6(a0),d0
                    jsr      AddScore(pc)
lbC0037E2:          addq.w   #1,lbB00662E-DT(a4)
                    move.w   #2,d0
                    btst     #0,lbB00662F-DT(a4)
                    beq.s    lbC0037F6
                    move.w   #1,d0
lbC0037F6:          sub.w    d0,fShields-DT(a4)
                    bcc.s    lbC00380C
                    clr.w    fShields-DT(a4)
                    jsr      PrintShields(pc)
lbC003804:          move.w   #2,d0
                    jsr      SubEnergy(pc)
lbC00380C:          jsr      lbC00329A(pc)
                    moveq    #1,d0
                    rts

DrawTeki:           movem.l  d1-d4/a5/a6,-(sp)
                    move.l   d0,d4
                    move.l   pbmTeki-DT(a4),a5
                    lea      rgobjTeki-DT(a4),a6
                    moveq    #0,d3
                    cmp.b    #$FF,d0
                    bne.s    lbC003834
                    move.w   #2,d3
                    moveq    #0,d0
                    move.w   -$402(a0),d0
lbC003834:          btst     #6,d0
                    beq.s    lbC00383C
                    addq.w   #1,d3
lbC00383C:          move.w   d4,d1
                    move.w   d4,d2
                    lsr.w    #8,d2
                    mulu     #$16,d2
                    tst.w    0(a6,d2.w)
                    beq.s    lbC00387C
                    move.w   10(a6,d2.w),d0
                    lea      lbL00386C(pc),a6
                    add.w    d3,d3
                    add.w    d3,d3
                    move.l   0(a6,d3.w),a6
                    and.l    #$3F,d0
                    lsl.l    #7,d0
                    tst.w    d0
                    bmi.s    lbC00386A
                    add.w    d0,a5
lbC00386A:          jmp      (a6)

lbL00386C:          dc.l     lbC00390E
                    dc.l     lbC0038AC
                    dc.l     lbC0039CA
                    dc.l     lbC00396A

lbC00387C:          move.w   8(a6,d2.w),d0
                    subq.w   #1,d0
                    lea      lbL006634-DT(a4),a6
                    add.w    d0,d0
                    add.w    d0,d0
                    move.l   0(a6,d0.w),a5
                    lea      lbL00389C(pc),a6
                    add.w    d3,d3
                    add.w    d3,d3
                    move.l   0(a6,d3.w),a6
                    jmp      (a6)

lbL00389C:          dc.l     lbC00390E
                    dc.l     lbC0038AC
                    dc.l     lbC0039CA
                    dc.l     lbC00396A

lbC0038AC:          move.w   2(a5),(a2)+
                    move.w   6(a5),(a3)+
                    move.w   10(a5),$4E(a2)
                    move.w   14(a5),$4E(a3)
                    move.w   18(a5),$9E(a2)
                    move.w   22(a5),$9E(a3)
                    move.w   26(a5),$EE(a2)
                    move.w   30(a5),$EE(a3)
                    move.w   34(a5),$13E(a2)
                    move.w   38(a5),$13E(a3)
                    move.w   42(a5),$18E(a2)
                    move.w   46(a5),$18E(a3)
                    move.w   50(a5),$1DE(a2)
                    move.w   54(a5),$1DE(a3)
                    move.w   58(a5),$22E(a2)
                    move.w   62(a5),$22E(a3)
lbC003908:          movem.l  (sp)+,d1-d4/a5/a6
                    rts

lbC00390E:          move.w   (a5),(a2)+
                    move.w   4(a5),(a3)+
                    move.w   8(a5),$4E(a2)
                    move.w   12(a5),$4E(a3)
                    move.w   16(a5),$9E(a2)
                    move.w   20(a5),$9E(a3)
                    move.w   24(a5),$EE(a2)
                    move.w   28(a5),$EE(a3)
                    move.w   32(a5),$13E(a2)
                    move.w   36(a5),$13E(a3)
                    move.w   40(a5),$18E(a2)
                    move.w   44(a5),$18E(a3)
                    move.w   48(a5),$1DE(a2)
                    move.w   52(a5),$1DE(a3)
                    move.w   56(a5),$22E(a2)
                    move.w   60(a5),$22E(a3)
                    bra.s    lbC003908

lbC00396A:          move.w   66(a5),(a2)+
                    move.w   70(a5),(a3)+
                    move.w   74(a5),$4E(a2)
                    move.w   78(a5),$4E(a3)
                    move.w   82(a5),$9E(a2)
                    move.w   86(a5),$9E(a3)
                    move.w   90(a5),$EE(a2)
                    move.w   94(a5),$EE(a3)
                    move.w   98(a5),$13E(a2)
                    move.w   102(a5),$13E(a3)
                    move.w   106(a5),$18E(a2)
                    move.w   110(a5),$18E(a3)
                    move.w   114(a5),$1DE(a2)
                    move.w   118(a5),$1DE(a3)
                    move.w   122(a5),$22E(a2)
                    move.w   126(a5),$22E(a3)
                    bra      lbC003908

lbC0039CA:          move.w   64(a5),(a2)+
                    move.w   68(a5),(a3)+
                    move.w   72(a5),$4E(a2)
                    move.w   76(a5),$4E(a3)
                    move.w   80(a5),$9E(a2)
                    move.w   84(a5),$9E(a3)
                    move.w   88(a5),$EE(a2)
                    move.w   92(a5),$EE(a3)
                    move.w   96(a5),$13E(a2)
                    move.w   100(a5),$13E(a3)
                    move.w   104(a5),$18E(a2)
                    move.w   108(a5),$18E(a3)
                    move.w   112(a5),$1DE(a2)
                    move.w   116(a5),$1DE(a3)
                    move.w   120(a5),$22E(a2)
                    move.w   124(a5),$22E(a3)
                    bra      lbC003908

KillThex:           tst.w    fDead-DT(a4)
                    bne.s    lbC003A8C
                    move.w   #-1,fDead-DT(a4)
                    lea      rgobjTeki-DT(a4),a0
                    move.l   pBitPlane2-DT(a4),a1
                    moveq    #8,d7
lbC003A40:          subq.w   #1,d7
                    bmi.s    lbC003A8C
                    bclr     #7,$12(a0)
                    bset     #6,$12(a0)
                    jsr      Random(pc)
                    and.l    #3,d0
                    bne.s    lbC003A60
                    move.b   #3,d0
lbC003A60:          move.b   d0,$13(a0)
                    jsr      Random(pc)
                    and.w    #3,d0
                    add.w    xThexScrOffSet-DT(a4),d0
                    move.b   d0,$14(a0)
                    jsr      Random(pc)
                    and.w    #3,d0
                    add.w    yThexScrOffSet-DT(a4),d0
                    lsl.w    #3,d0
                    move.b   d0,$15(a0)
                    add.w    #$16,a0
                    bra.s    lbC003A40

lbC003A8C:          rts

DrawBomb:           lea      rgobjTeki-DT(a4),a0
                    move.l   pBitPlane2-DT(a4),a1
lbC003A96:          lea      lbL003B08(pc),a2
                    tst.w    (a0)
                    bmi.s    lbC003B02
                    move.w   $12(a0),d0
                    btst     #14,d0
                    beq.s    lbC003AFC
                    tst.b    d0
                    beq.s    lbC003AB0
                    subq.b   #1,d0
                    bpl.s    lbC003AB8
lbC003AB0:          bclr     #14,d0
                    bset     #15,d0
lbC003AB8:          move.w   d0,$12(a0)
                    ext.w    d0
                    add.w    d0,d0
                    add.w    d0,d0
                    move.l   0(a2,d0.w),a2
                    moveq    #0,d0
                    move.l   d0,d1
                    move.b   $14(a0),d0
                    move.b   $15(a0),d1
                    mulu     #$50,d1
                    add.w    d0,d0
                    add.w    d1,d0
                    move.l   a1,a3
                    add.w    d0,a3
                    move.w   (a2)+,(a3)+
                    move.w   (a2)+,$4E(a3)
                    move.w   (a2)+,$9E(a3)
                    move.w   (a2)+,$EE(a3)
                    move.w   (a2)+,$13E(a3)
                    move.w   (a2)+,$18E(a3)
                    move.w   (a2)+,$1DE(a3)
                    move.w   (a2)+,$22E(a3)
lbC003AFC:          add.w    #$16,a0
                    bra.s    lbC003A96

lbC003B02:          rts

                    dc.l     lbL003B96
lbL003B08:          dc.l     lbW003B14
                    dc.l     lbW003B24
                    dc.l     lbW003B14

lbW003B14:          dc.w     0,0,$3C0,$FF0,$FF0,$3C0,0,0
lbW003B24:          dc.w     0,$3FFC,$FFFF,$FFFF,$FFFF,$FFFF,$3FFC,0

EraseBomb:          lea      rgobjTeki-DT(a4),a0
                    move.l   pBitPlane2-DT(a4),a1
lbC003B3C:          tst.w    (a0)
                    bmi.s    lbC003B94
                    move.w   $12(a0),d0
                    btst     #15,d0
                    beq.s    lbC003B8E
                    bclr     #15,d0
                    move.w   d0,$12(a0)
                    lea      lbL003B96(pc),a2
                    tst.w    (a0)
                    moveq    #0,d0
                    move.l   d0,d1
                    move.b   $14(a0),d0
                    move.b   $15(a0),d1
                    mulu     #$50,d1
                    add.w    d0,d0
                    add.w    d1,d0
                    move.l   a1,a3
                    add.w    d0,a3
                    move.w   (a2)+,(a3)+
                    move.w   (a2)+,$4E(a3)
                    move.w   (a2)+,$9E(a3)
                    move.w   (a2)+,$EE(a3)
                    move.w   (a2)+,$13E(a3)
                    move.w   (a2)+,$18E(a3)
                    move.w   (a2)+,$1DE(a3)
                    move.w   (a2)+,$22E(a3)
lbC003B8E:          add.w    #$16,a0
                    bra.s    lbC003B3C

lbC003B94:          rts

lbL003B96:          dcb.l    4,0

InitVBL:            move.l   4.w,a6
                    moveq    #5,d0
                    lea      VertBIntr-DT(a4),a1
                    jsr      _LVOAddIntServer(a6)
                    rts

RemoveVBL:          move.l   4.w,a6
                    moveq    #5,d0
                    lea      VertBIntr-DT(a4),a1
                    jsr      _LVORemIntServer(a6)
                    rts

VBLServer:          exg      a4,a1
                    move.l   (a4),a4
                    addq.w   #1,cVBL-DT(a4)
                    tst.w    _fMusicDone-DT(a4)
                    beq.s    lbC003BE8
                    movem.l  a1/a6,-(sp)
                    move.l   _soundInt-DT(a4),a1
                    move.l   4.w,a6
                    jsr      _LVOCause(a6)
                    movem.l  (sp)+,a1/a6
lbC003BE8:          exg      a1,a4
                    moveq    #0,d0
                    rts

InitInputHandler:   move.l   4.w,a6
                    move.l   #0,a1
                    jsr      _LVOFindTask(a6)
                    lea      gdInput-DT(a4),a3
                    move.l   d0,(a3)
                    moveq    #-1,d0
                    jsr      _LVOAllocSignal(a6)
                    lea      gdInput-DT(a4),a3
                    moveq    #1,d1
                    lsl.l    d0,d1
                    move.l   d1,4(a3)
                    move.w   #0,8(a3)
                    lea      MsgPort-DT(a4),a0
                    clr.l    10(a0)
                    clr.b    9(a0)
                    move.w   #4,8(a0)
                    move.b   #0,14(a0)
                    move.b   d0,15(a0)
                    move.l   (a3),$10(a0)
                    lea      $14(a0),a0
                    move.l   a0,(a0)
                    addq.l   #4,(a0)
                    clr.l    4(a0)
                    move.l   a0,8(a0)
                    lea      iorInput-DT(a4),a1
                    move.w   #5,8(a1)
                    clr.b    9(a1)
                    lea      MsgPort-DT(a4),a0
                    move.l   a0,14(a1)
                    moveq    #0,d0
                    moveq    #0,d1
                    lea      szInput-DT(a4),a0
                    jsr      _LVOOpenDevice(a6)
                    lea      inthInput-DT(a4),a3
                    lea      gdInput-DT(a4),a0
                    move.l   a0,14(a3)
                    lea      InputHandler(pc),a0
                    move.l   a0,18(a3)
                    move.b   #$33,9(a3)
                    lea      iorInput-DT(a4),a1
                    move.w   #9,28(a1)
                    move.l   a3,40(a1)
                    jsr      _LVODoIO(a6)
                    rts

RemoveInputHandler: move.l   4.w,a6
                    lea      inthInput-DT(a4),a0
                    tst.l    $12(a0)
                    beq.s    lbC003CC2
                    lea      iorInput-DT(a4),a1
                    move.w   #10,28(a1)
                    move.l   a0,40(a1)
                    jsr      _LVODoIO(a6)
                    lea      iorInput-DT(a4),a1
                    jsr      _LVOCloseDevice(a6)
lbC003CC2:          rts

InputHandler:       cmp.b    #1,4(a0)
                    bne.s    lbC003D4A
                    btst     #1,8(a0)
                    bne.s    lbC003D42
                    moveq    #0,d2
                    move.w   6(a0),d0
                    tst.b    d0
                    bmi.s    lbC003CE0
                    moveq    #1,d2
lbC003CE0:          and.w    #$7F,d0
                    moveq    #0,d1
                    cmp.w    #$3E,d0
                    beq.s    lbC003D4E
                    addq.w   #2,d1
                    cmp.w    #$3F,d0
                    beq.s    lbC003D4E
                    addq.w   #2,d1
                    cmp.w    #$2F,d0
                    beq.s    lbC003D4E
                    addq.w   #2,d1
                    cmp.w    #$1F,d0
                    beq.s    lbC003D4E
                    addq.w   #2,d1
                    cmp.w    #$1E,d0
                    beq.s    lbC003D4E
                    addq.w   #2,d1
                    cmp.w    #$1D,d0
                    beq.s    lbC003D4E
                    addq.w   #2,d1
                    cmp.w    #$2D,d0
                    beq.s    lbC003D4E
                    addq.w   #2,d1
                    cmp.w    #$3D,d0
                    beq.s    lbC003D4E
                    cmp.w    #$60,d0
                    beq.s    lbC003D5E
                    cmp.w    #$66,d0
                    beq.s    lbC003D58
                    move.w   6(a0),8(a1)
                    move.w   8(a0),d0
                    and.w    #$7FFF,d0
                    move.w   d0,10(a1)
lbC003D42:          moveq    #0,d0
                    tst.w    12(a1)
                    beq.s    lbC003D4C
lbC003D4A:          move.l   a0,d0
lbC003D4C:          rts

lbC003D4E:          move.l   14(a1),a2
                    move.w   d2,(a2,d1.w)
                    bra.s    lbC003D42

lbC003D58:          move.w   d2,$12(a1)
                    bra.s    lbC003D42

lbC003D5E:          move.w   d2,$14(a1)
                    bra.s    lbC003D42

DrawBeam:           tst.w    fThex-DT(a4)
                    bmi.s    lbC003D74
                    jsr      LaserSound(pc)
                    tst.w    fireCur-DT(a4)
                    bne.s    lbC003D76
lbC003D74:          rts

lbC003D76:          move.w   #-1,lbW0094AC-DT(a4)
                    move.w   #-1,wColor-DT(a4)
                    jsr      Beam(pc)
                    tst.l    d0
                    beq      lbC003EEA
                    divu     #640,d0
                    move.w   d0,d1
                    move.w   #$10,d2
                    lsr.l    d2,d0
                    divu     #$50,d0
                    lsr.l    d2,d0
                    lsr.l    #1,d0
                    move.b   d0,lbB0094AA-DT(a4)
                    move.b   d1,lbB0094AB-DT(a4)
                    add.w    xMap-DT(a4),d0
                    add.w    yMap-DT(a4),d1
                    mulu     #1024,d1
                    add.w    d0,d0
                    add.w    d1,d0
                    move.l   prgbDispMap-DT(a4),a0
lbC003DBC:          move.w   0(a0,d0.l),d1
                    cmp.w    #-1,d1
                    beq      lbC003EEA
                    cmp.b    #-1,d1
                    bne.s    lbC003DD4
                    sub.w    #1024,d0
                    bra.s    lbC003DBC

lbC003DD4:          tst.b    d1
                    bpl      lbC003EEC
                    lsr.w    #8,d1
                    mulu     #22,d1
                    lea      rgobjTeki-DT(a4),a0
                    add.w    d1,a0
                    tst.w    (a0)
                    beq      lbC003EEA
                    move.w   cLevel-DT(a4),d0
                    and.w    #15,d0
                    cmp.w    #1,d0
                    bgt.s    lbC003E04
                    cmp.w    #$34,8(a0)
                    beq      lbC003EEA
lbC003E04:          cmp.w    #6,6(a0)
                    beq.s    lbC003E14
                    cmp.w    #7,6(a0)
                    bne.s    lbC003E26
lbC003E14:          move.w   10(a0),d0
                    cmp.w    8(a0),d0
                    bne.s    lbC003E26
                    addq.w   #1,10(a0)
                    bra      lbC003EEA

lbC003E26:          btst     #6,$12(a0)
                    bne.s    _TekiHitSound
                    btst     #7,$12(a0)
                    bne.s    _TekiHitSound
                    bset     #6,$12(a0)
                    move.b   #3,$13(a0)
                    move.b   lbB0094AA-DT(a4),$14(a0)
                    move.b   lbB0094AB-DT(a4),d0
                    lsl.b    #3,d0
                    move.b   d0,$15(a0)
_TekiHitSound:      jsr      TekiHitSound(pc)
                    subq.w   #1,(a0)
                    beq.s    lbC003E5E
                    bpl      lbC003EEA
lbC003E5E:          clr.w    (a0)
                    move.l   prgbDispMap-DT(a4),a1
                    jsr      RemoveTekiFromMap(pc)
                    move.l   prgbBuffer1-DT(a4),a1
                    add.w    #0,a1
                    move.w   8(a0),d0
                    move.b   0(a1,d0.w),d0
                    lsr.w    #4,d0
                    add.w    d0,d0
                    lea      lbW003ECA(pc),a1
                    move.w   0(a1,d0.w),d0
                    jsr      AddEnergy(pc)
                    moveq    #0,d0
                    move.l   prgbBuffer1-DT(a4),a1
                    add.w    #128,a1
                    move.w   8(a0),d1
                    move.b   0(a1,d1.w),d0
                    lsr.w    #4,d0
                    and.w    #15,d0
                    lea      lbB003EBA(pc),a1
                    move.b   (a1,d0.w),d0
                    jsr      AddEnergyMax(pc)
                    move.w   #3,8(a0)
                    move.w   6(a0),d0
                    jmp      AddScore(pc)

lbB003EBA:          dc.b     0,4,6,8,$10,$12,$14,$16,$18,$20,$22,$24,$26,$28,$30,$32
lbW003ECA:          dc.w     0,2,4,6,8,$10,$12,$14,$16,$18,$20,$22,$24,$26,$28,$30

lbC003EEA:          rts

lbC003EEC:          add.l    d0,a0
                    cmp.b    bShootableWall1-DT(a4),d1
                    beq.s    lbC003F12
                    move.w   cLevel-DT(a4),d0
                    and.w    #15,d0
                    cmp.w    #8,d0
                    beq.s    lbC003F08
                    cmp.w    #10,d0
                    bne.s    lbC003F14
lbC003F08:          and.w    #15,d1
                    cmp.w    #12,d1
                    bne.s    lbC003F14
lbC003F12:          clr.w    (a0)
lbC003F14:          rts

Beam:               lea      lbW0093DE-DT(a4),a0
                    move.w   frmThex-DT(a4),d0
                    add.w    d0,d0
                    add.w    d0,d0
                    move.l   (a0,d0.w),d0
                    move.w   d0,d5
                    swap     d0
                    move.w   d0,d4
                    clr.l    d1
                    clr.l    d2
                    clr.l    d3
                    move.w   iTekiShoot-DT(a4),d0
                    cmp.w    #-1,d0
                    beq.s    lbC003F96
                    move.w   frmThex-DT(a4),d0
                    cmp.w    #$20,d0
                    blt.s    lbC003F50
                    and.w    #1,d0
                    beq.s    lbC003F96
                    moveq    #0,d0
                    rts

lbC003F50:          tst.w    wColor-DT(a4)
                    beq.s    lbC003F6E
                    move.w   lbW0094A4-DT(a4),d0
                    addq.w   #1,d0
                    and.w    #$1F,d0
                    move.w   d0,lbW0094A4-DT(a4)
                    bne.s    lbC003F6E
                    move.w   #2,d0
                    jsr      DoSubEnergy(pc)
lbC003F6E:          move.w   xTekiShoot-DT(a4),d0
                    move.w   yTekiShoot-DT(a4),d1
                    jsr      lbC004026(pc)
                    move.w   d0,d2
                    addq.w   #7,d2
                    move.w   d1,d3
                    addq.w   #3,d3
                    move.w   xThexScrOffSet-DT(a4),d0
                    move.w   yThexScrOffSet-DT(a4),d1
                    jsr      lbC004026(pc)
                    add.w    d4,d0
                    add.w    d5,d1
                    jmp      DrawLine(pc)

lbC003F96:          tst.w    wColor-DT(a4)
                    beq.s    lbC003FB4
                    move.w   lbW0094A4-DT(a4),d0
                    addq.w   #1,d0
                    and.w    #$1F,d0
                    move.w   d0,lbW0094A4-DT(a4)
                    bne.s    lbC003FB4
                    move.w   #2,d0
                    jsr      DoSubEnergy(pc)
lbC003FB4:          lea      lbW00937E-DT(a4),a0
                    move.w   frmThex-DT(a4),d0
                    add.w    d0,d0
                    move.w   0(a0,d0.w),d0
                    beq.s    lbC00400C
                    moveq    #0,d2
                    moveq    #0,d3
                    move.b   d0,d3
                    lsr.w    #8,d0
                    move.b   d0,d2
                    ext.w    d2
                    ext.w    d3
                    move.w   xThexScrOffSet-DT(a4),d0
                    move.w   yThexScrOffSet-DT(a4),d1
                    bsr.s    lbC004026
                    add.w    d4,d0
                    add.w    d5,d1
lbC003FE0:          movem.l  d4-d7/a0-a2,-(sp)
                    move.l   pBitPlane0-DT(a4),a0
                    move.l   pBitPlane1-DT(a4),a1
                    move.l   pBitPlane2-DT(a4),a2
                    jsr      PlotPoint(pc)
                    movem.l  (sp)+,d4-d7/a0-a2
                    add.w    d2,d0
                    bmi.s    lbC00400C
                    add.w    d3,d1
                    bmi.s    lbC00400C
                    cmp.w    #640-1,d0
                    bgt.s    lbC00400C
                    cmp.w    #175,d1
                    ble.s    lbC003FE0
lbC00400C:          moveq    #1,d0
                    rts

EraseBeam:          tst.w    lbW0094AC-DT(a4)
                    beq.s    lbC004024
                    clr.w    lbW0094AC-DT(a4)
                    move.w   #0,wColor-DT(a4)
                    bra      Beam

lbC004024:          rts

lbC004026:          lsl.w    #4,d0
                    add.w    d1,d1
                    add.w    d1,d1
                    add.w    d1,d1
                    rts

DrawLine:           movem.l  d4-d7/a0-a2,-(sp)
                    move.l   pBitPlane0-DT(a4),a0
                    move.l   pBitPlane1-DT(a4),a1
                    move.l   pBitPlane2-DT(a4),a2
                    bsr.s    PlotPoint
                    moveq    #1,d4
                    moveq    #1,d5
                    sub.w    d0,d2
                    bpl.s    lbC00404E
                    neg.w    d2
                    neg.w    d4
lbC00404E:          sub.w    d1,d3
                    bpl.s    lbC004056
                    neg.w    d3
                    neg.w    d5
lbC004056:          cmp.w    d2,d3
                    blt.s    lbC004076
                    move.w   d2,d6
                    lsr.w    #1,d6
                    move.w   d3,d7
lbC004060:          add.w    d5,d1
                    sub.w    d2,d6
                    bpl.s    lbC00406A
                    add.w    d4,d0
                    add.w    d3,d6
lbC00406A:          bsr.s    PlotPoint
                    dbra     d7,lbC004060
                    move.l   lbL0094A6-DT(a4),d0
                    bra.s    lbC004090

lbC004076:          move.w   d3,d6
                    lsr.w    #1,d6
                    move.w   d2,d7
lbC00407C:          add.w    d4,d0
                    sub.w    d3,d6
                    bpl.s    lbC004086
                    add.w    d5,d1
                    add.w    d2,d6
lbC004086:          bsr.s    PlotPoint
                    dbra     d7,lbC00407C
                    move.l   lbL0094A6-DT(a4),d0
lbC004090:          movem.l  (sp)+,d4-d7/a0-a2
                    rts

PlotPoint:          movem.l  d0-d3/a3,-(sp)
                    lea      lbW0040EC(pc),a3
                    and.l    #$FFFF,d0
                    move.l   d0,d2
                    mulu     #$50,d1
                    lsr.w    #4,d0
                    add.w    d0,d0
                    add.w    d1,d0
                    and.l    #15,d2
                    add.w    d2,d2
                    move.w   0(a3,d2.w),d1
                    move.w   wColor-DT(a4),d2
                    beq.s    lbC0040D2
                    move.w   0(a0,d0.l),d3
                    and.w    d1,d3
                    bne.s    lbC00410C
                    move.w   0(a1,d0.l),d3
                    and.w    d1,d3
                    bne.s    lbC00410C
lbC0040D2:          not.w    d1
                    move.w   0(a2,d0.l),d3
                    eor.w    d2,d3
                    and.w    d1,d3
                    eor.w    d2,d3
                    move.w   d3,0(a2,d0.l)
                    move.l   d0,lbL0094A6-DT(a4)
                    movem.l  (sp)+,d0-d3/a3
                    rts

lbW0040EC:          dc.w     $8000,$4000,$2000,$1000,$800,$400,$200,$100,$80
                    dc.w     $40,$20,$10,8,4,2,1

lbC00410C:          move.l   d0,lbL0094A0-DT(a4)
                    movem.l  (sp)+,d0-d3/a3
                    move.l   (sp)+,a0
                    move.l   lbL0094A0-DT(a4),d0
                    bra      lbC004090

InitScore:          clr.l    lScore-DT(a4)
                    rts

InitEnergy:         move.w   #256,wEnergy-DT(a4)
                    move.w   #256,wEnergyMax-DT(a4)
                    rts

AddScore:           movem.l  d0/a0/a1,-(sp)
                    lea      TblScores(pc),a0
                    add.w    d0,d0
                    add.w    d0,d0
                    move.l   0(a0,d0.w),d0
                    move.l   d0,tScore-DT(a4)
                    lea      lScore-DT(a4),a0
                    lea      wEnergy-DT(a4),a1
                    abcd     -(a0),-(a1)
                    abcd     -(a0),-(a1)
                    abcd     -(a0),-(a1)
                    abcd     -(a0),-(a1)
                    bsr.s    PrintScore
                    movem.l  (sp)+,d0/a0/a1
                    rts

TblScores:          dc.l     1,16,22,32,48,64,80,112,256,336,512,768
                    dc.l     1024,1280,1792,4096

PrintScore:         movem.l  d0-d7/a0-a3,-(sp)
                    move.l   pBitPlane0-DT(a4),a2
                    move.l   pBitPlane1-DT(a4),a3
                    move.w   #$3C0E,d1
                    add.w    d1,a2
                    add.w    d1,a3
                    lea      lScore-DT(a4),a0
                    move.w   #3,d2
lbC0041BA:          moveq    #0,d1
                    move.b   (a0)+,d1
                    move.l   d1,d0
                    lsr.w    #4,d0
                    add.b    #$30,d0
                    jsr      PrintAChar(pc)
                    move.l   d1,d0
                    and.w    #15,d0
                    add.b    #$30,d0
                    jsr      PrintAChar(pc)
                    dbra     d2,lbC0041BA
                    bra      lbC0042F6

DoSubEnergy:        movem.l  d0/a0/a1,-(sp)
                    bra.s    lbC0041F6

SubEnergy:          movem.l  d0/a0/a1,-(sp)
                    tst.w    fGod-DT(a4)
                    bne.s    lbC00420E
                    tst.w    fShields-DT(a4)
                    bne.s    lbC00420E
lbC0041F6:          move.w   d0,tScore-DT(a4)
                    lea      lbW00951E-DT(a4),a0
                    lea      wEnergyMax-DT(a4),a1
                    sbcd     -(a0),-(a1)
                    sbcd     -(a0),-(a1)
                    bcc.s    lbC00420E
                    move.w   #0,wEnergy-DT(a4)
lbC00420E:          bsr.s    PrintEnergy
                    movem.l  (sp)+,d0/a0/a1
                    rts

AddEnergy:          movem.l  d0/a0/a1,-(sp)
                    move.w   d0,tScore-DT(a4)
                    lea      lbW00951E-DT(a4),a0
                    lea      wEnergyMax-DT(a4),a1
                    abcd     -(a0),-(a1)
                    abcd     -(a0),-(a1)
                    move.w   wEnergy-DT(a4),d0
                    cmp.w    wEnergyMax-DT(a4),d0
                    ble.s    lbC00423A
                    move.w   wEnergyMax-DT(a4),wEnergy-DT(a4)
lbC00423A:          bsr.s    PrintEnergy
                    movem.l  (sp)+,d0/a0/a1
                    rts

PrintEnergy:        movem.l  d0-d7/a0-a3,-(sp)
                    move.l   pBitPlane0-DT(a4),a2
                    move.l   pBitPlane1-DT(a4),a3
                    move.w   #$3744,d1
                    add.w    d1,a2
                    add.w    d1,a3
                    lea      wEnergy-DT(a4),a0
                    moveq    #0,d1
                    move.b   (a0)+,d0
                    and.w    #15,d0
                    add.b    #$30,d0
                    jsr      PrintAChar(pc)
                    moveq    #0,d1
                    move.b   (a0)+,d1
                    move.l   d1,d0
                    lsr.w    #4,d0
                    add.b    #$30,d0
                    jsr      PrintAChar(pc)
                    move.l   d1,d0
                    and.w    #15,d0
                    add.b    #$30,d0
                    bsr.s    PrintAChar
                    jsr      DrawEnergyBar(pc)
                    bra.s    lbC0042F6

AddEnergyMax:       movem.l  d0/a0/a1,-(sp)
                    move.w   d0,tScore-DT(a4)
                    lea      lbW00951E-DT(a4),a0
                    lea      iorInput-DT(a4),a1
                    abcd     -(a0),-(a1)
                    abcd     -(a0),-(a1)
                    cmp.w    #$500,wEnergyMax-DT(a4)
                    ble.s    lbC0042AE
                    move.w   #$500,wEnergyMax-DT(a4)
lbC0042AE:          bsr.s    PrintEnergyMax
                    movem.l  (sp)+,d0/a0/a1
                    rts

PrintEnergyMax:     movem.l  d0-d7/a0-a3,-(sp)
                    move.l   pBitPlane0-DT(a4),a2
                    move.l   pBitPlane1-DT(a4),a3
                    move.w   #$3C44,d1
                    add.w    d1,a2
                    add.w    d1,a3
                    lea      wEnergyMax-DT(a4),a0
                    moveq    #0,d1
                    move.b   (a0)+,d0
                    and.w    #15,d0
                    add.b    #$30,d0
                    bsr.s    PrintAChar
                    moveq    #0,d1
                    move.b   (a0)+,d1
                    move.l   d1,d0
                    lsr.w    #4,d0
                    add.b    #$30,d0
                    bsr.s    PrintAChar
                    move.l   d1,d0
                    and.w    #15,d0
                    add.b    #$30,d0
                    bsr.s    PrintAChar
lbC0042F6:          movem.l  (sp)+,d0-d7/a0-a3
                    rts

PrintAChar:         move.l   pbmMap-DT(a4),a1
                    lsl.w    #5,d0
                    add.w    d0,a1
                    move.w   (a1)+,(a2)+
                    move.w   (a1)+,(a3)+
                    move.w   (a1)+,$4E(a2)
                    move.w   (a1)+,$4E(a3)
                    move.w   (a1)+,$9E(a2)
                    move.w   (a1)+,$9E(a3)
                    move.w   (a1)+,$EE(a2)
                    move.w   (a1)+,$EE(a3)
                    move.w   (a1)+,$13E(a2)
                    move.w   (a1)+,$13E(a3)
                    move.w   (a1)+,$18E(a2)
                    move.w   (a1)+,$18E(a3)
                    move.w   (a1)+,$1DE(a2)
                    move.w   (a1)+,$1DE(a3)
                    move.w   (a1)+,$22E(a2)
                    move.w   (a1)+,$22E(a3)
                    rts

PrintShields:       movem.l  d0-d7/a0-a3,-(sp)
                    move.l   pBitPlane2-DT(a4),a0
                    move.w   #$3A20,d1
                    add.w    d1,a0
                    moveq    #0,d0
                    tst.w    fShields-DT(a4)
                    beq.s    lbC00435A
                    moveq    #-1,d0
lbC00435A:          move.w   #$13,d1
lbC00435E:          move.l   d0,(a0)+
                    move.l   d0,$4C(a0)
                    move.l   d0,$9C(a0)
                    move.l   d0,$EC(a0)
                    move.l   d0,$13C(a0)
                    dbra     d1,lbC00435E

                    move.l   pBitPlane0-DT(a4),a2
                    move.l   pBitPlane1-DT(a4),a3
                    move.w   #$399E,d1
                    add.w    d1,a2
                    add.w    d1,a3
                    lea      SHIELDOFF_MSG(pc),a0
                    tst.w    fShields-DT(a4)
                    beq.s    lbC004392
                    lea      SHIELDON_MSG(pc),a0
lbC004392:          moveq    #0,d0
                    move.b   (a0)+,d0
                    beq.s    lbC00439E
                    jsr      PrintAChar(pc)
                    bra.s    lbC004392

lbC00439E:          movem.l  (sp)+,d0-d7/a0-a3
                    rts

SHIELDOFF_MSG:      dc.b     'SHIELD OFF',0
                    even
SHIELDON_MSG:       dc.b     'SHIELD  ON',0
                    even

UpDateShields:      tst.w    fShields-DT(a4)
                    beq.s    PrintShields
                    movem.l  d0-d7/a0-a3,-(sp)
                    move.l   pBitPlane2-DT(a4),a0
                    move.w   #$3A20,d1
                    add.w    d1,a0
                    move.l   a0,a1
                    add.w    #$50,a1
                    move.w   #$50,d0
                    move.w   d0,d2
                    sub.w    fShields-DT(a4),d0
                    move.w   d0,d1
                    lsr.w    #1,d0
                    and.w    #1,d1
                    sub.w    d1,d2
                    moveq    #0,d3
                    move.w   d3,d4
                    moveq    #0,d5
lbC0043F0:          cmp.w    d0,d5
                    blt.s    lbC0043FE
                    bgt.s    lbC004426
                    move.w   #15,d3
                    move.w   #$F0,d4
lbC0043FE:          move.b   d3,(a0)+
                    move.b   d4,-(a1)
                    move.b   d3,$4F(a0)
                    move.b   d4,$50(a1)
                    move.b   d3,$9F(a0)
                    move.b   d4,$A0(a1)
                    move.b   d3,$EF(a0)
                    move.b   d4,$F0(a1)
                    move.b   d3,$13F(a0)
                    move.b   d4,$140(a1)
                    addq.w   #1,d5
                    bra.s    lbC0043F0

lbC004426:          move.l   pBitPlane0-DT(a4),a2
                    move.l   pBitPlane1-DT(a4),a3
                    move.w   #$399E,d1
                    add.w    d1,a2
                    add.w    d1,a3
                    lea      SHIELDOFF_MSG0(pc),a0
                    tst.w    fShields-DT(a4)
                    beq.s    lbC004444
                    lea      SHIELDON_MSG0(pc),a0
lbC004444:          moveq    #0,d0
                    move.b   (a0)+,d0
                    beq.s    lbC004450
                    jsr      PrintAChar(pc)
                    bra.s    lbC004444

lbC004450:          movem.l  (sp)+,d0-d7/a0-a3
                    rts

SHIELDOFF_MSG0:     dc.b     'SHIELD OFF',0
                    even
SHIELDON_MSG0:      dc.b     'SHIELD  ON',0
                    even

DrawEnergyBar:      movem.l  d0-d7/a0-a3,-(sp)
                    move.l   pBitPlane0-DT(a4),a0
                    move.l   pBitPlane2-DT(a4),a1
                    move.l   #$37B0,d0
                    add.l    d0,a0
                    add.l    d0,a1
                    lea      lbW004520(pc),a2
                    lea      lbW004552(pc),a3
                    move.w   wEnergy-DT(a4),d0
                    cmp.w    #$100,d0
                    ble.s    lbC00449A
                    move.w   #$100,d0
lbC00449A:          clr.l    d2
                    and.w    #$FFF,d0
                    move.w   d0,d1
                    lsr.w    #8,d1
                    mulu     #$64,d1
                    add.w    d1,d2
                    move.w   d0,d1
                    lsr.w    #4,d1
                    and.w    #15,d1
                    mulu     #10,d1
                    add.w    d1,d2
                    and.w    #15,d0
                    add.w    d2,d0
                    move.w   d0,d1
                    move.w   #-1,d2
                    lsr.w    #1,d0
                    and.w    #1,d1
                    move.w   #0,d7
lbC0044CE:          cmp.w    d0,d7
                    beq.s    lbC0044DA
                    blt.s    lbC0044E6
                    move.w   #0,d2
                    bra.s    lbC0044E6

lbC0044DA:          move.w   #0,d2
                    tst.w    d1
                    beq.s    lbC0044E6
                    move.w   #$F0,d2
lbC0044E6:          move.w   d2,d3
                    move.w   d2,d4
                    and.b    (a2)+,d3
                    and.b    (a3)+,d4
                    move.b   d3,(a0)+
                    move.b   d4,(a1)+
                    move.b   d3,$4F(a0)
                    move.b   d4,$4F(a1)
                    move.b   d3,$9F(a0)
                    move.b   d4,$9F(a1)
                    move.b   d3,$EF(a0)
                    move.b   d4,$EF(a1)
                    move.b   d3,$13F(a0)
                    move.b   d4,$13F(a1)
                    addq.w   #1,d7
                    cmp.w    #$32,d7
                    blt.s    lbC0044CE
                    movem.l  (sp)+,d0-d7/a0-a3
                    rts

lbW004520:          dc.w     -1,-1,-1,-1,-1
                    dc.w     0,0,0,0,0
                    dc.w     -1,-1,-1,-1,-1
                    dc.w     -1,-1,-1,-1,-1
                    dc.w     -1,-1,-1,-1,-1
lbW004552:          dc.w     0,0,0,0,0
                    dc.w     -1,-1,-1,-1,-1
                    dc.w     -1,-1,-1,-1,-1
                    dc.w     -1,-1,-1,-1,-1
                    dc.w     -1,-1,-1,-1,-1

IncLevel:           movem.l  a0/a1,-(sp)
                    move.w   #1,tScore-DT(a4)
                    lea      lbW00951E-DT(a4),a0
                    lea      tScore-DT(a4),a1
                    abcd     -(a0),-(a1)
                    move.b   (a1),d0
                    lsl.w    #4,d0
                    move.b   (a1),d0
                    and.w    #$F0F,d0
                    or.w     #$3030,d0
                    movem.l  (sp)+,a0/a1
                    rts

PrintGameOver:      movem.l  d0-d7/a0-a6,-(sp)
                    lea      GAMEOVER_MSG-DT(a4),a0
                    move.l   pBitPlane0-DT(a4),a2
                    move.l   pBitPlane1-DT(a4),a3
                    move.w   #$141E,d1
                    add.w    d1,a2
                    add.w    d1,a3
lbC0045C4:          moveq    #0,d0
                    move.b   (a0)+,d0
                    beq.s    lbC0045D0
                    jsr      PrintAChar(pc)
                    bra.s    lbC0045C4

lbC0045D0:          movem.l  (sp)+,d0-d7/a0-a6
                    rts

PrintLevelCompleted:
                    movem.l  d0-d7/a0-a6,-(sp)
                    move.w   charBlank-DT(a4),d7
                    lea      LEVELXXCOMPLE_MSG-DT(a4),a0
                    move.w   wOldLevel-DT(a4),$2E(a0)
                    move.l   a0,a5
lbC0045EA:          move.l   pBitPlane0-DT(a4),a2
                    move.l   pBitPlane1-DT(a4),a3
                    move.w   #$1400,d1
                    add.w    d1,a2
                    add.w    d1,a3
                    move.w   #$27,d1
lbC0045FE:          moveq    #0,d0
                    move.b   (a0)+,d0
                    beq.s    lbC004622
                    cmp.b    #$2E,d0
                    bne.s    _PrintAChar
                    move.b   d7,d0
_PrintAChar:        jsr      PrintAChar(pc)
                    dbra     d1,lbC0045FE
                    addq.w   #1,a5
                    move.l   a5,a0
                    move.w   #4,d0
                    jsr      WaitSome(pc)
                    bra.s    lbC0045EA

lbC004622:          movem.l  (sp)+,d0-d7/a0-a6
                    rts

_setmem:            move.l   4(sp),a0
                    movem.l  8(sp),d0/d1
                    bra.s    .dbrajmp

.loop:              move.b   d1,(a0)+
.dbrajmp:           dbra     d0,.loop
                    rts

; D0.L = D0.L / D1.L
_divs:              movem.l  d1/d4,-(sp)
                    clr.l    d4
                    tst.l    d0
                    bpl.s    1$
                    neg.l    d0
                    addq.w   #1,d4
1$:                 tst.l    d1
                    bpl.s    2$
                    neg.l    d1
                    eor.w    #1,d4
2$:                 bsr.s    comdivide
divs_chksign:       tst.w    d4
                    beq.s    divs_exit
                    neg.l    d0
divs_exit:          movem.l  (sp)+,d1/d4
                    tst.l    d0
                    rts

comdivide:          movem.l  d2/d3,-(sp)
                    swap     d1
                    tst.w    d1
                    bne.s    .hardldv
                    swap     d1
                    move.w   d1,d3
                    move.w   d0,d2
                    clr.w    d0
                    swap     d0
                    divu     d3,d0
                    move.l   d0,d1
                    swap     d0
                    move.w   d2,d1
                    divu     d3,d1
                    move.w   d1,d0
                    clr.w    d1
                    swap     d1
                    movem.l  (sp)+,d2/d3
                    rts

.hardldv:           swap     d1
                    move.l   d1,d3
                    move.l   d0,d1
                    clr.w    d1
                    swap     d1
                    swap     d0
                    clr.w    d0
                    moveq    #16-1,d2
.loop:              add.l    d0,d0
                    addx.l   d1,d1
                    cmp.l    d1,d3
                    bhi.s    1$
                    sub.l    d3,d1
                    addq.w   #1,d0
1$:                 dbra     d2,.loop
                    movem.l  (sp)+,d2/d3
                    rts

;; Dispatch a device command
;; BeginIO(iORequest)
_BeginIO:           move.l   4(sp),a1
                    move.l   $14(a1),a6
                    jmp      _LVOSupervisor(a6)

; Get the status of an IORequest
; result = CheckIO(iORequest)
_CheckIO:           move.l   4(sp),a1
                    move.l   4.w,a6
                    jmp      _LVOCheckIO(a6)

; Conclude access to a device
; CloseDevice(iORequest)
_CloseDevice:       move.l   4(sp),a1
                    move.l   4.w,a6
                    jmp      _LVOCloseDevice(a6)

_CreatePort:        link     a5,#0
                    movem.l  d4/a2,-(sp)
                    pea      -1
                    jsr      _AllocSignal(pc)
                    addq.w   #4,sp
                    move.l   d0,d4
                    cmp.l    #-1,d0
                    bne.s    lbC005586
                    moveq    #0,d0
lbC00557E:          movem.l  (sp)+,d4/a2
                    unlk     a5
                    rts

lbC005586:          pea      $10001
                    pea      $22
                    jsr      _AllocMem(pc)
                    addq.w   #8,sp
                    move.l   d0,a2
                    tst.l    d0
                    bne.s    lbC0055A8
                    move.l   d4,-(sp)
                    jsr      _FreeSignal(pc)
                    addq.w   #4,sp
                    moveq    #0,d0
                    bra.s    lbC00557E

lbC0055A8:          move.l   8(a5),10(a2)
                    move.b   15(a5),9(a2)
                    move.b   #4,8(a2)
                    clr.b    14(a2)
                    move.b   d4,15(a2)
                    clr.l    -(sp)
                    jsr      _FindTask(pc)
                    addq.w   #4,sp
                    move.l   d0,$10(a2)
                    tst.l    8(a5)
                    beq.s    lbC0055DE
                    move.l   a2,-(sp)
                    jsr      _AddPort(pc)
                    addq.w   #4,sp
                    bra.s    lbC0055E8

lbC0055DE:          pea      $14(a2)
                    jsr      _NewList(pc)
                    addq.w   #4,sp
lbC0055E8:          move.l   a2,d0
                    bra.s    lbC00557E

_DeletePort:        link     a5,#0
                    move.l   a2,-(sp)
                    move.l   8(a5),a2
                    tst.l    10(a2)
                    beq.s    lbC005604
                    move.l   a2,-(sp)
                    jsr      _RemPort(pc)
                    addq.w   #4,sp
lbC005604:          move.b   #-1,8(a2)
                    move.l   #-1,20(a2)
                    moveq    #0,d0
                    move.b   15(a2),d0
                    move.l   d0,-(sp)
                    jsr      _FreeSignal(pc)
                    addq.w   #4,sp
                    pea      $22
                    move.l   a2,-(sp)
                    jsr      _FreeMem(pc)
                    addq.w   #8,sp
                    move.l   (sp)+,a2
                    unlk     a5
                    rts

; Add a public message port to the system
; AddPort(port)
_AddPort:           move.l   4(sp),a1
                    move.l   4.w,a6
                    jmp      _LVOAddPort(a6)

; Allocate a signal bit
; signalNum = AllocSignal(signalNum)
_AllocSignal:       move.l   4(sp),d0
                    move.l   4.w,a6
                    jmp      _LVOAllocSignal(a6)

; Allocate memory given certain requirements
; memoryBlock = AllocMem(byteSize,attributes)
_AllocMem:          movem.l  4(sp),d0/d1
                    move.l   4.w,a6
                    jmp      _LVOAllocMem(a6)

_DoIO:              move.l   4(sp),a1
                    move.l   4.w,a6
                    jmp      _LVODoIO(a6)

; Find a task with the given name or find oneself
; task = FindTask(name)
_FindTask:          move.l   4(sp),a1
                    move.l   4.w,a6
                    jmp      _LVOFindTask(a6)

; Deallocate with knowledge
; FreeMem(memoryBlock,byteSize)
_FreeMem:           move.l   4(sp),a1
                    move.l   8(sp),d0
                    move.l   4.w,a6
                    jmp      _LVOFreeMem(a6)

; Free a signal bit
; FreeSignal(signalNum)
_FreeSignal:        move.l   4(sp),d0
                    move.l   4.w,a6
                    jmp      _LVOFreeSignal(a6)

; Prepare a list structure for use
; NewList(list*)
_NewList:           move.l   4(sp),a0
                    move.l   a0,LH_HEAD(a0)
                    addq.l   #LH_TAIL,(a0)
                    clr.l    LH_TAIL(a0)
                    move.l   a0,LH_TAILPRED(a0)
                    rts

; Gain access to a device
; error = OpenDevice(devName,unitNumber,iORequest,flags)
_OpenDevice:        move.l   4(sp),a0
                    movem.l  8(sp),d0/a1
                    move.l   $10(sp),d1
                    move.l   4.w,a6
                    jmp      _LVOOpenDevice(a6)

; Remove a message port from the system
; RemPort(port)
_RemPort:           move.l   4(sp),a1
                    move.l   4.w,a6
                    jmp      _LVORemPort(a6)

; Initiate an I/O command
; SendIO(iORequest)
_SendIO:            move.l   4(sp),a1
                    move.l   4.w,a6
                    jmp      _LVOSendIO(a6)

; Wait for completion of an I/O request
; error = WaitIO(iORequest)
_WaitIO:            move.l   4(sp),a1
                    move.l   4.w,a6
                    jmp      _LVOWaitIO(a6)

; -------------------------------------------
                    section  vars,data

__H0_end:           dc.w     $700
Garbage:            dc.w     0,0
Save_A5:            dc.l     0
Save_SP:            dc.l     0
_prgbSongCur:       dc.l     0
fShieldsUsed:       dc.w     0
fJoy:               dc.w     0
bShootableWall1:    dc.b     0
bShootableWall2:    dc.b     0
lbW005872:          dc.w     0
lbW005874:          dc.w     0
cTekiOnScrn:        dc.w     0
RandomNum:          dc.w     0
lbL0058A6:          dcb.w    880,0
lbW005F86:          dc.w     $20,$20,$20,$20,$10,$10,$10,$20,$20,$20,$20,0,$70
                    dc.w     $70,$70,0
palette_title:      dc.w     $0,$0,$0
                    dc.w     $0,$0,$f
                    dc.w     $f,$f,$f
                    dc.w     $0,$b,$b
                    dc.w     $f,$0,$0
                    dc.w     $f,$f,$f
                    dc.w     $b,$b,$0
                    dc.w     $0,$f,$f
                    dc.w     $0,$b,$0
                    dc.w     $0,$0,$f
                    dc.w     $f,$f,$0
                    dc.w     $0,$f,$0
                    dc.w     $0,$f,$0
                    dc.w     $0,$f,$0
                    dc.w     $0,$f,$0
                    dc.w     $0,$f,$0

palette_game:       dc.w     $0,$0,$0
                    dc.w     $f,$5,$5
                    dc.w     $5,$5,$f
                    dc.w     $f,$f,$f
                    dc.w     $f,$7,$5
                    dc.w     $5,$f,$5
                    dc.w     $5,$5,$5
                    dc.w     $f,$f,$5
                    dc.w     $0,$f,$0       ; not used
                    dc.w     $0,$f,$0
                    dc.w     $0,$f,$0
                    dc.w     $0,$f,$0
                    dc.w     $0,$f,$0
                    dc.w     $0,$f,$0
                    dc.w     $0,$f,$0
                    dc.w     $0,$f,$0

_RegisterA4:        dc.l     0
PRESENTEDBYSI_MSG:  dc.b     'PRESENTED BY       SIERRA ON_LINE^ INC]',0
GAMEDESIGNHGO_MSG:  dc.b     'GAME DESIGN           H]GODAI_S]UESARA',0
                    even
AMIGAVERSIONS_MSG:  dc.b     'AMIGA VERSION      SYNERGISTIC SOFTWARE',0
PROGRAMMEDBYL_MSG:  dc.b     'PROGRAMMED BY      LLOYD D] OLLMANN JR]',0
THANKSTOMICHA_MSG:  dc.b     'THANKS TO            MICHAEL D] BRANHAM',0
HAYESHAUGEN_MSG:    dc.b     '                           HAYES HAUGEN',0
JOHNPCONLEY_MSG:    dc.b     '                         JOHN P] CONLEY',0
ROBERTCCLARDY_MSG:  dc.b     '                       ROBERT C] CLARDY',0
GAMEMUSIC_MSG:      dc.b     'GAME MUSIC                             ',0
THEXDERHGODAI_MSG:  dc.b     '     THEXDER                    H]GODAI',0
MOONLIGHTSONA_MSG:  dc.b     '     MOONLIGHT SONATA         BEETHOVEN',0
BROKENBYBLACK_MSG:  dc.b     'GAME CONCEPT BY     GAME ARTS CO]^ LTD]',0
COPYRIGHT1987_MSG:  dc.b     ' COPYRIGHT @1987 BY SIERRA ON_LINE^INC]',0
szSdWaveForms:      dc.b     'data/SdWaveForms.dat',0,0
szSpPres:           dc.b     'data/SpPres.dat',0
szSpWarn:           dc.b     'data/SpWarn.dat',0
szThexderDying:     dc.b     'data/Dying',0,0
dataMoonMusic_MSG:  dc.b     'data/MoonMusic.Data',0
dataThexMusic_MSG:  dc.b     'data/ThexMusic.Data',0
dataRedPlane_MSG:   dc.b     'data/RedPlane',0
dataBluePlane_MSG:  dc.b     'data/BluePlane',0,0
dataGreenPlan_MSG:  dc.b     'data/GreenPlane',0
datatx_MSG:         dc.b     'data/tx',0
datat1_MSG:         dc.b     'data/t1',0
datatexmp01_MSG:    dc.b     'data/texmp01',0
datateki01_MSG:     dc.b     'data/teki01',0,0
lbL006334:          dc.l     0
wOldLevel:          dc.w     0
charBlank:          dc.w     0
dataLogo_MSG:       dc.b     'data/Logo',0
ascii_MSG0:         dc.b     '01020304050607080910091213141516'
ascii_MSG1:         dc.b     '01020304050507080909111213141516'
lbW006386:          dc.w     0,0,0,0,0,640,0,0,0,640,0,0,0,640,0,0
lbB0063A6:          dc.b     0,0,0,0,0,0,0,0,0
                    dc.b     1,5,7,3,8,2,6,4,1
                    dc.b     1,5,7,3,8,2,6,4,2
                    dc.b     1,5,$8C,3,8,2,6,4,3
                    dc.b     1,5,7,3,8,2,6,4,4
                    dc.b     1,5,7,3,8,2,6,4,5
                    dc.b     1,5,7,3,8,2,6,4,6
                    dc.b     1,5,7,$8B,8,2,6,4,7
                    dc.b     1,5,7,3,8,2,6,4,8
                    dc.b     9,$87,$13,$14,13,14,$87,$83,$13
                    dc.b     10,$83,$13,$14,13,14,$87,$83,$14
                    dc.b     $89,$87,11,$92,$8D,$8E,$87,$83,$11
                    dc.b     $8A,$83,$91,12,$8D,$8E,$87,$83,$12
                    dc.b     9,$87,15,$10,13,14,$87,$83,15
                    dc.b     10,$83,15,$10,13,14,$87,$83,$10
                    dc.b     $13,$87,15,$10,15,$10,$87,$83,$13
                    dc.b     $14,$83,15,$10,15,$10,$87,$83,$14
                    dc.b     $89,$87,11,$92,$8D,$8E,$87,$83,$11
                    dc.b     $8A,$83,$91,12,$8D,$8E,$87,$83,$12
                    dc.b     $13,$87,15,$10,15,$10,$87,$83,$13
                    dc.b     $14,$83,15,$10,15,$10,$87,$83,$14
                    dc.b     0
lbW006464:          dc.l     0
                    dc.l     $e
                    dc.l     $7C
                    dc.l     $E0
                    dc.l     $c01C0
                    dc.l     $1C0000
                    dc.l     $19C000
                    dc.l     $38000
                    dc.l     $30007
                    dc.l     $e
                    dc.l     $e
                    dc.l     $3FC00
                    dc.l     $1FE0
                    dc.l     $3C00F
                    dc.l     $1FE
                    dc.l     $1FC00
                    dc.l     $1FC0
                    dc.l     $1C00
                    dc.l     $1C00
                    dc.l     $1C00
                    dc.l     $1C00
lbL0064B8:          dc.l     0
                    dc.l     P_UP_UP_Collisions
                    dc.l     P_UR_UR_Collisions
                    dc.l     P_RT_RT_Collisions
                    dc.l     P_DR_DR_Collisions
                    dc.l     P_DN_DN_Collisions
                    dc.l     P_DL_DL_Collisions
                    dc.l     P_LF_LF_Collisions
                    dc.l     P_UL_UL_Collisions
                    dc.l     M_UP_LF_Collisions
                    dc.l     M_UP_RT_Collisions
                    dc.l     M_LF_LF_Collisions
                    dc.l     M_RT_RT_Collisions
                    dc.l     M_UL_LF_Collisions
                    dc.l     M_UR_RT_Collisions
                    dc.l     F_DL_LF_Collisions
                    dc.l     F_DR_RT_Collisions
                    dc.l     M_SD_LF_Collisions
                    dc.l     M_SD_RT_Collisions
                    dc.l     F_DN_LF_Collisons
                    dc.l     F_DN_RT_Collisions
lbL00650C:          dc.l     0
                    dc.l     0
                    dc.l     0
                    dc.l     lbW002C9C,0,0,0
                    dc.l     lbW002C8A,0
                    dc.l     lbW002C6A
                    dc.l     lbW002C70
                    dc.l     lbW002CC0
                    dc.l     lbW002CAE
                    dc.l     lbW002C6A
                    dc.l     lbW002C70
                    dc.l     lbW002CC0
                    dc.l     lbW002CAE
                    dc.l     lbW002C76
                    dc.l     lbW002C80
                    dc.l     lbW002CC0
                    dc.l     lbW002CAE
                    dc.l     lbW002C5E
                    dc.l     lbW002C64
                    dc.l     lbW002C5E
                    dc.l     lbW002C64
lbW006570:          dc.w     0,0,1,1,1,0,$FFFF,$FFFF,$FFFF,0,0,$FFFF,1,$FFFF,1
                    dc.w     $FFFF,1,0,0,0,0,0,$FFFF,$FFFF,0,1,1,1,0,$FFFF
                    dc.w     $FFFF,$FFFF,0,0,$FFFF,$FFFF,1,1,0,0,1,1,0,$24,$26
                    dc.w     $28,$2A,$2C,$2E,$20,$22,0,8,0,8,0,8,0,8,0,8,0,8
fState:             dc.w     0
lbL0065F0:          dc.l     0
fThex:              dc.w     0
lbW006600:          dc.w     0
lbW006602:          dc.w     0
lbW006608:          dc.w     0
lbL00660C:          dc.l     0
lbW006610:          dc.w     0
lbW006612:          dc.w     0
lbW006616:          dc.w     0
wQuad:              dc.w     0
lbW00661A:          dc.w     0
lbW00661C:          dc.w     0
lbW00661E:          dc.w     0
cJump:              dc.w     0
lbW006622:          dc.b     0,4,6,8,$A,$C,$E,0,2,0
fDead:              dc.w     0
lbB00662E:          dc.b     0
lbB00662F:          dc.b     0
                    dc.l     lbW006644
lbL006634:          dc.l     lbW006644
                    dc.l     lbW0066C4
                    dc.l     lbW0066C4
                    dc.l     lbW006644
                    ; some gfx
lbW006644:          dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,15,$F000,0,0,$3F
                    dc.w     $FC00,0,0,$FF,$FF00,0,0,$FF,$FF00,0,0,$FF,$FF00,0
                    dc.w     0,$FF,$FF00,0,0,$3F,$FC00,0,0,15,$F000,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0
lbW0066C4:          dc.w     15,$F000,0,0,$FF,$FF00,0,0,$3FF,$FFC0,0,0,$FFF
                    dc.w     $FFF0,0,0,$3FFF,$FFFC,0,0,$3FFF,$FFFC,0,0,$FFFF
                    dc.w     $FFFF,0,0,$FFFF,$FFFF,0,0,$FFFF,$FFFF,0,0,$FFFF
                    dc.w     $FFFF,0,0,$3FFF,$FFFC,0,0,$3FFF,$FFFC,0,0,$FFF
                    dc.w     $FFF0,0,0,$3FF,$FFC0,0,0,$FF,$FF00,0,0,15,$F000,0
                    dc.w     0
lbL006744:          dc.l     lbC0032FE
                    dc.l     lbC003308
                    dc.l     lbC003354
                    dc.l     lbC00336E
                    dc.l     lbC0033C6
                    dc.l     lbC003492
                    dc.l     lbC0034F8
                    dc.l     MovePattern7
                    dc.l     lbC003554
                    dc.l     lbC0033C6
                    dc.l     lbC003308
                    dc.l     lbC003308
rgobjTeki:          dcb.l    2816,0
                    dc.w     $FFFF
iShoot:             dc.w     0
iTekiShoot:         dc.w     0
xTekiShoot:         dc.w     0
yTekiShoot:         dc.w     0
                    ; related to the beam drawing
lbW00937E:          dc.w     $FF00,$FF00,$FF00,$FF00,$FF00,$FF00,$FF00,$FF00
                    dc.w     $100,$100,$100,$100,$100,$100,$100,$100,$FF00
                    dc.w     $100,0,0,0,0,0,0,0,0,0,0,0,0,0,0,$FF00,0,$FEFF,0
                    dc.w     $FF,0,$2FF,0,$100,0,$201,0,1,0,$FE01,0
lbW0093DE:          dc.w     8,5,8,5,8,5,8,5,8,5,8,5,8,5,8,5,$26,5,$26,5,$26,5
                    dc.w     $26,5,$26,5,$26,5,$26,5,$26,5,8,5,$26,5,0,0,0,0,0
                    dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    dc.w     $FFFF,11,0,0,$FFFF,$FFFF,0,0,$18,$FFFF,0,0,$30
                    dc.w     $FFFF,0,0,$30,11,0,0,$30,$19,0,0,$18,$19,0,0
                    dc.w     $FFFF,$19,0,0
wColor:             dc.w     0
lbL0094A0:          dc.l     0
lbW0094A4:          dc.w     0
lbL0094A6:          dc.l     0
lbB0094AA:          dc.b     0
lbB0094AB:          dc.b     0
lbW0094AC:          dc.w     0
GAMEOVER_MSG:       dc.b     'GAME OVER',0
LEVELXXCOMPLE_MSG:  dc.b     '........................................'
                    dc.b     'LEVEL XX COMPLETED'
                    dc.b     '........................................'
                    ; Values are grouped like that due to abcd/sbcd pre-decrementations.
wBCDLevel:          dc.w     0
tScore:             dc.w     0
lbW00951E:          dc.w     0
lScore:             dc.l     0
wEnergy:            dc.w     0
wEnergyMax:         dc.w     0

iorInput:           dc.l     0,0,0,0,0,0,0,0
MsgPort:            dc.w     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
inthInput:          dc.w     0,0,0,0,0,0,0,0,0,0,0

gdInput:            dc.l     0,0,0
                    dc.w     0
                    dc.l     rgKeyPad
                    dc.w     0,0
rgKeyPad:           dcb.w    8,0
                    dc.w     $FFFF

VertBIntr:          dcb.l    2,0
                    dc.l     $2000000
                    dc.w     0
                    dc.l     _RegisterA4
                    dc.l     VBLServer

fAnyKey:            dc.w     0
fDemo:              dc.w     0
fFlash:             dc.w     0
cVBL:               dc.w     0
cLevel:             dc.w     0
xMapMax:            dc.w     0

cFrame:             dc.w     0

frmThex:            dc.w     0
frmTThex:           dc.w     0
xJoy1:              dc.w     0
xJoy2:              dc.w     0
yJoy1:              dc.w     0
yJoy2:              dc.w     0
joy1But1:           dc.w     0
joy2But1:           dc.w     0
xJoyCur:            dc.w     0
yJoyCur:            dc.w     0
PlayerCur:          dc.w     0
fireCur:            dc.w     0
xCur:               dc.w     0
yCur:               dc.w     0
xMap:               dc.w     0
yMap:               dc.w     0
xThex:              dc.w     0
yThex:              dc.w     0
xThexScrOffSet:     dc.w     0
yThexScrOffSet:     dc.w     0
iMapOffSetCur:      dc.w     0
iMapThexOffSetCur:  dc.w     0
pBitPlane0:         dc.l     0
pBitPlane1:         dc.l     0
pBitPlane2:         dc.l     0
pRedBuff:           dc.l     0
pGreenBuff:         dc.l     0
pBlueBuff:          dc.l     0
nscrGame:           dc.w     0,0,640,200,3
                    dc.b     0,1
                    dc.w     $8000
                    dc.w     $10F
                    dc.l     topaz_font
                    dc.l     0
                    dc.l     0,0

topaz_font:         dc.l     topazfont_MSG
                    dc.w     9,1

pIntuitionBase:     dc.l     0
pGfxBase:           dc.l     0
pDosBase:           dc.l     0
pscrGame:           dc.l     0
prgbBuffer1:        dc.l     0
pbmAll:             dc.l     0
pbmThex34:          dc.l     0
pbmThex33:          dc.l     0
pbmMap:             dc.l     0
pbmTeki:            dc.l     0
pbmDying:           dc.l     0
prgbDispMap:        dc.l     0
prgbMoonMusic:      dc.l     0
prgbThemeMusic:     dc.l     0
prgbSpPres:         dc.l     0
prgbSpWarn:         dc.l     0
prgbaudNullWaveForm: dc.l     0
prgbaudWaveForms:   dc.l     0
_fPause:            dc.w     0
_fMusicDone:        dc.w     0
_fMusic:            dc.w     0
_fSound:            dc.w     0
fQuit:              dc.w     0
fShields:           dc.w     0
fJumpLevel:         dc.w     0
fGod:               dc.w     0
topazfont_MSG:      dc.b     'topaz.font',0,0
IntuitionName:      dc.b     'intuition.library',0
GraphicsName:       dc.b     'graphics.library',0,0
DosName:            dc.b     'dos.library',0
szInput:            dc.b     'input.device',0,0
abcdef_MSG:         dc.b     '0123456789abcdef',0
                    even
_channelsPlaying:   ds.w     1
_allocIOB:          ds.l     1
_lockIOB:           ds.l     1
_audioDevice:       ds.l     1
_soundPort:         ds.l     1
_soundInt:          ds.l     1
_waveForm:          ds.l     1
_noiseForm:         ds.l     1
_sNode:             ds.l     5
                    ds.w     1
_chan:              ds.l     4
chan1_addr:         ds.l     5
chan2_addr:         ds.l     5
chan3_addr:         ds.l     1
__devtab:           ds.l     1

DT:
                    end
