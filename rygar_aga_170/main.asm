ENABLE_DEBUG:           equ     0       ; Enables system up debugging with MonAm
FAST_LOAD:              equ    	0              ; Read from floppy
ENABLE_MULTIDRIVE:	equ	0		; Must be set to 1 for release.
DEBUG_UNLIMITED_LIVES:  equ     0
DEBUG_OBS_DISABLE:      equ     0            ; Rygar can move through obstacles
DISABLE_GRAVITY:        equ     0                                    ; Disable gravity - useful for round debugging.
ENABLE_CHEAT:		equ	0

; --- MAIN DEBUG ---
DEBUG_TEXT_ENABLE:      equ     0
DEBUG_TEXT_ENABLE_EVERY_FRAME:	equ	0
DEBUG_FUNCTIONS:	equ	0
;
DEBUG_TESTSPRITE:	equ	0
DEBUG_CODE_POINTER:	equ	$100

START_TIME_BEGINNER:	equ	64*60
START_TIME_WARRIOR:	equ	64*50

START_LIVES:            equ     2

BUTTON_MODE:		equ	0	; 0 =Two button, 1= One Button
	
DEBUG_TILEMAP:		equ	0
DEBUG_ROUND:		equ	0
START_ROUND:            equ     1	

ENABLE_SHOW_HIDDEN_STONES:	equ	0

; Tiny Task Start Cursort Here1
ENABLE_ROUND1:		equ	0
ENABLE_ROUND2:		equ	0
ENABLE_ROUND3:		equ	0
ENABLE_ROUND4:		equ	0 
ENABLE_ROUND5:		equ	0
ENABLE_ROUND6:		equ	0		; Elder DONE
ENABLE_ROUND7:		equ	0
ENABLE_ROUND8:		equ	0
ENABLE_ROUND9:		equ	0
ENABLE_ROUND10:		equ	0		
ENABLE_ROUND11:		equ	0		; Elder DONE
ENABLE_ROUND12:		equ	0
ENABLE_ROUND13:		equ	0
ENABLE_ROUND14:		equ	0		; Elder DONE
ENABLE_ROUND15:		equ	0
ENABLE_ROUND16:		equ	0
ENABLE_ROUND17:		equ	0		; Elder DONE
ENABLE_ROUND18:		equ	0
ENABLE_ROUND19:		equ	0
ENABLE_ROUND20:		equ	0
ENABLE_ROUND21:		equ	0		; Elder DONE
ENABLE_ROUND22:		equ	0
ENABLE_ROUND23:		equ	0
ENABLE_ROUND24:		equ	0
ENABLE_ROUND25:		equ	0		; Elder DONE
ENABLE_ROUND26:		equ	0
ENABLE_ROUND27		equ	0			 
ENABLE_ROUND28		equ	0		; Elder	DONE
ENABLE_ROUND29		equ	0			
ENABLE_ROUND30:		equ	0			

DEBUG_PANRIGHT:         equ     0
DEBUG_PANRIGHT_TO:      equ     $848    ; Pan to map pixel position
DEBUG_BORDER_ENABLE:    equ     0

ENABLE_SPRITE_PERFORMANCE:	equ	0		; Cause sprites to flicker to keep performance at 50 fps


; Active sprites are sprites that are configured but may be disabled 
MAX_ACTIVE_SPRITES:     equ     32
MAX_SPRITES:            equ     32

ENABLE_ENEMY:		equ	1

ENABLE_DRAGON:          equ     ENABLE_ENEMY	
ENABLE_RHINO:           equ     ENABLE_ENEMY               
ENABLE_DRONE:           equ     ENABLE_ENEMY               
ENABLE_SQUIRREL:        equ     ENABLE_ENEMY               
ENABLE_GRIFFIN:         equ     ENABLE_ENEMY
ENABLE_EVIL:            equ     ENABLE_ENEMY		
ENABLE_BIGRHINO:        equ     ENABLE_ENEMY
ENABLE_RIDER:           equ     ENABLE_ENEMY		
ENABLE_TRIBESMAN:       equ     ENABLE_ENEMY
ENABLE_GIANTDEMON:      equ     ENABLE_ENEMY
ENABLE_GIANTWORM:       equ    	ENABLE_ENEMY
ENABLE_LAVAMAN:         equ     ENABLE_ENEMY		
ENABLE_FIREBALLS:       equ     ENABLE_ENEMY
ENABLE_CAVEBATS:	equ	ENABLE_ENEMY
ENABLE_ROCKETS:		equ	ENABLE_ENEMY
ENABLE_MONKEY:		equ	ENABLE_ENEMY
ENABLE_CRAB:		equ	ENABLE_ENEMY
ENABLE_APE:		equ	ENABLE_ENEMY
ENABLE_MUTANTFROG:	equ	ENABLE_ENEMY
ENABLE_VILLAGER:	equ	ENABLE_ENEMY
ENABLE_LIZARD:		equ	ENABLE_ENEMY
ENABLE_FISH:		equ	ENABLE_ENEMY
ENABLE_ELDER:		equ	ENABLE_ENEMY


;       1 Rygar, 2 Carousel, 4, Lava Man

MAX_DRAGONS:            equ     2               ; 4
MAX_RHINOS:             equ     6               ;
MAX_DRONES:             equ     6               ; 8
MAX_SQUIRRELS:          equ     1               ; 1
MAX_GRIFFINS:           equ     6
MAX_EVILS:              equ     1
MAX_BIGRHINOS:          equ     1               ; 1
MAX_RIDERS:             equ     1
MAX_GIANTWORMS:         equ     8
MAX_LAVAMAN:            equ     4
MAX_CAVEBATS:		equ	1
MAX_MONKEYS:		equ	1
MAX_MUTANTFROGS:	equ	2
MAX_VILLAGERS:		equ	12

                        IFEQ    FAST_LOAD
DEBUG_BG_ENABLE:        equ     1
FLOPPY_READ:            equ     1       ; Set to true to read files from the floppy in DF0, otherwise DH1:Rygar/[FILE]
ENABLE_OCEAN_LOADER:    equ     1       ; Set to true to use Ross's Ocean Loader instead of AmigaDOS
                                        ; Background assets for rounds will always use the Ocean Loader from DF0.
ENABLE_NOTES_SCREEN     equ     1
ENABLE_TITLE_SCREEN:    equ     1       ;
ENABLE_INTRO_SCREEN:    equ     1       ;
ENABLE_GRADIENTS:       equ     1
ENABLE_SOUND:           equ     1
ENABLE_SFX:             equ     1
ENABLE_MUSIC:           equ     1
ENABLE_HISCORE:		equ	1
                        ELSE
DEBUG_BG_ENABLE:        equ     1
FLOPPY_READ:            equ     0       ; Set to true to read files from the floppy in DF0, otherwise DH1:Rygar/[FILE]
ENABLE_OCEAN_LOADER:    equ     0       ; Set to true to use Ross's Ocean Loader instead of AmigaDOS
                                        ; Background assets for rounds will always use the Ocean Loader from DF0.

ENABLE_NOTES_SCREEN     equ     0
ENABLE_TITLE_SCREEN:    equ     0      ;
ENABLE_INTRO_SCREEN:    equ     0       ;
ENABLE_HISCORE:		equ	0
ENABLE_GRADIENTS:       equ     1
ENABLE_SOUND:           equ     1
ENABLE_SFX:             equ     1
ENABLE_MUSIC:           equ     1

                        ENDC

DEBUG_ENABLE_TEXT_WORD_1:       equ     1
DEBUG_ENABLE_TEXT_WORD_2:       equ     1
DEBUG_ENABLE_TEXT_WORD_3:       equ     1
DEBUG_ENABLE_TEXT_WORD_4:       equ     1
DEBUG_ENABLE_TEXT_WORD_5:       equ     1
DEBUG_ENABLE_TEXT_WORD_6:       equ     1
DEBUG_ENABLE_TEXT_LWORD_1:      equ     1
DEBUG_ENABLE_TEXT_LWORD_2:      equ     1
DEBUG_ENABLE_TEXT_LWORD_3:      equ     1
DEBUG_ENABLE_TEXT_LWORD_4:      equ     1
DEBUG_ENABLE_TEXT_LWORD_5:      equ     1
DEBUG_ENABLE_TEXT_LWORD_6:      equ     1



DEBUG_COLLISION_BITMAP: equ     0       ; Display collisions bitmap when scrolling
DEBUG_COLLISION_OVERLAY equ     0       ; Overlay collision bitmap when scrolling

TILE_UPPER_COLLCHECK:   equ     5       ; This must be 5 and all sprites cannot move faster than 6 pixels
                                        ; on the Y axis between any frame.

ENABLE_BACKGROUND:      equ     1

ASSETS_32X_START:       equ     0

HW_ASSETS_Y:            equ     18
HW_ASSETS_X:            equ     10
HW_ASSETS_SIZE:         equ     32
HW_ASSETS_BITPLANES:    equ     4
HW_ASSETS_LINE:         equ     HW_ASSETS_X*4

_LVOOldOpenLibrary  equ -408
;_LVOCloseLibrary    equ -$19E
_LVOWaitPort        equ -$180
_LVOGetMsg          equ -$174
_LVOReplyMsg        equ -$17A
_LVOForbid          equ -132
_LVOLock            equ -$54
_LVOUnLock          equ -$5a
_LVOParentDir       equ -$d2
_LVOCurrentDir      equ -$7e
_LVOInfo            equ -114

ThisTask            equ $114
;pr_CLI              equ $ac
;pr_MsgPort          equ $5c
sm_ArgList          equ $24
;cli_CommandName     equ $10
;SHARED_LOCK         equ -2
;id_UnitNumber       equ 4

COPPER_WAIT:            equ     26

Execbase:               equ     4

MENU_TEXT       MACRO

                ds.b    256                                     ; 3200
STACK:


        ENDM
	
GAMENOTES_TEXT	MACRO
NOTES_TELETYPE_TEXT:
	dc.b	"    SEISMIC MINDS PRESENTS...   "
	dc.b	"   RYGAR! - LEGENDARDY WARRIOR  "
	dc.b	"              V1.7              "
	dc.b	" THIS GAME IS PUBLIC DOMAIN FOR "
	dc.b	" THE COMMODORE AMIGA A1200/4000 "
	dc.b	"                                "
	dc.b	"        --- CREDITS ---         "
	dc.b	"CODE: MCGEEZER  AUDIO: DJ METUNE"
	dc.b	"DISK CODE: ROSS  MUSIC CODE: PHX"
	dc.b	"TILES: INVENT   RIPS: DLFRSILVER"
	dc.b	"    TESTING:  DAMIEND MALKO     "
	dc.b	"                                "
	dc.b	"                                "
	ds.b	32
	even

	ENDM
	

	
VERSION	MACRO
TEXT_START_GAME	dc.b	$00,"START GAME    ",0
		even
	ENDM

        cseg

        basereg data,a4
        near    a4

CODE_BASE:

ROUTEID	MACRO
	move.w	DEBUG_ROUTEID2(a4),DEBUG_ROUTEID3(a4)
	move.w	DEBUG_ROUTEID1(a4),DEBUG_ROUTEID2(a4)
	move.w	DEBUG_ROUTEID0(a4),DEBUG_ROUTEID1(a4)
	ENDM
	
FUNCID MACRO
	IFNE	DEBUG_FUNCTIONS
	nop
	move.l	a6,DEBUG_SCRATCHREG
	lea	DEBUG_SAVEREGS,a6
	movem.l	d0-d7/a0-a5,(a6)
	move.l	DEBUG_SCRATCHREG,a6
	move.l	DEBUG_FUNCID2,DEBUG_FUNCID3
	move.l	DEBUG_FUNCID1,DEBUG_FUNCID2
	move.l	DEBUG_FUNCID0,DEBUG_FUNCID1
	move.l	\1,DEBUG_FUNCID0
	ENDC
	
	ENDM
	
CRASH MACRO
	lea	oncrash_savea5(pc),a6
	move.l	a5,(a6)
	lea	oncrash(pc),a5
	movem.l	d0-d7/a0-a3,(a5)
	move.l	oncrash_savea5(pc),a5
	ENDM

	CNOP	0,4

RYGAR:


****************************************************************************

ENTRY:		
	IFEQ	ENABLE_MULTIDRIVE
		bra     	MAIN
	ENDC
		
	movea.l $4.w,a6
	lea 	doslibrary(pc),a1
	jsr 	_LVOOldOpenLibrary(a6)
	movea.l d0,a5           ; dosbase

	movea.l ThisTask(a6),a3
	move.l  pr_CLI(a3),d3   ; d3 = CLI or WB (NULL)
	bne.s   _from_cli

_from_wb:
; Get startup message if we started from Workbench

	lea 	pr_MsgPort(a3),a0
	jsr 	_LVOWaitPort(a6)    ; wait for a message
	lea 	pr_MsgPort(a3),a0
	jsr 	_LVOGetMsg(a6)      ; then get it
	movea.l d0,a3           ; a3 = WBStartup message
	movea.l sm_ArgList(a3),a0
	move.l	(a0),d5         ; (wa_Lock) FileLock on program dir
	exg	a5,a6               ; _dos
	bsr.s	_common

; Reply to the startup message

	jsr 	_LVOForbid(a6)      ; it prohibits WB to unloadseg me
	lea 	(a3),a1
	jmp 	_LVOReplyMsg(a6)    ; reply to WB message and exit

_from_cli:
; Get FileLock via command name if we started from CLI

	link    a3,#-256

; Copy BCPL string to C-style string

	lea 	(sp),a1
	lsl.l   #2,d3
	movea.l d3,a0
	move.l  cli_CommandName(a0),a0
	adda.l  a0,a0
	adda.l  a0,a0
	move.b  (a0)+,d0

.c:	move.b  (a0)+,(a1)+
	subq.b  #1,d0
	bne.b   .c
	clr.b   (a1)

; Get a lock on the program and its parent

	exg 	a5,a6               ; _dos
	move.l  sp,d1           ; d1 = STRPTR name (command string)
	moveq   #SHARED_LOCK,d2 ; d2 = accessMode
	jsr 	_LVOLock(a6)
	move.l  d0,d7
	move.l  d0,d1
	jsr 	_LVOParentDir(a6)
	move.l  d7,d1
	move.l  d0,d3           ; d3 = Lock on CLI program dir
	move.l  d0,d5           ; d5 = common Lock
	jsr 	_LVOUnLock(a6)

	unlk    a3

_common:	
	move.l  d5,d1
	jsr 	_LVOCurrentDir(a6)  ; CD to the program dir
	move.l  d0,d4           ; d4 = initial launch directory

	lea 	infodata(pc),a0
	lea 	id_UnitNumber(a0),a2
	move.l  d5,d1
	move.l  a0,d2
	jsr 	_LVOInfo(a6)
	tst.l   d0
	beq.b   _rts

	move.l  (a2),d0         ; d0 = current floppy device for the main exe

	movem.l d3/d4/a3/a5/a6,-(sp)
	bsr	MAIN
	movem.l (sp)+,d3/d4/a3/a5/a6
	move.l  d3,d1           ; UnLock program dir or zero (from WB)
	jsr 	_LVOUnLock(a6)

	move.l  d4,d1           ; CD to the initial directory
	jsr 	_LVOCurrentDir(a6)

	lea 	(a6),a1
	lea 	(a5),a6
	jsr 	_LVOCloseLibrary(a6)
	moveq   #0,d0
_rts:	rts

doslibrary:  	dc.b    'dos.library',0

		CNOP	0,4
	    
infodata:    	ds.l    9
			
DEBUG_EXCEPTION	dc.w	0

		CNOP	0,4
		
		IFNE	DEBUG_FUNCTIONS
DEBUG_SAVEREGS:		ds.l	16
DEBUG_SCRATCHREG:	dc.l	0
DEBUG_FUNCID0:		dc.l	0
DEBUG_FUNCID1:		dc.l	0
DEBUG_FUNCID2:		dc.l	0
DEBUG_FUNCID3:		dc.l	0
DEBUG_FUNCID4:		dc.l	0
DEBUG_FUNCID5:		dc.l	0
DEBUG_FONTAS:		dc.l	0
DEBUG_FONT_ASSET:	dc.l	0
DEBUG_IMG_TILES:	dc.l	0
DEBUG_IMG_SPRITES:	dc.l	0
DEBUG_HW16_SPRITES:	dc.l	0
		ENDC
	
        include i/custom.i              ; Custom Register definitions
        include i/cia.i                 ; CIA Hardware definitions
        include 'exec/types.i'
	include 'exec/exec.i'
	include 'libraries/dos.i'
	include 'libraries/dosextens.i'
        include 'hardware/blit.i'
        include 'hardware/dmabits.i'
        include s/ocean_loader.asm
        include s/ptplayer.asm
        include s/unpack.asm
        include s/macros.asm	
        include s/const.asm
        include s/system.asm
        include s/interupts.asm
GAMECODE_START:
        include s/setup.asm
        include s/display.asm
        include s/copper.asm
        include s/memory.asm
        include s/input.asm
        include s/sprite.asm
        include s/scroll_vertical.asm
        include s/scroll_horizontal.asm
        include s/background.asm
	include s/round.asm
        include s/enemy.asm
        include s/rygar.asm
        include s/hwspr.asm
        include s/music.asm
        include s/tile.asm
        include s/text.asm

        include s/diskarm.asm
        include s/item.asm
        include s/stone.asm
        include s/panels.asm
        include s/powers.asm
        include s/ropeline.asm
        include s/sanctuary.asm
	include s/hitpoint.asm
	include s/tables.asm
	include s/bonus.asm
	include s/hiscore.asm
	include s/endseq.asm
        include s/round_events.asm
        include s/menu.asm
        include s/intro.asm
        include s/gamenotes.asm
	include s/loadinfo.asm
        include s/caption.asm
        include s/colour.asm
        include s/score.asm
        include s/loader.asm

	IFNE	DEBUG_TEXT_ENABLE
        include s/debug.asm
	ENDC
        
	include s/round5_events.asm
        include s/round6_events.asm
        include s/round9_events.asm
        include s/round11_events.asm
	include s/round17_events.asm
	include s/round18_events.asm
	include s/round23_events.asm
	include s/round24_events.asm
	include s/round30_events.asm
        include s/enemies/drone.asm
        include s/enemies/rhino.asm
        include s/enemies/giantdemon.asm
        include s/enemies/dragon.asm
        include s/enemies/griffin.asm
        include s/enemies/lavaman.asm
        include s/enemies/lavaspit.asm
        include s/enemies/squirrel.asm
        include s/enemies/skeleton.asm
        include s/enemies/evilcomes.asm
        include s/enemies/bigrhino.asm
        include s/enemies/giantworm.asm
        include s/enemies/rider.asm
	include s/enemies/monkey.asm
        include s/enemies/tribesman.asm
        include s/enemies/fireball.asm
	include s/enemies/cavebat.asm
	include s/enemies/rocket.asm
	include s/enemies/crab.asm
	include s/enemies/ape.asm
	include s/enemies/rock.asm
	include s/enemies/mutantfrog.asm
	include s/enemies/lizard.asm
	include s/enemies/liztongue.asm
	include s/enemies/villager.asm
	include s/enemies/testsprite.asm
	include s/enemies/shield.asm
	include s/enemies/special.asm
	include s/enemies/hammer.asm
	include s/enemies/reaper.asm
	include s/enemies/fish.asm
	include s/enemies/weapon.asm
	include s/enemies/ligar.asm
	include s/enemies/crowd.asm
	include s/enemies/elder.asm

MAIN:	
	IFNE	DEBUG_FUNCTIONS
	move.l	#RYGAR,DEBUG_CODE_POINTER				; 100
	move.l	#DEBUG_FUNCID0,DEBUG_CODE_POINTER+4			; 104
	move.l	#FONT_ASSET,DEBUG_CODE_POINTER+8			; 108
	ENDC

	bsr	BASE_REGISTERS
	FUNCID	#$88256c6e
        bsr     GETVBR
        move.l  d0,VBRBASE(a4)
        move.w  VHPOSR(a5),RANDOM_SEED(a4)

; ---------------- Boot Strap Start ---------------
        bsr     OPEN_DOSLIB
        moveq   #ERR_OPEN_DOSLIB,d2
        beq     HANDLE_ERROR
        move.l  d0,DOSBASE(a4)

        bsr     ALLOCATE_MEMORY
        tst.w   d5
        bpl     .alloc_ok

        bsr     DEALLOCATE_MEMORY
        moveq   #ERR_ALLOC_MEM,d2
        bra     HANDLE_ERROR

.alloc_ok:
	bsr	GENERATE_BYTE_SWAP_TABLE
	
	bsr	CLEAR_BUFFERS

        bsr     LOADER
        tst.l   d0
        bmi     HANDLE_ERROR

	btst.b	#6,$bfe001
	bne	.skip_debug

	DISPLAY_GAME_POINTERS

.skip_debug:


	move.w	#COPRUN_LOADER,COPRUN_TYPE(a4)
	
	IFNE	DEBUG_FUNCTIONS
	move.l	FONT_ASSET,DEBUG_FONT_ASSET
	move.l	IMG_TILES,DEBUG_IMG_TILES
	move.l	IMG_SPRITES,DEBUG_IMG_SPRITES
	move.l	HW16_SPRITES,DEBUG_HW16_SPRITES
	ENDC
	
; --------------- Boot Strap End ------------------
        IFEQ    ENABLE_DEBUG
	bsr	InstallExceptionHandlers
        bsr     SAVE_DMA
        bsr     SAVE_COPPER
        bsr     DISABLE_OS
        move.w  #$7fff,INTENA(a5)
	move.w  #%0000000110000000,DMACON(a5)		
        move.w	#0,BPLCON0(a5)
        move.w	#0,BPLCON1(a5)
        move.w	#0,BPLCON2(a5)
	ENDC

        bsr     CLEAR_BUFFERS
        bsr     ALIGN_64

        IFNE    DEBUG_COLLISION_OVERLAY
        move.l  LSTPTR_MAPCOLLISION(a4),LSTPTR_BITPLANES(a4)
        ENDC

        move.l  RYGAR_MOUNTAINS(a4),a0
        bsr     GET_IFF_BODY
        move.l  a0,STCPTR_MOUNTAINS(a4)

        IFNE    ENABLE_SFX
        bsr     LOAD_SAMPLES
        ENDC

; Install Interupts	
	bsr     INSTALL_MUSIC_PLAYER

        IFEQ    ENABLE_DEBUG
	LEVEL3_START
        ENDC
	
	

;--------------------------------------
; HISCORE DEBUGGING

	;move.l	#200000,VAL_PLAYER1_SCORE
	;move.w	#23,ROUND_CURRENT(a4)
	;bsr	HISCORE
	
	
	
; Load the first round into the game.
	IFNE	FAST_LOAD
	move.w  #START_ROUND-1,ROUND_CURRENT(a4)
        ENDC
	bsr     LOAD_ROUND                      ; Load first round
	
	
; Set default menu options
	move.w	#$0,MENU_OPTIONS_SEL(a4)		; Force menu to be drawn.
	clr.w	ROUND_ADVANCE(a4)
	clr.w	SHOWED_NOTES(a4)
	move.w	#ENABLE_CHEAT,ENABLE_CHEATS(a4)
	move.w	#START_TIME_BEGINNER,TIME_REMAINING(a4)
	move.w	#START_TIME_BEGINNER,START_TIME(a4)
	
	bsr	CLEAR_FRONT_SCREEN_BUFFERS
	bsr	CLEAR_BACK_SCREEN_BUFFERS
	
.start_game:
	;clr.w	QUIT_GAME(a4)
	move.w	#1,QUIT_GAME(a4)	
	move.w	#1,PAUSE_GAME(a4)
        bsr     INIT_SANCTUARY_VARS

	move.w	#CAPTION_TYPE_LETSFIGHT,CAPTION_TYPE(a4)
        clr.w   RYGAR_CURRENT_POWERS(a4)
	clr.w	EXTRAS_POSITION(a4)
	
	move.w  #START_LIVES,RYGAR_LIVES(a4)
	move.w  #START_ROUND-1,ROUND_CURRENT(a4)
	
        clr.w   RYGAR_LIFE_LOST(a4)
        clr.w   RYGAR_CURRENT_POWERS(a4)
        clr.w   RYGAR_DEAD_STATE(a4)
	clr.w	RYGAR_THE_END(a4)
	clr.w	RYGAR_CURRENT_RANK(a4)
	
        IFNE    ENABLE_NOTES_SCREEN
        tst.w   SHOWED_NOTES(a4)
        bmi.s   .display_game_notes
	move.b	#0,NOTES_CHAR_DELAY(a4)
	lea     NOTES_TELETYPE_TEXT(a4),a0
        move.l	a0,NOTES_MESSAGE_PTR(a4)
	bsr     NOTES
        move.w  #-1,SHOWED_NOTES(a4)
.display_game_notes:
        ENDC

        IFNE    ENABLE_TITLE_SCREEN             ; Jump to title
        bsr     INIT_BITPLANE_POINTERS
        bsr     INIT_DISPLAY_POINTERS
	bsr     MENU
        bsr     CLEAR_BUFFERS
        ENDC
	
	IFNE	FAST_LOAD
	tst.w	ROUND_CURRENT(a4)
	beq.s	.already_loaded_round_1
        ENDC
	bsr     LOAD_ROUND                      ; Load first round
.already_loaded_round_1:
	
        IFNE    ENABLE_INTRO_SCREEN
        bsr     INTRO                           ; Show 4.5 mountain scene
        bsr     CLEAR_BUFFERS
        ENDC
	
	tst.w	ROUND_ADVANCE(a4)			; Has a pass code been entered?
	bmi.s	.advance
	move.w  #START_ROUND-1,ROUND_CURRENT(a4)
.advance:
	

	
	move.w	#-1,SHIELD_TIMER(a4)
        clr.l   VAL_PLAYER1_SCORE(a4)
        ;move.w  #-1,UPDATE_SCORE(a4)
        
	bsr     INIT_BITPLANE_POINTERS
        bsr     INIT_DISPLAY_POINTERS

        bsr     INIT_TILE_ATTRIBUTES
        bsr     CREATE_SPRITE_SLOTS
	clr.w	COPPER_ACTIVE(a4)

	IFNE	DEBUG_TEXT_ENABLE
        bsr     DEBUG_TEXT
	ENDC

        move.w  POTGOR(a5),OLD_POTGO(a4)
        clr.w   FRAME_BG(a4)

        move.w  #DEBUG_BG_ENABLE,BACKGROUND_ENABLE(a4)
        clr.w   DEBUG_PANRIGHT_DONE(a4)

        bsr     RESET_ROUND			; INIT_COPPER1 in here.
	
	clr.w	DISABLE_ENEMIES(a4)
	
        move.l  COPPTR_BPLCON(a4),a0
        lea     CHIPBASE,a5
        move.w  #$0211,2(a0)                            ; Enable Bitplanes
        bsr     DBUFF
	
	bsr	CREATE_BONUS_TEXT
	
	bsr     PAN_RIGHT


	move.w	#$c000,$dff034		; Reset POTGO

	bsr	INIT_ROUND_VARS
	
.wait:

        bsr     WAIT_FOR_VERTICAL_BLANK	
	tst.w	QUIT_GAME(a4)
	bmi	.start_game
	
	tst.w	RYGAR_THE_END(a4)
	bmi	.the_end
	
        add.l   #1,FRAME_COUNTER(a4)
        addq.w  #1,FRAME_BG(a4)
	
	bsr	UPDATE_COPPER_SPRITES			; < Causes the timer problem.
	
	bsr	CHECK_DIRECTION_CHANGE
	bsr     PAN_CAMERA_POSITION
        bsr     CHECK_EVENTS
	bsr	DISPLAY_BONUS_TEXT
	bsr	LOAD_TILE_MERGE_BUFFERS

; Autoscroll to the right when in the sanctuary.
        tst.w   AUTO_SCROLL_RIGHT(a4)
        beq.s   .scroll_done
        subq.w  #1,AUTO_SCROLL_RIGHT(a4)
        bsr     PAN_RIGHT

.scroll_done:
        ;COL0_RED
        IFNE    DEBUG_PANRIGHT
                tst.w   DEBUG_PANRIGHT_DONE(a4)
                bmi.s   .pan_done1
                cmp.w   #DEBUG_PANRIGHT_TO,MAP_PIXEL_POSX(a4)
                bge.s   .pan_done
                bsr     PAN_RIGHT
                bra.s   .pan_done1
.pan_done:
                move.w  #-1,DEBUG_PANRIGHT_DONE(a4)
                COL0_BLUE

                nop
.pan_done1:
        ENDC

        bsr     SANCTUARY
        bsr     DBUFF
	lea     LSTPTR_OFF_SCREEN(a4),a3
        bsr     COPY_BG_TO_SCREEN

; Update score if needed.
        tst.w   UPDATE_SCORE(a4)
        beq.s   .skip_score_update
        clr.w   UPDATE_SCORE(a4)
        bsr     UPDATE_PLAYER_SCORE

.skip_score_update
        bsr     RESTORE_ALL_SPRITES
        bsr     REST_DISKARM_BACKGROUND
	bsr	REST_HITPOINT_BACKGROUND
	bsr	REST_BONUS_TEXT
        bsr     BLIT_TILE_ONSCROLL_X
        bsr     BLIT_TILE_ONSCROLL_Y
        bsr     ANIMATE_SPRITES
        bsr     HDL_ROPE
        bsr     HDL_STONES
        bsr     HDL_ITEMS
        bsr     HDL_DISKARM
        bsr     PLOT_ALL_SPRITES
	bsr	HDL_HITPOINT
	bsr	HDL_BONUS_TEXT
        bsr     SPAWN_ENEMIES
	bsr	UPDATE_ROUND_GUIDE
	bsr	CYCLE_COLOURS
	bsr	UPDATE_TIMER
	bsr	CYCLE_WATERFALLS
	
.lmb_rmb
        btst    #6,$bfe001
        bne     .wait
	btst	#7,$bfe001
	bne	.wait
; Insert Mouse Events in here.

; Cause a round reset and life to be lost
	bsr 	SET_LAST_MAP_POSITION
	move.w	#-1,RYGAR_LIFE_LOST(a4)
        bra     .wait

        move.w  OLD_POTGO(a4),POTGO(a5)

        IFEQ    ENABLE_DEBUG
        bsr     OS_UP
        ENDC

        moveq   #NUMBER_OF_CHUNKS,d7
        bsr     DEALLOCATE_MEMORY
.end:
        moveq   #0,d0
        rts
	
.the_end:
	bsr     SET_ALL_ENEMIES_OFFSCREEN
        bsr	PLOT_ALL_SPRITES
	bsr     UPDATE_COPPER_SPRITES
	
	bsr	DESTROY_ALL_SPRITES
	
	moveq	#MUSIC_GAMEOVER,d0
	bsr	PLAY_TUNE
	
	bsr	TRANSITION_TO_BLACK
	and.w	#$ffe0,MAP_PIXEL_POSX(a4)

	move.w	#CAPTION_TYPE_THE_END,CAPTION_TYPE(a4)
	move.l  #CAPTION_PAUSE*3,d7
.caption_delay_blank:
        move.l  d7,-(a7)
        bsr     WAIT_FOR_VERTICAL_BLANK
	tst.w	QUIT_GAME(a4)
	bmi	.start_game
	bsr	DBUFF
        move.l  LSTPTR_CURRENT_SCREEN(a4),a1
        bsr     BLIT_CAPTION
        move.l  (a7)+,d7
        dbf     d7,.caption_delay_blank
 
	bsr	TRANSITION_TO_BLACK
; Now show thanks for playing.

.the_end_text:  
	lea     THANKS_FOR_PLAYING_TEXT(a4),a0
        move.l	a0,NOTES_MESSAGE_PTR(a4)
	bsr	NOTES
	
	bsr	STOP_TUNE
	bsr	HISCORE			; hi-score entry
	bra	.start_game

HANDLE_ERROR:
	FUNCID	#$f1d61bc7
        btst.b  #6,$bfe001
        bne.s   HANDLE_ERROR
        moveq   #-1,d0
        rts
	
BASE_REGISTERS:
	lea	data,a4
	lea	CHIPBASE,a5
	rts
	
GAMECODE_END:
	
        dseg
	



PALETTE_TILES:
	incbin	palettes/tiles30.col
	even


;;; Keep $8000 close to data:
dats:

TESTMAP_POINTER:	
	dc.l	TEST_TILEMAP
        
	include s/sprite.dat
        include s/copper.dat
        include s/colour.dat
        include s/scroll.dat
        include s/round.dat             ; 1c58 / 7256
        include s/diskarm.dat           ; 1cec / 7404
        include s/tables.dat            ; ab08 ; 43778
        include s/memory.dat
RANDOM_SEED:    dc.w    0
FIRECOLOUR:     dc.w    0
ROCK_IN_FLIGHT:	dc.w	0
QUIT_GAME:	dc.w	0
PAUSE_GAME:	dc.w	0

		CNOP	0,4
data:
	ds.l	16

	include s/loadinfo.dat
        include s/background.dat
        include s/intro.dat
        include s/system.dat
        include s/display.dat
        include s/input.dat
        include s/item.dat              ; 604 / 1540
        include s/stone.dat
        include s/gamenotes.dat
        include s/caption.dat
        include s/loader.dat
        include s/hwspr.dat
        include s/music.dat
        include s/sanctuary.dat
        include s/enemy.dat
        include s/score.dat
        include s/tile.dat
        include s/menu.dat
	include s/hitpoint.dat
        include s/panels.dat
        include s/ropeline.dat
        include s/sine.dat
	include s/bonus.dat
	include s/hiscore.dat
SPRITE_DEF_BASE:
        include s/rygar.dat
        include s/carousel.dat
        include s/enemies/drone.dat
        include s/enemies/evilcomes.dat
        include s/enemies/rhino.dat
        include s/enemies/dragon.dat
        include s/enemies/lavaman.dat
        include s/enemies/lavaspit.dat
        include s/enemies/squirrel.dat
        include s/enemies/giantdemon.dat
        include s/enemies/giantworm.dat
        include s/enemies/skeleton.dat
        include s/enemies/griffin.dat
        include s/enemies/bigrhino.dat
        include s/enemies/rider.dat
	include s/enemies/monkey.dat
        include s/enemies/tribesman.dat
        include s/enemies/ape.dat
        include s/enemies/fireball.dat
	include s/enemies/cavebat.dat
	include s/enemies/rocket.dat
	include s/enemies/crab.dat
	include s/enemies/rock.dat
	include s/enemies/mutantfrog.dat
	include s/enemies/lizard.dat
	include s/enemies/liztongue.dat
	include s/enemies/villager.dat
	include s/enemies/testsprite.dat
	include s/enemies/shield.dat
	include s/enemies/special.dat
	include s/enemies/hammer.dat
	include s/enemies/reaper.dat
	include s/enemies/fish.dat
	include s/enemies/weapon.dat
	include s/enemies/ligar.dat
	include s/enemies/crowd.dat
	include s/enemies/elder.dat
        include s/assets.asm
	
	IFNE	DEBUG_TEXT_ENABLE
	include s/debug.dat
	ENDC
	
DJMETUNE_MESSAGE MACRO
	dc.b 	1," AND NOW A MESSAGE FROM "
	dc.b	3," DJ METUNE   "
	dc.b	2," THIS SUPERB AMIGA PORT OF RYGAR WAS MY FIRST BUT HOPEFULLY NOT LAST"
	dc.b	2," CO-OPERATION WITH MCGEEZER AND ALSO AN OPPORTUNITY FOR ME TO INTRODUCE"
	dc.b	2," MYSELF A LITTLE BIT MORE TO THE AMIGA COMMUNITY, AS A MUSICIAN AND SOUND"
	dc.b	2," DESIGNER.   MY GOAL WAS SET TO REPRODUCE THE DEVINE ORIGINAL ARCADE MUSIC"
	dc.b	2," IN THE MOST FAITHFUL AND BEST WAY POSSIBLE WHILE NOT EASTING ALL THE CHIP RAM! "
	dc.b	2," I THINK I DID SUCCEED TO GIVE A GOOD EXAMPLE OF THE INCREDIBLE LEVELS OF "
	dc.b	2," QUALITY THAT THE AMIGA PAULA SOUND CHIP DOES HAVE.  IT IS AMAZING HOW GOOD"
	dc.b	2," IT CAN SOUND, DESPITE ITS AGE WITH TECHNOLOGY OF 1985! IT IS DIFFICULT TO"
	dc.b	2," DRAW ANY CLEAR LINE ON WHAT IS POSSIBLE OR NOT THOUGH AS THE AMIGA DOES NOT GIVE A"
	dc.b	2," DIME IF THE SOUND IS COMING FROM AN ARCADE FM SYNTH OF THE 1980S OR THE"
	dc.b	2," LATEST SCREAMS OF 2019!    IT IS AS IF WE ONLY GIVE IT THE RIGHT INGREDIENTS,"
	dc.b	2," COMMANDS, DEDICATION, TIME, SOUL AND CHIPMEM, THE AMIGA IS HAPPY TO CLIMB"
	dc.b	2," JUST ANY MOUNTAIN!"
	dc.b	2," I AM SO DEPLY IMPRESSED BY WHAT MCGEEZER HAS ACCOMPLISHED, AS THE MAIN"
	dc.b	2," BRAINS BEHIND ALL OF THIS. I CAN HARDLY BELIEVE MY EYES HOW GOOD THIS"
	dc.b	2," CONVERSION OF RYGAR IS!!!!  I WOULD ALSO LIKE TO TAKE THIS OPPORTUNITY TO BRING A TOAST"
	dc.b	2," TO YOU ALL OTHER AWESOME SPECIALISTS AND MAGICIANS WHO HAS BEN FOUGHT FOR"
	dc.b	2," THIS CONVERSION TO BE AS GOOD AS POSSIBLE. ESPECIALY INVENT AND DLFRSILVER."
	dc.b	2," CHEERS !!!!     "
	dc.b	3," -DJ METUNE   "
	ENDM

DLFRSILVER_MESSAGE MACRO
	dc.b 	1," AND NOW A MESSAGE FROM "
	dc.b	3," DLFRSILVER "
	dc.b	2," WELL, THIS IS THE FIRST GAME DEVELOPMENT THAT I AM PART OF FOR A COMPUTER."
	dc.b	2," I HAVE ENJOYED BRINGING THE ARCADE ASSETS IN THEIR FULL GLORY VIA"
	dc.b	2," THE REVERSE ENGINERING OF THE ARCADE VERSION ASSETS, BY READING THE ROMS,"
	dc.b	2," PICKING THE PALLETES FROM MAME AND THEN RECOLORING BY HAND WITH IRFANVIEW,"
	dc.b	2," IN A FORMAT COMPATIBLE WITH THE AMIGA. THIS MEANT SPRITES MADE OF 16X16"
	dc.b	2," PIXELS INSTEAD OF 8X8 PIXELS AS THE AMIGA IS NOT READILY COMPATIBLE WITH THIS TILE"
	dc.b	2," FORMAT. I SPENT 1 WEEK ALL IN ALL TO GET THOSE"
	dc.b	2," EXTRACTED AND THEN I SPENT MOST OF THE TIME SEPERATING THE SPRITES"
	dc.b	2," FROM EACH OTHER, CHECKING THAT NOTHING WAS FORGOTTEN."
	dc.b	2," MY GOAL ON THIS PROJECT WAS TO GIVE THE BEST AND THE MOST COMPLETE SETS"
	dc.b	2," OF GRAPHICS TO MCGEEZER SO THAT THE IMPLEMENTATION COULD BE VISUALY"
	dc.b	2," THE BEST POSSIBLE. I HAVE TO THANK INVENT FOR PROCESING BEFORE HAND THE"
	dc.b	2," GRAPHICS I SENT, BY REDUCING COLOURS A LITTLE AND ALSO FOR MAKING ALL THE"
	dc.b	2," LEVELS FROM THE TILES EXTRACTED. I SALUTE ALSO DJ METUNE FOR THE AWESOME"
	dc.b	2," MUSICAL AND SFX RENDITION OF THE ARCADE ORIGINAL."
	dc.b	2," RYGAR IS THEREFORE EXACTLY HOW WE MEANT IT TO BE. THANKS TO MCGEEZER"
	dc.b	2," INCREDIBLE WORK AND CODING SORCERY, THE A1200 SHOWS ITS TRUE COLOURS."
	dc.b	2," IT IS IN FACT PERFECTLY ABLE TO MAKE GREAT ARCADE PORTS, ABOVE WHAT WE KNOW"
	dc.b	2," ON THE AMIGA 500. A MILESTONE HAS BEEN SET, THANKS TO THE TEAM FOR THIS"
	dc.b	2," GREAT ACCOMPLISHMENT, BE PROUD GUYS! WE ALL DESERVE IT!   "
	dc.b	3," DLFRSILVER 09/2019 "
	ENDM
	
INVENT_MESSAGE MACRO
	dc.b 	1," AND NOW A MESSAGE FROM "
	dc.b	3," INVENT   "
	dc.b	2," IT HAS BEEN A REAL PLEASURE BEING PART OF THIS GREAT ARCADE CONVERSION TO OUR"
	dc.b	2," FAVOURITE PLATFORM.  MY INVOLVEMENT WAS MAINLY CONVERTING THE GRAPHIC TILES" 
	dc.b	2," INTO MAPS. WHILE DOING THIS"
	dc.b	2," I DID NOTICE A FEW AREAS OF THE MAP THAT HAD A FEW GRAPHIC GLITCHES BUT AS"
	dc.b	2," I WANTED THE MOST AUTHENTIC DESIGN I KEPT THEM IN THE GAME. I MANUALY PLACED"
	dc.b	2," EACH TILE INTO THEIR CORRECT POSITION USING THE TILED MAP EDITOR. INSIDE THE MAP EDITOR"
	dc.b	2," I HAD THE ARCADE LEVELS AS A TRANSPARENT LAYER AND OVERLAYED THE AMIGA ONES."
	dc.b	2," WHILE IT PROBABLY TOOK OVER A WEEK TO DO AND RE-CHECK, IT WAS WELL WORTH THE EFFORT,"
	dc.b	2," I THEN SENT MCGEEZER THE MAPS OF 27 ROUNDS.  WE DID ALSO CRAM IN A FEW MORE "
	dc.b	2," NEW TILES FOR EXTRA BONUS ROUNDS, AGAIN I REUSED PARTS OF THE CURRENT TILES"
	dc.b	2," FOR EXAMPLE THE SKULLS ONTO THE GREEN VINES TO KEEP THE PORT AS AUTHENTIC AS POSSIBLE."
	dc.b	2," ALSO......CREATING THE BOX ART AND MANUAL WAS A REAL JOY."
	dc.b	2," EVERYONES CONTRIBUTION IN THE TEAM HAS MADE THIS PORT ONE VERY SPECIAL ONE, "
	dc.b	2," DLFRSILVER,S AMAZING REVERSE ENGINERING OF THE ARCADE ASSETS, MCGEEZER VERY TALENTED "
	dc.b	2," CODING ABILITIES AND DJ METUNE,S SKILL CREATING THE MUSIC AND SOUND FX. "
	dc.b	2," I WOULD EASILY WORK ON ANOTHER PORT IF ANY OF THESE GUYS DECIDE TO IN FUTURE. "
	dc.b	2," I MUST ALSO THANK THE THOSE WHO HAS PLAYED AND TESTED THE GAME TO IRON OUT ANY "
	dc.b	2," BUGS OR ISSUES. "
	dc.b	2," I WILL CERTAINLY KEEP AN EYE OUT FOR ANY FUTURE PORTS THE TEAM MIGHT BE INTERESTED IN. "
	dc.b	3," INVENT - KEVIN SAUNDERS"
	ENDM
	
MCGEEZER_MESSAGE MACRO
	dc.b 	1," AND NOW A MESSAGE FROM "
	dc.b	3," MCGEEZER"

	dc.b	2," RYGAR AGA STARTED DEVELOPMENT ON 31ST JULY 2018 FOR THE ENGLISH AMIGA BOARD GAME COMPETITION."
	dc.b	2," THE DEVELOPMENT COMPLETED ON OR AROUND 28TH SEPTEMBER 2019 SO THE WHOLE GAME TOOK ROUGHLY"
	dc.b	2," AN ENTIRE YEAR TO COMPLETE.  THIS PROJECT WOULD NOT HAVE BEEN POSSIBLE WITHOUT THE"
	dc.b	2," MOTIVATION THAT I RECIEVED FROM THE GUYS AT THE ENGLISH AMIGA BOARD.  "
	dc.b	2," ALSO LOVE TO MY WIFE AND SON - CLARE AND LEO!    "
	dc.b	2," NOW THEN LETS SEE...   IF I HAD MY CHOICE I WOULD REWRITE A HUGE PORTION OF THIS GAME "
	dc.b	2," ALOT OF THE CODE WAS RUSHED DUE TO THE EAB GAME COMPO... "
	dc.b	2," BUT THAT IS NO BAD THING AS I WAS UNDER A TIME PRESSURE.  "
	dc.b	2," I JUST WANT TO SAY THANKS TO THE AWESOME WORK FROM THE REST OF THE TEAM THAT HELPED  "
	dc.b	2," MAKE THIS GAME POSSIBLE....  INVENTS AWESOME TILE WORK, DLFRSILVERS EXCELENT SPRITE "
	dc.b	2," EXTRACTION, ROSS AND HIS UNREAL KNOWLEDGE OF THE AMIGA ... THE GUY KEPT ME SANE "
	dc.b	2," PHX FOR AN AWESOME AUDIO DRIVER ... DJ METUNE FOR BEING AN ABSOLUTE AUDIO GENUIS "
	dc.b	2," I HONESTLY DONT KNOW HOW HE DID IT. " 

	ENDM
	
	



GREETS_SCROLLER:	
		dc.b	3,"       RYGAR AGA  "
		dc.b	1," LEGENDARY WARRIOR "
		dc.b	2," GAME PROGRAMMING: "
		dc.b	4," MCGEEZER      "
		dc.b	2," TILE MAPS:  "
		dc.b	4," INVENT    "
		dc.b	2," MUSIC:  "
		dc.b	4," DJ-METUNE   "
		dc.b	2," SPRITE RIPS:   "
		dc.b	4," DLFRSILVER   "
		dc.b	2," TRACK LOADER:  "
		dc.b	4," ROSS   "
		dc.b	2," PROTRACKER PLAYER:  "
		dc.b	4," PHX   "
		dc.b	2," PLAY TESTING BY   "
		dc.b	4," DAMIEN D AND MALKO   "
		dc.b	2," COMPATIBILITY TESTING BY:  "
		dc.b	4," PERCY FREDRIKSEN AND SABERMAN  "
		dc.b	2,"       BIG GREETS GO TO THE FOLLOWING AWESOME CHAPS RISING THE AMIGA AND BRINGING BACK THE GOOD DAYS     "
		dc.b	1," STERIL707   "
		dc.b	2," DAMIEN D   "
		dc.b	3," ALPINE9000  "
		dc.b	2," RICHARD LOWENSTEIN   "
		dc.b	1," ALPHA ONE   "
		dc.b	2," DAN SCOTT   "
		dc.b	3," JOTD   "
		dc.b	2," MALKO   "
		dc.b	2," AMIGA BILL   "
		dc.b	3," JAMIE MORGAN   "
		dc.b	2," CAMMY   "
		dc.b	1," MUSASHI9   "
		dc.b	2," PHOTON   "
		dc.b	3," GALAHAD   "
		dc.b	2," MEYNAF   "
		dc.b	1," THE AWESOME TONI WILEN          "
		DJMETUNE_MESSAGE
		DLFRSILVER_MESSAGE
		INVENT_MESSAGE
		MCGEEZER_MESSAGE
		dc.b	0
		
		even

CHEAT_SCROLLER:	
		dc.b	4,"         YOU DORTY CHEAT! "
		dc.b	1,"   INGAME KEYS...."
		dc.b	2," F1="
		dc.b	1," STAR POWER "
		dc.b	2," F2="
		dc.b	1," CROWN POWER "
		dc.b	2," F3="
		dc.b	1," TIGER POWER "
		dc.b	2," F4="
		dc.b	1," CROSS POWER "
		dc.b	2," F5="
		dc.b	1," SUN POWER "
		dc.b	2," F6="
		dc.b	1," EXTRA LIVES "
		dc.b	2," N="
		dc.b	1," NEXT ROUND "
		dc.b	2," E="
		dc.b	1," DESTROY ALL ENEMIES "
		dc.b	2," T="
		dc.b	1," EXTRA TIME "
		dc.b	2," A="
		dc.b	1," ALL POWERS NOW  "
		dc.b	4,"    CHEATERS NEVER PROSPER!   ",0
		even

TWATS_SCROLLER:	
		dc.b	1,"             "
		dc.b	1,"  LIKE IN MOST WALKS OF LIFE THERE IS ALWAYS THE ODD TWAT SO FOR AMITEN AND JOJO..."  
		dc.b	1,"  280 BYTES AND THE STEAM FROM MY PISS IS ALL YOU GET IN THIS GAME.       ",0
		even
		
TECMO_SCROLLER:	
		dc.b	1,"          "
		dc.b	1,"    I DID ASK TECMO FOR A LICENSE FOR THIS GAME...     THEY SAID NO.    ",0  
		even
		
COMPO_SCROLLER:	
		dc.b	1,"      THIS IS A MESSAGE FOR JOJO73......."
		dc.b	2,"  YOU WERE NOT INVITED TO THE EAB GAME COMPO IN 2018...."
		dc.b	2,"  TAKING PART WAS YOUR OWN DECISION..."
		dc.b	2,"  YOU CALLED THE ORGANISERS OF THE EAB GAME COMPO CHEATS...."
		dc.b	2,"  NOTHING COULD BE FURTHER FROM THE TRUTH..."
		dc.b	2,"  YOU SAID YOUR ALL VALLEY KARATE GAME WAS THE BEST IN THE COMPO..."
		dc.b	2,"  YOU ARE FUCKING DELUDED. "
		dc.b	2,"  READ MORE HERE...  "
		dc.b	3,"  HTTP://WWW.XENTE.MUNDO-R.com/JOJO073/PAGES/ALL.HTML  " 
		dc.b	1," ",0
		even
		

STATS_SCROLLER:
		dc.b	1,"  STATS:"
		dc.b	2,"  DEVELOPMENT TIME:-"
		dc.b	3," 13 MONTHS "
		dc.b	2,"  CODING HOURS:-"
		dc.b	3," 824 "  
		dc.b	2,"  ASSEMBLER LINES:-"
		dc.b	3," 19324 "
		dc.b	2," UNPACKED ASSETS:-"
		dc.b	3," 2.86MB "
		dc.b	2," PERONI CONSUMED DURING DEVELOPMENT:-"
		dc.b	3," 34,380 LITRES"
		dc.b	1,"     *** LONG LIVE THE AMIGA! ***   "
		dc.b	2,"   LOOK OUT FOR MY NEXT GAME IN 2020  "
		dc.b	0
	
TEST_TILEMAP:
ST
SE
