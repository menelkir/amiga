
        CNOP    0,4


; Rygar Sprite Animation Definitions
RYGAR_STAND_RIGHT:      equ     0
RYGAR_STAND_LEFT:       equ     1
RYGAR_RUNNING_RIGHT:    equ     2
RYGAR_RUNNING_LEFT:     equ     3
RYGAR_JUMP_RIGHT:       equ     4
RYGAR_JUMP_LEFT:        equ     5
RYGAR_CROUCH_RIGHT:     equ     6
RYGAR_CROUCH_LEFT:      equ     7
RYGAR_ATTACK_RIGHT:     equ     8
RYGAR_ATTACK_LEFT:      equ     9
RYGAR_CROUCH_ATTACK_RIGHT:      equ     10
RYGAR_CROUCH_ATTACK_LEFT:       equ     11
RYGAR_JUMP_ATTACK_RIGHT:        equ     12
RYGAR_JUMP_ATTACK_LEFT: equ     13
RYGAR_CAROUSEL:         equ     14
RYGAR_FACING:           equ     15
RYGAR_DEATH:            equ     16
RYGAR_ROLLFALL_LEFT:        equ     17
RYGAR_ROLLFALL_RIGHT:       equ     18
RYGAR_ON_ROPE:          equ     19
RYGAR_CLIMB_UP:         equ     20
RYGAR_CLIMB_DOWN:       equ     21
RYGAR_CLIMB_SWING_LEFT: equ     22
RYGAR_CLIMB_SWING_RIGHT:        equ     23
RYGAR_HEAVEN:			equ	24
RYGAR_CLIMB_ATTACK_LEFT:        equ     25
RYGAR_CLIMB_ATTACK_RIGHT:       equ     26


; Sprite States
SPRITE_INSTATE_DIRECTION_LEFT:  equ     1
SPRITE_INSTATE_DIRECTION_RIGHT: equ     0
SPRITE_INSTATE_ONPLATFORM:      equ     8
SPRITE_INSTATE_JUMPING:         equ     9
SPRITE_INSTATE_FALLING:         equ     10

RYGAR_LEFT_BARRIER:             equ     32
RYGAR_MOVE_SPEED:               equ     2
RYGAR_LEAP_DELAY:               equ     1                       ; Delay scrolling frame for when Rygar leaps

PRIVATE_RYGAR_SWING:            equ     34                      ; -1 = Swining Left / 0 = Static / 1 = Swinging Right
PRIVATE_RYGAR_SWING_SINEPTR:    equ     36                      ; Swing
PRIVATE_RYGAR_XPOS: 	        equ     38





; Private Variables
RYGAR_LAST_DIRECTION:           equ     12              ; Last direction travelled

; Test the jump button
RYGAR_JUMP_CHECK:
		FUNCID	#$837d1fd0
		
                movem.l d0-d1/a1-a2,-(a7)
		tst.w	VERTSCROLL_ENABLE(a4)
		bmi.s	.exit
                tst.w   RYGAR_LEAPING_STATE(a4)
                bmi.s   .leap_done
                subq.w  #1,RYGAR_LEAPING_STATE(a4)
.leap_done:     tst.w   RYGAR_JUMP_STATE(a4)            ; Already jumping?
                bne     .exit                           ; Yes
		tst.w	RYGAR_FALL_STATE(a4)
		bne.s	.exit
                tst.w   BTN2_STATE(a4)
                bne.s   .set_jump
                bra     .exit
; Fire button pressed
.set_jump:      tst.w   RYGAR_JUMP_STATE(a4)            ; Is it jumping already
                bne     .exit                           ; No
		bsr	RYGAR_INITIATE_JUMP
; Check here to see if we need to push a stone.
.exit:          movem.l (a7)+,d0-d1/a1-a2
                rts
		
RYGAR_INITIATE_JUMP:
		FUNCID	#$3f154f15
		
		move.w  #-1,RYGAR_JUMP_STATE(a4)                ; Only Rygar landing will unset this.
                move.w  RYGAR_YPOS(a4),RYGAR_LAST_JUMP_POS(a4)
                clr.w   RYGAR_SINE_INDEX(a4)    ; Reset Jump Sine pointer.
                move.w  #RYGAR_LEAP_DELAY,RYGAR_LEAPING_STATE(a4)
                bsr     CHECK_JUMP_STONE
                moveq   #SND_RYGARJUMP,d0               ; Play jump sample
                bsr     PLAY_SAMPLE
		rts

; Test the weapon button
RYGAR_DISKARM_CHECK:
		FUNCID	#$da35838a
		
                movem.l d0-d1/a1-a2,-(a7)
                tst.w   RYGAR_DISKARM_STATE(a4)         ; Already attacking
                bne     .exit                           ; Yes
                tst.w   BTN1_STATE(a4)
                bne.s   .set_diskarm
                bra     .exit
; Weapon button pressed
.set_diskarm:   
		clr.w	STATE_KILL_ALL_ENEMIES(a4)
		
		move.w  RYGAR_LAST_DIRECTION(a2),d3
                move.b  d3,RYGAR_MOVE_DIRECTION(a4)
                tst.w   RYGAR_DISKARM_STATE(a4)         ; Is it attaking already
                bne     .exit                           ; No
                move.w  #-1,RYGAR_DISKARM_STATE(a4)     ; Only hittin an enemy will reset this or it finishes the sequence.
                clr.w   DISKARM_CURRENT_FRAME(a4)       ; Reset Disk armour attack pointer.

                moveq   #SND_DISKARM_NOPOWER,d0         ; Play jump sample
                move.w  RYGAR_CURRENT_POWERS(a4),d7
                and.w   #POWER_STAR,d7
                beq.s   .play
                moveq   #SND_DISKARM_WHIP,d0            ; Play jump sample
.play:
                bsr     PLAY_SAMPLE
.exit:          movem.l (a7)+,d0-d1/a1-a2
                rts


MOUSE_WAIT1:	btst.b	#6,$bfe001
		bne.s	MOUSE_WAIT1
.loop:		btst.b	#6,$bfe001
		beq.s	.loop
		rts

HDL_RYGAR:
		FUNCID	#$a47dd64b
                SPRITE_INITIATE

;--------------------------------------------------------
.constructor:   ; Variables that will only be ran once while sprite is active.

		
                move.w  #SPRITE_INSTATE_ONPLATFORM,THIS_SPRITE_STATUS(a2)
		
                move.w  SPRITE_PLOT_XPOS(a1),PRIVATE_RYGAR_XPOS(a2)
		add.w	#2,PRIVATE_RYGAR_XPOS(a2)
		clr.w	PRIVATE_RYGAR_SWING(a2)
		clr.w	PRIVATE_RYGAR_SWING_SINEPTR(a2)

.active:	
		tst.w	REAPER_ACTIVE(a4)
		bmi	.heaven
		cmp.w	#SANCTUARY_TIMER_BONUS,RYGAR_SANCTUARY_COMMAND(a4)
		beq	.check_timer_bonus
		cmp.w	#SANCTUARY_SCROLL_RIGHT,RYGAR_SANCTUARY_COMMAND(a4) 		; Assembler will change this to tst.w
		beq	.in_game
                cmp.w   #SANCTUARY_SHOW_PASSWORD,RYGAR_SANCTUARY_COMMAND(a4)         
                beq.s   .show_password
                cmp.w   #SANCTUARY_DISABLE_ENEMIES,RYGAR_SANCTUARY_COMMAND(a4)         
                beq.s   .enemy_disable
                cmp.w   #SANCTUARY_ENABLE_ENEMIES,RYGAR_SANCTUARY_COMMAND(a4)         
                beq.s   .enemy_enable
                cmp.w   #SANCTUARY_RYGAR_FACING,RYGAR_SANCTUARY_COMMAND(a4)         
                beq.s   .is_facing
                cmp.w   #SANCTUARY_FACE_SCROLL_RIGHT,RYGAR_SANCTUARY_COMMAND(a4)
                bge.s   .is_facing_and_scroll
; anything else means Rygar is in the sanctuary somewhere.

.in_carousel:   move.w  RYGAR_SANCTUARY_COMMAND(a4),d1
                lsr.w   #2,d1
                tst.w   SANCTUARY_STAGE(a4)
                bne.s   .right_carousel

.left_carousel: move.w  d1,LEFT_CAROUSEL_POSITION(a4)
                bra.s   .carousel

.right_carousel:
                move.w  d1,RIGHT_CAROUSEL_POSITION(a4)
                bra.s   .carousel
               
.show_password:	bsr	SHOW_ROUND_PASSWORD
		bra	.exit
	
.check_timer_bonus:
		bsr	CHECK_SANCTUARY_TIMER_BONUS
		bra	.exit

.carousel:      move.w  RYGAR_XPOS(a4),d1
                add.w   RYGAR_SANCTUARY_COMMAND(a4),d1          ; This will be a positive number to offset Rygar to.
                move.w  d1,SPRITE_PLOT_XPOS(a1)
		move.w	d1,SHIELD_XPOS(a4)
                moveq   #RYGAR_CAROUSEL,d1
                bra     ANIMATE

.enemy_enable:  clr.w   DISABLE_ENEMIES(a4)
		move.w	#-1,UPDATE_SCORE(a4)
                bra     .exit
.enemy_disable: move.w  #-1,DISABLE_ENEMIES(a4)
                bra     .exit


.is_facing:     moveq   #RYGAR_FACING,d1
                bra     ANIMATE

.is_facing_and_scroll:
                move.w  RYGAR_SANCTUARY_COMMAND(a4),d3
                move.w  RYGAR_XPOS(a4),d1
                sub.w   #SANCTUARY_FACE_SCROLL_RIGHT,d3
                sub.w   d3,d1
                move.w  d1,SPRITE_PLOT_XPOS(a1)
		move.w	d1,SHIELD_XPOS(a4)
                moveq   #RYGAR_FACING,d1
                bra     ANIMATE


		
.heaven:
		moveq	#RYGAR_HEAVEN,d1
		bra	ANIMATE

.anim_death_sequence:

		tst.w	REAPER_ACTIVE(a4)
		bmi	.exit
		
		move.w	#-1,PAUSE_ENEMIES(a4)
		clr.w	RYGAR_DISKARM_STATE(a4)
                clr.w	DISKARM_DISABLE(a4)
		subq.w  #1,SPRITE_PLOT_XPOS(a1)
                lea     RYGAR_DEATH_SINE,a0
                move.w  RYGAR_SINE_INDEX(a4),d2
                move.w  (a0,d2*2),d2                    ; Get the value to adjust by.
                cmp.w   #$8000,d2
                beq.s   .death1
		
                add.w   d2,SPRITE_PLOT_YPOS(a1)
		cmp.w	#148,SPRITE_PLOT_YPOS(a1)
		bgt.s	.death2
                addq.w  #1,RYGAR_SINE_INDEX(a4)

.death1:        moveq   #RYGAR_DEATH,d1
                bsr     ANIMATE
                tst.w   d6
                bpl     .exit

.death2:
                move.w  #-1,RYGAR_LIFE_LOST(a4)
                IFEQ    DEBUG_UNLIMITED_LIVES
                subq.w  #1,RYGAR_LIVES(a4)
                ENDC
		
; Only set last map position when the rope is not enabled.
		tst.w	CHECKPOINT_DISABLE(a4)
		bmi	.exit
                bsr	SET_LAST_MAP_POSITION

                bra     .exit

.in_game:
                bsr     RYGAR_DISKARM_CHECK
                bsr     RYGAR_JUMP_CHECK
                move.w  #DISKARM_STANDPOS,DISKARM_VERT_OFFSET(a4)           ; Disk Armour Position when standing or running.

		move.w	RYGAR_FALLPREV_STATE(a4),RYGAR_FALLPREV_STATE1(a4)
                move.w  RYGAR_FALL_STATE(a4),RYGAR_FALLPREV_STATE(a4)                       ; Save previous fall state for collision

		tst.w	RYGAR_LIFE_LOST(a4)
		bmi	.anim_death_sequence
                cmp.w   #RYGAR_DEATH,THIS_SPRITE_STATUS(a2)
                beq     .anim_death_sequence

                tst.w   RYGAR_JUMP_STATE(a4)                    ; Is Jump flag set?
                beq     .apply_gravity                          ; runing or standing
; Collisions need to happen on decent. So we check the blocks under Rygar.
                cmp.w   #SPRITE_INSTATE_ONPLATFORM,THIS_SPRITE_STATUS(a2)
                bne.s   .jump_or_fall
                move.w  #SPRITE_INSTATE_JUMPING,THIS_SPRITE_STATUS(a2)  ; Set to jumping.

.jump_or_fall:  cmp.w   #SPRITE_INSTATE_JUMPING,THIS_SPRITE_STATUS(a2)
                beq.s   .jump                                   ; Rygar is lifting
                cmp.w   #SPRITE_INSTATE_FALLING,THIS_SPRITE_STATUS(a2)
                beq.s   .fall                                   ; Rygar is falling
                bra     .apply_gravity                          ; Neither, apply normal Earth gravity

; Jump in flight
.jump:          clr.w   DISKARM_DISABLE(a4)
                clr.w   RYGAR_BLOCKED_STATE(a4)         ; Default to allow scrolling
		
		
                lea     RYGAR_JUMP_SINE(a4),a3
                tst.w	RYGAR_MINI_JUMP(a4)
		beq.s	.normal_jump
.mini_jump:	lea     RYGAR_MINIJUMP_SINE(a4),a3

.normal_jump:
		moveq   #0,d2
                move.w  RYGAR_SINE_INDEX(a4),d2

                move.w  (a3,d2*2),d3
                cmp.w   #$8000,d3                               ; Has the jump completed?
                beq.s   .jump_done                              ; Yes
                bsr     CHECK_TILE                              ; No check tile for left/right obstacles
                add.w   d3,SPRITE_PLOT_YPOS(a1)                         ; Modify Y value for jump
                addq.w  #1,RYGAR_SINE_INDEX(a4)
                clr.w   RYGAR_CROUCH_STATE(a4)
                bra     .run_or_stand
.jump_done:             move.w  #SPRITE_INSTATE_FALLING,THIS_SPRITE_STATUS(a2)          ; Set Rygar falling.
                clr.w   RYGAR_SINE_INDEX(a4)
		clr.w	RYGAR_MINI_JUMP(a4)
                bra     .run_or_stand

; Fall in flight
.fall:
                move.w  #-1,RYGAR_FALL_STATE(a4)                        ; Set rygar falling
                lea     RYGAR_FALL_SINE,a3
                moveq   #0,d2
                move.w  RYGAR_SINE_INDEX(a4),d2

                move.w  (a3,d2*2),d3
                addq.w  #1,RYGAR_SINE_INDEX(a4)
                nop
                nop
                cmp.w   #SCREEN_BOTTOM,SPRITE_PLOT_YPOS(a1)                     ; Has fall killed Rygar?
                bge.s   .fall_done                      ; Yes
                cmp.w   #$8000,d3                       ; Has
                beq.s   .fall_done
                bsr     CHECK_TILE
		tst.w	ROPE_LINE_ENABLE(a4)
		bmi.s	.keep_falling
                tst.w   d7
                bmi.s   .keep_falling                   ; Platform not found... keep falling.
                and.w   #$fff0,SPRITE_PLOT_YPOS(a1)                    ; Snap to upper 16 pixels on Y axis for sprite
                ;addq.w #RYGAR_Y_TILEOFFSET,4(a1)
                add.w   d5,SPRITE_PLOT_YPOS(a1)
                bra.s   .fall_done
.keep_falling:
                add.w   d3,SPRITE_PLOT_YPOS(a1)                        ; Modify Y value
                clr.w   RYGAR_CROUCH_STATE(a4)
                bra     .run_or_stand
.fall_done:     move.w  #SPRITE_INSTATE_ONPLATFORM,THIS_SPRITE_STATUS(a2)               ; Set Rygar on platform.
                clr.w   RYGAR_SINE_INDEX(a4)
                clr.w   RYGAR_JUMP_STATE(a4)
                clr.w   RYGAR_FALL_STATE(a4)
                clr.w   RYGAR_ROLLFALL_STATE(a4)
                bra     .run_or_stand

; Executed when Rygar is not jumping or falling.
.apply_gravity:
		tst.w   VERTSCROLL_ENABLE(a4)
                bmi     .vertical

		bsr     CHECK_TILE
		tst.w   d7                      ; On a platform yet?
		beq	.landed
		
		IFNE	DISABLE_GRAVITY
			bra	.run_or_stand
		ENDC

                tst.w   SND_FALLFLAG(a4)
                bmi.s   .no_fall_snd
                subq.w  #1,SND_FALLFLAG(a4)
                move.l  d0,-(a7)
                moveq   #SND_SWEEP,d0
                bsr     PLAY_SAMPLE
                move.l  (a7)+,d0

.no_fall_snd:
                addq.w  #6,SPRITE_PLOT_YPOS(a1)         ; No, Keep falling
		move.w  #-1,RYGAR_FALL_STATE(a4)
                
		IFEQ    DISABLE_ENEMY_COLLISION
                cmp.w   #SCREEN_BOTTOM,SPRITE_PLOT_YPOS(a1)            ; Hit the depths of Hell though?
                bge.s   .dead                   			; Yes, he's dead
                ELSE
                ;move.w  #SCREEN_BOTTOM,4(a1)
                ENDC
                cmp.w   #SPRITE_INSTATE_DIRECTION_LEFT,RYGAR_LAST_DIRECTION(a2)
                beq     .is_falling_left
                bra     .is_falling_right

                bra.s   .run_or_stand           ; No, so fall animation


.dead:          move.w  #SCREEN_BOTTOM,SPRITE_PLOT_YPOS(a1)
                move.w  #-1,RYGAR_LIFE_LOST(a4)
                IFEQ    DEBUG_UNLIMITED_LIVES
                subq.w  #1,RYGAR_LIVES(a4)
                ENDC
		
		bsr     STOP_TUNE
		
                moveq   #SND_RYGARDEAD,d0               ; Play jump sample
                bsr     PLAY_SAMPLE

		tst.w   CHECKPOINT_DISABLE(a4)
                bmi     .exit

                bsr	SET_LAST_MAP_POSITION

                bra     .exit

.landed:        
		clr.w	RYGAR_LOCK_LEFT_RIGHT(a4)
		clr.w	RYGAR_FALL_STATE(a4)
		clr.w	RYGAR_ROLLFALL(a4)
		and.w   #$fff0,SPRITE_PLOT_YPOS(a1)
                clr.w   SND_FALLFLAG(a4)
                add.w   d5,SPRITE_PLOT_YPOS(a1)

.run_or_stand:
                tst.w   VERTSCROLL_ENABLE(a4)
                bmi     .vertical

                bsr     CHECK_TILE
                tst.w   SCROLL_DIRECTION_X(a4)
                beq.s   .is_standing
                bmi     .is_running_left
                bra     .is_running_right

.is_standing:                                   ; check last direction previous value here.
                cmp.w   #SPRITE_INSTATE_DIRECTION_LEFT,RYGAR_LAST_DIRECTION(a2)
                beq.s   .is_standing_left
                bra.s   .is_standing_right

.is_standing_left:
                moveq   #RYGAR_ATTACK_LEFT,d1
                tst.w   RYGAR_DISKARM_STATE(a4)                         ; Scrolling pauses when weapon is discharges when running
                bmi.s   .attack_left
                moveq   #RYGAR_STAND_LEFT,d1
.attack_left:
                tst.w   RYGAR_CROUCH_STATE(a4)                          ; Is Rygar Crouching?
                beq.s   .no_crouch_left

		movem.l	d0-d1/a0-a3,-(a7)
		bsr	CROUCH_DETECT
		movem.l	(a7)+,d0-d1/a0-a3
		cmp.w	#JOY1_DOWN_RIGHT,d3
		bne.s	.attack_left1
; Check if direction moved.  Swap Direction
		move.w	#SPRITE_INSTATE_DIRECTION_RIGHT,RYGAR_LAST_DIRECTION(a2)		
		bra.s	.attack_right

.attack_left1:	moveq   #RYGAR_CROUCH_ATTACK_LEFT,d1
                tst.w   RYGAR_DISKARM_STATE(a4)                         ; Scrolling pauses when weapon is discharges when running
                bmi.s   .crouch_attack_left
                moveq   #RYGAR_CROUCH_LEFT,d1
.crouch_attack_left:
                move.w  #DISKARM_CROUCHPOS,DISKARM_VERT_OFFSET(a4)          ; Disk Armour Position when crouching
                bra     .animate

.no_crouch_left:
                tst.w   RYGAR_JUMP_STATE(a4)
                beq     .animate
.is_falling_left:
                moveq   #RYGAR_JUMP_ATTACK_LEFT,d1
                tst.w  RYGAR_ROLLFALL(a4)
                beq.s  .no_roll_left
                moveq  #RYGAR_ROLLFALL_LEFT,d1
                bra	.animate

.no_roll_left:  move.w  #DISKARM_JUMPPOS,DISKARM_VERT_OFFSET(a4)
                tst.w   RYGAR_DISKARM_STATE(a4)                         ; Scrolling pauses when weapon is discharges when running
                bmi     .jump_attack_left1
                moveq   #RYGAR_JUMP_LEFT,d1
.jump_attack_left1:
                bra     .animate

.is_standing_right:
                moveq   #RYGAR_ATTACK_RIGHT,d1
                tst.w   RYGAR_DISKARM_STATE(a4)                         ; Scrolling pauses when weapon is discharges when running
                bmi     .attack_right
                moveq   #RYGAR_STAND_RIGHT,d1
.attack_right:
                tst.w   RYGAR_CROUCH_STATE(a4)
                beq.s   .no_crouch_right
		
		movem.l	d0-d1/a0-a3,-(a7)
		bsr	CROUCH_DETECT
		movem.l	(a7)+,d0-d1/a0-a3
		cmp.w	#JOY1_DOWN_LEFT,d3
		bne.s	.attack_right1
		move.w	#SPRITE_INSTATE_DIRECTION_LEFT,RYGAR_LAST_DIRECTION(a2)
		bra	.attack_left
		
.attack_right1:	moveq   #RYGAR_CROUCH_ATTACK_RIGHT,d1
                tst.w   RYGAR_DISKARM_STATE(a4)                         ; Scrolling pauses when weapon is discharges when running
                bmi     .crouch_attack_right
                moveq   #RYGAR_CROUCH_RIGHT,d1
.crouch_attack_right:
                move.w  #DISKARM_CROUCHPOS,DISKARM_VERT_OFFSET(a4)          ; Disk Armour Position when crouching
                bra     .animate

.no_crouch_right:
                tst.w   RYGAR_JUMP_STATE(a4)
                beq     .animate
.is_falling_right:
                moveq   #RYGAR_JUMP_ATTACK_RIGHT,d1

                tst.w  RYGAR_ROLLFALL(a4)
                beq.s  .no_roll_right
                moveq  #RYGAR_ROLLFALL_RIGHT,d1
		bra	.animate

.no_roll_right: move.w  #DISKARM_JUMPPOS,DISKARM_VERT_OFFSET(a4)
                tst.w   RYGAR_DISKARM_STATE(a4)                         ; Scrolling pauses when weapon is discharges when running
                bmi     .jump_attack_right1
                moveq   #RYGAR_JUMP_RIGHT,d1
.jump_attack_right1:
                bra     .animate

;----------------------------------------------------
; Running Left
;----------------------------------------------------
.is_running_left:
                tst.w   d6				
                beq.s   .no_obs_left
                move.w  #-1,RYGAR_BLOCKED_STATE(a4)
                moveq   #RYGAR_RUNNING_LEFT,d1
                bra     .animate

.no_obs_left:   clr.w   RYGAR_BLOCKED_STATE(a4)
                tst.b   SCROLL_EDGE_LOCK_X(a4)          ; 0 if locked.
                bmi.s   .static_left

; Here we move Rygar to the left toward the left edge of the screen
; In vertical scenes this value is different.
		move.w	MAP_PIXEL_POSX_LIMIT(a4),d2
		beq.s	.not_vertical
		
		tst.w	LIGAR_FIGHT(a4)			; Only when Rygar fights Ligar.
		bmi.s	.not_vertical
		
		cmp.w   #80,SPRITE_PLOT_XPOS(a1)
                ble.s   .static_left
.not_vertical:	


                cmp.w   #RYGAR_LEFT_BARRIER,SPRITE_PLOT_XPOS(a1)
                ble.s   .static_left
                subq.w  #RYGAR_MOVE_SPEED,SPRITE_PLOT_XPOS(a1)                 ; Move Rygar

.static_left:   move.w  #SPRITE_INSTATE_DIRECTION_LEFT,RYGAR_LAST_DIRECTION(a2)
                moveq   #RYGAR_RUNNING_LEFT,d1
                tst.w   RYGAR_JUMP_STATE(a4)
                beq     .animate
		
                moveq   #RYGAR_JUMP_ATTACK_LEFT,d1	
		tst.w  RYGAR_ROLLFALL(a4)
                beq.s  .no_roll_left_jump
                moveq  #RYGAR_ROLLFALL_LEFT,d1
		bra	.animate

.no_roll_left_jump:		
		move.w  #DISKARM_JUMPPOS,DISKARM_VERT_OFFSET(a4)
	
                tst.w   RYGAR_DISKARM_STATE(a4)                         ; Scrolling pauses when weapon is discharges when running
                bmi     .jump_attack_left2
                moveq   #RYGAR_JUMP_LEFT,d1
.jump_attack_left2:
                bra     .animate

;----------------------------------------------------
; Running Right
;----------------------------------------------------

.is_running_right:
                tst.w   d6
                beq.s   .no_obs_right
                move.w  #1,RYGAR_BLOCKED_STATE(a4)
                moveq   #RYGAR_RUNNING_RIGHT,d1
                bra     .animate

.no_obs_right:  clr.w   RYGAR_BLOCKED_STATE(a4)
		tst.b   SCROLL_EDGE_LOCK_X(a4)          ; 0 if locked.
                bmi.s   .static_right

		cmp.w   #248,SPRITE_PLOT_XPOS(a1)
                bge.s   .static_right
		
; Scroll lock off but has Rygar rygar reach the mid point.
                addq.w  #RYGAR_MOVE_SPEED,SPRITE_PLOT_XPOS(a1)

.static_right:  move.w  #SPRITE_INSTATE_DIRECTION_RIGHT,RYGAR_LAST_DIRECTION(a2)
                moveq   #RYGAR_RUNNING_RIGHT,d1
                tst.w   RYGAR_JUMP_STATE(a4)
                beq     .animate
                ;moveq  #RYGAR_JUMP_RIGHT,d1
                
                moveq   #RYGAR_JUMP_ATTACK_RIGHT,d1
		
		tst.w  RYGAR_ROLLFALL(a4)
                beq.s  .no_roll_right_jump
                moveq  #RYGAR_ROLLFALL_RIGHT,d1
		bra	.animate

.no_roll_right_jump:		
		move.w  #DISKARM_JUMPPOS,DISKARM_VERT_OFFSET(a4)		
                tst.w   RYGAR_DISKARM_STATE(a4)                         ; Scrolling pauses when weapon is discharges when running
                bmi     .jump_attack_right2
                moveq   #RYGAR_JUMP_RIGHT,d1
.jump_attack_right2:
                bra     .animate


.complete_jump:
		bsr     CHECK_TILE
		tst.w   d7                      ; On a platform yet?
		beq	.rygar_ready_for_rope
		addq.w	#3,SPRITE_PLOT_YPOS(a1)
		bra	.animate

.rygar_ready_for_rope:
		clr.w	RYGAR_JUMP_STATE(a4)
		clr.w	RYGAR_FALL_STATE(a4)
		
.vertical:
; We have to complete the jump
		tst.w	RYGAR_JUMP_STATE(a4)			; is Rygar still jumping?
		bmi	.complete_jump
		tst.w	RYGAR_FALL_STATE(a4)			; is Rygar still falling?
		bmi	.complete_jump

		cmp.w	#$82,SPRITE_PLOT_YPOS(a1)
		beq.s	.climb_done
		;tst.w	RYGAR_SHORT_CLIMB_FRAMES(a4)
		;beq.s	.climb_done
		;subq.w	#1,RYGAR_SHORT_CLIMB_FRAMES(a4)		; Do a short climb up the rope
		subq.w	#1,SPRITE_PLOT_YPOS(a1)			
		clr.w	RYGAR_SWING_STATE(a4)
		
		bra	.climb_up
		
.climb_done:
                cmp.w   #-1,PRIVATE_RYGAR_SWING(a2)
                beq.s   .swing_left
                cmp.w   #1,PRIVATE_RYGAR_SWING(a2)
                beq.s   .swing_right

                tst.w   RYGAR_SWING_STATE(a4)
                beq     .rope_check
                bmi     .swing_left
                bra     .swing_right

;-----------------------------------------
; Rygar Swing Left
.swing_left:    lea     SWING_SINE_POINTER(a4),a0
                move.w  (a0),d2
                bne.s   .swing_left_1
                move.w  SPRITE_PLOT_XPOS(a1),PRIVATE_RYGAR_XPOS(a2)
                move.w  #-1,PRIVATE_RYGAR_SWING(a2)
                move.w  #SPRITE_INSTATE_DIRECTION_LEFT,RYGAR_LAST_DIRECTION(a2)

.swing_left_1:  lea     SWING_LEFT_SINE(a4),a3
                move.w  (a3,d2*2),d2
                addq.w  #2,(a0)
                cmp.w   #$8000,d2
                bne.s   .swing_reset_left
                clr.w   (a0)
                clr.w   RYGAR_SWING_STATE(a4)
                clr.w   PRIVATE_RYGAR_SWING(a2)
                moveq   #0,d2

.swing_reset_left:
                add.w   PRIVATE_RYGAR_XPOS(a2),d2
                move.w  d2,SPRITE_PLOT_XPOS(a1)
                move.w  d2,ROPE_LINE_UPPER_X2(a4)
                move.w  d2,ROPE_LINE_LOWER_X1(a4)
                moveq   #RYGAR_CLIMB_SWING_LEFT,d1
                bra     .animate


;-----------------------------------------
; Rygar Swing Right
.swing_right:   lea     SWING_SINE_POINTER(a4),a0
                move.w  (a0),d2
                bne.s   .swing_right_1
                move.w  SPRITE_PLOT_XPOS(a1),PRIVATE_RYGAR_XPOS(a2)
                move.w  #1,PRIVATE_RYGAR_SWING(a2)
                move.w  #SPRITE_INSTATE_DIRECTION_RIGHT,RYGAR_LAST_DIRECTION(a2)

.swing_right_1: lea     SWING_RIGHT_SINE(a4),a3
                move.w  (a3,d2*2),d2
                addq.w  #2,(a0)
                cmp.w   #$8000,d2
                bne.s   .swing_reset_right
                clr.w   (a0)
                clr.w   RYGAR_SWING_STATE(a4)
                clr.w   PRIVATE_RYGAR_SWING(a2)
                moveq   #0,d2

.swing_reset_right:
                add.w   PRIVATE_RYGAR_XPOS(a2),d2
                move.w  d2,SPRITE_PLOT_XPOS(a1)
                move.w  d2,ROPE_LINE_UPPER_X2(a4)
                move.w  d2,ROPE_LINE_LOWER_X1(a4)
                moveq   #RYGAR_CLIMB_SWING_RIGHT,d1
                bra     .animate

.rope_check:
                tst.b   SCROLL_DIRECTION_Y(a4)
                beq.s   .on_rope
                bmi.s   .climb_up
                bra.s   .climb_down

.on_rope:       moveq   #RYGAR_ON_ROPE,d1
                bra     .animate

.climb_up:      moveq   #RYGAR_CLIMB_UP,d1
                bra     .animate

.climb_down:    moveq   #RYGAR_CLIMB_DOWN,d1
                bra     .animate

                nop

.animate:
                move.w  SPRITE_PLOT_XPOS(a1),RYGAR_XPOS(a4)
                move.w  SPRITE_PLOT_YPOS(a1),RYGAR_YPOS(a4)
		
                move.w  SPRITE_PLOT_XPOS(a1),SHIELD_XPOS(a4)
                move.w  SPRITE_PLOT_YPOS(a1),SHIELD_YPOS(a4)

                move.w  SPRITE_PLOT_XPOS(a1),RYGAR_COORDS(a4)               ; Store Rygar Xposition
                move.w  SPRITE_PLOT_XPOS(a1),RYGAR_COORDS+2(a4)
                move.w  SPRITE_PLOT_YPOS(a1),RYGAR_COORDS+4(a4)
                move.w  SPRITE_PLOT_YPOS(a1),RYGAR_COORDS+6(a4)

                add.w   #14,RYGAR_COORDS(a4)               ; 
                add.w   #32-14,RYGAR_COORDS+2(a4)
                		
		add.w   #4,RYGAR_COORDS+4(a4)
                add.w   #22,RYGAR_COORDS+6(a4)
		
		moveq   #0,d2
		move.w  MAP_PIXEL_POSX(a4),d2
		sub.w   #256,d2
		add.w   RYGAR_XPOS(a4),d2
		lsr.w   #4,d2
		lea     ROUND_PLATFORMS(a4),a0
		move.l  (a0,d2*4),d2
                bra     ANIMATE
.exit:          rts



