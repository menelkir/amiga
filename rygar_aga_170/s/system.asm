
INTENASET	= %1100000000100000
;		   ab-------cdefg--
;	a: SET/CLR Bit
;	b: Master Bit
;	c: Blitter Int
;	d: Vert Blank Int
;	e: Copper Int
;	f: IO Ports/Timers
;	g: Software Int

DMASET		= %1000001111100000
;		   a----bcdefghi--j
;	a: SET/CLR Bit
;	b: Blitter Priority
;	c: Enable DMA
;	d: Bit Plane DMA
;	e: Copper DMA
;	f: Blitter DMA
;	g: Sprite DMA
;	h: Disk DMA
;	i..j: Audio Channel 0-3


*******************************************
*** Get Address of the VBR		***
*******************************************

GETVBR:	move.l	a5,-(a7)
	move.l	a6,-(a7)
	moveq	#0,d0			; default at $0
	move.l	$4.w,a6
	btst	#0,296+1(a6)		; 68010+?
	beq.b	.is68k			; nope.
	lea	.getit,a5
	jsr	-30(a6)			; SuperVisor()
.is68k:	move.l	(a7)+,a6
	move.l	(a7)+,a5
	rts

.getit:	;movec   vbr,d0
	dc.l	$4e7a0801
	rte				; back to user state code

; Shutdown the Operating System
SAVE_DMA:
	move.w	DMACONR(a5),d0		
	or.w 	#$8000,d0
	move.w 	d0,SV_DMACON(a4)
	move.w	INTENAR(a5),d0
	or.w	#$8000,d0
	move.w	d0,SV_INTENA(a4)
	move.w	INTREQR(a5),d0
	or.w	#$8000,d0
	move.w	d0,SV_INTREQ(a4)
	move.w	ADKCONR(a5),d0
	or.w	#$8000,d0
	move.w	d0,SV_ADKCON(a4)
	rts

; Restore interupts
RESTORE_DMA:
	move.w	#$7fff,DMACON(a5)
	move.w	SV_DMACON(a4),DMACON(a5)
	move.w	#$7fff,INTENA(a0)
	move.w	SV_INTENA(a4),INTENA(a5)
	move.w	#$7fff,INTREQ(a0)
	move.w	SV_INTREQ(a4),INTREQ(a5)
	move.w	#$7fff,ADKCON(a0)
	move.w	SV_ADKCON(a4),ADKCON(a5)
	rts
	
	
; Store existing Copper pointers	
SAVE_COPPER:
	movem.l	d0-d7/a0-a6,-(a7)
	move.l	ExecBase,a6
	lea	GFXNAME(a4),a1
	moveq	#0,d0
	jsr	-552(a6)
	move.l	d0,GFXBASE(a4)
	move.l	d0,a6
	move.l	34(a6),SV_OLDVIEW(a4)
	move.l	$26(a6),SV_OLDCOPPER1(a4)
	move.l	$32(a6),SV_OLDCOPPER2(a4)

	move.l	#0,a1
	jsr 	LOADVIEW(a6)
	jsr	WAITTOF(a6)
	jsr	WAITTOF(a6)
	movem.l	(a7)+,d0-d7/a0-a6
	rts
	

	
; Store existing Copper pointers	
WAIT_FOR_COPPER:
	movem.l	d0-d7/a0-a6,-(a7)
	move.l	ExecBase,a6
	lea	GFXNAME(a4),a1
	moveq	#0,d0
	jsr	-552(a6)
	move.l	d0,GFXBASE(a4)
	move.l	d0,a6

	move.l	#0,a1
	jsr 	LOADVIEW(a6)
	jsr	WAITTOF(a6)
	jsr	WAITTOF(a6)
	movem.l	(a7)+,d0-d7/a0-a6
	rts

; Disable OS	
DISABLE_OS:
	move.l	ExecBase,a6
	jsr	FORBID(a6)	; FORBID	
	rts
	
OS_UP:
; Restore interupts
	move.w	#$7fff,DMACON(a5)
	move.w	SV_DMACON(a4),DMACON(a5)
	move.w	#$7fff,INTENA(a0)
	move.w	SV_INTENA(a4),INTENA(a5)
	move.w	#$7fff,INTREQ(a0)
	move.w	SV_INTREQ(a4),INTREQ(a5)
	move.w	#$7fff,ADKCON(a0)
	move.w	SV_ADKCON(a4),ADKCON(a5)
	
; Restore copper
	move.l	SV_OLDCOPPER1(a4),COP1LCH(a5)
	move.l	SV_OLDCOPPER2(a4),COP2LCH(a5)
	move.l	GFXBASE(a4),a6
	move.l	SV_OLDVIEW(a4),a1
	jsr	LOADVIEW(a6)
	jsr	WAITTOF(a6)
	jsr	WAITTOF(a6)
	
; Restore OS
	move.l	ExecBase,a6
	jsr	FORBID(a6)
	rts
	


*******************************************
*** Get Address of the VBR		***
*******************************************

GetVBR	move.l	a3,-(a7)
	moveq	#0,d0			; default at $0
	move.l	ExecBase,a6
	btst	#0,296+1(a6)		; 68010+?
	beq.b	.is68k			; nope.
	lea	.getit,a3
	jsr	-30(a6)			; SuperVisor()
.is68k	move.l	(a7)+,a3
	rts

.getit	
	dc.l	$4e7a0801
	;movec   vbr,d0
	rte				; back to user state code

