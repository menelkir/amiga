
ROUND17_EVENT_INJECT_VILLAGERS_1:
	nop
	moveq	#8,d7
	move.l	#$3000,d6
	
.deploy:
	movem.l	d6-d7,-(a7)
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	or.w	d6,d3				; Parameter Sets Delay Frames
	moveq	#SPR_VILLAGER,d0
	move.w	MAP_PIXEL_POSX(a4),d1
	add.w	#16,d1
	move.w	#90,d2
	lea	HDL_VILLAGER(pc),a0
	bsr	PUSH_SPRITE
	movem.l	(a7)+,d6-d7
	btst	#1,d7
	bne.s	.add
	add.w	#$0800,d6
.add:	add.w	#$0800,d6
	dbf	d7,.deploy
	rts
	

ROUND17_EVENT_INJECT_VILLAGERS_2:
	nop
	moveq	#4,d7
	move.l	#$4000,d6			; Wait  64 frames
	
.deploy1:
	movem.l	d6-d7,-(a7)
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	or.w	d6,d3				; Parameter Sets Delay Frames
	moveq	#SPR_VILLAGER,d0
	move.w	MAP_PIXEL_POSX(a4),d1
	add.w	#16,d1
	move.w	#90,d2
	lea	HDL_VILLAGER(pc),a0
	bsr	PUSH_SPRITE
	movem.l	(a7)+,d6-d7
	btst	#2,d7
	bne.s	.add1
	add.w	#$0800,d6
.add1:	add.w	#$0800,d6
	dbf	d7,.deploy1
	
	
	moveq	#2,d7
	move.l	#$4000,d6			; Wait  64 frames
	
.deploy2:
	movem.l	d6-d7,-(a7)
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	or.w	d6,d3				; Parameter Sets Delay Frames
	moveq	#SPR_VILLAGER,d0
	move.w	MAP_PIXEL_POSX(a4),d1
	add.w	#112,d1
	move.w	#138,d2
	lea	HDL_VILLAGER(pc),a0
	bsr	PUSH_SPRITE
	movem.l	(a7)+,d6-d7
	btst	#2,d7
	bne.s	.add2
	add.w	#$0800,d6
.add2:	add.w	#$1000,d6
	dbf	d7,.deploy2
	rts
	
	
ROUND17_EVENT_INJECT_VILLAGERS_3:
	nop
	moveq	#8,d7
	moveq	#0,d6
	
.deploy:
	movem.l	d6-d7,-(a7)
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	or.w	d6,d3				; Parameter Sets Delay Frames
	moveq	#SPR_VILLAGER,d0
	move.w	MAP_PIXEL_POSX(a4),d1
	add.w	#16,d1
	move.w	#138,d2
	lea	HDL_VILLAGER(pc),a0
	bsr	PUSH_SPRITE
	movem.l	(a7)+,d6-d7
	add.w	#$0800,d6
	dbf	d7,.deploy
	rts
	
	
ROUND17_EVENT_INJECT_VILLAGERS_4:
	nop
	moveq	#2,d7
	move.l	#$2000,d6			; Wait  64 frames
	
.deploy1:
	movem.l	d6-d7,-(a7)
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	or.w	d6,d3				; Parameter Sets Delay Frames
	moveq	#SPR_VILLAGER,d0
	move.w	MAP_PIXEL_POSX(a4),d1
	add.w	#2*16,d1
	move.w	#90,d2
	lea	HDL_VILLAGER(pc),a0
	bsr	PUSH_SPRITE
	movem.l	(a7)+,d6-d7
	add.w	#$0800,d6			; Wait 16 frames
	dbf	d7,.deploy1
	
	
	moveq	#2,d7
	move.l	#$3000,d6			; Wait  64 frames
	
.deploy2:
	movem.l	d6-d7,-(a7)
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	or.w	d6,d3				; Parameter Sets Delay Frames
	moveq	#SPR_VILLAGER,d0
	move.w	MAP_PIXEL_POSX(a4),d1
	sub.w	#1*16,d1
	move.w	#138,d2
	lea	HDL_VILLAGER(pc),a0
	bsr	PUSH_SPRITE
	movem.l	(a7)+,d6-d7
	add.w	#$1000,d6			; Wait 16 frames
	dbf	d7,.deploy2
	
	moveq	#2,d7
	move.l	#$5000,d6
	
.deploy3:
	movem.l	d6-d7,-(a7)
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	or.w	d6,d3				; Parameter Sets Delay Frames
	moveq	#SPR_VILLAGER,d0
	move.w	MAP_PIXEL_POSX(a4),d1
	sub.w	#4*16,d1
	move.w	#138,d2
	lea	HDL_VILLAGER(pc),a0
	bsr	PUSH_SPRITE
	movem.l	(a7)+,d6-d7
	add.w	#$0c00,d6			; Wait 16 frames
	dbf	d7,.deploy3
	rts
	

ROUND17_EVENT_INJECT_VILLAGERS_5:
	nop
	moveq	#3,d7
	move.l	#$8000,d6
	
.deploy:
	movem.l	d6-d7,-(a7)
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	or.w	d6,d3				; Parameter Sets Delay Frames
	moveq	#SPR_VILLAGER,d0
	move.w	MAP_PIXEL_POSX(a4),d1
	add.w	#16,d1
	move.w	#90,d2
	lea	HDL_VILLAGER(pc),a0
	bsr	PUSH_SPRITE
	movem.l	(a7)+,d6-d7
	btst	#1,d7
	bne.s	.add
	add.w	#$0800,d6
.add:	add.w	#$0800,d6
	dbf	d7,.deploy
	rts	
	
ROUND17_EVENT_INJECT_VILLAGERS_6:
	nop
	moveq	#1,d7
	move.l	#$7000,d6
	
.deploy1:
	movem.l	d6-d7,-(a7)
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	or.w	d6,d3				; Parameter Sets Delay Frames
	moveq	#SPR_VILLAGER,d0
	move.w	MAP_PIXEL_POSX(a4),d1
	add.w	#16,d1
	move.w	#138,d2
	lea	HDL_VILLAGER(pc),a0
	bsr	PUSH_SPRITE
	movem.l	(a7)+,d6-d7
	btst	#1,d7
	bne.s	.add1
	add.w	#$0800,d6
.add1:	add.w	#$0800,d6
	dbf	d7,.deploy1
	
	moveq	#1,d7
	move.l	#$3000,d6
	
.deploy2:
	movem.l	d6-d7,-(a7)
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	or.w	d6,d3				; Parameter Sets Delay Frames
	moveq	#SPR_VILLAGER,d0
	move.w	MAP_PIXEL_POSX(a4),d1
	add.w	#48,d1
	move.w	#138,d2
	lea	HDL_VILLAGER(pc),a0
	bsr	PUSH_SPRITE
	movem.l	(a7)+,d6-d7
	btst	#1,d7
	bne.s	.add2
	add.w	#$0800,d6
.add2:	add.w	#$0800,d6
	dbf	d7,.deploy2
	
	moveq	#1,d7
	move.l	#$5000,d6
	
.deploy3:
	movem.l	d6-d7,-(a7)
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	or.w	d6,d3				; Parameter Sets Delay Frames
	moveq	#SPR_VILLAGER,d0
	move.w	MAP_PIXEL_POSX(a4),d1
	add.w	#80,d1
	move.w	#138,d2
	lea	HDL_VILLAGER(pc),a0
	bsr	PUSH_SPRITE
	movem.l	(a7)+,d6-d7
	btst	#1,d7
	bne.s	.add3
	add.w	#$0800,d6
.add3:	add.w	#$0800,d6
	dbf	d7,.deploy3
	
	rts
	