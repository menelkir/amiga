
	
WAIT_FOR_VERTICAL_BLANK:
.paused:
	btst.b	#7,$bfe001
	beq.s	.unpause
	tst.w	PAUSE_GAME(a4)
	bmi.s	.paused
	bra.s	.vpos
.unpause:
	move.w	#1,PAUSE_GAME(a4)
.vpos:  move.l  VPOSR(a5),d0
        and.l   #$1ff00,d0
        cmp.l   #303<<8,d0              ; Wait for line 303
        bne.s   .vpos
	
.vposn: move.l  VPOSR(a5),d0
        and.l   #$1ff00,d0
        cmp.l   #303<<8,d0              ; Wait till line 303 finished.
        beq.s   .vposn
        rts
	

WAIT_VERT_POS:  equ     186
VERT_BORDER_OFFSET:     equ     $2a + 24


; Here we need to blank the front and back screens then
; copy the post screen lines from the bottom up.
TRANSITION_FROM_BLACK:
	FUNCID	#$aba0c5d2
        move.w  #-1,TRANSITION_ACTIVE(a4)

; Clear the front screen
        lea     LSTPTR_FRONT_SCREEN(a4),a1
        move.l  (a1),a3                         ; Get front screen
        bsr     TRANSITION_CLEAR_SCREEN
        move.l  20(a1),a3                       ; Get front screen
        bsr     TRANSITION_CLEAR_SCREEN

        move.l  COPPTR_VIDEOTOP(a4),a0
        lea     LSTPTR_FRONT_SCREEN(a4),a1              ; Set the display to the front screen
        moveq   #0,d3
        bsr     SET_FRAMEBUFF
	
; Turn on Bitplanes - screen is Black
        move.l  COPPTR_BPLCON(a4),a0
        IFEQ    DEBUG_BG_ENABLE
        move.w  #$5200,2(a0)                            ; Enable Bitplanes
        ELSE
        move.w  #$0211,2(a0)
        ENDC

        bsr     DO_CAPTION_BLACK
	
	clr.w	BLIT_BG_START_TMP
	
.tmp_loop:

; Copy lines from Back to front screen
        move.l  #PLAYFIELD_HEIGHT-1,d7                  ; 184 lines
.copyline:
        bsr     WAIT_FOR_VERTICAL_BLANK
	
; Copy lines from Back to Front screen
        rept    8
	move.l  d7,d0
	
        lea     LSTPTR_BACK_SCREEN(a4),a0               ; Source is in Back Screen
        lea     LSTPTR_FRONT_SCREEN(a4),a1
        move.l  (a0),a2
        move.l  (a1),a3
        bsr     TRANSITION_FG_COPY_LINE
; Copy the forefround line
	
;;;  Copy Background line 
	

	move.l  STCPTR_BACKGROUND(a4),a2		; Source Pointer to Background image
        lea     LSTPTR_OFF_SCREEN(a4),a1		; Destination Copy into this front screen
        move.l  20(a1),a3
        bsr     TRANSITION_BG_COPY_LINE
        subq.w  #1,d7
        endr

        move.l  LSTPTR_FRONT_SCREEN(a4),a1
        bsr     BLIT_CAPTION

        tst.w   d7
        bpl     .copyline
	
        clr.w   TRANSITION_ACTIVE(a4)

        bsr     DO_CAPTION_START

        bsr     WAIT_FOR_VERTICAL_BLANK
        bsr     BLIT_CAPTION_RESTORE
.exit:  rts


;d0 = line number
;a2 = source screen pointer
;a3 = destination screen pointer
TRANSITION_BG_COPY_LINE:
        FUNCID  #$9e57f54b
        movem.l d0-d7/a0-a3,-(a7)
	cmp.w	#BG_SCROLL_NORMAL,BG_SCROLL_TYPE(a4)
	bne	.exit
        lea     CANVAS_TABLE_Y3(a4),a1
	
;--------------------------
        moveq   #0,d3                           ; Start line
        moveq   #0,d4                           ; End line
        move.w  BLIT_BG_START(a4),d4            ;
        move.w  d4,d3                           ; start=32
        add.w   BLIT_BG_LENGTH(a4),d4           ; end  =144
	
	moveq	#-1,d6
        cmp.w   d3,d0                           ; Is 10 less then 32
        blt     .fill_line                      ; yes, fill line

        moveq	#0,d6
	cmp.w   d4,d0                           ; is 150 greater than 144
        bgt     .fill_line                      ; yes fill line

	move.w	#$700,FIRECOLOUR
; copy a line from the source to the destination line
.copy:
        move.l  d0,d1                           ; 
	sub.w	BLIT_BG_START(a4),d0

        mulu.w  #600,d0
        add.l   d0,a2                   ; Index to source line
        subq.w	#2,a3
	add.l   (a1,d1*4),a3            ; Index to dest line

	moveq	#0,d1
	moveq	#0,d2
        move.w  MAP_OFFSET_BG(a4),d2    ; Map off set in pixels
        add.w   SCROLL_HPOS(a4),d2      ; Compensate on HSCROLL
	
        move.w  d2,d3
        lsr.w   #3,d2                   ; Number of words in.
        add.l   d2,a2                   ; add to map source
        not.w   d3
        and.w   #$f,d3
        lsl.w   #8,d3
        lsl.w   #4,d3
        or.w    #$9f0,d3
	
	
; d2 now has pixel offset

;-----------------------------

	WAIT_FOR_BLITTER
        move.w  d3,BLTCON0(a5)
        move.w  #0,BLTCON1(a5)
        move.w  #160+4,BLTAMOD(a5)              ; 38 bytes (19 words) + 162 = 200
        move.w  #4,BLTDMOD(a5)			; 38 bytes (19 words) + 42  
        move.l  #$ffffffff,BLTAFWM(a5)
        move.l  a2,BLTAPTH(a5)
        move.l  a3,BLTDPTH(a5)
        moveq   #3,d3				; 3 bitplanes	
        lsl.w   #6,d3                                           ; Add length to blit
        add.w   #18,d3                                          ; width in words to blit
        move.w  d3,BLTSIZE(a5)



;------------------------------------
        bra     .exit

.fill_line:
        move.l  d0,d1                           ; what is the source line?
	
	move.w	#$ffff,DEBUG_BGSRC_LINE
	move.w	d1,DEBUG_BGDST_LINE

        add.l   (a1,d1*4),a3            ; Index to dest line

; Fill first bitplane only.
        move.l  d6,(a3)
        move.l  d6,4(a3)
        move.l  d6,8(a3)
        move.l  d6,12(a3)
        move.l  d6,16(a3)
        move.l  d6,20(a3)
        move.l  d6,24(a3)
        move.l  d6,28(a3)
        move.l  d6,32(a3)
        move.l  d6,36(a3)

.exit:  movem.l (a7)+,d0-d7/a0-a3
        rts



TRANSITION_TO_BLACK:
	FUNCID	#$7ca5f7bf

        move.l  #PLAYFIELD_HEIGHT-1,d7                  ; 184 lines
        moveq   #0,d0

.clearline:
        move.l  d0,-(a7)
        bsr     WAIT_FOR_VERTICAL_BLANK
        move.l  (a7)+,d0

        rept    8
        lea     LSTPTR_FRONT_SCREEN(a4),a1
        move.l  (a1),a3                         ; Get front screen
        bsr     TRANSITION_CLEAR_LINE

        lea     LSTPTR_BACK_SCREEN(a4),a1
        move.l  (a1),a3                         ; Get back screen
        bsr     TRANSITION_CLEAR_LINE
   
        lea     LSTPTR_OFF_SCREEN(a4),a1
        move.l  20(a1),a3                         ; Get front screen
        bsr     TRANSITION_CLEAR_LINE   
	
	addq.w  #1,d0
        subq.w  #1,d7

        endr

        tst.w   d7
        bpl     .clearline
        rts


TRANSITION_CLEAR_SCREEN:
	FUNCID	#$5dbaacc8
        move.l  #PLAYFIELD_HEIGHT-1,d7                  ; 184 lines
.clear:
        move.l  d7,d0
        bsr     TRANSITION_CLEAR_LINE
        dbf     d7,.clear
        rts


;d0 = line number
;a3 = screen pointer to clear
TRANSITION_CLEAR_LINE:
	FUNCID	#$863e1593
        move.l  a2,-(a7)
        move.l  a3,-(a7)
        move.l  d6,-(a7)
        lea     CANVAS_TABLE_Y3(a4),a2
        add.l   (a2,d0*4),a3
        add.w   MAP_POSX(a4),a3

        moveq   #50,d6                  ; 100 words wide (5 bitplanes * 20 words)
.clear:
        clr.l   (a3)+
        dbf     d6,.clear
        move.l  (a7)+,d6
        move.l  (a7)+,a3
        move.l  (a7)+,a2
        rts

;d0 = line number
;a2 = source screen pointer
;a3 = destination screen pointer
TRANSITION_FG_COPY_LINE:
	FUNCID	#$9e57f54b
        movem.l a0-a3,-(a7)
        lea     CANVAS_TABLE_Y3(a4),a1
        add.l   (a1,d0*4),a2
        add.l   (a1,d0*4),a3
        add.w   MAP_POSX(a4),a2
        add.w   MAP_POSX(a4),a3

        moveq   #50,d6                  ; 100 words wide (5 bitplanes * 20 words)
.move:
        move.l  (a2)+,(a3)+
        dbf     d6,.move
        movem.l (a7)+,a0-a3
        rts


;
; INIT_DISPLAY
;
; Initialise screen pointers
;
INIT_DISPLAY_POINTERS:
	FUNCID	#$4e9b7355
        bsr     CREATE_FRONTBUFF_POINTERS
        bsr     CREATE_BACKBUFF_POINTERS
        bsr     CREATE_POSTBUFF_POINTERS

        lea     LSTPTR_CURRENT_SCREEN(a4),a0
        lea     LSTPTR_FRONT_SCREEN(a4),a1
        lea     LSTPTR_OFF_SCREEN(a4),a2
        lea     LSTPTR_BACK_SCREEN(a4),a3

        rept    8
        move.l  (a1)+,(a0)+
        move.l  (a3)+,(a2)+
        endr
        rts



INIT_BITPLANE_POINTERS:
	FUNCID	#$65be8f89
        lea     LSTPTR_FRONT_SCREEN(a4),a0
        move.l  MEMCHK1_FRONT_FSCROLL(a4),d0

        rept    5
        move.l  d0,(a0)+
        add.l   #PLAYFIELD_SIZE_X,d0
        endr

        move.l  MEMCHK1_FRONT_BSCROLL(a4),d0
        rept    3
        move.l  d0,(a0)+
        add.l   #PLAYFIELD_SIZE_X,d0
        endr

        lea     LSTPTR_BACK_SCREEN(a4),a0
        move.l  MEMCHK1_BACK_FSCROLL(a4),d0
        rept    5
        move.l  d0,(a0)+
        add.l   #PLAYFIELD_SIZE_X,d0
        endr

        lea     LSTPTR_POST_SCREEN(a4),a0
        move.l  MEMCHK1_POST_SCROLL(a4),d0
        rept    5
        move.l  d0,(a0)+
        add.l   #PLAYFIELD_SIZE_X,d0
        endr
        rts

CREATE_FRONTBUFF_POINTERS:
	FUNCID	#$d08ee443
        move.l  MEMCHK1_FRONT_FSCROLL(a4),a1
        lea     LSTPTR_FRONT_SCREEN(a4),a3
        move.l  #PLAYFIELD_SIZE_X,d1
        moveq   #5-1,d5                                 ; 5 front bitplanes for background
        bsr     CREATE_BITPLANE_POINTERS

        move.l  MEMCHK1_FRONT_BSCROLL(a4),a1
        move.l  #PLAYFIELD_SIZE_X,d1
        moveq   #3-1,d5                                 ; 3 front bitplanes for background
        bsr     CREATE_BITPLANE_POINTERS
        rts

CREATE_BACKBUFF_POINTERS:
	FUNCID	#$a5529a9a
        move.l  MEMCHK1_BACK_FSCROLL(a4),a1
        lea     LSTPTR_BACK_SCREEN(a4),a3
        move.l  #PLAYFIELD_SIZE_X,d1
        moveq   #5-1,d5                                 ; 5 front bitplanes for background
        bsr     CREATE_BITPLANE_POINTERS

        move.l  MEMCHK1_FRONT_BSCROLL(a4),a1
        move.l  #PLAYFIELD_SIZE_X,d1
        moveq   #3-1,d5                                 ; 3 front bitplanes for background
        bsr     CREATE_BITPLANE_POINTERS
        rts

CREATE_POSTBUFF_POINTERS:
	FUNCID	#$5fbe64e8
        move.l  MEMCHK1_POST_SCROLL(a4),a1
        lea     LSTPTR_POST_SCREEN(a4),a3
        move.l  #PLAYFIELD_SIZE_X,d1
        moveq   #5-1,d5                                 ; Bitplanes
        bsr     CREATE_BITPLANE_POINTERS
        rts


; d1 = bitplane offsets
; d5 = planes
CREATE_BITPLANE_POINTERS:
	FUNCID	#$77fdb31c
.loop:  move.l  a1,(a3)+
        add.l   d1,a1
        dbf     d5,.loop
        rts


SETDISP_IN_COPPER:
	FUNCID	#$1dc772da
        moveq   #0,d0
        move.w  MAP_POSX(a4),d0
        add.l   d0,d2                                   ; 64 bit words in.

.loop:  move.w  d2,2(a0)
        swap    d2
        move.w  d2,6(a0)
        swap    d2
        add.l   #CANVAS_MODULO*1,d2
        addq.w  #8,a0
        dbf     d7,.loop

; Update horizontal scroll registers
        move.w  MAP_PIXEL_POSX(a4),d0
        and.w   #$1f,d0                                 ;Scroll Value
	
        move.l  COPPTR_BPLCON(a4),a0
        not.w   d0
        and.w   #$1f,d0
        move.w  d0,SCROLL_HPOS(a4)                  ; 64 bits

        move.w  d0,d1
        and.w   #$f,d1
        and.w   #$0010,d0                               ; keep bit 5
        lsl.w   #6,d0
        or.w    d0,d1
        lsl.w   #4,d0
        or.w    d0,d1
        move.b  d1,d0
        lsl.b   #4,d0
        or.b    d0,d1
        move.w  d1,6(a0)
        rts
	
HSCROLL_TAB:	dc.w	$00,$11,$22,$33,$44,$55,$66,$77,$88,$99,$aa,$bb,$cc,$dd,$ee,$ff

	CNOP	0,4
;
; SETSCR_BBUFF
;
; Set the screen pointers to use the Back Buffers
;
SETSCR_BBUFF:
	FUNCID	#$38e6d908
        lea     LSTPTR_BACK_SCREEN(a4),a1
        movem.l (a1),d0-d7
        lea     LSTPTR_CURRENT_SCREEN(a4),a0
        movem.l d0-d7,(a0)

        lea     LSTPTR_FRONT_SCREEN(a4),a1
        movem.l (a1),d0-d7
        lea     LSTPTR_OFF_SCREEN(a4),a0
        movem.l d0-d7,(a0)
        rts

;
; Sets the screen pointers to use Front Buffers
;
SETSCR_FBUFF:
	FUNCID	#$6174ef52
        lea     LSTPTR_FRONT_SCREEN(a4),a1
        movem.l (a1),d0-d7
        lea     LSTPTR_CURRENT_SCREEN(a4),a0
        movem.l d0-d7,(a0)

        lea     LSTPTR_BACK_SCREEN(a4),a1
        movem.l (a1),d0-d7
        lea     LSTPTR_OFF_SCREEN(a4),a0
        movem.l d0-d7,(a0)
        rts

;
; DBUFF()
;
; Toggle Screen and Display Buffer
;
DBUFF:  
	FUNCID	#$61766f38
	
        move.l  COPPTR_VIDEOTOP(a4),a0
        moveq   #0,d3
	not     SCR_TOGGLE(a4)
        beq.s   .set_fbuff

        lea     LSTPTR_BACK_SCREEN(a4),a1
	bsr     SET_FRAMEBUFF                           ; for restoring the sprite backgrounds
        bra     SETSCR_FBUFF        
.set_fbuff:
        lea     LSTPTR_FRONT_SCREEN(a4),a1
	bsr     SET_FRAMEBUFF
        bra     SETSCR_BBUFF
        

;
; SETDISP_FBUFF
;
; Set the display pointer to the front buffer
; a1=Screen List Pointer
SET_FRAMEBUFF:
	FUNCID	#$eea4c6b2
	
	
        move.l  a0,-(a7)
        move.l  a1,-(a7)
        moveq   #0,d0
        moveq   #0,d1

        moveq   #5-1,d7
        move.l  (a1),d2
        add.l   d3,d2
        bsr     SETDISP_IN_COPPER

; Do back 3 planes
        move.l  (a7)+,a1
        move.l  (a7)+,a0
        add.l   #40,a0
        move.l  20(a1),d2

        REPT    3
        move.w  d2,2(a0)
        swap    d2
        move.w  d2,6(a0)
        swap    d2
        add.l   #CANVAS_MODULO*1,d2
        addq.w  #8,a0
        ENDR

        rts


