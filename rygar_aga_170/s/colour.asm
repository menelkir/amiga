BLACK:  equ     $000
WHITE:  equ     $DB5

ENABLE_LAVA_CYCLE:	equ	-1
DISABLE_LAVA_CYCLE:	equ	0


COLOUR_INTERPOLATE:
                movem.l d1-d2/d4-d6,-(a7)
                moveq   #0,d0
                moveq   #2,d4
.ls:            moveq   #15,d5
                moveq   #15,d6
                and.w   d1,d5
                and.w   d2,d6
                lsr.w   #4,d1
                lsr.w   #4,d2
                sub.w   d5,d6
                muls.w  d7,d6
                muls.w  #$1111,d6
                add.w   d6,d6
                swap    d6
                addx.w  d6,d5
                or.w    d5,d0
                ror.w   #4,d0
                dbf     d4,.ls
                lsr.w   #4,d0
                movem.l (a7)+,d1-d2/d4-d6
                rts


; d7=palette size (15/31
; a0=source iff ptr
; a1=destination palette store
GET_PALETTE_FROM_IFF:
GET_16_PALETTE:
		FUNCID	#$bc1e746c
.loop:          cmp.l   #"CMAP",(a0)
                beq.s   .ok
                addq.w  #2,a0
                bra.s   .loop
.ok:            addq.w  #4,a0
                move.l  (a0)+,d7                ; number of bytes
                divu    #3,d7
                and.l   #$ffff,d7
                subq.w  #1,d7

.pal:
                move.b  (a0)+,d0
                and.b   #$f0,d0
                lsl.w   #4,d0
                move.b  (a0)+,d0
                and.b   #$f0,d0
                lsl.w   #4,d0
                move.b  (a0)+,d0
                lsr.w   #4,d0
                and.w   #$fff,d0

                move.w  d0,(a1)+

                dbf     d7,.pal
.exit:          rts


CYCLE_COLOURS:
	FUNCID	#$0f010e19
	tst.w	ROUND_TRANSITION(a4)
	bmi.s	.exit

	tst.l	ROUND_FG_LAVA_CYCLE(a4)
	beq.s	.water
        bsr     CYCLE_LAVA
	nop

.water:	tst.l   ROUND_FG_WATER_CYCLE(a4)
        beq.s   .exit
        bsr     CYCLE_WATER

.exit:  rts


CYCLE_LAVA:
	FUNCID	#$81369383
        moveq   #0,d7
	moveq	#0,d6

.loop:
        lea     LAVA_TABLE(a4),a0
        move.w  d7,d6
        lsl.w   #5,d6
        add.l   d6,a0
        tst.l   (a0)
        bmi.s   .exit

        move.l  (a0),a1
        move.l  (a1),a1
        subq.l  #1,20(a0)               ; decrement delay
        tst.l   20(a0)
        bpl.s   .ok
        move.l  12(a0),20(a0)           ; reset delay
        not.l   16(a0)                  ; swap

.ok:    move.l  4(a0),d1                ; Get colour
        tst.l   16(a0)                  ; 0 or 1?
        bmi.s   .col
        move.l  8(a0),d1

.col:   move.w  d1,14(a1)
        swap    d1
        move.w  d1,6(a1)
        addq.w  #1,d7
        bra.s   .loop
.exit:
        rts

WATER_ANIMS:    equ     5

; Cyble the Foreground water
CYCLE_WATER:
	FUNCID	#$08de7fc3
        lea     WATER_ANIM_DELAY(a4),a0
        tst.w   2(a0)
        bmi.s   .cycle
        subq.w  #1,2(a0)
        bra     .exit

.cycle:
        move.w  (a0),2(a0)              ; reset timer
        moveq   #WATER_ANIMS,d7

        cmp.w   #WATER_ANIMS-1,WATER_CYCLE_NUM(a4)
        ble.s   .loop
        move.w  #-1,WATER_CYCLE_NUM(a4)


.loop:
        addq.w  #1,WATER_CYCLE_NUM(a4)
        moveq   #0,d0
        move.w  WATER_CYCLE_NUM(a4),d0

        lea     WATER_TABLE(a4),a0
        add.w   d0,d0
        add.w   d0,d0
        add.l   d0,a0           ; index to cycle.
        move.l  COPPTR_WATER(a4),a1

        rept    WATER_ANIMS                     ; Defo correct
        move.l  (a0)+,d0
        move.w  d0,14(a1)
        swap    d0
        move.w  d0,6(a1)
        add.w   #16,a1
        endr

.exit:  rts



