POWER_STAR:		equ	1
POWER_CROWN:	equ	2
POWER_SUN:		equ	4
POWER_CROSS:	equ	8
POWER_TIGER:	equ	16

POINTS_KILLALL:	equ	10000
POINTS_EMBLEM1:	equ	200
POINTS_EMBLEM2:	equ	500
POINTS_EMBLEM3:	equ	1000
POINTS_STAR:	equ	5000
POINTS_7STARS:	equ	70000
POINTS_TREASURE:	equ	1000000
POINTS_ELDER:		equ	150000
POINTS_GIANTDEMON_STOMP:	equ	50000
POINTS_TRIBEMAN_BONUS:	equ	10000
POINTS_SANCTUARY_TIME_BONUS:	equ	10000

ZERO_POINTS:		equ	0
POINTS_APE:		equ	350
POINTS_MONKEY:		equ	150
POINTS_CAVEBAT:		equ	330
POINTS_LIZARD:		equ	100
POINTS_CRAB:		equ	200
POINTS_DRAGON:          equ     1250
POINTS_RIDER:		equ	230
POINTS_DRONE:           equ     100
POINTS_FIREBALL:	equ	150
POINTS_GIANTDEMON:	equ	2000
POINTS_FISH:		equ	150
POINTS_GIANTWORM:	equ	100
POINTS_GRIFFIN:         equ     230
POINTS_LAVAMAN:		equ	230
POINTS_MUTANTFROG:	equ	200
POINTS_TRIBESMAN:	equ	230
POINTS_VILLAGER:	equ	200
POINTS_TENTACLES:	equ	220
POINTS_RHINO:           equ     150
POINTS_BIGRHINO:        equ     760
POINTS_SQUIRREL:        equ     250
POINTS_LIGAR:		equ	100000
POINTS_HAMMER:		equ	300
POINTS_LAVASPIT:	equ	500


TIGER_STUN_STATIC_TIME:		equ	150	; Number of frames enemy is frozen with tiger power
TIGER_STUN_REVIVE_TIME:		equ	50	; Number of frames enemy shakes before being revived.

;d4=Power that was collected
HANDLE_POWERS:
	FUNCID	#$a4a9ad99
	
	and.w	#%1111110000000000,d4
	cmp.w	#ITEM_STARPOWER,d4
	beq	ASSIGN_STAR
	cmp.w	#ITEM_CROWNPOWER,d4
	beq	ASSIGN_CROWN
	cmp.w	#ITEM_TIGERPOWER,d4
	beq	ASSIGN_TIGER
	cmp.w	#ITEM_CROSSPOWER,d4
	beq	ASSIGN_CROSS
	cmp.w	#ITEM_SUNPOWER,d4
	beq	ASSIGN_SUN
	cmp.w	#ITEM_TIME,d4
	beq	INCREASE_TIME
	cmp.w	#ITEM_EXTRA1,d4
	beq	ADD_EXTRA_LIFE
	cmp.w	#ITEM_EXTRA2,d4
	beq	ADD_EXTRA_LIFE
	cmp.w	#ITEM_KILLALL,d4
	beq	ADD_POINTS_AND_DESTROY_ALL_ENEMIES
	cmp.w	#ITEM_EMBLEM1,d4
	beq	ITEM_EMBLEM1_ADD_POINTS
	cmp.w	#ITEM_EMBLEM2,d4
	beq	ITEM_EMBLEM2_ADD_POINTS
	cmp.w	#ITEM_EMBLEM3,d4
	beq	ITEM_EMBLEM3_ADD_POINTS
	cmp.w	#ITEM_STAR,d4
	beq	ITEM_STAR_ADD_POINTS
	cmp.w	#ITEM_TREASURE,d4
	beq	ITEM_TREASURE_ADD_POINTS

	rts
	
ADD_EXTRA_LIFE:
	movem.l	d0-d7/a0-a3,-(a7)
	addq.w	#1,RYGAR_LIVES(a4)
	moveq	#0,d0
	move.w	RYGAR_LIVES(a4),d0
	bsr	BLIT_RYGAR_LIVES
	
	move.w	#MUSIC_POWER_DELAY_TIME,MUSIC_DELAY_TIME(a4)
	moveq	#MUSIC_EXTRA,d0
	bsr	PLAY_TUNE
	movem.l	(a7)+,d0-d7/a0-a3
	rts

ADD_POINTS_AND_DESTROY_ALL_ENEMIES:
	FUNCID	#$5886afcb
	
	move.w	#-1,ITEM_KILLALL_SET(a4)
	add.l	#POINTS_KILLALL,VAL_PLAYER1_SCORE(a4)
	
	move.w	#-1,UPDATE_SCORE(a4)
	
	move.l	d0,-(a7)
	moveq	#SND_DESTROY,d0
	bsr	PLAY_SAMPLE
	move.l	(a7)+,d0
	
	move.w	#-1,STATE_KILL_ALL_ENEMIES(a4)
	bra	DESTROY_ALL_ENEMIES_ONSCREEN

ITEM_EMBLEM1_ADD_POINTS:
	FUNCID	#$bbc72122
	add.l	#POINTS_EMBLEM3,VAL_PLAYER1_SCORE(a4)
	move.w	#-1,UPDATE_SCORE(a4)
	rts

ITEM_EMBLEM2_ADD_POINTS:
	FUNCID	#$ba569118
	add.l	#POINTS_EMBLEM3,VAL_PLAYER1_SCORE(a4)
	move.w	#-1,UPDATE_SCORE(a4)
	rts

ITEM_EMBLEM3_ADD_POINTS:
	FUNCID	#$d3734425
	add.l	#POINTS_EMBLEM3,VAL_PLAYER1_SCORE(a4)
	move.w	#-1,UPDATE_SCORE(a4)
	rts

ITEM_STAR_ADD_POINTS:
	FUNCID	#$241fc1a7
	add.l	#POINTS_STAR,VAL_PLAYER1_SCORE(a4)
	move.w	#-1,UPDATE_SCORE(a4)
	addq.w	#1,BONUS_COLLECTED_STARS(a4)
	
	cmp.w	#6,BONUS_COLLECTED_STARS(a4)			; Get 70,000 bonus for collecting 7 stars.
	bne.s	.exit
	add.l	#POINTS_7STARS,VAL_PLAYER1_SCORE(a4)
	move.w	#-1,UPDATE_SCORE(a4)	
	clr.w	BONUS_COLLECTED_STARS(a4)
	
	lea	TEXT_FLASH_10(a4),a0				
	move.l	a0,TEXT_BONUS_POINTER(a4)
	move.w	#300,BONUS_DISPLAY_TIMER(a4)
	bsr	CREATE_BONUS_TEXT
	
	move.l	d0,-(a7)
	move.w	#MUSIC_POWER_DELAY_TIME,MUSIC_DELAY_TIME(a4)
	moveq	#MUSIC_BONUS_POINTS,d0
	bsr	PLAY_TUNE
	move.l	(a7)+,d0
.exit:	rts


CHECK_SANCTUARY_TIMER_BONUS:	
	moveq	#0,d5
	move.w  TIME_REMAINING(a4),d5
        bpl.s	.plus
	moveq	#0,d5
.plus:	lea     TXT_TIMER_SEC(a4),a0
	and.w	#%1111111111000000,d5
        lsr.w   #4,d5                           ; Seconds value
        add.l   d5,a0
	clr.w	$240
	
	move.b	2(a0),d5			; Get 	
	lea	TXT_PLAYER1_SCORE(a4),a1

	cmp.b	5(a1),d5
	bne.s	.exit
; Bonus achieved.
	clr.w	INPUT_DISABLED(a4)
	bsr	JOY_DETECT
	move.w	#-1,INPUT_DISABLED(a4)
	cmp.w	#JOY1_DOWN_LEFT,d3		; Holding down left?
	bne.s	.exit

	add.l	#POINTS_SANCTUARY_TIME_BONUS,VAL_PLAYER1_SCORE(a4)
	move.w	#-1,UPDATE_SCORE(a4)
	
	lea	TEXT_FLASH_8(a4),a0				
	move.l	a0,TEXT_BONUS_POINTER(a4)
	move.w	#200,BONUS_DISPLAY_TIMER(a4)
	bsr	CREATE_BONUS_TEXT

.exit:
	rts


ADD_TRIBESMAN_BONUS:
	FUNCID	#$241fc1a7
	add.l	#POINTS_TRIBEMAN_BONUS,VAL_PLAYER1_SCORE(a4)
	move.w	#-1,UPDATE_SCORE(a4)
	
	lea	TEXT_FLASH_8(a4),a0				
	move.l	a0,TEXT_BONUS_POINTER(a4)
	move.w	#150,BONUS_DISPLAY_TIMER
	bsr	CREATE_BONUS_TEXT
	
	move.l	d0,-(a7)
	move.w	#MUSIC_POWER_DELAY_TIME,MUSIC_DELAY_TIME(a4)
	moveq	#MUSIC_BONUS_POINTS,d0
	bsr	PLAY_TUNE
	move.l	(a7)+,d0
.exit:	rts

ITEM_ELDER_ADD_POINTS:
	or.w	#POWER_STAR,RYGAR_CURRENT_POWERS(a4)			; Assign STAR power
	moveq	#1,d0
	moveq	#0,d1
	bsr	BLIT_RYGAR_POWER
	
	or.w	#POWER_CROWN,RYGAR_CURRENT_POWERS(a4)			; Assign CROWN power
	moveq	#1,d0
	moveq	#1,d1
	bsr	BLIT_RYGAR_POWER	
	
	or.w	#POWER_TIGER,RYGAR_CURRENT_POWERS(a4)			; Assign TIGER power
	moveq	#1,d0
	moveq	#2,d1
	bsr	BLIT_RYGAR_POWER
	
	or.w	#POWER_SUN,RYGAR_CURRENT_POWERS(a4)			; Assign SUN power
	moveq	#1,d0
	moveq	#4,d1
	bsr	BLIT_RYGAR_POWER

	lea	TEXT_FLASH_15(a4),a0
	move.l	a0,TEXT_BONUS_POINTER(a4)
	move.w	#300,BONUS_DISPLAY_TIMER
	bsr	CREATE_BONUS_TEXT
	
	add.l	#POINTS_ELDER,VAL_PLAYER1_SCORE(a4)
	move.w	#-1,UPDATE_SCORE(a4)
	move.w	#MUSIC_PARCHMENT_DELAY_TIME,MUSIC_DELAY_TIME(a4)
	moveq	#MUSIC_PARCHMENT,d0
	bsr	PLAY_TUNE
.exit:	rts

SHOW_ROUND_PASSWORD:

	tst.w	ENABLE_CHEATS(a4)
	bmi.s	.show
	
	move.w	ROUND_CURRENT(a4),d0
	and.w	#3,d0				
	bne.s	.exit				; only rounds 4,8,12,16,20,24,28 (unless cheats).
	
	
.show:	lea	ROUND_PASSWORDS(a4),a1
	moveq	#0,d0
	move.w	ROUND_CURRENT(a4),d0
	lsl.w	#3,d0
	add.l	d0,a1
	lea	TEXT_FLASH_16(a4),a0
	move.b	(a1),21(a0)
	move.b	1(a1),22(a0)
	move.b	2(a1),23(a0)
	move.b	3(a1),24(a0)
	move.b	4(a1),25(a0)	
	move.l	a0,TEXT_BONUS_POINTER(a4)
	move.w	#300,BONUS_DISPLAY_TIMER(a4)
	bsr	CREATE_BONUS_TEXT
.exit:	rts

ITEM_TREASURE_ADD_POINTS:
	lea	TEXT_FLASH_1(a4),a0
	move.l	a0,TEXT_BONUS_POINTER(a4)
	move.w	#200,BONUS_DISPLAY_TIMER
	bsr	CREATE_BONUS_TEXT
	
	add.l	#POINTS_TREASURE,VAL_PLAYER1_SCORE(a4)
	move.w	#-1,UPDATE_SCORE(a4)
	move.w	#MUSIC_PARCHMENT_DELAY_TIME,MUSIC_DELAY_TIME(a4)
	moveq	#MUSIC_PARCHMENT,d0
	bsr	PLAY_TUNE
	rts

INCREASE_TIME:
	FUNCID	#$e9914ca9
	tst.w	TIME_REMAINING(a4)
	bmi.s	.time_out
	add.w	#(10*50),TIME_REMAINING(a4)
.time_out:	rts

ASSIGN_STAR:
	FUNCID	#$22db2282
	or.w	#POWER_STAR,RYGAR_CURRENT_POWERS(a4)
	moveq	#1,d0
	moveq	#0,d1
	bsr	BLIT_RYGAR_POWER
	
	move.w	#MUSIC_POWER_DELAY_TIME,MUSIC_DELAY_TIME(a4)
	moveq	#MUSIC_POWER,d0
	bsr	PLAY_TUNE
	rts

ASSIGN_CROWN:
	FUNCID	#$41de0f21
	or.w	#POWER_CROWN,RYGAR_CURRENT_POWERS(a4)
	moveq	#1,d0
	moveq	#1,d1
	bsr	BLIT_RYGAR_POWER
	
	move.w	#MUSIC_POWER_DELAY_TIME,MUSIC_DELAY_TIME(a4)
	moveq	#MUSIC_POWER,d0
	bsr	PLAY_TUNE
	rts

ASSIGN_TIGER:
	FUNCID	#$bd16c4a8
	or.w	#POWER_TIGER,RYGAR_CURRENT_POWERS(a4)
	moveq	#1,d0
	moveq	#2,d1
	bsr	BLIT_RYGAR_POWER

	move.w	#MUSIC_POWER_DELAY_TIME,MUSIC_DELAY_TIME(a4)
	moveq	#MUSIC_POWER,d0
	bsr	PLAY_TUNE
	rts

ASSIGN_CROSS:
	FUNCID	#$b0ef6e48
	or.w	#POWER_CROSS,RYGAR_CURRENT_POWERS(a4)
	moveq	#1,d0
	moveq	#3,d1
	bsr	BLIT_RYGAR_POWER

	move.w	#64*15,MUSIC_DELAY_TIME(a4)	
	move.w	#64*15,BONUS_DISPLAY_TIMER(a4)
	move.w	#15,SHIELD_TIMER(a4)			; Shield timer
	
	
	lea	TEXT_FLASH_14(a4),a0
	move.l	a0,TEXT_BONUS_POINTER(a4)
	bsr	CREATE_BONUS_TEXT
	moveq   #SPR_RESERVED_SHIELD,d0
        bsr    	ENABLE_SPRITE
	
	moveq	#MUSIC_SHIELD,d0
	bsr	PLAY_TUNE
	rts

ASSIGN_SUN:
	FUNCID	#$4dd8f6a9
	or.w	#POWER_SUN,RYGAR_CURRENT_POWERS(a4)
	moveq	#1,d0
	moveq	#4,d1
	bsr	BLIT_RYGAR_POWER
	
	move.w	#MUSIC_POWER_DELAY_TIME,MUSIC_DELAY_TIME(a4)
	moveq	#MUSIC_POWER,d0
	bsr	PLAY_TUNE
	rts


; Copy Decimal
; a0=Source text
; a1=Dest buffer
; d0=length
COPY_DECIMAL:
	subq.w	#1,d0
	bmi.s	.exit
	move.l	#$20202020,(a1)+
	move.l	#$20202020,(a1)+
	sub.l	d0,a1
	subq.w	#1,a1
.loop:	move.b	(a0)+,(a1)+
	dbf	d0,.loop
.exit:	rts

; *********************************************
;
; $VER:	Binary2Decimal.s 0.2b (22.12.15)
;
; Author: 	Highpuff
; Orginal code: Ludis Langens
;
; In:	D0.L = Hex / Binary
;
; Out:	A0.L = Ptr to null-terminated String
;	D0.L = String Length (Zero if null on input)
;
; *********************************************


b2dNegative	equ	0	; 0 = Only Positive numbers
				; 1 = Both Positive / Negative numbers

; *********************************************

Binary2Decimal:	movem.l	d1-d5/a1,-(sp)

		moveq	#0,d1		; Clear D1/2/3/4/5
		moveq	#0,d2
		moveq	#0,d3
		moveq	#0,d4
		moveq	#0,d5

		lea.l	b2dString+12(a4),a0
		movem.l	d1-d3,-(a0)	; Clear String buffer

		neg.l	d0		; D0.L ! D0.L = 0?
		bne	.notZero	; If NOT True, Move on...
		move.b	#$30,(a0)	; Put a ASCII Zero in buffer
		moveq	#1,d0		; Set Length to 1
		bra	.b2dExit	; Exit	
		
.notZero:	neg.l	d0		; Restore D0.L

.lftAlign:	addx.l	d0,d0		; D0.L = D0.L << 1
		bcc.s	.lftAlign	; Until CC is set (all trailing zeros are gone)

.b2dLoop:	abcd.b	d1,d1		; xy00000000
		abcd.b	d2,d2		; 00xy000000
		abcd.b	d3,d3		; 0000xy0000
		abcd.b	d4,d4		; 000000xy00
		abcd.b	d5,d5		; 00000000xy
		add.l	d0,d0		; D0.L = D0.L << 1
		bne.s	.b2dLoop	; Loop until D0.L = 0
	
		; Line up the 5x Bytes

		lea.l	b2dTemp(a4),a1	; A1.L = b2dTemp Ptr
		move.b	d5,(a1)		; b2dTemp = d5.xx.xx.xx.xx
		move.b	d4,1(a1)	; b2dTemp = d5.d4.xx.xx.xx
		move.b	d3,2(a1)	; b2dTemp = d5.d4.d3.xx.xx
		move.b	d2,3(a1)	; b2dTemp = d5.d4.d3.d2.xx
		move.b	d1,4(a1)	; b2dTemp = d5.d4.d3.d2.d1


		; Convert Nibble to Byte
		
		moveq	#5-1,d5		; 5 bytes (10 Bibbles) to check
.dec2ASCII:	move.b	(a1)+,d1	; D1.W = 00xy
		ror.w	#4,d1		; D1.W = y00x
		move.b	d1,(a0)+	; Save ASCII
		sub.b	d1,d1		; D1.B = 00
		rol.w	#4,d1		; D1.W = 000y
		move.b	d1,(a0)+	; Save ASCII
		dbf	d5,.dec2ASCII	; Loop until done...

		sub.l	#10,a0		; Point to first byte (keep "-" if it exists)
		move.l	a0,a1

		; Find where the numbers start and trim it...

		moveq	#10-1,d5	; 10 Bytes total to check
.trimZeros:	move.b	(a0),d0		; Move byte to D0.B
		bne.s	.trimSkip	; Not Zero? Exit loop
		add.l	#1,a0		; Next Character Byte
		dbf	d5,.trimZeros	; Loop
.trimSkip:	move.b	(a0)+,d0	; Move Number to D0.B
		add.b	#$30,d0		; Add ASCII Offset to D0.B
		move.b	d0,(a1)+	; Move to buffer
		dbf	d5,.trimSkip	; Loop

		; Get string length

		move.l	a1,d0		; D0.L = EOF b2dString
		lea.l	b2dString(a4),a0	; A0.L = SOF b2dString
		sub.l	a0,d0		; D0.L = b2dString.Length

.b2dExit:	movem.l	(sp)+,d1-d5/a1
		rts



