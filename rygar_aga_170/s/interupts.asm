; code-snippet by Noname 24.11.2004 
; taken and stripped from WickedOS 
; 
; this is not a startup-code. this is just a code-snippet 
; that demonstrates how to set up a working level 3 interrupt. 
; works with moved vector base table (68010+) while 
; still maintaining 68000 compatibility. 
; comes with example interrupt-code that increments a timer. 
; 
; hasn't been assembled for testing so bugs my lurk around. 
; 

InstallExceptionHandlers:
	move.l	a5,-(a7)
;--- get the vbr which is most probably somewhere in fast-ram 
	lea	getvbr(pc),a5 
	move.l	$4.w,a6
	jsr	-30(a6)			; returns vbr in d0 
	lea	vbroffset(pc),a0 
	move.l	d0,(a0)			; store vbr 
	move.l	d0,a0

;--- you could store some old values here 
;... 

;--- register our own level 3 interrupt (vertical blanc and blitter) 
	lea 	ExeptionAddressError(pc),a1 
	move.l	a1,$c(a0)
	lea 	ExeptionIllegal(pc),a1 
	move.l	a1,$10(a0)
	lea 	ExceptionDivBy0(pc),a1 
	move.l	a1,$14(a0)
	lea 	ExceptionChk(pc),a1 
	move.l	a1,$18(a0)
	;lea 	ExceptionPriv(pc),a1 
	;move.l	a1,$20(a0)	
	
	move.l	(a7)+,a5
.exit	rts 


ExeptionAddressError:
	move.w	#$ffa1,DEBUG_EXCEPTION
	CRASH
.crash:	bra.s	.crash

ExeptionIllegal:
	move.w	#$ffa2,DEBUG_EXCEPTION
	CRASH
.crash:	bra.s	.crash

ExceptionDivBy0:
	move.w	#$ffa3,DEBUG_EXCEPTION
	CRASH
.crash:	bra.s	.crash

ExceptionChk:
	move.w	#$ffa4,DEBUG_EXCEPTION
	CRASH
.crash:	bra.s	.crash

ExceptionPriv:
	move.w	#$ffa5,DEBUG_EXCEPTION
	CRASH
.crash:	bra.s	.crash

	CNOP	0,4

oncrash_savea5:	dc.l	0

oncrash:	ds.l	16

	CNOP	0,4

InstallLevel3Int:
	move.l	a5,-(a7)
;--- get the vbr which is most probably somewhere in fast-ram 
	lea	getvbr(pc),a5 
	move.l	$4.w,a6
	jsr	-30(a6)			; returns vbr in d0 
	lea	vbroffset(pc),a0 
	move.l	d0,(a0)			; store vbr 
	move.l	d0,a0

;--- register our own level 3 interrupt (vertical blanc and blitter) 
	lea 	Level3Int(pc),a1 
	move.l	a1,$6c(a0)
	
	move.l	vbroffset(pc),a0
	lea	irq5_old(pc),a1		; Save original irq5 handler.
	move.l	$74(a0),(a1)
	
	lea 	irq5(pc),a1 		; Install new irq handler.
	move.l	a1,$74(a0)
	
	move.l	(a7)+,a5
.exit	rts 

;--- get the vector base register 
getvbr:	movec	vbr,d0 
	;dc.l	$4e7a0801	; this is the opcode for the above command 
	rte 

vbroffset:	dc.l	0 

irq5_old:	dc.l	0

	cnop	0,4


Level3Int: 
	movem.l	d0-d7/a0-a6,-(a7)
	lea	data,a4
	lea	CHIPBASE,a5
	move.w	INTREQR(a5),d0			; intreq-read 
	btst	#5,d0				; Vertical Blank? 
	bne.s	.vertb 
	btst	#4,d0				; Copper
	bne	.copper 
	bra	.quitL3 

.vertb:
	lea	VerticalBlank(pc),a0
	move.w	#-1,(a0)

	bsr	DEBUG_KEY


	moveq	#0,d2
        move.b	Key+1(pc),d0
	
	lea	.key_done(pc),a2

	cmp.b	#27,d0				; Escape
	beq	KEY_ESCAPE
	cmp.b	#'p',d0				; Pause Game
	beq	KEY_PAUSE
	
	tst.w	ENABLE_CHEATS(a4)		; If cheats are on then accept
	beq.s	.key_done			; additonal keys
	cmp.b	#'e',d0				; Destry all enemies
	beq	KEY_ENEMIES_TOGGLE
	cmp.b	#'n',d0				; Next round
	beq	KEY_GOTO_NEXT_ROUND
	cmp.b	#-7,d0				
	beq	KEY_ASSIGN_STAR
	cmp.b	#-8,d0
	beq	KEY_ASSIGN_CROWN
	cmp.b	#-9,d0
	beq	KEY_ASSIGN_TIGER
	cmp.b	#-10,d0
	beq	KEY_ASSIGN_CROSS
	cmp.b	#-11,d0
	beq	KEY_ASSIGN_SUN	
	cmp.b	#-12,d0
	beq	KEY_EXTRA_LIFE
	
	
	IFNE	DEBUG_TEXT_ENABLE
	lea     DEBUG_TEMPLATE_POINTERS,a3
	
.key_loop:
	move.l  4(a3,d2*8),d1                            ; a0 now points to
	bmi.s	.key_done
	cmp.b	d0,d1
	beq.s	.key_found
	addq.w	#1,d2
	bra.s	.key_loop
	
.key_found:
	IFNE    DEBUG_TEXT_ENABLE
	move.w	d2,DEBUG_CURRENT_FOCUS
        bsr     DEBUG_TEXT
        ENDC
	
	ENDC
	
.key_done:
	move.w	#$4020,INTREQ(a5)   		;0100000000100000	; Clear vertical blank interupt.
	move.w	#$4020,INTREQ(a5)
	bra.w	.quitL3 

.copper:
	tst.w	DISABLE_ENEMIES(a4)
	bmi.s	.no_enemies
	tst.w	PAUSE_ENEMIES(a4)
	bmi.s	.no_enemies
	cmp.w	#COPRUN_GAME,COPRUN_TYPE(a4)
	bne.s	.no_enemies
; Only run this when the main game copper is running.
	bsr     FADE_BACKGROUND
	nop
	
.no_enemies:
	IFNE    DEBUG_TEXT_ENABLE
        IFNE	DEBUG_TEXT_ENABLE_EVERY_FRAME
	bsr     DEBUG_TEXT
	ENDC
        ENDC
	
	move.w	#$4010,INTREQ(a5)   		;0100000000100000	; Clear vertical blank interupt.
	move.w	#$4010,INTREQ(a5)
	bra.w	.quitL3 
	nop
	
.quitL3: 
	movem.l	(a7)+,d0-d7/a0-a6
	nop 
	rte 
	
KEY_ESCAPE:
	movem.l	d0-d7/a0-a3,-(a7)
	subq.w	#1,QUIT_GAME(a4)
	movem.l	(a7)+,d0-d7/a0-a3
	jmp	(a2)
	
KEY_PAUSE:
	movem.l	d0-d7/a0-a3,-(a7)
	subq.w	#1,PAUSE_GAME(a4)

	lea	TEXT_PAUSE(a4),a0
	move.l	a0,TEXT_BONUS_POINTER(a4)
	move.w	#4,BONUS_DISPLAY_TIMER(a4)
	bsr	CREATE_BONUS_TEXT	

.exit:	movem.l	(a7)+,d0-d7/a0-a3
	jmp	(a2)
	
KEY_ASSIGN_STAR:
	movem.l	d0-d7/a0-a3,-(a7)
	bsr	ASSIGN_STAR
	movem.l	(a7)+,d0-d7/a0-a3
	jmp	(a2)
	
KEY_ASSIGN_CROWN:
	movem.l	d0-d7/a0-a3,-(a7)
	bsr	ASSIGN_CROWN
	movem.l	(a7)+,d0-d7/a0-a3
	jmp	(a2)

KEY_ASSIGN_CROSS:
	movem.l	d0-d7/a0-a3,-(a7)
	bsr	ASSIGN_CROSS
	movem.l	(a7)+,d0-d7/a0-a3
	jmp	(a2)
	
KEY_ASSIGN_SUN:
	movem.l	d0-d7/a0-a3,-(a7)
	bsr	ASSIGN_SUN
	movem.l	(a7)+,d0-d7/a0-a3
	jmp	(a2)
	
KEY_ASSIGN_TIGER:
	movem.l	d0-d7/a0-a3,-(a7)
	bsr	ASSIGN_TIGER
	movem.l	(a7)+,d0-d7/a0-a3
	jmp	(a2)
	
KEY_ENEMIES_TOGGLE:
	movem.l	d0-d7/a0-a3,-(a7)
	bsr	DESTROY_ALL_ENEMIES
	movem.l	(a7)+,d0-d7/a0-a3
	jmp	(a2)

	
KEY_GOTO_NEXT_ROUND:
	movem.l	d0-d7/a0-a3,-(a7)
	bsr	DESTROY_ALL_ENEMIES_ONSCREEN
	move.w	ROUND_ENDPOINT(a4),MAP_PIXEL_POSX(a4)
	
.check_round_5:
	cmp.w	#5-1,ROUND_CURRENT(a4)
	bne.s	.check_round_9
	move.w	#2<<4,MAP_PIXEL_POSY(a4)
	move.w	#2,MAP_POSY(a4)
	bra	.exit
.check_round_9
	cmp.w	#9-1,ROUND_CURRENT(a4)
	bne.s	.check_round_11
	move.w	#48<<4,MAP_PIXEL_POSY(a4)
	move.w	#48,MAP_POSY(a4)
	bra	.exit	
.check_round_11:
	cmp.w	#11-1,ROUND_CURRENT(a4)
	bne.s	.check_round_23
	move.w	#0<<4,MAP_PIXEL_POSY(a4)
	move.w	#0,MAP_POSY(a4)
	bra	.exit
.check_round_23:
	cmp.w	#23-1,ROUND_CURRENT(a4)
	bne.s	.check_round_24
	move.w	#47<<4,MAP_PIXEL_POSY(a4)
	move.w	#47,MAP_POSY(a4)
	bra	.exit
.check_round_24:
	cmp.w	#24-1,ROUND_CURRENT(a4)
	bne.s	.exit
	move.w	#0<<4,MAP_PIXEL_POSY(a4)
	move.w	#0,MAP_POSY(a4)
	bra	.exit
	nop
.exit:	movem.l	(a7)+,d0-d7/a0-a3
	jmp	(a2)
	
KEY_EXTRA_LIFE:
	movem.l	d0-d7/a0-a3,-(a7)
	addq.w	#1,RYGAR_LIVES(a4)
	moveq	#0,d0
	move.w	RYGAR_LIVES(a4),d0
	bsr	BLIT_RYGAR_LIVES
	movem.l	(a7)+,d0-d7/a0-a3
	jmp	(a2)
	

; read ext. ASCII key code-->d0.0=no key!auto-fixes shift etc.
DEBUG_KEY:      lea	$DFF002,a6
		moveq   #0,d0
                lea     Key(pc),a1
                btst    #3,$1f-2(a6)
                beq.s   .End
                lea     $bfed01,a0
                btst    #3,(a0)
                beq.s   .End                    ;key pressed down?
                clr.b   (a0)                    ;nec?
                moveq   #0,d1
                move.b  -$100(a0),d1            ;read key
                move.b  #$50,$100(a0)           ;output+force load (start handshake)
                lea     6-2(a6),a2                      ;<-for spd/Size only
                move.b  (a2),d2                 ;wait for 75 microsecs=60 or so cycles
                not.b   d1
                lsr.b   #1,d1
                bcc.s   .Pres
                moveq   #-1,d0                  ;neg=released!
.Pres:          move.w  Shift(PC),d3
                bpl.s   .Shift
                move.w  Caps(PC),d3
                bmi.s   .NoSh
.Shift:         add.w   #KeyTblS-KeyTbl,d1
.NoSh:          move.b  KeyTbl-Key(a1,d1.w),d0  ;fetch ASCII equivalent
                moveq   #0,d1
                move.b  d0,d1
                bpl.s   .WLup1                  ;special key is neg!
                addq.b  #5,d1                   ;less than -5?
                bmi.s   .WLup1                  ;then no special key
                add.w   d1,d1
                move.w  d0,2(a1,d1.w)           ;put state in corr. key-slot (-=OFF!)
.WLup1:         cmp.b   (a2),d2                 ;QikFix:wait AT LEAST 1 scanline
                beq.s   .WLup1
                move.b  (a2),d2                 ;make this into an int later!
.WLup2:         cmp.b   (a2),d2
                beq.s   .WLup2
		
		;clr.w	$220

                move.b  #$10,$100(a0)           ;input+force load (handshake done)
                move.w  #8,$9c-2(a6)            ;clear lev2-intreq
.End:           move.w  d0,(a1)
                rts
		
		
		
; Ross' 2nd button handler.
irq5	        movem.l	d0/a5-a6,-(sp)
		lea	$dff000,a6
		lea	Button2(pc),a5

.rbtn	        moveq	#0,d0
		btst	#14-8,$16(a6) ; read 2nd joystick button
		seq	d0

		btst	#12-8,$1e(a6) ; normal or 'alternative'?
		beq.b	.rbf

.syn	        move.w	d0,(a5)       ; store normal
		move.w	#$5f00,d0     ; setup alternative
		bra.b	.set

.rbf	        or.w	d0,(a5)       ; add 'alternative'
		move.w	#$ff00,d0     ; setup normal

.set	        move.w	d0,$34(a6)    ; setup POTGO

		move.w	#$1800,$9c(a6) ; clear (both) IRQ5
		move.w	#$8400,$96(a6) ; reenable BLITPRI
		movem.l	(sp)+,d0/a5-a6 ; sequence must be this, to avoid problems in some fast machines for the usual question of IPL propagation
		rte

;d0/[key]=Ascii key

Key:            dc.w    0
Caps:           dc.w    -1
Amiga:          dc.w    -1                              ;special keys state
Alt:            dc.w    -1                              ;-=OFF(!)
Shift:          dc.w    -1
Ctrl:           dc.w    -1                              ;DONT SEP ^5!

KeyTbl:                                 ;clr unused keybytes for final opti.
                dc.b    "`1234567890-=\",0,"0"
                dc.b    "qwertyuiop[]",0,"123"
                dc.b    "asdfghjkl;'",0,0,"456"
                dc.b    0,"zxcvbnm,./",0,".789"
                dc.b    " ",8,9,13,13,27,127,0,0,0,"-",0,31,30,29,28    ;28-31=cursor keys
                dc.b    -7,-8,-9,-10,-11,-12,-13,-14,-15,-16,"()/*+",-6 ;F-keys,Help
                dc.b    -2,-2,-5,-1,-3,-3,-4,-4                         ;Shift,Ctrl,Alt,<A>
                dc.b    128,129,130,131,132,133,134,135
                dc.b    136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151
KeyTblS:                                                                ;SHIFTED
                dc.b    "~!@#$%^&*()_+|",0,"0"
                dc.b    "QWERTYUIOP{}",0,"123"
                dc.b    "ASDFGHJKL:",34,0,0,"456"
                dc.b    0,"ZXCVBNM<>?",0,".789"
                dc.b    " ",8,9,13,13,27,127,0,0,0,"-",0,31,30,29,28    ;28-31=cursor keys
                dc.b    -7,-8,-9,-10,-11,-12,-13,-14,-15,-16,"()/*+",-6 ;F-keys,Help
                dc.b    -2,-2,-5,-1,-3,-3,-4,-4                         ;Shift,Ctrl,Alt,<A>
                dc.b    128,129,130,131,132,133,134,135
                dc.b    136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151

		CNOP	0,4
		
;--- this Timer is updated once a frame. 
; there are 50 FPS on a PAL Amiga, so this variable has a resolution 
; of 1/50 s 
; or 20/1000 s 
; or 20 ms 
Timer:	dc.l	1
VerticalBlank:	dc.w	0
Button2:	ds.w	1
