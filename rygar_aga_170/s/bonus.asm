
BONUS_YPOS:	equ	32

CBT
CREATE_BONUS_TEXT:
	move.l	MEMCHK9_BONUS_IMAGE(a4),a1	
	moveq	#0,d0
	moveq	#0,d1
	move.l	TEXT_BONUS_POINTER(a4),a0	
	bsr	DRAW_SP_BONUS_TEXT
	
	rts
	
DISPLAY_BONUS_TEXT:
; If the shield is enabled then draw it here.
	tst.w	SHIELD_TIMER(a4)
	bmi.s	.exit
	move.w	FRAME_BG(a4),d0
	and.w	#%111111,d0
	bne.s	.skip
	subq.w	#1,SHIELD_TIMER(a4)
	
.skip:
	moveq	#0,d0
	move.w	SHIELD_TIMER(a4),d0
	lea     TXT_SHIELD_SEC(a4),a0
	lea	(a0,d0*4),a0

	move.l	MEMCHK9_BONUS_IMAGE(a4),a1
        moveq   #18,d0
        moveq   #0,d1
        bsr     DRAW_SP_BONUS_TEXT
	
.exit:	rts
	
HDL_BONUS_TEXT:
		tst.w	BONUS_DISPLAY_TIMER(a4)
		beq.s	.remove
		bmi	.exit
		
		
		subq.w	#1,BONUS_DISPLAY_TIMER(a4)
		move.l	LSTPTR_CURRENT_SCREEN(a4),d1
		bsr	.blit_bonus
		bra	.exit
		
.remove:	move.w	#-1,BONUS_DISPLAY_TIMER(a4)
		lea	TEXT_FLASH_0(a4),a0
		move.l	a0,TEXT_BONUS_POINTER(a4)
		bsr	CREATE_BONUS_TEXT
		
		move.l	LSTPTR_FRONT_SCREEN(a4),d1
		bsr	.blit_bonus
		move.l	LSTPTR_BACK_SCREEN(a4),d1
		bsr	.blit_bonus
		
		move.w	#-1,BONUS_RESTORE_SCREEN(a4)
		bra	.exit
	
.blit_bonus:
		movem.l a0-a3,-(a7)

		move.l	d1,a1					; Screen Address
		move.l  MEMCHK9_BONUS_IMAGE(a4),a2		; Image
		move.l  MEMCHK9_BONUS_IMAGE(a4),a3		; Mask

		moveq	#0,d0
		add.w   MAP_POSX(a4),d0		
		add.l   #(PLAYFIELD_SIZE_X*5)*BONUS_YPOS,d0
		add.l	d0,a1
		move.l	d0,BONUS_TEXT_OFFSET(a4)

		moveq	#0,d0
		moveq	#0,d1
		move.w  SCROLL_HPOS(a4),d0
		not.w   d0
		and.w   #$1f,d0
		move.w	d0,d1
		and.w	#$f,d0
		lsr.w	#3,d1
		add.l	d1,a1
		ror.w	#4,d0
		move.w	d0,d1
		or.w	#$fca,d0
		

		WAIT_FOR_BLITTER
		move.w  d0,BLTCON0(a5)
		move.w  d1,BLTCON1(a5)
		move.l  #-1,BLTAFWM(a5) ; Only want the first word

		move.w  #8,BLTAMOD(a5)
		move.w  #8,BLTBMOD(a5)
		move.w  #8,BLTCMOD(a5)
		move.w  #8,BLTDMOD(a5)

		move.l  a3,BLTAPTH(a5)          ; Load the mask address
		move.l  a2,BLTBPTH(a5)          ; Sprite address
		move.l  a1,BLTCPTH(a5)          ; Destination background
		move.l  a1,BLTDPTH(a5)
		move.w  #(40*64)+16,BLTSIZE(a5)
		movem.l (a7)+,a0-a3

.exit:		rts
		
REST_BONUS_TEXT:
		tst.w	BONUS_RESTORE_SCREEN(a4)
		bmi.s	.final_restore
		tst.w	BONUS_DISPLAY_TIMER(a4)
		bmi	.exit
.final_restore:
		clr.w	BONUS_RESTORE_SCREEN(a4)
		move.l  LSTPTR_POST_SCREEN(a4),a1
		move.l  LSTPTR_CURRENT_SCREEN(a4),a2
		add.l	BONUS_TEXT_OFFSET,a1
		add.l	BONUS_TEXT_OFFSET,a2
		
		WAIT_FOR_BLITTER
		move.w  #$09f0,BLTCON0(a5)
		move.w  #$0,BLTCON1(a5)
		move.l  #-1,BLTAFWM(a5) ; Only want the first word

		move.w  #0,BLTAMOD(a5)
		move.w  #0,BLTDMOD(a5)

		move.l  a1,BLTAPTH(a5)          ; Load the mask address
		move.l  a2,BLTDPTH(a5)
		move.w  #(40*64)+20,BLTSIZE(a5)
.exit:		rts
	

DRAW_SHIELD_TEXT:
	tst.w	SHIELD_TIMER(a4)
	bmi.s	.exit
	lea     TXT_SHIELDTIME_TEXT(a4),a0
        moveq   #20,d0
        moveq   #0,d1
        bsr     DRAW_SP_BONUS_TEXT
.exit:
	rts
	
	
; d0=8x8 x
; d1=8x8 y

; a0=text
; a1=Screen addresses
DRAW_SP_BONUS_TEXT:
	FUNCID	#$6d4b5ac1
	
        movem.l d0-d7/a0-a3,-(a7)
	lea	TAB_Y(a4),a2
	add.w	(a2,d1*2),a1
        add.l   d0,a1

        moveq   #0,d0
.loop:  move.l  FONT_ASSET(a4),a2
        add.w   #86,a2                  ; Offset to BODY
        move.b  (a0)+,d0
	beq	.exit
        sub.b  	#32,d0

.draw:
        add.l   d0,a2           ; index into character

	move.l	a1,a3

        move.b  (a2),d0
        move.b  FONT_WIDTH*1(a2),d1
        move.b  FONT_WIDTH*2(a2),d2
        move.b  FONT_WIDTH*3(a2),d3
        move.b  FONT_WIDTH*4(a2),d4
        move.b  FONT_WIDTH*5(a2),d5
        move.b  FONT_WIDTH*6(a2),d6
        move.b  FONT_WIDTH*7(a2),d7

	rept	5

        move.b  d0,(a1)
        move.b  d1,PLAYFIELD_SIZE_X*5*1(a1)
        move.b  d2,PLAYFIELD_SIZE_X*5*2(a1)
        move.b  d3,PLAYFIELD_SIZE_X*5*3(a1)
        move.b  d4,PLAYFIELD_SIZE_X*5*4(a1)
        move.b  d5,PLAYFIELD_SIZE_X*5*5(a1)
        move.b  d6,PLAYFIELD_SIZE_X*5*6(a1)
        move.b  d7,PLAYFIELD_SIZE_X*5*7(a1)

	add.w	#PLAYFIELD_SIZE_X,a1

	endr
	
	move.l	a3,a1
	
        addq.w  #1,a1
        bra     .loop

.exit:  movem.l (a7)+,d0-d7/a0-a3
        rts
	
