; d0=Number of bombs bnus

LOADINFO_WINDOW_X: equ     32
LOADINFO_WINDOW_Y: equ     32

LOADINFO_SCORE_STEP:       equ     200
LOADINFO_DELAY_TIME:       equ     10              ; 7 seconds

LOADINFO_WATER_DELAY:      equ     20
LOADINFO_CHAR_DELAY:       equ     2

LOADINFO_SCENE_SIZE_Y:     equ     184
LOADINFO_SCENE_BITPLANES:  equ     5
LOADINFO_SCENE_COLOURS:    equ     32

LOADINFO_WAIT_TIME:        equ     120

LOADINFO_WATER_ANIM_YPOS:  equ     152

LOADINFO_TEXT_LINES:       equ     11

INIT_LOADINFO:
	;bra	.exit1
        move.w  #LOADINFO_WAIT_TIME,LOADINFO_TIMER(a4)
        clr.b   LOADINFO_TEXT_DONE(a4)
        clr.b   LOADINFO_TEXT_XPOS(a4)
        clr.b   LOADINFO_TEXT_YPOS(a4)

        IFEQ    ENABLE_DEBUG
	bsr	WAIT_FOR_VERTICAL_BLANK	
        move.w  #%0000000110000000,DMACON(a5)		; Disable Copper
	bsr	WAIT_FOR_VERTICAL_BLANK
	ENDC

        bsr     INIT_LOADINFO_COPPER

        IFEQ    ENABLE_DEBUG
	;bsr	WAIT_FOR_COPPER
        move.l  MEMCHK2_COPPER1(a4),COP1LCH(a5)
        move.l  MEMCHK2_COPPER1(a4),COP2LCH(a5)
        ;         FEDCBA9876543210
        move.w  #%1000011111000000,DMACON(a5)
        ENDC

        bsr     SETSCR_FBUFF
        lea     LSTPTR_FRONT_SCREEN(a4),a1
        bsr     SET_FRAMEBUFF
.exit1:	rts
	


.vb:
        btst.b  #7,$bfe001
        bne.s   .vb
.wait:  btst.b  #7,$bfe001
        beq.s   .wait
        bra.s   .end

        bra.s   .vb

.end:

.exit:

.exit_LOADINFO:    moveq   #0,d6
.rts:           rts



;a0 = Text
;d6 = Colour (0-3)
DRAW_LOADINFO_LINE:
	moveq	#0,d0			; Position 0.
	move.b	LOADINFO_TEXT_XPOS(a4),d0
	bne.s	.no_advance
	bsr	LOADINFO_LINE_ADVANCE	
.no_advance:
	moveq	#22,d1
.loop:	tst.b	(a0)
	beq.s	.done

	moveq	#0,d7
	bsr	DRAW_LOADINFO_CHAR
	addq.w	#1,a0
	addq.w	#1,d0
	bra.s	.loop
.done:	move.b	d0,LOADINFO_TEXT_XPOS(a4)
	rts


LOADINFO_LINE_ADVANCE:
	movem.l	d0/d1/d7/a0/a1,-(a7)
	move.l	LSTPTR_FRONT_SCREEN(a4),a0
	move.l	a0,a1
	add.w	#(40*5)*8,a1
	
	move.l	#(192-8)*(40*5)/4,d7
.loop:	move.l	(a1)+,(a0)+
	dbf	d7,.loop
	movem.l	(a7)+,d0/d1/d7/a0/a1
	rts
	
	

; d0=8x8 x
; d1=8x8 y
; d6=Colour number 0-3 (0=BP1, 1=BP2, 2=BP3, 3=BP4, 4=BP5)
; d7=-1 Draw on nibble 0=byte
; a0=pointer to char to display
DRAW_LOADINFO_CHAR:
        movem.l d0-d7/a0-a3,-(a7)
        lsl.w   #3,d1
        mulu    #PLAYFIELD_SIZE_X*5,d1
        lsl.w   #2,d6
        lea     LSTPTR_FRONT_SCREEN(a4),a1

        move.l  (a1,d6),a1              ; Bitplane to draw in.

        add.l   d1,a1
        add.l   d0,a1
        sub.l   #(PLAYFIELD_SIZE_X*5)*8,a1

        addq.w  #4,a1

        moveq   #0,d0
        ;lea    FONT_ASSET(a4)+86,a2    ; start of BODY
        move.l  FONT_ASSET(a4),a2
        add.w   #86,a2                  ; Offset to BODY
        move.b  (a0),d0
	cmp.b	#$61,d0
	blt.s	.upper
	sub.b	#$20,d0
.upper:

        sub.b   #32,d0

.draw:  add.l   d0,a2           ; index into character

        move.b  (a2),d0
        or.b    d0,(a1)
        or.b    d0,PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,PLAYFIELD_SIZE_X*4(a1)


        move.b  FONT_WIDTH*1(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*1)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*1)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*1)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*1)+PLAYFIELD_SIZE_X*4(a1)

        move.b  FONT_WIDTH*2(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*2)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*2)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*2)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*2)+PLAYFIELD_SIZE_X*4(a1)

        move.b  FONT_WIDTH*3(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*3)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*3)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*3)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*3)+PLAYFIELD_SIZE_X*4(a1)

        move.b  FONT_WIDTH*4(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*4(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*4)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*4)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*4)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*4)+PLAYFIELD_SIZE_X*4(a1)

        move.b  FONT_WIDTH*5(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*5(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*5)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*5)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*5)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*5)+PLAYFIELD_SIZE_X*4(a1)

        move.b  FONT_WIDTH*6(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*6(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*6)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*6)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*6)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*6)+PLAYFIELD_SIZE_X*4(a1)

        move.b  FONT_WIDTH*7(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*7(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*7)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*7)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*7)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*7)+PLAYFIELD_SIZE_X*4(a1)

.exit:  movem.l (a7)+,d0-d7/a0-a3
        rts

;
; INIT_LOADINFO_COPPER
;
; Initialize the Copper list
;
INIT_LOADINFO_COPPER:
        move.l  MEMCHK2_COPPER1(a4),a1
        move.l  a1,d4

        move.w  #DDFSTRT,(a1)+  ;
        move.w  #$0038,(a1)+    ; 3c
        move.w  #DDFSTOP,(a1)+  ;
        move.w  #$00d0,(a1)+    ; a4

        move.l  #$1c01ff00,(a1)+
        move.l  a1,COPPTR_UPPER_PANEL(a4)
        bsr     INIT_COPPER_SPRITES

        move.l  #$3e0dfffe,(a1)+

        move.w  #FMODE,(a1)+
        move.w  #%0000000000001100,(a1)+

        move.w  #BPLCON0,(a1)+  ; 0             ; This is play area
        move.w  #$0000,(a1)+    ; 2             ; 8 PLanes here
        move.w  #BPLCON1,(a1)+  ; 4
        move.w  #0,(a1)+        ; 6
        move.w  #BPLCON2,(a1)+  ; 8
        move.w  #$00,(a1)+      ; 30
        move.w  #BPL1MOD,(a1)+  ;
        move.w  #$a0,(a1)+      ; a0
        move.w  #BPL2MOD,(a1)+  ;
        move.w  #$a0,(a1)+      ; a0

        move.w  #DIWSTRT,(a1)+  ;
        move.w  #$27a1,(a1)+    ;
        move.w  #DIWSTOP,(a1)+  ;
        move.w  #$20a8,(a1)+    ;

        lea     COPPTR_HARDWARE_UPPER_SPRITE_0(a4),a3
        bsr     CREATE_COPPER_SPRITE_POINTERS

        move.w  #BPLCON0,(a1)+          ; turn off the bitplanes
        move.w  #$0000,(a1)+

; Load Main Palette
        lea     LOADINFO_SCENE_BLACK_PAL(a4),a0
        move.l  #$180,d2
        moveq   #LOADINFO_SCENE_COLOURS-1,d7

        move.l  a1,COPPTR_LOADINFO_SCENE_PAL(a4)

.main_pal:
        move.w  d2,(a1)+
        move.w  (a0)+,(a1)+
        addq.w  #2,d2
        dbf     d7,.main_pal

        move.l  #$4001ff00,(a1)+
        move.w  #BPLCON0,(a1)+  ; 0             ; This is play area
        move.w  #$5200,(a1)+    ; 2

        lea     LSTPTR_CURRENT_SCREEN(a4),a0
        moveq   #LOADINFO_SCENE_BITPLANES-1,d7                     ; number of planes to load
        move.w  #BPL0PTL,d2
        move.w  #BPL0PTH,d3

.plane: move.l  (a0)+,d0
        move.w  d2,(a1)+
        move.w  d0,(a1)+
        swap    d0
        move.w  d3,(a1)+
        move.w  d0,(a1)+
        addq.l  #4,d2
        addq.l  #4,d3
        dbf     d7,.plane

; Blue Line
        move.l  #$4501ff00,(a1)+
        move.l  #$0180008f,(a1)+
        move.l  #$4601ff00,(a1)+
        move.l  #$01800000,(a1)+
	
        move.l  #$01820fff,(a1)+
        move.l  #$01840fff,(a1)+
        move.l  #$01860fff,(a1)+
        move.l  #$01880fff,(a1)+
        move.l  #$018a0fff,(a1)+
        move.l  #$018c0fff,(a1)+
        move.l  #$018e0fff,(a1)+
        move.l  #$01900fff,(a1)+
        move.l  #$01920fff,(a1)+
        move.l  #$01940fff,(a1)+
        move.l  #$01960fff,(a1)+
        move.l  #$01980fff,(a1)+
        move.l  #$019a0fff,(a1)+
        move.l  #$019c0fff,(a1)+
        move.l  #$019e0fff,(a1)+
        move.l  #$01a00fff,(a1)+
        move.l  #$01a20fff,(a1)+
        move.l  #$01a40fff,(a1)+
        move.l  #$01a60fff,(a1)+
        move.l  #$01a80fff,(a1)+
        move.l  #$01aa0fff,(a1)+
        move.l  #$01ac0fff,(a1)+
        move.l  #$01ae0fff,(a1)+
        move.l  #$01b00fff,(a1)+
        move.l  #$01b20fff,(a1)+
        move.l  #$01b40fff,(a1)+
        move.l  #$01b60fff,(a1)+
        move.l  #$01b80fff,(a1)+
        move.l  #$01ba0fff,(a1)+
        move.l  #$01bc0fff,(a1)+
        move.l  #$01be0fff,(a1)+
	
	move.l  #$e801ff00,(a1)+                
        move.l  #$01be003f,(a1)+
        move.l  #$e901ff00,(a1)+                
        move.l  #$01be004f,(a1)+
        move.l  #$ea01ff00,(a1)+                
        move.l  #$01be005f,(a1)+
        move.l  #$eb01ff00,(a1)+                
        move.l  #$01be006f,(a1)+
        move.l  #$ec01ff00,(a1)+               
        move.l  #$01be007f,(a1)+
        move.l  #$ed01ff00,(a1)+                
        move.l  #$01be008f,(a1)+
        move.l  #$ee01ff00,(a1)+                
        move.l  #$01be009f,(a1)+
        move.l  #$ef01ff00,(a1)+                
        move.l  #$01be00af,(a1)+		
        move.l  #$f001ff00,(a1)+
        move.l  #$01be0000,(a1)+

        move.l  #$f101ff00,(a1)+
        move.l  #$0180008f,(a1)+
        move.l  #$f201ff00,(a1)+
        move.l  #$01800000,(a1)+

; Set display properties, modulo, bitplanes...

        ;COPPER_DEBUG_TEXT

	lea	DUMMY_BPLCON0(a4),a3
        move.l  a3,COPPTR_BPLCON(a4)
	
        move.l  #$f301ff00,(a1)+
        move.w  #BPLCON0,(a1)+  ; 0             ; This is play area
        move.w  #$0200,(a1)+    ; 2

        move.l  #$fffffffe,(a1)+
        rts

