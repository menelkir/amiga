MAP_START:      equ     256 ; (start position 16 on Round 1).

ENABLE_WATERFALL:	equ	-1
DISABLE_WATERFALL:	equ	0

ENABLE_MOON_BG:		equ	1
ENABLE_SUNSET_BG:	equ	-1


RESET_ROUND:
	FUNCID	#$1d7cc0db
	
	bsr	INIT_COPPER1
	
        IFEQ    ENABLE_DEBUG
        move.l  MEMCHK2_COPPER1(a4),COP1LCH(a5)
        move.l  MEMCHK2_COPPER1(a4),COP2LCH(a5)
        move.w  #%1000001111101111,DMACON(a5)
	move.w	#%0000000000100000,INTENA(a5)
        ;         FEDCBA9876543210
        bset    #1,$bfe001
        ENDC
		
; Clear Disk Armour Positions
        lea     DISKARM_COORDS(a4),a2           ; Clear disk armour co-ordinates.
        moveq   #-1,d7
        rept    8
        move.l  d7,(a2)+
        endr
	
	move.w	#-1,ROUND_TRANSITION(a4)
        bsr     SET_ALL_ENEMIES_OFFSCREEN
        bsr     PLOT_ALL_SPRITES
        bsr     UPDATE_COPPER_SPRITES

        move.w  #-1,LAST_ENEMY_STATE(a4)
        clr.w   REPULSE_BONUS_NUM(a4)
        bsr     CLEAR_SPRITES_IN_USE
	lea     ENEMY_HWSPR_UPPER_ALLOCATED(a4),a0                      ; Free up hardware sprite
        bsr     DEALLOC_HARDWARE_SPRITES
    	lea     ENEMY_HWSPR_MID_ALLOCATED(a4),a0                      ; Free up hardware sprite
        bsr     DEALLOC_HARDWARE_SPRITES 
    	lea     ENEMY_HWSPR_LOWER_ALLOCATED(a4),a0                      ; Free up hardware sprite
        bsr     DEALLOC_HARDWARE_SPRITES
	
	bsr     DESTROY_ALL_SPRITES
	clr.w   NUM_SPRITES_INPLAY(a4)
	
	lea	TILEMERGE_BUFFERS_LEFT(a4),a0
	lea	TILEMERGE_BUFFERS_RIGHT(a4),a1
	moveq	#16-1,d7
.tbuffers:
	clr.w	(a0)+
	clr.w	(a1)+
	dbf	d7,.tbuffers
	
        bsr     INIT_SPRITE_FREE_LIST
        bsr     INIT_ROUND
        clr.w   NUM_SPRITES_INPLAY(a4)
        lea     GLOBAL_SPRITES(a4),a1                   ; sprites to initialize straight away
        bsr     INIT_SPRITES

        moveq   #SPR_RESERVED_LEFT_CAROUSEL,d0
        bsr     GET_SPRITE_CONTEXT
        move.w  #SANCTUARY_LEFT_CAROUEL_XPOS,SPRITE_PLOT_XPOS(a0)                               ; Set Xposition

        moveq   #SPR_RESERVED_RIGHT_CAROUSEL,d0
        bsr     GET_SPRITE_CONTEXT
        move.w  #SANCTUARY_RIGHT_CAROUEL_XPOS,SPRITE_PLOT_XPOS(a0)

        moveq   #SPR_RESERVED_LEFT_CAROUSEL,d0
        bsr     DISABLE_SPRITE
        moveq   #SPR_RESERVED_RIGHT_CAROUSEL,d0
        bsr     DISABLE_SPRITE
	
        moveq   #SPR_RESERVED_SHIELD,d0
        bsr     DISABLE_SPRITE
	clr.w	SHIELD_STATUS(a4)
	
; Reset Carousel frames
	movem.l	d0-d1,-(a7)
	moveq	#SPR_RYGAR,d0
	moveq	#RYGAR_CAROUSEL,d1
	bsr	RESET_SPRITE_FRAMES
	movem.l	(a7)+,d0-d1

        move.l  MEMCHK1_FRONT_BSCROLL(a4),a0            ; Fill the sky in the back buffer
        bsr     FILL_BG_BITMAP
	
        bsr     PAN_LEFT_TO_PLATFORM
	
	move.w	#-1,MUSIC_DELAY_TIME(a4)

	clr.w	BONUS_DISPLAY_TIMER(a4)
	clr.w	PAUSE_ENEMIES(a4)
	clr.w	SFX_DISABLE_DESTROY_ENEMY(a4)
	move.w	#COPRUN_GAME,COPRUN_TYPE(a4)
        rts
	
	
INIT_ROUND:
	FUNCID	#$26ab3f17
        move.l  MEMCHK1_FRONT_BSCROLL(a4),a0
        move.l  #$a000/4,d7
.clear2:	clr.l   (a0)+
		dbf     d7,.clear2

; Set active player.
        move.w  #PLAYER1,ACTIVE_PLAYER(a4)
        move.w  START_TIME,TIME_REMAINING(a4)
        clr.w   ROUND_GUIDE_PAL_PTR(a4)
        move.w  #100,BONUS_TIMER(a4)

        bsr     GET_FOREGROUND_PALETTE
        bsr     GET_ROUND_BACKGROUND_PALETTE

	IFEQ	ENABLE_DEBUG
	bsr	WAIT_FOR_VERTICAL_BLANK	
        move.w  #%0000000110000000,DMACON(a5)		; Disable Copper
	bsr	WAIT_FOR_VERTICAL_BLANK
	
;	move.w	#%0000000110000000,DMACON(a5)		; Stop Copper
;	nop
;	nop
;	move.w	#%0000000010000000,DMACON(a5)		; Stop Copper
	bsr     LOAD_FOREGROUND_PALETTE		; Load FG Palette
	move.w	#%1000000110000000,DMACON(a5)		; Start Copper
	move.w	#0,COPJMP1(a5)			; Cause Copper restrobe
	ENDC
	
        bsr     INIT_PANELS
	
; Reset Start Scene to beginning
        move.w  #-1,TRANSITION_ACTIVE(a4)
        move.l  MEMCHK1_FRONT_BSCROLL(a4),a0
        bsr     CLEAR_BG_BITPLANES
        move.l  MEMCHK1_FRONT_BSCROLL(a4),a0            ; Fill the sky in the back buffer
        bsr     FILL_BG_BITMAP

        move.w  #MAP_START,START_MAP_PIXEL_POSX(a4)         ; Round 1 starts in an advanced position
; Check here if game has just started here.
	tst.w   ROUND_CURRENT(a4)
        beq.s   .start_round_1
	
	tst.w	START_HIGHER_ROUND(a4)		; Really start at round 1?
	bmi.s	.start_round_1			; no...higher round
 						; yes
	
        clr.w   START_MAP_PIXEL_POSX(a4)                    ; All other rounds don't
	nop
.start_round_1:
        tst.w   RYGAR_LIFE_LOST(a4)                             ; If life has been lost then
        beq.s   .round_start                            ; do the transition
        move.w	ROUND_MAP_START_X(a4),d0
        lsl.w   #4,d0
	move.w	d0,START_MAP_PIXEL_POSX(a4)

.round_start:
	clr.w	START_HIGHER_ROUND(a4)
	move.w	ROUND_MAP_START_Y(a4),MAP_POSY(a4)
	move.w	MAP_POSY(a4),d0
        lsl.w   #4,d0
        move.w  d0,MAP_PIXEL_POSY(a4)       ; Store the actual pixel position offset

	bsr	WAIT_FOR_VERTICAL_BLANK

	bsr     INIT_COPPER1                            ; Restart Copper

	bsr	WAIT_FOR_VERTICAL_BLANK
	
        bsr     SET_START_SCENE
	
	bsr	CLEAR_OBJECTS_FROM_MAPPED_SCREEN_AREA

	bsr     DBUFF	
	
        tst.w   ROUND_CURRENT(a4)                               ; Is this the first round?
        beq.s   .first_round
	tst.w	MAP_PIXEL_POSX(a4)			; Is Map Pixel POSX = 0?
	bne.s	.first_round				; No....
        bsr     FIX_TEMPLE				; Yeah...
	nop

; Get copper list for background here.
.first_round:
.exit:
        tst.w   RYGAR_LIFE_LOST(a4)                     ; If life has been lost then
        bmi.s   .life                                   ; do the transition

        tst.w   ROUND_CURRENT(a4)                       ; If this is the first round
        bne.s   .skip                                   ; then do the transition
        bra.s   .nolife

.life:  clr.w   RYGAR_LIFE_LOST(a4)

        moveq   #MUSIC_RESTART,d0
        bsr     PLAY_TUNE

.nolife:

        bsr     TRANSITION_FROM_BLACK
	
.skip:
        tst.w	COPPER_ACTIVE
	beq.s	.no_copper2
        move.l  COPPTR_BPLCON(a4),a0
        IFEQ    DEBUG_BG_ENABLE
        move.w  #$5200,2(a0)                            ; Enable Bitplanes
        ELSE
        move.w  #$0211,2(a0)
        ENDC
.no_copper2:
	bsr	INIT_ROUND_VARS
	
	;LEVEL5_STOP
	;nop
	;nop
	;LEVEL5_START
	
	rts
	

INIT_ROUND_VARS:
        move.w  #6,ITEM_CURRENT(a4)
        move.w  #-1,BACKGROUND_ENABLE(a4)
        move.w  #-1,RYGAR_LEAPING_STATE(a4)
        clr.w   TRANSITION_ACTIVE(a4)
        clr.w   RYGAR_LIFE_LOST(a4)
        clr.w   RYGAR_DEAD_STATE(a4)
        clr.w   RYGAR_JUMP_STATE(a4)
        clr.w   RYGAR_DISKARM_STATE(a4)
        clr.w   RYGAR_BLOCKED_STATE(a4)
        clr.w   RYGAR_SINE_INDEX(a4)
        clr.w   FLAG_NEXT_ROUND(a4)
	clr.w	ENEMY_SPEED_ACCUMULATOR(a4)
	clr.w	ENEMY_SPEED_MODIFIER(a4)
	clr.w	FADE_SPRITE_INDEX(a4)
	clr.w	REAPER_ACTIVE(a4)
        clr.w   SANCTUARY_FRAME(a4)
	clr.w	ROUND_TRANSITION(a4)
	clr.w	VERTSCROLL_ENABLE(a4)
	clr.w	ROPE_LINE_ENABLE(a4)
	clr.w	VIDEO_SPLIT_POSY(a4)
	clr.w	VIDEO_SPLIT_LAST_POSY(a4)
	clr.w	VERTCOPY_LINES_TOP(a4)
	clr.w	VERTCOPY_LINES_BOTTOM(a4)
	clr.w	RYGAR_SWING_STATE(a4)
	clr.w	ROPE_LINE_DETACH(a4)
	move.w	#-1,MUSIC_DELAY_TIME(a4)
        clr.w   TIME_LOW(a4)
	clr.w	TIME_OVER(a4)
	clr.w	POWER_DROPPED(a4)
	clr.w	INPUT_DISABLED(a4)
	clr.w	INIT_END_SEQUENCE(a4)
	clr.w	LIGAR_FIGHT(a4)
	clr.w	SHOW_RYGAR_WIN(a4)
	clr.l	SPAWN_LEFT_BLOCK(a4)
	clr.l	SPAWN_RIGHT_BLOCK(a4)
	
	moveq	#MAX_SPRITES-1,d7
	lea	ATTACH_TABLE(a4),a0
.attach:	move.b	#-1,(a0)+
	dbf	d7,.attach

	move.w	ROUND_MAP_START_Y(a4),MAP_POSY(a4)
	move.w	MAP_POSY(a4),d0
        lsl.w   #4,d0
        move.w  d0,MAP_PIXEL_POSY(a4)       ; Store the actual pixel position offset

	moveq	#SPR_RYGAR,d2
        move.l  MEMCHK0_SPRITE_HDLVARS(a4),a0
        lea     TAB_64(a4),a3
        add.w   (a3,d2*2),a0
	clr.w	PRIVATE_RYGAR_SWING(a0)
	clr.w	PRIVATE_RYGAR_SWING_SINEPTR(a0)
	clr.w	RYGAR_LAST_DIRECTION(a0)
	clr.w	SFX_DISABLE_DESTROY_ENEMY(a4)
	
	clr.w	BACKGROUND_DISABLE_OVERRIDE(a4)
	
	bsr	RESET_EVENTS
	
	tst.w	CURRENT_BUTTON_MODE(a4)
	beq.s	.two_buttons
.one_button:
	move.w	#$0000,$dff034
	bra.s	.rts
	
.two_buttons:
	move.w	#$c000,$dff034		; Reset POTGO
	
.rts:	rts


; Load and initialize the current round
LOAD_ROUND:
	FUNCID	#$75f7fe31
	
	move.w	#-1,ROUND_TRANSITION(a4)
        clr.w   FLAG_NEXT_ROUND(a4)
        clr.w   STONE_CURRENT(a4)
        clr.w   ITEM_CURRENT(a4)
        move.w  #-1,BACKGROUND_ENABLE(a4)
        move.w  #114+32,RYGAR_LAST_JUMP_POS(a4)

        lea     HAVE_ITEMS(a4),a0
        rept    8
        clr.l   (a0)+
        endr

        clr.w   TIME_LOW(a4)
	clr.w	TIME_OVER(a4)
	clr.w	FADE_SPRITE_INDEX(a4)
	
	lea	CLOUDS_CONFIG(a4),a0			; Reset clouds position
	rept	8
	clr.w	(a0)
	add.l	#16,a0
	endr
	
	clr.w	MOON_BG_OFFSET(a4)			; Reset moon position.

; Clear the background before loading into it.
	move.l	MEMCHK10_BACKGROUND(a4),a0
	move.l	#MEMSIZE_CHUNK10,d4
.clear:	clr.b	(a0)+
	dbf	d4,.clear

        moveq   #0,d4
        move.w  ROUND_CURRENT(a4),d4
        bsr     DISKLOAD_ROUND
	bsr	FETCH_ROUND_POINTERS

        bsr     GET_ROUND_START_POSY
        bsr     GET_ROUND_BACKGROUND_POINTERS

        bsr     INIT_TILE_ATTRIBUTES
	IFNE	DEBUG_TILEMAP
	move.l	TESTMAP_POINTER(a4),ROUND_TILEMAP(a4)
	ELSE
	move.l	MEMCHK3_TILEMAP(a4),ROUND_TILEMAP(a4)
	ENDC

        bsr     GET_MAP_PLATFORMS
        bsr     GET_ROUND_ENDPOINT
        bsr     GET_ROUND_COLOUR_CYCLING
        bsr     INIT_STONES
        bsr     INIT_ITEMS

        bsr     MERGE_OBSTACLES_INTO_TILEMAP
        bsr     MERGE_OBSTACLES_INTO_PLATFORMS
        bsr     MERGE_STONES_INTO_TILEMAP
	
	clr.w	MAP_PIXEL_POSX_LIMIT(a4)
        rts




; Check for the end of the round, load the next round and initialise it.
CHECK_NEW_ROUND:
	FUNCID	#$fb57f810
	tst.w	INIT_END_SEQUENCE(a4)
	bmi	END_SEQUENCE
	
        move.w  ROUND_ENDPOINT(a4),d0
        cmp.w   MAP_PIXEL_POSX(a4),d0                   ; At the end of the round?
        bne.s   .no_auto                                ; Nope.
	
	;move.w	#-1,RYGAR_DISKARM_STATE(a4)
	;move.w	#-1,BTN1_STATE(a4)
	
; Stop the disk armour
	clr.w	RYGAR_DISKARM_STATE(a4)
	clr.w	DISKARM_CURRENT_FRAME(a4)
	clr.w	BTN1_STATE(a4)
	
	move.w	#-1,SFX_DISABLE_DESTROY_ENEMY(a4)

        moveq   #MUSIC_SANCTUARY,d0					; Sanctuary scene tune
        bsr     PLAY_TUNE
	
; Reset Carousel frames
	movem.l	d0-d1,-(a7)
	moveq	#SPR_RYGAR,d0
	moveq	#RYGAR_CAROUSEL,d1
	bsr	RESET_SPRITE_FRAMES
	movem.l	(a7)+,d0-d1
		
        bsr     DESTROY_ALL_ENEMIES             	; Destroy all active enemies on entry
        bsr     INIT_SANCTUARY_VARS			; Initialise the sanctuary

        tst.w   TIME_REMAINING(a4)
        beq.s   .zero_time
        bpl.s   .zero_time
        clr.w   TIME_REMAINING(a4)

.zero_time:
        move.w  #-1,DISABLE_ENEMIES(a4)
        tst.w   AUTO_PAN_RIGHT(a4)               	; Yes we are... are we auto panning?
        bne   	.exit

        move.w  #124,AUTO_PAN_RIGHT(a4)                 ; Amount to PAN right. (248 pixels)
                                                        ; Set Xposition
        moveq   #SPR_RESERVED_LEFT_CAROUSEL,d0
        bsr     ENABLE_SPRITE
        moveq   #SPR_RESERVED_RIGHT_CAROUSEL,d0
        bsr     ENABLE_SPRITE

.no_auto:
        tst.w   FLAG_NEXT_ROUND(a4)
        beq   	.exit

.load_next_round:
        move.l  ROUND_TILEMAP(a4),PREV_ROUND_TILEMAP(a4)
        addq.w  #1,ROUND_CURRENT(a4)
        clr.w   MAP_PIXEL_POSX(a4)
        clr.w   MAP_OFFSET_BG(a4)
        clr.w   FLAG_NEXT_ROUND(a4)

; Load next round from disk.
        bsr     LOAD_ROUND
	
; Reset Carousel frames
	movem.l	d0-d1,-(a7)
	moveq	#SPR_RYGAR,d0
	moveq	#RYGAR_CAROUSEL,d1
	bsr	RESET_SPRITE_FRAMES
	movem.l	(a7)+,d0-d1
	
; Initialise the round
        lea     DISKARM_COORDS(a4),a2           ; Clear disk armour co-ordinates.
        moveq   #-1,d7
        rept    8
        move.l  d7,(a2)+
        endr
	
	move.w	#-1,ROUND_TRANSITION(a4)
        ;bsr     SET_ALL_ENEMIES_OFFSCREEN	 ; <<< THIS BREAKS SHIT.
	
        move.w  #-1,LAST_ENEMY_STATE(a4)
	
	lea     ENEMY_HWSPR_UPPER_ALLOCATED(a4),a0                      ; Free up hardware sprite
        bsr     DEALLOC_HARDWARE_SPRITES
    	lea     ENEMY_HWSPR_MID_ALLOCATED(a4),a0                      ; Free up hardware sprite
        bsr     DEALLOC_HARDWARE_SPRITES 
    	lea     ENEMY_HWSPR_LOWER_ALLOCATED(a4),a0                      ; Free up hardware sprite
        bsr     DEALLOC_HARDWARE_SPRITES	
        bsr     INIT_ROUND
	
	bsr	INCREASE_RANK
	
	moveq	#0,d0
	bsr	LOAD_ENEMY_STATE_FROM_MAP


.exit:  rts



FETCH_ROUND_POINTERS:
	FUNCID	#$8e80c1b3

	IFNE	DEBUG_ROUND
	lea	DEBUG_ROUND_POINTERS(a4),a0
	moveq	#0,d0
	move.w	ROUND_CURRENT(a4),d0
	move.l	(a0,d0*4),MEMCHK3_ROUNDCONFIG(a4)
	ENDC
	
	move.l	MEMCHK3_ROUNDCONFIG(a4),a0
	move.l	a0,a3

        move.l  MEMCHK3_TILEMAP(a4),ROUND_TILEMAP(a4)  	 
	
	move.l	20(a0),d0			; Round Obstacles Offset
	lea	(a3,d0.l),a1
	move.l	a1,ROUND_OBSTACLES(a4)
	
	move.l	28(a0),d0			; Round stone placements
	lea	(a3,d0.l),a1
	move.l	a1,ROUND_STONES(a4)
	
	move.l	52(a0),d0			; Round stone placements
	lea	(a3,d0.l),a1
	move.l	a1,ROUND_ITEMS(a4)
	
	move.l	36(a0),d0			; Round enemy configuration
	lea	(a3,d0.l),a1	
	move.l	a1,ROUND_ENEMY_CONFIG(a4)

	move.l	48(a0),d0			; Round enemy configuration
	lea	(a3,d0.l),a1	
	move.l	a1,ROUND_ENEMY_STATE(a4)
	
	move.l	56(a0),d0			; Round enemy configuration
	lea	(a3,d0.l),a1	
	move.l	a1,ROUND_EVENTS_PTR(a4)
	
	move.l	40(a0),d0			; Round enemy configuration
	lea	(a3,d0.l),a1	
	move.l	a1,ROUND_BESPOKE_ENEMIES(a4)
	
	rts




GET_ROUND_COLOUR_CYCLING:
	FUNCID	#$0c6111ef
 
	IFNE	DEBUG_ROUND
	lea	DEBUG_ROUND_POINTERS(a4),a0
	moveq	#0,d0
	move.w	ROUND_CURRENT(a4),d0
	move.l	(a0,d0*4),MEMCHK3_ROUNDCONFIG(a4)
	ENDC
	
	move.l	MEMCHK3_ROUNDCONFIG(a4),a0
	move.l	a0,a3
	
	move.l  44(a0),ROUND_FG_WATER_CYCLE(a4)
	move.l	68(a0),ROUND_FG_LAVA_CYCLE(a4)
        rts

GET_ROUND_ENEMY_CONFIG:
	FUNCID	#$f5f6d2bf
        move.l  ROUND_ENEMY_CONFIG(a4),a0
        lea     ROUND_ENEMY_POSITIONS(a4),a1
        move.l  #176-1,d7
.loop1: move.l  (a0)+,d0
        cmp.l   #-1,d0
        beq.s   .exit
        move.l  (a0),(a1)+
.next:  dbf     d7,.loop1
.exit:  rts


GET_ROUND_START_POSY:
	FUNCID	#$584716c7

	IFNE	DEBUG_ROUND
	lea	DEBUG_ROUND_POINTERS(a4),a0
	moveq	#0,d0
	move.w	ROUND_CURRENT(a4),d0
	move.l	(a0,d0*4),MEMCHK3_ROUNDCONFIG(a4)
	ENDC
	
	move.l	MEMCHK3_ROUNDCONFIG(a4),a0
	move.l	a0,a3
	
        move.l  24(a0),d0
        move.w  d0,ROUND_MAP_START_Y(a4)
	move.w	d0,MAP_POSY(a4)
        lsl.w   #4,d0
        move.w  d0,MAP_PIXEL_POSY(a4)       ; Store the actual pixel position offset
        rts

GET_ROUND_BACKGROUND_POINTERS:
	FUNCID	#$5ca19a01

	IFNE	DEBUG_ROUND
	lea	DEBUG_ROUND_POINTERS(a4),a0
	moveq	#0,d0
	move.w	ROUND_CURRENT(a4),d0
	move.l	(a0,d0*4),MEMCHK3_ROUNDCONFIG(a4)
	ENDC

	move.l	MEMCHK3_ROUNDCONFIG(a4),a0
	move.l	a0,a3
	
        move.l  8(a0),d0
        move.w  d0,BG_SCROLL_TYPE(a4)

        move.l  12(a0),d0
        move.w  d0,BG_START_OFFSET(a4)     	; Get start of background offset

        move.l  16(a0),d0
        move.w  d0,BG_SCROLL_SPEED(a4)
	
        move.l  72(a0),d0
        move.w  d0,ROUND_WATERFALL(a4)

; Work here for relocation...
        move.l  4(a0),d0                        ; Get background pointer
	lea	(a3,d0.l),a1			; 
	
        move.l  4(a1),d0
	lea	(a3,d0.l),a1
	move.l	a1,ROUND_GRADIENT_PTR(a4)    ; Get copper pointer
	
	move.l  4(a0),d0                        ; Get background pointer
	lea	(a3,d0.l),a1

        move.l  8(a1),d0
        move.w  d0,BLIT_BG_START(a4)    	; Get start of Blit
        move.l  12(a1),d0
        move.w  d0,BLIT_BG_LENGTH(a4)       	; Get length

	move.l  4(a0),d0                        ; Get background pointer
	lea	(a3,d0.l),a1
        move.l  (a1),d0                         ; Get pointer to IFF
 	lea	(a3,d0.l),a1       
	move.l  (a1),a0                         ; DEREF Pointer

; default to normal scroll
	move.w	BG_SCROLL_TYPE(a4),d3	
	add.w	d3,d3
	move.w	.jump_table(pc,d3.w),d3
	jmp	.jump_table(pc,d3.w)
	
.jump_table:
	dc.w	.do_normal-.jump_table
	dc.w	.do_sunset-.jump_table
	dc.w	.do_moon-.jump_table
	dc.w	.do_clouds-.jump_table

.do_normal:
        move.l 	MEMCHK10_BACKGROUND(a4),a0
	bra.s	.get_iff	
	
.do_sunset:
	move.l	RYGAR_SUNSET(a4),a0
	bra.s	.get_iff
	
.do_moon:
	move.l	RYGAR_MOON(a4),a0
	bra.s	.get_iff
	
.do_clouds:
        move.l 	MEMCHK10_BACKGROUND(a4),a0
	bra.s	.get_iff
	
	nop
	
.get_iff:
	lea     STCPTR_BACKGROUND(a4),a1                ; Background pointer here.
        bsr     GET_IFF_RAW
        move.l  a0,(a1)
        rts

	

GET_ROUND_BACKGROUND_PALETTE:
	FUNCID	#$5f2d6114

	IFNE	DEBUG_ROUND
	lea	DEBUG_ROUND_POINTERS(a4),a0
	moveq	#0,d0
	move.w	ROUND_CURRENT(a4),d0
	move.l	(a0,d0*4),MEMCHK3_ROUNDCONFIG(a4)
	ENDC
	
	move.l	MEMCHK3_ROUNDCONFIG(a4),a0
	move.l	a0,a3
	
	move.l	4(a0),d0			; Round Obstacles Offset
	lea	(a3,d0.l),a1
	
        move.l  (a1),d0                         ; Get pointer to IFF
 	lea	(a3,d0.l),a1                         ; Get pointer to IFF
        move.l  (a1),a0                         ; DEREF Pointer
	
	move.w	BG_SCROLL_TYPE(a4),d3	
	add.w	d3,d3
	move.w	.jump_table(pc,d3.w),d3
	jmp	.jump_table(pc,d3.w)
	
.jump_table:
	dc.w	.do_normal-.jump_table
	dc.w	.do_sunset-.jump_table
	dc.w	.do_moon-.jump_table
	dc.w	.do_clouds-.jump_table

.do_normal:
        move.l 	MEMCHK10_BACKGROUND(a4),a0
	bra.s	.get_iff	
	
.do_sunset:
	move.l	RYGAR_SUNSET(a4),a0
	bra.s	.get_iff
	
.do_moon:
	move.l	RYGAR_MOON(a4),a0
	bra.s	.get_iff
	
.do_clouds:
        move.l 	MEMCHK10_BACKGROUND(a4),a0
	bra.s	.get_iff
	
	nop
	
.get_iff:
        lea     PAL_RYGAR_BG(a4),a1
        bsr     GET_IFF_PALETTE
        rts
	
	
	

GET_ROUND_ENDPOINT:
	FUNCID	#$1f117822

	IFNE	DEBUG_ROUND
	lea	DEBUG_ROUND_POINTERS(a4),a0
	moveq	#0,d0
	move.w	ROUND_CURRENT(a4),d0
	move.l	(a0,d0*4),MEMCHK3_ROUNDCONFIG(a4)
	ENDC

	move.l	MEMCHK3_ROUNDCONFIG(a4),a0
	move.l	a0,a3
	move.l  32(a0),d0
        move.w  d0,ROUND_ENDPOINT(a4)   ; Store the pixel position offset
        move.l  60(a0),d0
        move.w  d0,ROUND_HEIGHT_LIMIT(a4)
        move.l  64(a0),d0
        move.w  d0,ROUND_DEPTH_LIMIT(a4)
        rts


CLEAR_BG_BITPLANES:
        move.l  a0,a1
        add.w   MAP_POSX(a4),a0
.headloop:
        cmp.l   a0,a1
        beq.s   .head
        clr.w   (a1)+
        bra.s   .headloop
.head:
        move.l  a1,a0
        add.w   MAP_POSX(a4),a0

        move.l  a0,a1
        moveq   #0,d0
        moveq   #5-1,d6

.loop:  move.l  #192-1,d7
        move.l  a1,a0
.fill:  move.l  d0,(a0)         ; left border
        move.l  d0,4(a0)
        move.l  d0,8(a0)
        move.l  d0,12(a0)
        move.l  d0,16(a0)
        move.l  d0,20(a0)
        move.l  d0,24(a0)
        move.l  d0,28(a0)               ; right border
        move.l  d0,32(a0)
        move.l  d0,36(a0)
        add.w   #CANVAS_WIDTH,a0
        dbf     d7,.fill
        add.w   #40,a1          ; index to 3rd bitplane
        dbf     d6,.loop
        rts


FBM
FILL_BG_BITMAP:
	FUNCID	#$d80c12f8
        moveq   #0,d7
	
	moveq	#-1,d6
	
	move.w  BLIT_BG_START(a4),d7			; number of lines to fill from top.
 
	tst.l   ROUND_FG_WATER_CYCLE(a4)	
        beq.s   .fill_upper
	
	move.l	#146,d7			;146
	
.fill_upper:
        move.l  d6,(a0)
        move.l  d6,4(a0)
        move.l  d6,8(a0)
        move.l  d6,12(a0)
        move.l  d6,16(a0)
        move.l  d6,20(a0)
        move.l  d6,24(a0)
        move.l  d6,28(a0)
        move.l  d6,32(a0)
        move.l  d6,36(a0)

        add.w   #CANVAS_WIDTH,a0
        dbf     d7,.fill_upper
					; Water Cycle is running
	
	tst.l   ROUND_FG_WATER_CYCLE(a4)	; Don't fill bottom if water cycle is required.
        bne.s   .exit
	
	moveq	#0,d7
	move.w	BLIT_BG_LENGTH(a4),d7
	subq.w	#1,d7
	mulu	#PLAYFIELD_SIZE_X*5,d7
	add.l	d7,a0				
	
	move.l	#184,d7
	sub.w	BLIT_BG_LENGTH(a4),d7
	sub.w	BLIT_BG_START(a4),d7

	moveq	#0,d6
	cmp	#BG_SCROLL_SUNSET,BG_SCROLL_TYPE(a4)
	beq.s	.fill_lower
	;tst.w	BG_SCROLL_TYPE(a4)			; if sunset then fill lower
	;bmi.s	.fill_lower

	moveq	#-1,d6

.fill_lower:
        move.l  d6,(a0)
        move.l  d6,4(a0)
        move.l  d6,8(a0)
        move.l  d6,12(a0)
        move.l  d6,16(a0)
        move.l  d6,20(a0)
        move.l  d6,24(a0)
        move.l  d6,28(a0)
        move.l  d6,32(a0)
        move.l  d6,36(a0)

        add.w   #CANVAS_WIDTH,a0
        dbf     d7,.fill_lower	
	
.exit:	rts

GET_FOREGROUND_PALETTE:
	FUNCID	#$12e0dd66
        ;move.l  IMG_TILES(a4),a0
	lea	PALETTE_TILES(a4),a0
	
        lea     PAL_RYGAR_FG(a4),a1
        bsr     GET_IFF_PALETTE
        clr.l   PAL_RYGAR_FG(a4)                ; Black background patch
        rts

; Draws the scene at the start of the round
; This is important because it changes after the first round.
; End of the first round becomes start of second round and so on.
SET_START_SCENE:
	FUNCID	#$5cf5998b

        tst.w   ROUND_CURRENT(a4)
	bne.s   .draw_temple				; Round is not 0...

.draw_rocks:
        clr.w   MAP_OFFSET_BG(a4)
        clr.w   MAP_POSX(a4)
        clr.w   MAP_PIXEL_POSX(a4)
	
        moveq   #0,d1
        move.w  START_MAP_PIXEL_POSX(a4),d1
        bsr     SET_MAP_POSITION

        move.l  ROUND_TILEMAP(a4),a0            	; Get Y Position in the map
        moveq   #0,d0
        lea     MAP_TABLE_Y1(a4),a1                     ; Index to start of Map
        move.w  MAP_POSY(a4),d0
        add.l   (a1,d0*4),a0

        move.w  #MAP_COMPENSATE,COLLISION_COMPENSATE(a4)	; MAP_COMPENSATE = 256

; -------------------------
        move.w  START_MAP_PIXEL_POSX(a4),d2                 ; Last pixel position
        lsr.w   #4,d2                                   	; Get number of words
        moveq   #-32,d0                                 	; Compensate for
        sub.w   d2,d0
        add.w   #16,d0
        subq.w  #1,d0
; -------------------------

        bsr     DRAW_CANVAS
        move.l  ROUND_OBSTACLES(a4),a0
        bsr     BLIT_OBJECTS_INTO_CANVAS
        bra     .exit

.draw_temple:
        tst.w   SANCTUARY_STAGE(a4)
        beq.s   .not_in_sanctuary1

; Rygar is in sanctuary so we need to copy the current post screen
        move.l  LSTPTR_POST_SCREEN(a4),a0
	add.w  MAP_POSX(a4),a0
	addq.w	#2,a0
       
        clr.w   MAP_OFFSET_BG(a4)
        clr.w   MAP_POSX(a4)
        clr.w   MAP_PIXEL_POSX(a4)
       
; Copy whatever is on post screen back to the beginning of the next round
	move.l  LSTPTR_FRONT_SCREEN(a4),a1
        move.l  LSTPTR_BACK_SCREEN(a4),a2
        move.l  LSTPTR_POST_SCREEN(a4),a3
        move.l  #(40*5)*192,d7
        lsr.l   #2,d7
	
.copypost:
        move.l  (a0)+,d3
        move.l  d3,(a1)+
        move.l  d3,(a2)+
        move.l  d3,(a3)+
        dbf     d7,.copypost

; Rygar has started at a Round other than 1
.not_in_sanctuary1:
        moveq   #0,d1
        move.w  START_MAP_PIXEL_POSX(a4),d1
        bsr     SET_MAP_POSITION

        move.l  ROUND_TILEMAP(a4),a0            ; Get Y Position in the map
        moveq   #0,d0
        lea     MAP_TABLE_Y1(a4),a1                     ; Index to start of Map
        move.w  MAP_POSY(a4),d0
        add.l   (a1,d0*4),a0
        move.w  #MAP_COMPENSATE,COLLISION_COMPENSATE(a4)                    ; MAP_COMPENSATE = 256

; Reset Map Position
        move.w  START_MAP_PIXEL_POSX(a4),d2                 ; Last pixel position
        lsr.w   #4,d2                                   ; Get number of words
        moveq   #-32,d0                                 ; Compensate for
        sub.w   d2,d0
        add.w   #16,d0
        subq.w  #1,d0

        tst.w   SANCTUARY_STAGE(a4)                             ; If Rygar is in the Sanctuary
        bne.s   .not_in_sanctuary2                      ; then don't rebuild the canvas.
	bsr     DRAW_CANVAS
        move.l  ROUND_OBSTACLES(a4),a0
        bsr     BLIT_OBJECTS_INTO_CANVAS
.not_in_sanctuary2:
.exit:	rts



; MAP_POSY(a4) = Start Y Map position
;---------------------------------------
; a0=Map Start
;---------------------------------------
GET_MAP_PLATFORMS:
		FUNCID	#$5d5aa53a

		;move.w	#$077,FIRECOLOUR
		;bsr	FIREWAITSR
		;bra.s	.exit
		move.l  ROUND_TILEMAP(a4),a0
		moveq	#0,d3
		;move.w	MAP_POSY(a4),d3
		move.w	ROUND_MAP_START_Y(a4),d3
		
		add.w	d3,d3
		mulu.w	#MAP_WIDTH,d3
		and.l	#$ffff,d3
		add.l	d3,a0
		
                lea     ROUND_PLATFORMS(a4),a1
                move.l  #MAP_WIDTH,d6			; 176
                move.l  #12-1,d7

                movem.l d0-d5/a2-a3,-(a7)
                lea     TILE_ATTRIBUTES(a4),a2
                moveq   #0,d3
                move.l  d6,d4
                add.w   d4,d4           ; Map line length in d4
                subq.w  #1,d6

.loop_x:        move.l  a0,a3

                add.l   d3,a3
                move.l  d7,d5
                moveq   #0,d1

                moveq   #0,d2
.loop_y:        move.w  (a3),d0         ; Get tile number
                cmp.w   #-1,(a2,d0*2)   ; Is the tile a platform?
                bne.s   .next           ; No, just skip
                tst.w   d1              ; Yes, is there already a platofrm
                beq.s   .first          ; No, first one
                lsl.l   #4,d1           ; Yes! so shift it left!
                or.l    d2,d1           ; Store it
                bra.s   .next
.first:
                move.l  d2,d1

.next:          add.l   d4,a3
                addq.w  #1,d2                   ; Count platform
                dbf     d5,.loop_y
                move.l  d1,(a1)+

                addq.w  #2,d3                   ; Next tile
                dbf     d6,.loop_x

        bsr     GET_ROUND_ENEMY_CONFIG                              ; Get's tne enemy position config for the round.
        bsr     SET_PLATFORMS_ENEMY_CONFIG                      ; takes the enemy config and applies where there are no platforms for specific enemies
        bsr     SET_OBSTACLES_ENEMY_CONFIG


                movem.l (a7)+,d0-d5/a2-a3
.exit:          rts


PAN_LEFT_TO_PLATFORM:
	FUNCID	#$685b764a
        tst     MAP_PIXEL_POSX(a4)
        beq     .exit
        cmp.w   #$100,MAP_PIXEL_POSX(a4)
        beq     .exit

        moveq   #0,d1

.loop:
        move.l  d1,-(a7)
        bsr     WAIT_FOR_VERTICAL_BLANK
        bsr     PAN_CAMERA_POSITION
        move.l  (a7)+,d1

	moveq   #0,d0
        move.w  MAP_PIXEL_POSX(a4),d0
        sub.w   #256,d0
        add.w   RYGAR_XPOS(a4),d0
        lsr.w   #4,d0
        lea     ROUND_PLATFORMS(a4),a0
        move.l  (a0,d0*4),d0
	move.l	d0,d2
        and.l   #$f,d0
	bne.s	.found_platform
	
	move.l  d1,-(a7)
        bsr     PAN_LEFT
	bsr     DBUFF
	
	lea     LSTPTR_OFF_SCREEN(a4),a3
	bsr     COPY_BG_TO_SCREEN
        bsr     BLIT_TILE_ONSCROLL_X
	bsr     BLIT_TILE_ONSCROLL_Y
	bsr     HDL_STONES
	bsr     HDL_ITEMS

        move.l  (a7)+,d1

        bra   .loop

.found_platform:
; d2 has platforms block
.find_highest_platform:
	move.l	d2,d3			; d2=$00000059	d2=$00000005
	and.l	#$f,d3			; d3=$00000009	d3=$00000005 
	lsr.l	#4,d2			; d2=$00000005	d2=$00000000
	beq.s	.highest
	bra.s	.find_highest_platform
.highest:
	moveq	#SPR_RYGAR,d0		
	bsr	GET_SPRITE_CONTEXT	
	lsl.w	#4,d3			; d3=00000005 << 4
	move.w	d3,4(a0)
.exit:	
	rts




MOUSE_WAIT:
	FUNCID	#$1af04b57
.wait:  btst.b  #6,$bfe001
        bne.s   .wait
.held:  btst.b  #6,$bfe001
        beq.s   .held
        rts

MERGE_OBSTACLES_INTO_PLATFORMS:
	FUNCID	#$ba7037e0
        move.l  ROUND_OBSTACLES(a4),a0
.next:  lea     ROUND_PLATFORMS(a4),a1
        move.w  (a0),d0
        cmp.w   #$7fff,d0
        beq.s   .exit

	moveq	#0,d1
	moveq	#0,d2
        move.w  (a0)+,d1                ; Get X
        move.w  (a0)+,d2                ; Get Y
        and.w   #$f,d2

        add.w   d1,d1           ; multiply for long words
        add.w   d1,d1
        add.l   d1,a1           ; Index to correct X block.

.loop:  lea     MERGE_OBSTACLES_INTO_PLATFORMS_BUFFER(a4),a2
        clr.l   (a2)
        clr.l   4(a2)
        clr.w   8(a2)
        move.b  d2,(a2,d2)
        move.w  (a0)+,d0
        cmp.w   #-1,d0
        beq.s   .next
        moveq   #0,d4

        move.l  (a1),d3         ; Get current block
; d2=Platform number to merge into block
.process:       move.b  d3,d4           ; Get currentl block...may 00000059
        and.b   #$f,d4          ; d4 = 9
        beq.s   .sort           ; if zero then go to next
        move.b  d4,(a2,d4)              ; set array 9
        lsr.l   #4,d3           ; shift next digit.
        bra.s   .process

.sort:  moveq   #10-1,d7
        moveq   #0,d5

.sortloop:
        moveq   #0,d4
        move.b  (a2)+,d4
        and.w   #$f,d4
        beq.s   .nextsort
        or.l    d4,d5
        lsl.l   #4,d5
        ;bset   d4,d5

.nextsort:      dbf     d7,.sortloop
        lsr.l   #4,d5
        move.l  d5,(a1)+
        bra.s   .loop
.exit:  rts


MERGE_OBSTACLES_INTO_TILEMAP:
	FUNCID	#$9a7fb2d2
        move.l  ROUND_OBSTACLES(a4),a0
.next:  move.l  ROUND_TILEMAP(a4),a1
        move.w  (a0),d0
        cmp.w   #$7fff,d0
        beq.s   .exit

	moveq	#0,d1
	moveq	#0,d2
        move.w  (a0)+,d1                ; Get X
        move.w  (a0)+,d2                ; Get Y
        mulu    #MAP_WIDTH,d2
        add.w   d2,d2
        add.l   d2,a1
        add.w   d1,d1
        add.l   d1,a1                   ; At correct position in map.
.loop:  move.w  (a0)+,d0
        cmp.w   #-1,d0
        beq.s   .next
        or.w    d0,(a1)+
        bra.s   .loop
.exit:  rts

MERGE_STONES_INTO_TILEMAP:
	FUNCID	#$f97db041
        move.l  #MAP_WIDTH-1,d7                         ; Initialize the stone positions
        lea     STONE_POSITIONS(a4),a0
.init:  move.b  #-1,(a0)+
        dbf     d7,.init

        lea     STONES_BG_PTRS(a4),a3
        move.l  ROUND_STONES(a4),a0
        moveq   #-1,d7
.next:  addq.l  #1,d7

        move.l  ROUND_TILEMAP(a4),a1
        move.w  (a0),d0
        cmp.w   #$7fff,d0
        beq     .exit

        lea     STONE_POSITIONS(a4),a2
	moveq	#0,d1
	moveq	#0,d2
        move.w  (a0)+,d1                ; Get X
        move.w  (a0)+,d2                ; Get Y
        move.b  d7,(a2,d1)
        mulu    #MAP_WIDTH,d2
        add.w   d2,d2
        add.l   d2,a1
        add.w   d1,d1
        add.l   d1,a1                   ; At correct position in map.
.loop:
        move.w  (a0)+,d0
        cmp.w   #-1,d0
        beq.s   .next
        cmp.w   #TILECMD_PUSHSTONE_JUMP,d0
        bne.s   .not_jump
        ;move.w #17,(a1)                ; Jump Stone is here....  must adjust.
        or.w    d0,(a1)
        sub.w   #MAP_WIDTH*4,a1         ; Go up two rows
        bra.s   .save
        nop
.not_jump:
        or.w    d0,(a1)
        cmp.w   #TILECMD_PUSHSTONE_WALK,d0
        bne.s   .save
        sub.w   #MAP_WIDTH*4,a1         ; Go up two rows
        sub.w   #20,a1
        bra.s   .save
        nop
        ;or.w   #STONE_BLOCK1,(a1)                      ; Place the stone in the map
        ;or.w   #STONE_BLOCK2,2(a1)                     ; as attributes.
        ;or.w    #STONE_BLOCK3,MAP_WIDTH*2(a1)
        ;or.w    #STONE_BLOCK4,(MAP_WIDTH*2)+2(a1)
        ;bra.s  .loop
.save:                                                  ; This routine builds the 32x32 background tiles
        move.l  a1,d3                                   ; for fast restoring of the stones

        ;move.l  d3,a1
        move.w  (a1),d0
        and.w   #%0000001111111111,d0
        move.l  MEMBASE_CHUNK8(a4),a2
        move.l  d7,d6
        lsl.w   #2,d6
        add.l   d6,a2                                   ; a2 = dest
        move.l  a2,(a3)+                                ; save position of blit.
        bsr     SAVE_TILE_BG

        move.l  d3,a1
        move.w  2(a1),d0
        and.w   #%0000001111111111,d0
        move.l  MEMBASE_CHUNK8(a4),a2
        move.l  d7,d6
        lsl.w   #2,d6
        add.l   d6,a2                                   ; a2 = dest
        addq.w  #2,a2
        bsr     SAVE_TILE_BG

        move.l  d3,a1
        move.w  MAP_WIDTH*2(a1),d0
        and.w   #%0000001111111111,d0
        move.l  MEMBASE_CHUNK8(a4),a2
        move.l  d7,d6
        lsl.w   #2,d6
        add.l   d6,a2                                   ; a2 = dest
        ;add.w  #(PLAYFIELD_SIZE_X*BITPLANES)*16,a2
        add.w   #(MAX_STONES*4)*BITPLANES*16,a2
        bsr     SAVE_TILE_BG

        move.l  d3,a1
        move.w  (MAP_WIDTH*2)+2(a1),d0
        and.w   #%0000001111111111,d0
        move.l  MEMBASE_CHUNK8(a4),a2
        move.l  d7,d6
        lsl.w   #2,d6
        add.l   d6,a2                                   ; a2 = dest
        ;add.w  #(PLAYFIELD_SIZE_X*BITPLANES)*16,a2
        add.w   #(MAX_STONES*4)*BITPLANES*16,a2
        addq.w  #2,a2
        bsr     SAVE_TILE_BG
        bra     .loop

.exit:  rts


; a2=Save address
SAVE_TILE_BG:
	FUNCID	#$c697e928
        movem.l d0-d7/a0-a3,-(a7)
; Find Source tile (source tile in d0)
; Fast divide by 16 with remainder
	moveq	#0,d1
	moveq	#0,d2
        move.w  d0,d1
        move.w  d0,d2
        and.w   #$f,d1                          		; d1 = x position
        lsr.w   #4,d2                           		; d2 = Y position

        lea     ASSET_TABLE_Y1(a4),a0                  		; Fast Multiply to Y
        move.l  (a0,d2.w*4),d2                                  ; Bring in the offset

        add.w   d1,d1                                           ; Bytes to words for offset
        move.l  TILE_ASSETS_PTR(a4),a0                  	; Tiles location
        add.l   d1,a0                                           ; Add X Offset
        add.l   d2,a0

; Check to see if we should do the Post screen too.

        WAIT_FOR_BLITTER
        move.l  #$09f00000,BLTCON0(a5)          		;
        move.l  #$ffffffff,BLTAFWM(a5)          		;
        move.w  #ASSET_MODULO-2,BLTAMOD(a5)
        ;move.w #PLAYFIELD_SIZE_X-2,BLTDMOD(a5)
        move.w  #(MAX_STONES*4)-2,BLTDMOD(a5)
        move.l  a0,BLTAPTH(a5)                  		; Load the mask address
        move.l  a2,BLTDPTH(a5)
        move.w  #(80*64)+1,BLTSIZE(a5)          		; Default to 16x16,BLTSIZE(a5)
        movem.l (a7)+,d0-d7/a0-a3
        rts


;
; This routine removes the stones and items from the game
; map when Rygar is killed.
CLEAR_OBJECTS_FROM_MAPPED_SCREEN_AREA:
                movem.l d0/d6/d7/a0/a3,-(a7)
                move.l  ROUND_TILEMAP(a4),a0            ; Get tilemap pointer
		moveq	#0,d0
                move.w  MAP_PIXEL_POSX(a4),d0
                lsr.w   #4,d0
                add.l   d0,a0                           ; Index to X Position
                add.l   d0,a0
                sub.w   #$20,a0                         ; Compensate for temple at start of map (Eh???).

                lea     MAP_TABLE_Y1(a4),a3             ;
		moveq	#0,d4
                move.w  MAP_POSY(a4),d4
                add.l   (a3,d4*4),a0                    ; Index to Y Position
; a1 now points to top left of map.

                moveq   #12-1,d7                        ; Map Depth
.loop_y:        move.l  #20-1,d6                        ; Map Width
.loop_x:        
		move.l	a0,a1
		add.w	d6,a1
		add.w	d6,a1
		bsr	.remove_stone
		bsr	.remove_item
		
		dbf     d6,.loop_x
                add.l   #MAP_WIDTH*2,a0                 ; Next row.
                dbf     d7,.loop_y
                movem.l (a7)+,d0/d6/d7/a0/a3
		bra	.exit
		
.remove_stone:
		movem.l	a0/a1/d6/d7,-(a7)
		moveq	#MAX_STONES-1,d6
		lea	STONE_STRUCT(a4),a0
.loop_stone:	cmp.l	STONE_STRUCT_MAPPOS(a0),a1		; check map address
		bne.s	.next_stone
; ---- Stone found...
		and.w	#%0000001111111111,(a1)
		and.w	#%0000001111111111,2(a1)
		add.w	#MAP_WIDTH*2,a1
		and.w	#%0000001111111111,(a1)
		and.w	#%0000001111111111,2(a1)
		clr.l	STONE_STRUCT_MAPPOS(a0)
		clr.w	STONE_STRUCT_SCRPOSX(a0)
		clr.w	STONE_STRUCT_SCRPOSY(a0)
.next_stone:	add.w	#STONE_STRUCT_SIZE,a0
		dbf	d6,.loop_stone
		movem.l	(a7)+,a0/a1/d6/d7
		bra.s	.exit
		
.remove_item:	
		movem.l	a0/a1/d6/d7,-(a7)
		moveq	#MAX_ITEMS-1,d6
		lea	ITEM_STRUCT(a4),a0
.loop_item:	cmp.l	ITEM_STRUCT_MAPPOS(a0),a1		; check map address
		bne.s	.next_item
; ---- Item found...
		and.w	#%0000001111111111,(a1)
		clr.l	ITEM_STRUCT_MAPPOS(a0)
		clr.w	ITEM_STRUCT_SCRPOSX(a0)
		clr.w	ITEM_STRUCT_SCRPOSY(a0)
.next_item:	add.w	#ITEM_STRUCT_SIZE,a0
		dbf	d6,.loop_item
		movem.l	(a7)+,a0/a1/d6/d7
		bra.s	.exit
		nop
.exit:		rts

SET_LAST_MAP_POSITION:
	tst.w	CHECKPOINT_DISABLE(a4)
	bmi.s	.exit
        move.w  MAP_PIXEL_POSX(a4),d5
	;add.w	#32,d5					; Can't do this cos it fucks things up with end of round.
	and.w	#$ffe0,d5

	lsr.w	#4,d5
	move.w	d5,ROUND_MAP_START_X(a4)
	
        move.w  MAP_PIXEL_POSY(a4),d5
	lsr.w	#4,d5
	move.w	d5,ROUND_MAP_START_Y(a4)

	clr.w	BACKGROUND_DISABLE_OVERRIDE(a4)
.exit:	rts
	