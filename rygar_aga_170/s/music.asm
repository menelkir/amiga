MUSIC_INTRO:	equ	0
MUSIC_MAIN:	equ	1
MUSIC_RESTART:	equ	2
MUSIC_BONUS_POINTS:	equ	16
MUSIC_EXTRA:	equ	16
MUSIC_SANCTUARY:	equ	18
MUSIC_SHIELD:	equ	13
MUSIC_TIMER:	equ	20
MUSIC_EVIL:	equ	23
MUSIC_PARCHMENT:	equ	27
MUSIC_POWER:	equ	11
MUSIC_GAMEOVER:	equ	24
MUSIC_ENDING:	equ	26
MUSIC_HISCORE:	equ	33

NUMBER_SAMPLES:	equ	15

MUSIC_POWER_DELAY_TIME:	equ	175		; 5 seconds
MUSIC_PARCHMENT_DELAY_TIME:	equ	700


SND_DISKARM_NOPOWER:	equ	0
SND_RYGARJUMP:		equ	1
SND_FIREBALL:		equ	2	
SND_RIDERFALL:		equ	3
SND_RYGARDEAD:		equ	4
SND_SWEEP:		equ	5
SND_SQUIRREL		equ	6
SND_LAND_ON_ENEMY:	equ	7
SND_DESTROY:		equ	8
SND_GRIFFIN:		equ	9
SND_STONE_HIT:		equ	10
SND_LAVARISE:		equ	11
SND_DISKARM_WHIP:	equ	12
SND_PICKUP:		equ	13
SND_ITEMDROP:		equ	14
SND_RYGARWIN:		equ	15
SND_GROWL:		equ	16


	IFNE	ENABLE_SFX
LOAD_SAMPLES:
	FUNCID	#$b2297014
	
	moveq	#0,d0
	lea	SAMPLE_MAP(a4),a2
.loop:	cmp.l	#-1,(a2)
	beq.s	.exit
	move.l	(a2)+,d5		; a0 = Volume
	move.l	(a2)+,a1		; a1 = memory space to unpack
	move.l	(a1),a1			; a1 = deference [a1]
	move.l	(a2)+,d6		; d6 = priority
	move.l	(a2)+,d7		; d7 = period
	;bsr	Unpack			; unpack the sample.
					; a1 now has unpacked sample

	lea	SAMPLES_STRUCT(a4),a3	; a3= 
	move.l	d0,d1
	lsl.w	#4,d1
	add.l	d1,a3
	
.find:	cmp.l	#"BODY",(a1)		; Find the sample body.
	beq.s	.found
	addq.w	#2,a1
	bra.s	.find
	
.found:
	addq.l	#6,a1			
	move.w	(a1),d1			; Get size in words
	lsr.w	#1,d1			; Turn into bytes
	
	move.w	d1,4(a3)		; store sample length
	
	addq.l	#2,a1			; advance to start of sample waveform
	move.l	a1,(a3)			; store sample address pointer
	move.w	d7,6(a3)		; sample period
	
	move.w	d5,8(a3)		; store volume
	move.b	#-1,10(a3)		; any channel
	
	;move.b	d6,11(a3)		; priority
	move.b	#10,11(a3)
	
	addq.w	#1,d0			; next sample.
	bra.s	.loop
.exit:	rts

	ENDC


INSTALL_MUSIC_PLAYER:
	
; Initialise Music / SFX routines
	IFNE	ENABLE_SOUND
	FUNCID	#$05dd6e39
	move.l	a6,-(a7)		
	lea	CHIPBASE,a6		
	move.l	VBRBASE(a4),a0
	moveq	#%0011,d0			 
	bsr	_mt_install_cia
	move.l	(a7)+,a6
	ENDC
	rts

REMOVE_MUSIC_PLAYER:
	IFNE	ENABLE_SOUND
	move.l	a6,-(a7)
	lea	CHIPBASE,a6
	bsr	_mt_remove_cia
	move.l	(a7)+,a6
	ENDC
	rts

PLAY_TUNE:
	IFNE	ENABLE_MUSIC
	FUNCID	#$2eb01893
	move.l	a6,-(a7)

	;tst.w	MUSIC_ON
	;beq.s	.unmask			; Music is off.

	lea	CHIPBASE,a6
	move.l	MUSIC_MAINMODULE(a4),a0
	sub.l	a1,a1
	bsr	_mt_init
	
	move.l	d0,-(a7)
	lea	CHIPBASE,a6
	moveq	#0,d0		; reserve chans 1 and 2 for music
	bsr	_mt_musicmask
	move.l	(a7)+,d0
	
	move.b	#-1,_mt_Enable		; Play music
	bra.s	.exit

.unmask:
	move.l	d0,-(a7)
	lea	CHIPBASE,a6
	moveq	#0,d0		; reserve chans 1,2 & 3 for ziks
	bsr	_mt_musicmask
	move.l	(a7)+,d0
	clr.b	_mt_Enable


.exit:	move.l	(a7)+,a6
	ENDC
	rts
	
STOP_TUNE:
	IFNE	ENABLE_MUSIC
	FUNCID	#$93aae433
	move.l	a6,-(a7)
	clr.b	_mt_Enable
	lea	CHIPBASE,a6
	bsr	_mt_end
	move.l	(a7)+,a6
	ENDC
	rts

; Play sample
; d0=Sample to play.
PLAY_SAMPLE:
	FUNCID	#$e104a333
	IFNE	ENABLE_SFX
	movem.l	d0-d7/a0-a6,-(a7)

	;tst.w	SOUND_ON
	;beq.s	.exit

	eor.w	#1,TOGGLE_CHANNEL(a4)		; Toggle between channel 3 and 4
	move.w	TOGGLE_CHANNEL(a4),d1
	lea	SAMPLES_STRUCT(a4),a0
	lsl.w	#4,d0
	add.l	d0,a0

.play:	move.b	d1,10(a0)		; select channel
	lea	CHIPBASE,a6
	bsr	_mt_playfx

.exit:	
	movem.l	(a7)+,d0-d7/a0-a6
	ENDC

	rts


; Play sample
; d0=Sample to play.
PLAY_SAMPLE_CHANNEL_4:
	FUNCID	#$e104a333
	IFNE	ENABLE_SFX
	movem.l	d0-d7/a0-a6,-(a7)

	lea	SAMPLES_STRUCT(a4),a0
	lsl.w	#4,d0
	add.l	d0,a0

.play:	move.b	#3,10(a0)		; select channel
	lea	CHIPBASE,a6
	bsr	_mt_playfx

.exit:	
	movem.l	(a7)+,d0-d7/a0-a6
	ENDC

	rts
