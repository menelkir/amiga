;STONE_BLOCK1:   equ     17*1024                 ; Destroyable Stone.
;STONE_BLOCK2:   equ     18*1024
;STONE_BLOCK3:   equ     19*1024
;STONE_BLOCK4:   equ     20*1024

STONE_BLOCK1:   equ     17*1024                 ; Destroyable Stone.
STONE_BLOCK2:   equ     18*1024
;STONE_BLOCK3:   equ     47*1024
;STONE_BLOCK4:   equ     48*1024

TILEOBS01:      equ     33*1024                 ; Obstacles
TILEOBS02:      equ     34*1024
TILEOBS03:      equ     35*1024
TILEOBS04:      equ     36*1024
TILEOBS05:      equ     37*1024
TILEOBS06:      equ     38*1024
TILEOBS07:      equ     39*1024
TILEOBS08:      equ     40*1024
TILEOBS09:      equ     41*1024
TILEOBS10:      equ     42*1024
TILEOBS11:      equ     43*1024
TILEOBS12:      equ     44*1024
TILEOBS13:      equ     45*1024
TILEOBS14:      equ     46*1024
TILEOBS15:      equ     47*1024
TILEOBS16:      equ     48*1024


SPRITE_Y_TILE_OFFSET:   equ     2                       ; Offset added to Y tile for Sprites
SPRITE_Y_OBS_OFFSET:    equ     5                       ; Offset added to Y obstacles for Sprites

TILE_MASK:              equ     %0000001111111111
OBSTACLE_MASK:          equ     %1000000000000000       ;%0100000000000000

OBTACLE_BIT:    equ     15
DESTROY_BIT:    equ     14
COLLECT_BIT:    equ     13

INIT_TILE_ATTRIBUTES:
		FUNCID	#$4dd43b08
                lea     TILE_ATTRIBUTES(a4),a0
                lea     TILE_PLATFORMS(a4),a1
.loop:          tst.l   (a1)
                bmi.s   .exit
                move.w  (a1)+,d0
                move.w  (a1)+,(a0,d0*2)
                bra.s   .loop
.exit:          rts

a5

;d0=current sprite number
;a1=pointer to sprite global vars
;a2=pointer to sprite local vars
;Out d6=-1 if obstacle left, 0 if no obstacle, +1 if obstacle right.
;Out d7=zero if on platform, -1 if jumping.
CHECK_TILE:
		FUNCID	#$aa59b94f
		
                move.l  d0,-(a7)
                move.l  d1,-(a7)
                move.l  d3,-(a7)
                lea     SPRITE_COLLISION_MASKS(a4),a3
                lea     (a3,d0*4),a3
                lea     MAP_TABLE_Y1(a4),a0

                move.l  ROUND_TILEMAP(a4),a3
                move.w  MAP_POSY(a4),d0
                add.l   (a0,d0*4),a3
                moveq   #-1,d7                  ; Default off platform

                moveq   #0,d4
                moveq   #0,d5
                move.w  SPRITE_PLOT_YPOS(a1),d5                ; Get Y position of Sprite
		bpl.s	.y_ok
		moveq	#0,d5
.y_ok:

                lsr.w   #4,d5                   ; Divide by 16
                add.l   (a0,d5*4),a3            ; Correct row.

; Now get the tile where Rygar is on X position
                move.w  SPRITE_PLOT_XPOS(a1),d5                ; Get X Position
		bpl.s	.x_ok
		moveq	#0,d5
.x_ok:		
                move.w  d5,d2
                lsr.w   #4,d5                   ; Get X Position
                and.w   #$f,d2                  ; Sprite Remainder in d2
                ;move.w d2,DEBUG_SPROFX
                add.w   d5,d5
                add.l   d5,a3
                subq.w  #2,a3                   ; Off by 1 ERROR!!!!
                ;addq.w #2,a3


                IFEQ    DEBUG_PANRIGHT
                nop
                ELSE
                tst.w   DEBUG_PANRIGHT_DONE(a4)
                bmi.s   .debug
                nop
                nop
                nop
.debug:
                ENDC

                move.w  MAP_PIXEL_POSX(a4),d5
                ;move.w d5,DEBUG_PANOFX
                sub.w   COLLISION_COMPENSATE(a4),d5
                bpl.s   .x1
		moveq	#0,d5
.x1:
		;bmi.s	.y_check
		move.w  d5,d6                   ; Use map position as shift offset
                and.w   #$f,d6
                ;move.w d6,DEBUG_TLOFFX
                lsr.w   #4,d5
                add.w   d5,d5
                add.l   d5,a3

.rygar_left_edge:
; a3 now points to the left tile where Rygar is standing.
                lea     TILE_ATTRIBUTES(a4),a0
                moveq   #0,d3
                ;moveq  #0,d4
                moveq   #0,d1                   ; Default no obstacle left or right

; This code tests if Rygar has obstacles to his left or right
		cmp.w	#32,SPRITE_PLOT_YPOS(a1)
		blt.s	.no_obstacle
                moveq   #SPRITE_Y_TILE_OFFSET,d0
                move.w  -(MAP_WIDTH*4)+2(a3),d2         ; Get upper tile
                and.w   #OBSTACLE_MASK,d2               ; Only interested in obstacle bit.
                move.w  d2,d5
                move.w  -(MAP_WIDTH*2)+2(a3),d2
                and.w   #OBSTACLE_MASK,d2               ;
                lsr.w   #1,d2
                or.w    d2,d5
                tst.w   d5                              ; If neither obstacle bits set then
                beq.s   .no_obstacle                    ; skip

; There is an obstacle, so determine which way it is relative to Sprite
; Should only do this if it is Rygar Sprite
                ;COL0_RED
                moveq   #1,d1                           ; Default to left
                tst.w   RYGAR_LAST_DIRECTION(a2)
                beq.s   .y_check                        ; Rygar last moved right
                moveq   #-1,d1                          ; He moved left, set to 1
                ;COL0_BLUE
                bra.s   .y_check
                nop

.no_obstacle:
;a5

.y_check:

; Start
                move.w  (a3),d5                         ; Get left most tile
                and.w   #TILE_MASK,d5
                move.w  (a0,d5*2),d4                    ; Get tile attribute
                swap    d4
                move.w  2(a3),d5
                move.w  d5,d2
                and.w   #TILE_MASK,d5
                and.w   #OBSTACLE_MASK,d2               ; Obstacle bit.
                btst    #OBTACLE_BIT,d2
                bne     .on_obstacle
                move.w  (a0,d5*2),d4                    ; Get tile attribute
; d4 now has two tiles
                move.w  (a3),d3                         ; Get left side sprite mask attribute
                swap    d3
                lsr.l   d2,d3                           ; Sprite offset shift
                lsl.l   d6,d4                           ; Tile offset shift
                ;move.l d3,DEBUG_SPLMSK
                ;move.l d4,DEBUG_TLLMSK
                and.l   d3,d4                           ; Is it overlapping?
                ;move.l d4,DEBUG_RSLMSK
                bne.s   .on_platform                    ; Yes...
; Check Right tiles
                moveq   #0,d3
                moveq   #0,d4
                move.w  2(a3),d5                        ; Get right most tile
                move.w  d5,d2
                and.w   #TILE_MASK,d5
                and.w   #OBSTACLE_MASK,d2               ; Obstacle bit.
                ;move.w d5,DEBUG_TLLEFT
                btst    #OBTACLE_BIT,d2
                bne     .on_obstacle
                move.w  (a0,d5*2),d4                    ; Get tile attribute
                swap    d4
                move.w  4(a3),d5
                and.w   #TILE_MASK,d5

                move.w  (a0,d5*2),d4                    ; Get tile attribute
; d4 now has two tiles
                move.w  2(a3),d3                        ; Get left side sprite mask attribute
                swap    d3
                lsr.l   d2,d3                           ; Sprite offset shift
                lsl.l   d6,d4                           ; Tile offset shift
                ;move.l d3,DEBUG_SPRMSK
                ;move.l d4,DEBUG_TLRMSK
                and.l   d3,d4                           ; Is it overlapping?
                ;move.l d4,DEBUG_RSRMSK
                bne.s   .on_platform                    ; Yes...

                bra.s   .exit

.on_obstacle:   addq.w  #SPRITE_Y_OBS_OFFSET,d0         ; Offset value when on an obstacle
                bra.s   .obs_override
.on_platform:
                move.w  SPRITE_PLOT_YPOS(a1),d5                ; Get Y position of Sprite
                add.w   #32,d5                  ; bottom of the sprite.
                and.w   #$f,d5
                cmp.w   #TILE_UPPER_COLLCHECK,d5        ; Only test the top 4 pixels of the tile
                bgt     .exit

.obs_override:  moveq   #0,d7
.exit:          move.l  d0,d5
                move.l  d1,d6

                IFNE    DEBUG_OBS_DISABLE
                moveq   #0,d6
                ENDC

                move.l  (a7)+,d3
                move.l  (a7)+,d1
                move.l  (a7)+,d0
                ;COL0_BLACK
                rts
