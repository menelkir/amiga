DEBUG_TEXT:     movem.l d0-d7/a0-a3,-(a7)

                moveq   #0,d0
                move.w  DEBUG_CURRENT_FOCUS,d0
                lea     DEBUG_TEMPLATE_POINTERS,a3
                move.l  (a3,d0*8),a3                            ; a0 now points to

; Do Words
                rept    6
                move.l  (a3)+,d0                                ; Get X Position
		move.l	d0,d2
                move.l  (a3)+,d1                                ; Get Y Position
                move.l  MEMCHK6_DBGSCREEN(a4),a1                ; Screen Address
                move.l  (a3)+,a0                                ; Get Text Pointer

                bsr     DRAW_TEXT                               ; Draw Text in a0
                lea     TXT_WORD(a4),a0                         ; Buffer for word in
                move.l  (a3)+,a2                                ; Pointer to value
                move.w  (a2),d0                                 ; Dereference
                
		moveq   #TXTLEN_WORD,d7                         ; size of a word
                bsr     HEX_TO_ASCII                            ; convert it
                move.l	d2,d0
		add.w   #7,d0                                   ; Index to the Right of the text
		lea     TXT_WORD(a4),a0                         ; Print the converted word
                move.l  MEMCHK6_DBGSCREEN(a4),a1
                bsr     DRAW_TEXT
                endr

; Do Lwords
                rept    6
                move.l  (a3)+,d0                                ; Get X Position
		move.l	d0,d2
                move.l  (a3)+,d1                                ; Get Y Position
                move.l  MEMCHK6_DBGSCREEN(a4),a1                ; Screen Address
                move.l  (a3)+,a0                                ; Get Text Pointer

                bsr     DRAW_TEXT                               ; Draw Text in a0
                lea     TXT_LWORD(a4),a0                         ; Buffer for word in
                move.l  (a3)+,a2                                ; Pointer to value
                move.l  (a2),d0                                 ; Dereference
                
		moveq   #TXTLEN_LWORD,d7                         ; size of a word
                bsr     HEX_TO_ASCII                            ; convert it
                move.l	d2,d0
		add.w   #7,d0                                   ; Index to the Right of the text
		lea     TXT_LWORD(a4),a0                         ; Print the converted word
                move.l  MEMCHK6_DBGSCREEN(a4),a1
                bsr     DRAW_TEXT
                endr

.exit:          movem.l (a7)+,d0-d7/a0-a3
                rts

