
TXTLEN_LWORD:	equ	8-1
TXTLEN_WORD:	equ	4-1

;------------------------------------------------
; System
ExecBase:	equ	4
AllocMem:	equ	-30-168
FreeMem:	equ	-30-180
_LVOOpenLibrary:	equ	-552
_LVOCloseLibrary:	equ	-414
_LVOOutput:		equ	-60
_LVOWrite:		equ	-48
LOADVIEW:	equ	-222
WAITTOF:	equ	-270
FORBID:		equ	-132

ERR_OPEN_DOSLIB:	equ	-2
ERR_ALLOC_MEM:		equ	-3
;=================================================

;-------------------------------------------------
; Display Constants
PLAYFIELD_SIZE_X	equ	40	; bytes
PLAYFIELD_HEIGHT:	equ	192

BG_SIZE_X:		equ	1600/8
BITPLANES:		equ	5
BITPLANES_BG:		equ	3
BITPLANES_FG:		equ	5
SUNSET_POSY:		equ	10
MOON_POSY:		equ	0
SUNSET_LENGTH:		equ	84
MOON_LENGTH:		equ	80
MOUNTAINS_POSY:		equ	54
LEVELMAP_SIZE_X:	equ	128
LEVELMAP_SIZE_Y:	equ	16

SCREEN_TOP:		equ	0
SCREEN_BOTTOM:		equ	223

;=================================================

;-------------------------------------------------
; BOB Sprites
MAX_SPRITE_ASSETS:	equ	1680		

SPRITE_ASSETS_SIZE_X:	equ	320/8		; 40 (20 words)
SPRITE_ASSETS_SIZE_Y:	equ	84
SPR16_SIZE:		equ	16
SPR16_BITPLANES:	equ	5


NO_SPRITE_MASK_OVER:	equ	3000

MAX_ANIMATIONS_PER_SPRITE:	equ	24
MAX_ENEMY_TYPES:	equ	32

;Sprites to Tiles
;Emblems to Tiles
ITEM_EMBLEM1:	equ	1*1024			;Collectables
ITEM_EMBLEM2:	equ	2*1024
ITEM_EMBLEM3:	equ	3*1024
ITEM_QUESTION:	equ	4*1024
ITEM_STAR:	equ	5*1024
ITEM_TREASURE:	equ	6*1024
ITEM_TIME:	equ	7*1024
ITEM_KILLALL:	equ	8*1024
ITEM_EXTRA1:	equ	9*1024
ITEM_EXTRA2:	equ	10*1024
ITEM_TIGERPOWER	equ	11*1024
ITEM_STARPOWER	equ	12*1024
ITEM_CROWNPOWER	equ	13*1024
ITEM_SUNPOWER:	equ	14*1024
ITEM_CROSSPOWER	equ	15*1024
EMBLEM16:	equ	16*1024

DEFAULT_MAP_END_POINT:	equ	$9f8			; 9f8

SPR_RESERVED_PLAYER:		equ	0
SPR_RESERVED_SHIELD:		equ	1
SPR_RESERVED_LEFT_CAROUSEL:     equ     2
SPR_RESERVED_RIGHT_CAROUSEL:    equ     3

; Sprite numbers alloctaed to each type
SPR_RYGAR:	equ	0
SPR_DRONE:	equ	1
SPR_SQUIRREL:	equ	2
SPR_GRIFFIN:	equ	3
SPR_APE:	equ	4
SPR_MONKEY:	equ	5
SPR_ROCK:	equ	6
SPR_CAROUSEL:	equ	7
SPR_MUTANTFROG:	equ	8
SPR_RHINO:	equ	9
SPR_DRAGON1:	equ	10
SPR_DRAGON2:	equ	11
SPR_LAVAMAN:	equ	12
SPR_EVIL_UL:	equ	13
SPR_EVIL_UR:	equ	14
SPR_EVIL_DL:	equ	15
SPR_EVIL_DR:	equ	16
SPR_BIGRHINO1:	equ	17
SPR_BIGRHINO2:	equ	18
SPR_RIDER:	equ	19
SPR_TRIBESMAN:	equ	20
SPR_GIANTDEMON:	equ	21
SPR_GIANTWORM:	equ	22
SPR_FIREBALL:	equ	23
SPR_LAVASPIT:	equ	24
SPR_CAVEBAT:	equ	25
SPR_ROCKET:	equ	26
SPR_CRAB:	equ	27
SPR_LIZARD:	equ	28
SPR_LIZTONGUE:	equ	29
SPR_VILLAGER:	equ	30
SPR_TESTSPRITE:	equ	31
SPR_SHIELD:	equ	32
SPR_SPECIAL:	equ	33
SPR_HAMMER:	equ	34
SPR_REAPER:	equ	35
SPR_FISH:	equ	36
SPR_WEAPON:	equ	37
SPR_LIGAR1:	equ	38
SPR_LIGAR2:	equ	39
SPR_CROWD:	equ	40
SPR_ELDER:	equ	41

SCROLL_LOCK_POSX:	equ	96+32

; Sprites by type
; These determine the collision screen that the sprite mask is placed in.
SPR_TYPE_PLAYER:	equ	0		;32x32
SPR_TYPE_ENEMY_2X32:	equ	1		;32x32
SPR_TYPE_ENEMY_2X16:	equ	2		;32x16
SPR_TYPE_ENEMY_2X64:	equ	3		;32x64
SPR_TYPE_ENEMY_3X48:	equ	4		;48x48
SPR_TYPE_ENEMY_1X16:	equ	5		;16x16
SPR_TYPE_BLOCK_48x16:	equ	6
SPR_TYPE_BLOCK_32x16:	equ	7
SPR_TYPE_BLOCK_16x16:	equ	8
SPR_TYPE_OBJECT:	equ	9		; 32x32
SPR_TYPE_HIDDEN:	equ	10


SPR_TYPE_ENEMY_HW:	equ	32
SPR_TYPE_ENEMY_SLOT1:	equ	33
SPR_TYPE_ENEMY_SLOT2:	equ	34
SPR_TYPE_ENEMY_SLOT3:	equ	35
SPR_TYPE_ENEMY_SLOT4:	equ	36
SPR_TYPE_ENEMY_SLOT5:	equ	37
SPR_TYPE_ENEMY_SLOT6:	equ	38
SPR_TYPE_ENEMY_SLOT7:	equ	39
SPR_TYPE_ENEMY_SLOT8:	equ	40
SPR_TYPE_ENEMY_SLOT9:	equ	41
SPR_TYPE_ENEMY_SLOT10:	equ	42
SPR_TYPE_ENEMY_SLOT11:	equ	43
SPR_TYPE_ENEMY_SLOT12:	equ	44

SPRITE_MAG:		equ	4		; Sprite position interpolation 4 = 16 frames per 1 pixel

ENABLE_FG_WATER_CYCLE:	equ	1
DISABLE_FG_WATER_CYCLE:	equ	0

SPRITE_MOVE_LEFT:                  equ     -1
SPRITE_MOVE_RIGHT:                 equ     0

SPRITE_DEFAULT_ANIM_LEFT:	equ	6
SPRITE_DEFAULT_ANIM_RIGHT:	equ	7


; Sprite offsets (used on a1)
SPRITE_PLOT_ANIM:	equ	0		; current animation
SPRITE_PLOT_XPOS:	equ	2		; Sprite screen x position
SPRITE_PLOT_YPOS:	equ	4		; Sprite screen y position
SPRITE_PLOT_TYPE:	equ	6		; Sprite control word/Type
SPRITE_PLOT_BUFFPTR:	equ	8		; Sprite copy buffer address
SPRITE_PLOT_BUFFPTR_OLD:	equ	12	; Old Sprite buff pointer

; Local Sprites Variable States (a2)
THIS_SPRITE_ACTIVE:		equ	0		; set to 0 if not activated
THIS_SPRITE_ID:			equ	2		; Current Sprite Number
THIS_SPRITE_STATUS:		equ	4		; Arbitrary
THIS_SPRITE_XPOS:		equ	6		; Current Interpolated X Position
THIS_SPRITE_YPOS:		equ	8		; Current Interpolated Y Position
THIS_SPRITE_TYPE:		equ	10		; The Sprite Type
THIS_SPRITE_MAP_XPOS:		equ	12		; Current Interpolated Map X Position
THIS_SPRITE_MAP_YPOS:		equ	14		; Current Interpolated Map Y Position
SPRITE_DESTROY_END_ROUND:	equ	16		; Set when sprite is destroyed at the of the round.
THIS_SPRITE_OFFSCREEN:		equ	18		; If sprite is currently off screen
THIS_SPRITE_HWSLOT:		equ	20		; Hardware sprite slot.
THIS_SPRITE_SWEEP_INDEX:	equ	22		; Sweep index for death of enemy
THIS_SPRITE_STUN_TIMER:		equ	24		; Stun Timer
THIS_SPRITE_DIRECTION:		equ	26		; Sprite Direction (used for stun)
THIS_SPRITE_BOUNDARY_TIMER:	equ	28		; Boundary TImer for Sprite.
THIS_SPRITE_PARAMETER:		equ	30		; Used for passing parameters to sprites (Tribesman formations and stuff).

FRAMES_OFFSCREEN_BEFORE_DESTROYED:	equ	100






