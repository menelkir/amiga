NUMBER_OF_CHUNKS:       equ     2
MEMF_ANY:       equ     0
;MEMF_CHIP:      equ     2

AvailMem:	equ	-216

                even


; Allocate Chip Ram
;       move.l  #RESERVE_CHIP_SIZE,d0
;       moveq   #2,d1

;d1= Get RAM Type
;d0= Get RAM Size
ALLOCATE_RAM:
                move.l  ExecBase,a6
                jsr     AllocMem(a6)
                move.l  d0,MEM_BASE(a4)
                rts

FREE_RAM:       move.l  ExecBase,a6
                jsr     FreeMem(a6)
                rts

OPEN_DOSLIB:    move.l  ExecBase,a6
                lea     DOSNAME(a4),a1
                moveq   #0,d0
                jsr     _LVOOpenLibrary(a6)
                rts

CLOSE_DOSLIB:   move.l  ExecBase,a6
                move.l  DOSBASE(a4),a1
                jsr     _LVOCloseLibrary(a6)
                rts

; a0 = Text Pointer
PRINT_STRING:
                movem.l d0-d7/a0-a6,-(a7)
                move.l  DOSBASE(a4),a6
                moveq   #-1,d3
                move.l  a0,d2
.strlen:        addq.l  #1,d3
                tst.b   (a0)+
                bne.s   .strlen
                jsr     _LVOOutput(a6)
                move.l  d0,d1
                jsr     _LVOWrite(a6)
                move.l  a6,a1
                movem.l (a7)+,d0-d7/a0-a6
                rts

GFC
GET_FREE_CHIPRAM:
		move.l	$4,a6
		move.l	#MEMF_CHIP,d1
		jsr	AvailMem(a6)
		move.l	d0,MEMF_CHIP_AVAILABLE(a4)
		rts
		
		
; d0=Value
; a0=
HEX_TO_ASCII_MEM:
		movem.l	d0-d7/a0,-(a7)
		moveq	#8-1,d7
		move.l	d0,d2
		
.loop:
		move.b	d0,d1
		and.b	#$f,d1
		add.b	#$30,d1
		cmp.b	#$39,d1
		ble.s	.next
		add.b	#7,d1			;must be a alpha
		
.next:		lsr.l	#4,d0
		move.b	d1,(a0,d7)
		dbf	d7,.loop
		movem.l	(a7)+,d0-d7/a0
		
		rts

; d5.l 0=OK, -1=Error
ALLOCATE_MEMORY moveq   #0,d5
                moveq   #0,d7

.next_chunk:    move.l  d7,d6
                lsl.w   #5,d6
                lea     MEMINIT_TABLE(a4),a3
                add.l   d6,a3                   ; Pointer to memory alloc
                tst.w   (a3)                    ; End of the table?
                bmi.s   .exit                   ; All done.

                move.l  12(a3),a0               ; Move text string address
                ;bsr    PRINT_STRING

                move.l  (a3),d1                 ; Get RAM Type
                move.l  4(a3),d0                ; Get RAM Size
                bsr     ALLOCATE_RAM            ; Allocate the Ram
                tst.l   d0
                beq     RAM_ALLOCATE_ERROR

                move.l  8(a3),a2                ; Get Membase Pointer
                move.l  d0,(a2)                 ; Store Membase

                move.l  24(a3),a2               ; Get chunks table

.loop:          tst.w   (a2)
                bmi.s   .next
                move.l  d0,d1
                move.l  (a2),a3                 ; Get memory pointer
                add.l   4(a2),d1
                move.l  d1,(a3)                 ; store offset
                addq.w  #8,a2
                bra.s   .loop
.next:          addq.w  #1,d7
                bra.s   .next_chunk
.exit:          rts


DEALLOCATE_MEMORY:
                subq.w  #1,d7
.next_chunk:    tst.w   d7
                bmi.s   .exit
                move.l  d7,d6
                lsl.w   #5,d6
                lea     MEMINIT_TABLE(a4),a3
                add.l   d6,a3                   ; Pointer to memory alloc
                move.l  8(a3),a2                ; Get Membase Pointer
                move.l  (a2),a1                 ; Get Memory Base
                move.l  4(a3),d0
                bsr     FREE_RAM                ; Free the Ram
                tst.l   d0
                beq     RAM_DEALLOCATE_ERROR
.next:          subq.w  #1,d7
                bra.s   .next_chunk
.exit:          rts


RAM_ALLOCATE_ERROR:
                lea     TXT_FAIL_ALLOCATE(a4),a0
                bsr     PRINT_STRING
                moveq   #-1,d5
                rts


RAM_DEALLOCATE_ERROR:
                lea     TXT_FAIL_DEALLOCATE(a4),a0
                bsr     PRINT_STRING
                moveq   #-1,d5
                moveq   #0,d0
                rts

ALIGN_64:
        move.l  MEMCHK1_FRONT_FSCROLL(a4),d0
        addq.l  #8,d0
        and.l   #$fffffff8,d0           ;64 bit align
        move.l  d0,MEMCHK1_FRONT_FSCROLL(a4)

        move.l  MEMCHK1_FRONT_BSCROLL(a4),d0
        addq.l  #8,d0
        and.l   #$fffffff8,d0           ;64 bit align
        move.l  d0,MEMCHK1_FRONT_BSCROLL(a4)

        move.l  MEMCHK1_BACK_FSCROLL(a4),d0
        addq.l  #8,d0
        and.l   #$fffffff8,d0           ;64 bit align
        move.l  d0,MEMCHK1_BACK_FSCROLL(a4)

        move.l  MEMCHK1_POST_SCROLL(a4),d0
        addq.l  #8,d0
        and.l   #$fffffff8,d0           ;64 bit align
        move.l  d0,MEMCHK1_POST_SCROLL(a4)

        move.l  MEMCHK5_LOADBUFF(a4),d0
        addq.l  #4,d0
        and.l   #$fffffffc,d0           ;32 bit align
        move.l  d0,MEMCHK5_LOADBUFF(a4)

        move.l  MEMCHK6_DBGSCREEN(a4),d0
        addq.l  #8,d0
        and.l   #$fffffff8,d0           ;64 bit align
        move.l  d0,MEMCHK6_DBGSCREEN(a4)
        rts

