
ROUND24_LIZARD1_XPOS:	equ	$1b0			; right
ROUND24_LIZARD1_YPOS:	equ	-$c0

ROUND24_LIZARD2_XPOS:	equ	$12c			;
ROUND24_LIZARD2_YPOS:	equ	-$a4

ROUND24_LIZARD3_XPOS:	equ	$12c			; 
ROUND24_LIZARD3_YPOS:	equ	-$e4

ROUND24_LIZARD4_XPOS:	equ	$12c			; 
ROUND24_LIZARD4_YPOS:	equ	-$34


ROUND24_EVENT_INJECT_LIZARDS:
	nop
	
	move.w	#2,VERTICAL_COMPENSATE(a4)
	
; Lizard positions enable in the cave.
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#SPR_LIZARD,d0
	move.w	#ROUND24_LIZARD1_XPOS,d1
	move.w	#ROUND24_LIZARD1_YPOS,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	lea	HDL_LIZARD(pc),a0
	bsr	PUSH_SPRITE
	tst.w	d4
	bmi	.max_reached
	move.w	d4,BASE_LIZARD_SPRITE(a4)
	movem.l	(a7)+,d0-d7/a0-a3
	
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#SPR_LIZARD,d0
	move.w	#ROUND24_LIZARD2_XPOS,d1
	move.w	#ROUND24_LIZARD2_YPOS,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	lea	HDL_LIZARD(pc),a0
	bsr	PUSH_SPRITE
	tst.w	d4
	bmi.s	.max_reached
	movem.l	(a7)+,d0-d7/a0-a3
	
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#SPR_LIZARD,d0
	move.w	#ROUND24_LIZARD3_XPOS,d1
	move.w	#ROUND24_LIZARD3_YPOS,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	lea	HDL_LIZARD(pc),a0
	bsr	PUSH_SPRITE
	tst.w	d4
	bmi.s	.max_reached
	movem.l	(a7)+,d0-d7/a0-a3
	
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#SPR_LIZARD,d0
	move.w	#ROUND24_LIZARD4_XPOS,d1
	move.w	#ROUND24_LIZARD4_YPOS,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	lea	HDL_LIZARD(pc),a0
	bsr	PUSH_SPRITE
	
.max_reached:
	movem.l	(a7)+,d0-d7/a0-a3
	rts


	