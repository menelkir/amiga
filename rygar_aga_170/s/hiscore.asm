
MAX_HISCORES:	equ	5

HISCORE_MAX_RANK:	equ	5

HI_COLOUR_FLASH_RED:	equ	3

HISCORE_ENTRY_DISABLE:	equ	4

; d0=Number of bombs bnus

HISCORE_WINDOW_X: equ     32
HISCORE_WINDOW_Y: equ     32

HISCORE_SCORE_STEP:       equ     200
HISCORE_DELAY_TIME:       equ     350             ; 7 seconds

HISCORE_WATER_DELAY:      equ     5
HISCORE_CHAR_DELAY:       equ     0

HISCORE_SCENE_SIZE_Y:     equ     184
HISCORE_SCENE_BITPLANES:  equ     5
HISCORE_SCENE_COLOURS:    equ     32

HISCORE_WAIT_TIME:        equ     120

HISCORE_WATER_ANIM_YPOS:  equ     152

HISCORE_TEXT_LINES:       equ     8


HISCORE:
	FUNCID	#$11c93066
	
	bsr	CLEAR_FRONT_SCREEN_BUFFERS
	bsr	CLEAR_BACK_SCREEN_BUFFERS
	
	clr.w	INPUT_DISABLED(a4)
	clr.w	LIGAR_FIGHT(a4)
	clr.w	DISABLE_ENEMIES(a4)
	clr.w	HISCORE_ENTRY_OFFSET(a4)
	
;---------------------------
	moveq	#0,d1
	move.l	VAL_PLAYER1_SCORE,d0
	
	move.w	ROUND_CURRENT(a4),d1
	addq.w	#1,d1
	swap	d1
	move.w	RYGAR_CURRENT_RANK(a4),d1
	addq.w	#1,d1

	lea	HISCORE_TABLE(a4),a1
	moveq	#MAX_HISCORES+1,d2
	bsr	HISCORE_SORT
	
	lea	HISCORE_TABLE(a4),a0
	move.l	VAL_PLAYER1_SCORE,d0
	moveq	#MAX_HISCORES-1,d2
	bsr	GET_HISCORE_RANK
	move.w	d1,HISCORE_RANK(a4)
; If rank is 1 then it is the hi-score

	tst.w	d1
	bne.s	.not_a_new_hiscore

	clr.w	$100
	
	move.l  VAL_PLAYER1_SCORE(a4),d0
        bsr     Binary2Decimal
	lea     TXT_HISCORE_SCORE(a4),a1
        moveq   #8-1,d6					; d6 7
        bsr     COPY_DECIMAL

.not_a_new_hiscore:
	
	
;--------------------------

	clr.w	HISCORE_UPDATE_TIMER(a4)
	move.w	#(64*10),HISCORE_DISPLAY_TIMER(a4)		;pause timer
	cmp.w	#HISCORE_MAX_RANK,HISCORE_RANK(a4)
	bge.s	.no_tune
	move.w	#64*60,TIME_REMAINING(a4)
	move.w	#64*60,HISCORE_DISPLAY_TIMER(a4)
	move.w	#-1,HISCORE_UPDATE_TIMER(a4)

		
	moveq	#MUSIC_HISCORE,d0
	bsr	PLAY_TUNE
	
	
.no_tune:
	clr.l	VAL_PLAYER1_SCORE(a4)
 
; Get the palette
        move.l  RYGAR_HISCORE_ASSETS(a4),a0
        lea     HISCORE_SCENE_COLOUR_PAL(a4),a1
        bsr     GET_PALETTE_FROM_IFF
	lea     HISCORE_SCENE_COLOUR_PAL(a4),a1
        move.w  #TEXT_RGB,62(a1)
	
        bsr     SETSCR_BBUFF
        lea     LSTPTR_BACK_SCREEN(a4),a1
        bsr     SET_FRAMEBUFF
	
	bsr	INIT_PANELS
;----
	bsr	WAIT_FOR_VERTICAL_BLANK	
        move.w  #%0000000110000000,DMACON(a5)		; Disable Copper
	bsr	WAIT_FOR_VERTICAL_BLANK
	
        bsr     INIT_HISCORE_COPPER

        IFEQ    ENABLE_DEBUG
	move.w	#COPRUN_HISCORE,COPRUN_TYPE(a4)
        move.l  MEMCHK2_COPPER1(a4),COP1LCH(a5)
        move.l  MEMCHK2_COPPER1(a4),COP2LCH(a5)
        ;         FEDCBA9876543210
        move.w  #%1000011111000000,DMACON(a5)
        ENDC

	bsr	HISCORE_SETUP

.reset
        bsr     HISCORE_TRANS_BLACK_TO_COLOUR



.update:
	bsr	WAIT_FOR_VERTICAL_BLANK
	bsr	HISCORE_ENTRY_COLOUR_CYCLE
	addq.w	#1,FRAME_BG(a4)
	
	subq.w	#1,HISCORE_DISPLAY_TIMER(a4)
	tst.w	HISCORE_DISPLAY_TIMER(a4)
	beq	.done	
	
	bsr	HISCORE_JOY_DETECT

; If the high-score did not rank then skip the entry option
	cmp.w	#HISCORE_MAX_RANK,HISCORE_RANK(a4)
	bge.s	.no_rank
	cmp.w	#HISCORE_ENTRY_DISABLE,HISCORE_ENTRY_OFFSET(a4)
	bge.s	.border
	
	tst.w	HISCORE_UPDATE_TIMER(a4)
	beq.s	.skip
	bsr	UPDATE_TIMER	
	bsr	HISCORE_INPUT
	bsr	DRAW_HIENTRY_INITIALS
.border:
	bsr	DRAW_HISCORE_ENTRY_BORDER


.skip:	cmp.w	#HISCORE_ENTRY_DISABLE,HISCORE_ENTRY_OFFSET(a4)
	blt.s	.update
	tst.w	HIENTRY_DELAY_VAR(a4)
	bmi.s	.check_fire
.no_rank:
	subq.w	#1,HIENTRY_DELAY_VAR(a4)		; End of hiscore delay
	tst.w	HIENTRY_DELAY_VAR(a4)
	bpl.s	.update
.check_fire:
	btst	#7,CIAAPRA					; fire pressed
	bne.s	.update
.fire_held:	
	btst	#7,CIAAPRA					; fire pressed
	beq.s	.fire_held	

.done:	bsr	HISCORE_TRANS_COLOUR_TO_BLACK

	bsr	STOP_TUNE

	clr.w	MENU_EXIT(a4)
	moveq   #0,d6
	rts



HISCORE_SETUP:
; Draw the BEST 5 caption
	moveq	#5,d7

	moveq	#32,d0
	moveq	#14,d2
	moveq	#1,d3
.best5:	bsr	BLIT_HISCORE_ASSET
	addq.w	#1,d0
	addq.w	#2,d2
	dbf	d7,.best5
	
; Draw the column header - RANK  SCORE  NAME ROUND
	moveq	#4,d0
	moveq	#4,d1
	moveq	#0,d2
	moveq	#3,d6
	lea	HISCORE_HEADER_TEXT(a4),a0
	bsr	DRAW_HISCORE_TEXT

; Draw the top 5 numbers
	moveq	#4,d7
	moveq	#39,d0
	moveq	#4,d2
	moveq	#6,d3
.font5:	bsr	BLIT_HISCORE_ASSET
	addq.w	#1,d0
	addq.w	#3,d3
	dbf	d7,.font5

; Draw the full stop
	moveq	#4,d7
	moveq	#45,d0
	moveq	#6,d2
	moveq	#6,d3
.point:	bsr	BLIT_HISCORE_ASSET
	addq.w	#3,d3
	dbf	d7,.point
	
; Draw Hi-Score Values
	bsr	DRAW_HISCORE_VALUES
	
; Draw Hi-Score Names
	bsr	DRAW_HISCORE_NAMES
	
; Draw Ranks
	bsr	DRAW_HISCORE_RANKS

; Draw Round
	bsr	DRAW_HISCORE_ROUNDS
	rts
	
; d0 = Asset number to blit
; d2 = X Position on Screen 8x8
; d3 = Y Position on Screen 8x8 
; a0 = Screen Pointers
BLIT_HISCORE_ASSET:
	movem.l	d0-d4/a0-a2,-(a7)
	move.l	LSTPTR_BACK_SCREEN(a4),a0
	move.l	RYGAR_HISCORE_ASSETS(a4),a1
	add.l	#$b0,a1

; Find the source asset
	moveq	#0,d1
	moveq	#0,d4			; offset to add
	btst	#5,d0			; At least 32?
	beq.s	.blit_from_top_row
	move.l	#((512/8)*5)*8,d4
	sub.l	#32,d0
	moveq	#1,d1
.blit_from_top_row:
	add.l	d4,a1
			
	add.w	d0,d0
	add.l	d0,a1			; We are now pointing at the correct asset.
	
; Now lets find the destination in screen ram.
	lsl.w	#3,d3			; Multiply Y by 8
	
; Index to Y Position
        lea     CANVAS_TABLE_Y3(a4),a2
        add.l   (a2,d3*4),a0            ; add y position

	add.w	d2,a0			; Index to X Position
	
; a0 = Pointer to screen address offset
; a1 = Asset
; d1 = 0 (8 y pixels), 1=16
; Copy a simple block.

	move.w	(ASSET_SIZES,d1*2),d2
	subq.w	#1,d2
	
	move.l	#PLAYFIELD_SIZE_X,d0
	move.l	#512/8,d1
	
.loop:
	move.b	(a1),(a0)
	move.b	1(a1),1(a0)
	add.l	d1,a1
	add.l	d0,a0
	dbf	d2,.loop
	
	movem.l	(a7)+,d0-d4/a0-a2
	
	rts
	
ASSET_SIZES:	dc.l	(40<<16!80)
	

;
; INIT_HISCORE_COPPER
;
; Initialize the Copper list
;
INIT_HISCORE_COPPER:
	FUNCID	#$6075f776
	
        move.l  MEMCHK2_COPPER1(a4),a1
        move.l  a1,d4

        move.w  #DDFSTRT,(a1)+  ;
        move.w  #$0038,(a1)+    ; 3c
        move.w  #DDFSTOP,(a1)+  ;
        move.w  #$00d0,(a1)+    ; a4

        move.l  #$1c01ff00,(a1)+
        move.l  a1,COPPTR_UPPER_PANEL(a4)
        bsr     INIT_COPPER_SPRITES

        bsr     COPPER_UPPER_PANEL

        move.l  #$3e0dfffe,(a1)+

        move.w  #FMODE,(a1)+
        move.w  #%0000000000001100,(a1)+

        move.w  #BPLCON0,(a1)+  ; 0             ; This is play area
        move.w  #$0000,(a1)+    ; 2             ; 8 PLanes here
        move.w  #BPLCON1,(a1)+  ; 4
        move.w  #0,(a1)+        ; 6
        move.w  #BPLCON2,(a1)+  ; 8
        move.w  #$00,(a1)+      ; 30
        move.w  #BPL1MOD,(a1)+  ;
        move.w  #$a0,(a1)+      ; a0
        move.w  #BPL2MOD,(a1)+  ;
        move.w  #$a0,(a1)+      ; a0

        move.w  #DIWSTRT,(a1)+  ;
        move.w  #$27a1,(a1)+    ;
        move.w  #DIWSTOP,(a1)+  ;
        move.w  #$10a8,(a1)+    ;

        lea     COPPTR_HARDWARE_UPPER_SPRITE_0(a4),a3
        bsr     CREATE_COPPER_SPRITE_POINTERS

; Load Main Palette
        lea     HISCORE_SCENE_BLACK_PAL(a4),a0
        move.l  #$180,d2
        moveq   #HISCORE_SCENE_COLOURS-1,d7
        move.l  a1,COPPTR_HISCORE_SCENE_PAL(a4)

.main_pal:
        move.w  d2,(a1)+
        move.w  (a0)+,(a1)+
        addq.w  #2,d2
        dbf     d7,.main_pal

        move.l  #$4001ff00,(a1)+
        move.w  #BPLCON0,(a1)+  ; 0             ; This is play area
        move.w  #$5200,(a1)+    ; 2

        lea     LSTPTR_BACK_SCREEN(a4),a0
        moveq   #HISCORE_SCENE_BITPLANES-1,d7                     ; number of planes to load
        move.w  #BPL0PTL,d2
        move.w  #BPL0PTH,d3

.plane: move.l  (a0)+,d0
        move.w  d2,(a1)+
        move.w  d0,(a1)+
        swap    d0
        move.w  d3,(a1)+
        move.w  d0,(a1)+
        addq.l  #4,d2
        addq.l  #4,d3
        dbf     d7,.plane
	
; $188 normal colour is $f00
; $1a0 normal colour is $bbb

	moveq	#$12,d7

	move.l	#$7201fffe,d2
	move.l	#$7285fffe,d3
	move.l	#$72a5fffe,d4

	move.l	a1,COPPTR_HISCORE_CYCLE(a4)
.cycle:
	move.l	d2,(a1)+    			; 4
        move.l  #$01900800,(a1)+		; 8
        move.l  #$01a00bbb,(a1)+		; 12
        
	move.l	d3,(a1)+	 
        move.l  #$019000f0,(a1)+	
        move.l  #$01a00f0f,(a1)+		; 24
	
	move.l	d4,(a1)+              
        move.l  #$01900800,(a1)+	
        move.l  #$01a00bbb,(a1)+   		; 36
	
	add.l	#$01000000,d2
	add.l	#$01000000,d3
	add.l	#$01000000,d4
	dbf	d7,.cycle
	
        bsr     COPPER_LOWER_PANEL
	
	lea	DUMMY_BPLCON0(a4),a3
        move.l  a3,COPPTR_BPLCON(a4)

        move.l  #$fffffffe,(a1)+
        rts


HIC
HISCORE_ENTRY_COLOUR_CYCLE:
	move.w	FRAME_BG(a4),d0
	and.w	#1,d0
	beq.s	.exit
	move.l	COPPTR_HISCORE_CYCLE(a4),a0
	add.w	#$111,HISCORE_CYCLE_COLOUR
	moveq	#$12,d7
	
	move.w	HISCORE_CYCLE_COLOUR,d6
	move.w	d6,d5
	rol.w	#4,d5
	
.loop:


	add.w	#$e11,d5
	and.w	#$f00,d5
	move.w	d5,18(a0)
	
	sub.w	#$1e1,d6	
	and.w	#$0f0,d6
	move.w	d6,22(a0)
	
	add.l	#36,a0
	dbf	d7,.loop
.exit:	rts
	
	
	
	
	
HISCORE_CYCLE_COLOUR:		dc.w	0

; d0=8x8 x
; d1=8x8 y
; d2=0 = normal font, 1=Time font.
; a0=text
; d6 = Bitplane
DRAW_HISCORE_TEXT:
	FUNCID	#$f749e301
	
	movem.l	d0-d3/a0-a2,-(a7)
	
        move.l  LSTPTR_CURRENT_SCREEN(a4),a1
        add.l   d0,a1                                   ; add x position
        lsl.w   #3,d1                                   ; multiply y by 8
        lea     CANVAS_TABLE_Y3(a4),a2
        add.l   (a2,d1*4),a1                            ; add y position

        lea     TAB_Y(a4),a2
        add.w   (a2,d6*2),a1

        moveq   #32,d1
        moveq   #(512/8)-16,d3


.loop:
        move.l  FONT_ASSET(a4),a2
        add.w   #86,a2                  ; Offset to BODY

        tst.w   d2
        beq.s   .normal_font

.timer_font:
        add.l   d3,a2                   ; timer font.

.normal_font:
        move.b  (a0)+,d0
        beq.s   .exit
	bmi.s	.exit
        sub.b   d1,d0

.draw:  add.l   d0,a2           ; index into character
        move.b  (a2),(a1)
        move.b  FONT_WIDTH*1(a2),(PLAYFIELD_SIZE_X*5)*1(a1)
        move.b  FONT_WIDTH*2(a2),(PLAYFIELD_SIZE_X*5)*2(a1)
        move.b  FONT_WIDTH*3(a2),(PLAYFIELD_SIZE_X*5)*3(a1)
        move.b  FONT_WIDTH*4(a2),(PLAYFIELD_SIZE_X*5)*4(a1)
        move.b  FONT_WIDTH*5(a2),(PLAYFIELD_SIZE_X*5)*5(a1)
        move.b  FONT_WIDTH*6(a2),(PLAYFIELD_SIZE_X*5)*6(a1)
        addq.w  #1,a1
        bra     .loop
.exit:  
	movem.l	(a7)+,d0-d3/a0-a2
	rts



; Transition scene colours from Black to its paletter
HISCORE_TRANS_BLACK_TO_COLOUR:
	FUNCID	#$0af305a1
	
        moveq   #15,d7

.loop:  bsr     WAIT_FOR_VERTICAL_BLANK
        lea     HISCORE_SCENE_BLACK_PAL(a4),a0
        lea     HISCORE_SCENE_COLOUR_PAL(a4),a1
        lea     HISCORE_SCENE_FLUID_PAL(a4),a2
        lea     HISCORE_SCENE_COLOURS*2(a0),a3
.next:  move.w  (a0)+,d2
        move.w  (a1)+,d1
        bsr     COLOUR_INTERPOLATE
        move.w  d0,(a2)+
        cmp.l   a0,a3
        bne.s   .next

        bsr     HISCORE_COPY_PAL_TO_COPPER
        dbf     d7,.loop
        rts


; Transition scene colours from Black to its paletter
HISCORE_TRANS_COLOUR_TO_BLACK:
	FUNCID	#$5e70b45d
	
        moveq   #15,d7

.loop:  bsr     WAIT_FOR_VERTICAL_BLANK
        lea     HISCORE_SCENE_COLOUR_PAL(a4),a0
        lea     HISCORE_SCENE_BLACK_PAL(a4),a1
        lea     HISCORE_SCENE_FLUID_PAL(a4),a2
        lea     HISCORE_SCENE_COLOURS*2-2(a0),a3
.next:  move.w  (a0)+,d2
        move.w  (a1)+,d1
        bsr     COLOUR_INTERPOLATE
        move.w  d0,(a2)+
        cmp.l   a0,a3
        bne.s   .next

        bsr     HISCORE_COPY_PAL_TO_COPPER
        dbf     d7,.loop
        rts


HISCORE_COPY_PAL_TO_COPPER:
	FUNCID	#$31c7e295
	
        move.l  a0,-(a7)
        move.l  a1,-(a7)
        lea     HISCORE_SCENE_FLUID_PAL(a4),a0
        move.l  COPPTR_HISCORE_SCENE_PAL(a4),a1

        rept    32
        move.w  (a0)+,2(a1)
        addq.w  #4,a1
        endr

        move.l  (a7)+,a1
        move.l  (a7)+,a0
        rts




;----------------------------------------------
; Draw the hiscore initials down the display
;==============================================
DRAW_HISCORE_NAMES:
	moveq	#MAX_HISCORES-1,d7
	lea	HISCORE_TABLE(a4),a2
	moveq	#22,d0
	moveq	#7,d1
	moveq	#4,d6

.loop:	movem.l	d0-d7/a0-a3,-(a7)
	move.l	d0,-(a7)
	move.l	8(a2),d0

	lea	TXT_TEMP(a4),a0
	and.l	#$ffffff00,d0
	move.b	d0,3(a0)
	lsr.l	#8,d0
	move.b	d0,2(a0)
	lsr.l	#8,d0
	move.b	d0,1(a0)
	lsr.l	#8,d0
	move.b	d0,0(a0)
	move.l	(a7)+,d0

	moveq	#0,d2
	bsr	DRAW_HISCORE_TEXT
	
	moveq	#1,d6
	bsr	DRAW_HISCORE_TEXT
	
	movem.l	(a7)+,d0-d7/a0-a3

	add.w	#16,a2
	addq.w	#3,d1

	dbf	d7,.loop
	rts


;---------------------------------
; Draw the high score values 
;=================================
DRAW_HISCORE_VALUES:
	moveq	#MAX_HISCORES-1,d7
	lea	HISCORE_TABLE(a4),a2
	moveq	#10,d0
	moveq	#7,d1
	moveq	#4,d6

.loop:	movem.l	d0-d7/a0-a3,-(a7)
	move.l	d0,-(a7)
	move.l	4(a2),d0
	lea	TXT_SCORETMP(a4),a0
	clr.l	(a0)
	clr.l	4(a0)
	bsr	Binary2Decimal
	move.l	a0,a2
	move.l	(a7)+,d0
	
	lea	TMP_HISCORE(a4),a1
	moveq	#0,d5

.spaces:	
	move.b	(a0)+,d4
	sub.b	#$30,d4
	bmi.s	.stop
	cmp.b	#9,d4
	bgt.s	.stop
	addq.w	#1,d5
	cmp.b	#8,d5
	beq.s	.stop
	bra.s	.spaces
	
.stop:
	move.l	a2,a0
	moveq	#8,d4
	sub.b	d5,d4

.stop2:	tst.b	d4
	beq.s	.do_score
	move.b	#$20,(a1)+
	subq.b	#1,d4
	bra.s	.stop2

.do_score:	
	tst.b	d5
	beq.s	.stop1
	move.b	(a0)+,(a1)+
	subq.b	#1,d5
	bra.s	.do_score

.stop1:
; Update the score in the panel.
	moveq	#0,d2
	moveq	#4,d6
	lea	TMP_HISCORE(a4),a0
	bsr	DRAW_HISCORE_TEXT
	moveq	#1,d6
	lea	TMP_HISCORE(a4),a0
	bsr	DRAW_HISCORE_TEXT
	
	movem.l	(a7)+,d0-d7/a0-a3
	add.w	#16,a2
	addq.w	#3,d1
	dbf	d7,.loop
	rts



DRAW_HISCORE_RANKS:	
	moveq	#MAX_HISCORES-1,d7
	lea	HISCORE_TABLE(a4),a2
	moveq	#7,d2
	moveq	#7,d3
	moveq	#4,d6

.loop:	movem.l	d0-d7/a0-a3,-(a7)
	move.l	(a2),d0				; Get rank
	bsr	BLIT_HISCORE_ASSET
	movem.l	(a7)+,d0-d7/a0-a3
	add.w	#16,a2
	addq.w	#3,d3
	dbf	d7,.loop
	rts

DHR	
DRAW_HISCORE_ROUNDS:	
	moveq	#MAX_HISCORES-1,d7
	lea	HISCORE_TABLE(a4),a2


	moveq	#6,d3
	moveq	#4,d6

.loop:	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#31,d2
	
	move.l	12(a2),d1				; Get round number
	lea	HEX2DECTAB(a4),a1
	move.b	(a1,d1),d1

	and.l	#$0f,d1
	add.b	#46,d1
	move.b	d1,d0
	bsr	BLIT_HISCORE_ASSET

	subq.w	#2,d2
	move.l	12(a2),d1				; Get round number
	lea	HEX2DECTAB(a4),a1
	move.b	(a1,d1),d1
	lsr.w	#4,d1
	add.b	#46,d1
	move.b	d1,d0
	bsr	BLIT_HISCORE_ASSET

	movem.l	(a7)+,d0-d7/a0-a3
	add.w	#16,a2
	addq.w	#3,d3
	dbf	d7,.loop
	rts
	

;-------------------------------------------------------------------------
; Descend sort the high score table taking into account the new hi-score,
; a1=Hiscore Table structure pointer
; d2=Max scores + 1
; d1=Round number | Rank number
; d0=New score
;=========================================================================
HISCORE_SORT:	
	lea	HISCORE_NEW(a4),a0
	move.l	d0,4(a0)
	move.w	d1,2(a0)
	swap	d1
	move.w	d1,14(a0)
	;or.l	#$20202000,d1			; default 3 spaces
	move.l	#$20202000,d1
	move.l	d1,8(a0)
.sort:	move.l	a1,a0
	move.l	d2,d0
	subq.w	#1,d0
	moveq	#0,d1
	
.loop:	move.l	20(a0),d3			; Get next score down...
	cmp.l	4(a0),d3			; Compare this score with next score
	beq.s	.noswap
	bcs.s	.noswap
	
.doswap:
	move.l	(a0),d1				; Swap around Rank
	move.l	16(a0),(a0)
	move.l	d1,16(a0)
	
	move.l	4(a0),d1			; Get this score
	move.l	20(a0),4(a0)			; Get next score down
	move.l	d1,20(a0)
	
	move.l	8(a0),d1
	move.l	24(a0),8(a0)
	move.l	d1,24(a0)
	
	move.l	12(a0),d1				; Swap around Rank
	move.l	28(a0),12(a0)
	move.l	d1,28(a0)
	
	moveq	#1,d1
	
.noswap:
	add.l	#16,a0
	dbf	d0,.loop
	tst.w	d1
	bne.s	.sort
	rts		
	
;----------------------------------------------
; Get the rank number for this high score
;a0=Hiscore table
;d0=score
;d2=max scores
;Out - if d2 is minus then no score found
;===============================================
GET_HISCORE_RANK:
	moveq	#0,d1
.loop:	cmp.l	4(a0),d0
	beq.s	.done
	add.w	#16,a0
	addq.w	#1,d1
	dbf	d2,.loop	
.done:
	rts

;----------------------------------------------------------------------




HISCORE_JOY_DETECT:
	clr.w	JOY_MENU_FIRE(a4)	; Set fire button not pressed
	bsr	JOY_DETECT
	cmp.w	#JOY1_NO_MOVE,d3		
	beq.s	.exit
	;move.w	#(50*10),HISCORE_DISPLAY_TIMER(a4)
	move.w	#1,MENU_DELAY(a4)
.exit:	rts

;---------------------------------------------
; Start the high score input initials loop
;=============================================

HISCORE_INPUT:
	lea	HIENTRY_SELECT_CHARS(a4),a0
	add.w	HISCORE_CHAR_POSITION(a4),a0

	lea	HIENTRY_INITIALS(a4),a1
	add.w	HISCORE_ENTRY_OFFSET(a4),a1

	move.b	(a0),(a1)

	btst	#7,CIAAPRA		; Left mouse pressed?
	beq.s	HISCORE_CHAR_ENTER
	clr.w	JOY_HISCORE_FIRE(a4)	; Set fire button not pressed

	bsr	JOY_DETECT
	cmp.w	#JOY1_NO_MOVE,d3		
	beq.s	HISCORE_JOY_NO_MOVE
	cmp.w	#JOY1_LEFT,d3
	beq.s	HISCORE_CHAR_LEFT
	cmp.w	#JOY1_RIGHT,d3
	beq.s	HISCORE_CHAR_RIGHT

.no_move:	
	rts

HISCORE_JOY_NO_MOVE:
	cmp.w	JOY_HISCORE_INPUT(a4),d3
	beq.s	.exit
	move.w	d3,JOY_HISCORE_INPUT(a4)
.exit:	rts

; ---------------------
; There is a stupid bug in here when the initials entry move
; that i need to fix.
;======================
HISCORE_CHAR_RIGHT:
	cmp.w	JOY_HISCORE_INPUT(a4),d3
	beq.s	.exit
	tst.w	HISCORE_CHAR_POSITION(a4)
	beq.s	.reset
	subq.w	#1,HISCORE_CHAR_POSITION(a4)
	move.w	d3,JOY_HISCORE_INPUT(a4)
	bra.s	.exit
.reset:	move.w	#28,HISCORE_CHAR_POSITION(a4)
.exit:	rts

HISCORE_CHAR_LEFT:
	cmp.w	JOY_HISCORE_INPUT(a4),d3
	beq.s	.exit
	cmp.w	#27,HISCORE_CHAR_POSITION(a4)
	bgt.s	.reset
	addq.w	#1,HISCORE_CHAR_POSITION(a4)
	move.w	d3,JOY_HISCORE_INPUT(a4)
	bra.s	.exit
.reset:	clr.w	HISCORE_CHAR_POSITION(a4)
.exit:	rts

HISCORE_CHAR_ENTER:
	tst.w	JOY_HISCORE_FIRE(a4)	; Is fire button still pressed?
	bmi.s	.exit				; Yes, reloop
	move.w	#-1,JOY_HISCORE_FIRE(a4)	; no, set fire button pressed

	lea	HIENTRY_SELECT_CHARS(a4),a0
	add.w	HISCORE_CHAR_POSITION(a4),a0

	lea	HIENTRY_INITIALS(a4),a1
	add.w	HISCORE_ENTRY_OFFSET(a4),a1	
	move.b	(a0),(a1)

	addq.w	#1,HISCORE_ENTRY_OFFSET(a4)	; add 1 to the initial entry

	cmp.w	#3,HISCORE_ENTRY_OFFSET(a4)
	beq.s	.save_hiscore
	bra.s	.exit
.save_hiscore:
	addq.w	#1,HISCORE_ENTRY_OFFSET(a4)

	lea	HISCORE_TABLE(a4),a0
	moveq	#0,d0
	move.w	HISCORE_RANK(a4),d0
	lsl.w	#4,d0				; Multiply by 8
	add.w	d0,a0
	addq.w	#8,a0

	lea	HIENTRY_INITIALS(a4),a1
	move.b	(a1)+,(a0)+			;Initial 1
	move.b	(a1)+,(a0)+			;Initial 2
	move.b	(a1)+,(a0)+			;Initial 3
	clr.b	(a0)
.exit:	rts


;-------------------------------------------
; Draw the high score entry initials
;===========================================
DRAW_HIENTRY_INITIALS:
	lea	HIENTRY_INITIALS(a4),a0
	moveq	#22,d0				; xpos 
	moveq	#4,d6
	moveq	#0,d1
	move.w	HISCORE_RANK(a4),d1
	move.w	d1,d4
	add.w	d1,d1
	add.w	d4,d1
	addq.w	#7,d1				; ypos
	moveq	#0,d2
	bsr	DRAW_HISCORE_TEXT
	rts


;---------------------------------------------------
; Draw the high score border around the initials
;===================================================
DRAW_HISCORE_ENTRY_BORDER:
	lea	HI_ENTRY_TOPLEFT(a4),a0
	moveq	#21,d0				; xpos 
	moveq	#HI_COLOUR_FLASH_RED,d6
	moveq	#0,d1
	move.w	HISCORE_RANK(a4),d1
	move.w	d1,d4
	add.w	d1,d1
	add.w	d4,d1
	addq.w	#6,d1				; ypos
	bsr	DRAW_HISCORE_ENTRY_BLOCK

	lea	HI_ENTRY_LEFT(a4),a0
	addq.w	#1,d1
	bsr	DRAW_HISCORE_ENTRY_BLOCK
	lea	HI_ENTRY_BOTLEFT(a4),a0
	addq.w	#1,d1
	bsr	DRAW_HISCORE_ENTRY_BLOCK	

	addq.w	#2,d0

	lea	HI_ENTRY_TOPVERT(a4),a0
	move.w	HISCORE_RANK(a4),d1
	move.w	d1,d4
	add.w	d1,d1
	add.w	d4,d1	
	addq	#6,d1				; ypos
	bsr	DRAW_HISCORE_ENTRY_BLOCK
	lea	HI_ENTRY_BOTVERT(a4),a0
	addq.w	#2,d1
	bsr	DRAW_HISCORE_ENTRY_BLOCK
	
	addq.w	#2,d0

	lea	HI_ENTRY_TOPRIGHT(a4),a0
	move.w	HISCORE_RANK(a4),d1
	move.w	d1,d4
	add.w	d1,d1
	add.w	d4,d1
	addq	#6,d1				; ypos
	bsr	DRAW_HISCORE_ENTRY_BLOCK

	lea	HI_ENTRY_RIGHT(a4),a0
	addq.w	#1,d1
	bsr	DRAW_HISCORE_ENTRY_BLOCK
	lea	HI_ENTRY_BOTRIGHT(a4),a0
	addq.w	#1,d1
	bsr	DRAW_HISCORE_ENTRY_BLOCK
	rts


;d0=xpos
;d1=ypos
;d6=colour
;a0=block pointer
DRAW_HISCORE_ENTRY_BLOCK:
	movem.l	d0-d7/a0-a5,-(a7)
	lsl.w	#3,d1
	mulu	#PLAYFIELD_SIZE_X*5,d1
	lsl.w	#2,d6
	lea	LSTPTR_BACK_SCREEN(a4),a1
	move.l	(a1,d6),a1		; Bitplane to draw in.

	cmp.w	#2,HISCORE_ENTRY_OFFSET(a4)
	ble.s	.active
	move.w	#HISCORE_ENTRY_DISABLE,HISCORE_ENTRY_OFFSET(a4)
	move.l	a1,a0			; Just point source at zero

.active:
	add.w	d1,a1
	add.w	d0,a1

	rept	8
	move.b	(a0)+,(a1)
	move.b	(a0)+,1(a1)
	add.w	#PLAYFIELD_SIZE_X*5,a1
	endr
	movem.l	(a7)+,d0-d7/a0-a5
	rts

	