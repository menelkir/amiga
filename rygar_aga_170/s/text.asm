FONT_WIDTH:     equ     (624/8)*1


; d0=8x8 x
; d1=8x8 y
; a0=text
; a1= Direct pointer to screen address
DRAW_TEXT:
        movem.l d0-d1/a0-a2,-(a7)
        lsl.w   #3,d1
        lea     TAB_Y(a4),a2
        add.l   d0,a1
        add.w   (a2,d1*2),a1

        moveq   #0,d0
.loop:
	move.l  FONT_ASSET(a4),a2

        add.w   #86,a2                  ; Offset to BODY
	
	tst.b   (a0)
        beq.s   .exit
        move.b  (a0)+,d0
        sub.b   #32,d0

.draw:  add.l   d0,a2           ; index into character

        move.b  (a2),(a1)
        move.b  FONT_WIDTH*1(a2),PLAYFIELD_SIZE_X*1(a1)
        move.b  FONT_WIDTH*2(a2),PLAYFIELD_SIZE_X*2(a1)
        move.b  FONT_WIDTH*3(a2),PLAYFIELD_SIZE_X*3(a1)
        move.b  FONT_WIDTH*4(a2),PLAYFIELD_SIZE_X*4(a1)
        move.b  FONT_WIDTH*5(a2),PLAYFIELD_SIZE_X*5(a1)
        move.b  FONT_WIDTH*6(a2),PLAYFIELD_SIZE_X*6(a1)
        addq.w  #1,a1
        bra     .loop
.exit:  movem.l (a7)+,d0-d1/a0-a2
        rts


;d0=word
;a0=text pointer
;d7=length 3=Word 7=LWORD
HEX_TO_ASCII:
        movem.l d0-d1,-(a7)
        clr.b   1(a0,d7)                        ; Safe terminate
.loop:  move.b  d0,d1
        and.b   #$f,d1
        cmp.b   #10,d1
        bge.s   .alpha
        add.b   #48,d1
        bra.s   .num
.alpha: add.b   #55,d1
.num:   move.b  d1,(a0,d7)
        lsr.l   #4,d0
        dbf     d7,.loop
        movem.l (a7)+,d0-d1
        rts

