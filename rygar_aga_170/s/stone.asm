
STONE_STRUCT_MAPPOS:        equ     4
STONE_STRUCT_SCRPOSX:       equ     8
STONE_STRUCT_SCRPOSY:       equ     10
STONE_STRUCT_FRAMES:        equ     12
STONE_STRUCT_STATE: equ     16
STONE_STRUCT_COLLISION:     equ     18
STONE_STRUCT_MEMOFFSET:     equ     20
STONE_STRUCT_MEMSAVEBG:     equ     24
STONE_STRUCT_ANIMPTR:       equ     28
STONE_STRUCT_ONSCREEN:      equ     32

MAX_STONES:     equ     16                      ; Max number of items that can be stored
STONE_SIZE_Y:   equ     32
STONE_SIZE_X:   equ     32/8
STONE_PLANES:   equ     5
STONE_MEMSIZE:  equ     (STONE_SIZE_X*STONE_PLANES)*STONE_SIZE_Y

STONE_ENTER_SCENE:      equ     1
STONE_STAYS_SCENE:      equ     0
STONE_LEAVE_SCENE:      equ     -1

STONE_ASSET:    equ     1250

STONE_TIMEOUT:  equ     300*50

; a2=Coordinate set (in cmp2 format)
; d2=x1, d3=y1, d4=x2, d5=y2

STONE_COLLCHECK MACRO
                moveq   #0,d1
.coll_loop:     tst.w   (a2)
                bmi.s   .coll_exit
                cmp.w   2(a2),d2
                bgt.s   .coll_next
                cmp.w   (a2),d4
                blt.s   .coll_next
                cmp.w   6(a2),d3
                bgt.s   .coll_next
                cmp.w   4(a2),d5
                blt.s   .coll_next
                moveq   #-1,d1
                bra.s   .coll_exit
.coll_next:     addq.w  #8,a2
                bra.s   .coll_loop
.coll_exit:
                ENDM

STONE_CHECK:
		FUNCID	#$bed10352
                move.l  FRAME_COUNTER(a4),d0
                lea     STONES_PUSH(a4),a0
                moveq   #0,d1

.loop1:         move.w  (a0),d1
                bmi.s   .pop
                addq.w  #4,a0
                cmp.l   d0,d1
                bne.s   .loop1

                move.w  -2(a0),d0                       ; d0 has asset
                move.l  #STONE_TIMEOUT,d3
                bsr     PUSH_STONE

.pop:
.exit:          rts


STONE_SEARCH_RIGHT_FROM:        equ     8


INIT_STONES:    
		FUNCID	#$49c47085
		moveq   #MAX_STONES-1,d7
                lea     STONE_STRUCT(a4),a0
.next1:         moveq   #STONE_STRUCT_SIZE-1,d6
                move.l  #"ST0N",(a0)+
                subq.w  #4,d6
.init1:         clr.b   (a0)+
                dbf     d6,.init1
                dbf     d7,.next1

                moveq   #MAX_STONES-1,d7
.next2:         lea     STONE_LIST(a4),a0
                move.w  #-1,(a0,d7*2)
                lea     STONES_BG_PTRS(a4),a0
                clr.l   (a0,d7*4)
                dbf     d7,.next2

                clr.w   STONE_CURRENT(a4)
                clr.w   STONE_PTR(a4)
                rts


;-----------------------
; *** STONES HANDLER ***
;=======================
HDL_STONES:     
		FUNCID	#$786159ee
		bsr     HDL_STONES_ATTRIBS                      ; Decrement the frames timer for each stone.

                moveq   #MAX_STONES-1,d7
                moveq   #0,d6                                   ; Allocate this slot with asset number

.loop:          lea     STONE_LIST(a4),a0
                tst.w   (a0,d6*2)
                bmi     .next_slot

                lea     STONE_VTABLE(a4),a0
                move.l  (a0,d6*4),a0                            ; Get structure pointer for the slot

                tst.w   STONE_STRUCT_STATE(a0)                      ; Tri-state
                beq.s   .next_slot                              ; DO nothing if 0
                bmi.s   .pop                                    ; Remove if -1
                bra.s   .push                                   ; Add if +1

                nop

; ITEM_STRUCT_STATE will be 0 if entering.
.push:          move.l  STONE_STRUCT_ANIMPTR(a0),a1
                bsr.s   STONE_ANIM
                tst.w   d2                                      ; Animation finished?
                bpl.s   .next_slot
                move.w  #STONE_STAYS_SCENE,STONE_STRUCT_STATE(a0)   ; Keep stone on screen
                bra.s   .next_slot

.pop:           move.l  STONE_STRUCT_ANIMPTR(a0),a1
                bsr     STONE_ANIM
                tst.w   d2                                              ; Animation finished?
                bpl.s   .next_slot

                lea     STONE_LIST(a4),a0
                move.w  #-1,(a0,d6*2)                           ; Remove the stone
                bsr     POP_STONE

;---------------------------------------------------------------
; Display the item in place of the stone.

                moveq   #0,d0
                move.l  ROUND_ITEMS(a4),a0
                move.w  (a0,d6*2),d0

                cmp.w   #ITEM_RANDOM,d0
                bne.s   .no_special
                bsr     GET_RANDOM_ITEM
                bra.s   .special

.no_special:
                lsr.w   #8,d0
                lsr.w   #2,d0
                subq.w  #1,d0

.special:       lea     ITEM_DISPLAY_LIST(a4),a0
                move.w  (a0,d0*2),d0
                lea     STONE_VTABLE(a4),a0
                move.l  (a0,d6*4),a0                            ; Get structure pointer
		moveq	#0,d1
		moveq	#0,d2
                move.w  STONE_STRUCT_SCRPOSX(a0),d1
                move.w  STONE_STRUCT_SCRPOSY(a0),d2
		add.w	#16,d2
                move.l  #ITEM_TIMEOUT,d3
                bsr     PUSH_ITEM
;---------------------------------------------------------------

.next_slot:     addq.w  #1,d6
                dbf     d7,.loop
.exit:          rts

; a1=pointer to animation frames
STONE_ANIM:     FUNCID	#$e8b401ec
		moveq   #0,d2                                   ; Default to only blit to front and back screen
                tst.w   2(a1)                                   ; is this the last frame?
                bpl.s   .not_last
                moveq   #-1,d2                                  ; Blit to back screen as well.

.not_last:      addq.l  #2,STONE_STRUCT_ANIMPTR(a0)
                tst.w   STONE_STRUCT_ONSCREEN(a0)           ; Is the stone on screen???
                bmi.s   .exit                                   ; No... skip the blit.
                move.w  (a1),d0
                move.l  STONE_STRUCT_MEMOFFSET(a0),d1
                move.w  STONE_STRUCT_STATE(a0),d4
                bsr     STONE_BLIT_ANIM                         ; Don't animate if off the screen though!
.exit           rts

GRI
GET_RANDOM_ITEM:
		FUNCID	#$11bdd278

; If time is less than 10 seconds then choose from random items 3
; There's more chance of extra time occuring here.

                lea     RANDOM_ITEMS3(a4),a0
		tst.w	TIME_LOW(a4)
		bmi.s	.assign

; If the time is not low... then choose from either random items 1 or 2
; depending on if the frame is odd or even

.time:		move.w  FRAME_BG(a4),d0
                and.w   #1,d0
                beq.s   .even_frame
                lea     RANDOM_ITEMS1(a4),a0
                bra.s   .assign
		
.even_frame:	lea     RANDOM_ITEMS2(a4),a0

; Get a random number between 0-7 for the display item
.assign:
                move.w  VPOSR(a5),d2
                swap    d2
                move.b  $bfe801,d2
                lsl.w   #8,d2
                move.b  $bfd800,d2
		
                move.w  RANDOM_SEED(a4),d0
                eor.w   d2,d0
                and.w   #$3,d0
                move.w  (a0,d0*2),d0
                lsr.w   #8,d0
                lsr.w   #2,d0
                subq.w  #1,d0

;                lea     HAVE_ITEMS(a4),a0
;.retry_loop:    tst.w   (a0,d0*2)
;                bmi.s   .retry
;                move.w  #-1,(a0,d0*2)
;                bra.s   .exit
;.retry:         addq.w  #1,d0
;                bra.s   .retry_loop

.exit:          rts



; Handler for managing the stones attributes.
HDL_STONES_ATTRIBS:
		FUNCID	#$89fb66b1
                moveq   #MAX_STONES-1,d7
                moveq   #0,d6                                   ; Allocate this slot with asset number
.loop:          lea     STONE_LIST(a4),a0
                tst.w   (a0,d6*2)
                bmi     .next_slot
                lea     STONE_VTABLE(a4),a0
                move.l  (a0,d6*4),a0                            ; Get structure pointer for the slot

; Mandatory object management, we must keep track of the stone if it is onscreen or not.
                move.w  SCROLL_DIRECTION_X(a4),d4
                sub.w   d4,STONE_STRUCT_SCRPOSX(a0)
                sub.w   d4,STONE_STRUCT_SCRPOSX(a0)
                subq.l  #1,STONE_STRUCT_FRAMES(a0)          ; Decrement the frames number

; First we should determine if the stone is onscreen, no point in further checks if off screen.
.screen_check:  clr.w   STONE_STRUCT_ONSCREEN(a0)
                move.w  STONE_STRUCT_SCRPOSX(a0),d4
                lea     STONE_SCREEN_LIMITS(a4),a1
		cmp.w   (a1),d4
		bgt.s   .leave_check
		cmp.w	2(a1),d4
		blt.s	.leave_check
                move.w  #-1,STONE_STRUCT_ONSCREEN(a0)

; Check here to see if the stone time expired (if collision then it will be 0)
.leave_check:   tst.l   STONE_STRUCT_FRAMES(a0)                                             ; Have the number of frames
                bpl.s   .diskarm_check                                                  ; completed???
                move.w  #STONE_LEAVE_SCENE,STONE_STRUCT_STATE(a0)                                   ; Set stone to leaving (-1) state.
                move.l  #1000,STONE_STRUCT_FRAMES(a0)

; Now we determine if the stone is leaving because it was destroyed or because it time expired.
                
		lea	STONES_DESTROY_ANIM(a4),a3
		move.l  a3,STONE_STRUCT_ANIMPTR(a0)                       ; Stone desroyed by default - likely.
                tst.w   STONE_STRUCT_COLLISION(a0)                                  ; Was it hit with disk armour?
                bmi.s   .is_collision     				; No
		lea	STONES_DOWN_ANIM(a4),a3
                move.l  a3,STONE_STRUCT_ANIMPTR(a0)

.is_collision:
                bra     .next_slot

; Probably want to check collision with disk armour here and destroy the sprite if collided.
.diskarm_check: tst.w   STONE_STRUCT_ONSCREEN(a0)           ; If stone is off screen then no point in
                bmi.s   .dont_check_collision                   ; checking collision with disk armour.
                tst.w   STONE_STRUCT_COLLISION(a0)          ; Is the collision flag already set?
                bmi.s   .next_slot                              ; Yes, don't check it again.

                lea     DISKARM_COORDS(a4),a2
                move.l  a2,a3
                move.w  STONE_STRUCT_SCRPOSX(a0),d2
                move.w  STONE_STRUCT_SCRPOSY(a0),d3
                ;add.w  #32,d2                                  ; HMMMMMMMMMMMMMMMM.... Compensate Y
                add.w   #32,d3
                move.w  d2,d4                                   ; Check a 32x32 boundary from x1/y1
                move.w  d3,d5
                add.w   #32,d4
                add.w   #32,d5

                STONE_COLLCHECK
                tst.w   d1
                beq.s   .next_slot

                moveq   #SND_STONE_HIT,d0
                bsr     PLAY_SAMPLE

                move.w  #-1,STONE_STRUCT_COLLISION(a0)              ; Set hit flag
                move.l  #-1,STONE_STRUCT_FRAMES(a0)         ; Cause an expiry check
.dont_check_collision
                bra.s   .next_slot

                nop

.next_slot:     addq.w  #1,d6
                dbf     d7,.loop
.exit:          rts


; This places a stone on a platform.
; d0=STONES Type to push (i.e. STONE_EMBLEM1 or STONE_STARPOWER)
; d1=Screen 16 X Position to appear (0=offscreen left, 20=offscreen right) - indexes to map position
; d2=Screen 16 Y Position to appear (0=top,14=bottom) - indexes to map position
; d3=Frame Time Limit
;
PUSH_STONE:
		FUNCID	#$7e80128f
                cmp.w   #MAX_STONES,STONE_PTR(a4)               ; Are all items full?
                beq     .exit

		moveq	#0,d6
		moveq	#0,d7
                move.w  d1,d6                                   ; Save screen x position
                move.w  d2,d7                                   ; Save screen y position

                moveq   #0,d4                                   ; Allocate this slot with asset number
                move.w  STONE_PTR(a4),d4                        ; It cannot already be in use (because we checked)
                lea     STONE_LIST(a4),a0
                move.w  d5,(a0,d4*2)                            ; Set slot to be busy
                lea     STONE_VTABLE(a4),a0
                move.l  (a0,d4*4),a0                            ; Get structure pointer for the slot
                move.l  d3,STONE_STRUCT_FRAMES(a0)          ; Store number of frames

                move.l  ROUND_TILEMAP(a4),a1            ; Get tilemap pointer
                move.w  MAP_PIXEL_POSX(a4),d4
                lsr.w   #4,d4
                add.l   d4,a1
                add.l   d4,a1

                sub.w   #34,a1                                  ; Compensate for temple at start of map

; Get the stone background position
                lea     STONE_POSITIONS(a4),a2          ; Get stone background number
                add.l   d1,a2
                add.l   d4,a2

                moveq   #0,d4
                move.b  -17(a2),d4                              ; Check the Static or Jump Stone
                ;tst.b  d4
                bpl.s   .found_stone

                move.b  -7(a2),d4                               ; Check the Walk past stone
                ;tst.b  d4
                bpl.s   .found_stone
		
		
.crashed:
                ;COL0_GREEN
                COL0_BLUE
                ;COL0_RED
		
		bra	.exit
		
		btst.b	#7,$BFE001
		beq	.exit
		
		bra.s   .crashed
                nop
                nop

.found_stone:
; Save the background pointer for this stone.
                lea     STONES_BG_PTRS(a4),a2
                move.l  (a2,d4*4),STONE_STRUCT_MEMSAVEBG(a0)

                lea     MAP_TABLE_Y1(a4),a2             	; Get the map position to place the item
		moveq	#0,d4
		move.w  MAP_POSY(a4),d4
                add.l   (a2,d4*4),a1                            ; Add current map tile Y position
                add.l   d1,a1                                   ; Add onscreen tile X position
                add.l   d1,a1                                   ;
                add.l   (a2,d2*4),a1                            ; Add onscreen tile Y position

                move.l  a1,STONE_STRUCT_MAPPOS(a0)

                or.w    #STONE_BLOCK1,(a1)                      ; Place the stone in the map (CMD is in same place).
                or.w    #STONE_BLOCK2,2(a1)                     ; as attributes.
                ;or.w    #STONE_BLOCK3,MAP_WIDTH*2(a1)
                ;or.w    #STONE_BLOCK4,(MAP_WIDTH*2)+2(a1)

; a0 has the base pointer to the structure for this stone.
; a1 has the map pointer address

; Store the x/y coordinates of the stone and store the memory address in screen ram to plot.
                moveq   #0,d1
                move.w  MAP_PIXEL_POSX(a4),d1
                move.w  d1,d4
                lsr.w   #4,d1
                add.w   d1,d1
                and.w   #%0000000000001111,d4
                add.w   d6,d1                                   ; Add x offset bytes
                add.w   d6,d1
                lea     CANVAS_TABLE_Y2(a4),a1
                add.l   (a1,d7*4),d1                            ; Get screen offset
                lsl.w   #4,d6                                   ; (Multiply by 16 for pixel position)
                lsl.w   #4,d7
                sub.w   d4,d6

                subq.w  #4,d1

                move.w  d6,STONE_STRUCT_SCRPOSX(a0)         ; Store x position of item
                move.w  d7,STONE_STRUCT_SCRPOSY(a0)         ; Store y position of item
                move.l  d1,STONE_STRUCT_MEMOFFSET(a0)

                clr.w   STONE_STRUCT_COLLISION(a0)          ; clear collision flag
                move.w  #STONE_ENTER_SCENE,STONE_STRUCT_STATE(a0)
                ;;;move.l  #STONES_UP_ANIM,STONE_STRUCT_ANIMPTR(a0)    ; save animation pointer.
		lea	STONES_UP_ANIM(a4),a3
		move.l	a3,STONE_STRUCT_ANIMPTR(a0)
	      
                addq.w  #1,STONE_PTR(a4)
.exit:          rts


; d6=item number to pop
POP_STONE:      
		FUNCID	#$72449746
		movem.l a0-a1,-(a7)
                lea     STONE_VTABLE(a4),a0
                move.l  (a0,d6*4),a0                            ; Get structure pointer
                move.l  STONE_STRUCT_MAPPOS(a0),a1
                cmp.l   #0,a1
                beq.s   .exit
                and.l   #%00000011111111110000001111111111,(a1)         ; Clear tiles
                and.l   #%00000011111111110000001111111111,MAP_WIDTH*2(a1)              ; Clear tiles
.exit:  movem.l (a7)+,a0-a1
                rts




; Routine that checks if a stone is pushed up when Rygar jumps
CHECK_JUMP_STONE:
		FUNCID	#$4f34b39d
                movem.l d0-d7/a0-a3,-(a7)
                lea     MAP_TABLE_Y1(a4),a0
                move.l  ROUND_TILEMAP(a4),a1
                move.w  MAP_POSY(a4),d0
                add.l   (a0,d0*4),a1

                moveq   #0,d5
                move.w  RYGAR_YPOS(a4),d5               ; Get Y position of Sprite
                ;add.w  #32,d5                          ; bottom of the sprite.
                lsr.w   #4,d5                           ; Divide by 16
                add.l   (a0,d5*4),a1                    ; Correct row.
                move.w  d5,d2

; Now get the tile where Rygar is on X position
                move.w  RYGAR_XPOS(a4),d5               ; Get X Position
                lsr.w   #4,d5
                add.w   d5,d5
                add.l   d5,a1
                ;subq.w #2,a1                           ; Off by 1 ERROR!!!!

                move.w  MAP_PIXEL_POSX(a4),d5
                sub.w   COLLISION_COMPENSATE(a4),d5
                bmi.s   .rygar_left_edge
                lsr.w   #4,d5
                add.w   d5,d5
                add.l   d5,a1

                ;move.w #17,(a1)

.rygar_left_edge:
                move.w  (a1),d5
                and.w   #%0111110000000000,d5
                cmp.w   #TILECMD_PUSHSTONE_JUMP,d5
                bne.s   .no_stone

                and.w   #%0000001111111111,(a1)         ; Clear tile command once hit.
                move.l  a1,-(a7)
                move.l  #STONE_ASSET,d0                 ; Put the stone asset on screen
                moveq   #9,d1                           ; Just under Rygar!
                move.w  RYGAR_LAST_JUMP_POS(a4),d2
                lsr.w   #4,d2

                subq.w  #2,d2                           ; Compensate

                move.l  #STONE_TIMEOUT,d3
                bsr     PUSH_STONE
                move.l  (a7)+,a1

                ;addq.w #1,STONE_CURRENT(a4)


.no_stone:      movem.l (a7)+,d0-d7/a0-a3
                rts



; Blit stone animations when required
;
; d0.w = sprite to blit
; d1.l = screen offset to place stone
; d2.l = -1 (blit to post screen), 0 (only back and front screen)
; d4.w = -1 Save bg used as source background, 0 Post screen used as source background
STONE_BLIT_ANIM:
	FUNCID	#$f506604d
        move.l  d6,-(a7)
        move.l  d7,-(a7)

        ;lea    SPRITE_POINTERS(a4),a1
        move.l  MEMCHK0_SPRITE_POINTERS(a4),a1
        move.l  (a1,d0*8),a2            ; Get sprite address
        move.l  4(a1,d0*8),a3           ; Get mask address

; Check to see if we should do the Post screen too.
        tst.w   d2
        bmi.s   .skip_post

        move.l  LSTPTR_POST_SCREEN(a4),d7
        add.w   d1,d7
        move.w  #PLAYFIELD_SIZE_X-4,d6
        tst.w   d4
        beq.s   .copy_post
        ;moveq  #PLAYFIELD_SIZE_X-4,d6
        moveq   #(MAX_STONES-1)*4,d6
        move.l  STONE_STRUCT_MEMSAVEBG(a0),d7
.copy_post:
        move.l  LSTPTR_POST_SCREEN(a4),a1                       ; Screen location Back Buffer
        add.l   d1,a1
        WAIT_FOR_BLITTER
        move.l  #$0fca0000,BLTCON0(a5)          ;
        move.l  #$ffffffff,BLTAFWM(a5)          ;
        move.w  #SPRITE_ASSETS_SIZE_X-4,BLTAMOD(a5)
        move.w  #SPRITE_ASSETS_SIZE_X-4,BLTBMOD(a5)
        move.w  d6,BLTCMOD(a5)                  ; This changes when it is a POP!  Needs to be modulo of save buffer (0)
        move.w  #PLAYFIELD_SIZE_X-4,BLTDMOD(a5)
        move.l  a3,BLTAPTH(a5)                  ; Load the mask address
        move.l  a2,BLTBPTH(a5)                  ; Sprite address
        move.l  d7,BLTCPTH(a5)                  ; This changes when it is a POP!  Needs to be address of save buffer!
        move.l  a1,BLTDPTH(a5)
        move.w  #(160*64)+2,BLTSIZE(a5)         ; Default to 16x16,BLTSIZE(a5)

.skip_post:
; Do front screen
        move.l  LSTPTR_CURRENT_SCREEN(a4),d7
        add.w   d1,d7
        move.w  #PLAYFIELD_SIZE_X-4,d6
        tst.w   d4
        beq.s   .copy_front
        ;moveq  #PLAYFIELD_SIZE_X-4,d6
        moveq   #(MAX_STONES-1)*4,d6
        move.l  STONE_STRUCT_MEMSAVEBG(a0),d7
.copy_front:
        move.l  LSTPTR_CURRENT_SCREEN(a4),a1                    ; Screen location Back Buffer
        add.l   d1,a1
        WAIT_FOR_BLITTER
        move.l  #$0fca0000,BLTCON0(a5)          ;               ;
        move.l  #$ffffffff,BLTAFWM(a5)          ;
        move.w  #SPRITE_ASSETS_SIZE_X-4,BLTAMOD(a5)
        move.w  #SPRITE_ASSETS_SIZE_X-4,BLTBMOD(a5)
        move.w  d6,BLTCMOD(a5)                  ; This changes when it is a POP!  Needs to be modulo of save buffer (0)
        move.w  #PLAYFIELD_SIZE_X-4,BLTDMOD(a5)
        move.l  a3,BLTAPTH(a5)                  ; Load the mask address
        move.l  a2,BLTBPTH(a5)                  ; Sprite address
        move.l  d7,BLTCPTH(a5)                  ; This changes when it is a POP!  Needs to be address of save buffer!
        move.l  a1,BLTDPTH(a5)
        move.w  #(160*64)+2,BLTSIZE(a5)         ; Default to 16x16,BLTSIZE(a5)


.exit:  move.l  (a7)+,d7
        move.l  (a7)+,d6
        rts


