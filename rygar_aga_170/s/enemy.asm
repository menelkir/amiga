ENEMY_DEF_DRONE:        equ     $1                      ; 0 Ground enemy
ENEMY_DEF_APE:          equ     $2                      ; 1 Ground enemy - static
ENEMY_DEF_GRIFFIN:      equ     $4                      ; 2 Air enemy
ENEMY_DEF_DRAGON:	equ     $8                      ; 3 Air enemy
ENEMY_DEF_SNAKE:      	equ     $10                     ; 4 Ground enemy
ENEMY_DEF_CAVEBAT:      equ     $20                     ; 5 Air enemy
ENEMY_DEF_RHINO:        equ     $40                     ; 6 Ground enemy
ENEMY_DEF_BIGRHINO:     equ     $80                     ; 7 Ground enemy
ENEMY_DEF_RIDER:        equ     $100                    ; 8 Ground enemy
ENEMY_DEF_TRIBESMAN:    equ     $200                    ; 9 Ground enemy
ENEMY_DEF_GIANTDEMON:   equ     $400                    ; 10 Ground enemy
ENEMY_DEF_GIANTWORM:    equ     $800                    ; 11 Ground enemy
ENEMY_DEF_LAVAMAN:      equ     $1000                   ; 12 Ground enemy
ENEMY_DEF_BOSS:		equ     $2000                   ; 13 Ground enemy
ENEMY_DEF_ROCKET:	equ     $4000                   ; 14 Air enemy
ENEMY_DEF_MONKEY:	equ	$8000			; 15 Ground enemy
ENEMY_DEF_CRAB:		equ	$10000			; 16 Ground enemy
ENEMY_DEF_ROCK:		equ	$20000      		; 17 Air enemy
ENEMY_DEF_MUTANTFROG:	equ	$40000			; 18 Ground enemy
ENEMY_DEF_VILLAGER:	equ	$80000			; 19 Ground enemy
ENEMY_DEF_RHINORIDER:	equ	$100000			; 20 Ground enemy
ENEMY_DEF_MUTANTTENT:	equ	$200000			; 21 Mutant enemy
ENEMY_DEF_TENTACLES:	equ	$400000			; 22 Mutant enemy

GROUND_ENEMIES: 	equ     (ENEMY_DEF_VILLAGER!ENEMY_DEF_MUTANTFROG!ENEMY_DEF_DRONE!ENEMY_DEF_APE!ENEMY_DEF_RHINO!ENEMY_DEF_RIDER!ENEMY_DEF_TRIBESMAN!ENEMY_DEF_GIANTDEMON!ENEMY_DEF_GIANTWORM!ENEMY_DEF_LAVAMAN!ENEMY_DEF_MONKEY!ENEMY_DEF_CRAB)

ENEMY_STATE_0:          equ     $00000000               ; Enemy Config
ENEMY_STATE_1:          equ     $10000000               ; Enemy Config
ENEMY_STATE_2:          equ     $20000000               ; Enemy Config
ENEMY_STATE_3:          equ     $30000000               ; Enemy Config
ENEMY_STATE_4:          equ     $40000000               ; Enemy Config
ENEMY_STATE_5:          equ     $50000000               ; Enemy Config
ENEMY_STATE_6:          equ     $60000000               ; Enemy Config
ENEMY_STATE_7:          equ     $70000000               ; Enemy Config
ENEMY_STATE_8:          equ     $80000000               ; Enemy Config
ENEMY_STATE_9:          equ     $90000000               ; Enemy Config
ENEMY_STATE_10:         equ     $a0000000               ; Enemy Config
ENEMY_STATE_11:         equ     $b0000000               ; Enemy Config
ENEMY_STATE_12:         equ     $c0000000               ; Enemy Config
ENEMY_STATE_13:         equ     $d0000000               ; Enemy Config
ENEMY_STATE_14:         equ     $e0000000               ; Enemy Config
ENEMY_STATE_15:         equ     $f0000000               ; Enemy Config

ENEMY_RESPAWN_FRAMES16:		equ	%1111
ENEMY_RESPAWN_FRAMES32:		equ	%11111
ENEMY_RESPAWN_FRAMES64:		equ	%111111
ENEMY_RESPAWN_FRAMES128:	equ	%1111111

ENEMY_STATUS_DESTROYED:         equ     0
ENEMY_STATUS_BONES:             equ     1
ENEMY_STATUS_SWEEP_LEFT:        equ     2
ENEMY_STATUS_SWEEP_RIGHT:       equ     3
ENEMY_STATUS_STUN_LEFT:         equ     4
ENEMY_STATUS_STUN_RIGHT:        equ     5

ENEMY_BOUNDARY_TOLERANCE:       equ     $300            ; Amount of pixels * 8 before the enemy is destroyed.
ENEMY_BOUNDARY_TIME:            equ     64              ; number of frames outside of boundary before enemy is destroyed.



; Check boundary of sprite!
ENEMY_BOUNDARY_CHECK MACRO
                moveq   #0,d2
                move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                lea     BOUNDARY_X1POS(a4),a0
		cmp.l	(a0),d2
		blt.s	.dectimer
		cmp.l	4(a0),d2
		bgt.s	.dectimer
		bra.s	.in_bounds
			
.dectimer:
                subq.w  #1,THIS_SPRITE_BOUNDARY_TIMER(a2)
                bpl.s   .out_bounds
                move.w  #ENEMY_STATUS_BONES,THIS_SPRITE_STATUS(a2)
		lea	ATTACH_TABLE(a4),a3
		move.b	#-1,(a3,d0)
.in_bounds:	move.w  #ENEMY_BOUNDARY_TIME,THIS_SPRITE_BOUNDARY_TIMER(a2)
.out_bounds:      
		ENDM

;d2 = Sprite Type
;a3 = Pointer to current config base.
GET_ENEMY_STATE_IN_MAP  MACRO
                        lea     CURRENT_ENEMY_STATE(a4),a3
                        lsl.w   #3,d2				; Should be #3
                        add.l   d2,a3                           ; a1 now points to config type
                        ENDM

; Animate Bones when enemy is killed.
METHOD_ANIMATE_BONES    MACRO
.method_animate_bones:  moveq   #ENEMY_STATUS_BONES,d1
                movem.l d0-1/a1-a2,-(a7)
                bsr     ANIMATE
                movem.l (a7)+,d0-1/a1-a2
                tst.w   d6
                bmi     .method_destroy_sprite
                bra     .exit
                ENDM

METHOD_ANIMATE_SWEEP_LEFT       MACRO
.method_animate_sweep_left:
                cmp.w   #32,SPRITE_PLOT_XPOS(a1)
                ble     .method_set_bones
                lea     ENEMY_DEATH_SINE(a4),a3
                move.w  THIS_SPRITE_SWEEP_INDEX(a2),d3
                move.w  (a3,d3*2),d3
                tst.w   d3
                bmi     .method_set_bones
                addq.w  #1,THIS_SPRITE_SWEEP_INDEX(a2)
                sub.w   d3,SPRITE_PLOT_XPOS(a1)
                moveq   #ENEMY_STATUS_SWEEP_LEFT,d1
                bra     ANIMATE
                ENDM


METHOD_ANIMATE_SWEEP_RIGHT      MACRO
.method_animate_sweep_right:
                cmp.w   #248,SPRITE_PLOT_XPOS(a1)
                bge     .method_set_bones
                lea     ENEMY_DEATH_SINE(a4),a3
                move.w  THIS_SPRITE_SWEEP_INDEX(a2),d3
                move.w  (a3,d3*2),d3
                tst.w   d3
                bmi     .method_set_bones
                addq.w  #1,THIS_SPRITE_SWEEP_INDEX(a2)
                add.w   d3,SPRITE_PLOT_XPOS(a1)
                moveq   #ENEMY_STATUS_SWEEP_RIGHT,d1
                bra     ANIMATE
                ENDM

METHOD_SET_BONES        MACRO
.method_set_bones:
                move.w  #ENEMY_STATUS_BONES,THIS_SPRITE_STATUS(a2)
		moveq	#ENEMY_STATUS_BONES,d1
		bra	ANIMATE
                ;bra     .set_update
                ENDM

METHOD_SET_SWEEP_DIRECTION      MACRO
.method_set_sweep_direction:
                add.l   d2,VAL_PLAYER1_SCORE(a4)
                move.w  #-1,UPDATE_SCORE(a4)
		
		move.w	#HITPOINT_DELAY,HITPOINT_FRAMES(a4)

		tst.w	SFX_DISABLE_DESTROY_ENEMY(a4)
		bmi.s	.no_sfx_at_sanctuary
		
                addq.w  #1,REPULSE_BONUS_NUM(a4)                    ; Add one to enemies killed.
                
		move.l  d0,-(a7)
                moveq   #SND_LAND_ON_ENEMY,d0
                bsr     PLAY_SAMPLE
                move.l  (a7)+,d0
		
.no_sfx_at_sanctuary:
		move.w	RYGAR_CURRENT_POWERS(a4),d2
		and.w	#POWER_CROWN,d2
		bne.s	.has_crown_power
		
		tst.w	STATE_KILL_ALL_ENEMIES(a4)
		bmi.s	.has_crown_power
		move.w  #-1,DISKARM_DISABLE(a4)
		
.has_crown_power:
                move.w  RYGAR_XPOS(a4),d2                                       ; d2 = 114
                add.w   #16,d2                                          ; centre of Rygar
                cmp.w   SPRITE_PLOT_XPOS(a1),d2                         ; xpos = 80
                ble.s   .method_set_sweep_right
                bra.s   .method_set_sweep_left

.method_set_sweep_right:
                move.w  #ENEMY_STATUS_SWEEP_RIGHT,THIS_SPRITE_STATUS(a2)
		moveq	#ENEMY_STATUS_SWEEP_RIGHT,d1
                bra     ANIMATE

.method_set_sweep_left:
                move.w  #ENEMY_STATUS_SWEEP_LEFT,THIS_SPRITE_STATUS(a2)
		moveq	#ENEMY_STATUS_SWEEP_LEFT,d1
                bra     ANIMATE
                ENDM


ENEMY_SHAKE     MACRO
                cmp.w   #TIGER_STUN_REVIVE_TIME,THIS_SPRITE_STUN_TIMER(a2)      ; if Stun revive time is greater
                bgt     ANIMATE                                                 ; then remaining time, then do nothing.
                lea     ENEMY_SHAKE_OFFSETS(a4),a3
                move.w  THIS_SPRITE_STUN_TIMER(a2),d4
                move.w  (a3,d4*2),d4
                add.w   d4,SPRITE_PLOT_XPOS(a1)
                ENDM


ENEMY_CHECK_END_OF_ROUND        MACRO
		moveq	#ZERO_POINTS,d2
                tst.w   SPRITE_DESTROY_END_ROUND(a2)            ; Is the sprite set destroyed at the end of the round?
                beq.s   .sprite_0                               ; No, so carry on
                clr.w   SPRITE_DESTROY_END_ROUND(a2)            ; Yes, so set the sweep direction to destroy the enemy.	
		bra     .method_set_sweep_direction
                ENDM

RUN_SPRITE_METHOD       MACRO
.run_sprite_method:     lea     .animate_table(pc),a0                   ; Call the method based on value of THIS_SPRITE_STATUS(a2)
                moveq	#0,d2
		move.w  THIS_SPRITE_STATUS(a2),d2
		bmi	.crash
		cmp.w	#MAX_ANIMATIONS_PER_SPRITE,d2
		bgt.s	.crash
                add.l   (a0,d2*4),a0
                jmp     (a0)

.crash:		move.w	#$070,$dff180
		;bsr	FIREWAITSR
		bra	.exit
                ENDM



METHOD_SET_JUMP_OR_DESTROY      MACRO
.method_set_jump_or_destroy:
                move.l  d0,-(a7)
                moveq   #SND_LAND_ON_ENEMY,d0
                bsr     PLAY_SAMPLE
                move.l  (a7)+,d0

; Reset Roll Fall frames
		movem.l	d0-d1,-(a7)
		moveq	#SPR_RYGAR,d0
		moveq	#RYGAR_ROLLFALL_LEFT,d1
		bsr	RESET_SPRITE_FRAMES
		moveq	#SPR_RYGAR,d0
		moveq	#RYGAR_ROLLFALL_RIGHT,d1
		bsr	RESET_SPRITE_FRAMES
		movem.l	(a7)+,d0-d1
		
		move.w	#-1,RYGAR_ROLLFALL(a4)
		
		move.w	#HITPOINT_DELAY,HITPOINT_FRAMES(a4)

                clr.w   RYGAR_FALL_STATE(a4)                            ; Stop Falling
                move.w  #-1,RYGAR_JUMP_STATE(a4)                            ; Start Jumping
                clr.w   RYGAR_SINE_INDEX(a4)                            ; Reset Jump Sine pointer.
                GET_PLAYER_SPRITE_CONTEXT
                move.w  #SPRITE_INSTATE_JUMPING,THIS_SPRITE_STATUS(a0)  ; Set to jumping.
; Check here if Rygar has Tiger power.

                move.w  RYGAR_CURRENT_POWERS(a4),d2
                and.w   #POWER_TIGER,d2
                bne     .method_set_sweep_direction                     ; If Rygar has Tiger then enemy is destroy
                bra     .exit                     			; If not then the enemy is stunned.
                ENDM

; If it's a ground enemy then it will be stunned, if an air enemy then Rygar will Jump.
METHOD_SET_STUN_OR_DESTROY      MACRO
.method_set_stun_or_destroy:
                move.l  d0,-(a7)
                moveq   #SND_LAND_ON_ENEMY,d0
                bsr     PLAY_SAMPLE
                move.l  (a7)+,d0
		
; Reset Roll Fall frames
		movem.l	d0-d1,-(a7)
		moveq	#SPR_RYGAR,d0
		moveq	#RYGAR_ROLLFALL_LEFT,d1
		bsr	RESET_SPRITE_FRAMES
		moveq	#SPR_RYGAR,d0
		moveq	#RYGAR_ROLLFALL_RIGHT,d1
		bsr	RESET_SPRITE_FRAMES
		movem.l	(a7)+,d0-d1
		
		move.w	#-1,RYGAR_ROLLFALL(a4)
		
		move.w	#HITPOINT_DELAY,HITPOINT_FRAMES(a4)

                tst.w   THIS_SPRITE_STUN_TIMER(a2)                      ; If enemy already in timer
                bpl     .exit
		
                clr.w   RYGAR_FALL_STATE(a4)                            ; Stop Falling
                move.w  #-1,RYGAR_JUMP_STATE(a4)                            ; Start Jumping
                clr.w   RYGAR_SINE_INDEX(a4)                            ; Reset Jump Sine pointer.
                GET_PLAYER_SPRITE_CONTEXT
                move.w  #SPRITE_INSTATE_JUMPING,THIS_SPRITE_STATUS(a0)  ; Set to jumping.
; Check here if Rygar has Tiger power.

                move.w  RYGAR_CURRENT_POWERS(a4),d2
                and.w   #POWER_TIGER,d2
                bne     .method_set_sweep_direction                     ; If Rygar has Tiger then enemy is destroy
                bra     .method_set_stun_direction                      ; If not then the enemy is stunned.
                ENDM


METHOD_SET_STUN_DIRECTION       MACRO
.method_set_stun_direction:
                move.w  #TIGER_STUN_STATIC_TIME,THIS_SPRITE_STUN_TIMER(a2)              ; Set timer
		move.w	ROUND_CURRENT(a4),d2
		lsl.w	#2,d2
		sub.w	d2,THIS_SPRITE_STUN_TIMER(a2)
                tst.w   THIS_SPRITE_DIRECTION(a2)
                bmi.s   .method_set_stun_left
                bra.s   .method_set_stun_right

.method_set_stun_left:
                move.w  #ENEMY_STATUS_STUN_LEFT,THIS_SPRITE_STATUS(a2)
		moveq	#ENEMY_STATUS_STUN_LEFT,d1
		bra	ANIMATE
                ;bra     .set_update

.method_set_stun_right:
                move.w  #ENEMY_STATUS_STUN_RIGHT,THIS_SPRITE_STATUS(a2)
		moveq	#ENEMY_STATUS_STUN_RIGHT,d1
		bra	ANIMATE
                ;bra     .set_update
                ENDM


METHOD_SET_RYGAR_DEATH_SEQUENCE MACRO
.method_set_rygar_death_sequence:
		tst.w	SHIELD_TIMER(a4)
		bpl	.method_set_sweep_direction

		tst.w	RYGAR_INVINCIBLE(a4)
		bmi	.exit
		
                tst.w   RYGAR_DEAD_STATE(a4)
                bmi     .exit                           ; Rygar already dead!
                move.w  #-1,RYGAR_DEAD_STATE(a4)
                clr.w   RYGAR_SINE_INDEX(a4)
                GET_PLAYER_SPRITE_CONTEXT
                move.w  #RYGAR_DEATH,THIS_SPRITE_STATUS(a0)
                bsr     STOP_TUNE
                moveq   #SND_RYGARDEAD,d0               ; Play jump sample
                bsr     PLAY_SAMPLE
                bra     .exit
                ENDM



ENEMY_TO_DISKARM_COLLISION      MACRO
                moveq   #0,d6
.eny_coll_loop: tst.w   (a3)
                bmi.s   .eny_coll_exit
                cmp.w   2(a3),d2
                bgt.s   .eny_coll_next
                cmp.w   (a3),d4
                blt.s   .eny_coll_next
                cmp.w   6(a3),d3
                bgt.s   .eny_coll_next
                cmp.w   4(a3),d5
                blt.s   .eny_coll_next
                moveq   #-1,d6
		move.w	d2,HITPOINT_XPOS(a4)
		move.w	d3,HITPOINT_YPOS(a4)
                bra.s   .eny_coll_exit
.eny_coll_next: addq.w  #8,a3
                bra.s   .eny_coll_loop
.eny_coll_exit:
                ENDM


ENEMY_TO_RYGAR_COLLISION        MACRO
                moveq   #0,d6
.ryg_coll_loop: cmp.w   2(a3),d2                                ; Check X1 against Rygar X2
                bgt.s   .ryg_coll_next
                cmp.w   (a3),d4                                 ; Check X2 against Rygar X1
                blt.s   .ryg_coll_next
                cmp.w   6(a3),d3                                ; Check Y1 against Rygar Y2
                bgt.s   .ryg_coll_next
                cmp.w   4(a3),d5                                ; Check Y2 against Rygar Y1
                blt.s   .ryg_coll_next
                moveq   #-1,d6
		move.w	d2,HITPOINT_XPOS(a4)
		move.w	d3,HITPOINT_YPOS(a4)
.ryg_coll_next: nop
.ryg_coll_exit:
                ENDM


;d0 = Enemy Config number
LEM
LOAD_ENEMY_STATE_FROM_MAP:
		FUNCID	#$167fe249
                cmp.w   LAST_ENEMY_STATE(a4),d0
                beq.s   .rts
                move.w  d0,LAST_ENEMY_STATE(a4)
		
		movem.l a0-a3,-(a7)

		IFNE	DEBUG_ROUND
		move.l	d0,-(a7)
		lea	DEBUG_ROUND_POINTERS(a4),a0
		moveq	#0,d0
		move.w	ROUND_CURRENT(a4),d0
		move.l	(a0,d0*4),MEMCHK3_ROUNDCONFIG(a4)
		move.l	(a7)+,d0
		ENDC
		
		move.l	MEMCHK3_ROUNDCONFIG(a4),a0
		move.l	a0,a3
		
                move.l  ROUND_ENEMY_STATE(a4),a0
                move.l  (a0,d0*4),d0            

		lea	(a3,d0.l),a0			; a0 now points to ROUNDX_ENEMY_STATE_N
                lea     CURRENT_ENEMY_STATE(a4),a1
		
; Need to improve this methinks.	
.loop:          move.l  a0,a2
		move.l	a1,a3
		moveq	#0,d0
                move.w  (a0),d0
                bmi.s   .exit
                lsl.w   #3,d0
                add.l   d0,a3                           ; a1 now points to config type
                move.l  (a2)+,(a3)+                     ; copy config for this sprite.
                move.l  (a2)+,(a3)+
                addq.w   #8,a0
                bra.s   .loop
.exit:          movem.l (a7)+,a0-a3
.rts:           rts




SPAWN_ENEMIES:
	FUNCID	#$95e21444
        tst.w   DISABLE_ENEMIES(a4)
        bmi     .exit

        moveq   #0,d3

; Check to see if DRAGON1 can spawn on the right.
.loop:  move.w  #288,d1                 ; xposition                     ; Add Map Pixel
        add.w   d3,d1
        add.w   MAP_PIXEL_POSX(a4),d1
        sub.w   #256,d1
        moveq   #0,d4
        move.w  d1,d4
        lsr.w   #4,d4                   ; divide by 16 for tiles

	lea	ROUND_PLATFORMS(a4),a3
	;move.l	(a3,d4*4),RIGHT_PLATFORM_BLOCK(a4)

        lea     ROUND_ENEMY_POSITIONS(a4),a3
        move.l  (a3,d4*4),SPAWN_RIGHT_BLOCK(a4)
	

        move.w  MAP_PIXEL_POSX(a4),d0
        and.w   #$f,d0
        bne.s   .do_not_load_config
        move.l  SPAWN_RIGHT_BLOCK(a4),d0
        swap    d0
        lsr.w   #8,d0
        lsr.w   #4,d0
        and.l   #$f,d0
        bsr     LOAD_ENEMY_STATE_FROM_MAP

.do_not_load_config:

; Reserved Queuing system in here..
; If the queue has sprites in there then they will be priorised for the enemy type.


	
        IFNE    ENABLE_RHINO
                moveq   #SPR_RHINO,d2
                GET_ENEMY_STATE_IN_MAP
                moveq   #0,d7
                move.w  2(a3),d7
		move.w	4(a3),d1
                move.l  #ENEMY_DEF_RHINO,d2
		move.l	#(146<<16!0),d5
		moveq	#SPR_TYPE_ENEMY_2X16,d6
		or.w	6(a3),d6
		swap	d6
		move.w	#SPR_RHINO,d6
                lea     HDL_RHINO(pc),a0
                lea     RHINO_TOGGLE(a4),a1
                lea     RHINOS_IN_USE(a4),a2
                bsr     SPAWN_ENEMY

        ENDC

        IFNE    ENABLE_DRONE
                moveq   #SPR_DRONE,d2
                GET_ENEMY_STATE_IN_MAP
                moveq   #0,d7
                move.w  2(a3),d7
		move.w	4(a3),d1
                move.l  #ENEMY_DEF_DRONE,d2
		move.l	#(146<<16!0),d5
		moveq	#SPR_TYPE_ENEMY_2X32,d6
		or.w	6(a3),d6
		swap	d6
		move.w	#SPR_DRONE,d6
                lea     HDL_DRONE(pc),a0
                lea     DRONE_TOGGLE(a4),a1
                lea     DRONES_IN_USE(a4),a2
                bsr     SPAWN_ENEMY
        ENDC

        IFNE    ENABLE_MONKEY
                moveq   #SPR_MONKEY,d2
                GET_ENEMY_STATE_IN_MAP
                moveq   #0,d7
                move.w  2(a3),d7
		move.w	4(a3),d1
                move.l  #ENEMY_DEF_MONKEY,d2
		move.l	#(146<<16!0),d5
		moveq	#SPR_TYPE_ENEMY_2X32,d6
		or.w	6(a3),d6
		swap	d6
		move.w	#SPR_MONKEY,d6
                lea     HDL_MONKEY(pc),a0
                lea     MONKEY_TOGGLE(a4),a1
                lea     MONKEYS_IN_USE(a4),a2
                bsr     SPAWN_ENEMY
        ENDC
		


        IFNE    ENABLE_GIANTWORM
                moveq   #SPR_GIANTWORM,d2
                GET_ENEMY_STATE_IN_MAP
                moveq   #0,d7
                move.w  2(a3),d7
		move.l	#(192<<16!0),d5
		move.w	4(a3),d1
		;move.l	#(SPR_TYPE_ENEMY_2X32<<16!SPR_GIANTWORM),d6
                moveq   #SPR_TYPE_ENEMY_2X32,d6		
		or.w	6(a3),d6
                swap    d6
                move.w  #SPR_GIANTWORM,d6
                move.l  #ENEMY_DEF_GIANTWORM,d2
		lea     HDL_GIANTWORM(pc),a0
                lea     GIANTWORM_TOGGLE(a4),a1
                lea     GIANTWORMS_IN_USE(a4),a2
                bsr     SPAWN_ENEMY
        ENDC

        IFNE    ENABLE_LAVAMAN
                moveq   #SPR_LAVAMAN,d2
                GET_ENEMY_STATE_IN_MAP
                moveq   #0,d7
                move.w  2(a3),d7
		move.l	#(192<<16!0),d5
		move.w	4(a3),d1
                move.w  #SPR_TYPE_ENEMY_2X32,d6
		or.w	6(a3),d6
                swap    d6
                move.w  #SPR_LAVAMAN,d6
                move.l  #ENEMY_DEF_LAVAMAN,d2
                lea     HDL_LAVAMAN(pc),a0
                lea     LAVAMAN_TOGGLE(a4),a1
                lea     LAVAMEN_IN_USE(a4),a2
                bsr     SPAWN_ENEMY		
        ENDC

	IFNE	ENABLE_CAVEBATS
	        moveq   #SPR_CAVEBAT,d2
                GET_ENEMY_STATE_IN_MAP
		moveq   #0,d7
                move.w  2(a3),d7
		move.l	#(56<<16!0),d5
		move.w	4(a3),d1
                move.w  #SPR_TYPE_ENEMY_2X32,d6
		or.w	6(a3),d6
                swap    d6
                move.w  #SPR_CAVEBAT,d6
                move.l  #ENEMY_DEF_CAVEBAT,d2
                lea     HDL_CAVEBAT(pc),a0
                lea     CAVEBAT_TOGGLE(a4),a1
                lea     CAVEBATS_IN_USE(a4),a2
                bsr     SPAWN_ENEMY
	ENDC

        IFNE    ENABLE_GRIFFIN
                moveq   #SPR_GRIFFIN,d2
                GET_ENEMY_STATE_IN_MAP
		
		moveq   #0,d7		
		move.w  2(a3),d7
		move.l	#(56<<16!0),d5
		move.w	4(a3),d1
                move.w  #SPR_TYPE_ENEMY_2X32,d6
		or.w	6(a3),d6
                swap    d6
                move.w  #SPR_GRIFFIN,d6
		lea     HDL_GRIFFIN(pc),a0
                lea     GRIFFIN_TOGGLE(a4),a1
                lea     GRIFFINS_IN_USE(a4),a2
                move.w  #%0000000000011111,d1
                move.l  #ENEMY_DEF_GRIFFIN,d2
                bsr     SPAWN_ENEMY
        ENDC

        IFNE    ENABLE_DRAGON
		bsr     SPAWN_DRAGON
        ENDC

        IFNE    ENABLE_EVIL
		tst.w	TIME_REMAINING(a4)
		bpl.s	.exit
                tst.w   BACKGROUND_ENABLE(a4)
                bmi.s   .exit
                bsr     SPAWN_EVIL

        ENDC


.exit:  rts


SET_PLATFORMS_ENEMY_CONFIG:
	FUNCID	#$7d44d434
        lea     ROUND_PLATFORMS(a4),a0
        lea     ROUND_ENEMY_POSITIONS(a4),a1
.loop:  move.l  (a0)+,d0
        bmi.s   .exit
        bne.s   .next
; if zero then there is no platform in that tile position
; Clear all ground enemies then
        move.l  #(GROUND_ENEMIES),d0
        not.l   d0
        and.l   d0,(a1)
.next:  addq.w  #4,a1
        bra.s   .loop
.exit:
        rts


; Set all enemies to be destroyed... called at end of round and when
; Rygar collects the KILLALL item
DESTROY_ALL_ENEMIES_ONSCREEN:
	FUNCID	#$0a907069
        moveq   #MAX_SPRITES-1,d7
        lea     SPRITE_FREE_LIST(a4),a0
        moveq   #0,d0
.loop:
        move.l  d7,d0

        move.w  (a0,d0*2),d1
        tst.w   d1
        bmi.s   .skip

        cmp.w   #SPR_EVIL_UL,d1         ;Evil is indestructible.
        beq     .skip
        cmp.w   #SPR_EVIL_UR,d1
        beq     .skip
        cmp.w   #SPR_EVIL_DL,d1
        beq     .skip
        cmp.w   #SPR_EVIL_DR,d1
        beq     .skip

        movem.l d0/d7/a0,-(a7)

        GET_SPRITE_POINTER

        move.w  SPRITE_PLOT_XPOS(a0),d1 ; d0 now has xpos
        addq.w  #1,d1
        lea     SPRITE_SCREEN_LIMITS_X(a4),a1
	cmp.w   (a1),d1
        blt.s   .next
	cmp.w	2(a1),d1
	bgt.s	.next	
        
	cmp.w   #SPR_TYPE_ENEMY_2X32,SPRITE_PLOT_TYPE(a0)
        beq.s   .destroy
        cmp.w   #SPR_TYPE_ENEMY_2X16,SPRITE_PLOT_TYPE(a0)
        beq.s   .destroy
        cmp.w   #SPR_TYPE_ENEMY_1X16,SPRITE_PLOT_TYPE(a0)
        beq.s   .destroy
        cmp.w   #SPR_TYPE_ENEMY_HW,SPRITE_PLOT_TYPE(a0)
        bge.s   .destroy
        bra.s   .next

.destroy:
        move.l  MEMCHK0_SPRITE_HDLVARS(a4),a1
        lea     TAB_64(a4),a2
        add.w   (a2,d0*2),a1
        move.w  #-1,SPRITE_DESTROY_END_ROUND(a1)

.next:
        movem.l (a7)+,d0/d7/a0

.skip:  dbf     d7,.loop
.exit:  rts




; Set all enemies to be destroyed... called at end of round and when
DESTROY_ALL_ENEMIES:
	FUNCID	#$333e26e0
        moveq   #MAX_SPRITES-1,d7
        lea     SPRITE_FREE_LIST(a4),a0
        moveq   #0,d0
.loop:

	move.l  d7,d0
        move.w  (a0,d0*2),d1
        tst.w   d1
        bmi.s   .skip

	cmp.w   #SPR_RYGAR,d1         ; Destroy everything except Rygar and the Carousels.
        beq     .skip
	cmp.w	#SPR_CAROUSEL,d1
	beq	.skip
	cmp.w	#SPR_REAPER,d1
	beq	.skip

        movem.l d0/d7/a0,-(a7)

        GET_SPRITE_POINTER

.destroy:
        move.l  MEMCHK0_SPRITE_HDLVARS(a4),a1
        lea     TAB_64(a4),a2
        add.w   (a2,d0*2),a1
        move.w  #-1,SPRITE_DESTROY_END_ROUND(a1)

.next:
        movem.l (a7)+,d0/d7/a0

.skip:  dbf     d7,.loop
.exit:  rts

SET_ALL_ENEMIES_OFFSCREEN:
	FUNCID	#$a3413fc6
        lea     SPRITE_FREE_LIST(a4),a1
        moveq   #MAX_SPRITES-1,d7


.loop:  tst.w   (a1,d7*2)
        bmi.s   .next
        move.l  d7,d0
        GET_SPRITE_POINTER              ; a0 now has sprite pointer
        tst.w   (a0)
        bmi     .next
        move.w  #0,SPRITE_PLOT_XPOS(a0)
        move.w  #0,SPRITE_PLOT_YPOS(a0)
.next:  dbf     d7,.loop
.exit:  rts




;a0 = Obstcales pointer
SET_OBSTACLES_ENEMY_CONFIG:
	FUNCID	#$b9b2526c
        move.l  ROUND_OBSTACLES(a4),a0
        lea     ROUND_ENEMY_POSITIONS(a4),a1

.loop:
        moveq   #0,d0
        moveq   #0,d1
        move.w  (a0)+,d6                                ; Read X Point
        move.w  (a0)+,d7                                ; Read Y Point

        cmp.w   #$7fff,d6                               ; finished?
        beq     .exit

.loop2: move.w  (a0)+,d5                                ;Read First Tile
        cmp.w   #-1,d5
        beq.s   .loop

        move.l  #GROUND_ENEMIES,d0
        not.l   d0
        and.l   d0,(a1,d6*4)
        addq.w  #1,d6
        bra.s   .loop2

.next:  addq.w  #4,a1
        bra.s   .loop
.exit:  rts




SPAWN_DRAGON:   
		FUNCID	#$56d39438
		
                move.w  FRAME_BG(a4),d0
                and.w   #%0000000000000111,d0           ; Every 64 frames
                bne     .exit

                not.w   DRAGON1_TOGGLE(a4)
                move.w  #-1,DRAGON1_TOGGLE(a4)

                move.l  SPAWN_RIGHT_BLOCK(a4),d4
                and.l   #ENEMY_DEF_DRAGON,d4   ; Is there a platform?
                beq     .exit

                moveq   #SPR_DRAGON1,d2
                GET_ENEMY_STATE_IN_MAP
                moveq   #0,d7
                move.w  2(a3),d7

		IFNE	ENABLE_RIDER
                cmp.w   #MAX_RIDERS,RIDERS_IN_USE(a4)
                bge   .exit
		ENDC
		
                cmp.w   #MAX_DRAGONS,DRAGONS_IN_USE(a4)
                bge   	.exit
                addq.w  #1,DRAGONS_IN_USE(a4)

; Count how many free hardware sprites slots there is in the upper zone
; if two are available then push the dragon.

; Test bank 0
		lea	ENEMY_HWSPR_UPPER_ALLOCATED(a4),a3
		move.w	2(a3),d2
		swap	d2
		move.w	6(a3),d2
		tst.l	d2
		bpl.s	.bank_1
		;tst.w	(a3)			; Is bank 0 free?
		;bpl.s	.bank_1			; Yes
		;tst.w	6(a3)
		;bpl.s	.bank_1
		bra.s	.bank_free

.bank_1:
		addq.w	#8,a3			; No, try bank 1
		move.w	2(a3),d2
		swap	d2
		move.w	6(a3),d2
		tst.l	d2
		bpl	.exit
		
		;tst.w	2(a3)			
		;bpl	.exit		; Yes... so allocate
		;tst.w	6(a3)
		;bpl	.exit
		nop

.bank_free:
                moveq   #SPR_DRAGON1,d0         ; sprite number
                moveq   #0,d1
                move.w  #64,d2                  ; yposition
                move.w  #SPR_TYPE_ENEMY_2X32,d3 ; control
                lea     HDL_DRAGON1(pc),a0              ; handler
                moveq   #0,d7
                bsr     PUSH_SPRITE
		tst.w	d4
		bmi.s	.max_reached
		move.w	d4,2(a3)
		
		move.l	d4,d6			; Save dragon sprite allocated number
		
;d4=sprite number that was allocated

; Do second part of Dragon.
.slot:          moveq   #0,d1
                moveq   #SPR_DRAGON2,d0         ; sprite number
                move.w  #64,d2                  ; yposition
                move.w  #SPR_TYPE_ENEMY_2X32,d3 ; control
                lea     HDL_DRAGON2(pc),a0              ; handler
                bsr     PUSH_SPRITE
		tst.w	d4
		bmi.s	.max_reached
		move.w	d4,6(a3)
		
		lea	ATTACH_TABLE(a4),a3		; Attach Dragon 2 to Dragon 1
		move.b	d6,(a3,d4)
		

.rider:
		IFNE	ENABLE_RIDER
                cmp.w   #MAX_RIDERS,RIDERS_IN_USE(a4)
                bge.s   .exit
                addq.w  #1,RIDERS_IN_USE(a4)

                moveq   #0,d1
                moveq   #SPR_RIDER,d0           ; sprite number
                move.w  #64,d2                  ; yposition
                move.w  #SPR_TYPE_ENEMY_2X32,d3 ; control
                lea	HDL_RIDER(pc),a0           ; handler
                bsr     PUSH_SPRITE
		tst.w	d4
		bmi.s	.max_reached

		lea	ATTACH_TABLE(a4),a3		; Save the allocated dragon into the rider cell.
		move.b	d6,(a3,d4)
		move.b	d4,(a3,d6)
.already_attached:		
		ENDC
.max_reached:
.exit:          rts



;a0 = Handler Address
;a1 = Toggle pointer (.w)
;a2 = In Use pointer
;d1 = Frames
;d2 = Enemy Definition Type
;d5 = ypos,xpos
;d6 = sprite number, sprite type
;d7 = max number of allowed type
SPAWN_ENEMY:    

		FUNCID	#$25daa87e
		tst.w   DISABLE_ENEMIES(a4)
                bmi     .exit

                move.w  FRAME_BG(a4),d0
                and.w   d1,d0                           ; Every 64 frames
                bne     .exit
		
                move.w  #-1,(a1)

                move.l  SPAWN_RIGHT_BLOCK(a4),d4
                and.l   d2,d4   ; Is there a platform?
                beq.s   .exit

                cmp.w   (a2),d7
                ble.s   .exit
                addq.w  #1,(a2)

; Count how many free hardware sprites slots there is in the upper zone
; if two are available then push the dragon.

                move.w  #0,d0
                move.b  d6,d0                   ; Sprite number		d6=[TTTTPPNN] T=Type, P=Parameter, N=Number
                moveq   #0,d3
                swap    d6
                move.w  d6,d3                   ; Sprite Type

                moveq   #0,d1
                move.w  d5,d1                   ; X position
                swap    d5
                moveq   #0,d2
                move.w  d5,d2                   ; Y position

                moveq   #0,d7
                bsr     PUSH_SPRITE
.exit:          rts

SPAWN_EVIL:     
		FUNCID	#$74fab636
		
		tst.w   DISABLE_ENEMIES(a4)
                bmi     .exit

                tst.w   EVIL_IN_USE(a4)
                bmi     .exit
                move.w  #-1,EVIL_IN_USE(a4)

; Count how many free hardware sprites slots there is in the upper zone
; if two are available then push the dragon.
                moveq   #0,d1
                moveq   #SPR_EVIL_UL,d0         ; sprite number
                move.w  #56,d2                  ; yposition
                move.w  #SPR_TYPE_ENEMY_2X32,d3 ; control
                lea	HDL_EVIL_UL(pc),a0         ; handler
                moveq   #0,d7
                bsr     PUSH_SPRITE
		tst.w	d4
		bmi.s	.max_reached
                move.w  d4,EVIL_UL_SPRNUM(a4)

                moveq   #0,d1
                moveq   #SPR_EVIL_UR,d0         ; sprite number
                move.w  #56,d2                  ; yposition
                move.w  #SPR_TYPE_ENEMY_2X32,d3 ; control
                lea	HDL_EVIL_UR(pc),a0         ; handler
                moveq   #0,d7
                bsr     PUSH_SPRITE
		tst.w	d4
		bmi.s	.max_reached
                move.w  d4,EVIL_UR_SPRNUM(a4)

                moveq   #0,d1
                moveq   #SPR_EVIL_DL,d0         ; sprite number
                move.w  #56,d2                  ; yposition
                move.w  #SPR_TYPE_ENEMY_2X32,d3 ; control
                lea	HDL_EVIL_DL(pc),a0         ; handler
                moveq   #0,d7
                bsr     PUSH_SPRITE
		tst.w	d4
		bmi.s	.max_reached
                move.w  d4,EVIL_DL_SPRNUM(a4)

                moveq   #0,d1
                moveq   #SPR_EVIL_DR,d0         ; sprite number
                move.w  #56,d2                  ; yposition
                move.w  #SPR_TYPE_ENEMY_2X32,d3 ; control
                lea	HDL_EVIL_DR(pc),a0         ; handler
                moveq   #0,d7
                bsr     PUSH_SPRITE
		tst.w	d4
		bmi.s	.max_reached
                move.w  d4,EVIL_DR_SPRNUM(a4)
.max_reached:	
.exit:          rts



CLEAR_SPRITES_IN_USE:
	FUNCID	#$e5ac44a4
	
	clr.w	APES_IN_USE(a4)
	clr.w	BIGRHINOS_IN_USE(a4)	
	clr.w	CAVEBATS_IN_USE(a4)
	clr.w	CRABS_IN_USE(a4)
	clr.w	DRAGONS_IN_USE(a4)
	clr.w	DRONES_IN_USE(a4)
	clr.w	EVIL_IN_USE(a4)	
	clr.w	FIREBALLS_IN_USE(a4)
	clr.w	FISH_IN_USE(a4)
	clr.w	GIANTDEMONS_IN_USE(a4)	
	clr.w	GIANTWORMS_IN_USE(a4)
	clr.w	GRIFFINS_IN_USE(a4)
	clr.w	HAMMERS_IN_USE(a4)
	clr.w	LAVAMEN_IN_USE(a4)
	clr.w	LAVASPIT_IN_USE(a4)	
	clr.w	LIZARDS_IN_USE(a4)
	clr.w	MONKEYS_IN_USE(a4)	
	clr.w	MUTANTFROGS_IN_USE(a4)
	clr.w	RHINOS_IN_USE(a4)	
	clr.w	ROCKETS_IN_USE(a4)
	clr.w	RIDERS_IN_USE(a4)	
	clr.w	SQUIRRELS_IN_USE(a4)
	clr.w	TRIBESMEN_IN_USE(a4)
	clr.w	VILLAGERS_IN_USE(a4)	
	clr.w	GRIFFIN_LAST_SPAWN(a4)

	rts

