CAROUSEL_TURN1:         equ     0
CAROUSEL_TURN2:         equ     1
CAROUSEL_TURN3:         equ     2
CAROUSEL_TURN4:         equ     3
CAROUSEL_TURN5:         equ     4

SANCTUARY_LEFT_CAROUEL_XPOS:    equ     256+16
SANCTUARY_RIGHT_CAROUEL_XPOS:   equ     512

SANCTUARY_SCROLL_RIGHT:		equ	0
SANCTUARY_TIMER_BONUS:		equ	96
SANCTUARY_SHOW_PASSWORD:	equ	97
SANCTUARY_DISABLE_ENEMIES:	equ	98
SANCTUARY_ENABLE_ENEMIES:	equ	99
SANCTUARY_RYGAR_FACING:		equ	100
SANCTUARY_FACE_SCROLL_RIGHT:	equ	101


BONUS_CHAR_DELAY:       equ     1



MAX_REPULSE:            equ     63

INIT_SANCTUARY_VARS:
	FUNCID	#$7a062fbb
        clr.b   BONUS_TEXT_WAIT(a4)
        clr.b   BONUS_TEXT_DONE(a4)
        clr.b   BONUS_TEXT_XPOS(a4)
        clr.b   BONUS_TEXT_YPOS(a4)
        clr.w   DONE_DRAW_REPULSE_BONUS(a4)
        clr.w   DONE_DRAW_CLEAR(a4)
        clr.w   DONE_DRAW_REPULSE_SUM(a4)
        clr.w   DONE_DRAW_SUMMARY(a4)
        clr.w   REPULSE_BONUS_COUNT(a4)

        clr.l   TOTAL_REPULSE_BONUS(a4)
        clr.l   TOTAL_RANK_BONUS(a4)
        clr.l   TOTAL_TIME_BONUS(a4)
        clr.l   TOTAL_BONUS(a4)
        rts
	
SANCTUARY_ENTERING:	equ	0
SANCTUARY_GETBONUS:	equ	1
SANCTUARY_LEAVING:	equ	2
SANCTUARY_COMPLETE:	equ	3


SANCTUARY:
	FUNCID	#$b79aa950
        tst.w   AUTO_PAN_RIGHT(a4)
        beq.s   .exit
        bmi.s   .exit

	bsr	UPDATE_COPPER_SPRITES

        lea     SANCTUARY_TABLE_1(a4),a0
	
        cmp.w   #0,SANCTUARY_STAGE(a4)                          ; Rygar running into sanctuary
        beq.s   .stage_1                                        ; at this stage.
        cmp.w   #1,SANCTUARY_STAGE(a4)                          ; Bonus being given during this
        beq     SANCTUARY_BONUS                                 ; sequence
        cmp.w   #2,SANCTUARY_STAGE(a4)                          ; Rygar running out of sanctuary
        beq.s   .stage_2                                        ; at this stage.
        cmp.w   #3,SANCTUARY_STAGE(a4)                  	; Sanctuary completed at this stage.
        beq     SANCTUARY_DONE
	
.stage_2:
        lea     SANCTUARY_TABLE_2(a4),a0

.stage_1:
        moveq   #0,d0
        move.w  SANCTUARY_FRAME(a4),d0
        addq.w  #1,SANCTUARY_FRAME(a4)
        move.w  (a0,d0*2),d0
        tst.w   d0
        bmi.s   .turn_done
        move.w  d0,RYGAR_SANCTUARY_COMMAND(a4)                  ; used as x_offset.

	cmp.w   #SANCTUARY_SCROLL_RIGHT,d0
        beq.s   .scroll_right	
        cmp.w   #SANCTUARY_RYGAR_FACING,d0
	beq.s	.stance_only
        cmp.w   #SANCTUARY_FACE_SCROLL_RIGHT,d0
        bge.s   .scroll_and_stance

        bra.s   .exit
	
.turn_done:
        clr.w   RYGAR_SANCTUARY_COMMAND(a4)
        clr.w   SANCTUARY_FRAME(a4)
        addq.w  #1,SANCTUARY_STAGE(a4)
        bra.s   .exit

.scroll_right:
        bsr     PAN_RIGHT
        bsr     SCROLL_CAROUSELS
        bra.s   .exit

.stance_only:
        bra.s   .exit

.scroll_and_stance:
        bsr     PAN_RIGHT
        bsr     SCROLL_CAROUSELS
	bra.s	.exit
	nop
.exit:  rts

SCROLL_CAROUSELS:
	FUNCID	#$fbab09e0
        moveq   #SPR_RESERVED_LEFT_CAROUSEL,d0
        bsr     GET_SPRITE_CONTEXT
        subq.w  #2,SPRITE_PLOT_XPOS(a0)                         ; Set Xposition

        moveq   #SPR_RESERVED_RIGHT_CAROUSEL,d0
        bsr     GET_SPRITE_CONTEXT
        subq.w  #2,SPRITE_PLOT_XPOS(a0)
        rts

; d7 = pause frames
SANCTUARY_PAUSE:
	FUNCID	#$4176be17
.loop:  bsr     WAIT_FOR_VERTICAL_BLANK
        dbf     d7,.loop
        rts


; d0=8x8 x
; d1=8x8 y
; d6=Colour number 0-3 (0=BP1, 1=BP2, 2=BP3, 3=BP4, 4=BP5)
; d7=-1 Draw on nibble 0=byte
; a0=text
; a1=Screen addresses

; Here we display the Repulse bonus
SANCTUARY_BONUS:
	FUNCID	#$f3ad08f4
        tst.w   DONE_DRAW_REPULSE_BONUS(a4)                             ; Has 'REPULSE BONUS' text been drawn?
        bmi.s   .repulse_text_done                              ; Yes
        move.w  #-1,DONE_DRAW_REPULSE_BONUS(a4)                 ; No, Mark Yes for next frame
;       move.w  REPULSE_BONUS_NUM(a4),REPULSE_BONUS_SAVE                ; Enemies killed

        bsr     SET_COPPER_BONUS_SCENE1                         ; Set colour scheme for copper
        bsr     DISPLAY_TEXT_REPULSE_BONUS

        move.l  #25,d7
        bsr     SANCTUARY_PAUSE
        bra     .face

.repulse_text_done:

        cmp.w   #MAX_REPULSE,REPULSE_BONUS_NUM(a4)
        ble.s   .max_repulse
        move.w  #MAX_REPULSE,REPULSE_BONUS_NUM(a4)
.max_repulse:

        tst.w   REPULSE_BONUS_NUM(a4)                           ; Has the repulse bonus counted down?
        beq     .count_done                                     ; Yes, move on to timer..
        bsr     OVERWRITE_BONUS_BACKGROUND                      ; Overwrite 'BONUS' background text
        bsr     DISPLAY_TEXT_REPULSE_BONUS
        bsr     DRAW_REPULSE_COUNTER
        bsr     DRAW_REPULSE_TOTAL
        bra     .face

.repulse_sum_done:
.count_done:
        tst.w   DONE_DRAW_CLEAR(a4)
        bmi.s   .show_summary
        move.w  #-1,DONE_DRAW_CLEAR(a4)

        move.l  #25,d7
        bsr     SANCTUARY_PAUSE

        bsr     OVERWRITE_TEXT_WITH_TEMPLE
        bra     .face

.show_summary:
        tst.w   DONE_DRAW_SUMMARY(a4)
        bmi     .countdown
        move.w  #-1,DONE_DRAW_SUMMARY(a4)

        bsr     SET_COPPER_BONUS_SCENE2         ; Set colour scheme for first bonus
        bsr     DISPLAY_REPULSE_BONUS
        move.l  #25,d7
        bsr     SANCTUARY_PAUSE

        bsr     DISPLAY_RANK_BONUS

        move.l  #25,d7
        bsr     SANCTUARY_PAUSE

        bsr     DISPLAY_TIMER_BONUS

        move.l  #25,d7
        bsr     SANCTUARY_PAUSE

        bsr     DISPLAY_TOTAL_BONUS
        bsr     DRAW_COUNTDOWN_TOTAL

.countdown:
;---------------------------------------------
; Draw Total value
        tst.l   TOTAL_BONUS(a4)
        bmi.s   .pause
        beq.s   .pause
        sub.l   #100,TOTAL_BONUS(a4)
        add.l   #100,VAL_PLAYER1_SCORE(a4)
        bsr     OVERWRITE_BONUS_BACKGROUND
        bsr     DISPLAY_TEXT_REPULSE_BONUS

        moveq   #8,d0
        moveq   #5,d1
        lea     TEXT_SUMMARY_TOTAL(a4),a0
        move.l  LSTPTR_FRONT_SCREEN(a4),a1
        add.w   MAP_POSX(a4),a1
        move.l  LSTPTR_POST_SCREEN(a4),a2
        add.w   MAP_POSX(a4),a2
        bsr     DRAW_BONUS_TEXT
        move.l  LSTPTR_BACK_SCREEN(a4),a1
        add.w   MAP_POSX(a4),a1
        bsr     DRAW_BONUS_TEXT

        bsr     DRAW_COUNTDOWN_TOTAL
        bsr     UPDATE_PLAYER_SCORE

        move.l  #1,d7
        bsr     SANCTUARY_PAUSE

        bra     .face

; Now clear bg

.pause:
        move.l  #25,d7
        bsr     SANCTUARY_PAUSE

        bsr     OVERWRITE_TEXT_WITH_TEMPLE
        bsr     SET_MAINGAME_COPPER

        addq.w  #1,SANCTUARY_STAGE(a4)                          ; Move to next stage
        move.w  #-1,FLAG_NEXT_ROUND(a4)                             ; Next round load
.face:  move.w  #SANCTUARY_RYGAR_FACING,RYGAR_SANCTUARY_COMMAND(a4)                 ; Make Rygar face the screen.
.exit:  rts

SGT

DRAW_COUNTDOWN_TOTAL:
	FUNCID	#$36d88ca9
        moveq   #0,d0
        move.l  TOTAL_BONUS(a4),d0
        bsr     Binary2Decimal
        lea     TEXT_TOTAL_SUM(a4),a1
        moveq   #6-1,d6
        bsr     COPY_DECIMAL

        moveq   #22,d0
        moveq   #5,d1
        lea     TEXT_TOTAL_SUM(a4),a0
        move.l  LSTPTR_FRONT_SCREEN(a4),a1
        add.w   MAP_POSX(a4),a1
        move.l  LSTPTR_POST_SCREEN(a4),a2
        add.w   MAP_POSX(a4),a2
        bsr     DRAW_BONUS_TEXT
        move.l  LSTPTR_BACK_SCREEN(a4),a1
        add.w   MAP_POSX(a4),a1
        bsr     DRAW_BONUS_TEXT
        rts



SANCTUARY_DONE:
	FUNCID	#$b1413864
        clr.w   AUTO_PAN_RIGHT(a4)
        clr.w   RYGAR_SANCTUARY_COMMAND(a4)
        clr.w   SANCTUARY_FRAME(a4)
        clr.w   SANCTUARY_STAGE(a4)

        moveq   #SPR_RESERVED_LEFT_CAROUSEL,d0
        bsr     GET_SPRITE_CONTEXT
        move.w  #SANCTUARY_LEFT_CAROUEL_XPOS,SPRITE_PLOT_XPOS(a0)                               ; Set Xposition
	
        moveq   #SPR_RESERVED_RIGHT_CAROUSEL,d0
        bsr     GET_SPRITE_CONTEXT
        move.w  #SANCTUARY_RIGHT_CAROUEL_XPOS,SPRITE_PLOT_XPOS(a0)

        moveq   #SPR_RESERVED_LEFT_CAROUSEL,d0
        bsr     DISABLE_SPRITE
        moveq   #SPR_RESERVED_RIGHT_CAROUSEL,d0
        bsr     DISABLE_SPRITE
	
; Reset Carousel frames
	clr.w	LEFT_CAROUSEL_POSITION(a4)
	clr.w	RIGHT_CAROUSEL_POSITION(a4)
	moveq	#SPR_RESERVED_LEFT_CAROUSEL,d0
	moveq	#0,d1
	bsr	RESET_SPRITE_FRAMES
	moveq	#SPR_RESERVED_RIGHT_CAROUSEL,d0
	moveq	#0,d1
	bsr	RESET_SPRITE_FRAMES	

; Keep playing shield music if timer not finished.
        moveq   #MUSIC_RESTART,d0
	tst.w	SHIELD_TIMER(a4)
	bmi.s	.play_tune
        moveq   #MUSIC_SHIELD,d0	
.play_tune:
        bsr     PLAY_TUNE
.exit:	rts


HDL_CAROUSEL:
		nop
		
		FUNCID	#$14c697ad
                ;subq.w #1,2(a1)                                ; Move right.

                cmp.w   #SPR_RESERVED_LEFT_CAROUSEL,d0
                beq.s   .left
                cmp.w   #SPR_RESERVED_RIGHT_CAROUSEL,d0
                beq.s   .right
                bra.s   .right

.left:          moveq   #0,d1
                move.w  LEFT_CAROUSEL_POSITION(a4),d1
                bra     ANIMATE
.right:         moveq   #0,d1
                move.w  RIGHT_CAROUSEL_POSITION(a4),d1
                bra     ANIMATE
                rts



OVERWRITE_BONUS_BACKGROUND:
	FUNCID	#$682054b0
        move.l  LSTPTR_POST_SCREEN(a4),a0
        add.w   MAP_POSX(a4),a0
        move.l  LSTPTR_FRONT_SCREEN(a4),a1
        add.w   MAP_POSX(a4),a1
        move.l  LSTPTR_BACK_SCREEN(a4),a2
        add.w   MAP_POSX(a4),a2
        add.w   #(PLAYFIELD_SIZE_X*5)*40,a0
        add.w   #(PLAYFIELD_SIZE_X*5)*40,a1
        add.w   #(PLAYFIELD_SIZE_X*5)*40,a2

        move.l  #(PLAYFIELD_SIZE_X*5)*8/4,d7
.block: move.l  (a0)+,d0
        move.l  d0,(a1)+
        move.l  d0,(a2)+
        dbf     d7,.block
.exit:  rts


DRAW_REPULSE_TOTAL:
	FUNCID	#$058c5e41
        moveq   #0,d0
        move.w  REPULSE_BONUS_COUNT(a4),d0
        mulu    #200,d0
        bsr     Binary2Decimal
        lea     TEXT_REPULSE_SUM(a4),a1
        moveq   #6-1,d6
        bsr     COPY_DECIMAL

        moveq   #0,d0
        move.w  REPULSE_BONUS_COUNT(a4),d0
        lea     TXT_TIMER_SEC(a4),a0
        move.l  (a0,d0*4),d0
        and.l   #$00ffffff,d0
        or.l    #$2000002a,d0                   ; 2a='*'
        move.l  d0,TEXT_REPULSE_AMOUNT(a4)

        moveq   #12,d0
        moveq   #5,d1
        lea     TEXT_REPULSE_AMOUNT(a4),a0
        move.l  LSTPTR_BACK_SCREEN(a4),a1
        add.w   MAP_POSX(a4),a1
        move.l  LSTPTR_POST_SCREEN(a4),a2
        add.w   MAP_POSX(a4),a2
        bsr     DRAW_BONUS_TEXT
        move.l  LSTPTR_FRONT_SCREEN(a4),a1
        add.w   MAP_POSX(a4),a1
        bsr     DRAW_BONUS_TEXT

        moveq   #20,d0
        moveq   #5,d1
        lea     TEXT_REPULSE_SUM(a4),a0
        move.l  LSTPTR_FRONT_SCREEN(a4),a1
        add.w   MAP_POSX(a4),a1
        move.l  LSTPTR_POST_SCREEN(a4),a2
        add.w   MAP_POSX(a4),a2
        bsr     DRAW_BONUS_TEXT
        move.l  LSTPTR_BACK_SCREEN(a4),a1
        add.w   MAP_POSX(a4),a1
        bsr     DRAW_BONUS_TEXT
        rts

DRAW_REPULSE_COUNTER:
	FUNCID	#$48a043d1
        movem.l d0-d7/a0-a3,-(a7)
        tst.b   BONUS_TEXT_WAIT(a4)
        bmi.s   .do_char
        subq.b  #1,BONUS_TEXT_WAIT(a4)
        bra     .done

.do_char:
        addq.w  #1,REPULSE_BONUS_COUNT(a4)
        subq.w  #1,REPULSE_BONUS_NUM(a4)
        move.b  #BONUS_CHAR_DELAY,BONUS_TEXT_WAIT(a4)

        moveq   #8,d0
        moveq   #2,d1
        add.b   BONUS_TEXT_XPOS(a4),d0
        add.b   BONUS_TEXT_YPOS(a4),d1
        move.l  LSTPTR_FRONT_SCREEN(a4),a1
        add.w   MAP_POSX(a4),a1
        bsr     DRAW_BONUS_CHAR

        moveq   #8,d0
        moveq   #2,d1
        add.b   BONUS_TEXT_XPOS(a4),d0
        add.b   BONUS_TEXT_YPOS(a4),d1
        move.l  LSTPTR_BACK_SCREEN(a4),a1
        add.w   MAP_POSX(a4),a1
        bsr     DRAW_BONUS_CHAR

        cmp.b   #19,BONUS_TEXT_XPOS(a4)
        beq.s   .next_y
        addq.b  #1,BONUS_TEXT_XPOS(a4)
        bra.s   .done

.next_y:
        clr.b   BONUS_TEXT_XPOS(a4)
        addq.b  #1,BONUS_TEXT_YPOS(a4)

.done:  movem.l (a7)+,d0-d7/a0-a3
        rts



; d0=8x8 x
; d1=8x8 y
; a1=Screen addresses
DRAW_BONUS_CHAR:
	FUNCID	#$33ef25db
        movem.l d0-d7/a0-a3,-(a7)
        lsl.w   #3,d1
        mulu    #PLAYFIELD_SIZE_X*5,d1
        add.l   d1,a1
        add.l   d0,a1

        moveq   #0,d0
        move.l  FONT_ASSET(a4),a2
        add.w   #86,a2                  ; Offset to BODY

        move.b  #"\",d0
        sub.b   #32,d0

.draw:  add.l   d0,a2           ; index into character

        move.b  (a2),d0
        or.b    d0,(a1)
        or.b    d0,PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,PLAYFIELD_SIZE_X*4(a1)

        move.b  FONT_WIDTH*1(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*1)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*1)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*1)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*1)+PLAYFIELD_SIZE_X*4(a1)

        move.b  FONT_WIDTH*2(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*2)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*2)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*2)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*2)+PLAYFIELD_SIZE_X*4(a1)

        move.b  FONT_WIDTH*3(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*3)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*3)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*3)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*3)+PLAYFIELD_SIZE_X*4(a1)

        move.b  FONT_WIDTH*4(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*4(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*4)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*4)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*4)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*4)+PLAYFIELD_SIZE_X*4(a1)

        move.b  FONT_WIDTH*5(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*5(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*5)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*5)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*5)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*5)+PLAYFIELD_SIZE_X*4(a1)

        move.b  FONT_WIDTH*6(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*6(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*6)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*6)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*6)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*6)+PLAYFIELD_SIZE_X*4(a1)

        move.b  FONT_WIDTH*7(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*7(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*7)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*7)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*7)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*7)+PLAYFIELD_SIZE_X*4(a1)

.exit:  movem.l (a7)+,d0-d7/a0-a3
        rts


; d0=8x8 x
; d1=8x8 y
; d7=-1 Draw on nibble 0=byte
; a0=text
; a1=Screen addresses
; a2=Background Screen address
DRAW_BONUS_TEXT:
	FUNCID	#$f044bf14
        movem.l d0-d7/a0-a3,-(a7)
        lsl.w   #3,d1
        mulu    #PLAYFIELD_SIZE_X*5,d1
        add.l   d1,a1
        add.l   d0,a1

        moveq   #0,d0
.loop:  move.l  FONT_ASSET(a4),a2
        add.w   #86,a2                  ; Offset to BODY
        tst.b   (a0)
        beq     .exit
        move.b  (a0)+,d0
        sub.b   #32,d0

.draw:
        add.l   d0,a2           ; index into character

        move.b  (a2),d0
        or.b    d0,(a1)
        or.b    d0,PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,PLAYFIELD_SIZE_X*4(a1)

        move.b  FONT_WIDTH*1(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*1)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*1)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*1)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*1)+PLAYFIELD_SIZE_X*4(a1)

        move.b  FONT_WIDTH*2(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*2)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*2)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*2)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*2)+PLAYFIELD_SIZE_X*4(a1)

        move.b  FONT_WIDTH*3(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*3)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*3)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*3)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*3)+PLAYFIELD_SIZE_X*4(a1)

        move.b  FONT_WIDTH*4(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*4(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*4)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*4)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*4)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*4)+PLAYFIELD_SIZE_X*4(a1)

        move.b  FONT_WIDTH*5(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*5(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*5)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*5)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*5)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*5)+PLAYFIELD_SIZE_X*4(a1)

        move.b  FONT_WIDTH*6(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*6(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*6)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*6)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*6)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*6)+PLAYFIELD_SIZE_X*4(a1)

        move.b  FONT_WIDTH*7(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*7(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*7)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*7)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*7)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*7)+PLAYFIELD_SIZE_X*4(a1)

        addq.w  #1,a1
        bra     .loop

.exit:  movem.l (a7)+,d0-d7/a0-a3
        rts


        move.l  LSTPTR_FRONT_SCREEN(a4),a1
        add.w   MAP_POSX(a4),a1



;d1:rank to blit
;a1:Screen address
SUMMARY_BLIT_RANK:
	FUNCID	#$48f9de53
        cmp.w   #MAX_RANK,d1
        bgt     .exit

        move.l  RANKS_MAIN(a4),a0
        add.w   #$B0,a0

        WAIT_FOR_BLITTER
        move.w  #$09f0,BLTCON0(a5)
        move.w  #$0,BLTCON1(a5)
        move.l  #-1,BLTAFWM(a5)                                 ; Only want the first word


								; Rank is 0.
        add.w   #14,a1
        add.w   #(PLAYFIELD_SIZE_X*5)*16,a1

        move.w  #(512/8)-6,BLTAMOD(a5)                          ; Blit the wings
        move.w  #PLAYFIELD_SIZE_X-6,BLTDMOD(a5)
        move.l  a0,BLTAPTH(a5)                                  
        move.l  a1,BLTDPTH(a5)
        move.w  #(40*64)+3,BLTSIZE(a5)                          

        tst.w   d1		
        beq.s   .exit
        addq.w  #4,a0                                           ; rank at least 2

.above_rank1:
        WAIT_FOR_BLITTER
	
        add.w   d1,d1
        add.l   d1,a0
        addq.w  #2,a1                                           ; Offset in from left

        move.w  #(512/8)-2,BLTAMOD(a5)                          ; screen size is $a2
        move.w  #PLAYFIELD_SIZE_X-2,BLTDMOD(a5)
        move.l  a0,BLTAPTH(a5)                                  ; Load the mask address
        move.l  a1,BLTDPTH(a5)
        move.w  #(40*64)+1,BLTSIZE(a5)                          ; 8*5 bitplanes
.exit:  rts



SET_MAINGAME_COPPER:
	FUNCID	#$555364ea
	bsr	WAIT_FOR_VERTICAL_BLANK
        move.l  COPPTR_MAIN_START(a4),a0                                ; Main copper
        move.l  #$01fe0000,(a0)+
        move.l  #$01fe0000,(a0)+
        move.l  #$01fe0000,(a0)+
        rts


SET_COPPER_BONUS_SCENE1:
	FUNCID	#$99cecfc9
	bsr	WAIT_FOR_VERTICAL_BLANK
        move.l  COPPTR_MAIN_START(a4),a0                                ; Main copper
        move.l  COPPTR_BONUS_SCENE1(a4),d0
        move.l  #COP2LCL,d1
        lsl.l   #8,d1
        lsl.l   #8,d1
        move.w  d0,d1
        move.l  d1,(a0)+

        swap    d0

        move.l  #COP2LCH,d1
        lsl.l   #8,d1
        lsl.l   #8,d1
        move.w  d0,d1
        move.l  d1,(a0)+

        move.l  #COPJMP2,d1
        lsl.l   #8,d1
        lsl.l   #8,d1
        move.l  d1,(a0)+
        rts


SET_COPPER_BONUS_SCENE2:
	FUNCID	#$375dae6b
	bsr	WAIT_FOR_VERTICAL_BLANK
        move.l  COPPTR_MAIN_START(a4),a0                                ; Main copper
        move.l  COPPTR_BONUS_SCENE2(a4),d0
        move.l  #COP2LCL,d1
        lsl.l   #8,d1
        lsl.l   #8,d1
        move.w  d0,d1
        move.l  d1,(a0)+

        swap    d0

        move.l  #COP2LCH,d1
        lsl.l   #8,d1
        lsl.l   #8,d1
        move.w  d0,d1
        move.l  d1,(a0)+

        move.l  #COPJMP2,d1
        lsl.l   #8,d1
        lsl.l   #8,d1
        move.l  d1,(a0)+
        rts


DISPLAY_TEXT_REPULSE_BONUS:
	FUNCID	#$8a268797
        moveq   #8,d0
        moveq   #1,d1
        lea     TEXT_REPULSE_BONUS(a4),a0
        move.l  LSTPTR_FRONT_SCREEN(a4),a1
        add.w   MAP_POSX(a4),a1
        move.l  LSTPTR_POST_SCREEN(a4),a2
        add.w   MAP_POSX(a4),a2
        bsr     DRAW_BONUS_TEXT
        move.l  LSTPTR_BACK_SCREEN(a4),a1
        add.w   MAP_POSX(a4),a1
        bsr     DRAW_BONUS_TEXT
        rts


OVERWRITE_TEXT_WITH_TEMPLE:
	FUNCID	#$2e757f47
        move.l  LSTPTR_BACK_SCREEN(a4),a0
        add.w   MAP_POSX(a4),a0
        move.l  LSTPTR_FRONT_SCREEN(a4),a1
        add.w   MAP_POSX(a4),a1
        move.l  LSTPTR_POST_SCREEN(a4),a2
        add.w   MAP_POSX(a4),a2
        move.l  #(40*5)*100/4,d7
.copy_loop:
        move.l  (a2)+,d0
        move.l  d0,(a0)+
        move.l  d0,(a1)+
        dbf     d7,.copy_loop
        rts


DISPLAY_REPULSE_BONUS:
	FUNCID	#$b0e81be8
;---------------------------------------------
; Draw "Repulse Bonus:"
        moveq   #8,d0
        moveq   #1,d1
        lea     TEXT_SUMMARY_REPULSE(a4),a0
        move.l  LSTPTR_FRONT_SCREEN(a4),a1
        add.w   MAP_POSX(a4),a1
        move.l  LSTPTR_POST_SCREEN(a4),a2
        add.w   MAP_POSX(a4),a2
        bsr     DRAW_BONUS_TEXT
        move.l  LSTPTR_BACK_SCREEN(a4),a1
        add.w   MAP_POSX(a4),a1
        bsr     DRAW_BONUS_TEXT

;---------------------------------------------
; Draw Repulse Bonus value
        moveq   #22,d0
        moveq   #1,d1
        lea     TEXT_REPULSE_SUM(a4),a0
        move.l  LSTPTR_FRONT_SCREEN(a4),a1
        add.w   MAP_POSX(a4),a1
        move.l  LSTPTR_POST_SCREEN(a4),a2
        add.w   MAP_POSX(a4),a2
        bsr     DRAW_BONUS_TEXT
        move.l  LSTPTR_BACK_SCREEN(a4),a1
        add.w   MAP_POSX(a4),a1
        bsr     DRAW_BONUS_TEXT
        rts


DISPLAY_RANK_BONUS:
	FUNCID	#$60d3d804
;---------------------------------------------
; Draw Rank Summary
        moveq   #8,d0
        moveq   #2,d1
        lea     TEXT_SUMMARY_RANK(a4),a0
        move.l  LSTPTR_FRONT_SCREEN(a4),a1
        add.w   MAP_POSX(a4),a1
        move.l  LSTPTR_POST_SCREEN(a4),a2
        add.w   MAP_POSX(a4),a2
        bsr     DRAW_BONUS_TEXT
        move.l  LSTPTR_BACK_SCREEN(a4),a1
        add.w   MAP_POSX(a4),a1
        bsr     DRAW_BONUS_TEXT

;---------------------------------------------
;d1:rank to blit
;a1:Screen address
        moveq   #0,d1
        move.w  RYGAR_CURRENT_RANK(a4),d1
        move.l  LSTPTR_FRONT_SCREEN(a4),a1
        add.w   MAP_POSX(a4),a1
        bsr     SUMMARY_BLIT_RANK
        moveq   #0,d1
        move.w  RYGAR_CURRENT_RANK(a4),d1
        move.l  LSTPTR_BACK_SCREEN(a4),a1
        add.w   MAP_POSX(a4),a1
        bsr     SUMMARY_BLIT_RANK

;---------------------------------------------
; Draw Rank value
        moveq   #0,d0
        move.w  RYGAR_CURRENT_RANK(a4),d0
        addq.w  #1,d0
        mulu    #1000,d0                                ; Rank is * 1000 points
        move.l  d0,TOTAL_RANK_BONUS(a4)
        bsr     Binary2Decimal
        lea     TEXT_RANK_SUM(a4),a1
        moveq   #6-1,d6
        bsr     COPY_DECIMAL

        moveq   #22,d0
        moveq   #2,d1
        lea     TEXT_RANK_SUM(a4),a0
        move.l  LSTPTR_FRONT_SCREEN(a4),a1
        add.w   MAP_POSX(a4),a1
        move.l  LSTPTR_POST_SCREEN(a4),a2
        add.w   MAP_POSX(a4),a2
        bsr     DRAW_BONUS_TEXT
        move.l  LSTPTR_BACK_SCREEN(a4),a1
        add.w   MAP_POSX(a4),a1
        bsr     DRAW_BONUS_TEXT
        rts

DISPLAY_TIMER_BONUS:
	FUNCID	#$98b1def5
;---------------------------------------------
; Draw Timer Summary
        moveq   #8,d0
        moveq   #3,d1
        lea     TEXT_SUMMARY_TIMER(a4),a0
        move.l  LSTPTR_FRONT_SCREEN(a4),a1
        add.w   MAP_POSX(a4),a1
        move.l  LSTPTR_POST_SCREEN(a4),a2
        add.w   MAP_POSX(a4),a2
        bsr     DRAW_BONUS_TEXT
        move.l  LSTPTR_BACK_SCREEN(a4),a1
        add.w   MAP_POSX(a4),a1
        bsr     DRAW_BONUS_TEXT

;---------------------------------------------
; Draw Timer value
        moveq   #0,d0

        move.w  TIME_REMAINING(a4),d0
        lsr.w   #6,d0
                                        ; Seconds value
        ;addq.w #1,d0
        mulu    #100,d0
        move.l  d0,TOTAL_TIME_BONUS(a4)
        bsr     Binary2Decimal


        lea     TEXT_TIMER_SUM(a4),a1
        moveq   #6-1,d6
        bsr     COPY_DECIMAL

        moveq   #22,d0
        moveq   #3,d1
        lea     TEXT_TIMER_SUM(a4),a0
        move.l  LSTPTR_FRONT_SCREEN(a4),a1
        add.w   MAP_POSX(a4),a1
        move.l  LSTPTR_POST_SCREEN(a4),a2
        add.w   MAP_POSX(a4),a2
        bsr     DRAW_BONUS_TEXT
        move.l  LSTPTR_BACK_SCREEN(a4),a1
        add.w   MAP_POSX(a4),a1
        bsr     DRAW_BONUS_TEXT

; Draw Timer Summary
        moveq   #8,d0
        moveq   #3,d1
        lea     TEXT_SUMMARY_TIMER(a4),a0
        move.l  LSTPTR_FRONT_SCREEN(a4),a1
        add.w   MAP_POSX(a4),a1
        move.l  LSTPTR_POST_SCREEN(a4),a2
        add.w   MAP_POSX(a4),a2
        bsr     DRAW_BONUS_TEXT
        move.l  LSTPTR_BACK_SCREEN(a4),a1
        add.w   MAP_POSX(a4),a1
        bsr     DRAW_BONUS_TEXT
        rts

DISPLAY_TOTAL_BONUS:
	FUNCID	#$cf3001b7
; Draw Total Summary
        moveq   #8,d0
        moveq   #5,d1
        lea     TEXT_SUMMARY_TOTAL(a4),a0
        move.l  LSTPTR_FRONT_SCREEN(a4),a1
        add.w   MAP_POSX(a4),a1
        move.l  LSTPTR_POST_SCREEN(a4),a2
        add.w   MAP_POSX(a4),a2
        bsr     DRAW_BONUS_TEXT
        move.l  LSTPTR_BACK_SCREEN(a4),a1
        add.w   MAP_POSX(a4),a1
        bsr     DRAW_BONUS_TEXT

        moveq   #0,d0
        move.w  REPULSE_BONUS_COUNT(a4),d0
        mulu    #200,d0
        move.l  d0,TOTAL_REPULSE_BONUS(a4)
        add.l   TOTAL_RANK_BONUS(a4),d0
        add.l   TOTAL_TIME_BONUS(a4),d0
        move.l  d0,TOTAL_BONUS(a4)
        rts