

WAIT_FOR_BLITTER        MACRO
        tst.b   DMACONR(a5)
.1\@    btst    #6,DMACONR(a5)
        bne.s   .1\@
        ENDM
	

	
LEVEL3_START	MACRO
	bsr    InstallLevel3Int
        move.w #$c030,INTENA(a5)               ; vertb + copper 
ENDM

LEVEL3_STOP	MACRO
	move.w	#4030,INTENA(a5)
	nop
	ENDM
	
LEVEL5_START	MACRO
        move.w #$1800,INTENA(a5)               ; irq5
	move.l	vbroffset(pc),a0
	lea 	irq5(pc),a1 			; Install new irq handler.
	move.l	a1,$74(a0)
        move.w #$9800,INTENA(a5)               ; irq5
ENDM

LEVEL5_STOP	MACRO
        move.w #$1800,INTENA(a5)               ; irq5
	move.l	vbroffset(pc),a0
	lea 	irq5_old(pc),a1 		; Install new irq handler.
	move.l	a1,$74(a0)
ENDM
	

; In = d7=Slot number to free
;      a0=Allocation pointer
FREE_HARDWARE_SPRITE MACRO
        move.w  #-1,2(a0,d7*4)
        ENDM

MARK_SPRITE_END MACRO
                rept    16
                dc.w    $8080
                endr
                ENDM



SET_UPDATE_SCORE MACRO
        add.l   d2,VAL_PLAYER1_SCORE(a4)
        move.w  #-1,UPDATE_SCORE(a4)
        ;addq.w  #1,REPULSE_BONUS_NUM(a4)

        move.l  d0,-(a7)
        moveq   #SND_DESTROY,d0
        bsr     PLAY_SAMPLE
        move.l  (a7)+,d0

        ENDM


FAST_SHIFT      MACRO
        IFC     '','\1'
        FAIL    missing parameter
        MEXIT
        ENDC
        IFC     'BLIT_COPY','\1'
        lea     FAST_SHIFT_BLIT_COPY(a4),a1
        move.w  d0,d3
        and.w   #$f,d3
        move.w  (a1,d3*2),d3
        ENDC
        IFC     'BOMB_MASK','\1'
        lea     FAST_SHIFT_BOMB_MASK(a4),a1
        move.w  d0,d3
        and.w   #$f,d3
        move.w  2(a1,d3*4),d4
        move.w  (a1,d3*4),d3
        ENDC
        IFC     'BLIT_COOKIE','\1'
        lea     FAST_SHIFT_BLIT_COOKIE(a4),a1
        move.w  d0,d3
        and.w   #$f,d3
        move.w  2(a1,d3*4),d4
        move.w  (a1,d3*4),d3
        ENDC
        IFC     'BLIT_AND','\1'
        lea     FAST_SHIFT_BLIT_AND(a4),a1
        move.w  d0,d3
        and.w   #$f,d3
        move.w  2(a1,d3*4),d4
        move.w  (a1,d3*4),d3
        ENDC

        ENDM
        even
	
DISPLAY_GAME_POINTERS MACRO
; Text Gamecode Start
	move.l	#GAMECODE_START,d0
	lea	TEXT_HEX_TO_ASCII(a4),a0
	bsr	HEX_TO_ASCII_MEM
	
	movem.l	d0-d7/a0-a6,-(a7)
	clr.b	LOADINFO_TEXT_XPOS(a4)
	moveq	#0,d6	
	lea	TEXT_HEX_TO_ASCII(a4),a0		
	bsr	DRAW_LOADINFO_LINE
	movem.l	(a7)+,d0-d7/a0-a6
	movem.l	d0-d7/a0-a6,-(a7)
	moveq	#0,d6	
	lea	TEXT_WINUAE_GAMECODE_START(a4),a0		
	bsr	DRAW_LOADINFO_LINE
	movem.l	(a7)+,d0-d7/a0-a6

; Text Gamecode Length
	move.l	#GAMECODE_END-GAMECODE_START,d0
	lea	TEXT_HEX_TO_ASCII(a4),a0
	bsr	HEX_TO_ASCII_MEM
	
	movem.l	d0-d7/a0-a6,-(a7)
	clr.b	LOADINFO_TEXT_XPOS(a4)
	moveq	#0,d6	
	lea	TEXT_HEX_TO_ASCII(a4),a0		
	bsr	DRAW_LOADINFO_LINE
	movem.l	(a7)+,d0-d7/a0-a6
	movem.l	d0-d7/a0-a6,-(a7)
	moveq	#0,d6	
	lea	TEXT_WINUAE_GAMECODE_LENGTH(a4),a0		
	bsr	DRAW_LOADINFO_LINE
	movem.l	(a7)+,d0-d7/a0-a6

; Text TILES Start
	move.l	IMG_TILES,d0
	lea	TEXT_HEX_TO_ASCII(a4),a0
	bsr	HEX_TO_ASCII_MEM
	
	movem.l	d0-d7/a0-a6,-(a7)
	clr.b	LOADINFO_TEXT_XPOS(a4)
	moveq	#0,d6	
	lea	TEXT_HEX_TO_ASCII(a4),a0		
	bsr	DRAW_LOADINFO_LINE
	movem.l	(a7)+,d0-d7/a0-a6
	movem.l	d0-d7/a0-a6,-(a7)
	moveq	#0,d6	
	lea	TEXT_WINUAE_TILES_START(a4),a0		
	bsr	DRAW_LOADINFO_LINE
	movem.l	(a7)+,d0-d7/a0-a6	
	
; Text TILES LENGTH
	move.l	#160000,d0
	lea	TEXT_HEX_TO_ASCII(a4),a0
	bsr	HEX_TO_ASCII_MEM
	
	movem.l	d0-d7/a0-a6,-(a7)
	clr.b	LOADINFO_TEXT_XPOS(a4)
	moveq	#0,d6	
	lea	TEXT_HEX_TO_ASCII(a4),a0		
	bsr	DRAW_LOADINFO_LINE
	movem.l	(a7)+,d0-d7/a0-a6
	movem.l	d0-d7/a0-a6,-(a7)
	moveq	#0,d6	
	lea	TEXT_WINUAE_TILES_LENGTH(a4),a0		
	bsr	DRAW_LOADINFO_LINE
	movem.l	(a7)+,d0-d7/a0-a6	

	
; Text SPRITES Start
	move.l	IMG_SPRITES,d0
	lea	TEXT_HEX_TO_ASCII(a4),a0
	bsr	HEX_TO_ASCII_MEM
	movem.l	d0-d7/a0-a6,-(a7)
	clr.b	LOADINFO_TEXT_XPOS(a4)
	moveq	#0,d6	
	lea	TEXT_HEX_TO_ASCII(a4),a0		
	bsr	DRAW_LOADINFO_LINE
	movem.l	(a7)+,d0-d7/a0-a6
	movem.l	d0-d7/a0-a6,-(a7)
	moveq	#0,d6	
	lea	TEXT_WINUAE_SPRITES_START(a4),a0		
	bsr	DRAW_LOADINFO_LINE
	movem.l	(a7)+,d0-d7/a0-a6
	
	move.l	#269364,d0
	lea	TEXT_HEX_TO_ASCII(a4),a0
	bsr	HEX_TO_ASCII_MEM
	movem.l	d0-d7/a0-a6,-(a7)
	clr.b	LOADINFO_TEXT_XPOS(a4)
	moveq	#0,d6	
	lea	TEXT_HEX_TO_ASCII(a4),a0		
	bsr	DRAW_LOADINFO_LINE
	movem.l	(a7)+,d0-d7/a0-a6
	movem.l	d0-d7/a0-a6,-(a7)
	moveq	#0,d6	
	lea	TEXT_WINUAE_SPRITES_LENGTH(a4),a0		
	bsr	DRAW_LOADINFO_LINE
	movem.l	(a7)+,d0-d7/a0-a6	
	
; Text HARDSPRITES Start
	move.l	HW16_SPRITES,d0
	lea	TEXT_HEX_TO_ASCII(a4),a0
	bsr	HEX_TO_ASCII_MEM
	movem.l	d0-d7/a0-a6,-(a7)
	clr.b	LOADINFO_TEXT_XPOS(a4)
	moveq	#0,d6	
	lea	TEXT_HEX_TO_ASCII(a4),a0		
	bsr	DRAW_LOADINFO_LINE
	movem.l	(a7)+,d0-d7/a0-a6
	movem.l	d0-d7/a0-a6,-(a7)
	moveq	#0,d6	
	lea	TEXT_WINUAE_HARDSPRITES_START(a4),a0		
	bsr	DRAW_LOADINFO_LINE
	movem.l	(a7)+,d0-d7/a0-a6
	
	move.l	#97920,d0
	lea	TEXT_HEX_TO_ASCII(a4),a0
	bsr	HEX_TO_ASCII_MEM
	movem.l	d0-d7/a0-a6,-(a7)
	clr.b	LOADINFO_TEXT_XPOS(a4)
	moveq	#0,d6	
	lea	TEXT_HEX_TO_ASCII(a4),a0		
	bsr	DRAW_LOADINFO_LINE
	movem.l	(a7)+,d0-d7/a0-a6
	movem.l	d0-d7/a0-a6,-(a7)
	moveq	#0,d6	
	lea	TEXT_WINUAE_HARDSPRITES_LENGTH(a4),a0		
	bsr	DRAW_LOADINFO_LINE
	movem.l	(a7)+,d0-d7/a0-a6	
	
; Text MUSIC Start
	move.l	MUSIC_MAINMODULE,d0
	lea	TEXT_HEX_TO_ASCII(a4),a0
	bsr	HEX_TO_ASCII_MEM
	movem.l	d0-d7/a0-a6,-(a7)
	clr.b	LOADINFO_TEXT_XPOS(a4)
	moveq	#0,d6	
	lea	TEXT_HEX_TO_ASCII(a4),a0		
	bsr	DRAW_LOADINFO_LINE
	movem.l	(a7)+,d0-d7/a0-a6
	movem.l	d0-d7/a0-a6,-(a7)
	moveq	#0,d6	
	lea	TEXT_WINUAE_MUSIC_START(a4),a0		
	bsr	DRAW_LOADINFO_LINE
	movem.l	(a7)+,d0-d7/a0-a6
	
	move.l	#176000,d0
	lea	TEXT_HEX_TO_ASCII(a4),a0
	bsr	HEX_TO_ASCII_MEM
	movem.l	d0-d7/a0-a6,-(a7)
	clr.b	LOADINFO_TEXT_XPOS(a4)
	moveq	#0,d6	
	lea	TEXT_HEX_TO_ASCII(a4),a0		
	bsr	DRAW_LOADINFO_LINE
	movem.l	(a7)+,d0-d7/a0-a6
	movem.l	d0-d7/a0-a6,-(a7)
	moveq	#0,d6	
	lea	TEXT_WINUAE_MUSIC_LENGTH(a4),a0		
	bsr	DRAW_LOADINFO_LINE
	movem.l	(a7)+,d0-d7/a0-a6	
	
; Text Samples START
	;move.l	SNDPTR_RYGAR_DISKARM_NOPOWER,d0
	move.l	SNDPTR_RYGAR_PICKUP,d0
	lea	TEXT_HEX_TO_ASCII(a4),a0
	bsr	HEX_TO_ASCII_MEM
	movem.l	d0-d7/a0-a6,-(a7)
	clr.b	LOADINFO_TEXT_XPOS(a4)
	moveq	#0,d6	
	lea	TEXT_HEX_TO_ASCII(a4),a0		
	bsr	DRAW_LOADINFO_LINE
	movem.l	(a7)+,d0-d7/a0-a6
	movem.l	d0-d7/a0-a6,-(a7)
	moveq	#0,d6	
	lea	TEXT_WINUAE_SAMPLES_START(a4),a0		
	bsr	DRAW_LOADINFO_LINE
	movem.l	(a7)+,d0-d7/a0-a6
	
	;move.l	SNDPTR_RYGAR_RYGARWIN,d0
	;sub.l	SNDPTR_RYGAR_DISKARM_NOPOWER,d0
	move.l	#9990,d0
	lea	TEXT_HEX_TO_ASCII(a4),a0
	bsr	HEX_TO_ASCII_MEM
	movem.l	d0-d7/a0-a6,-(a7)
	clr.b	LOADINFO_TEXT_XPOS(a4)
	moveq	#0,d6	
	lea	TEXT_HEX_TO_ASCII(a4),a0		
	bsr	DRAW_LOADINFO_LINE
	movem.l	(a7)+,d0-d7/a0-a6
	movem.l	d0-d7/a0-a6,-(a7)
	moveq	#0,d6	
	lea	TEXT_WINUAE_SAMPLES_LENGTH(a4),a0		
	bsr	DRAW_LOADINFO_LINE
	movem.l	(a7)+,d0-d7/a0-a6	
	
	
	
;TEXT_WINUAE_PARAMS
	
	movem.l	d0-d7/a0-a6,-(a7)
	clr.b	LOADINFO_TEXT_XPOS(a4)
	moveq	#0,d6	
	lea	TEXT_WINUAE_PARAMS(a4),a0		
	bsr	DRAW_LOADINFO_LINE
	movem.l	(a7)+,d0-d7/a0-a6

	movem.l	d0-d7/a0-a6,-(a7)
	clr.b	LOADINFO_TEXT_XPOS(a4)
	moveq	#0,d6	
	lea	TEXT_WINUAE_PRESSFIRE(a4),a0		
	bsr	DRAW_LOADINFO_LINE
	movem.l	(a7)+,d0-d7/a0-a6
	
	move.w	#$007,FIRECOLOUR
	bsr	FIREWAITSR
	ENDM


COPPER_UPPER_PANEL:
	FUNCID	#$9ebb1858
        move.w  #FMODE,(a1)+
        move.w  #7,(a1)+

        move.w  #DIWSTRT,(a1)+  ;
        move.w  #$2799,(a1)+    ;
        move.w  #DIWSTOP,(a1)+  ;
        move.w  #$10a1,(a1)+    ;
        move.w  #DDFSTRT,(a1)+  ;
        move.w  #$0038,(a1)+    ; 3c
        move.w  #DDFSTOP,(a1)+  ;
        move.w  #$00D0,(a1)+    ; a4
	


        move.w  #BPLCON0,(a1)+
        move.w  #$5200,(a1)+

        move.w  #BPLCON2,(a1)+  ; 8
        move.w  #%000000000000000,(a1)+ ; 10

        move.l  MEMCHK9_UPPER_PANEL(a4),d0
        move.w  #BPL0PTL,(a1)+
        move.w  d0,(a1)+
        swap    d0
        move.w  #BPL0PTH,(a1)+
        move.w  d0,(a1)+
        swap    d0
        add.w   #PLAYFIELD_SIZE_X,d0

        move.w  #BPL1PTL,(a1)+
        move.w  d0,(a1)+
        swap    d0
        move.w  #BPL1PTH,(a1)+
        move.w  d0,(a1)+
        swap    d0
        add.w   #PLAYFIELD_SIZE_X,d0

        move.w  #BPL2PTL,(a1)+
        move.w  d0,(a1)+
        swap    d0
        move.w  #BPL2PTH,(a1)+
        move.w  d0,(a1)+
        swap    d0
        add.w   #PLAYFIELD_SIZE_X,d0

        move.w  #BPL3PTL,(a1)+
        move.w  d0,(a1)+
        swap    d0
        move.w  #BPL3PTH,(a1)+
        move.w  d0,(a1)+
        swap    d0
        add.w   #PLAYFIELD_SIZE_X,d0

        move.w  #BPL4PTL,(a1)+
        move.w  d0,(a1)+
        swap    d0
        move.w  #BPL4PTH,(a1)+
        move.w  d0,(a1)+
        swap    d0
        add.w   #PLAYFIELD_SIZE_X,d0

        move.w  #BPLCON1,(a1)+  ;
        move.w  #0,(a1)+        ;
        ;move.w #BPLCON2,(a1)+  ;
        ;move.w #$30,(a1)+
        move.w  #BPLCON3,(a1)+
        move.w  #%100000,(a1)+    ;
        move.w  #BPLCON4,(a1)+
        move.w  #$0000,(a1)+
        move.w  #BPL1MOD,(a1)+
        move.w  #$98,(a1)+              ; a0
        move.w  #BPL2MOD,(a1)+
        move.w  #$98,(a1)+              ; a0

	move.l  #$01060020,(a1)+
	
; Load Upper Palette
        lea     UPPER_PANEL_PAL(a4),a0
        clr.l   (a0)
        move.l  #$180,d2                        ; col reg pointer.
        moveq   #32-1,d7
                ; Upper bits going in
.upper_pal:
        move.w  d2,(a1)+                        ; move in colour reg
        move.w  (a0)+,(a1)+
        addq.w  #2,d2
        dbf     d7,.upper_pal

        move.l  #$01800000,(a1)+
        move.l  #$01820000,(a1)+


        move.l  #$2f0dfffe,(a1)+                ; Start of Middle Row.
        move.l  #$01920fff,(a1)+                ; White in Bitplane 3
        move.l  a1,COPPTR_ACTIVE_PLAYER(a4)
        move.l  #$01860fff,(a1)+                ; Active Player (Flashing Red) P1
        move.l  #$018a0fff,(a1)+                ; Inactive Player (White) P2

        move.l  #$360dfffe,(a1)+
        move.l  #$01860f00,(a1)+                ; Time is Red
        move.l  #$018a0ff0,(a1)+                ; Timer is Yellow

        ;move.l #$01800ff0,(a1)+                ; Timer is Yellow
        rts


COPPER_LOWER_PANEL:
	FUNCID	#$be7d3b65
        move.l  #$fe01ff00,(a1)+			; Line 255
	
	move.w  #BPLCON0,(a1)+
        move.w  #$0200,(a1)+
        move.w  #BPLCON1,(a1)+  ;
        move.w  #0,(a1)+        ;
        move.w  #BPLCON2,(a1)+  ;
        move.w  #$00,(a1)+
        move.w  #BPLCON3,(a1)+
        move.w  #$0020,(a1)+    ;
        move.w  #BPLCON4,(a1)+
        move.w  #$0000,(a1)+
        move.w  #BPL1MOD,(a1)+
        move.w  #$98,(a1)+              ; a0
        move.w  #BPL2MOD,(a1)+
        move.w  #$98,(a1)+              ; a0
	
;-----------------------------
; Load Lower Palette
        lea     LOWER_PANEL_PAL(a4),a0
        clr.l   (a0)
        clr.l   4(a0)
        move.l  #$180,d2                        ; col reg pointer.
        moveq   #31-1,d7
                ; Upper bits going in
.lower_pal:
        move.w  d2,(a1)+                        ; move in colour reg
        move.w  (a0)+,(a1)+
        addq.w  #2,d2
        dbf     d7,.lower_pal
	


;----------------
	
	move.l  #$ff01ff00,(a1)+			; Line 255

        move.w  #FMODE,(a1)+
        move.w  #7,(a1)+

        move.w  #DDFSTRT,(a1)+  ;
        move.w  #$0038,(a1)+    ; 3c
        move.w  #DDFSTOP,(a1)+  ;
        move.w  #$00D0,(a1)+    ; a4

        move.w  #BPLCON0,(a1)+
        move.w  #$5200,(a1)+

        move.w  #BPLCON2,(a1)+  ; 8
        move.w  #%000000000000000,(a1)+ ; 10

        move.l  MEMCHK9_LOWER_PANEL(a4),d0
        move.w  #BPL0PTL,(a1)+
        move.w  d0,(a1)+
        swap    d0
        move.w  #BPL0PTH,(a1)+
        move.w  d0,(a1)+
        swap    d0
        add.w   #PLAYFIELD_SIZE_X,d0

        move.w  #BPL1PTL,(a1)+
        move.w  d0,(a1)+
        swap    d0
        move.w  #BPL1PTH,(a1)+
        move.w  d0,(a1)+
        swap    d0
        add.w   #PLAYFIELD_SIZE_X,d0

        move.w  #BPL2PTL,(a1)+
        move.w  d0,(a1)+
        swap    d0
        move.w  #BPL2PTH,(a1)+
        move.w  d0,(a1)+
        swap    d0
        add.w   #PLAYFIELD_SIZE_X,d0

        move.w  #BPL3PTL,(a1)+
        move.w  d0,(a1)+
        swap    d0
        move.w  #BPL3PTH,(a1)+
        move.w  d0,(a1)+
        swap    d0
        add.w   #PLAYFIELD_SIZE_X,d0

        move.w  #BPL4PTL,(a1)+
        move.w  d0,(a1)+
        swap    d0
        move.w  #BPL4PTH,(a1)+
        move.w  d0,(a1)+
        swap    d0
        add.w   #PLAYFIELD_SIZE_X,d0

	move.l  a1,COPPTR_ROUND_GUIDE_PAL(a4)
        move.l  #$01be0000,(a1)+

        move.w  #$ffdf,(a1)+            ; Wait for line 255
        move.w  #$fffe,(a1)+
        move.w  #$1707,(a1)+            ; Wait Horizontal position
        move.w  #$1900,(a1)+            ; unmask verticla and horizontal
        rts



COPPER_DEBUG_TEXT:
	FUNCID	#$2d1ac2c4
        move.l  #$fc01ff00,(a1)+
	
	move.l	#$01060200,(a1)+;
        move.l  #$01820000,(a1)+
        move.l  #$01060000,(a1)+
        move.l  #$01820707,(a1)+
	
        move.w  #FMODE,(a1)+
        move.w  #7,(a1)+

        move.w  #DIWSTRT,(a1)+  ;
        move.w  #$2781,(a1)+    ;
        move.w  #DIWSTOP,(a1)+  ;
        move.w  #$26c1,(a1)+    ;

        move.l  MEMCHK6_DBGSCREEN(a4),d0
	sub.l	#40,d0
        move.w  #BPL0PTL,(a1)+
        move.w  d0,(a1)+
        swap    d0
        move.w  #BPL0PTH,(a1)+
        move.w  d0,(a1)+

        move.w  #BPLCON1,(a1)+  ;
        move.w  #0,(a1)+        ;
        move.w  #BPLCON2,(a1)+  ;
        move.w  #$00,(a1)+      ;
        move.w  #BPL1MOD,(a1)+
        move.w  #$0,(a1)+               ; a0
        move.w  #BPL2MOD,(a1)+
        move.w  #$0,(a1)+               ; a0

        move.w  #BPLCON0,(a1)+
        move.w  #$1200,(a1)+
	
	move.l  a1,COPPTR_ROUND_GUIDE_PAL(a4)
        move.l  #$01be0000,(a1)+

; Bottom play area (8)                  ; Wait till bottom area
        move.w  #$ffdf,(a1)+            ; Wait for line 255
        move.w  #$fffe,(a1)+
        move.w  #$1707,(a1)+            ; Wait Horizontal position
        move.w  #$ff00,(a1)+            ; unmask verticla and horizontal
        rts


COPPER_PLAYAREA MACRO
        move.w  #FMODE,(a1)+
        move.w  #%0000000000001100,(a1)+
        move.w  #BPLCON0,(a1)+  ; 0             ; This is play area
        move.w  #$0000,(a1)+    ; 2             ; 8 PLanes here
        move.w  #BPLCON1,(a1)+  ; 4
        move.w  #0,(a1)+        ; 6

        move.w  #DDFSTRT,(a1)+  ;
        move.w  #$0038,(a1)+    ; 3c
        move.w  #DDFSTOP,(a1)+  ;
        move.w  #$00b0,(a1)+    ; a4
        ENDM



COL0_RED        MACRO
        move.w  #$0000,$dff106
        move.w  #$0f00,$dff180
        move.w  #$0200,$dff106
        move.w  #$0f00,$dff180
        ENDM

COL0_LIGHT_GREY MACRO
        move.w  #$0000,$dff106
        move.w  #$0ddd,$dff180
        move.w  #$0200,$dff106
        move.w  #$0000,$dff180
        ENDM

COL0_GREY       MACRO
        move.w  #$0000,$dff106
        move.w  #$0888,$dff180
        move.w  #$0200,$dff106
        move.w  #$0000,$dff180
        ENDM

COL0_DARK_GREY  MACRO
        move.w  #$0000,$dff106
        move.w  #$0333,$dff180
        move.w  #$0200,$dff106
        move.w  #$0000,$dff180
        ENDM

COL0_BLUE       MACRO
        move.w  #$0000,$dff106
        move.w  #$000f,$dff180
        move.w  #$0200,$dff106
        move.w  #$000f,$dff180
        ENDM

COL0_YELLOW     MACRO
        move.w  #$0000,$dff106
        move.w  #$0ff0,$dff180
        move.w  #$0200,$dff106
        move.w  #$0ff0,$dff180
        ENDM

COL0_GREEN      MACRO
        move.w  #$0000,$dff106
        move.w  #$00f0,$dff180
        move.w  #$0200,$dff106
        move.w  #$00f0,$dff180
        ENDM

COL0_PURPLE     MACRO
        move.w  #$0000,$dff106
        move.w  #$0f0f,$dff180
        move.w  #$0200,$dff106
        move.w  #$0f0f,$dff180
        ENDM

COL0_BLACK      MACRO
        move.w  #$0000,$dff106
        move.w  #$0000,$dff180
        move.w  #$0200,$dff106
        move.w  #$0000,$dff180
        ENDM


SPRITE_INITIATE         MACRO		
		nop
                move.l  MEMCHK0_SPRITE_HDLVARS(a4),a2   ; 8
                lea     TAB_64(a4),a3           ; 8                     ; 4
                add.w   (a3,d0*2),a2                    ; 18
; Initiate variables (Contructor)
                tst.w   THIS_SPRITE_ACTIVE(a2)
                bmi     .active
		
		move.b	SPRITE_PLOT_TYPE(a1),THIS_SPRITE_PARAMETER+1(a2)
		and.w	#$ff,SPRITE_PLOT_TYPE(a1)

                move.w  #-1,THIS_SPRITE_ACTIVE(a2)
                move.w  d0,THIS_SPRITE_ID(a2)
                move.w  #-1,THIS_SPRITE_STUN_TIMER(a2)				; Set the stun timer	
                move.w  #ENEMY_BOUNDARY_TIME,THIS_SPRITE_BOUNDARY_TIMER(a2)	; Set the boundary timer
; --- Constructor End ;---
                ENDM
		
		
CREATE_ENEMY_LEFT_OR_RIGHT MACRO
                move.w  VPOSR(a5),d2
                swap    d2
                move.b  $bfe801,d2
                lsl.w   #8,d2
                move.b  $bfd800,d2
                and.w   #1,d2
                beq.s   .create_right

.create_left:
                move.w  #SPRITE_DEFAULT_ANIM_RIGHT,THIS_SPRITE_STATUS(a2) ; Create to the right, walk left
                move.w  #SPRITE_MOVE_RIGHT,THIS_SPRITE_DIRECTION(a2)
                exg     d6,d7                                           ; set space position
.create_left_loop:
                move.l  d7,d2
                lea     ROUND_ENEMY_POSITIONS(a4),a0
                lsr.w   #8,d2                           ; Interpolate down
                move.l  (a0,d2*4),d2
                and.l   d5,d2
                bne.s   .create
                sub.w   #16*16,d7
                bra.s   .create_left_loop

.create_right:  move.w  #SPRITE_DEFAULT_ANIM_LEFT,THIS_SPRITE_STATUS(a2)  ; Create to the left, walk right
                move.w  #SPRITE_MOVE_LEFT,THIS_SPRITE_DIRECTION(a2)

.create_right_loop:
                move.l  d7,d2
                lea     ROUND_ENEMY_POSITIONS(a4),a0
                lsr.w   #8,d2                           ; Interpolate down
                move.l  (a0,d2*4),d2
                and.l   d5,d2
                bne.s   .create
                add.w   #16*16,d7
                bra.s   .create_right_loop
		ENDM
		


; Return the current camera position at the left most handle.
; d6=left position, d7=right position
FIND_CAMERA_XPOS        MACRO
                moveq   #0,d6
                moveq   #0,d7
;               move.w  MAP_PIXEL_POSX(a4),d6           ; Pixel Position of current camera
;               sub.w   #256,d6
;               move.w  d6,d7                           ; Get left most position
;               add.w   #(PLAYFIELD_SIZE_X*8),d7        ; Get Right most position
;               lsl.w   #4,d6                           ; Interpolate to 16*16... 4 for speed interpolate
;               lsl.w   #4,d7                           ;
                move.w  CAMERA_X1POS(a4),d6
                move.w  CAMERA_X2POS(a4),d7
                ENDM

FIREWAIT        MACRO
.held           move.w  FIRECOLOUR(a4),$dff180
                btst.b  #7,$bfe001
                beq.s   .held
.release:       move.w  FIRECOLOUR(a4),$dff180
                btst.b  #7,$bfe001
                bne.s   .release
                ENDM

FIREWAITSR
.held           move.w  FIRECOLOUR(a4),$dff180
                btst.b  #7,$bfe001
                beq.s   .held
.release:       move.w  FIRECOLOUR(a4),$dff180
                btst.b  #7,$bfe001
                bne.s   .release
                rts


; Check the current x position in d6 if it is blocked
; In:   d5=1 (right) -1 (left)
;       d6=xposition interpolated
;      d7=ENEMY MASK (i.e ENEMY_DEF_RHINO
; Out: d6=0 (Blocked), d6=+ Unblocked

IS_PATH_BLOCKED MACRO
                lea     ROUND_ENEMY_POSITIONS(a4),a0
                lsr.w   #8,d6                           ; Interpolate down
                sub.w   d5,d6
                move.l  (a0,d6*4),d6
                and.l   d7,d6
                ENDM

; Return 0 in d6 if different.
; In d7=Current platform.  d6=Xposition interpolated
IS_PATH_DIFFERENT       MACRO
        lea     ROUND_PLATFORMS(a4),a0
        lsr.w   #8,d6
        sub.w   d5,d6
        move.l  (a0,d6*4),d6    ; Get platform attribute
	
        and.w   #$f,d7
        lea     PLATFORM_POSITION_TABLE(a4),a0
        move.l  (a0,d7*4),d7
        eor.l   d7,d6          ; this, because below code in fact checks for nibbles =0
        move.l  d6,d5
        not.l   d6
        sub.l   #$11111111,d5
        and.l   #$88888888,d6
        and.l   d5,d6
        sne     d6               ; could have been bne found
        ENDM

UPDATE_SPRITE_PLOT_XPOS MACRO
                move.w  d2,THIS_SPRITE_MAP_XPOS(a2)
                move.w  MAP_PIXEL_POSX(a4),d3
                sub.w   #$100,d3
                lsr.w   #4,d2
                sub.w   d3,d2
                move.w  d2,SPRITE_PLOT_XPOS(a1)
                ENDM

GET_PLAYER_SPRITE_CONTEXT MACRO
                moveq   #SPR_TYPE_PLAYER,d2
                move.l  MEMCHK0_SPRITE_HDLVARS(a4),a0
                lea     TAB_64(a4),a3
                add.w   (a3,d2*2),a0
                ENDM


MOVE_SPRITES    MACRO
                move.l  MEMCHK0_SPRITE_HDLVARS(a4),a2   ; 8
                lea     TAB_64(a4),a3           ; 8                     ; 4
                add.w   (a3,d0*2),a2                    ; 18

;x check
                tst.w   (a2)
                bpl.s   .right
                bmi.s   .left

                nop

.left:          cmp.w   #4,SPRITE_PLOT_XPOS(a1)
                ble.s   .turn
                move.w  d0,d2
                subq.w  #2,d2
                sub.w   d2,SPRITE_PLOT_XPOS(a1)
                bra.s   .animate

.right:         cmp.w   #256+32,SPRITE_PLOT_XPOS(a1)
                bge.s   .turn
                move.w  d0,d2
                subq.w  #2,d2
                add.w   d2,SPRITE_PLOT_XPOS(a1)
                bra.s   .animate

.turn:          not.w   (a2)
                bra.s   .animate

                nop
.animate:
                ENDM



RYGAR_CAROUSEL_CHECK	MACRO
		cmp.w   #98,RYGAR_SANCTUARY_COMMAND(a4)         ; 100 means that Rygar is facing
                beq	.method_set_enemies_disable
                cmp.w   #99,RYGAR_SANCTUARY_COMMAND(a4)         ; 100 means that Rygar is facing
                beq	.method_set_enemies_enable

                tst.w   RYGAR_SANCTUARY_COMMAND(a4)                     ; if not 0 then Rygar is somewhere in the sanctuary
                beq     .not_in_sanctuary

                cmp.w   #RYGAR_FACE,RYGAR_SANCTUARY_COMMAND(a4)         ; 100 means that Rygar is facing
                beq.s   .is_facing
                cmp.w   #101,RYGAR_SANCTUARY_COMMAND(a4)
                bge.s   .is_facing_and_scroll
; anything else means Rygar is in the sanctuary.


.in_carousel:   move.w  RYGAR_SANCTUARY_COMMAND(a4),d1
                lsr.w   #2,d1
                tst.w   SANCTUARY_STAGE(a4)
                bne.s   .right_carousel

.left_carousel: move.w  d1,LEFT_CAROUSEL_POSITION(a4)	
                bra.s   .carousel

.right_carousel:
                move.w  d1,RIGHT_CAROUSEL_POSITION(a4)
                bra.s   .carousel
                nop

.carousel:      move.w  RYGAR_XPOS(a4),d1
                add.w   RYGAR_SANCTUARY_COMMAND(a4),d1          ; This will be a positive number to offset Rygar to.
                move.w  d1,SPRITE_PLOT_XPOS(a1)
                move.w  #RYGAR_CAROUSEL,THIS_SPRITE_STATUS(a2)
                bra.s	.in_sanctuary

.is_facing:     move.w   #RYGAR_FACING,THIS_SPRITE_STATUS(a2)
                bra     .in_sanctuary

.is_facing_and_scroll:
                move.w  RYGAR_SANCTUARY_COMMAND(a4),d3
                move.w  RYGAR_XPOS(a4),d1
                sub.w   #101,d3
                sub.w   d3,d1
                move.w  d1,SPRITE_PLOT_XPOS(a1)
                move.w   #RYGAR_FACING,THIS_SPRITE_STATUS(a2)
                bra     .in_sanctuary
		nop
.in_sanctuary:
		ENDM