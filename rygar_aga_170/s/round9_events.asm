
ROUND9_LIZARD1_XPOS:	equ	$2b0			; Right
ROUND9_LIZARD1_YPOS:	equ	$14c

ROUND9_LIZARD2_XPOS:	equ	$22c			; Left
ROUND9_LIZARD2_YPOS:	equ	$1bc

ROUND9_LIZARD3_XPOS:	equ	$22c			; Left 
ROUND9_LIZARD3_YPOS:	equ	$230

ROUND9_LIZARD4_XPOS:	equ	$2b0			; Right 
ROUND9_LIZARD4_YPOS:	equ	$240

ROUND9_LIZARD5_XPOS:	equ	$22c			; Left 
ROUND9_LIZARD5_YPOS:	equ	$280

ROUND9_LIZARD6_XPOS:	equ	$2b0			; Right 
ROUND9_LIZARD6_YPOS:	equ	$2d0
	
; This is a vertical scrolling event setup when Rygar must start at the bottom 
; and climb up a rope and move to the right.
ROUND9_EVENT_INJECT_LIZARDS:
	nop
	
	clr.w	VERTICAL_COMPENSATE(a4)
		
; Lizard positions enable in the cave.
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#SPR_LIZARD,d0
	move.w	#ROUND9_LIZARD1_XPOS,d1
	move.w	#ROUND9_LIZARD1_YPOS,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	or.w	#5<<8,d3				; Delay interval for lizard tongue attack
	lea	HDL_LIZARD(pc),a0
	bsr	PUSH_SPRITE
	tst.w	d4
	bmi	.max_reached
	move.w	d4,BASE_LIZARD_SPRITE(a4)
	movem.l	(a7)+,d0-d7/a0-a3
	
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#SPR_LIZARD,d0
	move.w	#ROUND9_LIZARD2_XPOS,d1
	move.w	#ROUND9_LIZARD2_YPOS,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	lea	HDL_LIZARD(pc),a0
	bsr	PUSH_SPRITE
	tst.w	d4
	bmi	.max_reached
	movem.l	(a7)+,d0-d7/a0-a3
	
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#SPR_LIZARD,d0
	move.w	#ROUND9_LIZARD3_XPOS,d1
	move.w	#ROUND9_LIZARD3_YPOS,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	lea	HDL_LIZARD(pc),a0
	bsr	PUSH_SPRITE
	tst.w	d4
	bmi	.max_reached
	movem.l	(a7)+,d0-d7/a0-a3
	
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#SPR_LIZARD,d0
	move.w	#ROUND9_LIZARD4_XPOS,d1
	move.w	#ROUND9_LIZARD4_YPOS,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	lea	HDL_LIZARD(pc),a0
	bsr	PUSH_SPRITE
	tst.w	d4
	bmi.s	.max_reached
	movem.l	(a7)+,d0-d7/a0-a3
	
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#SPR_LIZARD,d0
	move.w	#ROUND9_LIZARD5_XPOS,d1
	move.w	#ROUND9_LIZARD5_YPOS,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	lea	HDL_LIZARD(pc),a0
	bsr	PUSH_SPRITE
	tst.w	d4
	bmi.s	.max_reached
	movem.l	(a7)+,d0-d7/a0-a3
	
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#SPR_LIZARD,d0
	move.w	#ROUND9_LIZARD6_XPOS,d1
	move.w	#ROUND9_LIZARD6_YPOS,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	lea	HDL_LIZARD(pc),a0
	bsr	PUSH_SPRITE
.max_reached:	
	movem.l	(a7)+,d0-d7/a0-a3
	
.exit:	rts


	