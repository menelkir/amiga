
ITEM_STRUCT_MAPPOS: equ     4
ITEM_STRUCT_SCRPOSX:        equ     8
ITEM_STRUCT_SCRPOSY:        equ     10
ITEM_STRUCT_FRAMES: equ     12
ITEM_STRUCT_STATE:  equ     16
ITEM_STRUCT_COLLISION:      equ     18
ITEM_STRUCT_MEMOFFSET:      equ     20
ITEM_STRUCT_MEMSAVEBG:      equ     24
ITEM_STRUCT_MODIFY:        equ     28
ITEM_STRUCT_ONSCREEN:       equ     32              ; word

ITEM_RANDOM:	equ	-1


MAX_ITEMS:      equ     16                      ; Max number of items that can be stored
ITEM_SIZE_Y:    equ     32
ITEM_SIZE_X:    equ     32/8
ITEM_PLANES:    equ     5
ITEM_MEMSIZE:   equ     (ITEM_SIZE_X*ITEM_PLANES)*ITEM_SIZE_Y

ITEM_ENTER_SCENE:       equ     1
ITEM_STAYS_SCENE:       equ     0
ITEM_LEAVE_SCENE:       equ     -1

ITEM_BLOCK1:    equ     1250

;ITEM_ASSET:    equ     1250

ITEM_TIMEOUT:   equ     600*50

INIT_ITEMS:     
		FUNCID	#$1d4da17e
		moveq   #MAX_ITEMS-1,d7
                lea     ITEM_STRUCT(a4),a0
.next:          moveq   #ITEM_STRUCT_SIZE-1,d6
                move.l  #"ITEM",(a0)+
                subq.w  #4,d6
.init:          clr.b   (a0)+
                dbf     d6,.init
                dbf     d7,.next
                rts

; a2=Coordinate set (in cmp2 format)
; d2=x1, d3=y1, d4=x2, d5=y2

ITEM_COLLCHECK  MACRO
                moveq   #0,d1
                cmp.w   2(a2),d2
                bgt.s   .coll_exit
                cmp.w   (a2),d4
                blt.s   .coll_exit
                cmp.w   6(a2),d3
                bgt.s   .coll_exit
                cmp.w   4(a2),d5
                blt.s   .coll_exit
                moveq   #-1,d1
.coll_exit:
                ENDM


ITEM_SEARCH_RIGHT_FROM: equ     8


;-----------------------
; *** ITEMS HANDLER ***
;=======================
HDL_ITEMS:      
		FUNCID	#12e19762
		clr.w	ITEM_KILLALL_SET(a4)
		bsr     HDL_ITEMS_ATTRIBS                       ; Decrement the frames timer for each stone.

                moveq   #MAX_ITEMS-1,d7
                moveq   #0,d6                                   ; Allocate this slot with asset number

.loop:          lea     ITEM_LIST(a4),a0
                tst.w   (a0,d6*2)
                bmi.s   .next_slot

                lea     ITEM_VTABLE(a4),a0
                move.l  (a0,d6*4),a0                            ; Get structure pointer for the slot

                tst.w   ITEM_STRUCT_STATE(a0)                       ; Tri-state
                beq.s   .next_slot                              ; DO nothing if 0
                bmi.s   .pop                                    ; Remove if -1
                ;bra.s  .push                                   ; Add if +1

; ITEM_STRUCT_STATE will be 0 if entering.
.push:          moveq   #-1,d2                                  ; Blit to post screen as well.
                tst.w   ITEM_STRUCT_ONSCREEN(a0)            ; Is the stone on screen???
                bmi.s   .next_slot                              ; No... skip the blit.

		moveq	#0,d0
                lea     ITEM_LIST(a4),a1
                move.w  (a1,d6*2),d0
                lsr.w   #8,d0
                lsr.w   #2,d0
                subq.w  #1,d0
                lea     SPRITE_TO_TILE_MERGE_MAP(a4),a1 ; Get the sprite to blit
                move.w  (a1,d0*2),d0

                move.l  ITEM_STRUCT_MEMOFFSET(a0),d1
                bsr     ITEM_BLIT_PUSH                          ; Don't animate if off the screen though!

                move.w  #ITEM_STAYS_SCENE,ITEM_STRUCT_STATE(a0)                     ; Set tri-state to 0
                bra.s   .next_slot

; ITEM_STRUCT_STATE will be -1 if leaving
.pop:
; Need tile number to replace?

                move.l  ITEM_STRUCT_MAPPOS(a0),a1
                cmp.l   #0,a1
                beq.s   .next_slot
                move.w  (a1),d0
                and.w   #%0000001111111111,d0
                move.l  ITEM_STRUCT_MEMOFFSET(a0),d1
                bsr     ITEM_BLIT_POP

                lea     ITEM_LIST(a4),a0
                bsr     POP_ITEM                                ; Here we can push an item at the selected position

                move.w  #-1,(a0,d6*2)                           ;
                bra.s   .next_slot

                nop

.next_slot:     addq.w  #1,d6
                dbf     d7,.loop
.exit:          rts


; Handler for managing the ITEMS attributes.
HDL_ITEMS_ATTRIBS:
                FUNCID	#$db36df71
		moveq   #MAX_ITEMS-1,d7
                moveq   #0,d6                                   ; Allocate this slot with asset number
.loop:          lea     ITEM_LIST(a4),a0
                tst.w   (a0,d6*2)
                bmi     .next_slot
                lea     ITEM_VTABLE(a4),a0
                move.l  (a0,d6*4),a0                            ; Get structure pointer for the slot

; Mandatory object management, we must keep track of the stone if it is onscreen or not.
                move.w  SCROLL_DIRECTION_X(a4),d4
                subq.l  #1,ITEM_STRUCT_FRAMES(a0)           ; Decrement the frames number
                add.w   d4,d4
                sub.w   d4,ITEM_STRUCT_SCRPOSX(a0)

; First we should determine if the stone is onscreen, no point in further checks if off screen.
.screen_check:  clr.w   ITEM_STRUCT_ONSCREEN(a0)
                move.w  ITEM_STRUCT_SCRPOSX(a0),d4
                lea     ITEM_SCREEN_LIMITS(a4),a1
		cmp.w   (a1),d4
		bgt.s   .leave_check
		cmp.w	2(a1),d4
		blt.s	.leave_check
                move.w  #ITEM_LEAVE_SCENE,ITEM_STRUCT_ONSCREEN(a0)          ;-1

; Check here to see if the stone time expired (if collision then it will be 0)
.leave_check:   tst.l   ITEM_STRUCT_FRAMES(a0)                                              ; Have the number of frames
                bpl.s   .rygar_check                                                    ; completed???
                move.w  #ITEM_LEAVE_SCENE,ITEM_STRUCT_STATE(a0)                                     ; Set stone to leaving (-1) state.
                bra     .next_slot

; Probably want to check collision with Rygar here and collect the item if collided.
.rygar_check:   tst.w   ITEM_STRUCT_ONSCREEN(a0)            ; If stone is off screen then no point in
                bmi   	.dont_check_collision                   ; checking collision with disk armour.
                tst.w   ITEM_STRUCT_COLLISION(a0)           ; Is the collision flag already set?
                bmi   	.next_slot                              ; Yes, don't check it again.

                lea     RYGAR_COORDS(a4),a2
                move.l  a2,a3
                move.w  ITEM_STRUCT_SCRPOSX(a0),d2
                move.w  ITEM_STRUCT_SCRPOSY(a0),d3
                ;add.w  #32,d2                                  ; HMMMMMMMMMMMMMMMM....
                add.w   #32,d3
                move.w  d2,d4                                   ; Check a 16x16 boundary from x1/y1
                move.w  d3,d5
                add.w   #16,d4
                add.w   #16,d5
                ITEM_COLLCHECK
                tst.w   d1
                beq.s   .check_diskarm
	
; Collided with Rygar here....	
; d6 contains index to the item that was picked up.
                move.w  #ITEM_ENTER_SCENE,ITEM_STRUCT_STATE(a0)         ; Set item to leaving (-1) state.
                move.w  #-1,ITEM_STRUCT_COLLISION(a0)               	; Set hit flag
                move.l  #-1,ITEM_STRUCT_FRAMES(a0)          		; Cause an expiry check

                lea     ITEM_LIST(a4),a1
                move.w  (a1,d6*2),d4				; d4 now has the item number that was picked up

                move.w  d4,ITEM_POWERS(a4)			
                movem.l d0-d7/a0-a3,-(a7)
                bsr     HANDLE_POWERS
                movem.l (a7)+,d0-d7/a0-a3
		
                moveq   #SND_PICKUP,d0
                bsr     PLAY_SAMPLE
		
.check_diskarm:
                tst.w   RYGAR_DISKARM_STATE(a4)
                beq     .next_slot

		lea     DISKARM_COORDS(a4),a2
                move.l  a2,a3
                move.w  ITEM_STRUCT_SCRPOSX(a0),d2
                move.w  ITEM_STRUCT_SCRPOSY(a0),d3
                add.w   #16,d3
                move.w  d2,d4                                   ; Check a 16x16 boundary from x1/y1
                move.w  d3,d5
                add.w   #16,d4
                add.w   #16,d5

                moveq   #0,d1
                cmp.w   2(a2),d2
                bgt.s   .coll_exit1
                cmp.w   (a2),d4
                blt.s   .coll_exit1
                cmp.w   6(a2),d3
                bgt.s   .coll_exit1
                cmp.w   4(a2),d5
                blt.s   .coll_exit1
                moveq   #-1,d1
.coll_exit1:
                tst.w   d1
                beq.s   .next_slot
; Was it a Question mark?
		lea     ITEM_LIST(a4),a1
                move.w  (a1,d6*2),d4				; d4 now has the item number that was picked up		
		tst.w	ITEM_STRUCT_MODIFY(a0)
		beq.s	.next_slot

; Here we change 
		tst.w	ITEM_HIT(a4)
		bmi	.next_slot
		bsr	CHANGE_ITEM
		
		bra.s	.next_slot

.dont_check_collision:
                bra.s   .next_slot

                nop

.next_slot:     addq.w  #1,d6
                dbf     d7,.loop
.exit:          rts

CHANGE_ITEM:
		cmp.w	#4,ITEM_HIT_INDEX(a4)
		beq.s	.exit
		
		movem.l	d0-d7/a0-a3,-(a7)
		move.w	#-1,ITEM_HIT(a4)
; Restore Tile		
; Clear the Item from the Tile attributes
		move.l  ITEM_STRUCT_MAPPOS(a0),a1
                move.w  (a1),d0
                and.w   #%0000001111111111,d0

; Restore the background
                move.l  ITEM_STRUCT_MEMOFFSET(a0),d1
                bsr     ITEM_BLIT_POP
		movem.l	(a7)+,d0-d7/a0-a3


;Blit new item
		movem.l	d0-d7/a0-a3,-(a7)
; d0 will contain the new item to change to!  Eazzzyyy!!!!

		move.w	FRAME_BG(a4),d0
		and.w	#$3,d0
		lea	ITEM_HIT_LIST(a4),a1			; Get list...
		move.l	(a1,d0*4),a1				; Get 
		
		moveq	#0,d0
		move.w	ITEM_HIT_INDEX,d0
		move.w	(a1,d0*2),d0
		
		lea     ITEM_LIST(a4),a1
		move.w  d0,(a1,d6*2)				; d4 now has the item number that was picked up		
		
		move.l  ITEM_STRUCT_MAPPOS(a0),a1		; Tile position		
		or.w	d0,(a1)
                lsr.w   #8,d0
                lsr.w   #2,d0
                subq.w  #1,d0
		
; This needs to get ITEM_EMBLEM_1, ITEM_EMBLEM_2, ITEM_EMBLEM_3 then ITEM_CROSSPOWER
                lea     SPRITE_TO_TILE_MERGE_MAP(a4),a1 ; Get the sprite to blit
                move.w  (a1,d0*2),d0

                move.l  ITEM_STRUCT_MEMOFFSET(a0),d1
                bsr     ITEM_BLIT_PUSH                          ; Don't animate if off the screen though!
		

		addq.w	#1,ITEM_HIT_INDEX(a4)
		
		moveq	#SND_SWEEP,d0
		bsr	PLAY_SAMPLE
		
		movem.l	(a7)+,d0-d7/a0-a3
.exit:		rts
		
; This places a stone on a platform.
; d0=ITEMS Type to push (i.e. ITEM_EMBLEM1 or ITEM_STARPOWER)
; d1=Screen 16 X Position to appear (0=offscreen left, 20=offscreen right) - indexes to map position
; d2=Screen 16 Y Position to appear (0=top,14=bottom) - indexes to map position
; d3=Frame Time Limit
;
PUSH_ITEM:
		FUNCID	#$13d3dbe0
                movem.l d4-d7/a0-a3,-(a7)
                cmp.w   #MAX_ITEMS,ITEM_PTR(a4)         ; Are all items full?
                beq     .exit

                moveq   #0,d4                                   ; Allocate this slot with asset number
                move.w  ITEM_PTR(a4),d4                 	; It cannot already be in use (because we checked)
                lea     ITEM_LIST(a4),a0
                move.w  d0,(a0,d4*2)                            ; Set slot to be busy
                lea     ITEM_VTABLE(a4),a0
                move.l  (a0,d4*4),a0                            ; Get structure pointer for the slot

; Store x and y positions
		move.l  d3,ITEM_STRUCT_FRAMES(a0)           ; Store number of frames
                move.w  d1,ITEM_STRUCT_SCRPOSX(a0)          	; Store x position of item
                move.w  d2,ITEM_STRUCT_SCRPOSY(a0)          	; Store y position of item
                lsr.w   #4,d1
		lsr.w	#4,d2	
		move.w	d1,d6
		move.w	d2,d7
		
		moveq	#0,d4
                move.w  MAP_PIXEL_POSX(a4),d4
                lsr.w   #4,d4
                move.l  ROUND_TILEMAP(a4),a1            	; Get tilemap pointer
                add.l   d4,a1
                add.l   d4,a1
                sub.w   #$20,a1                                 ; Compensate for temple at start of map

		moveq	#0,d4
                lea     MAP_TABLE_Y1(a4),a2             	; Get the map position to place the item
                move.w  MAP_POSY(a4),d4
                add.l   (a2,d4*4),a1                            ; Add current map tile Y position
                add.l   d1,a1                                   ; Add screen X position
                add.l   d1,a1                                   ;
                add.l   (a2,d2*4),a1                            ; Add screen Y position

                lea     TILE_ATTRIBUTES(a4),a3          	; Tile Attributes pointer
                moveq   #0,d3
	
                move.l  a1,ITEM_STRUCT_MAPPOS(a0)
                or.w    d0,(a1)                 		; Place the stone in the map

		clr.w	ITEM_STRUCT_MODIFY(a0)
		cmp.w	#ITEM_QUESTION,d0
		bne.s	.modify
		move.w	#-1,ITEM_STRUCT_MODIFY(a0)

; Find the screen position to blit to.
.modify:	moveq   #0,d1
                move.w  MAP_PIXEL_POSX(a4),d1
                move.w  d1,d4
                lsr.w   #4,d1
                add.w   d1,d1
                and.w   #%0000000000001111,d4
                add.w   d6,d1                                   ; Add x offset bytes
                add.w   d6,d1
		subq.w	#2,d1
                lea     CANVAS_TABLE_Y2(a4),a1
                add.l   (a1,d7*4),d1                            ; Get screen offset
                move.l  d1,ITEM_STRUCT_MEMOFFSET(a0)

                clr.w   ITEM_STRUCT_COLLISION(a0)           ; clear collision flag
                move.w  #ITEM_ENTER_SCENE,ITEM_STRUCT_STATE(a0)                                                                                                                                                                                 
                addq.w  #1,ITEM_PTR(a4)
.exit:
                movem.l (a7)+,d4-d7/a0-a3
                rts



; d6=item number to pop
POP_ITEM:       
		FUNCID	#$fea4bd1a
		movem.l a0-a1,-(a7)
                lea     ITEM_VTABLE(a4),a0
                move.l  (a0,d6*4),a0                            ; Get structure pointer
                move.l  ITEM_STRUCT_MAPPOS(a0),a1
                cmp.l   #0,a1
                beq.s   .exit
                and.w   #%0000001111111111,(a1)         ; Clear tiles
		clr.w	ITEM_HIT_INDEX(a4)
                subq.w  #1,ITEM_PTR(a4)
                movem.l (a7)+,a0-a1
.exit:  rts



; Blit stone animations when required
;
; d0.w = 16x16 sprite to cookie cut
; d1.l = screen offset to sprite/item
ITEM_BLIT_PUSH:
	FUNCID	#$30509e58
        move.l  d6,-(a7)
        move.l  d7,-(a7)

        ;lea    SPRITE_POINTERS(a4),a1
        move.l  MEMCHK0_SPRITE_POINTERS(a4),a1
        move.l  (a1,d0*8),a2            ; Get sprite address
        move.l  4(a1,d0*8),a3           ; Get mask address
        move.l  #$28000006,d7           ; Default sprite size of 3*2 bytes wide by 32 pixels depth.

; Check to see if we should do the Post screen too.
        move.l  LSTPTR_POST_SCREEN(a4),a1                       ; Screen location Back Buffer
        add.l   d1,a1
        WAIT_FOR_BLITTER
        move.l  #$0fca0000,BLTCON0(a5)          ;
        move.l  #$ffffffff,BLTAFWM(a5)          ;
        move.w  #SPRITE_ASSETS_SIZE_X-2,BLTAMOD(a5)
        move.w  #SPRITE_ASSETS_SIZE_X-2,BLTBMOD(a5)
        move.w  #PLAYFIELD_SIZE_X-2,BLTCMOD(a5)                 ; This changes when it is a POP!  Needs to be modulo of save buffer (0)
        move.w  #PLAYFIELD_SIZE_X-2,BLTDMOD(a5)
        move.l  a3,BLTAPTH(a5)                  ; Load the mask address
        move.l  a2,BLTBPTH(a5)                  ; Sprite address
        move.l  a1,BLTCPTH(a5)                  ; This changes when it is a POP!  Needs to be address of save buffer!
        move.l  a1,BLTDPTH(a5)
        move.w  #(80*64)+1,BLTSIZE(a5)          ; Default to 16x16,BLTSIZE(a5)

        move.l  LSTPTR_FRONT_SCREEN(a4),a1                      ; Screen location Back Buffer
        add.l   d1,a1
        WAIT_FOR_BLITTER
        move.l  #$0fca0000,BLTCON0(a5)          ;               ;
        move.l  #$ffffffff,BLTAFWM(a5)          ;
        move.w  #SPRITE_ASSETS_SIZE_X-2,BLTAMOD(a5)
        move.w  #SPRITE_ASSETS_SIZE_X-2,BLTBMOD(a5)
        move.w  #PLAYFIELD_SIZE_X-2,BLTCMOD(a5)                 ; This changes when it is a POP!  Needs to be modulo of save buffer (0)
        move.w  #PLAYFIELD_SIZE_X-2,BLTDMOD(a5)
        move.l  a3,BLTAPTH(a5)                  ; Load the mask address
        move.l  a2,BLTBPTH(a5)                  ; Sprite address
        move.l  a1,BLTCPTH(a5)                  ; This changes when it is a POP!  Needs to be address of save buffer!
        move.l  a1,BLTDPTH(a5)
        move.w  #(80*64)+1,BLTSIZE(a5)          ; Default to 16x16,BLTSIZE(a5)

        move.l  LSTPTR_BACK_SCREEN(a4),a1                       ; Screen location Back Buffer
        add.l   d1,a1
        WAIT_FOR_BLITTER
        move.l  #$0fca0000,BLTCON0(a5)          ;               ;
        move.l  #$ffffffff,BLTAFWM(a5)          ;
        move.w  #SPRITE_ASSETS_SIZE_X-2,BLTAMOD(a5)
        move.w  #SPRITE_ASSETS_SIZE_X-2,BLTBMOD(a5)
        move.w  #PLAYFIELD_SIZE_X-2,BLTCMOD(a5)                 ; This changes when it is a POP!  Needs to be modulo of save buffer (0)
        move.w  #PLAYFIELD_SIZE_X-2,BLTDMOD(a5)
        move.l  a3,BLTAPTH(a5)                  ; Load the mask address
        move.l  a2,BLTBPTH(a5)                  ; Sprite address
        move.l  a1,BLTCPTH(a5)                  ; This changes when it is a POP!  Needs to be address of save buffer!
        move.l  a1,BLTDPTH(a5)
        move.w  #(80*64)+1,BLTSIZE(a5)          ; Default to 16x16,BLTSIZE(a5)

.exit:  move.l  (a7)+,d7
        move.l  (a7)+,d6
        rts


; d0=tile ID to blit.
; d1=Screen offset.
ITEM_BLIT_POP
	FUNCID	#$52108876
        move.l  d6,-(a7)
        move.l  d7,-(a7)

        move.l  d1,-(a7)
        move.l  a0,-(a7)

; Find Source tile (source tile in d0)
; Fast divide by 16 with remainder
	moveq	#0,d1
	moveq	#0,d2
        move.w  d0,d1
        move.w  d0,d2
        and.w   #$f,d1                          ; d1 = x position
        lsr.w   #4,d2                           ; d2 = Y position

        lea     ASSET_TABLE_Y1(a4),a0                   ; Fast Multiply to Y
        move.l  (a0,d2.w*4),d2                                  ; Bring in the offset

        add.w   d1,d1                                           ; Bytes to words for offset
        move.l  TILE_ASSETS_PTR(a4),a0                  ; Tiles location
        add.l   d1,a0                                           ; Add X Offset
        add.l   d2,a0                                           ; Add Y offset
        move.l  a0,a3

        move.l  (a7)+,a0
        move.l  (a7)+,d1


;a3 needs to point to source


; Check to see if we should do the Post screen too.
        move.l  LSTPTR_POST_SCREEN(a4),a1                       ; Screen location Back Buffer
        add.l   d1,a1
        WAIT_FOR_BLITTER
        move.l  #$09f00000,BLTCON0(a5)          ;
        move.l  #$ffffffff,BLTAFWM(a5)          ;
        move.w  #ASSET_MODULO-2,BLTAMOD(a5)
        move.w  #PLAYFIELD_SIZE_X-2,BLTDMOD(a5)
        move.l  a3,BLTAPTH(a5)                  ; Load the mask address
        move.l  a1,BLTDPTH(a5)
        move.w  #(80*64)+1,BLTSIZE(a5)          ; Default to 16x16,BLTSIZE(a5)

        move.l  LSTPTR_FRONT_SCREEN(a4),a1                      ; Screen location Back Buffer
        add.l   d1,a1
        WAIT_FOR_BLITTER
        move.l  #$09f00000,BLTCON0(a5)          ;               ;
        move.l  #$ffffffff,BLTAFWM(a5)          ;
        move.w  #ASSET_MODULO-2,BLTAMOD(a5)
        move.w  #PLAYFIELD_SIZE_X-2,BLTDMOD(a5)
        move.l  a3,BLTAPTH(a5)                  ; Load the mask address
        move.l  a1,BLTDPTH(a5)
        move.w  #(80*64)+1,BLTSIZE(a5)          ; Default to 16x16,BLTSIZE(a5)

        move.l  LSTPTR_BACK_SCREEN(a4),a1                       ; Screen location Back Buffer
        add.l   d1,a1
        WAIT_FOR_BLITTER
        move.l  #$09f00000,BLTCON0(a5)          ;               ;
        move.l  #$ffffffff,BLTAFWM(a5)          ;
        move.w  #ASSET_MODULO-2,BLTAMOD(a5)
        move.w  #PLAYFIELD_SIZE_X-2,BLTDMOD(a5)
        move.l  a3,BLTAPTH(a5)                  ; Load the mask address
        move.l  a1,BLTDPTH(a5)
        move.w  #(80*64)+1,BLTSIZE(a5)          ; Default to 16x16,BLTSIZE(a5)

.exit:  move.l  (a7)+,d7
        move.l  (a7)+,d6
        rts
