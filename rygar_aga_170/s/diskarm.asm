DISKARM_STANDPOS:       equ     2
DISKARM_JUMPPOS:        equ     8
DISKARM_CROUCHPOS:      equ     14

DISKARM_SPR_RAZOR:      equ     1340
DISKARM_SPR_MID:        equ     1341
DISKARM_SPR_TRAIL:      equ     1342
DISKARM_SPR_CHAIN:      equ     1343
DISKARM_SPR_BLANK:      equ     1364

DISKARM_SPR_RAZOR_CROWN:        equ     1344
DISKARM_SPR_MID_CROWN:          equ     1345

; For this we need to understand
; [+] Which way is Rygar facing?
; [+] What extra powers he has
; [+] Which which direction did he fire? (vertical or horizontal)
; [+] Based on this we then select a plot formation in DISKARM_TABLE(a4)

HDL_DISKARM:
		FUNCID	#$c2d354bc
		
                tst.w   RYGAR_DISKARM_STATE(a4)
                beq     .exit
		
                moveq   #0,d0                   ; Assume Rygar facing left
                move.b  RYGAR_MOVE_DIRECTION(a4),d0                             (0=left, 1=right
		
		tst.w	CURRENT_BUTTON_MODE(a4)			; Two button mode?
		beq.s	.2butts					; Yes
		
		tst.w	JOY_PORT1(a4)				; No... is the joystick moving in any direction?
		beq.s	.circle_attack				; No, so perform a circle attack.
		
.2butts:	cmp.w	#JOY1_UP,JOY_PORT1(a4)
		beq.s	.circle_attack
;
;	If the previous state of RYGAR_ATTACK_UP
;
		move.w	RYGAR_ATTACK_UP(a4),d3			; Get the current attack status
		cmp.w	RYGAR_ATTACK_UP_PREV(a4),d3		; Is it the same as the last one?
		beq.s	.same_attack				; Yes... keep going through frames
								; No, it changed so clean up frames.
		bsr	REST_DISKARM_BACKGROUND
		
.same_attack:
		move.w	d3,RYGAR_ATTACK_UP_PREV(a4)
                tst.w   d3
                beq.s   .no_vertical_attack
.circle_attack:
		
		
                addq.w  #2,d0                           ; Add this as it's a vertical attack

                move.w  RYGAR_CURRENT_POWERS(a4),d1
                and.w   #POWER_SUN,d1                   ; Now tst if Rygar has the Sun power
                beq.s   .no_vertical_attack
                addq.w  #2,d0                           ; Yes, so select vertical Sun power attack

.no_vertical_attack:
                moveq   #0,d1                   ; Assume Rygar has no extra powers
                move.w  RYGAR_CURRENT_POWERS(a4),d1
                and.w   #POWER_CROWN|POWER_STAR,d1      ; 1=star power, 2=crown power, 3=star and crown power

                moveq   #0,d2                   ; Assume Rygar fired upwards
		moveq	#0,d3

                lea     DISKARM_TABLE(a4),a0		
		add.w	(a0,d0*2),a0			; Get facing direction
		
		move.w	(a0,d1*2),d3			; Get formation pointer offset
		
                lea     DISKARM_TABLE(a4),a0
		add.l	d3,a0
		          	
                moveq   #0,d3
                move.w  DISKARM_CURRENT_FRAME(a4),d3
		
                move.w  (a0,d3*2),d3            ; Get position list to display.
                lea     DISKARM_TABLE(a4),a0
		add.l	d3,a0
	
                lea     DISKARM_RESTORE_PTRS(a4),a1
                lea     DISKARM_COORDS(a4),a2           ; Clear disk armour co-ordinates.
                moveq   #-1,d7				; Used for sprite bound box collision.
                rept    8
                move.l  d7,(a2)+
                endr
                lea     DISKARM_COORDS(a4),a2

                tst.w   DISKARM_DISABLE(a4)	; If diskarm disable byte is set then 
                bmi.s   .hit			; it hit an enemy mid way through the frames.
                moveq   #0,d7
                tst.w   (a0)                    ; Are we at the end of the frames?
                beq.s   .plot_armour_part       ; No, keep going
.hit:           

; Somehow I need to restore the blocks from the post screen to the current screen.
		
		bsr	FIXUP_DISKARM_BACKGROUND
		clr.w   DISKARM_CURRENT_FRAME(a4)                       ; frames finished.
                clr.w   RYGAR_DISKARM_STATE(a4)
                clr.w   DISKARM_DISABLE(a4)
		clr.w	DISKARM_RESTORE_FRAMES(a4)
                ; Weapong formation finished here....

                bra.s   .exit d5

.plot_armour_part:
                tst.w   (a0)+                                   ; end of sprites to plot?
                bmi.s   .next                                   ; Yes.

                moveq   #0,d0
                moveq   #0,d1
                moveq   #0,d2

                move.w  RYGAR_XPOS(a4),d1
                add.w   (a0)+,d1                                ; d1 = xpos

                move.w  d1,(a2)+                                ; Store X Cordinate Limit
                move.w  d1,(a2)
                add.w   #16,(a2)+                               ; 16 pixels wide

                move.w  RYGAR_YPOS(a4),d2
                add.w   (a0)+,d2                                ; d2 = ypos
                add.w   DISKARM_VERT_OFFSET(a4),d2                                      ; Vertical Offset

;-------------------------------------------------
                ;add.w  #18,d2          ; FOR Debugging Collision in DiskArmour
;-------------------------------------------------

                move.w  d2,(a2)+                                ; Store Y Coordinate Limit
                move.w  d2,(a2)
                add.w   #16,(a2)+                               ; 16 pixels depth.

                move.w  (a0)+,d0                                ; d0 = sprite number to blit

                move.l  d7,-(a7)
                bsr.s   BLIT_DISKARM
                move.l  (a7)+,d7

                addq.w  #8,a1
                addq.w  #1,d7
                bra.s   .plot_armour_part

.next:         
		addq.w  #1,DISKARM_CURRENT_FRAME(a4) 
		move.w	FRAME_BG(a4),d6
		and.w	#1,d6
		bne.s	.next1
		addq.w  #1,DISKARM_CURRENT_FRAME(a4) 	
.next1:
                move.w  d7,DISKARM_RESTORE_FRAMES(a4)
.exit:          rts


;
; BLIT_DISKARM
;
; Draw 16x16 sprite routine using blitter cookie cutter
;
; In:
;       d0 = Sprite number to draw
;       d1 = xpos
;       d2 = ypos
;       a1 = pointer for restore address

;d5
BLIT_DISKARM:
		FUNCID	#$731912d4
		
                movem.l a0-a2,-(a7)
                tst.w   d1                      ; Plot can't be off screen left
                bmi     .exit
                tst.w   d2                      ; Plot can't be off screen upper
                bmi     .exit

                move.l  a1,a0

                move.l  MEMCHK0_SPRITE_POINTERS(a4),a1
                move.l  (a1,d0*8),a2            ; Get sprite address
                move.l  4(a1,d0*8),a3           ; Get mask address

                move.w  d1,d0                   ; d0 now has xpos
                move.w  d2,d1                   ; d1 now has ypos

                move.w  SCROLL_HPOS(a4),d4
                not.w   d4
                and.w   #$1f,d4
                add.w   d4,d0

.size16:
                FAST_SHIFT BLIT_COOKIE
                GET_SPRITE_COORDS_Y32

                move.l  LSTPTR_CURRENT_SCREEN(a4),a1
		move.l	a1,d5
		
		and.l	#$ffff,d1
                add.l   d1,a1                   ; add Y line offset
                and.l   #$fffe,d0
                add.l   d0,a1                   ; add X word offset
                add.w   MAP_POSX(a4),a1
                subq.w  #4,a1

                move.l  a1,d6
                sub.l   d5,d6

; Store the plot offset so that it can be used to restore background later.
                move.l  (a0),4(a0)              ; Save the old offset for next frame restore.
                move.l  d6,(a0)                 ; Store offset size

                WAIT_FOR_BLITTER
                move.w  d4,BLTCON0(a5)
                move.w  d3,BLTCON1(a5)

                move.l  #$ffff0000,BLTAFWM(a5)      ; Only want the first word
                ;move.w  #$0,BLTALWM(a5)         ; Dump the last word

                move.w  #SPRITE_ASSETS_SIZE_X-4,BLTAMOD(a5)
                move.w  #SPRITE_ASSETS_SIZE_X-4,BLTBMOD(a5)
                move.w  #PLAYFIELD_SIZE_X-4,BLTCMOD(a5)
                move.w  #PLAYFIELD_SIZE_X-4,BLTDMOD(a5)

		move.w	#(16*5)<<6+2,d3
                move.w  d3,(a0)                 ; Store Blit Size

                move.l  a3,BLTAPTH(a5)          ; Load the mask address
                move.l  a2,BLTBPTH(a5)          ; Sprite address
                move.l  a1,BLTCPTH(a5)          ; Destination background
                move.l  a1,BLTDPTH(a5)
                move.w  d3,BLTSIZE(a5)
.exit:          movem.l (a7)+,a0-a2
                rts


REST_DISKARM_BACKGROUND:
		FUNCID	#$fa9ed8d4
		
                movem.l d6-d7/a0-a2,-(a7)
	
		tst.w   ROUND_RESTORE_POST(a4)
                bmi     .exit
                tst.w   VERTSCROLL_ENABLE(a4)
                bmi     .exit
		
                moveq   #0,d7
                move.w  DISKARM_RESTORE_FRAMES(a4),d7
                beq.s   .exit
                lea     DISKARM_RESTORE_PTRS(a4),a0

.loop:          move.l  4(a0),d6
                addq.w  #8,a0

                swap    d6
                tst.w   d6
                beq.s   .next
		;cmp.w	#$1402,d6
		;bne.s	.next
                move.w  d6,d3
                clr.w   d6
                swap    d6

                WAIT_FOR_BLITTER
                move.l  #$09f00000,BLTCON0(a5)  ; Select straigt A-D mode $f0
                move.l  #-1,BLTAFWM(a5)  ; No masking needed
                move.w  #$24,BLTAMOD(a5)                ; Playfield Modulo Src
                move.w  #$24,BLTDMOD(a5)                ; Playfield Modulo Dest

                move.l  LSTPTR_CURRENT_SCREEN(a4),a1
                move.l  LSTPTR_POST_SCREEN(a4),a2

                add.l   d6,a1                   ; a1 = dest
                add.l   d6,a2                   ; a3 = src

                move.l  a2,BLTAPTH(a5)          ; set source address
                move.l  a1,BLTDPTH(a5)          ; set dest address
                move.w  d3,BLTSIZE(a5)  ; boom..... 16x16
.next:          dbf     d7,.loop
.exit:          movem.l (a7)+,d6-d7/a0-a2
                rts



FIXUP_DISKARM_BACKGROUND:
		FUNCID	#$c3014f70
		
		clr.w	ITEM_HIT(a4)
		
                movem.l d6-d7/a0-a2,-(a7)
	
		tst.w   ROUND_RESTORE_POST(a4)
                bmi     .exit
                tst.w   VERTSCROLL_ENABLE(a4)
                bmi     .exit
		
                moveq   #0,d7
                move.w  DISKARM_RESTORE_FRAMES(a4),d7
                beq.s   .exit
                lea     DISKARM_RESTORE_PTRS(a4),a0

.loop:          move.l	(a0),d6
                addq.w  #8,a0

                swap    d6
                tst.w   d6
                beq.s   .next
		;cmp.w	#$1402,d6
		;bne.s	.next
                move.w  d6,d3
                clr.w   d6
                swap    d6

                WAIT_FOR_BLITTER
                move.l  #$09f00000,BLTCON0(a5)  ; Select straigt A-D mode $f0
                move.l  #-1,BLTAFWM(a5)  ; No masking needed
                move.w  #$24,BLTAMOD(a5)                ; Playfield Modulo Src
                move.w  #$24,BLTDMOD(a5)                ; Playfield Modulo Dest

                move.l  LSTPTR_OFF_SCREEN(a4),a1
                move.l  LSTPTR_POST_SCREEN(a4),a2

                add.l   d6,a1                   ; a1 = dest
                add.l   d6,a2                   ; a3 = src

                move.l  a2,BLTAPTH(a5)          ; set source address
                move.l  a1,BLTDPTH(a5)          ; set dest address
                move.w  d3,BLTSIZE(a5)  ; boom..... 16x16
		
.next:          dbf     d7,.loop
.exit:          movem.l (a7)+,d6-d7/a0-a2
                rts
