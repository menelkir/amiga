
ROUND11_LIZARD1_XPOS:	equ	$2b0			; Right
ROUND11_LIZARD1_YPOS:	equ	-$50

ROUND11_LIZARD2_XPOS:	equ	$230			; Left
ROUND11_LIZARD2_YPOS:	equ	-$a4

ROUND11_LIZARD3_XPOS:	equ	$2b0			; Left 
ROUND11_LIZARD3_YPOS:	equ	-$f4

ROUND11_LIZARD4_XPOS:	equ	$22c			; Right 
ROUND11_LIZARD4_YPOS:	equ	-$13a

ROUND11_LIZARD5_XPOS:	equ	$22c			; Left 
ROUND11_LIZARD5_YPOS:	equ	-$1a4

ROUND11_LIZARD6_XPOS:	equ	$2b0			; Right 
ROUND11_LIZARD6_YPOS:	equ	-$1c4

ROUND11_LIZARD7_XPOS:	equ	$22c			; Left 
ROUND11_LIZARD7_YPOS:	equ	-$1e4
	
; This is a vertical scrolling event setup when Rygar must start at the bottom 
; and climb up a rope and move to the right.
ROUND11_EVENT_INJECT_LIZARDS:
	nop
	
	clr.w	VERTICAL_COMPENSATE(a4)
	
; Lizard positions enable in the cave.
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#SPR_LIZARD,d0
	move.w	#ROUND11_LIZARD1_XPOS,d1
	move.w	#ROUND11_LIZARD1_YPOS,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	or.w	#5<<8,d3				; Delay interval for lizard tongue attack
	lea	HDL_LIZARD(pc),a0
	bsr	PUSH_SPRITE
	tst.w	d4
	bmi	.max_reached
	move.w	d4,BASE_LIZARD_SPRITE(a4)
	movem.l	(a7)+,d0-d7/a0-a3
	
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#SPR_LIZARD,d0
	move.w	#ROUND11_LIZARD2_XPOS,d1
	move.w	#ROUND11_LIZARD2_YPOS,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	lea	HDL_LIZARD(pc),a0
	bsr	PUSH_SPRITE
	tst.w	d4
	bmi	.max_reached
	movem.l	(a7)+,d0-d7/a0-a3
	
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#SPR_LIZARD,d0
	move.w	#ROUND11_LIZARD3_XPOS,d1
	move.w	#ROUND11_LIZARD3_YPOS,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	lea	HDL_LIZARD(pc),a0
	bsr	PUSH_SPRITE
	tst.w	d4
	bmi	.max_reached
	movem.l	(a7)+,d0-d7/a0-a3
	
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#SPR_LIZARD,d0
	move.w	#ROUND11_LIZARD4_XPOS,d1
	move.w	#ROUND11_LIZARD4_YPOS,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	lea	HDL_LIZARD(pc),a0
	bsr	PUSH_SPRITE
	tst.w	d4
	bmi.s	.max_reached
	movem.l	(a7)+,d0-d7/a0-a3
	
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#SPR_LIZARD,d0
	move.w	#ROUND11_LIZARD5_XPOS,d1
	move.w	#ROUND11_LIZARD5_YPOS,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	lea	HDL_LIZARD(pc),a0
	bsr	PUSH_SPRITE
	tst.w	d4
	bmi.s	.max_reached
	movem.l	(a7)+,d0-d7/a0-a3
	
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#SPR_LIZARD,d0
	move.w	#ROUND11_LIZARD6_XPOS,d1
	move.w	#ROUND11_LIZARD6_YPOS,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	lea	HDL_LIZARD(pc),a0
	bsr	PUSH_SPRITE
	tst.w	d4
	bmi.s	.max_reached
	movem.l	(a7)+,d0-d7/a0-a3
	
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#SPR_LIZARD,d0
	move.w	#ROUND11_LIZARD7_XPOS,d1
	move.w	#ROUND11_LIZARD7_YPOS,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	lea	HDL_LIZARD(pc),a0
	bsr	PUSH_SPRITE
	
.max_reached:
	movem.l	(a7)+,d0-d7/a0-a3
	
	
.exit:	rts
