
; These are sprite definitions.[anim index]
; Max 32 anim types - 32x4 = 128 bytes
;==================================================================

;
; GET_SPRITE_POINTER Macro
;
; Returns sprite attributes for a given sprite number
; In:
;       d0.w = sprite number to point to
; Out:
;       a0.l = returns sprite ptr structure
; Trash:
;       d0.w
;
GET_SPRITE_POINTER MACRO
        lea     SPRITE_SLOTS(a4),a0     ; Sprites base pointer
        move.l  (a0,d0*4),a0
        add.w   (a0),a0                 ; a0 now has sprite address
        ENDM

;
; GET_SPRITE_OFFSETS Macro
;
; Returns the word offset within bitplane of where to plot a sprite
; In:
;       d0.w = Sprite X Position
;       d1.w = Sprite Y Position
; Out:
;       d0.w = X bytes offset
;       d1.w = Y bytes offset
;
; Trash:
;       a1.l
GET_SPRITE_COORDS_Y32 MACRO
        lea     TAB_CLIP_Y32(a4),a1     ; Load Sprite Blitter Table. (depending on d2)
        move.l  2(a1,d1.w*8),d6         ; Get blit sizes  (BLIT_SIZE.w | INDEX_INTO_SPRITE.w)
        move.w  (a1,d1.w*8),d1          ; Get offset y line in bitplane
        lsr.w   #3,d0
        ENDM


GET_SPRITE_OFFSETS MACRO
        lea     TAB_Y(a4),a1    ; load y table positions
        move.w  (a1,d1.w*2),d1          ; Get offset y line in bitplane
        lsr.w   #3,d0
        ENDM



;
; GET_SPRITE_MASK_POINTERS Macro
;
; Returns pointers to sprite assets
;
; In:
;       d0.w = Sprite asset number to get
;
; Out:
;       a2.l = Pointer to sprite asset data address
;       a3.l = Pointer to sprite asset mask address
;       d0.l = High word = Blitter Modulo, Low word = Blit Size
;
GET_SPRITE_MASK_XPOINTERS MACRO
        move.l  d1,-(a7)
        move.l  a0,-(a7)
        lea     TAB_16(a4),a0
        move.w  (a0,d0*2),d0
        and.l   #$ffff,d0

        ;lea    SPRITE_POINTERS(a4),a0  ; pointer to sprite addresses
        move.l  MEMCHK0_SPRITE_POINTERS(a4),a0
                                                ; and mask addresses
                                                ; 1 long for sprite asset in chip
                                                ; 1 long for sprite mask in chip
        add.l   d0,a0
        movem.l (a0)+,d0-d1/a2-a3               ; get sprite sizes and pointers
        swap    d0
        move.w  d1,d0                           ; d0 now has sizes

        move.l  (a7)+,a0
        move.l  (a7)+,d1
        ENDM


SPRITE_SLOT_SIZE:       equ     768

CREATE_SPRITE_SLOTS:
	FUNCID	#$7d72a90f
        lea     SPRITE_SLOTS(a4),a0
        moveq   #MAX_SPRITES-1,d7
;        move.l  #SPRITE_SLOTS(a4),d0

        move.l  MEMCHK0_SPR_STRUCTURES(a4),d1
.loop:  move.l  d1,(a0)+
        add.l   #SPRITE_SLOT_SIZE,d1
        dbf     d7,.loop
        rts


CMS
CREATE_MIRROR_SPRITES:
	lea	SPRITE_MIRROR_TABLE(a4),a2
.next:	moveq	#0,d0
	moveq	#0,d1
	move.w	(a2)+,d0
	bmi	.exit
	
	move.w	(a2)+,d1
	
; Get source sprite in a0
        move.l  MEMCHK0_SPRITE_POINTERS(a4),a0
        move.l  (a0,d0*8),a0

; Destination sprite in a1
        move.l  MEMCHK0_SPRITE_POINTERS(a4),a1
        move.l  (a1,d1*8),a1

	
	move.l	#(32*5)-1,d7		; 32 pixels depth by
	move.l	#40,d6			; Bitplane length
	
.loop:
	move.l	(a0),d1
	bsr	REVERSE_LONGWORD
	move.l	d1,(a1)
	
	add.l	d6,a0
	add.l	d6,a1
	dbf	d7,.loop
	bra.s	.next
	
.exit:	rts

;
; INIT_SPRITES
;
; This section simply builds up a sprite list from a table
;
;
;
; INIT_SPRITES Sub Routine
;
; Initialises pre-configured sprites such as Jack, ESB, Powerball &
; Bombs
;
; In:
;       None
; Out:
;       None
; Trash:
;       All Registers (excluding a6/a7)
;
INIT_SPRITES:
	FUNCID	#$d36fb8ae
.loop:  cmp.l   #-1,(a1)
        beq.s   .done
        moveq   #0,d0
        moveq   #0,d1
        moveq   #0,d2
        moveq   #0,d3
        move.w  (a1)+,d0                ; sprite number
        move.w  (a1)+,d1                ; xposition
        move.w  (a1)+,d2                ; yposition
        move.w  (a1)+,d3                ; control
        lea     CODE_BASE(pc),a0
        add.l   (a1)+,a0                ; handler
        move.l  a1,-(a7)
        bsr     PUSH_SPRITE

        move.l  (a7)+,a1
        bra.s   .loop
.done:  rts


; Initiate list of free sprites
INIT_SPRITE_FREE_LIST:
	FUNCID	#$d07cf1d6
        lea     SPRITE_FREE_LIST(a4),a0
        moveq   #MAX_SPRITES-1,d7
.loop:  move.w  #-1,(a0)+
        dbf     d7,.loop
        rts



INIT_SPRITE_STRUCTURE:
	FUNCID	#$778df3a6
        move.l  MEMCHK0_SPR_STRUCTURES(a4),a0
        move.l  #SPR_STRUCTURES_LEN,d7
        lsr.l   #3,d7
        subq.w  #1,d7
.clear: clr.l   (a0)+
        clr.l   (a0)+
        dbf     d7,.clear
        rts

; Wipe sprite status attributes

CLEAR_SPRITE_ATTRIBUTES:
	FUNCID	#$0efc0688
        move.l  MEMCHK0_SPRITE_HDLVARS(a4),a0
        moveq   #0,d0
        moveq   #0,d1
        moveq   #0,d2
        moveq   #0,d3
        moveq   #0,d4
        moveq   #0,d5
        moveq   #0,d6
        moveq   #0,d7

        rept    MAX_SPRITES
        movem.l d0-d7,(a0)
        add.l   #32,a0
        movem.l d0-d7,(a0)
        add.l   #32,a0
        movem.l d0-d7,(a0)
        add.l   #32,a0
        movem.l d0-d7,(a0)
        add.l   #32,a0
        endr
        rts



;
; CREATE_SPRITE_ADDRESS_POINTERS Sub Routine
;
; Creates a list of address pointers to each sprite asset in chip ram.
;
; In:
;       None
; Out:
;       None
; Trash:
;       All Registers (excluding a6/a7)
;
;       move.l  LSTPTR_SPR16_ASSETS,a0                  ; a0 pointer to sprite data
;       move.l  MEMCHK2_SPRITE_MASKS,a1         ; a1 pointer to mask data
;       lea     SPR16_AM_PTR,a2                 ; a2 pointer to list to be stored
;       bsr     INIT_SPRITE_POINTERS(a4)

CREATE_SPRITE_ADDRESS_POINTERS:
	FUNCID	#$a4ad9f74
        move.l  #(SPRITE_ASSETS_SIZE_X),d3                      ; number of sprites in row
        mulu    #(SPR16_BITPLANES*SPR16_SIZE),d3                ; size to add per row

        moveq   #0,d0                                           ; Sprite counter
        moveq   #SPRITE_ASSETS_SIZE_Y-1,d5                      ; number of spr16 rows to import
        clr.w   NUM_SPRITE_ASSETS(a4)
.loop_y:
        move.w  #20-1,d6                                        ; Number of sprites in row

.loop_x:

.next:  move.l  a0,(a2)+                                        ; store sprite location
        move.l  a1,(a2)+                                        ; store mask location
        addq.w  #2,a0
        addq.w  #2,a1
        addq.w  #1,d0
        addq.w  #1,NUM_SPRITE_ASSETS(a4)
        dbf     d6,.loop_x

        sub.l   #SPRITE_ASSETS_SIZE_X,a0
        sub.l   #SPRITE_ASSETS_SIZE_X,a1

        add.l   d3,a0
        add.l   d3,a1
        dbf     d5,.loop_y
        rts

;
; CREATE_SPRITE_MASKS
;
; Copies 16x16 sprite assets from IFF file into memory while generating required masks
; and pointers.
;
;       lea     SPRITE_IFF,a0                           ; (i) Sprite Iff data
;       move.l  MEMCHK2_SPRITE_ASSETS,a1        ; (o) Pointer to sprites buffer to store sprites
;       move.l  MEMCHK2_SPRITE_MASKS,a2 ; (o) Pointer to masks buffer to store masks
;       bsr     CREATE_SPRITE_MASKS
CREATE_SPRITE_MASKS:
	FUNCID	#$2e93dfcc
.find   cmp.l   #"BODY",(a0)
        beq.s   .found
        addq.w  #2,a0
        bra.s   .find

.found: addq.l  #8,a0
        move.l  a0,STCPTR_SPRITE_ASSETS(a4)
        move.l  a0,a1
        move.l  #(SPRITE_ASSETS_SIZE_Y*16),d5

; d5 has lines in depth
        moveq   #0,d7

.copy_line:
        moveq   #20-1,d6                        ; words wide

.copy_word:
        ;move.w (a0),(a1)                       ; Source word
        ;move.w SPRITE_ASSETS_SIZE_X*1(a0),SPRITE_ASSETS_SIZE_X*1(a1)   ; Source word
        ;move.w SPRITE_ASSETS_SIZE_X*2(a0),SPRITE_ASSETS_SIZE_X*2(a1)
        ;move.w SPRITE_ASSETS_SIZE_X*3(a0),SPRITE_ASSETS_SIZE_X*3(a1)
        ;move.w SPRITE_ASSETS_SIZE_X*4(a0),SPRITE_ASSETS_SIZE_X*4(a1)

        move.w  (a0),d7
        or.w    SPRITE_ASSETS_SIZE_X*1(a1),d7
        or.w    SPRITE_ASSETS_SIZE_X*2(a1),d7
        or.w    SPRITE_ASSETS_SIZE_X*3(a1),d7
        or.w    SPRITE_ASSETS_SIZE_X*4(a1),d7

        move.w  d7,(a2)                         ; Or'd mask
        move.w  d7,SPRITE_ASSETS_SIZE_X*1(a2)
        move.w  d7,SPRITE_ASSETS_SIZE_X*2(a2)
        move.w  d7,SPRITE_ASSETS_SIZE_X*3(a2)
        move.w  d7,SPRITE_ASSETS_SIZE_X*4(a2)

        addq.w  #2,a0
        addq.w  #2,a1
        addq.w  #2,a2
        dbf     d6,.copy_word

        sub.l   #SPRITE_ASSETS_SIZE_X,a0
        sub.l   #SPRITE_ASSETS_SIZE_X,a1
        sub.l   #SPRITE_ASSETS_SIZE_X,a2

        add.l   #SPRITE_ASSETS_SIZE_X*5,a0
        add.l   #SPRITE_ASSETS_SIZE_X*5,a1
        add.l   #SPRITE_ASSETS_SIZE_X*5,a2
        dbf     d5,.copy_line
.return:        rts


; Destroy all sprite objects
DESTROY_ALL_SPRITES:
	FUNCID	#$2e470b08
        moveq   #0,d0
        move.w  NUM_SPRITES_INPLAY(a4),d0
        subq.w  #1,d0
	bpl.s	.loop
	bra.s	.exit
.loop:  move.l  d0,-(a7)
        bsr     DESTROY_SPRITE
        move.l  (a7)+,d0
        dbf     d0,.loop
.exit:	clr.w	NUM_SPRITES_INPLAY(a4)
        rts

;
; PLOT_ALL_SPRITES
;
; Subroutine responsible for displaying all sprites on screen.
;
;       lea     SPRITE_FREE_LIST(a4),a0
;       bsr     PLOT_ALL_SPRITES
PLOT_ALL_SPRITES:
	bsr	PLOT_HWSPRITES
	bsr	PLOT_BOBSPRITES
	rts



PLOT_BOBSPRITES:
	moveq   #MAX_SPRITES-1,d7
        lea     SPRITE_FREE_LIST(a4),a0
.loop:
        move.l  d7,d0
        tst.w   (a0,d0*2)
        bmi.s   .skip
	
; Get sprite context into a0
	lea     SPRITE_SLOTS(a4),a1     ; 
        move.l  (a1,d0*4),a1
        add.w   (a1),a1                 ; 
	tst.w   (a1)                    ; If the sprite is disabled 
        bmi     .skip			; then skip it.
	move.w  SPRITE_PLOT_TYPE(a1),d3 ; 
	and.w	#$ff,d3
        cmp.w   #SPR_TYPE_ENEMY_HW,d3	; Is this a hardware sprite?
        bge.s	.skip           	; yes...
	IFNE	ENABLE_SPRITE_PERFORMANCE
; This is a bob.
	cmp.w	#1,d0			; Always plot Rygar and his shield
	ble.s	.plot_it
	moveq   #1,d6                           ; PLot mask
        move.l  VPOSR(a5),d3
        and.l   #$1ff00,d3
        cmp.l   #250<<8,d3              ; Greater than line 200?
        ble.b   .plot_it
        move.w  FRAME_BG(a4),d6
        and.w   #1,d6                   ; d6 = 0        d6 = 1
        btst    d6,d3                   ; Alternate plotting odd and even
        beq.s   .skip                   ; sprite numbers.	
	ENDC
	
.plot_it:
	movem.l d0/d7/a0/a6,-(a7)
        bsr     PLOT_SPRITE_ASSET
        movem.l (a7)+,d0/d7/a0/a6
.skip: 	dbf     d7,.loop
	rts
	

; This plots all of the hardware sprites.
PLOT_HWSPRITES:
	moveq   #MAX_SPRITES-1,d7
        lea     SPRITE_FREE_LIST(a4),a0
.loop:
        move.l  d7,d0
        tst.w   (a0,d0*2)
        bmi.s   .skip
	
; Get sprite context into a0
	lea     SPRITE_SLOTS(a4),a1    ; 
        move.l  (a1,d0*4),a1
        add.w   (a1),a1                 ; 
	tst.w	(a1)                 	; If the sprite is disabled 
        bmi     .skip			; then skip it.
	move.w  SPRITE_PLOT_TYPE(a1),d3 ; 
	and.w	#$ff,d3
        cmp.w   #SPR_TYPE_ENEMY_HW,d3	; Is this a hardware sprite?
        blt.s	.skip           	; yes...
.plot_it:
	movem.l d0/d7/a0/a6,-(a7)
        bsr     PLOT_SPRITE_ASSET
        movem.l (a7)+,d0/d7/a0/a6
.skip: 	dbf     d7,.loop
	rts
	

;
; RESTORE_ALL_SPRITES
;
; Restore sprite routine that removes all sprites from screen and
; restores their background from the pre/post buffer.
;
;       lea     SPRITE_FREE_LIST(a4),a0
;       bsr     RESTOTR_ALL_SPRITES
RESTORE_ALL_SPRITES:
	FUNCID	#$c06021bb
        tst.w   ROUND_RESTORE_POST(a4)
        bmi     VERTSCROLL_RESTORE
        tst.w   VERTSCROLL_ENABLE(a4)
        bmi     VERTSCROLL_RESTORE

        moveq   #MAX_SPRITES-1,d7
        lea     SPRITE_FREE_LIST(a4),a0

.loop:  move.l  d7,d0
        tst.w   (a0,d0*2)
        bmi.s   .skip

        movem.l d0/d7/a0/a6,-(a7)
        bsr     REST_SPRITE_BACKGROUND
        movem.l (a7)+,d0/d7/a0/a6
.skip:  ;addq.w #1,d0
        dbf     d7,.loop
        rts



;
; REST_SPRITE_BACKGROUND
;
; Restore background from pre/post buffer over currently displayed sprite
; In:
;       d0 = Sprite number to restore
;
REST_SPRITE_BACKGROUND:
	FUNCID	#$a7d4dba7
        GET_SPRITE_POINTER
        tst.w   (a0)                    ; Hardware sprite if minus
        bmi     .exit

        move.w  SPRITE_PLOT_TYPE(a0),d3 ; Get control word
	and.w	#$ff,d3
        cmp.w   #SPR_TYPE_ENEMY_HW,d3   ; hardware sprite so nothing to restore
        bge     .exit
        move.w  (a0),d0                 ; current sprite number

	move.w  SPRITE_PLOT_YPOS(a0),d1 ; d1 now has ypos
	beq	.exit
	bmi	.exit

        move.l  #$28000006,d7
        move.l  SPRITE_PLOT_BUFFPTR_OLD(a0),d0  ; Restore buffer offset

; Default values for 32x32 sprite.
        moveq   #PLAYFIELD_SIZE_X,d4    ; Modulo Size
        move.w  #((32*5)*64)+3,d1               ; Blit size is 32 Depth

        lea     .enemy_tab(pc),a6
        add.w   (a6,d3*2),a6
        jmp     (a6)

.enemy_tab:
        dc.w    .enemy_default-.enemy_tab               ; 0 Sprite 0
        dc.w    .enemy_default-.enemy_tab               ; 1 Sprite 1 32x32
        dc.w    .enemy_32x16-.enemy_tab			; 2
        dc.w    .enemy_32x64-.enemy_tab			; 3
        dc.w    .enemy_48x48-.enemy_tab			; 4
        dc.w    .enemy_16x16-.enemy_tab			; 5
        dc.w    .enemy_48x16-.enemy_tab			; 6	
        dc.w    .enemy_block32x16-.enemy_tab		; 7	
        dc.w    .enemy_16x16-.enemy_tab			; 8
        dc.w    .enemy_default-.enemy_tab               ; 9 spr type 6 = object
	dc.w	.exit-.enemy_tab			; 10 hidden sprite

.enemy_48x16:
        move.w  #((16*5)*64)+4,d1       ; Adjust blit size
        sub.l   #$14000000,d7           ; Blit only 16 lines
        move.w	#$0008,d7
	bra.s   .enemy_default

.enemy_block32x16:
        move.w  #((16*5)*64)+3,d1       ; Adjust blit size
        sub.l   #$14000000,d7           ; Blit only 16 lines
	bra.s   .enemy_default

.enemy_16x16:
        move.w  #((16*5)*64)+2,d1
        sub.l   #$14000000,d7                           ; Adjust Blit Size
        move.w  #4,d7
        bra.s   .enemy_default

.enemy_32x16:
        move.w  #((16*5)*64)+3,d1       ; Adjust blit size
        sub.l   #$14000000,d7           ; Blit only 16 lines
        bra.s   .enemy_default

.enemy_32x64:
        move.w  #((64*5)*64)+3,d1       ; Adjust blit size
        add.l   #$28000000,d7           ; Blit only 16 lines
        bra.s   .enemy_default

.enemy_48x48:
        ;move.w  #((48*5)*64)+4,d1       ; Adjust blit size
        ;add.l   #$14000002,d7           ; Blit only 16 lines
        move.w  #((64*5)*64)+4,d1       ; Adjust blit size
        add.l   #$28000002,d7           ; Blit only 16 lines
        bra.s   .enemy_default
        nop

        CNOP    0,4

.enemy_default:
        sub.w   d7,d4                   ; Get modulo size
        move.w  d7,d1                   ; Add Y Size to blit
        lsr.w   #1,d1                   ; Number of words wide to blit.
        swap    d7
        add.w   d7,d1                   ; d1 has blit size


.cont:  WAIT_FOR_BLITTER
        move.l  #$09f00000,BLTCON0(a5)  ; Select straigt A-D mode $f0
        move.l  #-1,BLTAFWM(a5)  ; No masking needed
        move.w  d4,BLTAMOD(a5)          ; Playfield Modulo Src
        move.w  d4,BLTDMOD(a5)          ; Playfield Modulo Dest

        move.l  LSTPTR_CURRENT_SCREEN(a4),a1
        move.l  LSTPTR_POST_SCREEN(a4),a3

        swap    d0
        move.w  d0,d1
        clr.w   d0
        swap    d0
        add.l   d0,a1                   ; a1 = dest
        add.l   d0,a3                   ; a3 = src

.no_size:
        tst.w   d1
        beq.s   .exit
	
        move.l  a3,BLTAPTH(a5)          ; set source address
        move.l  a1,BLTDPTH(a5)          ; set dest address
        move.w  d1,BLTSIZE(a5)  ; boom.....

.exit:  rts

EXCEPTION:      move.w  #$f00,FIRECOLOUR(a4)
                bsr     FIREWAITSR
                rts



	

;
; PLOT_SPRITE_ASSET
;
; Draw 16x16 sprite routine using blitter cookie cutter
;
; In:
;       d0 = Sprite number to draw
;
PLOT_SPRITE_ASSET:
	FUNCID	#$ad264118
; if d0 is a minus then it is a hardware sprite.
        lea     SPRITE_SLOTS(a4),a0     ; Sprites base pointer
        move.l  (a0,d0*4),a0
        add.w   (a0),a0                 ; a0 now has sprite address

        move.w  (a0),d0                 ; current sprite number
        bmi     .exit

        addq.w  #1,NUM_PLOT_SPRITES(a4)

        moveq   #0,d2
        move.w  d0,d2                   ; Required if it is a hardware sprite.

        move.l  MEMCHK0_SPRITE_POINTERS(a4),a1
        move.l  (a1,d0*8),a2            ; Get sprite address
        move.l  4(a1,d0*8),a3           ; Get mask address
        move.l  #$28000006,d7           ; Default sprite size of 3*2 bytes wide by 32 pixels depth.

        move.w  SPRITE_PLOT_XPOS(a0),d0 ; d0 now has xpos
        addq.w  #1,d0
        lea     SPRITE_SCREEN_LIMITS_X(a4),a1
	cmp.w   (a1),d0
        blt   	.exit
	cmp.w	2(a1),d0
	bgt	.exit	
        move.w  SPRITE_PLOT_YPOS(a0),d1 ; d1 now has ypos
        bmi	.exit
	beq.s	.zero
	bra.s   .y_ok
.zero:  moveq   #1,d1
.y_ok:
        lea     SPRITE_SCREEN_LIMITS_Y(a4),a1
	cmp.w   (a1),d1
        blt   	.exit
	cmp.w	2(a1),d1
	bgt	.exit	

        move.w  SPRITE_PLOT_TYPE(a0),d3 ; Get control word
	and.w	#$ff,d3
        cmp.w   #SPR_TYPE_ENEMY_HW,d3
        bge     PLOT_HWSPRITE           ; so send it to the copper

;Adjust/compensate scroll position for the sprite
        move.w  SCROLL_HPOS(a4),d4
        not.w   d4
        and.w   #$1f,d4
        add.w   d4,d0

        move.w  d3,d2
        FAST_SHIFT BLIT_COOKIE          ; d0/d3/d4
        GET_SPRITE_COORDS_Y32           ; Get the Y Co-ordinate - Blit sizes go into D6 Trashes d0/d1/a1/d6

        moveq   #0,d5                   ; Add offset to sprite.
        move.w  d6,d5
        add.l   d5,a2
        add.l   d5,a3

        move.w  d7,d6
        move.l  d6,d7                   ; Do size.

        move.l  LSTPTR_CURRENT_SCREEN(a4),a1
        swap    d1
        clr.w   d1
        swap    d1
        add.l   d1,a1                   ; add Y line offset
        and.l   #$fffe,d0
        add.l   d0,a1                   ; add X word offset
        add.w   MAP_POSX(a4),a1
        subq.w  #4,a1                   ; setting this to 4 causes issues!

        lea     .enemy_tab(pc),a6
        add.w   (a6,d2*2),a6
        jmp     (a6)

.enemy_tab:
        dc.w    .enemy_default-.enemy_tab               ; 0 Sprite 0
        dc.w    .enemy_default-.enemy_tab               ; 1 Sprite 1 32x32
        dc.w    .enemy_32x16-.enemy_tab			; 2
        dc.w    .enemy_32x64-.enemy_tab			; 3
        dc.w    .enemy_48x48-.enemy_tab			; 4
        dc.w	.enemy_16x16-.enemy_tab			; 5
	dc.w    .enemy_48x16-.enemy_tab		; 6	
        dc.w    .enemy_block32x16-.enemy_tab		; 7	
        dc.w    .enemy_16x16-.enemy_tab			; 8
        dc.w    .enemy_default-.enemy_tab               ; 9 spr type 6 = object
	dc.w	.exit-.enemy_tab			; 10 hidden sprite
	
.enemy_48x16:
	cmp.w	#18,SPRITE_PLOT_YPOS(a0)
	blt	.exit
	cmp	#$ce,SPRITE_PLOT_YPOS(a0)
	bgt.s	.e48
; Call this if it is a 16x16 sprite.... this is for the crab and stuff...
        sub.l   #$14000000,d7                           ; Adjust Blit Size
.e48:	move.w  #$0008,d7
        bra.s   .enemy_default

.enemy_block32x16:
	cmp.w	#18,SPRITE_PLOT_YPOS(a0)
	blt	.exit
	cmp	#$ce,SPRITE_PLOT_YPOS(a0)
	bgt.s	.enemy_default
; Call this if it is a 16x16 sprite.... this is for the crab and stuff...
        sub.l   #$14000000,d7                           ; Adjust Blit Size
        bra.s   .enemy_default
	
.enemy_16x16:
	cmp.w	#18,SPRITE_PLOT_YPOS(a0)
	blt	.exit
	cmp	#$ce,SPRITE_PLOT_YPOS(a0)
	bgt.s	.e16
; Call this if it is a 16x16 sprite.... this is for the crab and stuff...
        sub.l   #$14000000,d7                           ; Adjust Blit Size
.e16:	move.w  #4,d7
        bra.s   .enemy_default

.enemy_32x16:
	cmp.w	#18,SPRITE_PLOT_YPOS(a0)
	blt	.exit
	cmp	#$ce,SPRITE_PLOT_YPOS(a0)
	bgt.s	.e32	
        sub.l   #$14000000,d7                           ; Adjust Blit Size	
	
; Call this if it is a 16x32 sprite.... this is for the Rhino.
.e32:	add.l   #(PLAYFIELD_SIZE_X*5)*16,a1             ; Index amount into Playfield
        add.l   #(SPRITE_ASSETS_SIZE_X*5)*16,a2         ; Index amount into Sprite
        add.l   #(SPRITE_ASSETS_SIZE_X*5)*16,a3         ; Index amount into Mask
	bra.s   .enemy_default

.enemy_32x64:
; Call this if it is a 64x32 sprite.... this is for the Rhino.
        add.l   #$28000000,d7                           ; Adjust Blit Size
        bra.s   .enemy_default

.enemy_48x48:
; Call this if it is a 16x32 sprite.... this is for the Rhino.
        ;add.l   #(PLAYFIELD_SIZE_X*5)*16,a1             ; Index amount into Playfield
        ;add.l   #(SPRITE_ASSETS_SIZE_X*5)*16,a2         ; Index amount into Sprite
        ;add.l   #(SPRITE_ASSETS_SIZE_X*5)*16,a3         ; Index amount into Mask
        ;add.l   #$14000002,d7                           ; Adjust Blit Size 3 words by 48 pixels
        ;add.l   #(PLAYFIELD_SIZE_X*5)*16,a1             ; Index amount into Playfield
        ;add.l   #(SPRITE_ASSETS_SIZE_X*5)*16,a2         ; Index amount into Sprite
        ;add.l   #(SPRITE_ASSETS_SIZE_X*5)*16,a3         ; Index amount into Mask
        add.l   #$28000002,d7                           ; Adjust Blit Size 3 words by 48 pixels
        bra.s   .enemy_default

        nop

        CNOP    0,4

.enemy_default:
        move.l  a1,d6
        sub.l   LSTPTR_CURRENT_SCREEN(a4),d6

; Store the plot offset so that it can be used to restore background later.
        move.l  SPRITE_PLOT_BUFFPTR(a0),SPRITE_PLOT_BUFFPTR_OLD(a0)
        move.l  d6,SPRITE_PLOT_BUFFPTR(a0)

        WAIT_FOR_BLITTER
        move.w  d4,BLTCON0(a5)
        move.w  d3,BLTCON1(a5)
        move.l  #$ffff0000,BLTAFWM(a5)  ; Only want the first word

        moveq   #SPRITE_ASSETS_SIZE_X,d3  ; Modulo for sprites (40)
        moveq   #PLAYFIELD_SIZE_X,d4      ; Modulo for playfield (40)
        sub.w   d7,d3                     ; Adjust modulo to sprite size (
        sub.w   d7,d4

        move.w  d3,BLTAMOD(a5)
        move.w  d3,BLTBMOD(a5)
        move.w  d4,BLTCMOD(a5)
        move.w  d4,BLTDMOD(a5)

        move.w  d7,d3                   ; Unravel Blit size
        lsr.w   #1,d3                   ; depending on if it is a
        swap    d7                      ; 16x16 sprite or 32x32 sprite.
        add.w   d7,d3

        move.w  d3,SPRITE_PLOT_BUFFPTR(a0)      ; Save Blit Size.

        tst.w   d3
        beq.s   .exit

        move.l  a3,BLTAPTH(a5)          ; Load the mask address
        move.l  a2,BLTBPTH(a5)          ; Sprite address
        move.l  a1,BLTCPTH(a5)          ; Destination background
        move.l  a1,BLTDPTH(a5)
        move.w  d3,BLTSIZE(a5)
.exit:  rts


; d0 = sprite number
ENABLE_SPRITE:
	FUNCID	#$57c832b9
        move.l  d0,-(a7)
        move.l  a0,-(a7)
        GET_SPRITE_POINTER
        clr.b   (a0)
.done:  move.l  (a7)+,a0
        move.l  (a7)+,d0
        rts

; d0 = sprite number
DISABLE_SPRITE:
	FUNCID	#$b5e2c67c
        move.l  d0,-(a7)
        move.l  a0,-(a7)
        GET_SPRITE_POINTER
        move.b  #-1,(a0)
.done:  move.l  (a7)+,a0
        move.l  (a7)+,d0
        rts

;d0=Sprite number
;a0=Sprite contect pointer
GET_SPRITE_CONTEXT:
	FUNCID	#$42d0d6b2
        lea     SPRITE_SLOTS(a4),a0
        move.l  (a0,d0*4),a0
        add.w   (a0),a0
        rts


;
; DESTROY_SPRITE
;
; Destroys all attributes associated with a sprite and frees it.
;
; In:
;       d0.w = Sprite to destroy
; Out:
;       None
; Trash:
;       All registers (Excluding a6/a7)
;
DESTROY_SPRITE:
	FUNCID	#$3790b006
        tst.w   NUM_SPRITES_INPLAY(a4)
        beq     .exit
        bmi     .exit
        subq.w  #1,NUM_SPRITES_INPLAY(a4)

        lea     SPRITE_SLOTS(a4),a0     ; Sprites base pointer
        move.l  (a0,d0*4),a0
        add.w   (a0),a0                 ; a0 now has sprite address

        move.b  #-1,(a0)                ; Disable the sprite.

        lea     SPRITE_FREE_LIST(a4),a0
        move.w  #-1,(a0,d0*2)

        move.l  d0,-(a7)
        move.l  a0,-(a7)
        move.l  MEMCHK0_SPRITE_HDLVARS(a4),a0
        lsl.w   #6,d0                   ; Multiply by 64
        and.l   #$ffff,d0
        add.l   d0,a0
        rept    16                      ; clear 64 bytes
        clr.l   (a0)+
        endr

        move.l  (a7)+,a0
        move.l  (a7)+,d0


.done:
.exit:  rts


;
; ANIMATE_SPRITES
;
; Animate all currently pushed sprites that are in the list.
;
; This routine calls all of the sprite handlers which in turn
; calls the ANIMATE routine for each sprite.
;
ANIMATE_SPRITES:
	FUNCID	#$df9dfaf6
        moveq   #MAX_SPRITES-1,d7
        moveq   #0,d0

.loop:  
	lea     SPRITE_FREE_LIST(a4),a3
	
        ;tst.w   (a3,d0*2)
        move.w	(a3,d0*2),d5
	bmi.s   .sprite_disabled        ; but was destroyed.

        lea     SPRITE_SLOTS(a4),a0
        move.l  (a0,d0*4),a0
        move.l  a0,a1
        add.w   (a0),a1
        tst.w   (a1)
        bmi.s   .sprite_disabled
        move.l  2(a0),a2                ; Sprite handler (*** SHOULD LOOK UP
                                        ; THE SPRITE INDEX AND GET THE HANDLER)
	
	cmp.w	#2,d0			; Always animate Rygar, and the Carousels.
	ble.s	.mandatory_sprites
	cmp.w	#SPR_REAPER,d5		; If it's the reaper then it must animate.
	beq.s	.mandatory_sprites
	tst.w	PAUSE_ENEMIES(a4)
	bmi.s	.sprite_disabled
.mandatory_sprites:
; a2=Sprite Handler
        movem.l d0/d7/a3,-(a7)
	cmp.b	#$4e,(a2)		; This is a #failsafe for the game crashing.
	bne.s	.skip
        jsr     (a2)                    ; Do the sprite handler
.skip:	movem.l (a7)+,d0/d7/a3

.sprite_disabled:
        addq.w  #1,d0
        dbf     d7,.loop
	rts
	
	
	

; 	d0 = sprite number animate in sprite list (player 1 is 0)
; 	d1 = Type of animation ie. (0 standing, 1 walk left, 2 walk right)	
RESET_SPRITE_FRAMES:
	FUNCID	#$6b371fd5
	movem.l	a0-a1,-(a7)
	lea 	SPRITE_SLOTS(a4),a0
	move.l	(a0,d0*4),a0		; PLR_RYGAR
; a0 points to sprite base.	
		
	move.l	a0,a1
	add.w	(6,a0,d1*2),a0		; a0 now pointing to base animation set
	add.w	(a0)+,a1		; a1 points to variables
	clr.w	(a1)			; Clear frame
	movem.l	(a7)+,a0-a1
	rts

;
; ANIMATE
;
; The sprite handler is called with the sprite to work on in d0
; Then ANIMATE is called from the handler with the animate type in d1
;
; In
; 	d0 = sprite number animate in sprite list (player 1 is 0)
; 	d1 = Type of animation ie. (0 standing, 1 walk left, 2 walk right)
; Out:
;	d6 = -1 = End of sprite loop reached
;
ANIMATE:	
		FUNCID	#$b4361446
		move.l	d0,-(a7)
		move.l	d1,-(a7)
	
		lea 	SPRITE_SLOTS(a4),a0
		move.l	(a0,d0*4),a0		; PLR_RYGAR
; a0 points to sprite base.	
		
		move.l	a0,a1
		move.l	a0,a2
		move.l	a0,a3
		
		add.w	(6,a0,d1*2),a0		; a0 now pointing to base animation set

		add.w	(a0)+,a1		; a1 points to variables
		add.w	(a0)+,a2		; a2 points to frames
		add.w	(a0)+,a3		; a3 points to speed
		
		lea 	SPRITE_SLOTS(a4),a0		
		move.l	(a0,d0*4),a0
		add.w	(a0),a0			; a0 points to global sprite vares
		
;----		
		move.w	(a1),d1			; Current frame
		move.w	(a3),d2			; Current speed
		
		moveq	#0,d6
		move.w	(a2,d1*2),d3		; Store the current frame

; need to compare $fffe here for play sprite once.
; if true then we need to disable and pop the sprite from the list.
; Are we at the end of the loop?
		bpl.s	.cont
; Yes we are, reset loop counter back to zero.
		clr.w	(a1)			; reset frame from start		
		addq.w	#1,d3			; is value $fffe->$ffff or $ffff->$0000?
		beq.s	.end			; If not zero then do not set end of animation flag.
		moveq	#-1,d6			; Was $fffe (-2) so set end flag.
		
.end:		moveq	#0,d1
		move.w	(a2,d1*2),d3		; 		

; Has the speed reached the set speed?
.cont:		move.w	d3,(a0)			; Set Frame
		cmp.w	2(a1),d2		; speed reached?  next frame
		bne.s	.loopdone
		move.w	#-1,2(a1)
		addq.w	#1,(a1)			; Next frame		
.loopdone:	addq.w	#1,2(a1)		; Increase speed.
.exit		
		move.l	(a7)+,d1
		move.l	(a7)+,d0

		rts


;
; CAST_SPRITE
;
; Adds a sprite in the sprite list
;
; In:
;       d0 = Sprite type (0, bombjack, 1=MUMMY
;       d1 = start xpos
;       d2 = start ypos
;       d3 = sprite control word to set
;       a0 = offset address of handler to assign
;
; Out:
;       d4 = Allocated Sprite number (-1 if not enough sprites) available.
PUSH_SPRITE:
	FUNCID	#$93e013fe
	moveq	#-1,d4
        cmp.w   #MAX_ACTIVE_SPRITES-1,NUM_SPRITES_INPLAY(a4)                        ; Sprites may be pushed
        beq     .exit
        move.l	d6,-(a7)
	addq.w  #1,NUM_SPRITES_INPLAY(a4)
        
	lea     SPRITE_FREE_LIST(a4),a1

        moveq   #0,d4
        moveq   #-1,d5
.spr1:  
	cmp.w   (a1)+,d5                ; Find next spare sprite
	beq.s   .spare                  ; In free list.
	addq.w  #1,d4                   ; Slot count!
	cmp.w   (a1)+,d5                ; Find next spare sprite
	beq.s   .spare                  ; In free list.
	addq.w  #1,d4                   ; Slot count!
	bra.s   .spr1

.spare:
        move.w	d0,-(a1)
        lea	SPRITE_DEF_BASE(a4),a1
        lea     SPRITE_DEFS(a4),a2
        add.w   (a2,d0*2),a1            ; CHECK THIS FOR OVERFLOW.
; a1 now points to source

        lea     SPRITE_SLOTS(a4),a2         ; Pointer to available sprite list
        move.l  (a2,d4*4),a2
; a2 now points to available sprite space.
; copy the sprite to a2

        move.l   #$80808080,d6
.copy_sprite
        REPT 4
            move.l  (a1)+,d5
            cmp.l   d6,d5
            beq.s   .copy_done
            move.l  d5,(a2)+
        ENDR
        bra.s   .copy_sprite

.copy_done:
; a2 now has copy of the sprite data.
        lea     SPRITE_SLOTS(a4),a1         ; Pointer to available sprite list
        move.l  (a1,d4*4),a1

; a1 now back at the head of the sprite data.

        move.l  a0,2(a1)                ; Save the sprite handler address
        add.w    (a1),a1
        movem.w  d0-d3,(a1)                 ; Initialize Sprite number (unused)
        move.l	(a7)+,d6
.exit:  rts
 	