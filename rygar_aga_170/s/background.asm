MAX_GRADIENT_LINES:     equ     50
COPPER_GRADIENT_SIZE:   equ     2048

BG_SCROLL_NORMAL:	equ	0
BG_SCROLL_SUNSET:	equ	1
BG_SCROLL_MOON:		equ	2
BG_SCROLL_CLOUDS:	equ	3

INIT_GRADIENTS:
	FUNCID	#$27f596ea
	movem.l	a0-a1,-(a7)
	lea	COPPER_GRADIENTS_TABLE(a4),a1
	move.l	MEMCHK2_COPPER2(a4),a0
	rept	16
	move.l	a0,(a1)+
	add.l	#COPPER_GRADIENT_SIZE,a0
	endr
	movem.l	(a7)+,a0-a1
	rts

CFG
CREATE_FADE_GRADIENTS:
	FUNCID	#$ce89c506
        movem.l d0-d7/a0-a3,-(a7)
        lea     COPPER_GRADIENTS_TABLE(a4),a2
	moveq   #15,d7

.gradients:
        move.l  (a2)+,a0
        movem.l d0-d7/a0-a3,-(a7)
        bsr.s   .coploop
        movem.l (a7)+,d0-d7/a0-a3
        dbf     d7,.gradients
        bra.s   .done

        ; Do copper as well....

.coploop:
        cmp.w   #COP1LCH,(a0)			; End of the copper list?
        beq.s   .exit
	
	cmp.w	#$106,(a0)
	beq.s	.colour
	addq.w	#4,a0
	bra.s	.coploop
	
.colour:
	moveq   #0,d1
        move.w  6(a0),d2
        bsr     COLOUR_INTERPOLATE
        move.w  d0,6(a0)
	addq.w	#4,a0
        bra.s   .coploop
.done:  movem.l (a7)+,d0-d7/a0-a3
.exit:	rts




; This called each frame....
FADE_BACKGROUND:
        tst.w   BACKGROUND_TRANSITION_FRAMES(a4)
        beq.s   .exit
	bmi.s	.exit

	;bsr	FIREWAITSR
	
	tst.w	FADE_DELAY(a4)
	bne.s	.skip
	
	move.w	#3,FADE_DELAY(a4)
        subq.w  #1,BACKGROUND_TRANSITION_FRAMES(a4)
	addq.w	#1,FADE_SPRITE_INDEX(a4)
.skip:
	subq.w	#1,FADE_DELAY(a4)
        bsr     BACKGROUND_TRANS_TO_BLACK                       ; Modify all copper palette registers.
        lea     BACKGROUND_TRANSITION_PALETTE(a4),a0
        bsr     LOAD_BACKGROUND_PALETTE                         ; Changes background palette colour
;---- Repoint Copper registers here ---

	moveq	#0,d0
	move.w	BACKGROUND_TRANSITION_FRAMES(a4),d0
	not.w	d0
	and.w	#$f,d0
	lea	COPPER_GRADIENTS_TABLE,a0
	move.l	(a0,d0*4),d0
	move.l	COPPTR_GRADIENT_START(a4),a0
	move.l	d0,DEBUG_GRADIENT
	
	move.w	d0,6(a0)
	swap	d0
	move.w	d0,2(a0)
	
	tst.w	INIT_END_SEQUENCE(a4)
	bmi.s	.exit
	
        tst.w   BACKGROUND_TRANSITION_FRAMES(a4)                ; Stop background blits for speed
        bne.s   .exit
        move.l  MEMCHK1_FRONT_BSCROLL(a4),a0
        bsr     CLEAR_BG_BITPLANES
        clr.w   BACKGROUND_ENABLE(a4)
.exit:  rts



; Call this 16 times only to fade to black.
BACKGROUND_TRANS_TO_BLACK:
	lea     PAL_RYGAR_BG(a4),a0                     ; Source colour
        lea     BACKGROUND_TRANSITION_PALETTE(a4),a2
        lea     32(a0),a3

        moveq   #0,d7
        move.w  BACKGROUND_TRANSITION_FRAMES(a4),d7

.next:  move.w  (a0)+,d2
        moveq   #0,d1
        bsr     COLOUR_INTERPOLATE
        move.w  d0,(a2)+
        cmp.l   a0,a3
        bne.s   .next
.exit:	rts


        CNOP    0,4

; a3 = screen pointer.
COPY_BG_TO_SCREEN:
	FUNCID	#$40e0583a
        tst.w   BACKGROUND_ENABLE(a4)
        beq     .exit
	
	move.l	a3,a1
	
	move.w	BG_SCROLL_TYPE(a4),d3
	add.w	d3,d3
	move.w	.jump_table(pc,d3.w),d3
	jmp	.jump_table(pc,d3.w)
	
.jump_table:
	dc.w	.do_normal-.jump_table
	dc.w	.do_sunset-.jump_table
	dc.w	.do_moon-.jump_table
	dc.w	.do_clouds-.jump_table

	CNOP	0,4

; Normal scrolling done here	
.do_normal:
	move.l  STCPTR_BACKGROUND(a4),a0
	move.l	a3,a1
        moveq   #BITPLANE3,d3
        bsr     BLIT_NORMALBG

        move.l  STCPTR_BACKGROUND(a4),a0
	move.l	a3,a1
        moveq   #BITPLANE2,d3
        bsr     BLIT_NORMALBG

        move.l  STCPTR_BACKGROUND(a4),a0
	move.l	a3,a1
        moveq   #BITPLANE1,d3
        bsr     BLIT_NORMALBG
        bra     .exit

	CNOP	0,4
	
; Moon Blitting done here	
.do_moon:
        move.l  STCPTR_BACKGROUND(a4),a0
	move.l	a3,a1
        moveq   #BITPLANE1,d3
        bsr     BLIT_MOON

        move.l  STCPTR_BACKGROUND(a4),a0
        move.l	a3,a1
        moveq   #BITPLANE2,d3
        bsr     BLIT_MOON

	moveq	#0,d6
        move.l  STCPTR_MOUNTAINS(a4),a0
        move.l	a3,a1
        moveq   #BITPLANE3,d3
        bsr     BLIT_NORMALBG
	bra.s	.exit

	CNOP	0,4
	
; Parallax Clouds done here	
.do_clouds:
	move.l  STCPTR_BACKGROUND(a4),a0
	move.l	a3,a1
	lea	CLOUDS_CONFIG(a4),a3

	moveq	#8-1,d6				; 8 layers.

.parallax:
	move.l	(a3)+,d0				; X Offset
	clr.w	d0
	swap	d0
	cmp.w	#$500,d0			; 1600-320 = 1280 ($500)...loop clouds to start
	blt.s	.split
	sub.w	#$500,d0
.split:
	move.l	(a3)+,d2			; Start Y of blit.
	move.l	(a3)+,d5			; Length of blit
	move.l	(a3)+,d3			; Number of bitplanes
	
.do_bitplane:
	movem.l	d0-d7/a0-a3,-(a7)		; Copy each bitplane
	bsr	BLIT_CLOUDS
	movem.l	(a7)+,d0-d7/a0-a3	
	dbf	d3,.do_bitplane
	
	dbf	d6,.parallax			; Load next parallax layer.
	bra	.exit
	
	CNOP	0,4
	
; Sunset Blitting Done Here.
.do_sunset:
        move.l  STCPTR_BACKGROUND(a4),a0
	move.l	a3,a1
        moveq   #BITPLANE1,d3
        bsr     BLIT_SUNSET

        move.l  STCPTR_BACKGROUND(a4),a0
	move.l	a3,a1
	moveq   #BITPLANE2,d3
        bsr     BLIT_SUNSET

        move.l  STCPTR_MOUNTAINS(a4),a0
	move.l	a3,a1
	moveq   #BITPLANE3,d3
        bsr     BLIT_NORMALBG
.exit:  rts


	CNOP	0,4


; d2=offset into scene to copy...
; d3=plane to copy
; 1024 size wide (128 bytes wide) (64 words wide)
BLIT_NORMALBG:
	FUNCID	#$79ca529f
        move.l  d3,d4
        addq.w  #5,d3                   ; 5th bitplane to start copy
        move.l  (a1,d3.w*4),a1          ; index in 5 bitplanes

	move.w	BG_SCROLL_TYPE(a4),d5	
	add.w	d5,d5
	move.w	.jump_table(pc,d5.w),d5
	jmp	.jump_table(pc,d5.w)
	
.jump_table:
	dc.w	.do_normal-.jump_table
	dc.w	.do_sunset-.jump_table
	dc.w	.do_moon-.jump_table

	CNOP	0,4
	
.do_normal:
	move.l  #(BG_SIZE_X*2)+(BG_SIZE_X-PLAYFIELD_SIZE_X)+2,d5        ; default to normal BG modulo
        lea     BG_BITPLANE_INDEX(a4),a2
        add.w   (a2,d4.w*2),a0
        bra.s   .do_blit
	
	CNOP	0,4
.do_sunset:
        move.l  #(BG_SIZE_X-PLAYFIELD_SIZE_X)+2,d5
        add.w   #(PLAYFIELD_SIZE_X*5)*MOUNTAINS_POSY,a1
	bra.s	.do_blit
	
	CNOP	0,4
.do_moon:
        move.l  #(BG_SIZE_X-PLAYFIELD_SIZE_X)+2,d5
        add.w   #(PLAYFIELD_SIZE_X*5)*(MOUNTAINS_POSY+2),a1
	bra.s	.do_blit
	
	nop
	
.do_blit:
;d2=source modulo (128*2)
;d3=dest modulo (40*4)
;a0=source
;a1=dest
	moveq	#0,d0
        move.w  MAP_OFFSET_BG(a4),d0    ; Map off set in pixels
        add.w   SCROLL_HPOS(a4),d0      ; Compensate on HSCROLL
	
        move.w  d0,d1
        lsr.w   #3,d0                   ; Number of words in.
        add.l   d0,a0                   ; add to map source
        not.w   d1
        and.w   #$f,d1
        lsl.w   #8,d1
        lsl.w   #4,d1
        or.w    #$9f0,d1

        lea     CANVAS_TABLE_Y3(a4),a2
        moveq   #0,d0
        move.w  BLIT_BG_START(a4),d0
        add.l   (a2,d0.w*4),a1                  ; fast add Y start for round
        subq.w  #2,a1				; a1 is destination

        WAIT_FOR_BLITTER
        move.w  d1,BLTCON0(a5)
        move.w  #0,BLTCON1(a5)
        move.w  d5,BLTAMOD(a5)                  ; 128 - Byte copy length(40) = 88
        move.w  #(40*4)+2,BLTDMOD(a5)
        move.l  #$ffffffff,BLTAFWM(a5)
        move.l  a0,BLTAPTH(a5)

        move.l  a1,BLTDPTH(a5)
        moveq   #0,d0
        move.w  BLIT_BG_LENGTH(a4),d0
        lsl.w   #6,d0                                           ; Add length to blit
        add.w   #19,d0                                          ; width in words to blit
        move.w  d0,BLTSIZE(a5)
        rts

        CNOP    0,4
	
; d0=Offset pixels
; d2=Y Offset to add
; d5=Blit Length
;--------------------------------------
BLIT_CLOUDS:        
	move.l  d3,d4
        addq.w  #5,d3                   ; 5th bitplane to start dest copy
        move.l  (a1,d3.w*4),a1          ; a1 = dest
        lea     BG_BITPLANE_INDEX(a4),a2
        add.w   (a2,d4.w*2),a0		; a0 = source
	
; Destination bitplane

	add.w   SCROLL_HPOS(a4),d0      ; Compensate on HSCROLL
        move.w  d0,d1
        lsr.w   #3,d0                   ; Number of words in.
        add.l   d0,a0                   ; add to map source
        not.w   d1
        and.w   #$f,d1
        lsl.w   #8,d1
        lsl.w   #4,d1
        or.w    #$9f0,d1

; Add the destination Y Offset
        lea     CANVAS_TABLE_Y3(a4),a2
        move.l  (a2,d2.w*4),d2                  ; 18

        add.l   d2,a1                           ; 8 Start of Y Blit in dest
        move.l  d2,d3                           ; 4 d2 & d3=$c8
        add.l   d2,d2                           ; 8 d3 =
        add.l   d3,d2                           ; 8 multiply by 3
        add.l   d2,a0                           ; 8 Start of Y blit in source
;                                               ; 46
        subq.w  #2,a1

        WAIT_FOR_BLITTER
        move.w  d1,BLTCON0(a5)
        move.w  #0,BLTCON1(a5)
        move.w  #562,BLTAMOD(a5)                  ; 1600/8 = 200 bytes * 3 bitplanes = 600-38 (19 words) = 562		width = 600 
        move.w  #(40*4)+2,BLTDMOD(a5)		   ; 38 (19 words) + 40*4+2) = 200 bytes (width)			width = 200
        move.l  #$ffffffff,BLTAFWM(a5)
        move.l  a0,BLTAPTH(a5)
        move.l  a1,BLTDPTH(a5)
	
        lsl.w   #6,d5                                           ; Add length to blit
        add.w   #19,d5                                          ; width in words to blit
        move.w  d5,BLTSIZE(a5)
        rts

        CNOP    0,4

; d2=offset into scene to copy...
; d3=plane to copy
; 1024 size wide (128 bytes wide) (64 words wide)
BLIT_SUNSET:
	FUNCID	#$3c21c370
        move.l  d3,d4
        addq.w  #5,d3                   ; 5th bitplane to start copy
        lsl.w   #2,d3
        move.l  (a1,d3),a1              ; index in 5 bitplanes

        lea     BG_SUNSET_INDEX(a4),a2
        add.w   (a2,d4.w*2),a0

;d2=source modulo (128*2)
;d3=dest modulo (40*4)
;a0=source
;a1=dest
        moveq   #0,d0
        add.w   SCROLL_HPOS(a4),d0      ; Compensate on HSCROLL

        move.w  d0,d1
        lsr.w   #3,d0                   ; Number of words in.
        add.l   d0,a0                   ; add to map source
        not.w   d1
        and.w   #$f,d1
        lsl.w   #8,d1
        lsl.w   #4,d1
        or.w    #$9f0,d1

        lea     CANVAS_TABLE_Y3(a4),a2
        moveq   #0,d0
        move.w  BLIT_BG_START(a4),d0
        add.l   (a2,d0.w*4),a1                  ; fast add Y start for round
        subq.w  #2,a1

        WAIT_FOR_BLITTER
        move.w  d1,BLTCON0(a5)
        move.w  #0,BLTCON1(a5)
        move.w  #(40*2)+2,BLTAMOD(a5)           ; 32
        move.w  #(40*4)+2,BLTDMOD(a5)           40-16=24
        move.l  #$-1,BLTAFWM(a5)
        move.l  a0,BLTAPTH(a5)
        move.l  a1,BLTDPTH(a5)
        moveq   #SUNSET_LENGTH,d0
        lsl.w   #6,d0                           ; Add length to blit
        add.w   #19,d0                          ; 8 words
        move.w  d0,BLTSIZE(a5)
	rts


; d2=offset into scene to copy...
; d3=plane to copy
; 1024 size wide (128 bytes wide) (64 words wide)
BLIT_MOON:
	FUNCID	#$3c21c370
        move.l  d3,d4
        addq.w  #5,d3                   ; 5th bitplane to start copy
        lsl.w   #2,d3
        move.l  (a1,d3),a1              ; index in 5 bitplanes

        lea     BG_SUNSET_INDEX(a4),a2
        add.w   (a2,d4.w*2),a0

;d2=source modulo (128*2)
;d3=dest modulo (40*4)
;a0=source
;a1=dest
        moveq   #0,d0
        add.w   SCROLL_HPOS(a4),d0      ; Compensate on HSCROLL


	
	move.w	MOON_BG_OFFSET(a4),d1
	lsr.w	#3,d1

	add.w	d1,d0

        move.w  d0,d1
        lsr.w   #3,d0                   ; Number of words in.
        add.l   d0,a0                   ; add to map source
        not.w   d1
        and.w   #$f,d1
        lsl.w   #8,d1
        lsl.w   #4,d1
        or.w    #$9f0,d1

        lea     CANVAS_TABLE_Y3(a4),a2
        moveq   #0,d0
        move.w  BLIT_BG_START(a4),d0
        add.l   (a2,d0.w*4),a1                  ; fast add Y start for round
; a1 = destination.
	add.w	#(40*3)-14,a0
	;sub.w	#16,a0
	;addq.w	#8,a1

        WAIT_FOR_BLITTER
        move.w  d1,BLTCON0(a5)
        move.w  #0,BLTCON1(a5)
        move.w  #(40*2)+2,BLTAMOD(a5)           ; 32
        move.w  #(40*4)+2,BLTDMOD(a5)           40-16=24
        move.l  #$-1,BLTAFWM(a5)
        move.l  a0,BLTAPTH(a5)
        move.l  a1,BLTDPTH(a5)
        moveq   #MOON_LENGTH,d0
        lsl.w   #6,d0                           ; Add length to blit
        add.w   #19,d0                          ; 8 words
        move.w  d0,BLTSIZE(a5)
	rts
		



; Find the background water colour pointers in the copper.

; a1 = Buffer to store pointers.
; d1 = colour register to match 0-7
; d2 = YPOS Start = must match!!
; d3 = YPOS Stop = must match!!
FWP
FIND_WATERBG_POINTERS:
	FUNCID	#$77ea1cd6
	
	moveq	#0,d0
	;move.w	BACKGROUND_TRANSITION_FRAMES(a4),d0
	;not.w	d0
	;and.w	#$f,d0
	lea	COPPER_GRADIENTS_TABLE(a4),a0			; a0 points to wrong address.
	move.l	(a0,d0*4),a0

	lsl.w	#8,d1
	lsl.w	#5,d1
	
	lsl.l	#8,d2
	lsl.l	#8,d2
	lsl.l	#8,d2
	or.l	#%00000000000000001111111100000000,d2
	
	lsl.l	#8,d3
	lsl.l	#8,d3
	lsl.l	#8,d3
	or.l	#%00000000000000001111111100000000,d3
	
.loop:	move.l	(a0)+,d4
	and.l	#%11111111000000001111111100000000,d4
	cmp.w	#$ff00,d4
	bne.s	.loop
	cmp.l	d3,d4
	beq.s	.exit
	
	move.l	d2,d5
	move.l	d3,d6
	rol.l	#8,d5
	rol.l	#8,d4
	rol.l	#8,d6
	
	cmp.w	d4,d5			; Check lower
	bgt.s	.loop
	cmp.w	d4,d6			; Check upper
	blt.s	.loop
	
.process:
	or.w	#%100000,d1			; BRDRBLNK
	cmp.w	2(a0),d1		; Matching colour reg?
	bne.s	.loop			; Nope
	move.l	a0,(a1)+		; Yes.
	bra.s	.loop
.exit:	move.l	#-1,(a1)+
	rts
	
	
CYCLE_WATERFALLS:
	FUNCID	#$622e90c1

	tst.w	ROUND_WATERFALL(a4)
	beq.s	.exit
	
	move.w	FRAME_BG(a4),d0
	and.w	#1,d0
	beq.s	.exit
	
	lea	COPPTR_WATERBG_REGS1(a4),a0
	lea	WATERBG_CYCLE1(a4),a1
	lea	WATERBG_CYCLE1_INDEX(a4),a2
	moveq	#96-1,d7
	bsr	CYCLE_WATERBG
	
	lea	COPPTR_WATERBG_REGS5(a4),a0
	lea	WATERBG_CYCLE5(a4),a1
	lea	WATERBG_CYCLE5_INDEX(a4),a2
	moveq	#48-1,d7
	bsr	CYCLE_WATERBG
	
	lea	COPPTR_WATERBG_REGS7(a4),a0
	lea	WATERBG_CYCLE7(a4),a1
	lea	WATERBG_CYCLE7_INDEX(a4),a2
	moveq	#48-1,d7
	bsr	CYCLE_WATERBG
	
.exit:	rts

;a0 = Copper Pointers
;a1 = Colours
;a2 = Colours Index	
CW
CYCLE_WATERBG:
	FUNCID	#$47291e8b
	move.w	(a2),d1
        bpl.s   .reset
        move.w  d7,(a2)                ; To top...
.reset:
.loop:  move.l  (a0)+,d3                ; Next copper register on Y Axis.
        bmi.s   .exit                   ; The end?

        move.w  (a1,d1*2),d2            ; Get colour value
        bpl.s   .ok                     ; Keep going....
; end of cycle... need to reset
        moveq   #0,d1                   ; Reset from start of beginning.
        move.w  (a1,d1*2),d2

.ok:    move.l  d3,a3                   ; Shift to address register.
        move.w  d2,6(a3)
        addq.w  #1,d1                   ; Next colour
        bra.s   .loop

.exit:  subq.w  #1,(a2)
        rts

	