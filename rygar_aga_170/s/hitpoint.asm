
SPR_HITPOINT:   equ     1292
HITPOINT_DELAY:	equ	7

CREATE_HITPOINT:
		FUNCID	#$4f91fbc1
		move.w	RYGAR_XPOS(a4),HITPOINT_XPOS(a4)
		move.w	RYGAR_YPOS(a4),HITPOINT_YPOS(a4)
		move.w	#10,HITPOINT_FRAMES(a4)
		rts

;
; BLIT_HITPOINT
;
; Draw 16x16 sprite routine using blitter cookie cutter
HDL_HITPOINT:	
		FUNCID	#$a9ad4544
                moveq   #0,d0
		move.w	#DISKARM_SPR_BLANK,d0
		
                tst.w   HITPOINT_FRAMES(a4)
                bmi.s   .exit				; if minus then do not blit
		beq.s	.blank				; if plus then blit a blank.
                move.w  #SPR_HITPOINT,d0		
		
.blank: 	subq.w  #1,HITPOINT_FRAMES(a4)
                move.w  HITPOINT_XPOS(a4),d1
                move.w  HITPOINT_YPOS(a4),d2
                bsr     BLIT_HITPOINT
.exit:          rts



;
; In:
;       d0 = Sprite number to draw
;       d1 = xpos
;       d2 = ypos
BLIT_HITPOINT:
		FUNCID	#$0f639230
		
                movem.l a0-a2,-(a7)
                tst.w   d1                      ; Plot can't be off screen left
                bmi     .exit
                tst.w   d2                      ; Plot can't be off screen upper
                bmi     .exit

                move.l  MEMCHK0_SPRITE_POINTERS(a4),a1
                move.l  (a1,d0*8),a2            ; Get sprite address
                move.l  4(a1,d0*8),a3           ; Get mask address

                move.w  d1,d0                   ; d0 now has xpos
                move.w  d2,d1                   ; d1 now has ypos

                move.w  SCROLL_HPOS(a4),d4
                not.w   d4
                and.w   #$1f,d4
                add.w   d4,d0

.size16:
                FAST_SHIFT BLIT_COOKIE
                GET_SPRITE_COORDS_Y32

                move.l  LSTPTR_CURRENT_SCREEN(a4),a1
                move.l  a1,d5

                and.l   #$ffff,d1
                add.l   d1,a1                   ; add Y line offset
                and.l   #$fffe,d0
                add.l   d0,a1                   ; add X word offset
                add.w   MAP_POSX(a4),a1
                subq.w  #4,a1

                move.l  a1,d6
                sub.l   d5,d6			; d6 contains offset.

                WAIT_FOR_BLITTER
                move.w  d4,BLTCON0(a5)
                move.w  d3,BLTCON1(a5)

                move.l  #$ffff0000,BLTAFWM(a5)      ; Only want the first word

                move.w  #SPRITE_ASSETS_SIZE_X-4,BLTAMOD(a5)
                move.w  #SPRITE_ASSETS_SIZE_X-4,BLTBMOD(a5)
                move.w  #PLAYFIELD_SIZE_X-4,BLTCMOD(a5)
                move.w  #PLAYFIELD_SIZE_X-4,BLTDMOD(a5)

		lea	HITPOINT_RESTORE_PTRS(a4),a0
		move.l	(a0),4(a0)		; Save other frame offset.
                move.l  d6,(a0)                 ; Store this frame offset.

                move.l  a3,BLTAPTH(a5)          ; Load the mask address
                move.l  a2,BLTBPTH(a5)          ; Sprite address
                move.l  a1,BLTCPTH(a5)          ; Destination background
                move.l  a1,BLTDPTH(a5)
                move.w  #(16*5)<<6+2,BLTSIZE(a5)
.exit:          movem.l (a7)+,a0-a2
                rts


REST_HITPOINT_BACKGROUND:
		FUNCID	#$9bf25841
		
                tst.w   HITPOINT_FRAMES(a4)		; if hit frames i minus then nothing 
                bmi.s   .done				; to do.

                ;movem.l d6-d7/a0-a2,-(a7)

                tst.w   ROUND_RESTORE_POST(a4)
                bmi     .exit
                tst.w   VERTSCROLL_ENABLE(a4)
                bmi     .exit

                WAIT_FOR_BLITTER
                move.l  #$09f00000,BLTCON0(a5)  ; Select straigt A-D mode $f0
                move.l  #-1,BLTAFWM(a5)  	; No masking needed
                move.w  #$24,BLTAMOD(a5)          ; Playfield Modulo Src
                move.w  #$24,BLTDMOD(a5)          ; Playfield Modulo Dest

                move.l  LSTPTR_CURRENT_SCREEN(a4),a1
                move.l  LSTPTR_POST_SCREEN(a4),a2

                move.l  HITPOINT_RESTORE_PTRS+4(a4),d6
		beq.s	.exit
		
                add.l   d6,a1                   ; a1 = dest
                add.l   d6,a2                   ; a3 = src

                move.l  a2,BLTAPTH(a5)          ; set source address
                move.l  a1,BLTDPTH(a5)          ; set dest address
                move.w  #(16*5)<<6+2,BLTSIZE(a5)  	; boom..... 16x16
		
		tst.w   HITPOINT_FRAMES(a4)	; On the last frame?
                bne.s   .exit			; No... if yes then blit to both screens.
						; is equal to 0 so clear both buffers.
						
                WAIT_FOR_BLITTER	
		move.l  LSTPTR_OFF_SCREEN(a4),a1
		add.l	d6,a1
                move.l  a2,BLTAPTH(a5)          ; set source address
                move.l  a1,BLTDPTH(a5)          ; set dest address
                move.w  #(16*5)<<6+2,BLTSIZE(a5)  	; boom..... 16x16		
		
.exit:          ;movem.l (a7)+,d6-d7/a0-a2
.done:          rts

