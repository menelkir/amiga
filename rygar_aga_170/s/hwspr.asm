SPR_CHAN0:      equ     0
SPR_CHAN1:      equ     1
SPR_CHAN2:      equ     2
SPR_CHAN3:      equ     3
SPR_CHAN4:      equ     4
SPR_CHAN5:      equ     5
SPR_CHAN6:      equ     6
SPR_CHAN7:      equ     7

UPPER_Y:        equ     74              ; min=27 max=74
MID_Y:          equ     120             ; min=108 max=156
LOWER_Y:        equ     190             ; Min=190 Max 220

HWSPRITE_XOFFSET:       equ     122
HWSPRITE_YOFFSET:       equ     32


; Convert bitmap sprites to hardware sprite format.
; LSTPTR_HARDWARE_SPRITE(a4) will contain a long word list to each sprite pointer
;
BUILD_HWSPR_POINTERS:
	FUNCID	#$439f398e
	
	move.l	a0,a1
        move.l  a0,a2
        moveq   #HW_ASSETS_Y-1,d4               ; Y assets

.col:   moveq   #HW_ASSETS_X-1,d5       ; assets per row = 10
        move.l  a2,d3

.row:   move.l  d3,a0
        moveq   #HW_ASSETS_SIZE-1,d6
        move.l  a1,(a3)+                        ; Store Sprite Location
	addq.w	#8,a1
.loop1: 
        addq.w  #8,a1                           ; Next sprite line
        add.l   #HW_ASSETS_LINE*HW_ASSETS_BITPLANES,a0          ; Next line (10*4)*4
        dbf     d6,.loop1
	addq.w	#8,a1

        move.l  d3,a0
        moveq   #31,d6
        move.l  a1,(a3)+                        ; Store Sprite Location
	addq.w	#8,a1
	
.loop2: addq.w  #8,a1                           ; Next sprite line
        add.l   #HW_ASSETS_LINE*HW_ASSETS_BITPLANES,a0          ; Next Bitplane line (40x4)
        dbf     d6,.loop2

	addq.w	#8,a1
	
        addq.l  #4,d3                           ; next 32x sprite in row
        dbf     d5,.row

        add.l   #(HW_ASSETS_LINE*HW_ASSETS_BITPLANES)*HW_ASSETS_SIZE,a2
        dbf     d4,.col
        rts
	
; ---------------------------------------------------------
; --- THIS ROUTINE IS USED TO BUILD THE hwconv.dat file ---
; Convert bitmap sprites to hardware sprite format.
; a0=source
; a1=dest
BITMAP_SPRITES_TO_HWSPRDDD:	
	;lea	HW16_SPRITES,a0					; Source IFF
	;move.l	MEMCHK5_HWSPRITES,a1			; Allocated memory
	;lea	LSTPTR_HARDWARE_SPRITE,a5

; find Body
.find:	cmp.l	#"BODY",(a0)
	beq.s	.found
	addq.w	#2,a0
	bra.s	.find
.found:	addq.w	#8,a0
	
	move.l	a0,a2
	moveq	#HW_ASSETS_Y-1,d4		; Y assets

.col:	moveq	#HW_ASSETS_X-1,d5	; assets per row = 10
	move.l	a2,d3

.row:	move.l	d3,a0
	moveq	#HW_ASSETS_SIZE-1,d6
	move.l	a1,(a3)+			; Store Sprite Location 
	move.l	#0,(a1)+			; Sprite Control Words
	move.l	#0,(a1)+
.loop1:	move.l	(a0),(a1)			; Bitplane 1
	move.l	HW_ASSETS_LINE(a0),4(a1)	; Bitplane 2
	addq.w	#8,a1				; Next sprite line
	add.l	#HW_ASSETS_LINE*HW_ASSETS_BITPLANES,a0		; Next line (10*4)*4
	dbf	d6,.loop1
	move.l	#0,(a1)+
	move.l	#0,(a1)+

	move.l	d3,a0
	moveq	#31,d6
	move.l	a1,(a3)+			; Store Sprite Location 
	move.l	#0,(a1)+			; Sprite Control Long Words
	move.l	#0,(a1)+
.loop2:	move.l	HW_ASSETS_LINE*2(a0),(a1)	; Bitplane 3
	move.l	HW_ASSETS_LINE*3(a0),4(a1)	; Bitplane 4
	addq.w	#8,a1				; Next sprite line
	add.l	#HW_ASSETS_LINE*HW_ASSETS_BITPLANES,a0		; Next Bitplane line (40x4)
	dbf	d6,.loop2
	move.l	#0,(a1)+
	move.l	#0,(a1)+

	addq.l	#4,d3				; next 32x sprite in row
	dbf	d5,.row

	add.l	#(HW_ASSETS_LINE*HW_ASSETS_BITPLANES)*HW_ASSETS_SIZE,a2
	dbf	d4,.col
	rts

BUILD_HWSPRITE_COORDS:
	FUNCID	#$746e0d5a
        lea     HWSPR_TAB_XPOS(a4),a0
        lea     HWSPR_TAB_YPOS(a4),a1

        move.l  #512-1,d7
        moveq   #0,d0
        moveq   #0,d1
        moveq   #0,d2
        moveq   #0,d3

        moveq   #0,d6           ; counter

.loop:
        move.w  d6,d1           ; vstart
        move.w  d6,d2
        lsr.w   #8,d2
        and.w   #1,d2           ; vstart high bit
        lsl.w   #2,d2           ; into bit position 2
        and.w   #$ff,d1         ; vstart low bits
        lsl.w   #8,d1           ; Word 1
        move.w  d1,(a1)+        ; store SPRPOS word

        move.w  d6,d1           ; vstop
        move.w  d6,d3
        add.w   #32,d1
        add.w   #32,d3
        lsr.w   #8,d3
        and.w   #1,d3           ; vstart high bit
        lsl.w   #1,d3           ; into bit position 2
        and.w   #$ff,d1         ; vstart low bits
        lsl.w   #8,d1           ; Word 1
        or.w    d2,d1
        or.w    d3,d1
        or.w    #$80,d1         ; set Attach bit.
        move.w  d1,(a1)+        ; store Y SPRCTL word
; Now do horizontal

        moveq   #0,d0
        moveq   #0,d1
        move.w  d6,d0
        move.w  d6,d1
        and.w   #1,d0
        lsr.w   #1,d1
        move.w  d1,(a0)+        ; Store Hstart High bits
        move.w  d0,(a0)+        ; Store Hstart Low bits

        addq.w  #1,d6
        dbf     d7,.loop

        rts



; Allocate unattached hardware sprite
;d0.w Xpos 30
;d1.w Ypos 50
;d2.w Bob Sprite number
;d6.w if plus then allocate to given hw sprite address
HWSPRITE_ATTACHED:
	FUNCID	#$fdae1911
        add.w   #HWSPRITE_XOFFSET,d0
        add.w   #HWSPRITE_YOFFSET,d1
        moveq   #0,d7
        lea     SPR_TO_HWSPR(a4),a1
        add.w   d2,d2                   ; Multiply by 4
        add.l   d2,a1
        move.w  2(a1),d7

        sub.l   a2,a2
        movem.l d0-d7/a0-a2,-(a7)
        bsr     HWSPRITE_UNATTACHED
        movem.l (a7)+,d0-d7/a0-a2

        addq.w  #1,d6
        addq.w  #1,d7
        bsr     HWSPRITE_UNATTACHED
.exit:  rts


; HWSPRITE_UNATTACHED Called by HWSPRITE_ATTACHED in most cicumstances
; Allocate unattached hardware sprite
;a3.l Copper Pointer
;a2.l if zero then set attach bit.
;d0.w Xpos 30
;d1.w Ypos 50
;d6.w if plus then allocate to given hw sprite address
;d7.w Sprite asset number to display
HWSPRITE_UNATTACHED:
	FUNCID	#$ae742026
        lea     LSTPTR_HARDWARE_SPRITE(a4),a0
        move.l  (a0,d7*4),a0

        lea     HWSPR_TAB_YPOS(a4),a2
        move.l  (a2,d1*4),d2
        lea     HWSPR_TAB_XPOS(a4),a2
        or.l    (a2,d0*4),d2
        move.l  d2,d1

        tst.w   d6
        bmi.s   .exit

; I think this should just be a loop in Vblank.

        move.l  a3,a1
        lsl.w   #4,d6                   ; Multiply by 16
        add.l   d6,a1
        move.l  a0,4(a1)                ; Store the sprite address.
        move.l  d1,8(a1)                ; Store control words
.exit:  rts


; Routine to update the copper pointers
UPDATE_COPPER_SPRITES:
	;movem.l	d0-d7/a0-a6,-(a7)
        moveq   #8-1,d7
.loop_upper:
        move.l  d7,d6
        lea     COPPTR_UPPER_HWSPRITES(a4),a0   ; Index to copper spr pointer
        lsl.w   #4,d6
        add.l   d6,a0
        move.l  (a0),a1                 ; Get pointer to copper offset.
        move.l  MEMCHK2_COPPER1(a4),a2
        add.l   (a1),a2                 ; copper ptr in a1
        move.l  4(a0),d5                ; Get the sprite control words
        move.w  8(a0),10(a2)            ; Store Position
        move.w  10(a0),14(a2)           ; Store Control word

        addq.l  #8,d5
        move.w  d5,2(a2)                ; Low word
        swap    d5
        move.w  d5,6(a2)                ; High word
        dbf     d7,.loop_upper

; Mid sprites multiplexer

	moveq	#0,d0
	move.w	FADE_SPRITE_INDEX(a4),d0	; Index to correct copper list for sprites
	not.w	d0
	and.w	#$f,d0
	lsl.w	#8,d0				; Multiply by 1024 (very careful - COPPER_GRADIENT_SIZE MUST MATCH!!!!)
	lsl.w	#3,d0

        moveq   #8-1,d7
.loop_mid:
        move.l  d7,d6
        lea     COPPTR_MID_HWSPRITES(a4),a0     ; Index to copper spr pointer
        lsl.w   #4,d6
        add.l   d6,a0
        move.l  (a0),a1                 ; Get pointer to copper offset.
        move.l  MEMCHK2_COPPER1(a4),a2
        add.l   (a1),a2                 ; copper ptr in a1
	sub.l	d0,a2
        move.l  4(a0),d5                ; Get the sprite control words
        move.w  8(a0),10(a2)            ; Store Position
        move.w  10(a0),14(a2)           ; Store Control word

        addq.l  #8,d5
        move.w  d5,2(a2)                ; Low word
        swap    d5
        move.w  d5,6(a2)                ; High word
        dbf     d7,.loop_mid

; Lower sprites multiplexer
        moveq   #8-1,d7
.loop_lower:
        move.l  d7,d6
        lea     COPPTR_LOWER_HWSPRITES(a4),a0   ; Index to copper spr pointer
        lsl.w   #4,d6
        add.l   d6,a0
        move.l  (a0),a1                 ; Get pointer to copper offset.
        move.l  MEMCHK2_COPPER1(a4),a2
        add.l   (a1),a2                 ; copper ptr in a1
        move.l  4(a0),d5                ; Get the sprite control words
        move.w  8(a0),10(a2)            ; Store Position
        move.w  10(a0),14(a2)           ; Store Control word

        addq.l  #8,d5
        move.w  d5,2(a2)                ; Low word
        swap    d5
        move.w  d5,6(a2)                ; High word
        dbf     d7,.loop_lower
	;movem.l	(a7)+,d0-d7/a0-a6
.exit:  rts






; d2 will have sprite number to display
; d3 will have the sprite mulitplex slot number, 1-4 upper zone, 5-8 mid zone, 9-12 lower zone.
PLOT_HWSPRITE:
	FUNCID	#$7572ed33
        sub.w   #SPR_TYPE_ENEMY_HW+1,d3
        lea     TABLE_SPR_MULTIPLEX(a4),a2
        move.l  (a2,d3*8),a3                            ; Get copper pointer
        move.l  4(a2,d3*8),d6                           ; Get sprite channel
        moveq   #0,d0
        moveq   #0,d1
        move.w  SPRITE_PLOT_XPOS(a0),d0                 ; Get X Position
        move.w  SPRITE_PLOT_YPOS(a0),d1                 ; Get Y Position
; need to work out channel and slot position.
        bsr     HWSPRITE_ATTACHED
.exit:  rts

;a3=Base Copper Pointers to update
;d0.w Xpos 30
;d1.w Ypos 50
;d2.w Bob Sprite number
;d6.w if plus then allocate to given hw sprite address
DO_HWSPRITES:
	FUNCID	#$f5090d0d
        lea     COPPTR_UPPER_HWSPRITES(a4),a3
;Upper zone sprite updates
        move.l  #272,d0                                 ; Xposition
        move.l  #70,d1                                  ; Yposition
        move.l  #4,d2                                   ; Sprite number
        moveq   #SPR_CHAN0,d6                           ; Sprite Channel
        bsr     HWSPRITE_ATTACHED
        rts


; In = a0=Allocation pointer
;      d0=Sprite number to allocate
; Out = d7=Allocated to slot, -1 unable to allocate.
AHS
ALLOC_HARDWARE_SPRITE:
	FUNCID	#$e67433f6
        ;lea    ENEMY_HWSPR_MID_ALLOCATED(a4),a0
        moveq   #0,d7
        rept    4
        tst.w   2(a0)
        bmi.s   .allocate
        addq.w  #1,d7
        addq.w  #4,a0
        endr
        moveq   #-1,d7
        bra.s   .exit
.allocate:
        move.w  d0,2(a0)
.exit:  rts


; In = a0=Allocation pointer
DEALLOC_HARDWARE_SPRITES:
	FUNCID	#$776e418d
        ;lea     ENEMY_HWSPR_UPPER_ALLOCATED(a4),a0
        ;bsr     .dealloc
        ;lea     ENEMY_HWSPR_MID_ALLOCATED(a4),a0
        ;bsr     .dealloc
        ;lea     ENEMY_HWSPR_LOWER_ALLOCATED(a4),a0
.dealloc:
        rept    4
        move.w  #-1,2(a0)
        addq.w  #4,a0
        endr

.exit:  rts


