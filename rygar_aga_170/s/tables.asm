
	
; Input d1=longword
; Output d1=longword reversed
REVERSE_LONGWORD:
	move.w	d0,-(a7)
	clr.w	d0
	move.b	d1,d0
	move.b  BYTE_REVERSE_TAB(a4,d0.w),d1
	ror.w	#8,d1
	move.b	d1,d0
	move.b	BYTE_REVERSE_TAB(a4,d0.w),d1
        swap    d1
	move.b	d1,d0
	move.b	BYTE_REVERSE_TAB(a4,d0.w),d1
	ror.w   #8,d1
	move.b	d1,d0
	move.b  BYTE_REVERSE_TAB(a4,d0.w),d1
	move.w	(a7)+,d0
	rts
	
; Ross's code to generate a byte swap table
; Conisderably more efficient and smaller than my effort.
GBS
GENERATE_BYTE_SWAP_TABLE:
	movem.l	d0-d3/a0,-(a7)
	;moveq	#0,d1
	lea     BYTE_REVERSE_TAB(a4),a0
	moveq   #0,d3
.k:	moveq   #7,d2
	move.b  d3,d0
.l:	add.b   d0,d0
	roxr.b  #1,d1
	dbf     d2,.l
	move.b  d1,(a0)+
	addq.b  #1,d3
	bne.b   .k
	movem.l	(a7)+,d0-d3/a0
	rts
