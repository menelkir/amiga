
COPRUN_GAME:		equ	0
COPRUN_GAMEINFO:	equ	1
COPRUN_MENU:		equ	2
COPRUN_LOADER:		equ	3
COPRUN_INTRO:		equ	4
COPRUN_HISCORE:		equ	5
COPRUN_GAMENOTES:	equ	6


;
; INIT_COPPER
;
; Initialize the Copper list
;
INIT_COPPER1:
	FUNCID	#$6d7bc22b
	
	move.w	#-1,COPPER_ACTIVE(a4)
        move.l  MEMCHK2_COPPER1(a4),a1
        move.l  a1,d4

; Wait for line 28
; Sprites need to be active very early on in the display.
        move.l  #$1c01ff00,(a1)+
        move.l  a1,COPPTR_UPPER_PANEL(a4)
        bsr     INIT_COPPER_SPRITES

; Upper sprite zone
        lea     COPPTR_HARDWARE_UPPER_SPRITE_0(a4),a3
        bsr     CREATE_COPPER_SPRITE_POINTERS

        move.w  #BPLCON2,(a1)+  ; 8
        move.w  #%00100100,(a1)+        ; 30            %0110000    000100100   = %100100 Sprites have pritority

        bsr     COPPER_UPPER_PANEL
        move.w  #BPLCON2,(a1)+  ; 8
        move.w  #$00,(a1)+      ; 30
	
        lea     PAL_RYGAR_BG(a4),a0
        bsr     LOAD_BACKGROUND_PALETTE_COPPER		; Overlay Backgound Palette
	
        move.l  #$3e0dfffe,(a1)+
        move.w  #FMODE,(a1)+
        move.w  #%0000000000000101,(a1)+                ; 4x fetch and 32x sprites
        move.w  #DDFSTRT,(a1)+  ;
        move.w  #$0038,(a1)+    ; 3c
        move.w  #DDFSTOP,(a1)+  ;
        move.w  #$00B0,(a1)+    ; a4

        move.w  #BPLCON0,(a1)+          ; turn off the bitplanes
        move.w  #$0000,(a1)+
; Set display properties, modulo, bitplanes...

        move.w  #BPLCON2,(a1)+  ; 8
        move.w  #%00100100,(a1)+        ; 30            %0110000    000100100   = %100100 Sprites have pritority
        move.w  #BPLCON4,(a1)+
        move.w  #$0011,(a1)+
        move.w  #BPL1MOD,(a1)+  ;
        move.w  #$A4,(a1)+      ; a0
        move.w  #BPL2MOD,(a1)+  ;
        move.w  #$A4,(a1)+      ; a0

	IFNE	DEBUG_BORDER_ENABLE		; Debug borders for scrolling
        move.w  #DIWSTRT,(a1)+  ;
        move.w  #$2771,(a1)+    ;
        move.w  #DIWSTOP,(a1)+  ;
        move.w  #$10b8,(a1)+    ;               1098
	
	ELSE
	
        move.w  #DIWSTRT,(a1)+  ;
        move.w  #$27a1,(a1)+    ;
        move.w  #DIWSTOP,(a1)+  ;
        move.w  #$1098,(a1)+    ;               1098
	ENDC

; Load Main Palette
        lea     PAL_RYGAR_FG(a4),a0
        clr.l   (a0)
        move.l  #$180,d2                        ; col reg pointer.
        moveq   #32-1,d7
        move.l  #$01060c00,(a1)+                ; Upper bits going in
.main_pal_msb:
        move.w  d2,(a1)+                        ; move in colour reg
        move.w  (a0)+,(a1)+
        addq.w  #2,a0
        addq.w  #2,d2
        dbf     d7,.main_pal_msb

        move.l  #$180,d2                        ; col reg pointer.
        move.l  #$01060e00,(a1)+
        moveq   #32-1,d7
        lea     PAL_RYGAR_FG+2(a4),a0
.main_pal_lsb:
        move.w  d2,(a1)+
        move.w  (a0)+,(a1)+
        addq.w  #2,a0
        addq.w  #2,d2
        dbf     d7,.main_pal_lsb

        move.l  a1,COPPTR_VIDEOTOP(a4)
        lea     LSTPTR_CURRENT_SCREEN(a4),a0
        moveq   #8-1,d7                 ; number of planes to load
        bsr     INIT_COPPER_BITPLANES

        move.w  #INTREQ,(a1)+                   ; Read buttons at this line!
        move.w  #$8010,(a1)+

        move.l  #$4001ff00,(a1)+
        move.l  a1,COPPTR_BPLCON(a4)
        move.w  #BPLCON0,(a1)+  ; 0             ; This is play area
        move.w  #$0000,(a1)+    ; 2             ; 8 PLanes here
        move.w  #BPLCON1,(a1)+  ; 4
        move.w  #0,(a1)+        ; 6
	
;----------------------------------------------------------------------------------------------------
        move.l  a1,COPPTR_MAIN_START(a4)
        move.l  #$01fe0000,(a1)+                ; Dummy Copper  ; COP2LCH
        move.l  #$01fe0000,(a1)+                ; Dummy Copper  ; COP2LCL
        move.l  #$01fe0000,(a1)+                ; Dummy Copper  ; COPJMP2

;------- RYGAR PALETTE GRADIENT -------------------
        IFNE    ENABLE_GRADIENTS
	bsr	INIT_GRADIENTS


	move.l	a1,-(a7)

	lea     COPPER_GRADIENTS_TABLE(a4),a2			; Copper Gradient offsets.
        moveq   #15,d7						; Interpolate 16 times.
	
	move.l	a1,d3						; Copper pointer to jump to after gradient colours
	add.l	#12,d3

.gradients:
        move.l  (a2)+,a1
        movem.l d0-d7/a0-a3,-(a7)
        move.L  #$8b,d7                                         ; Line to wait to insert sprite pointers
        move.l  ROUND_GRADIENT_PTR(a4),a0
        lea     COPPTR_HARDWARE_MID_SPRITE_0(a4),a3
        bsr     INSERT_GRADIENT_INTO_COPPER
	swap	d3
	move.w	#COP1LCH,(a1)+
	move.w	d3,(a1)+
	swap	d3
	move.w	#COP1LCL,(a1)+
	move.w	d3,(a1)+
	move.l  #COPJMP1,d3                     ; Strobe COPJMP 1
	swap	d3
        move.l  d3,(a1)+
	
        movem.l (a7)+,d0-d7/a0-a3
        dbf     d7,.gradients
	
	bsr	CREATE_FADE_GRADIENTS
	
	move.l	(a7)+,a1
	
        move.l  a1,COPPTR_GRADIENT_START(a4)
	lea	COPPER_GRADIENTS_TABLE,a0
	move.l	(a0),d0
	swap	d0
	move.w	#COP2LCH,(a1)+
	move.w	d0,(a1)+
	swap	d0
	move.w	#COP2LCL,(a1)+
	move.w	d0,(a1)+	
	move.l	#COPJMP2,d0
	swap	d0
	move.l	d0,(a1)+
	
	;move.l  #$01fe0000,(a1)+                ; Dummy Copper  ; COP2LCH
        ;move.l  #$01fe0000,(a1)+                ; Dummy Copper  ; COP2LCL
        ;move.l  #$01fe0000,(a1)+                ; Dummy Copper  ; COPJMP2	
	
.copper1_return:

        ENDC
;--------------------------------------------------

; Wait for a specific line in upper zone
        move.l  a1,COPPTR_LOWERSPRITE(a4)

;Lower Sprite zone
        move.l  #$d401ff00,(a1)+                                        ; WAIT FOR LINE $D1

        lea     COPPTR_HARDWARE_LOWER_SPRITE_0(a4),a3
        bsr     CREATE_COPPER_SPRITE_POINTERS

;----------------------------------------------------
; Below here is debug screen
;----------------------------------------------------
; Water Animation here...

        move.l  a1,COPPTR_MAIN_END(a4)

        move.l  #$d701ff00,(a1)+

        move.l  a1,COPPTR_WATER(a4)
        move.l  #$01060000,(a1)+                ; Colour 5 (Dark)
        move.l  #$018a0fff,(a1)+
        move.l  #$01060200,(a1)+
        move.l  #$018a0000,(a1)+

        move.l  #$01060000,(a1)+                ; Colour 22
        move.l  #$01b00fff,(a1)+
        move.l  #$01060200,(a1)+
        move.l  #$01b00000,(a1)+

        move.l  #$01060000,(a1)+                ; Colour 11
        move.l  #$01960fff,(a1)+
        move.l  #$01060200,(a1)+
        move.l  #$01960000,(a1)+

        move.l  #$01060000,(a1)+                ; Colour 24
        move.l  #$01ac0fff,(a1)+
        move.l  #$01060200,(a1)+
        move.l  #$01ac0000,(a1)+

        move.l  #$01060000,(a1)+                ; Colour 8
        move.l  #$01900fff,(a1)+
        move.l  #$01060200,(a1)+
        move.l  #$01900000,(a1)+

; Lava Animation here...
        move.l  #$f701ff00,(a1)+

        move.l  a1,COPPTR_LAVA_COL03(a4)
        move.l  #$01060000,(a1)+
        move.l  #$01860e24,(a1)+
        move.l  #$01060200,(a1)+
        move.l  #$01860100,(a1)+

        move.l  a1,COPPTR_LAVA_COL02(a4)
        move.l  #$01060000,(a1)+
        move.l  #$01840e24,(a1)+
        move.l  #$01060200,(a1)+
        move.l  #$01840000,(a1)+

        move.l  a1,COPPTR_LAVA_COL01(a4)
        move.l  #$01060000,(a1)+
        move.l  #$01820542,(a1)+
        move.l  #$01060200,(a1)+
        move.l  #$01820cc9,(a1)+

        move.l  a1,COPPTR_LAVA_COL17(a4)
        move.l  #$01060000,(a1)+
        move.l  #$01a20542,(a1)+
        move.l  #$01060200,(a1)+
        move.l  #$01a20cc9,(a1)+

        ;move.l  #$fd01ff00,(a1)+
        ;move.l  #$01060000,(a1)+
        ;move.l  #$01820080,(a1)+
        ;move.l  #$01060200,(a1)+
        ;move.l  #$01820000,(a1)+

        IFEQ    DEBUG_TEXT_ENABLE
        bsr     COPPER_LOWER_PANEL
        ELSE
        bsr     COPPER_DEBUG_TEXT
        ENDC




;Rearm Copper 1
        move.l  MEMCHK2_COPPER1(a4),d0          ; Reload copper start
        move.l  #COP1LCL,d1
        lsl.l   #8,d1
        lsl.l   #8,d1
        move.w  d0,d1
        move.l  d1,(a1)+
        swap    d0
        move.l  #COP1LCH,d1
        lsl.l   #8,d1
        lsl.l   #8,d1
        move.w  d0,d1
        move.l  d1,(a1)+

        move.l  #$fffffffe,(a1)+

; Copper 2 - BONUS SCENE 1
        move.l  a1,COPPTR_BONUS_SCENE1(a4)

        move.l  #$4101ff00,(a1)+                ; Repulse Bonus in White
        move.w  #BPLCON0,(a1)+                  ; turn off the bitplanes
        move.w  #$5200,(a1)+

        move.l  #$4801ff00,(a1)+                ; Repulse Bonus in White
        move.l  #$01060000,(a1)+
        move.l  #$01be0ddd,(a1)+
        move.l  #$01060200,(a1)+
        move.l  #$01be0ddd,(a1)+

;First Row
        move.l  #$5001ff00,(a1)+                ; Alien Head in Yellow
        move.l  #$01060000,(a1)+
        move.l  #$01be0dd0,(a1)+
        move.l  #$01060200,(a1)+
        move.l  #$01be0dd0,(a1)+

        move.l  #$5201ff00,(a1)+                ; Alien Body in Red
        move.l  #$01060000,(a1)+
        move.l  #$01be0d00,(a1)+
        move.l  #$01060200,(a1)+
        move.l  #$01be0d00,(a1)+

;Second Row
        move.l  #$5801ff00,(a1)+                ; Alien Head in Yellow
        move.l  #$01060000,(a1)+
        move.l  #$01be0dd0,(a1)+
        move.l  #$01060200,(a1)+
        move.l  #$01be0dd0,(a1)+

        move.l  #$5a01ff00,(a1)+                ; Alien Body in Red
        move.l  #$01060000,(a1)+
        move.l  #$01be0d00,(a1)+
        move.l  #$01060200,(a1)+
        move.l  #$01be0d00,(a1)+


; Third Row
        move.l  #$6001ff00,(a1)+                ; Alien Head in Yellow
        move.l  #$01060000,(a1)+
        move.l  #$01be0dd0,(a1)+
        move.l  #$01060200,(a1)+
        move.l  #$01be0dd0,(a1)+

        move.l  #$6201ff00,(a1)+                ; Alien Body in Red
        move.l  #$01060000,(a1)+
        move.l  #$01be0d00,(a1)+
        move.l  #$01060200,(a1)+
        move.l  #$01be0d00,(a1)+


        move.l  #$6801ff00,(a1)+                ; Total Bonus in Yellow
        move.l  #$01060000,(a1)+
        move.l  #$01be0dd0,(a1)+
        move.l  #$01060200,(a1)+
        move.l  #$01be0dd0,(a1)+

        move.l  #$7001ff00,(a1)+                ; End in Black
        move.l  #$01060000,(a1)+
        move.l  #$01be0665,(a1)+
        move.l  #$01060200,(a1)+
        move.l  #$01be0b57,(a1)+		; 6B6C57
	
        move.l  #$7801ff00,(a1)+                ; End in Black
        move.l  #$01060000,(a1)+
        move.l  #$01be0ddd,(a1)+
        move.l  #$01060200,(a1)+
        move.l  #$01be0ddd,(a1)+

        move.l  COPPTR_MAIN_END(a4),d0          ; Load main start copper area
        move.l  #COP1LCL,d1
        lsl.l   #8,d1
        lsl.l   #8,d1
        move.w  d0,d1
        move.l  d1,(a1)+

        swap    d0
        move.l  #COP1LCH,d1
        lsl.l   #8,d1
        lsl.l   #8,d1
        move.w  d0,d1
        move.l  d1,(a1)+

        move.l  #COPJMP1,d1                     ; Strobe COPJMP 1
        lsl.l   #8,d1
        lsl.l   #8,d1
        move.l  d1,(a1)+

; Copper 2 - BONUS SCENE 2
        move.l  a1,COPPTR_BONUS_SCENE2(a4)

        move.l  #$4101ff00,(a1)+                ; Repulse Bonus in White
        move.w  #BPLCON0,(a1)+                  ; turn off the bitplanes
        move.w  #$5200,(a1)+

        move.l  #$4801ff00,(a1)+                ; Repulse Bonus in Yellow
        move.l  #$01060000,(a1)+                ; Rank in Yellow
        move.l  #$01be0ddd,(a1)+                ; Timer Bonus in Yellow
        move.l  #$01060200,(a1)+
        move.l  #$01be0ddd,(a1)+

        move.l  #$6801ff00,(a1)+                ; Total Bonus in Cyan
        move.l  #$01060000,(a1)+
        move.l  #$01be04bf,(a1)+
        move.l  #$01060200,(a1)+
        move.l  #$01be04bf,(a1)+

        move.l  #$7001ff00,(a1)+                ; End in Black
        move.l  #$01060000,(a1)+
        move.l  #$01be0665,(a1)+
        move.l  #$01060200,(a1)+
        move.l  #$01be0b57,(a1)+		; 6B6C57
	
        move.l  #$7801ff00,(a1)+                ; End in Black
        move.l  #$01060000,(a1)+
        move.l  #$01be0ddd,(a1)+
        move.l  #$01060200,(a1)+
        move.l  #$01be0ddd,(a1)+		; 6B6C57

        move.l  COPPTR_MAIN_END(a4),d0          ; Load main start copper area
        move.l  #COP1LCL,d1
        lsl.l   #8,d1
        lsl.l   #8,d1
        move.w  d0,d1
        move.l  d1,(a1)+

        swap    d0
        move.l  #COP1LCH,d1
        lsl.l   #8,d1
        lsl.l   #8,d1
        move.w  d0,d1
        move.l  d1,(a1)+

        move.l  #COPJMP1,d1                     ; Strobe COPJMP 1
        lsl.l   #8,d1
        lsl.l   #8,d1
        move.l  d1,(a1)+

; Create dummy / Black copper

	move.l	a1,COPPTR_BLACK(a4)	
	move.w  #BPLCON0,(a1)+          ; turn off the bitplanes
        move.w  #$0000,(a1)+
        move.l  #$1c01ff00,(a1)+
        bsr     INIT_COPPER_SPRITES
	
	move.w  #FMODE,(a1)+
        move.w  #%0000000000000101,(a1)+                ; 4x fetch and 32x sprites
        move.w  #DDFSTRT,(a1)+  ;
        move.w  #$0038,(a1)+    ; 3c
        move.w  #DDFSTOP,(a1)+  ;
        move.w  #$00B0,(a1)+    ; a4

	tst.w	ROUND_WATERFALL(a4)
	beq.s	.exit
	
; Need to test Water scene rounds here....
	moveq	#1,d1				; Col Register 1
	moveq	#110,d2
	move.l	#190,d3
	lea	COPPTR_WATERBG_REGS1(a4),a1
	bsr	FIND_WATERBG_POINTERS

	moveq	#5,d1				; Col Register 5
	moveq	#110,d2
	move.l	#190,d3
	lea	COPPTR_WATERBG_REGS5(a4),a1
	bsr	FIND_WATERBG_POINTERS
	
	moveq	#7,d1				; Col Register 7
	moveq	#114,d2
	move.l	#190,d3
	lea	COPPTR_WATERBG_REGS7(a4),a1
	bsr	FIND_WATERBG_POINTERS

.exit:
        rts

INIT_COPPER_BITPLANES:
	FUNCID	#$8808ba3e
	
        move.w  #BPL0PTL,d2
        move.w  #BPL0PTH,d3

.loop:  move.l  (a0)+,d0
        move.w  d2,(a1)+
        move.w  d0,(a1)+
        swap    d0
        move.w  d3,(a1)+
        move.w  d0,(a1)+
        addq.l  #4,d2
        addq.l  #4,d3
        dbf     d7,.loop
        rts

INIT_MAPCOLLISION_BITPLANES:
	FUNCID	#$90e81695
	
        move.w  #BPL0PTL,d2
        move.w  #BPL0PTH,d3

.loop:  move.l  (a0)+,d0
        move.w  d2,(a1)+
        move.w  d0,(a1)+
        swap    d0
        move.w  d3,(a1)+
        move.w  d0,(a1)+
        addq.l  #4,d2
        addq.l  #4,d3
        dbf     d7,.loop
        rts

INIT_COPPER_SPRITES:
	FUNCID	#$e5123749
	
;Init sprite 0
        move.w  #SPR0PTH,(a1)+
        move.w  #$0000,(a1)+
        move.w  #SPR0PTL,(a1)+
        move.w  #$0000,(a1)+
        move.w  #SPR0POS,(a1)+
        move.w  #$0000,(a1)+
        move.w  #SPR0CTL,(a1)+
        move.w  #$0000,(a1)+

;Init sprite 1
        move.w  #SPR1PTH,(a1)+
        move.w  #$0000,(a1)+
        move.w  #SPR1PTL,(a1)+
        move.w  #$0000,(a1)+
        move.w  #SPR1POS,(a1)+
        move.w  #$0000,(a1)+
        move.w  #SPR1CTL,(a1)+
        move.w  #$0000,(a1)+

;Init sprite 2
        move.w  #SPR2PTH,(a1)+
        move.w  #$0000,(a1)+
        move.w  #SPR2PTL,(a1)+
        move.w  #$0000,(a1)+
        move.w  #SPR2POS,(a1)+
        move.w  #$0000,(a1)+
        move.w  #SPR2CTL,(a1)+
        move.w  #$0000,(a1)+

;Init sprite 3
        move.w  #SPR3PTH,(a1)+
        move.w  #$0000,(a1)+
        move.w  #SPR3PTL,(a1)+
        move.w  #$0000,(a1)+
        move.w  #SPR3POS,(a1)+
        move.w  #$0000,(a1)+
        move.w  #SPR3CTL,(a1)+
        move.w  #$0000,(a1)+

;Init sprite 4
        move.w  #SPR4PTH,(a1)+
        move.w  #$0000,(a1)+
        move.w  #SPR4PTL,(a1)+
        move.w  #$0000,(a1)+
        move.w  #SPR4POS,(a1)+
        move.w  #$0000,(a1)+
        move.w  #SPR4CTL,(a1)+
        move.w  #$0000,(a1)+

;Init sprite 5
        move.w  #SPR5PTH,(a1)+
        move.w  #$0000,(a1)+
        move.w  #SPR5PTL,(a1)+
        move.w  #$0000,(a1)+
        move.w  #SPR5POS,(a1)+
        move.w  #$0000,(a1)+
        move.w  #SPR5CTL,(a1)+
        move.w  #$0000,(a1)+

;Init sprite 6
        move.w  #SPR6PTH,(a1)+
        move.w  #$0000,(a1)+
        move.w  #SPR6PTL,(a1)+
        move.w  #$0000,(a1)+
        move.w  #SPR6POS,(a1)+
        move.w  #$0000,(a1)+
        move.w  #SPR6CTL,(a1)+
        move.w  #$0000,(a1)+

;Init sprite 7
        move.w  #SPR7PTH,(a1)+
        move.w  #$0000,(a1)+
        move.w  #SPR7PTL,(a1)+
        move.w  #$0000,(a1)+
        move.w  #SPR7POS,(a1)+
        move.w  #$0000,(a1)+
        move.w  #SPR7CTL,(a1)+
        move.w  #$0000,(a1)+
        rts

RESET_COPPER_SPRITES:			;---- UNUSED ROUTINE
	FUNCID	#$dc70158e
	
        move.l  #0,d2
        move.l  d2,d3
        swap    d3
;Init sprite 0
        move.w  #SPR0PTH,(a1)+
        move.w  d3,(a1)+
        move.w  #SPR0PTL,(a1)+
        move.w  d2,(a1)+

;Init sprite 1
        move.w  #SPR1PTH,(a1)+
        move.w  d3,(a1)+
        move.w  #SPR1PTL,(a1)+
        move.w  d2,(a1)+

;Init sprite 2
        move.w  #SPR2PTH,(a1)+
        move.w  d3,(a1)+
        move.w  #SPR2PTL,(a1)+
        move.w  d2,(a1)+

;Init sprite 3
        move.w  #SPR3PTH,(a1)+
        move.w  d3,(a1)+
        move.w  #SPR3PTL,(a1)+
        move.w  d2,(a1)+

;Init sprite 4
        move.w  #SPR4PTH,(a1)+
        move.w  d3,(a1)+
        move.w  #SPR4PTL,(a1)+
        move.w  d2,(a1)+

;Init sprite 5
        move.w  #SPR5PTH,(a1)+
        move.w  d3,(a1)+
        move.w  #SPR5PTL,(a1)+
        move.w  d2,(a1)+

;Init sprite 6
        move.w  #SPR6PTH,(a1)+
        move.w  d3,(a1)+
        move.w  #SPR6PTL,(a1)+
        move.w  d2,(a1)+

;Init sprite 7
        move.w  #SPR7PTH,(a1)+
        move.w  d3,(a1)+
        move.w  #SPR7PTL,(a1)+
        move.w  d2,(a1)+
        rts

CREATE_COPPER_SPRITE_POINTERS:
	FUNCID	#$1420e752
	
;       tst.l   d1
;       bpl.s   .copper
;       add.l   #128,a1
;       bra     .exit
;.copper:
        movem.l d0/d4-d5/a0/a3,-(a7)
        move.l  a1,d5
        sub.l   d4,d5
        move.l  d5,(a3)+
	lea	HW_SPRITE_DUMMY(a4),a0
        move.l  a0,d0
        move.w  #SPR0PTL,(a1)+
        move.w  d0,(a1)+
        swap    d0
        move.w  #SPR0PTH,(a1)+
        move.w  d0,(a1)+
        move.w  #SPR0POS,(a1)+
        move.w  #$0000,(a1)+
        move.w  #SPR0CTL,(a1)+
        move.w  #$0000,(a1)+

        move.l  a1,d5
        sub.l   d4,d5
        move.l  d5,(a3)+
	lea	HW_SPRITE_DUMMY(a4),a0
        move.l  a0,d0
        move.w  #SPR1PTL,(a1)+
        move.w  d0,(a1)+
        swap    d0
        move.w  #SPR1PTH,(a1)+
        move.w  d0,(a1)+
        move.w  #SPR1POS,(a1)+
        move.w  #$0000,(a1)+
        move.w  #SPR1CTL,(a1)+
        move.w  #$0000,(a1)+
	
        move.l  a1,d5
        sub.l   d4,d5
        move.l  d5,(a3)+
	lea	HW_SPRITE_DUMMY(a4),a0
        move.l  a0,d0
        move.w  #SPR2PTL,(a1)+
        move.w  d0,(a1)+
        swap    d0
        move.w  #SPR2PTH,(a1)+
        move.w  d0,(a1)+
        move.w  #SPR2POS,(a1)+
        move.w  #$0000,(a1)+
        move.w  #SPR2CTL,(a1)+
        move.w  #$0000,(a1)+

        move.l  a1,d5
        sub.l   d4,d5
        move.l  d5,(a3)+
	lea	HW_SPRITE_DUMMY(a4),a0
        move.l  a0,d0
        move.w  #SPR3PTL,(a1)+
        move.w  d0,(a1)+
        swap    d0
        move.w  #SPR3PTH,(a1)+
        move.w  d0,(a1)+
        move.w  #SPR3POS,(a1)+
        move.w  #$0000,(a1)+
        move.w  #SPR3CTL,(a1)+
        move.w  #$0000,(a1)+

        move.l  a1,d5
        sub.l   d4,d5
        move.l  d5,(a3)+
	lea	HW_SPRITE_DUMMY(a4),a0
        move.l  a0,d0
        move.w  #SPR4PTL,(a1)+
        move.w  d0,(a1)+
        swap    d0
        move.w  #SPR4PTH,(a1)+
        move.w  d0,(a1)+
        move.w  #SPR4POS,(a1)+
        move.w  #$0000,(a1)+
        move.w  #SPR4CTL,(a1)+
        move.w  #$0000,(a1)+

        move.l  a1,d5
        sub.l   d4,d5
        move.l  d5,(a3)+
	lea	HW_SPRITE_DUMMY(a4),a0
        move.l  a0,d0
        move.w  #SPR5PTL,(a1)+
        move.w  d0,(a1)+
        swap    d0
        move.w  #SPR5PTH,(a1)+
        move.w  d0,(a1)+
        move.w  #SPR5POS,(a1)+
        move.w  #$0000,(a1)+
        move.w  #SPR5CTL,(a1)+
        move.w  #$0000,(a1)+

        move.l  a1,d5
        sub.l   d4,d5
        move.l  d5,(a3)+
	lea	HW_SPRITE_DUMMY(a4),a0
        move.l  a0,d0
        move.w  #SPR6PTL,(a1)+
        move.w  d0,(a1)+
        swap    d0
        move.w  #SPR6PTH,(a1)+
        move.w  d0,(a1)+
        move.w  #SPR6POS,(a1)+
        move.w  #$0000,(a1)+
        move.w  #SPR6CTL,(a1)+
        move.w  #$0000,(a1)+

        move.l  a1,d5
        sub.l   d4,d5
        move.l  d5,(a3)+
	lea	HW_SPRITE_DUMMY(a4),a0
        move.l  a0,d0
        move.w  #SPR7PTL,(a1)+
        move.w  d0,(a1)+
        swap    d0
        move.w  #SPR7PTH,(a1)+
        move.w  d0,(a1)+
        move.w  #SPR7POS,(a1)+
        move.w  #$0000,(a1)+
        move.w  #SPR7CTL,(a1)+
        move.w  #$0000,(a1)+

        movem.l (a7)+,d0/d4-d5/a0/a3
.exit:  rts



; a0=colour list pointer
; a1=copper list
; a3=Sprite list pointers
; d7=vertical position to place sprite pointers
INSERT_GRADIENT_INTO_COPPER:
	FUNCID	#$a37623e9
	
	move.l	d3,-(a7)
        moveq   #0,d2
        moveq   #0,d6

.loop:  cmp.l   #-2,(a0)
        beq     .done

        cmp.l   #-1,(a0)
        beq     .next
	
	move.l  (a0),d0
        ;add.b   #22,d0			; index 22 y lines (need to fix).
	cmp.b	d0,d6
	beq	.scanline
	
        addq.w  #1,d6
        moveq   #0,d2
        moveq   #1,d3
        moveq   #0,d5

        cmp.b   d7,d6			; compare with mid sprite line.
        bne.s   .no_sprites
        lsl.l   #8,d7
        lsl.l   #8,d7
        lsl.l   #8,d7
	or.l	#$0001ff00,d7
        move.l  d7,(a1)+
        lsr.l   #8,d7
        lsr.l   #8,d7
        lsr.l   #8,d7
        bsr     CREATE_COPPER_SPRITE_POINTERS
.no_sprites:
        cmp.b   d0,d6
        beq.s   .scanline
        bra.s   .loop

.scanline:
        move.l  (a0)+,d0                ; Get the line number

	move.l	d3,-(a7)
;d4
        move.b  d0,d2
        lsl.w   #8,d2
        or.w    d3,d2			; add horizontal position
        lsl.l   #8,d2
        lsl.l   #8,d2
        or.w    #$fffe,d2
        move.l  d2,(a1)+

        move.l  (a0)+,d2                ; Get the colour number
        lsl.w   #8,d2
        lsl.w   #5,d2                   ; bit 13

        move.l  (a0),d3                 ; get colour value.
        and.l   #$00f0f0f0,d3
        move.l  d3,d5
        lsr.l   #4,d3
        move.l  d3,d5
        lsr.l   #4,d5
        or.l    d5,d3
        lsr.l   #4,d5
        and.l   #$ff,d3
        and.w   #$f00,d5
        or.l    d5,d3
        or.l    #$01800000,d3
        move.w  #BPLCON3,(a1)+                  ; BPL CON
	or.w	#%100000,d2			; BRDRBLNK
        move.w  d2,(a1)+
        move.l  d3,(a1)+                        ; Colour Value

        move.l  (a0)+,d5
        and.l   #$000f0f0f,d5
        move.l  d5,d3
        lsr.l   #4,d5
        or.l    d5,d3
        lsr.l   #4,d5
        and.l   #$ff,d3
        and.l   #$f00,d5
        or.l    d5,d3

        or.l    #$1800000,d3
        move.w  #BPLCON3,(a1)+
	or.w	#%100000,d2			; BRDRBLNK
        or.w    #$200,d2
        move.w  d2,(a1)+
        move.l  d3,(a1)+
	
	move.l	(a7)+,d3
	
	addq.w	#4,d3				; Next available hpos
        bra     .loop

.next:  addq.w  #4,a0
        bra     .loop

.done:  move.l	(a7)+,d3
	rts

