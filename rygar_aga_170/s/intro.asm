; d0=Number of bombs bnus

INTRO_WINDOW_X: equ     32
INTRO_WINDOW_Y: equ     32

INTRO_SCORE_STEP:       equ     200
INTRO_DELAY_TIME:       equ     350             ; 7 seconds

INTRO_WATER_DELAY:      equ     5
INTRO_CHAR_DELAY:       equ     0

INTRO_SCENE_SIZE_Y:     equ     184
INTRO_SCENE_BITPLANES:  equ     5
INTRO_SCENE_COLOURS:    equ     32

INTRO_WAIT_TIME:        equ     80

INTRO_WATER_ANIM_YPOS:  equ     152

INTRO_TEXT_LINES:       equ     8

TEXT_RGB:       equ     $fff

INTRO:
	FUNCID	#$11c93066
	
	bsr	LOAD_INTRO_SCENE
	
        IFNE    ENABLE_MUSIC
        moveq   #MUSIC_INTRO,d0
        bsr     PLAY_TUNE
        ENDC
	
        move.w  #INTRO_WAIT_TIME,INTRO_TIMER(a4)
        clr.b   INTRO_TEXT_DONE(a4)
        clr.b   INTRO_TEXT_XPOS(a4)
        clr.b   INTRO_TEXT_YPOS(a4)

        move.l  INTRO_SCENE_PTR(a4),a0
        lea     INTRO_SCENE_COLOUR_PAL(a4),a1
        bsr     GET_PALETTE_FROM_IFF

        lea     INTRO_SCENE_COLOUR_PAL(a4),a1
        move.w  #TEXT_RGB,62(a1)

        move.l  INTRO_SCENE_ANIM(a4),a0
.anim1: cmp.l   #"BODY",(a0)
        beq.s   .anim2
        addq.w  #2,a0
        bra.s   .anim1
.anim2: addq.w  #8,a0
        move.l  a0,INTRO_ANIM_BODY_PTR(a4)
        bsr     DRAW_INTRO_SCENE

        moveq   #0,d7                           ; Time Delay
        bsr     DO_WATER_ANIM

	IFEQ	ENABLE_DEBUG
	bsr	WAIT_FOR_VERTICAL_BLANK	
        move.w  #%0000000110000000,DMACON(a5)		; Disable Copper
	bsr	WAIT_FOR_VERTICAL_BLANK
	ENDC
	
        bsr     INIT_INTRO_COPPER

        IFEQ    ENABLE_DEBUG
	move.w	#COPRUN_INTRO,COPRUN_TYPE(a4)
        move.l  MEMCHK2_COPPER1(a4),COP1LCH(a5)
        move.l  MEMCHK2_COPPER1(a4),COP2LCH(a5)
        ;         FEDCBA9876543210
        move.w  #%1000011111000000,DMACON(a5)
        ENDC

        bsr     SETSCR_FBUFF
        bsr     SET_FRAMEBUFF

        bsr     INTRO_TRANS_BLACK_TO_COLOUR

.vb:
        moveq   #INTRO_WATER_DELAY-1,d7                         ; Time Delay
        bsr     DO_WATER_ANIM

        ;btst.b #7,$bfe001
        ;beq    .end

        subq.w  #1,INTRO_TIMER(a4)
        tst.w   INTRO_TIMER(a4)
        bmi.s   .end
        bra.s   .vb

.end:
        bsr     INTRO_TRANS_COLOUR_TO_BLACK

.exit:

.exit_INTRO:    moveq   #0,d6
.rts:           rts



; Animate water routine
; d7=number of frames to wait before painting of the animations
DO_WATER_ANIM:
	FUNCID	#$6526bda5
	
        movem.l d0-d7/a0-a3,-(a7)

.frame: bsr     WAIT_FOR_VERTICAL_BLANK
        bsr     DRAW_INTRO_LINE
        dbf     d7,.frame

        move.w  ANIM_WATER_FRAME(a4),d2
        and.w   #1,d2
        moveq   #0,d0                                   ; Anim source tile index to plot (0-3)
        moveq   #18,d1                                  ; Destination on screen
        add.w   d2,d0
        bsr     ANIM_WATER
        moveq   #2,d0
        moveq   #33,d1
        add.w   d2,d0
        bsr     ANIM_WATER
        addq.w  #1,ANIM_WATER_FRAME(a4)
        movem.l (a7)+,d0-d7/a0-a3
        rts


LOAD_INTRO_SCENE:
	LEVEL5_STOP
	move.l	MEMCHK1_POST_SCROLL(a4),a0
	lea	FILE_RYGAR_INTRO(a4),a1
        move.l  MEMCHK5_LOADBUFF(a4),a2
        sub.l   a3,a3
        moveq   #0+64,d0

        IFNE    ENABLE_DEBUG
        move.w  #$4000,$dff09a
        ELSE
        move.w #$8,$dff09a
        ENDC
        bsr     ocean
	beq	DOS_ERROR
        moveq   #-3,d0
        bsr     ocean
        IFNE    ENABLE_DEBUG
        move.w  #$4000,$dff09a
        ELSE
        move.w #$8,$dff09a
        ENDC
        move.l  MEMCHK1_POST_SCROLL(a4),a0          ; source	
        move.l  MEMCHK1_POST_SCROLL(a4),a1          ; dest
        bsr     Unpack
	move.l	a0,INTRO_SCENE_PTR(a4)
	LEVEL5_START
	rts


; a1=Screen pointers
DRAW_INTRO_SCENE:
	FUNCID	#$52128393
	
        lea     LSTPTR_FRONT_SCREEN(a4),a1
        move.l  INTRO_SCENE_PTR(a4),a0
.loop:  cmp.l   #"BODY",(a0)
        beq.s   .found
        addq.w  #2,a0
        bra.s   .loop

.found: addq.w  #8,a0

;a0=bitmap - width = = 24
;a1=screen pointer - width = 40 bytes
        move.l  #INTRO_SCENE_SIZE_Y-1,d7                                ; lines to copy
        lea     LSTPTR_FRONT_SCREEN(a4),a1
        move.l  (a1),a1
        addq.w  #4,a1

.SCENE:
        rept    16                      ; (12 * 8)*2 = 192
        move.w  (a0)+,(a1)+             ; 16 words,
        endr
        add.w   #8,a1                   ; Playfield Modulo
        rept    16
        move.w  (a0)+,(a1)+
        endr
        add.w   #8,a1                   ; Playfield Modulo
        rept    16
        move.w  (a0)+,(a1)+
        endr
        add.w   #8,a1                   ; Playfield Modulo
        rept    16
        move.w  (a0)+,(a1)+
        endr
        add.w   #8,a1                   ; Playfield Modulo
        rept    16
        move.w  (a0)+,(a1)+
        endr
        add.w   #8,a1                   ; Playfield Modulo

        dbf     d7,.SCENE
        rts




;d0=block (0,1,2,3)
;d1=xindex (0-39)
ANIM_WATER:
	FUNCID	#$4614ab98
	
        move.l  INTRO_ANIM_BODY_PTR(a4),a0
        lea     LSTPTR_FRONT_SCREEN(a4),a1
        move.l  (a1),a1
        add.w   d0,d0
        add.l   d0,a0
        add.l   d1,a1

; a0 now has block

        add.l   #(PLAYFIELD_SIZE_X*INTRO_SCENE_BITPLANES)*INTRO_WATER_ANIM_YPOS,a1                      ; PLAYFIELD is 320 pixels wide

        moveq   #INTRO_SCENE_COLOURS-1,d7

.copy:
        move.b  (a0),(a1)
        move.b  8(a0),40(a1)
        move.b  16(a0),80(a1)
        move.b  24(a0),120(a1)
        move.b  32(a0),160(a1)

        move.b  1(a0),1(a1)
        move.b  9(a0),41(a1)
        move.b  17(a0),81(a1)
        move.b  25(a0),121(a1)
        move.b  33(a0),161(a1)

        add.w   #(8*5),a0                               ; next line in Source block
        add.w   #(40*5),a1                              ; next line in PF

        dbf     d7,.copy
        rts


; Transition scene colours from Black to its paletter
INTRO_TRANS_BLACK_TO_COLOUR:
	FUNCID	#$0af305a1
	
        moveq   #15,d7

.loop:  bsr     WAIT_FOR_VERTICAL_BLANK
        lea     INTRO_SCENE_BLACK_PAL(a4),a0
        lea     INTRO_SCENE_COLOUR_PAL(a4),a1
        lea     INTRO_SCENE_FLUID_PAL(a4),a2
        lea     INTRO_SCENE_COLOURS*2(a0),a3
.next:  move.w  (a0)+,d2
        move.w  (a1)+,d1
        bsr     COLOUR_INTERPOLATE
        move.w  d0,(a2)+
        cmp.l   a0,a3
        bne.s   .next

        bsr     INTRO_COPY_PAL_TO_COPPER
        dbf     d7,.loop
        rts


; Transition scene colours from Black to its paletter
INTRO_TRANS_COLOUR_TO_BLACK:
	FUNCID	#$5e70b45d
	
        moveq   #15,d7

.loop:  bsr     WAIT_FOR_VERTICAL_BLANK
        lea     INTRO_SCENE_COLOUR_PAL(a4),a0
        lea     INTRO_SCENE_BLACK_PAL(a4),a1
        lea     INTRO_SCENE_FLUID_PAL(a4),a2
        lea     INTRO_SCENE_COLOURS*2-2(a0),a3
.next:  move.w  (a0)+,d2
        move.w  (a1)+,d1
        bsr     COLOUR_INTERPOLATE
        move.w  d0,(a2)+
        cmp.l   a0,a3
        bne.s   .next

        bsr     INTRO_COPY_PAL_TO_COPPER
        dbf     d7,.loop
        rts




;a0=
DRAW_INTRO_LINE:
	FUNCID	#$ea6e19d0
	
        movem.l d0-d7/a0-a3,-(a7)
        tst.b   INTRO_TEXT_DONE(a4)
        bmi     .done

        tst.b   INTRO_TEXT_WAIT(a4)
        beq.s   .do_char
        sub.b   #1,INTRO_TEXT_WAIT(a4)
        bra     .done

.do_char:
        move.b  #INTRO_CHAR_DELAY,INTRO_TEXT_WAIT(a4)
.next_y:
        moveq   #0,d0
        moveq   #0,d1

        lea     INTRO_TELETYPE_TEXT(a4),a0
        move.b  INTRO_TEXT_XPOS(a4),d0
        moveq   #0,d5
        move.b  INTRO_TEXT_YPOS(a4),d5
        cmp.b   #INTRO_TEXT_LINES,d5
        bne.s   .cont
        move.b  #-1,INTRO_TEXT_DONE(a4)
        bra     .done

.cont:
        add.b   d5,d1
        add.b   d1,d1
        addq.w  #2,d1
        lsl.b   #5,d5
        add.l   d5,a0                           ; y position
        add.l   d0,a0                           ; x position

        moveq   #0,d7
        moveq   #0,d6
        add.w   #1,d0                           ; index in 3 from left
        bsr     DRAW_INTRO_CHAR

        cmp.b   #31,INTRO_TEXT_XPOS(a4)
        bne.s   .not_xpos
        clr.b   INTRO_TEXT_XPOS(a4)
        add.b   #1,INTRO_TEXT_YPOS(a4)
        bra.s   .next_y

.not_xpos:
        add.b   #1,INTRO_TEXT_XPOS(a4)
.done:  movem.l (a7)+,d0-d7/a0-a3
        rts



; d0=8x8 x
; d1=8x8 y
; d6=Colour number 0-3 (0=BP1, 1=BP2, 2=BP3, 3=BP4, 4=BP5)
; d7=-1 Draw on nibble 0=byte
; a0=pointer to char to display
; a1=Screen addresses
DRAW_INTRO_CHAR:
	FUNCID	#$1f804d68
	
        movem.l d0-d7/a0-a3,-(a7)
        lsl.w   #3,d1
        mulu    #PLAYFIELD_SIZE_X*5,d1
        lsl.w   #2,d6
        lea     LSTPTR_FRONT_SCREEN(a4),a1
        move.l  (a1,d6),a1              ; Bitplane to draw in.
        add.l   d1,a1
        add.l   d0,a1

        addq.w  #2,a1

        moveq   #0,d0
        ;lea    FONT_ASSET(a4)+86,a2    ; start of BODY
        move.l  FONT_ASSET(a4),a2
        add.w   #86,a2                  ; Offset to BODY
        move.b  (a0),d0

        sub.b   #32,d0

.draw:  add.l   d0,a2           ; index into character

        move.b  (a2),d0
        or.b    d0,(a1)
        or.b    d0,PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,PLAYFIELD_SIZE_X*4(a1)


        move.b  FONT_WIDTH*1(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*1)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*1)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*1)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*1)+PLAYFIELD_SIZE_X*4(a1)

        move.b  FONT_WIDTH*2(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*2)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*2)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*2)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*2)+PLAYFIELD_SIZE_X*4(a1)

        move.b  FONT_WIDTH*3(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*3)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*3)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*3)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*3)+PLAYFIELD_SIZE_X*4(a1)

        move.b  FONT_WIDTH*4(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*4(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*4)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*4)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*4)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*4)+PLAYFIELD_SIZE_X*4(a1)

        move.b  FONT_WIDTH*5(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*5(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*5)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*5)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*5)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*5)+PLAYFIELD_SIZE_X*4(a1)

        move.b  FONT_WIDTH*6(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*6(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*6)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*6)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*6)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*6)+PLAYFIELD_SIZE_X*4(a1)

        move.b  FONT_WIDTH*7(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*7(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*7)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*7)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*7)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*7)+PLAYFIELD_SIZE_X*4(a1)

.exit:  movem.l (a7)+,d0-d7/a0-a3
        rts



;
; INIT_INTRO_COPPER
;
; Initialize the Copper list
;
INIT_INTRO_COPPER:
	FUNCID	#$6075f776
	
        move.l  MEMCHK2_COPPER1(a4),a1
        move.l  a1,d4

        move.w  #DDFSTRT,(a1)+  ;
        move.w  #$0038,(a1)+    ; 3c
        move.w  #DDFSTOP,(a1)+  ;
        move.w  #$00d0,(a1)+    ; a4

        move.l  #$1c01ff00,(a1)+
        move.l  a1,COPPTR_UPPER_PANEL(a4)
        bsr     INIT_COPPER_SPRITES

        bsr     COPPER_UPPER_PANEL

        move.l  #$3e0dfffe,(a1)+

        move.w  #FMODE,(a1)+
        move.w  #%0000000000001100,(a1)+

        move.w  #BPLCON0,(a1)+  ; 0             ; This is play area
        move.w  #$0000,(a1)+    ; 2             ; 8 PLanes here
        move.w  #BPLCON1,(a1)+  ; 4
        move.w  #0,(a1)+        ; 6
        move.w  #BPLCON2,(a1)+  ; 8
        move.w  #$00,(a1)+      ; 30
        move.w  #BPL1MOD,(a1)+  ;
        move.w  #$a0,(a1)+      ; a0
        move.w  #BPL2MOD,(a1)+  ;
        move.w  #$a0,(a1)+      ; a0

        move.w  #DIWSTRT,(a1)+  ;
        move.w  #$27a1,(a1)+    ;
        move.w  #DIWSTOP,(a1)+  ;
        move.w  #$10a8,(a1)+    ;

        lea     COPPTR_HARDWARE_UPPER_SPRITE_0(a4),a3
        bsr     CREATE_COPPER_SPRITE_POINTERS

        move.w  #BPLCON0,(a1)+          ; turn off the bitplanes
        move.w  #$0000,(a1)+

; Load Main Palette
        lea     INTRO_SCENE_BLACK_PAL(a4),a0
        move.l  #$180,d2
        moveq   #INTRO_SCENE_COLOURS-1,d7

        move.l  a1,COPPTR_INTRO_SCENE_PAL(a4)

.main_pal:
        move.w  d2,(a1)+
        move.w  (a0)+,(a1)+
        addq.w  #2,d2
        dbf     d7,.main_pal

        move.l  #$4001ff00,(a1)+
        move.w  #BPLCON0,(a1)+  ; 0             ; This is play area
        move.w  #$5200,(a1)+    ; 2

        lea     LSTPTR_CURRENT_SCREEN(a4),a0
        moveq   #INTRO_SCENE_BITPLANES-1,d7                     ; number of planes to load
        move.w  #BPL0PTL,d2
        move.w  #BPL0PTH,d3

.plane: move.l  (a0)+,d0
        move.w  d2,(a1)+
        move.w  d0,(a1)+
        swap    d0
        move.w  d3,(a1)+
        move.w  d0,(a1)+
        addq.l  #4,d2
        addq.l  #4,d3
        dbf     d7,.plane




; Set display properties, modulo, bitplanes...

        bsr     COPPER_LOWER_PANEL

        ;COPPER_DEBUG_TEXT

        ;move.l  #DUMMY_BPLCON0,COPPTR_BPLCON(a4)
	
	lea	DUMMY_BPLCON0(a4),a3
        move.l  a3,COPPTR_BPLCON(a4)

        move.l  #$fffffffe,(a1)+
        rts

INTRO_COPY_PAL_TO_COPPER:
	FUNCID	#$31c7e295
	
        move.l  a0,-(a7)
        move.l  a1,-(a7)
        lea     INTRO_SCENE_FLUID_PAL(a4),a0
        move.l  COPPTR_INTRO_SCENE_PAL(a4),a1

        rept    32
        move.w  (a0)+,2(a1)
        addq.w  #4,a1
        endr

        move.l  (a7)+,a1
        move.l  (a7)+,a0
        rts

