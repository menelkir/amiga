
ROUND1_SQUIRREL_XPOS:	equ	$868
ROUND1_SQUIRREL_YPOS:	equ	84

DISMOUNT_ROPE_TO_LEFT:	equ	-1
DISMOUNT_ROPE_TO_RIGHT:	equ	1

CHECK_EVENTS:
	FUNCID	#$69ffb9db
	bsr	CHECK_NEW_ROUND
	bsr	CHECK_SHIELD
	bsr	CHECK_MUSIC_EVENTS
	bsr	CHECK_RYGAR_LIFE
	tst.l	d0
	bmi	.exit

; Check round events
	move.l	ROUND_EVENTS_PTR(a4),a0
.loop:	tst.l	(a0)			; Finished?
	bmi.s	.exit			; Yes
	tst.w	4(a0)			; Has	the event already fired?
	bmi.s	.next			; Yes next...

	move.w	(a0),d0			; Get the Xposition of the event.
	cmp.w	MAP_PIXEL_POSX(a4),d0
	beq.s	.do_event
	;move.w	2(a0),d0
	;cmp.w	MAP_PIXEL_POSY(a4),d0
	;beq.s	.do_event
	bra.s	.next
.do_event:
	move.w	#-1,4(a0)
	move.l	6(a0),d0
	lea	EVENT_POINTERS(pc),a1
	move.l	(a1,d0*4),a1
	add.l	#CODE_BASE,a1
	move.l	a0,-(a7)
	cmp.b	#$4e,(a1)		; Fail safe, look for nop
	bne.s	.crash
	jsr	(a1)
.crash:	move.l	(a7)+,a0
.next:	add.w	#10,a0
	bra.s	.loop
.exit:	rts

; Reset All events after the current MAP_PIXEL_POSY
RESET_EVENTS:
	FUNCID	#$deec2eff
	move.l	ROUND_EVENTS_PTR(a4),a0
	
.loop:	tst.l	(a0)			; Finished?
	bmi.s	.exit			; Yes
	move.w	(a0),d0
	cmp.w	MAP_PIXEL_POSX(a4),d0
	bge.s	.reset_event
	bra.s	.next
.reset_event:
	clr.w	4(a0)
.next:	add.w	#10,a0
	bra.s	.loop
.exit:	rts

;Event Repeat
EVENT_REPEAT:	equ	1			; Event will be triggered, until reset called
EVENT_ONCE:	equ	0			; Event will only be triggered once
EVENT_ALWAYS:	equ	-1			; Event will always be triggered.

;Event Match
EVENT_MATCH_MAP_XY:	equ	0
EVENT_MATCH_MAP_X:	equ	-1
EVENT_MATCH_MAP_Y:	equ	1

; Event structure is
;	MAP_PIXEL_XPOS.w,MAP_PIXEL_YPOS.w
;	EVENT_MATCH.b,EVENT_REPEAT.b
;	EVENT_STATUS.w		(0 Active , -1 Inactive)
;	EVENT_HANDLER_PTR.l


HANDLE_EVENTS:
	FUNCID	#$eb39bc78
; Check round events
	move.l	ROUND_EVENTS_PTR(a4),a0
.loop:	tst.l	(a0)			; Finished?
	bmi	.exit			; Yes
	move.w	(a0),d0			; Get Xpos in d0
	move.w	2(a0),d1		; Get Ypos in d1
	tst.b	4(a0)
	beq.s	.check_map_xy
	bmi.s	.check_map_x
	bra.s	.check_map_y
.check_map_xy:
	cmp.w	MAP_PIXEL_POSX(a4),d0
	bne.s	.exit
	cmp.w	MAP_PIXEL_POSY(a4),d1
	bne.s	.exit
	bra.s	.do_trigger
.check_map_x:
	cmp.w	MAP_PIXEL_POSX(a4),d0
	beq.s	.do_trigger
	bra.s	.exit

.check_map_y:
	cmp.w	MAP_PIXEL_POSY(a4),d1
	beq.s	.do_trigger
	bra.s	.exit

.do_trigger:
	move.b	5(a0),d0
	btst	#4,d0			; Once already triggered?
	bne.s	.next
	btst	#5,d0			; Repeat already triggered?
	bne.s	.next			
	
	tst.b	d0
	beq.s	.trigger_once
	bmi.s	.trigger_always
	bra.s	.trigger_repeat
	


.trigger_once:
	move.w	#EVENT_ONCE,6(a0)
	move.b	#$10,5(a0)
	bra.s	.do_event

.trigger_repeat:
	move.w	#EVENT_REPEAT,6(a0)
	move.b	#$20,5(a0)
	bra.s	.do_event
	
.trigger_always:
	move.w	#EVENT_ALWAYS,6(a0)
	move.b	#-1,5(a0)
	;bra.s	.do_event
	
.do_event:
	move.l	8(a0),d0
	lea	(CODE_BASE,d0.l),a1
	move.l	a0,-(a7)
	jsr	(a1)
	move.l	(a7)+,a0
.next:	add.w	#12,a0
	bra	.loop

.exit:	rts

; Reset All events after the current MAP_PIXEL_POSY
RESET_EVENTSX:
	FUNCID	#$68086586
	move.l	ROUND_EVENTS_PTR(a4),a0
.loop:	tst.l	(a0)			; Finished?
	bmi.s	.exit			; Yes
	cmp.w	#EVENT_REPEAT,6(a0)
	beq.s	.reset_event
	bra.s	.next
.reset_event:
	move.b	#1,5(a0)		; Cause event to be recreated.
.next:	add.w	#12,a0
	bra.s	.loop
.exit:	rts



CHECK_RYGAR_LIFE:
	FUNCID	#$eb0d315e
        moveq   #0,d0
        tst.w   RYGAR_LIFE_LOST(a4)
        beq     .exit
; Check to see if it is the last life?
	tst.w   RYGAR_LIVES(a4)
        bpl     .lives_remain


	
	tst.w	REAPER_ACTIVE(a4)
	bmi	.exit
	
; Clear the shield sprite so that 
	;bsr	DESTROY_ALL_ENEMIES_ONSCREEN			; this is not running.
	
        lea     SPRITE_FREE_LIST(a4),a0				; Clear the shield sprite.
	move.w	#-1,2(a0)						; Reaper will be forced into this one.			

; In:
;       d0 = Sprite type (0, bombjack, 1=MUMMY
;       d1 = start xpos
;       d2 = start ypos
;       d3 = sprite control word to set
;       a0 = offset address of handler to assign
;
	moveq	#SPR_REAPER,d0
	moveq	#0,d1
	move.w	MAP_PIXEL_POSX(a4),d1
	add.l	#100,d1
	move.l	#50,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	lea	HDL_REAPER(pc),a0
	bsr	PUSH_SPRITE

	move.w	#-1,INPUT_DISABLED(a4)
	move.w	#-1,REAPER_ACTIVE(a4)
	move.w	#-1,DISABLE_ENEMIES(a4)
	move.w	#-1,PAUSE_ENEMIES(a4)
	moveq	#-1,d0
	bra	.exit

.lives_remain:
        clr.w   RYGAR_CURRENT_POWERS(a4)

        bsr     TRANSITION_TO_BLACK					; Close screen
        bsr     RESET_ROUND

; Set RYGAR_START_POSITION
        moveq   #SPR_TYPE_PLAYER,d0
        bsr     GET_SPRITE_CONTEXT
        move.w  #SCROLL_LOCK_POSX,SPRITE_PLOT_XPOS(a0)

        moveq   #0,d2
        move.l  MEMCHK0_SPRITE_HDLVARS(a4),a0
        lea     TAB_64(a4),a3
        add.w   (a3,d2*2),a0
        move.w  #SPRITE_INSTATE_ONPLATFORM,THIS_SPRITE_STATUS(a0)
        move.w  #SPRITE_INSTATE_DIRECTION_RIGHT,RYGAR_LAST_DIRECTION(a0)
	
        clr.w   RYGAR_LIFE_LOST(a4)

; PAN LEFT TO PLATFORM
        moveq   #-1,d0
.exit:  rts


CHECK_SHIELD:
	tst.w	SHIELD_TIMER(a4)
	beq.s	.shield_off
	bmi.s	.exit
.shield_on:
	;subq.w	#1,SHIELD_TIMER(a4)
	bra.s	.exit
.shield_off:
	move.w	#-1,SHIELD_TIMER(a4)
	moveq   #SPR_RESERVED_SHIELD,d0
        bsr    	DISABLE_SPRITE	
	bsr	DRAW_SHIELD_TEXT
	
	eor.w	#POWER_CROSS,RYGAR_CURRENT_POWERS(a4)
	
	moveq	#0,d0
	moveq	#3,d1
	bsr	BLIT_RYGAR_POWER
.exit:	rts


; This needs to handle Music timing correctly.
; If power is collected before sanctuary...then Sanctuary must override.
; Same with Evil Comes...
CHECK_MUSIC_EVENTS:
	tst.w	MUSIC_DELAY_TIME(a4)		; Finished playing current mini-tune?
	bmi.s	.exit				; N/A
	beq.s	.play_main_or_evil_or_shield	; Yes!
	subq.w	#1,MUSIC_DELAY_TIME(a4)		; Decrement timer
	bra.s	.exit
	
; Here we restart playing 
.play_main_or_evil_or_shield:				; or sanctuary.... it needs to get the last playing tune.
	subq.w	#1,MUSIC_DELAY_TIME(a4)
	
	tst.w	DISABLE_ENEMIES(a4)		; Is in Sanctuary
	bmi.s	.exit				; Sanctuary music must continue.
	
; Still in game action.
	tst.w	SHIELD_TIMER(a4)		; No bonus if Shield is on.
	bmi.s	.play_main_or_shield
	moveq	#MUSIC_SHIELD,d0
	bra.s	.play

.play_main_or_shield:
	moveq	#MUSIC_RESTART,d0
	tst.w	TIME_REMAINING(a4)
	bpl.s	.play
	moveq	#MUSIC_EVIL,d0
	
.play:	bsr	PLAY_TUNE
	
.exit:	rts

	

BACKGROUND_OFF:
	nop
	FUNCID	#$947ca447
	bsr	SET_LAST_MAP_POSITION	
	clr.w	BACKGROUND_ENABLE(a4)
	rts
	
BACKGROUND_ON:
	nop
	FUNCID	#$ff2818f4
	bsr	SET_LAST_MAP_POSITION	
	move.w	#-1,BACKGROUND_ENABLE(a4)
	rts

CHECKPOINTS_OFF:
	nop
	FUNCID	#$d3306bd3
	move.w	MAP_PIXEL_POSX(a4),MAP_PIXEL_POSX_LIMIT(a4)
	bsr	SET_LAST_MAP_POSITION	
	move.w	#-1,CHECKPOINT_DISABLE(a4)
	rts
	
CHECKPOINTS_ON:
	nop
	FUNCID	#$57a4c980
	
	move.w	MAP_PIXEL_POSX(a4),MAP_PIXEL_POSX_LIMIT(a4)
	
	clr.w	CHECKPOINT_DISABLE(a4)
	bsr	SET_LAST_MAP_POSITION
	bsr	GET_MAP_PLATFORMS
	clr.w	CHECKPOINT_DISABLE(a4)
	rts
	


	
ROUND_EVENT_INJECT_SQUIRREL:
	nop
	FUNCID	#$9b8add6a
	moveq	#SPR_SQUIRREL,d0
	move.w	MAP_PIXEL_POSX(a4),d1
	add.w	#16,d1
	moveq	#84,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	or.w	2(a0),d3				; Parameter Sets X Swoop Speed	
	lea	HDL_SQUIRREL(pc),a0
	bsr	PUSH_SPRITE
	rts
	
ROUND_EVENT_INJECT_BIGRHINO:
	nop
; Test bank 0
	lea	ENEMY_HWSPR_MID_ALLOCATED(a4),a3
	tst.w	2(a3)			; Is bank 0 free?
	bpl.s	.bank_1			; Yes
	tst.w	6(a3)
	bpl.s	.bank_1
	bra.s	.bank_free

.bank_1:
	addq.w	#8,a3			; No, try bank 1
	tst.w	2(a3)			
	bpl.s	.exit		; Yes... so allocate
	tst.w	6(a3)
	bpl.s	.exit
	nop

.bank_free:
        moveq   #0,d1
        moveq   #SPR_BIGRHINO1,d0       
        move.w  #146,d2                 
        move.w  #SPR_TYPE_ENEMY_2X32,d3 
	or.w	2(a0),d3				; Parameter Increases Rhino Move Speed
        lea	HDL_BIGRHINO1(pc),a0               
        moveq   #0,d7
        bsr     PUSH_SPRITE
	tst.w	d4
	bmi.s	.exit
	move.w	d4,2(a3)

        moveq   #0,d1
        moveq   #SPR_BIGRHINO2,d0       
        move.w  #146,d2                 
        move.w  #SPR_TYPE_ENEMY_2X32,d3 
	or.w	2(a0),d3				; Parameter Sets Delay Speed
        lea	HDL_BIGRHINO2(pc),a0       
        bsr     PUSH_SPRITE
	tst.w	d4
	bmi.s	.exit
	move.w	d4,6(a3)
.exit:	rts
	
ROUND_EVENT_INJECT_BIGRHINO_WITH_RIDER:
	nop
	
; Test bank 0
	lea	ENEMY_HWSPR_MID_ALLOCATED(a4),a3
	tst.w	2(a3)			; Is bank 0 free?
	bpl.s	.bank_1			; Yes
	tst.w	6(a3)
	bpl.s	.bank_1
	bra.s	.bank_free

.bank_1:
	addq.w	#8,a3			; No, try bank 1
	tst.w	2(a3)			
	bpl	.exit		; Yes... so allocate
	tst.w	6(a3)
	bpl	.exit
	nop

.bank_free:
        moveq   #0,d1
        moveq   #SPR_BIGRHINO1,d0       
        move.w  #146,d2                 
        move.w  #SPR_TYPE_ENEMY_2X32,d3 
	or.w	2(a0),d3				; Parameter Increases Rhino Move Speed
        lea	HDL_BIGRHINO1(pc),a0               
        moveq   #0,d7
        bsr     PUSH_SPRITE
	tst.w	d4
	bmi.s	.exit
	move.w	d4,2(a3)
	
	move.l	d4,d6			; Save big rhino sprite allocated number

        moveq   #0,d1
        moveq   #SPR_BIGRHINO2,d0       
        move.w  #146,d2                 
        move.w  #SPR_TYPE_ENEMY_2X32,d3 
	or.w	2(a0),d3				; Parameter Sets Delay Speed
        lea	HDL_BIGRHINO2(pc),a0       
        bsr     PUSH_SPRITE
	tst.w	d4
	bmi.s	.exit
	move.w	d4,6(a3)

	moveq   #0,d1
	moveq   #SPR_RIDER,d0           ; sprite number
	move.w  #64,d2                  ; yposition
	move.w  #SPR_TYPE_ENEMY_2X32,d3 ; control
	or.w	#$100,d3				; Set bit 1 for Rider on back sprite.
	lea	HDL_RIDER(pc),a0           ; handler
	bsr     PUSH_SPRITE
	tst.w	d4
	bmi.s	.exit
	
	lea	ATTACH_TABLE(a4),a3		; Save the allocated dragon into the rider cell.
	move.b	d6,(a3,d4)
	move.b	d4,(a3,d6)
.exit:	rts
	
ROUND_EVENT_INJECT_GIANTDEMON:
	nop
	FUNCID	#$71b64114
	moveq	#SPR_GIANTDEMON,d0
	move.w	MAP_PIXEL_POSX(a4),d1
	add.w	#64,d1
	move.w	#114,d2
	moveq	#SPR_TYPE_ENEMY_2X64,d3
	or.w	2(a0),d3				; Parameter Sets Delay Speed
	lea	HDL_GIANTDEMON(pc),a0
	bsr	PUSH_SPRITE
	rts
	
ROUND_EVENT_INJECT_GIANTWORM:
	nop
	moveq	#SPR_GIANTWORM,d0
	move.w	MAP_PIXEL_POSX(a4),d1
	add.w	#16,d1
	move.w	#192,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	or.w	2(a0),d3				; Parameter Sets Delay Speed
	lea	HDL_GIANTWORM(pc),a0
	bsr	PUSH_SPRITE
	rts
	
ROUND_EVENT_INJECT_GRIFFIN:
	nop
	moveq	#SPR_GRIFFIN,d0
	move.w	MAP_PIXEL_POSX(a4),d1
	add.w	#16,d1
	move.w	#50,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	or.w	2(a0),d3				; Parameter Sets Delay Speed
	lea	HDL_GRIFFIN(pc),a0
	bsr	PUSH_SPRITE
	rts
	
ROUND_EVENT_INJECT_LAVAMAN:
	nop
	moveq	#SPR_LAVAMAN,d0
	move.w	MAP_PIXEL_POSX(a4),d1
	add.w	#16,d1
	move.w	#192,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	or.w	2(a0),d3				; Parameter Sets Delay Speed
	lea	HDL_LAVAMAN(pc),a0
	bsr	PUSH_SPRITE
	rts
	
ROUND_EVENT_INJECT_CRAB:
	nop
	moveq	#SPR_CRAB,d0
	move.w	MAP_PIXEL_POSX(a4),d1
	add.w	#16,d1
	move.w	#176,d2
	moveq	#SPR_TYPE_ENEMY_1X16,d3
	or.w	2(a0),d3				; Parameter Sets Delay Speed
	lea	HDL_CRAB(pc),a0
	bsr	PUSH_SPRITE
	rts
	
RIT
ROUND_EVENT_INJECT_TRIBESMEN:
	nop
	
	FUNCID	#$31cb96a9
	
	clr.w	TRIBESMAN_STACK_COUNT(a4)
	move.w	2(a0),d3
	lsr.w	#8,d3
	and.w	#$f,d3
	bne.s	.casc0
	
	move.l	a0,-(a7)
	lea	TRIBESMAN_SPRITE_ALLOC(a4),a0
	moveq	#MAX_SPRITES-1,d7
	moveq	#-1,d6
.init1:	move.b	d6,(a0)+
	dbf	d7,.init1
	
	clr.l	TRIBESMAN_CASCADE(a4)
	clr.l	TRIBESMAN_CASCADE_YPOS(a4)
	move.l	(a7)+,a0
	
	;clr.w	TRIBESMAN_STACK_COUNT(a4)
	
.casc0:
	clr.l	TRIBESMAN_HOLDFRAMES(a4)
	move.l	a0,-(a7)
	moveq	#SPR_TRIBESMAN,d0
	move.w	MAP_PIXEL_POSX(a4),d1
	move.w	#146,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	or.w	2(a0),d3				; Parameter Sets Formation Type of Attack.
	lea	HDL_TRIBESMAN(pc),a0
	bsr	PUSH_SPRITE
	tst.w	d4
	bmi	.max_reached
	lea	TRIBESMAN_SPRITE_ALLOC(a4),a0
	and.l	#$1f,d4
	move.b	#0,(a0,d4)
	addq.w	#1,TRIBESMEN_IN_USE(a4)	
	lsr.w	#8,d3
	and.w	#$f,d3
	bne.s	.casc1
	lea	TRIBESMAN_CASCADE(a4),a0
	move.b	d4,0(a0)

.casc1:	move.l	(a7)+,a0
	
	move.l	a0,-(a7)	
	moveq	#SPR_TRIBESMAN,d0
	move.w	MAP_PIXEL_POSX(a4),d1
	add.w	#32,d1
	move.w	#146,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	or.w	2(a0),d3
	lea	HDL_TRIBESMAN(pc),a0
	bsr	PUSH_SPRITE
	tst.w	d4
	bmi	.max_reached
	lea	TRIBESMAN_SPRITE_ALLOC(a4),a0
	and.l	#$1f,d4
	move.b	#1,(a0,d4)
	addq.w	#1,TRIBESMEN_IN_USE(a4)
	lsr.w	#8,d3
	and.w	#$f,d3
	bne.s	.casc2
	lea	TRIBESMAN_CASCADE(a4),a0
	move.b	d4,1(a0)
.casc2:	move.l	(a7)+,a0
	
	move.l	a0,-(a7)	
	moveq	#SPR_TRIBESMAN,d0
	move.w	MAP_PIXEL_POSX(a4),d1
	add.w	#64,d1
	move.w	#146,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	or.w	2(a0),d3
	lea	HDL_TRIBESMAN(pc),a0
	bsr	PUSH_SPRITE
	tst.w	d4
	bmi	.max_reached
	lea	TRIBESMAN_SPRITE_ALLOC(a4),a0
	and.l	#$1f,d4
	move.b	#2,(a0,d4)
	addq.w	#1,TRIBESMEN_IN_USE(a4)
	lsr.w	#8,d3
	and.w	#$f,d3
	bne.s	.casc3
	lea	TRIBESMAN_CASCADE(a4),a0
	move.b	d4,2(a0)
.casc3:	move.l	(a7)+,a0
	
	move.l	a0,-(a7)	
	moveq	#SPR_TRIBESMAN,d0
	move.w	MAP_PIXEL_POSX(a4),d1
	add.w	#96,d1
	move.w	#146,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	or.w	2(a0),d3
	lea	HDL_TRIBESMAN(pc),a0
	bsr	PUSH_SPRITE
	tst.w	d4
	bmi	.max_reached
	lea	TRIBESMAN_SPRITE_ALLOC(a4),a0
	and.l	#$1f,d4
	move.b	#3,(a0,d4)
	addq.w	#1,TRIBESMEN_IN_USE(a4)
	lsr.w	#8,d3
	and.w	#$f,d3
	bne.s	.casc4
	lea	TRIBESMAN_CASCADE(a4),a0
	move.b	d4,3(a0)
.max_reached:
.casc4:	move.l	(a7)+,a0
.exit:	rts


ROUND_EVENT_INJECT_FIREBALL:
	nop
	moveq	#SPR_TYPE_ENEMY_1X16,d3
	or.w	2(a0),d3				; Parameter Sets Delay Speed
	moveq	#SPR_FIREBALL,d0
	move.w	MAP_PIXEL_POSX(a4),d1
	add.w	#16,d1
	move.w	#186+8,d2
	lea	HDL_FIREBALL(pc),a0
	bsr	PUSH_SPRITE
	rts
	
ROUND_EVENT_INJECT_SPECIAL:
	nop
	moveq	#SPR_TYPE_ENEMY_1X16,d3
	or.w	2(a0),d3				; Parameter Sets Delay Speed
	moveq	#SPR_SPECIAL,d0
	move.w	MAP_PIXEL_POSX(a4),d1
	add.w	#16,d1
	moveq	#0,d2
	lea	HDL_SPECIAL(pc),a0
	bsr	PUSH_SPRITE
	rts
	
ROUND_EVENT_INJECT_ELDER:
	nop
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	or.w	2(a0),d3				; Parameter Sets Delay Speed
	moveq	#SPR_ELDER,d0
	move.w	MAP_PIXEL_POSX(a4),d1
	add.w	#16,d1
	moveq	#0,d2
	lea	HDL_ELDER(pc),a0
	bsr	PUSH_SPRITE
	rts

ROUND_EVENT_INJECT_APE:
	nop
	FUNCID	#$db4e452b
	moveq	#SPR_APE,d0
	move.w	MAP_PIXEL_POSX(a4),d1
	add.w	#16,d1
	move.w	#80,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	or.w	2(a0),d3				; Parameter Sets Delay Speed
	lea	HDL_APE(pc),a0
	bsr	PUSH_SPRITE
	rts


ROUND_EVENT_INJECT_FISH:
	nop
	FUNCID	#$db4e452b
	moveq	#SPR_FISH,d0
	move.w	MAP_PIXEL_POSX(a4),d1
	add.w	#16,d1
	move.w	#FISH_YPOS,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	or.w	2(a0),d3				; Parameter Sets Delay Speed
	lea	HDL_FISH(pc),a0
	bsr	PUSH_SPRITE
	rts


ROUND_EVENT_INJECT_MUTANTFROG:
	nop
	FUNCID	#$219312b6
	moveq	#SPR_MUTANTFROG,d0
	move.w	MAP_PIXEL_POSX(a4),d1
	add.w	#16,d1
	move.w	#146,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	or.w	2(a0),d3				; Parameter Sets Delay Speed
	lea	HDL_MUTANTFROG(pc),a0
	bsr	PUSH_SPRITE
	rts
	


ROUND_EVENT_TOGGLE_BACKGROUND:
	nop
	FUNCID	#$c5d54b04
	not.w	BACKGROUND_ENABLE(a4)
	rts
	
;
ROUND_EVENT_INJECT_ROPELINE:
	nop
	move.l	#242,d0
	move.l	#0,d1
	move.l	d0,d2
	move.l	#186,d3
        move.l  #40*5,d4	

        lea     LSTPTR_FRONT_SCREEN(a4),a1
        lea     LSTPTR_BACK_SCREEN(a4),a2
        lea     LSTPTR_POST_SCREEN(a4),a3
        addq.w  #8,a1
        addq.w  #8,a2
	addq.w	#8,a3
        moveq   #2-1,d7
	
	moveq	#0,d6
	move.w	MAP_PIXEL_POSX(a4),d6
	lsr.w	#3,d6
	
.loop:
        move.l  d7,-(a7)
        move.l  (a1)+,a0
        add.l   d6,a0
        bsr     ROPE_LINE

        move.l  (a2)+,a0
        add.l   d6,a0
        bsr     ROPE_LINE

        move.l  (a3)+,a0
        add.l   d6,a0
        bsr     ROPE_LINE	
	
        move.l  (a7)+,d7
        dbf     d7,.loop
	rts
	
	
; d4 = ITEM_XXXXXPOWER
ROUND_EVENT_INJECT_WEAPON:
	nop
	FUNCID	#$db4e452b
	
;Even Round - Enemy kills before weapon unlock: 75 (Delay before drop).
;OR!!!
;Odd Round - Random number between -10  to 20 seconds.

        lea     ROUND_PLATFORMS(a4),a0
        moveq   #0,d0
        move.w  MAP_PIXEL_POSX(a4),d0

        move.w  FRAME_BG(a4),d1 ; If odd or even frame
        moveq   #-16,d6          ; If Left add 16 pixels
        and.w   #$1,d1
        bne.s   .left
.right:
        sub.l   #(12*16),d0     ; Start from right.
        moveq   #16,d6         ; If right subtract 16 pixels
.left:                          ; Start from left

        moveq   #10-1,d7        ; Move 10 blocks max

.loop:  move.l  d0,d1
        lsr.w   #4,d1
        move.l  (a0,d1*4),d1            ; get the block
        bne.s   .get_y                  ; no platform?
        add.w   d6,d0                   ; Go further right or left.
        dbf     d7,.loop
        moveq   #-1,d0
        bra.s   .exit
.get_y
        move.b  d1,d2
        lsr.l   #4,d2
        and.w   #$f,d2
        beq.s   .found
        lsr.l   #4,d1
        bra.s   .get_y

.found: subq.w #1,d1           ; subtrack 32 from y position
                                ; Not needed due to Y Border anyway.
        lsl.w   #4,d1
; d0 = Start X Position in Map (d0=1 if no platform could be found).
; d1 = Start Y Position

	move.w	d1,d2
	move.w	d0,d1

; Work out 
	moveq	#SPR_WEAPON,d0				; y Pos
	moveq	#SPR_TYPE_ENEMY_1X16,d3
	lsr.w	#2,d4
	or.w	d4,d3			; Set parameter
	
	lea	HDL_WEAPON(pc),a0
	bsr	PUSH_SPRITE
	
	moveq	#SND_ITEMDROP,d0
	bsr	PLAY_SAMPLE
	
.exit:
        rts
	
	
	
ROUND_EVENT_VSCROLL_START:
	nop
	FUNCID	#$c5d54b04
	move.l	a0,-(a7)
	clr.w	BACKGROUND_ENABLE(a4)				; Disable the background
	bsr	DESTROY_ALL_ENEMIES				; Destroy all enemies
	
; Set last map position to just before
	bsr	SET_LAST_MAP_POSITION					; Set the last map position at the bottom of the rope.
								; Rygar will respawn here if he loses a life in the cave.
; Stop Lava Cycle
	clr.l	ROUND_FG_LAVA_CYCLE(a4)				; disable lava colour cycle
	move.w	#-1,VERTSCROLL_ENABLE(a4)			; Enable vertical scrolling
	move.b	#-1,SCROLL_EDGE_LOCK_X(a4)			; Lock all horizontal scrolling
	clr.w	ROUND_HEIGHT_STATE(a4)				; Round height state initially to zero
								; this is so Rygar cannot jump off the rope early.
; Rope enable
	move.w	#-1,ROPE_LINE_ENABLE(a4)			; Enable the rope line
	clr.w	ROPE_LINE_DETACH(a4)				; hmmmmm...
; Setup lines
	move.w	#130,ROPE_LINE_UPPER_X1(a4)			; Set the rope line co-ordintes.
	move.w	#130,ROPE_LINE_UPPER_X2(a4)
	move.w	#0,ROPE_LINE_UPPER_Y1(a4)
	move.w	#89,ROPE_LINE_UPPER_Y2(a4)

	move.w	#130,ROPE_LINE_LOWER_X1(a4)
	move.w	#130,ROPE_LINE_LOWER_X2(a4)
	move.w	#90,ROPE_LINE_LOWER_Y1(a4)
	move.w	#186,ROPE_LINE_LOWER_Y2(a4)
	
	clr.w	RYGAR_SWING_STATE(a4)
	
	move.w	#16,RYGAR_SHORT_CLIMB_FRAMES(a4)		; This causes Rygar to do a short
								; Climb up the rope from the bottom, once activated.
	move.l	(a7)+,a0
	move.w	2(a0),DISMOUNT_ROPE_ADJUST(a4)			; Parameter sets rope dismount direction
								
								; When Rygar climbs from the bottom, but then dismounts
								; again from the bottom then he needs to just moved to 
								; the left slightly so that the rope can be re-enabled.
								; -1 for left, 0 for right	
	rts
	
	
SET_LAST_POSITION:
	nop
	bsr	CHECKPOINTS_OFF
	rts
	
;------------------------------------------------------------
; Eventy Pointers Must Match Offsets Table.
; Pointers....
EVTNUM_BACKGROUND_ON:			equ	0
EVTNUM_BACKGROUND_OFF:			equ	1
EVTNUM_CHECKPOINTS_OFF:			equ	2
EVTNUM_CHECKPOINTS_ON:			equ	3
EVTNUM_ROUND_EVENT_TOGGLE_BACKGROUND:	equ	4
EVTNUM_ROUND_EVENT_VSCROLL_START:	equ	5
EVTNUM_ROUND_EVENT_INJECT_APE:		equ	6
EVTNUM_ROUND_EVENT_INJECT_BIGRHINO:	equ	7
EVTNUM_ROUND_EVENT_INJECT_CRAB:		equ	8
EVTNUM_ROUND_EVENT_INJECT_FIREBALL:	equ	9
EVTNUM_ROUND_EVENT_INJECT_GIANTDEMON:	equ	10
EVTNUM_ROUND_EVENT_INJECT_GIANTWORM:	equ	11
EVTNUM_ROUND_EVENT_INJECT_MUTANTFROG:	equ	12	
EVTNUM_ROUND_EVENT_INJECT_SQUIRREL:	equ	13
EVTNUM_ROUND_EVENT_INJECT_TRIBESMEN:	equ	14
EVTNUM_ROUND5_EVENT_INJECT_LIZARDS:	equ	15
EVTNUM_ROUND6_EVENT_INJECT_VILLAGERS_1:	equ	16
EVTNUM_ROUND6_EVENT_INJECT_VILLAGERS_2:	equ	17
EVTNUM_ROUND6_EVENT_INJECT_VILLAGERS_3:	equ	18
EVTNUM_ROUND6_EVENT_INJECT_VILLAGERS_4:	equ	19
EVTNUM_ROUND9_EVENT_INJECT_LIZARDS:	equ	20
EVTNUM_ROUND11_EVENT_INJECT_LIZARDS:	equ	21
EVTNUM_ROUND_EVENT_INJECT_SPECIAL:	equ	22
EVTNUM_ROUND_EVENT_INJECT_BIGRHINO_WITH_RIDER:	equ	23
EVTNUM_ROUND17_EVENT_INJECT_VILLAGERS_1:	equ	24
EVTNUM_ROUND17_EVENT_INJECT_VILLAGERS_2:	equ	25
EVTNUM_ROUND17_EVENT_INJECT_VILLAGERS_3:	equ	26
EVTNUM_ROUND17_EVENT_INJECT_VILLAGERS_4:	equ	27
EVTNUM_ROUND17_EVENT_INJECT_VILLAGERS_5:	equ	28
EVTNUM_ROUND17_EVENT_INJECT_VILLAGERS_6:	equ	29
EVTNUM_ROUND18_EVENT_INJECT_VILLAGERS_1:	equ	30
EVTNUM_ROUND18_EVENT_INJECT_VILLAGERS_2:	equ	31
EVTNUM_ROUND18_EVENT_INJECT_VILLAGERS_3:	equ	32
EVTNUM_ROUND18_EVENT_INJECT_VILLAGERS_4:	equ	33
EVTNUM_ROUND18_EVENT_INJECT_VILLAGERS_5:	equ	34
EVTNUM_ROUND18_EVENT_INJECT_VILLAGERS_6:	equ	35
EVTNUM_ROUND18_EVENT_INJECT_VILLAGERS_7:	equ	36
EVTNUM_ROUND_EVENT_INJECT_GRIFFIN:		equ	37
EVTNUM_ROUND_EVENT_INJECT_LAVAMAN:		equ	38
EVTNUM_ROUND_EVENT_INJECT_FISH:			equ	39
EVTNUM_ROUND23_EVENT_INJECT_LIZARDS:		equ	40
EVTNUM_ROUND24_EVENT_INJECT_LIZARDS:		equ	41
EVTNUM_ROUND30_EVENT_AUTOSCROLL:		equ	42
EVTNUM_SET_LAST_POSITION:		equ	43
EVTNUM_ROUND_EVENT_INJECT_ELDER:	equ	44
EVTNUM_ROUND_EVENT_INJECT_ROPELINE:	equ	45

;------------------------------------------------------------
; Offsets Table...
EVENT_POINTERS:
	dc.l	BACKGROUND_ON-CODE_BASE					;0
	dc.l	BACKGROUND_OFF-CODE_BASE				;1
	dc.l	CHECKPOINTS_OFF-CODE_BASE				;2
	dc.l	CHECKPOINTS_ON-CODE_BASE				;3
	dc.l	ROUND_EVENT_TOGGLE_BACKGROUND-CODE_BASE			;4
	dc.l	ROUND_EVENT_VSCROLL_START-CODE_BASE			;5
	dc.l	ROUND_EVENT_INJECT_APE-CODE_BASE			;6
	dc.l	ROUND_EVENT_INJECT_BIGRHINO-CODE_BASE			;7
	dc.l	ROUND_EVENT_INJECT_CRAB-CODE_BASE			;8
	dc.l	ROUND_EVENT_INJECT_FIREBALL-CODE_BASE			;9
	dc.l	ROUND_EVENT_INJECT_GIANTDEMON-CODE_BASE			;10	
	dc.l	ROUND_EVENT_INJECT_GIANTWORM-CODE_BASE			;11
	dc.l	ROUND_EVENT_INJECT_MUTANTFROG-CODE_BASE			;12
	dc.l	ROUND_EVENT_INJECT_SQUIRREL-CODE_BASE			;13
	dc.l	ROUND_EVENT_INJECT_TRIBESMEN-CODE_BASE			;14
	dc.l	ROUND5_EVENT_INJECT_LIZARDS-CODE_BASE			;15
	dc.l	ROUND6_EVENT_INJECT_VILLAGERS_1-CODE_BASE		;16
	dc.l	ROUND6_EVENT_INJECT_VILLAGERS_2-CODE_BASE		;17
	dc.l	ROUND6_EVENT_INJECT_VILLAGERS_3-CODE_BASE		;18
	dc.l	ROUND6_EVENT_INJECT_VILLAGERS_4-CODE_BASE		;19
	dc.l	ROUND9_EVENT_INJECT_LIZARDS-CODE_BASE			;20
	dc.l	ROUND11_EVENT_INJECT_LIZARDS-CODE_BASE			;21
	dc.l	ROUND_EVENT_INJECT_SPECIAL-CODE_BASE			;22
	dc.l	ROUND_EVENT_INJECT_BIGRHINO_WITH_RIDER-CODE_BASE	;23
	dc.l	ROUND17_EVENT_INJECT_VILLAGERS_1-CODE_BASE		;24
	dc.l	ROUND17_EVENT_INJECT_VILLAGERS_2-CODE_BASE		;25
	dc.l	ROUND17_EVENT_INJECT_VILLAGERS_3-CODE_BASE		;26
	dc.l	ROUND17_EVENT_INJECT_VILLAGERS_4-CODE_BASE		;27
	dc.l	ROUND17_EVENT_INJECT_VILLAGERS_5-CODE_BASE		;28
	dc.l	ROUND17_EVENT_INJECT_VILLAGERS_6-CODE_BASE		;29
	dc.l	ROUND18_EVENT_INJECT_VILLAGERS_1-CODE_BASE		;30
	dc.l	ROUND18_EVENT_INJECT_VILLAGERS_2-CODE_BASE		;31
	dc.l	ROUND18_EVENT_INJECT_VILLAGERS_3-CODE_BASE		;32
	dc.l	ROUND18_EVENT_INJECT_VILLAGERS_4-CODE_BASE		;33
	dc.l	ROUND18_EVENT_INJECT_VILLAGERS_5-CODE_BASE		;34
	dc.l	ROUND18_EVENT_INJECT_VILLAGERS_6-CODE_BASE		;35	
	dc.l	ROUND18_EVENT_INJECT_VILLAGERS_7-CODE_BASE		;36	
	dc.l	ROUND_EVENT_INJECT_GRIFFIN-CODE_BASE			;37
	dc.l	ROUND_EVENT_INJECT_LAVAMAN-CODE_BASE			;38	
	dc.l	ROUND_EVENT_INJECT_FISH-CODE_BASE			;39
	dc.l	ROUND23_EVENT_INJECT_LIZARDS-CODE_BASE			;40
	dc.l	ROUND24_EVENT_INJECT_LIZARDS-CODE_BASE			;41
	dc.l	ROUND30_EVENT_AUTOSCROLL-CODE_BASE			;42	
	dc.l	SET_LAST_POSITION-CODE_BASE				;43
	dc.l	ROUND_EVENT_INJECT_ELDER-CODE_BASE			;44
	dc.l	ROUND_EVENT_INJECT_ROPELINE-CODE_BASE			;45