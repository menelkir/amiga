; Initiate Auto Scroll...
	
; Load new palette into copper.

; Set new blit copy parameters for background



; When stopped... disable background blitting.
	
	

END_SEQUENCE:
	tst.w	RYGAR_CELEBRATE(a4)
	bmi	ENDSEQ_DISPLAY_TEXT
	tst.w	SHOW_RYGAR_WIN(a4)
	bmi.s	ENDSEQ_WIN
	cmp.w	#$810,MAP_PIXEL_POSX(a4)				; 
	beq	ENDSEQ_LOAD_BOSS_PALETTE
	cmp.w	#$820,MAP_PIXEL_POSX(a4)				; 
	beq	ENDSEQ_INJECT_LIGAR
	cmp.w	#$8f4,MAP_PIXEL_POSX(a4)				; Pan right until end sequence
	beq	ENDSEQ_START_BOSS
	cmp.w	#$8f6,MAP_PIXEL_POSX(a4)				; Pan right until end sequence
	beq	ENDSEQ_WIN
	bsr	PAN_RIGHT
.exit:	rts

ENDSEQ_WIN:
	moveq	#MUSIC_ENDING,d0
	bsr	PLAY_TUNE
	
	bsr	TRANSITION_TO_BLACK
	
	clr.w	BACKGROUND_ENABLE(a4)
	
	;move.l  MEMCHK1_BACK_BSCROLL(a4),a0
        ;bsr     CLEAR_BG_BITPLANES
        move.l  MEMCHK1_FRONT_BSCROLL(a4),a0
        bsr     CLEAR_BG_BITPLANES
	
	moveq	#0,d4
	lea	SPRITE_SLOTS(a4),a0     ; Sprites base pointer
	move.l	(a0,d4*4),a0
	add.w	(a0),a0                 ; a0 now has sprite address
; Rygar sprite context now in a0
	move.w	#0,SPRITE_PLOT_XPOS(a0)
	
	moveq	#0,d0
	bsr	DESTROY_SPRITE
	
	;clr.w	SPRITE_PLOT_YPOS(a0)
	
	move.w	#-1,INPUT_DISABLED(a4)
	move.w	#-1,RYGAR_CELEBRATE(a4)
	
; Go to Black...
; Inject sprites...
; Display text....
; Timer.... > HiScore.

	moveq	#SPR_CROWD,d0
	move.w	MAP_PIXEL_POSX(a4),d1
	move.w	#114,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	or.w	#2<<8,d3
	lea	HDL_CROWD(pc),a0
	bsr	PUSH_SPRITE
	
	moveq	#SPR_CROWD,d0
	move.w	MAP_PIXEL_POSX(a4),d1
	move.w	#114,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	lea	HDL_CROWD(pc),a0
	bsr	PUSH_SPRITE

	moveq	#SPR_CROWD,d0
	move.w	MAP_PIXEL_POSX(a4),d1
	move.w	#114,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	or.w	#1<<8,d3
	lea	HDL_CROWD(pc),a0
	bsr	PUSH_SPRITE

	move.w  #NOTES_WAIT_TIME,NOTES_TIMER(a4)
        clr.b   NOTES_TEXT_DONE(a4)
        clr.b   NOTES_TEXT_XPOS(a4)
        clr.b   NOTES_TEXT_YPOS(a4)
	
; Won game text
	lea     ENDSEQ_TEXT(pc),a0
        move.l	a0,NOTES_MESSAGE_PTR(a4)
	rts

ENDSEQ_DISPLAY_TEXT:
        lea     LSTPTR_FRONT_SCREEN(a4),a1
        bsr     ENDSEQ_NOTES_LINE
	btst.b	#7,$bfe001
	bne.s	.exit
	move.w	#-1,RYGAR_THE_END(a4)
	clr.w	INPUT_DISABLED(a4)
.exit:	rts

ENDSEQ_LOAD_BOSS_PALETTE:
	move.l	COPPTR_GRADIENT_START(a4),a0
	move.w	2(a0),d0
	swap	d0
	move.w	6(a0),d0
	move.l	d0,a0
	
	lea	BOSS_PALETTE(pc),a1
.loop:	cmp.w	#$88,(a0)
	beq.s	.exit
	
	cmp.w	#$106,(a0)
	bne.s	.next
.load:
	moveq	#0,d0
	move.w	2(a0),d0
	lsr.w	#8,d0
	lsr.w	#5,d0			; Get palette number
	move.w	(a1,d0*2),6(a0)
	
.next:
	addq.w	#4,a0
	bra.s	.loop

.exit:
	bsr	PAN_RIGHT
	rts

	CNOP	0,4
	
BOSS_PALETTE:	dc.w	$030,$005,$019,$05d,$09f,$0f0,$0cf,$0a0
		
	CNOP	0,4
	
	
ENDSEQ_INJECT_LIGAR:
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#SPR_LIGAR1,d0
	move.w	MAP_PIXEL_POSX(a4),d1
	add.w	#160,d1
	move.w	#112,d2
	moveq	#SPR_TYPE_ENEMY_2X64,d3
	lea	HDL_LIGAR1(pc),a0
	bsr	PUSH_SPRITE
	movem.l	(a7)+,d0-d7/a0-a3
	
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#SPR_LIGAR2,d0
	move.w	MAP_PIXEL_POSX(a4),d1
	add.w	#160+32,d1
	move.w	#112,d2
	moveq	#SPR_TYPE_ENEMY_2X64,d3
	lea	HDL_LIGAR2(pc),a0
	bsr	PUSH_SPRITE
	movem.l	(a7)+,d0-d7/a0-a3
	
	bsr	PAN_RIGHT
	rts
	
	
ENDSEQ_START_BOSS:
	clr.w	INPUT_DISABLED(a4)
	clr.w	INIT_END_SEQUENCE(a4)
	move.w	#-1,LIGAR_FIGHT(a4)
	rts
	
	

;a0=
ENDSEQ_NOTES_LINE:
        movem.l d0-d7/a0-a3,-(a7)
        tst.b   NOTES_TEXT_DONE(a4)
        bmi     .done

        tst.b   NOTES_TEXT_WAIT(a4)
        beq.s   .do_char
        sub.b   #1,NOTES_TEXT_WAIT(a4)
        bra     .done

.do_char:
        move.b  NOTES_CHAR_DELAY(a4),NOTES_TEXT_WAIT(a4)
.next_y:
        moveq   #0,d0
        moveq   #0,d1


	move.l	NOTES_MESSAGE_PTR(a4),a0
        move.b  NOTES_TEXT_XPOS(a4),d0
        moveq   #0,d5
        move.b  NOTES_TEXT_YPOS(a4),d5
        cmp.b   #NOTES_TEXT_LINES,d5
        bne.s   .cont
        move.b  #-1,NOTES_TEXT_DONE(a4)
        bra     .done

.cont:
        add.b   d5,d1
        add.b   d1,d1
        addq.w  #2,d1
        lsl.l   #5,d5                           ; Get Y Line
        add.l   d5,a0                           ; y position
        add.w   d0,a0                           ; x position

        moveq   #0,d7
        moveq   #0,d6
        add.w   #1,d0                           ; index in 3 from left
        lea     LSTPTR_FRONT_SCREEN(a4),a1
        bsr     ENDSEQ_NOTES_CHAR
        lea     LSTPTR_BACK_SCREEN(a4),a1
        bsr     ENDSEQ_NOTES_CHAR
        lea     LSTPTR_POST_SCREEN(a4),a1
        bsr     ENDSEQ_NOTES_CHAR
	
        cmp.b   #31,NOTES_TEXT_XPOS(a4)
        bne.s   .not_xpos
        clr.b   NOTES_TEXT_XPOS(a4)
        add.b   #1,NOTES_TEXT_YPOS(a4)
        bra.s   .next_y

.not_xpos:
        add.b   #1,NOTES_TEXT_XPOS(a4)
.done:  movem.l (a7)+,d0-d7/a0-a3
        rts



; d0=8x8 x
; d1=8x8 y
; d6=Colour number 0-3 (0=BP1, 1=BP2, 2=BP3, 3=BP4, 4=BP5)
; d7=-1 Draw on nibble 0=byte
; a0=pointer to char to display
; a1=Screen addresses
ENDSEQ_NOTES_CHAR:
        movem.l d0-d7/a0-a3,-(a7)
        lsl.w   #3,d1
        mulu    #PLAYFIELD_SIZE_X*5,d1
        lsl.w   #2,d6


        move.l  (a1,d6),a1              ; Bitplane to draw in.

        add.l   d1,a1
        add.w   d0,a1
        sub.l   #(PLAYFIELD_SIZE_X*5)*8,a1

        addq.w  #4,a1

        moveq   #0,d0
        ;lea    FONT_ASSET(a4)+86,a2    ; start of BODY
        move.l  FONT_ASSET(a4),a2
        add.w   #86,a2                  ; Offset to BODY
        move.b  (a0),d0

        sub.b   #32,d0

.draw:  add.w   d0,a2           ; index into character

        move.b  (a2),d0
        or.b    d0,(a1)
        or.b    d0,PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,PLAYFIELD_SIZE_X*4(a1)


        move.b  FONT_WIDTH*1(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*1)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*1)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*1)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*1)+PLAYFIELD_SIZE_X*4(a1)

        move.b  FONT_WIDTH*2(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*2)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*2)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*2)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*2)+PLAYFIELD_SIZE_X*4(a1)

        move.b  FONT_WIDTH*3(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*3)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*3)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*3)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*3)+PLAYFIELD_SIZE_X*4(a1)

        move.b  FONT_WIDTH*4(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*4(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*4)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*4)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*4)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*4)+PLAYFIELD_SIZE_X*4(a1)

        move.b  FONT_WIDTH*5(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*5(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*5)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*5)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*5)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*5)+PLAYFIELD_SIZE_X*4(a1)

        move.b  FONT_WIDTH*6(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*6(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*6)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*6)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*6)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*6)+PLAYFIELD_SIZE_X*4(a1)

        move.b  FONT_WIDTH*7(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*7(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*7)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*7)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*7)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*7)+PLAYFIELD_SIZE_X*4(a1)

.exit:  movem.l (a7)+,d0-d7/a0-a3
        rts

ENDSEQ_TEXT:
	dc.b	"       PEACE HAS AGAIN COME     " 
	dc.b	"           TO THE WORLD         "
	dc.b	"                                "
	dc.b	"    DEDICATED TO CLARE AND LEO  "
	dc.b	"       LONG LIVE THE AMIGA!     "
	dc.b	"                                "
	dc.b	"                                "
	dc.b	"                                "
	dc.b	"                                "
	dc.b	"                                "
	dc.b	"                                "
	ds.b	32
	even