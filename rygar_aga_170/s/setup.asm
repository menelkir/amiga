GET_IFF_RAW:
	nop
	nop
	FUNCID	#$cd9e67f1
.loop:  cmp.l   #"BODY",(a0)
        beq.s   .ok
        addq.w  #2,a0
        bra.s   .loop
.ok:    addq.w  #8,a0
        rts

CLEAR_FRONT_SCREEN_BUFFERS:
        move.l  MEMCHK1_FRONT_FSCROLL(a4),a0
        move.l  #$a000,d7
        lsr.l   #2,d7
.loop1: clr.l   (a0)+
        dbf     d7,.loop1
	rts

CLEAR_BACK_SCREEN_BUFFERS:
        move.l  MEMCHK1_BACK_FSCROLL(a4),a0
        move.l  #$a000,d7
        lsr.l   #2,d7
.loop1: clr.l   (a0)+
        dbf     d7,.loop1
	rts


CLEAR_BUFFERS:
	FUNCID	#$9ec5476a
        move.l  MEMBASE_CHUNK1(a4),a0
        move.l  #MEMSIZE_CHUNK1,d7
        lsr.l   #2,d7
.loop1: clr.l   (a0)+
        dbf     d7,.loop1

        move.l  MEMBASE_CHUNK6(a4),a0
        move.l  #MEMSIZE_CHUNK6,d7
        lsr.l   #2,d7
.loop3: clr.l   (a0)+
        dbf     d7,.loop3

        rts

PROCESS_TILES:
	FUNCID	#$d25d8b7b
        cmp.l   #"FORM",(a0)
        bne     .return

.findbody:
        cmp.l   #"BODY",(a0)
        beq.s   .found
        addq.w  #2,a0
        bra.s   .findbody
.found:
        addq.w  #8,a0
        move.l  a0,TILE_ASSETS_PTR(a4)      ; save pointer to start of tiles
.return:
        rts

GET_IFF_BODY:
	FUNCID	#$39ac81b7
.loop:  cmp.l   #"BODY",(a0)
        beq.s   .ok
        addq.w  #2,a0
        bra.s   .loop
.ok:    addq.w  #8,a0
        rts


; a0=Pointer to CMAP
GET_IFF_PALETTE:
		nop
		nop
		FUNCID	#$b5f4671f
.loop:          cmp.l   #"CMAP",(a0)
                beq.s   .ok
                addq.w  #2,a0
                bra.s   .loop
.ok:            addq.w  #4,a0
                move.l  (a0)+,d7                ; number of bytes
                divu    #3,d7
                and.l   #$ffff,d7
                subq.w  #1,d7

.pal:           moveq   #0,d0
                move.b  (a0)+,d0
                lsl.l   #8,d0
                move.b  (a0)+,d0
                lsl.l   #8,d0
                move.b  (a0)+,d0

                move.l  d0,d1
                and.l   #$00f0f0f0,d0
                and.l   #$000f0f0f,d1

                moveq   #0,d2
                lsr.l   #4,d0
                move.b  d0,d2
                lsr.l   #4,d0
                or.b    d0,d2
                lsr.l   #4,d0
                and.w   #$f00,d0
                or.w    d0,d2
                move.w  d2,(a1)+                ; High nibbles first

                moveq   #0,d2
                move.b  d1,d2
                lsr.l   #4,d1
                or.b    d1,d2
                lsr.l   #4,d1
                and.w   #$f00,d1
                or.w    d1,d2
                move.w  d2,(a1)+                ; High nibbles first
                dbf     d7,.pal
.exit:          rts

;a0 = RGB Palette.
LOAD_BACKGROUND_PALETTE:		
                lea     $180(a5),a1

                moveq   #0,d0
                moveq   #8-1,d5

.bank:          move.l  d0,d1
                lsl.l   #8,d1
                lsl.l   #5,d1

                moveq   #0,d6
                or.w    d1,d6                   ; Palette bank select
                move.w  d6,BPLCON3(a5)
                move.w  (a0)+,(a1)              ; load high colour
                eor.w   #$200,d6
                move.w  d6,BPLCON3(a5)
                move.w  (a0)+,(a1)              ; load low colour
                addq.w  #1,d0
                dbf     d5,.bank
                rts

;a0 = RGB Palette.
LOAD_BACKGROUND_PALETTE_COPPER:
		FUNCID	#$c21e499a
                moveq   #0,d0
                moveq   #8-1,d2

.bank:          move.l  d0,d1
                lsl.l   #8,d1
                lsl.l   #5,d1

                moveq   #0,d6
                or.w    d1,d6                   ; Palette bank select
                move.w	#BPLCON3,(a1)+
		move.w	d6,(a1)+
		
		move.w	#$180,(a1)+
		move.w  (a0)+,(a1)+              ; load high colour
                
		eor.w   #$200,d6
		
                move.w	#BPLCON3,(a1)+
		move.w	d6,(a1)+           
		move.w	#$180,(a1)+
		move.w  (a0)+,(a1)+              ; load high colour	   
                addq.w  #1,d0
                dbf     d2,.bank
                rts


LOAD_FOREGROUND_PALETTE:
		FUNCID	#$a62d38f7
		
                moveq   #0,d0                   ; select palette bank
                moveq   #8-1,d5                 ; 8 * 32 palettes

.bank:          move.l  d0,d1
                lsl.l   #8,d1
                lsl.l   #5,d1                   ; bit 13

                moveq   #32-1,d7
                lea     PAL_RYGAR_FG(a4),a0
                lea     $dff180,a1
.colour:
                moveq   #0,d6
                or.w    d1,d6                   ; Palette bank select
                move.w  d6,BPLCON3(a5)
                move.w  (a0)+,(a1)              ; load high colour
                or.w    #$200,d6
                move.w  d6,BPLCON3(a5)
                move.w  (a0)+,(a1)              ; load low colour
                addq.w  #2,a1

                dbf     d7,.colour

                addq.w  #1,d0
                dbf     d5,.bank
                rts

