
open       =-30
close      =-30-6
read       =-42
write      =-48
IoErr      =-132
mode_old   =1005
mode_new   =1006

FILE_RYGAR_SUNSET_BG:   equ     -1
FILE_RYGAR_MOON_BG:   equ     -1

LOADER:
	FUNCID	#$77b4fd2c


.loop:
        moveq   #0,d0
        move.w  FILE_INDEX(a4),d0
        lea     FILE_TABLE(a4),a2
        lsl.w   #5,d0
        add.l   d0,a2
	
; a2 points to file record to load
; Allocate Ram for file to load to.
	tst.l	(a2)
	bmi	.done
        move.l  4(a2),d0                        ; size (if -1 then don't allocate)
        bmi   .noalloc
        move.l  #MEMF_CHIP,d1                   ; Chip Rame
        movem.l d1-d7/a0-a4,-(a7)
        bsr     ALLOCATE_RAM                    ; d0=pointer to allocated ram
        movem.l (a7)+,d1-d7/a0-a4
        move.l	d0,d6
	tst.l   d0
        bne.s   .ram_ok
; Failed to allocate ram here.	
	movem.l	d0-d7/a0-a6,-(a7)
	clr.b	LOADINFO_TEXT_XPOS(a4)
	moveq	#0,d6	
	lea	LOADINFO_TEXT_NORAM(a4),a0
	bsr	DRAW_LOADINFO_LINE
	movem.l	(a7)+,d0-d7/a0-a6
	
        moveq   #-1,d0
        bra	.done

.ram_ok:

.skip_first2:

        IFEQ    ENABLE_OCEAN_LOADER
;--- DOS LOAD ---
        move.l  0(a2),d1                ; d1 = filename
        bmi     .done

        lea     STR_LOADING(a4),a0      
	bsr     PRINT_STRING
        move.l  d1,a0
	
        lea     STR_LOADING(a4),a0
	bsr     OPEN_EXISTING_FILE
        move.l  d0,d4                   ; Save File Handle
        tst.l   d0
        beq     DOS_ERROR

        move.l  d6,d2                   ; Buffer
        moveq	#-1,d3			; Read all.
        bsr     READ_FILE
	
        tst.l   d0
        beq     DOS_ERROR

        move.l  d4,d1                   ; Load File handle
        bsr     CLOSE_FILE
;--- END OF DOS LOAD ---
        ELSE
        move.l  (a2),d1                ; filename
        bmi     .done
	move.l	d6,a0
        move.l  d1,a1
        move.l  a2,d2
        move.l  MEMCHK5_LOADBUFF(a4),a2
        moveq   #0+64,d0
        sub.l   a3,a3
        move.w  #$4000,$dff09a
        bsr     ocean
	beq	DOS_ERROR
        moveq   #-3,d0
	beq	DOS_ERROR	
        move.l  d2,a2
        ENDC

.noalloc:
; Store Allocated address.
        move.l  8(a2),a1
	move.l	d6,(a1)			; Store pointer
	
;a0=Packed file address
;a1=unpack address.
	move.l	d6,a0			; Source
	move.l	d6,a1			; Dest
        bsr     Unpack

; Initialise if required
        move.l  16(a2),a0
        cmp.l   #-1,a0                                  ; Do we need to catll a patch routine.
        beq.s   .next
        movem.l d0-d7/a0-a6,-(a7)
        jsr     (a0)
        movem.l (a7)+,d0-d7/a0-a6
.next:
        addq.w  #1,FILE_INDEX(a4)
        bra     .loop
.done:  rts


; d1=Filename ptr to load
; d2=Address to load
; d3=Bytes to load
LOAD_FILE_INTO_RAM:
	FUNCID	#$6c161f80
        bsr     OPEN_EXISTING_FILE
        bsr     READ_FILE
        tst.l   d0
        beq     DOS_ERROR

        bsr     CLOSE_FILE
.dos_close:
        bsr     CLOSE_DOSLIB
        moveq   #0,d0
.exit:  rts

;d1= filename
OPEN_EXISTING_FILE:             ;*open file,mode in D0
	FUNCID	#$64900d96
        move.l  DOSBASE(a4),a6       ;DOS base address in A6
        move.l  #mode_old,d2
        jsr     open(a6)         ;open file
        rts


;d2=colour
DOS_ERROR:
	FUNCID	#$3c2cc985
	
        move.l  DOSBASE(a4),a6       ;address of library name
        jsr     IoErr(a6)        ;call IoErr for error info
        move.l  d0,d5
        move.l  #100000,d7
.loop:  nop
        nop
        move.w  d2,$dff180
        bra.s   .loop
        dbf     d7,.loop
        move.w  #$000,$dff180
        moveq   #-1,d0                  ; failed to open.
        rts

; d1 = file handle
; d2 = buff pointer
; d3 = read amount
READ_FILE:
	FUNCID	#$ba6e05f8
        move.l  DOSBASE(a4),a6     ;DOS base address in A6
        move.l  #$ffffff,d3    ;read an arbitrary number of bytes
        jsr     read(a6)       ;read data
        rts

;d1= filename
OPEN_NEW_FILE:                  ;*open file,mode in D0
	FUNCID	#$a92a62dc
        move.l  DOSBASE(a4),a6       ;DOS base address in A6
        move.l  #mode_new,d2
        jsr     open(a6)         ;open file
        move.l  d0,FILEHD(a4)        ;save handle
        rts

CLOSE_FILE:                      ;*close file
	FUNCID	#$8e5c7e79
        move.l  DOSBASE(a4),a6       ;DOS base address in A6
        jsr     close(a6)        ;close file
        rts

; Initialise Sprites while they are in memory.
ONLOAD_INIT_SPRITES:
	FUNCID	#$7f6e4de9
	
; First off we need to create the mirrors of the required sprites.
	
; Next we create the sprite masks in memory (for blit speed)
        move.l  IMG_SPRITES(a4),a0                      ; (i) Sprite Iff data
        move.l  MEMCHK4_SPRMASK(a4),a2                  ; (o) Pointer to masks buffer to store masks
        bsr     CREATE_SPRITE_MASKS


; Now build a list of pointers to each sprite and it's mask.
; Each pointer is to a 16x16 word address - because in places we use 16x16 sprites.
        move.l  STCPTR_SPRITE_ASSETS(a4),a0
        move.l  MEMCHK4_SPRMASK(a4),a1                  ; a1 pointer to mask data
        ;lea    SPRITE_POINTERS(a4),a2                  ; a2 pointer to list to be stored
        move.l  MEMCHK0_SPRITE_POINTERS(a4),a2
        bsr     CREATE_SPRITE_ADDRESS_POINTERS
	
; Create the required sprite mirrors
	bsr	CREATE_MIRROR_SPRITES
	
; Next we create the sprite masks in memory (for blit speed)
        move.l  IMG_SPRITES(a4),a0                      ; (i) Sprite Iff data
        move.l  MEMCHK4_SPRMASK(a4),a2                  ; (o) Pointer to masks buffer to store masks
        bsr     CREATE_SPRITE_MASKS

        bsr     INIT_SPRITE_FREE_LIST
        ;lea    GLOBAL_SPRITES(a4),a1                   ; sprites to initialize straight away
        ;bsr    INIT_SPRITES
        rts

; Initialise Tiles.
ONLOAD_INIT_TILES:
	FUNCID	#$ae19b233
        move.l  IMG_TILES(a4),a0
        move.l  a0,TILES_PTR(a4)
        move.l  LSTPTR_CURRENT_SCREEN(a4),a1
        bsr     PROCESS_TILES
        rts

; This converts the IFF of sprites into Hardware sprites.
ONLOAD_INIT_HWSPRITES:
	FUNCID	#$46c52837
        move.l  HW16_SPRITES(a4),a0                     ; a1 = destination
        lea     LSTPTR_HARDWARE_SPRITE(a4),a3
        bsr     BUILD_HWSPR_POINTERS			; Get a list of pointers to each hardware sprite
        bsr     BUILD_HWSPRITE_COORDS           	; Build x/y co-ordinates for hardware sprites.
        rts


ONLOAD_INIT_LOADINFO:
	FUNCID	#$7a18b739
        bsr     INIT_BITPLANE_POINTERS
        bsr     INIT_DISPLAY_POINTERS
	bsr	CLEAR_BUFFERS
        bsr     CLEAR_FRONT_SCREEN_BUFFERS
	bsr	CLEAR_BACK_SCREEN_BUFFERS
	bsr	INIT_LOADINFO
	
	clr.b	LOADINFO_TEXT_XPOS(a4)
	moveq	#0,d6	
	lea	LOADINFO_TEXT_LOADING(a4),a0
	bsr	DRAW_LOADINFO_LINE
.exit:	rts

; Create Letsfight mask
ULM
ONLOAD_CREATE_CAPTION_MASK:
	movem.l	d0-d7/a0-a3,-(a7)
        move.l  IMG_CAPTION(a4),a0
        add.w   #$B0,a0
        move.l  MASK_CAPTION(a4),a1
        ;add.w   #$B0,a3
	
	moveq	#0,d0
	moveq	#0,d1

	moveq	#32-1,d6

.loop_y:
	move.l	a0,a2
	move.l	a1,a3
	
	moveq	#11-1,d7
	
.loop:
	move.l	(a2),d2
	or.l	22(a2),d2
	or.l	44(a2),d2
	or.l	66(a2),d2
	or.l	88(a2),d2
	
	move.l	d2,(a3)
	move.l	d2,22(a3)
	move.l	d2,44(a3)
	move.l	d2,66(a3)
	move.l	d2,88(a3)
	addq.l	#4,a2
	addq.l	#4,a3
	dbf	d7,.loop
	
	
	add.l	#22*6,a0
	add.l	#22*6,a1

	dbf	d6,.loop_y

	movem.l	(a7)+,d0-d7/a0-a3
	rts
	
	

;d1=round number
; Background will be loaded to MEMCHK10_BACKGROUND(a4) and map will be loaded to MEMCHK3_TILEMAP(a4)
DISKLOAD_ROUND:
	FUNCID	#$c2bfa60e
	
	LEVEL5_STOP
	
; Clear tilemap and config areas
	move.l	#MEMSIZE_CHUNK3-1,d7
        move.l  MEMCHK3_TILEMAP(a4),a0          ; dest	
.clear:	clr.b	(a0)+
	dbf	d7,.clear
	
; Load tile map
        moveq   #0,d4
        move.w  ROUND_CURRENT(a4),d4
	add.w	d4,d4
        lea     ROUND_TABLE(a4),a0
        move.l  4(a0,d4*8),a1
        move.l  MEMCHK3_TILEMAP(a4),a0          ; dest
        move.l  MEMCHK5_LOADBUFF(a4),a2
        sub.l   a3,a3
        moveq   #0+64,d0

        IFNE    ENABLE_DEBUG
        move.w  #$4000,$dff09a
        ELSE
        move.w #$8,$dff09a
        ENDC
        bsr     ocean
	beq	DOS_ERROR
        moveq   #-3,d0
        bsr     ocean
        IFNE    ENABLE_DEBUG
        move.w  #$4000,$dff09a
        ELSE
        move.w #$8,$dff09a
        ENDC
        ;bsr     ocean
        move.l  MEMCHK3_TILEMAP(a4),a0          ; source	
        move.l  MEMCHK3_TILEMAP(a4),a1          ; dest
        bsr     Unpack

; Load level configuration.
	IFEQ	DEBUG_ROUND
        moveq   #0,d4
        move.w  ROUND_CURRENT(a4),d4
	add.w	d4,d4
        lea     ROUND_TABLE(a4),a0
        move.l  8(a0,d4*8),a1
        move.l  MEMCHK3_ROUNDCONFIG(a4),a0          ; dest
        move.l  MEMCHK5_LOADBUFF(a4),a2
        sub.l   a3,a3
        moveq   #0+64,d0

        IFNE    ENABLE_DEBUG
        move.w  #$4000,$dff09a
        ELSE
        move.w #$8,$dff09a
        ENDC
        bsr     ocean
	beq	DOS_ERROR
        moveq   #-3,d0
        bsr     ocean
        IFNE    ENABLE_DEBUG
        move.w  #$4000,$dff09a
        ELSE
        move.w #$8,$dff09a
        ENDC
        bsr     ocean
	beq	DOS_ERROR
	ENDC
	
	move.l  MEMCHK3_ROUNDCONFIG(a4),a0              ; source
        move.l  MEMCHK3_ROUNDCONFIG(a4),a1              ; dest
        bsr     Unpack
	
; Load the background.
        moveq   #0,d4
        move.w  ROUND_CURRENT(a4),d4
	add.w	d4,d4
        lea     ROUND_TABLE(a4),a0
        move.l  (a0,d4*8),a1
        cmp.l   #FILE_RYGAR_SUNSET_BG,a1
        beq.s   .drives_off_and_exit
        move.l  MEMCHK10_BACKGROUND(a4),a0              ; dest
        move.l  MEMCHK5_LOADBUFF(a4),a2
        sub.l   a3,a3
        moveq   #0+64,d0

        IFNE    ENABLE_DEBUG
        move.w  #$4000,$dff09a
        ELSE
        move.w #$8,$dff09a
        ENDC
        bsr     ocean
	beq	DOS_ERROR
        moveq   #-3,d0
        bsr     ocean
        IFNE    ENABLE_DEBUG
        move.w  #$4000,$dff09a
        ELSE
        move.w #$8,$dff09a
        ENDC
        ;bsr     ocean

        move.l  MEMCHK10_BACKGROUND(a4),a0              ; source
        move.l  MEMCHK10_BACKGROUND(a4),a1              ; dest
        bsr     Unpack

;Stop drive motors
.drives_off_and_exit:
        lea     $bfd100,a0
        or.b    #$f8,(a0)
        nop
        and.b   #$87,(a0)
        nop
        or.b    #$78,(a0)
        nop
	
	LEVEL5_START
	rts

