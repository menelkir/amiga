; d0=Number of bombs bnus

NOTES_WINDOW_X: equ     32
NOTES_WINDOW_Y: equ     32

NOTES_SCORE_STEP:       equ     200
NOTES_DELAY_TIME:       equ     10              ; 7 seconds

NOTES_WATER_DELAY:      equ     20
;NOTES_CHAR_DELAY:       equ     2

NOTES_SCENE_SIZE_Y:     equ     184
NOTES_SCENE_BITPLANES:  equ     5
NOTES_SCENE_COLOURS:    equ     32

NOTES_WAIT_TIME:        equ     120

NOTES_WATER_ANIM_YPOS:  equ     152

NOTES_TEXT_LINES:       equ     11

NOTES:

	tst.w	SHOWED_NOTES(a4)
	bmi.s	.clear_panels

; Clear panel areas.
	move.l	MEMCHK9_UPPER_PANEL(a4),a0
	move.l	#$12c0-1,d7
.upper:	clr.b	(a0)+
	dbf	d7,.upper
	
	move.l	MEMCHK9_LOWER_PANEL(a4),a0
	move.l	#$c80-1,d7
.lower:	clr.b	(a0)+
	dbf	d7,.lower	
	
.clear_panels:
        move.w  #NOTES_WAIT_TIME,NOTES_TIMER(a4)
        clr.b   NOTES_TEXT_DONE(a4)
        clr.b   NOTES_TEXT_XPOS(a4)
        clr.b   NOTES_TEXT_YPOS(a4)

        lea     NOTES_SCENE_COLOUR_PAL(a4),a1
        move.w  #TEXT_RGB,62(a1)
	
        IFEQ    ENABLE_DEBUG
	bsr	WAIT_FOR_VERTICAL_BLANK	
        move.w  #%0000000110000000,DMACON(a5)		; Disable Copper
	bsr	WAIT_FOR_VERTICAL_BLANK
	ENDC
	
	bsr	CLEAR_FRONT_SCREEN_BUFFERS
	bsr	CLEAR_BACK_SCREEN_BUFFERS
        bsr     INIT_NOTES_COPPER

        IFEQ    ENABLE_DEBUG
	move.w	#COPRUN_GAMENOTES,COPRUN_TYPE(a4)	
        move.l  MEMCHK2_COPPER1(a4),COP1LCH(a5)
        move.l  MEMCHK2_COPPER1(a4),COP2LCH(a5)
        ;         FEDCBA9876543210
        move.w  #%1000011111000000,DMACON(a5)
        ENDC

        bsr     SETSCR_FBUFF
        lea     LSTPTR_FRONT_SCREEN(a4),a1
        bsr     SET_FRAMEBUFF

        bsr     NOTES_TRANS_BLACK_TO_COLOUR

.vb:
        moveq   #NOTES_WATER_DELAY-1,d7                         ; Time Delay

.frame: move.l  d7,-(a7)
        bsr     WAIT_FOR_VERTICAL_BLANK
        bsr     DRAW_NOTES_LINE
	tst.w	SHOWED_NOTES(a4)
        bmi.s	.skip_flash
	bsr     NOTES_FREE_FLASH
.skip_flash:
	move.l  (a7)+,d7
        dbf     d7,.frame

        btst.b  #7,$bfe001
        bne.s   .vb
.wait:  btst.b  #7,$bfe001
        beq.s   .wait
        bra.s   .end

        ;subq.w #1,NOTES_TIMER(a4)
        ;tst.w  NOTES_TIMER(a4)
        ;bmi.s  .end

        bra.s   .vb

.end:
        bsr     NOTES_TRANS_COLOUR_TO_BLACK

.exit:

.exit_NOTES:    moveq   #0,d6
.rts:           rts



; Transition scene colours from Black to its paletter
NOTES_TRANS_BLACK_TO_COLOUR:
        moveq   #15,d7

.loop:  bsr     WAIT_FOR_VERTICAL_BLANK
        lea     NOTES_SCENE_BLACK_PAL(a4),a0
        lea     NOTES_SCENE_COLOUR_PAL(a4),a1
        lea     NOTES_SCENE_FLUID_PAL(a4),a2
        lea     NOTES_SCENE_COLOURS*2(a0),a3
.next:  move.w  (a0)+,d2
        move.w  (a1)+,d1
        bsr     COLOUR_INTERPOLATE
        move.w  d0,(a2)+
        cmp.l   a0,a3
        bne.s   .next

        bsr     NOTES_COPY_PAL_TO_COPPER
        dbf     d7,.loop
        rts


; Transition scene colours from Black to its paletter
NOTES_TRANS_COLOUR_TO_BLACK:
        moveq   #15,d7

.loop:  bsr     WAIT_FOR_VERTICAL_BLANK
        lea     NOTES_SCENE_COLOUR_PAL(a4),a0
        lea     NOTES_SCENE_BLACK_PAL(a4),a1
        lea     NOTES_SCENE_FLUID_PAL(a4),a2
        lea     NOTES_SCENE_COLOURS*2-2(a0),a3
.next:  move.w  (a0)+,d2
        move.w  (a1)+,d1
        bsr     COLOUR_INTERPOLATE
        move.w  d0,(a2)+
        cmp.l   a0,a3
        bne.s   .next

        bsr     NOTES_COPY_PAL_TO_COPPER
        dbf     d7,.loop
        rts




;a0=
DRAW_NOTES_LINE:
        movem.l d0-d7/a0-a3,-(a7)
        tst.b   NOTES_TEXT_DONE(a4)
        bmi     .done

        tst.b   NOTES_TEXT_WAIT(a4)
        beq.s   .do_char
        sub.b   #1,NOTES_TEXT_WAIT(a4)
        bra     .done

.do_char:
        move.b  NOTES_CHAR_DELAY(a4),NOTES_TEXT_WAIT(a4)
.next_y:
        moveq   #0,d0
        moveq   #0,d1


	move.l	NOTES_MESSAGE_PTR(a4),a0
        move.b  NOTES_TEXT_XPOS(a4),d0
        moveq   #0,d5
        move.b  NOTES_TEXT_YPOS(a4),d5
        cmp.b   #NOTES_TEXT_LINES,d5
        bne.s   .cont
        move.b  #-1,NOTES_TEXT_DONE(a4)
        bra     .done

.cont:
        add.b   d5,d1
        add.b   d1,d1
        addq.w  #2,d1
        lsl.l   #5,d5                           ; Get Y Line
        add.l   d5,a0                           ; y position
        add.w   d0,a0                           ; x position

        moveq   #0,d7
        moveq   #0,d6
        add.w   #1,d0                           ; index in 3 from left
        lea     LSTPTR_FRONT_SCREEN(a4),a1
        bsr     DRAW_NOTES_CHAR

        cmp.b   #31,NOTES_TEXT_XPOS(a4)
        bne.s   .not_xpos
        clr.b   NOTES_TEXT_XPOS(a4)
        add.b   #1,NOTES_TEXT_YPOS(a4)
        bra.s   .next_y

.not_xpos:
        add.b   #1,NOTES_TEXT_XPOS(a4)
.done:  movem.l (a7)+,d0-d7/a0-a3
        rts



; d0=8x8 x
; d1=8x8 y
; d6=Colour number 0-3 (0=BP1, 1=BP2, 2=BP3, 3=BP4, 4=BP5)
; d7=-1 Draw on nibble 0=byte
; a0=pointer to char to display
; a1=Screen addresses
DRAW_NOTES_CHAR:
        movem.l d0-d7/a0-a3,-(a7)
        lsl.w   #3,d1
        mulu    #PLAYFIELD_SIZE_X*5,d1
        lsl.w   #2,d6


        move.l  (a1,d6),a1              ; Bitplane to draw in.

        add.l   d1,a1
        add.w   d0,a1
        sub.l   #(PLAYFIELD_SIZE_X*5)*8,a1

        addq.w  #3,a1

        moveq   #0,d0
        ;lea    FONT_ASSET(a4)+86,a2    ; start of BODY
        move.l  FONT_ASSET(a4),a2
        add.w   #86,a2                  ; Offset to BODY
        move.b  (a0),d0

        sub.b   #32,d0

.draw:  add.w   d0,a2           ; index into character

        move.b  (a2),d0
        or.b    d0,(a1)
        or.b    d0,PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,PLAYFIELD_SIZE_X*4(a1)


        move.b  FONT_WIDTH*1(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*1)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*1)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*1)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*1)+PLAYFIELD_SIZE_X*4(a1)

        move.b  FONT_WIDTH*2(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*2)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*2)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*2)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*2)+PLAYFIELD_SIZE_X*4(a1)

        move.b  FONT_WIDTH*3(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*3)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*3)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*3)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*3)+PLAYFIELD_SIZE_X*4(a1)

        move.b  FONT_WIDTH*4(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*4(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*4)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*4)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*4)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*4)+PLAYFIELD_SIZE_X*4(a1)

        move.b  FONT_WIDTH*5(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*5(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*5)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*5)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*5)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*5)+PLAYFIELD_SIZE_X*4(a1)

        move.b  FONT_WIDTH*6(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*6(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*6)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*6)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*6)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*6)+PLAYFIELD_SIZE_X*4(a1)

        move.b  FONT_WIDTH*7(a2),d0
        or.b    d0,PLAYFIELD_SIZE_X*5*7(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*7)+PLAYFIELD_SIZE_X*1(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*7)+PLAYFIELD_SIZE_X*2(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*7)+PLAYFIELD_SIZE_X*3(a1)
        or.b    d0,(PLAYFIELD_SIZE_X*5*7)+PLAYFIELD_SIZE_X*4(a1)

.exit:  movem.l (a7)+,d0-d7/a0-a3
        rts


NOTES_FREE_FLASH:
                lea     ACTIVE_PLAYER_PAL(a4),a0
                addq.w  #1,ACTIVE_PLAYER_PTR(a4)
                move.w  ACTIVE_PLAYER_PTR(a4),d0
                lsr.w   #1,d0
                move.w  (a0,d0*2),d0
                tst.w   d0
                bpl.s   .free

                clr.w   ACTIVE_PLAYER_PTR(a4)
                moveq   #0,d0
                move.w  (a0,d0*2),d0

.free:          move.l  COPPTR_FREE_FLASH(a4),a0
                move.w  ACTIVE_PLAYER(a4),d1
                move.w  d0,6(a0)
                rts

;
; INIT_NOTES_COPPER
;
; Initialize the Copper list
;
INIT_NOTES_COPPER:
        move.l  MEMCHK2_COPPER1(a4),a1
        move.l  a1,d4

        move.w  #DDFSTRT,(a1)+  ;
        move.w  #$0038,(a1)+    ; 3c
        move.w  #DDFSTOP,(a1)+  ;
        move.w  #$00d0,(a1)+    ; a4

        move.l  #$1c01ff00,(a1)+
        move.l  a1,COPPTR_UPPER_PANEL(a4)
        bsr     INIT_COPPER_SPRITES

        bsr     COPPER_UPPER_PANEL

        move.l  #$3e0dfffe,(a1)+

        move.w  #FMODE,(a1)+
        move.w  #%0000000000001100,(a1)+

        move.w  #BPLCON0,(a1)+  ; 0             ; This is play area
        move.w  #$0000,(a1)+    ; 2             ; 8 PLanes here
        move.w  #BPLCON1,(a1)+  ; 4
        move.w  #0,(a1)+        ; 6
        move.w  #BPLCON2,(a1)+  ; 8
        move.w  #$00,(a1)+      ; 30
        move.w  #BPL1MOD,(a1)+  ;
        move.w  #$a0,(a1)+      ; a0
        move.w  #BPL2MOD,(a1)+  ;
        move.w  #$a0,(a1)+      ; a0

        move.w  #DIWSTRT,(a1)+  ;
        move.w  #$27a1,(a1)+    ;
        move.w  #DIWSTOP,(a1)+  ;
        move.w  #$10a8,(a1)+    ;

        lea     COPPTR_HARDWARE_UPPER_SPRITE_0(a4),a3
        bsr     CREATE_COPPER_SPRITE_POINTERS

; Load Main Palette
        lea     NOTES_SCENE_BLACK_PAL(a4),a0
        move.l  #$180,d2
        moveq   #NOTES_SCENE_COLOURS-1,d7

        move.l  a1,COPPTR_NOTES_SCENE_PAL(a4)

.main_pal:
        move.w  d2,(a1)+
        move.w  (a0)+,(a1)+
        addq.w  #2,d2
        dbf     d7,.main_pal

        move.l  #$4001ff00,(a1)+
        move.w  #BPLCON0,(a1)+  ; 0             ; This is play area
        move.w  #$5200,(a1)+    ; 2

        lea     LSTPTR_FRONT_SCREEN(a4),a0
        moveq   #NOTES_SCENE_BITPLANES-1,d7                     ; number of planes to load
        move.w  #BPL0PTL,d2
        move.w  #BPL0PTH,d3

.plane: move.l  (a0)+,d0
        move.w  d2,(a1)+
        move.w  d0,(a1)+
        swap    d0
        move.w  d3,(a1)+
        move.w  d0,(a1)+
        addq.l  #4,d2
        addq.l  #4,d3
        dbf     d7,.plane

	cmp.w	#CAPTION_TYPE_THE_END,CAPTION_TYPE(a4)
	beq	.skip
; Blue Line
        move.l  #$4501ff00,(a1)+
        move.l  #$0180008f,(a1)+
        move.l  #$4601ff00,(a1)+
        move.l  #$01800000,(a1)+

        move.l  #$4801ff00,(a1)+                
        move.l  #$01be0f00,(a1)+
        move.l  #$4901ff00,(a1)+                
        move.l  #$01be0f11,(a1)+
        move.l  #$4a01ff00,(a1)+               
        move.l  #$01be0f22,(a1)+
        move.l  #$4b01ff00,(a1)+                
        move.l  #$01be0f33,(a1)+
        move.l  #$4c01ff00,(a1)+                
        move.l  #$01be0f44,(a1)+
        move.l  #$4d01ff00,(a1)+                
        move.l  #$01be0f55,(a1)+
        move.l  #$4e01ff00,(a1)+                
        move.l  #$01be0f66,(a1)+	
        move.l  #$4f01ff00,(a1)+                
        move.l  #$01be0f77,(a1)+	
        move.l  #$5001ff00,(a1)+
        move.l  #$01be0000,(a1)+

        move.l  #$5801ff00,(a1)+                
        move.l  #$01be00a0,(a1)+
        move.l  #$5901ff00,(a1)+                
        move.l  #$01be01b1,(a1)+
        move.l  #$5a01ff00,(a1)+                
        move.l  #$01be02c2,(a1)+
        move.l  #$5b01ff00,(a1)+                
        move.l  #$01be03d3,(a1)+
        move.l  #$5c01ff00,(a1)+                
        move.l  #$01be04e4,(a1)+
        move.l  #$5d01ff00,(a1)+                
        move.l  #$01be05f5,(a1)+
        move.l  #$5e01ff00,(a1)+                
        move.l  #$01be06f6,(a1)+
        move.l  #$5f01ff00,(a1)+                
        move.l  #$01be07f7,(a1)+
        move.l  #$6001ff00,(a1)+
        move.l  #$01be0000,(a1)+

        move.l  a1,COPPTR_FREE_FLASH(a4)
        move.l  #$7801ff00,(a1)+                ; This game is free
        move.l  #$01be0a00,(a1)+
        move.l  #$9001ff00,(a1)+
        move.l  #$01be0000,(a1)+

        move.l  #$a801ff00,(a1)+                
        move.l  #$01be0aaa,(a1)+
        move.l  #$a901ff00,(a1)+                
        move.l  #$01be0bbb,(a1)+
        move.l  #$aa01ff00,(a1)+                
        move.l  #$01be0ccc,(a1)+
        move.l  #$ab01ff00,(a1)+                
        move.l  #$01be0ddd,(a1)+
        move.l  #$ac01ff00,(a1)+                
        move.l  #$01be0ccc,(a1)+
        move.l  #$ad01ff00,(a1)+                
        move.l  #$01be0bbb,(a1)+
        move.l  #$ae01ff00,(a1)+                
        move.l  #$01be0aaa,(a1)+
        move.l  #$af01ff00,(a1)+                
        move.l  #$01be0999,(a1)+
        move.l  #$b001ff00,(a1)+
        move.l  #$01be0000,(a1)+

        move.l  #$b801ff00,(a1)+                
        move.l  #$01be0f00,(a1)+
        move.l  #$b901ff00,(a1)+                
        move.l  #$01be0f10,(a1)+
        move.l  #$ba01ff00,(a1)+                
        move.l  #$01be0f20,(a1)+
        move.l  #$bb01ff00,(a1)+                
        move.l  #$01be0f30,(a1)+
        move.l  #$bc01ff00,(a1)+               
        move.l  #$01be0f40,(a1)+
        move.l  #$bd01ff00,(a1)+                
        move.l  #$01be0f50,(a1)+
        move.l  #$be01ff00,(a1)+                
        move.l  #$01be0f60,(a1)+
        move.l  #$bf01ff00,(a1)+                
        move.l  #$01be0f70,(a1)+	
        move.l  #$c001ff00,(a1)+                
        move.l  #$01be0000,(a1)+

        move.l  #$c801ff00,(a1)+                
        move.l  #$01be0f40,(a1)+
        move.l  #$c901ff00,(a1)+                
        move.l  #$01be0f50,(a1)+
        move.l  #$ca01ff00,(a1)+                
        move.l  #$01be0f60,(a1)+
        move.l  #$cb01ff00,(a1)+                
        move.l  #$01be0f70,(a1)+
        move.l  #$cc01ff00,(a1)+               
        move.l  #$01be0f80,(a1)+
        move.l  #$cd01ff00,(a1)+                
        move.l  #$01be0f90,(a1)+
        move.l  #$ce01ff00,(a1)+                
        move.l  #$01be0fa0,(a1)+
        move.l  #$cf01ff00,(a1)+                
        move.l  #$01be0fb0,(a1)+	
        move.l  #$d001ff00,(a1)+                
        move.l  #$01be0000,(a1)+

        move.l  #$d801ff00,(a1)+                
        move.l  #$01be0f80,(a1)+
        move.l  #$d901ff00,(a1)+                
        move.l  #$01be0f90,(a1)+
        move.l  #$da01ff00,(a1)+                
        move.l  #$01be0fa0,(a1)+
        move.l  #$db01ff00,(a1)+                
        move.l  #$01be0fb0,(a1)+
        move.l  #$dc01ff00,(a1)+               
        move.l  #$01be0fc0,(a1)+
        move.l  #$dd01ff00,(a1)+                
        move.l  #$01be0fd0,(a1)+
        move.l  #$de01ff00,(a1)+                
        move.l  #$01be0fe0,(a1)+
        move.l  #$df01ff00,(a1)+                
        move.l  #$01be0ff0,(a1)+	
        move.l  #$e001ff00,(a1)+                
        move.l  #$01be0000,(a1)+
	
        move.l  #$e801ff00,(a1)+                
        move.l  #$01be0fa0,(a1)+
        move.l  #$e901ff00,(a1)+                
        move.l  #$01be0fa1,(a1)+
        move.l  #$ea01ff00,(a1)+                
        move.l  #$01be0fa2,(a1)+
        move.l  #$eb01ff00,(a1)+                
        move.l  #$01be0fa3,(a1)+
        move.l  #$ec01ff00,(a1)+               
        move.l  #$01be0fa4,(a1)+
        move.l  #$ed01ff00,(a1)+                
        move.l  #$01be0fa5,(a1)+
        move.l  #$ee01ff00,(a1)+                
        move.l  #$01be0fa6,(a1)+
        move.l  #$ef01ff00,(a1)+                
        move.l  #$01be0fa7,(a1)+		
        move.l  #$f001ff00,(a1)+
        move.l  #$01be0000,(a1)+

        move.l  #$f101ff00,(a1)+
        move.l  #$0180008f,(a1)+
        move.l  #$f201ff00,(a1)+
        move.l  #$01800000,(a1)+

; Set display properties, modulo, bitplanes...
.skip:
        bsr     COPPER_LOWER_PANEL

	lea	DUMMY_BPLCON0(a4),a3
        move.l  a3,COPPTR_BPLCON(a4)

        move.l  #$fffffffe,(a1)+
        rts

NOTES_COPY_PAL_TO_COPPER:
        move.l  a0,-(a7)
        move.l  a1,-(a7)
        lea     NOTES_SCENE_FLUID_PAL(a4),a0
        move.l  COPPTR_NOTES_SCENE_PAL(a4),a1

        rept    32
        move.w  (a0)+,2(a1)
        addq.w  #4,a1
        endr

        move.l  (a7)+,a1
        move.l  (a7)+,a0
        rts

