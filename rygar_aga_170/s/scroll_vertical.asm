VERTICAL_SCROLL_SPEED:  equ     2
VERTICAL_WAIT_MAX:      equ     192

;-----------------------------------------------------------------

BLIT_TILE_ONSCROLL_Y:
	FUNCID	#$43871c22
	
        tst.w   VERTSCROLL_ENABLE(a4)
        beq     .exit
        move.w  MAP_PIXEL_POSY(a4),d0                           ; if moving up then it is at the top
                                                                ; if down then it is at the bottom
        tst.b   SCROLL_DIRECTION_Y(a4)
        beq     .exit
        bmi.s   .up
        add.w   #VERTICAL_WAIT_MAX,d0

.up:
        ;COL0_PURPLE
        move.w  d0,d1
        move.w  d0,d2
        and.w   #$f,d1                          ; d1 = x position
        lsr.w   #4,d2                           ; d2 = Y position

        moveq   #0,d0
        move.w  d2,d0
        lea     MAP_TABLE_Y1(a4),a1             ; Index to start of Map
        move.l  ROUND_TILEMAP(a4),a0
        ;move.w MAP_POSY(a4),d0
        add.l   (a1,d0*4),a0
; d1 has remainder

        moveq   #0,d0
        move.w  MAP_POSX(a4),d0
        add.l   d0,a0                   ;	 Index into map (far left)


        sub.w   #(MAP_COMPENSATE/16)*2,a0                       ; This is the daft collision compensate problem
						; 256/16 * 2 = 32

        addq.w  #2,a0

        move.w  VIDEO_SPLIT_LAST_POSY(a4),d2

        moveq   #0,d3

        moveq   #20-1,d7
.tileloop:
        move.w  (a0)+,d0
        bsr     CHECK_TILE_COMMANDS
        and.w   #%0000001111111111,d0   		; Only tiles below 1024
        move.l  LSTPTR_POST_SCREEN(a4),a1               ; Screen location
        bsr     BLIT_TILE_LINE

        addq.w  #1,d3
        dbf     d7,.tileloop
.exit:  rts

VERT_USEBLITTER:        equ     1

; Restore the post screen to the current screen
; 1) From the POST screen start at line VSLAST and copy VERTOP lines to 0 CURRENT
; 2) From the POST screen start at line 0 and copy VSLAST-1 to VERTOP in CURRENT
VERTSCROLL_RESTORE:
	FUNCID	#$8bc3c7f3
	
        moveq   #0,d2
        move.w  #VERTICAL_WAIT_MAX,d2
        sub.w   VIDEO_SPLIT_LAST_POSY(a4),d2
        move.w  d2,VERTCOPY_LINES_TOP(a4)
        move.w  VIDEO_SPLIT_LAST_POSY(a4),VERTCOPY_LINES_BOTTOM(a4)

        move.l  LSTPTR_POST_SCREEN(a4),a0               ; Screen location
        move.l  LSTPTR_CURRENT_SCREEN(a4),a1            ; Screen location

        moveq   #0,d0
        add.w   MAP_POSX(a4),d0

        add.l   d0,a0
        add.l   d0,a1
        move.l  d0,d5

        move.l  a0,d6
        move.l  a1,d7
;
; Copy from POST (0,VSPLIT_POSY) to CURRENT(0)

        lea     CANVAS_TABLE_Y3(a4),a2
        moveq   #0,d2
        move.w  VIDEO_SPLIT_LAST_POSY(a4),d2    ; Get Vsplit Y Position
        add.l   (a2,d2.w*4),a0                  ; Index into POST screen.
; a0 now has start of copy.

        moveq   #0,d0
        move.w  VERTCOPY_LINES_TOP(a4),d0
	addq.w	#2,d0
        bmi.s   .skip_top
        beq.s   .skip_top

	move.w	d0,d1			; Mutliply d0 by 5 for height * bitplanes
	add.w	d0,d0
        add.w   d0,d0
	add.w	d1,d0
	
	addq.w	#2,a0
	addq.w	#2,a1
	
        lsl.w   #6,d0                   ; Upper 10 bits for Height
        add.w   #16,d0            	; Lower 6 bits for number of words.
        WAIT_FOR_BLITTER
        move.w  #$09f0,BLTCON0(a5)
        move.w  #$0000,BLTCON1(a5)      ; Select straigt A-D mode $f0
        move.l  #-1,BLTAFWM(a5)         ; No masking needed
        move.w  #8,BLTAMOD(a5)          ; Playfield Modulo Src
        move.w  #8,BLTDMOD(a5)          ; Playfield Modulo Dest
        move.l  a0,BLTAPTH(a5)          ; set source address
        move.l  a1,BLTDPTH(a5)          ; set dest address to back screen
        move.w  d0,BLTSIZE(a5)          ; boom.....

.skip_top
; Copy lines from Top of the Post screen to Bottom of the current screen.
        move.l  d6,a0
        move.l  d7,a1

        moveq   #0,d2
        move.w  VERTCOPY_LINES_TOP(a4),d2
	add.w	VERTICAL_COMPENSATE(a4),d2		Round 5 = 2, Round 9 = 0
        add.l   (a2,d2.w*4),a1          ; Index into Verical Top Lines into Destination.

        moveq   #0,d0
        move.w  VIDEO_SPLIT_LAST_POSY(a4),d0
	addq.w	#2,d0
        bmi     .fb
        beq     .fb

	move.w	d0,d1		; Mutliply d0 by 5 for height * bitplanes
	add.w	d0,d0
        add.w   d0,d0
	add.w	d1,d0
	
	addq.w	#2,a0
	addq.w	#2,a1
	
        lsl.w   #6,d0
        add.w   #16,d0                    ; d0 has blit size (100 words)

        WAIT_FOR_BLITTER
        move.w  #$09f0,BLTCON0(a5)
        move.w  #$0000,BLTCON1(a5)      ; Select straigt A-D mode $f0
        move.l  #-1,BLTAFWM(a5)         ; No masking needed
        move.w  #8,BLTAMOD(a5)          ; Playfield Modulo Src
        move.w  #8,BLTDMOD(a5)          ; Playfield Modulo Dest
        move.l  a0,BLTAPTH(a5)          ; set source address
        move.l  a1,BLTDPTH(a5)          ; set dest address to back screen
        move.w  d0,BLTSIZE(a5)          ; boom.....

; Event to restore the post screen, used
; when verical scrolling is done.
        tst.w   ROUND_RESTORE_POST(a4)
        beq     .exit
	subq.w	#2,MAP_PIXEL_POSX(a4)
        
	clr.w   ROUND_RESTORE_POST(a4)

        move.l  LSTPTR_CURRENT_SCREEN(a4),a0            ; Destination
        move.l  LSTPTR_POST_SCREEN(a4),a1               ; Source
        add.l   d5,a0					; Add offset
        add.l   d5,a1					; Add offset

        move.l  #186,d0					; 186 lines
        add.w   d0,d0					; *2
        lsl.w   #6,d0					
        add.w   #(20*5)/2,d0                    ; d0 has blit size (100 words)

; Current Screen is the Source
; Post Screen is the Destination
        WAIT_FOR_BLITTER
        move.w  #$09f0,BLTCON0(a5)
        move.w  #$0000,BLTCON1(a5)      ; Select straigt A-D mode $f0
        move.l  #-1,BLTAFWM(a5)         ; No masking needed
        move.w  #0,BLTAMOD(a5)          ; Playfield Modulo Src
        move.w  #0,BLTDMOD(a5)          ; Playfield Modulo Dest
        move.l  a0,BLTAPTH(a5)          ; set source address
        move.l  a1,BLTDPTH(a5)          ; set dest address to back screen
        move.w  d0,BLTSIZE(a5)          ; boom.....

; Now restore the Front Screen
        move.l  LSTPTR_POST_SCREEN(a4),a0
        move.l  LSTPTR_FRONT_SCREEN(a4),a1
        add.l   d5,a0
        add.l   d5,a1
	
        WAIT_FOR_BLITTER
        move.w  #$09f0,BLTCON0(a5)
        move.w  #$0000,BLTCON1(a5)      ; Select straigt A-D mode $f0
        move.l  #-1,BLTAFWM(a5)         ; No masking needed
        move.w  #0,BLTAMOD(a5)          ; Playfield Modulo Src
        move.w  #0,BLTDMOD(a5)          ; Playfield Modulo Dest
        move.l  a0,BLTAPTH(a5)          ; set source address
        move.l  a1,BLTDPTH(a5)          ; set dest address to back screen
        move.w  d0,BLTSIZE(a5)          ; boom.....

; Now restore the Back Screen
        move.l  LSTPTR_POST_SCREEN(a4),a0
        move.l  LSTPTR_BACK_SCREEN(a4),a1
        add.l   d5,a0
        add.l   d5,a1
	
        WAIT_FOR_BLITTER
        move.w  #$09f0,BLTCON0(a5)
        move.w  #$0000,BLTCON1(a5)      ; Select straigt A-D mode $f0
        move.l  #-1,BLTAFWM(a5)         ; No masking needed
        move.w  #0,BLTAMOD(a5)          ; Playfield Modulo Src
        move.w  #0,BLTDMOD(a5)          ; Playfield Modulo Dest
        move.l  a0,BLTAPTH(a5)          ; set source address
        move.l  a1,BLTDPTH(a5)          ; set dest address to back screen
        move.w  d0,BLTSIZE(a5)          ; boom.....

.fb:

.exit:
        rts

        CNOP    0,4

; This next routing is called for vertical scrolling
;d0=Tile Number
;d1=Y Offset within tile
;d2=Y Line number in screen to Blit to.
;d3=X Offset in Destination
;a1=Screen Pointer
BLIT_TILE_LINE:
	FUNCID	#$d02e8ec3
	
        movem.l d0-d3/a0-a1,-(a7)
        move.l  d1,-(a7)
; Find Destination tile

        add.w   MAP_POSX(a4),a1
        lea     CANVAS_TABLE_Y3(a4),a0          ; Add the Y offset
        add.l   (a0,d2*4),a1                    ; to start blit.
        add.l   d3,a1                           ; Correct X position
        add.l   d3,a1

; Find Source tile (source tile in d0)
; Fast divide by 16 with remainder
	moveq	#0,d1
	moveq	#0,d2
        move.w  d0,d1
        move.w  d0,d2
        and.w   #$f,d1                          ; d1 = x position
        lsr.w   #4,d2                           ; d2 = Y position

        lea     ASSET_TABLE_Y1(a4),a0                   ; Fast Multiply to Y
        move.l  (a0,d2.w*4),d2                                  ; Bring in the offset
        add.w   d1,d1                                           ; Bytes to words for offset
        move.l  TILE_ASSETS_PTR(a4),a0                  ; Tiles location
        add.l   d1,a0                                           ; Add X Offset
        add.l   d2,a0                                           ; Add Y offset

        move.l  (a7)+,d1

;a0 Now points to correct tile
        lea     ASSET_TABLE_Y2(a4),a2
        add.w   (a2,d1*2),a0                                    ; Get correct line to blit

;a0=Pointer to tile asset line to blit
;a1=screen line to blit to.
        WAIT_FOR_BLITTER
        move.l  #$09f00000,BLTCON0(a5)  ; Select straigt A-D mode $f0
        move.l  #-1,BLTAFWM(a5)         ; No masking needed
        move.w  #ASSET_MODULO-2,BLTAMOD(a5)             ; Playfield Modulo Src
        move.w  #CANVAS_MODULO-2,BLTDMOD(a5)            ; Playfield Modulo Dest
        move.l  a0,BLTAPTH(a5)                  ; Load the mask address
        move.l  a1,BLTDPTH(a5)
        ;move.w #1024+1,d0
        move.w  #(10*64)+1,d0                   ; Default to 1x16
        move.w  d0,BLTSIZE(a5)
        movem.l (a7)+,d0-d3/a0-a1
        rts


PAN_UP: 
	FUNCID	#$21b68a38
	
	tst.w   MAP_PIXEL_POSY(a4)
        bmi.s   .exit
	
	tst.w	RYGAR_SWING_STATE(a4)
	bne	.exit
	
	tst.w   RYGAR_DISKARM_STATE(a4)
	bne     .exit

	move.w	#-1,ROUND_HEIGHT_STATE(a4)
        move.w  MAP_PIXEL_POSY(a4),d0
        cmp.w   ROUND_HEIGHT_LIMIT(a4),d0
        ble.s   .exit
	clr.w	ROUND_HEIGHT_STATE(a4)
        move.b  #-1,SCROLL_DIRECTION_Y(a4)
        move.w  MAP_PIXEL_POSY(a4),d0
        lsr.w   #4,d0
        move.w  d0,MAP_POSY(a4)
	
        cmp.w   #0,VIDEO_SPLIT_POSY(a4)
        bge.s   .sub
        move.w  #VERTICAL_WAIT_MAX,VIDEO_SPLIT_POSY(a4)
        bra.s   .pan
.sub:   move.w  VIDEO_SPLIT_POSY(a4),VIDEO_SPLIT_LAST_POSY(a4)
        subq.w  #VERTICAL_SCROLL_SPEED,VIDEO_SPLIT_POSY(a4)
        subq.w  #VERTICAL_SCROLL_SPEED,MAP_PIXEL_POSY(a4)
	


.pan:	

.exit:  rts

        CNOP    0,4

PAN_DOWN:
	FUNCID	#$786dcc91
	
	tst.w	RYGAR_SWING_STATE(a4)
	bne	.exit
	tst.w   RYGAR_DISKARM_STATE(a4)
	bne     .exit
	
	move.w	#-1,ROUND_DEPTH_STATE(a4)
        move.w  MAP_PIXEL_POSY(a4),d0
        cmp.w   ROUND_DEPTH_LIMIT(a4),d0
        bgt	DISABLE_VERTICAL_SCROLL_LOCK				; determine if Vertical should disable
	
	clr.w	ROUND_DEPTH_STATE(a4)
        move.b  #1,SCROLL_DIRECTION_Y(a4)
        ;addq.w  #VERTICAL_SCROLL_SPEED,MAP_PIXEL_POSY(a4)
	
        move.w  MAP_PIXEL_POSY(a4),d0
        lsr.w   #4,d0
        move.w  d0,MAP_POSY(a4)

        cmp.w   #VERTICAL_WAIT_MAX,VIDEO_SPLIT_POSY(a4)
        ble.s   .sub
        move.w  #0,VIDEO_SPLIT_POSY(a4)
        bra.s   .pan
.sub:   move.w  VIDEO_SPLIT_POSY(a4),VIDEO_SPLIT_LAST_POSY(a4)
        addq.w  #VERTICAL_SCROLL_SPEED,VIDEO_SPLIT_POSY(a4)
        addq.w  #VERTICAL_SCROLL_SPEED,MAP_PIXEL_POSY(a4)

.pan:
.exit:  rts

        CNOP    0,4






PAN_CAMERA_AXIS_Y:
	FUNCID	#$890989b3

        rts

        CNOP    0,4
