
ocean:
	IFNE	FAST_LOAD
	moveq	#0,d0
	ELSE
	move.l	infodata+4,d0				; Read drive is what was used to load from Workbench or Cli.
	ENDC
	
	incbin	"ocean_loader_ciaa.68k"
	even