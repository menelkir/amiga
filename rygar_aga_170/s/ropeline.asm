HDL_ROPE:
	FUNCID	#$422dfe03
        tst.w   ROPE_LINE_ENABLE(a4)
        beq     .exit

        moveq   #0,d0
        moveq   #0,d1
        moveq   #0,d2
        moveq   #0,d3
        move.w  ROPE_LINE_UPPER_X1(a4),d0
        move.w  ROPE_LINE_UPPER_Y1(a4),d1

        move.w  ROPE_LINE_UPPER_X2(a4),d2
        ;move.w ROPE_LINE_UPPER_Y2(a4),d3
        move.w  RYGAR_YPOS(a4),d3
        sub.w   #16,d3
        move.l  #40*5,d4
        lea     LSTPTR_FRONT_SCREEN(a4),a1
        lea     LSTPTR_BACK_SCREEN(a4),a2
        addq.w  #8,a1
        addq.w  #8,a2
        moveq   #2-1,d7

.loop_upper:
        move.l  d7,-(a7)
        move.l  (a1)+,a0
        add.w   MAP_POSX(a4),a0
        bsr     ROPE_LINE

        move.l  (a2)+,a0
        add.w   MAP_POSX(a4),a0
        bsr     ROPE_LINE
        move.l  (a7)+,d7
        dbf     d7,.loop_upper

        moveq   #0,d0
        moveq   #0,d1
        moveq   #0,d2
        moveq   #0,d3
        move.w  ROPE_LINE_LOWER_X1(a4),d0
        ;move.w ROPE_LINE_LOWER_Y1(a4),d1

        move.w  RYGAR_YPOS(a4),d1
        sub.w   #16,d1
        move.w  ROPE_LINE_LOWER_X2(a4),d2
        move.w  ROPE_LINE_LOWER_Y2(a4),d3
        move.l  #40*5,d4
        lea     LSTPTR_FRONT_SCREEN(a4),a1
        lea     LSTPTR_BACK_SCREEN(a4),a2
        addq.w  #8,a1
        addq.w  #8,a2
        moveq   #2-1,d7

.loop_lower:
        move.l  d7,-(a7)
        move.l  (a1)+,a0
        add.w   MAP_POSX(a4),a0
        bsr     ROPE_LINE

        move.l  (a2)+,a0
        add.w   MAP_POSX(a4),a0
        bsr     ROPE_LINE
        move.l  (a7)+,d7
        dbf     d7,.loop_lower

        ;bsr    SWING_ROPE
.exit:  rts

SWING_ROPE:
	FUNCID	#$99e243c7
	
        lea     SWING_SINE_POINTER(a4),a0
        move.w  (a0),d0

        lea     SWING_LEFT_SINE(a4),a1
        move.w  (a1,d0*2),d0
        addq.w  #2,(a0)
        cmp.w   #$8000,d0
        bne.s   .reset
        clr.w   (a0)
        moveq   #0,d0
.reset:
        add.w   RYGAR_XPOS(a4),d0
        move.w  d0,ROPE_LINE_UPPER_X2(a4)
        move.w  d0,ROPE_LINE_LOWER_X1(a4)
        rts



;
; simpleline.asm
;
;   This example uses the line draw mode of the blitter
;   to draw a line.  The line is drawn with no pattern
;   and a simple `or' blit into a single bitplane.
;   (Link with amiga.lib)
;
;   Input:  d0=x1 d1=y1 d2=x2 d3=y2 d4=width a0=aptr
;
;        include 'exec/types.i'
;        include 'hardware/custom.i'
;        include 'hardware/blit.i'
;        include 'hardware/dmabits.i'
;        include 'hardware/hw_examples.i'
;
;
;   Our entry point.
;
ROPE_LINE:
	FUNCID	#$fa331d70
	
        ;;;;bra ROSS_LINE
        movem.l d0-d5,-(a7)
        sub.w   d0,d2           ; calculate dx
        bmi.s   xneg            ; if negative, octant is one of [3,4,5,6]
        sub.w   d1,d3           ; calculate dy   ''   is one of [1,2,7,8]
        bmi.s   yneg            ; if negative, octant is one of [7,8]
        cmp.w   d3,d2           ; cmp |dx|,|dy|  ''   is one of [1,2]
        bmi.s   ygtx            ; if y>x, octant is 2
        moveq.l #OCTANT1+LINEMODE,d5    ; otherwise octant is 1
        bra     lineagain       ; go to the common section
ygtx:
        exg     d2,d3           ; X must be greater than Y
        moveq.l #OCTANT2+LINEMODE,d5    ; we are in octant 2
        bra.s   lineagain       ; and common again.
yneg:
        neg.w   d3              ; calculate abs(dy)
        cmp.w   d3,d2           ; cmp |dx|,|dy|, octant is [7,8]
        bmi.s   ynygtx          ; if y>x, octant is 7
        moveq.l #OCTANT8+LINEMODE,d5    ; otherwise octant is 8
        bra.s   lineagain
ynygtx:
        exg     d2,d3           ; X must be greater than Y
        moveq.l #OCTANT7+LINEMODE,d5    ; we are in octant 7
        bra.s   lineagain
xneg:
        neg.w   d2              ; dx was negative! octant is [3,4,5,6]
        sub.w   d1,d3           ; we calculate dy
        bmi.s   xyneg           ; if negative, octant is one of [5,6]
        cmp.w   d3,d2           ; otherwise it's one of [3,4]
        bmi.s   xnygtx          ; if y>x, octant is 3
        moveq.l #OCTANT4+LINEMODE,d5    ; otherwise it's 4
        bra.s   lineagain
xnygtx:
        exg     d2,d3           ; X must be greater than Y
        moveq.l #OCTANT3+LINEMODE,d5    ; we are in octant 3
        bra.s   lineagain
xyneg:
        neg.w   d3              ; y was negative, in one of [5,6]
        cmp.w   d3,d2           ; is y>x?
        bmi.s   xynygtx         ; if so, octant is 6
        moveq.l #OCTANT5+LINEMODE,d5    ; otherwise, octant is 5
        bra.s   lineagain
xynygtx:
        exg     d2,d3           ; X must be greater than Y
        moveq.l #OCTANT6+LINEMODE,d5    ; we are in octant 6
lineagain:
        mulu.w  d4,d1           ; Calculate y1 * width
        ror.l   #4,d0           ; move upper four bits into hi word
        add.w   d0,d0           ; multiply by 2
        add.l   d1,a0           ; ptr += (x1 >> 3)
        add.w   d0,a0           ; ptr += y1 * width
        swap    d0              ; get the four bits of x1
        or.w    #$BFA,d0        ; or with USEA, USEC, USED, F=A+C
        lsl.w   #2,d3           ; Y = 4 * Y
        add.w   d2,d2           ; X = 2 * X
        move.w  d2,d1           ; set up size word
        lsl.w   #5,d1           ; shift five left
        add.w   #$42,d1         ; and add 1 to height, 2 to width
        btst    #DMAB_BLTDONE-8,DMACONR(a5)     ; safety check
waitblit:
        btst    #DMAB_BLTDONE-8,DMACONR(a5)     ; wait for blitter
        bne.s   waitblit
        move.w  d3,BLTBMOD(a5)  ; B mod = 4 * Y
        sub.w   d2,d3
        ext.l   d3
        move.l  d3,BLTAPT(a5)   ; A ptr = 4 * Y - 2 * X
        bpl.s   lineover        ; if negative,
        or.w    #SIGNFLAG,d5    ; set sign bit in con1
lineover:
        move.w  d0,BLTCON0(a5)  ; write control registers
        move.w  d5,BLTCON1(a5)
        move.w  d4,BLTCMOD(a5)  ; C mod = bitplane width
        move.w  d4,BLTDMOD(a5)  ; D mod = bitplane width
        sub.w   d2,d3
        move.w  d3,BLTAMOD(a5)  ; A mod = 4 * Y - 4 * X
        move.w  #$8000,BLTADAT(a5)      ; A data = 0x8000
        moveq.l #-1,d5          ; Set masks to all ones
        move.l  d5,BLTAFWM(a5)  ; we can hit both masks at once
        move.l  a0,BLTCPT(a5)   ; Pointer to first pixel to set
        move.l  a0,BLTDPT(a5)
        move.w  d1,BLTSIZE(a5)  ; Start blit
        movem.l (a7)+,d0-d5
        rts                     ; and return, blit still in progress.

