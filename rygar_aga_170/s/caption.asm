
CAPTION_TYPE_LETSFIGHT:	equ	0
CAPTION_TYPE_THE_END:	equ	1

CAPTION_WIDTH:        equ     176
CAPTION_DEPTH:        equ     16
CAPTION_BITPLANES:    equ     5

CAPTION_XPOS:         equ     32
CAPTION_YPOS:         equ     80

        IFNE    DEBUG_TEXT_ENABLE
CAPTION_PAUSE:        equ     50
        ELSE
CAPTION_PAUSE:        equ     40
        ENDC


DO_CAPTION_START:
DO_CAPTION_BLACK:
	FUNCID	#$26048ef8
	
        move.l  #CAPTION_PAUSE,d7
.caption_delay_blank:
        move.l  d7,-(a7)
        bsr     WAIT_FOR_VERTICAL_BLANK
        move.l  LSTPTR_FRONT_SCREEN(a4),a1
        bsr     BLIT_CAPTION
        move.l  (a7)+,d7
        dbf     d7,.caption_delay_blank
        rts


;DO_CAPTION_START:
;	FUNCID	#$a901643e
;	
;        move.l  #CAPTION_PAUSE,d7
;.caption_delay_round:
;        move.l  d7,-(a7)
;        bsr     WAIT_FOR_VERTICAL_BLANK
;        move.l  LSTPTR_FRONT_SCREEN(a4),a1
;        bsr     BLIT_CAPTION
;        move.l  (a7)+,d7
;        dbf     d7,.caption_delay_round
;        rts


BLIT_CAPTION:
	FUNCID	#$a11d38d7
	
        movem.l d0-d1/a0-a3,-(a7)
        move.l  IMG_CAPTION(a4),a2
        add.w   #$B0,a2
        move.l  MASK_CAPTION(a4),a3
	
	moveq	#0,d0
	move.w	CAPTION_TYPE(a4),d0
	mulu.w	#(110*16),d0
	add.l	d0,a2
	add.l	d0,a3
	
	;add.l	#(110*16),a2
	;add.l	#(110*16),a3
 
        add.l   #(PLAYFIELD_SIZE_X*5)*CAPTION_YPOS,a1
        add.w   MAP_POSX(a4),a1
        addq.w  #4,a1

        WAIT_FOR_BLITTER
        move.w  #$0fca,BLTCON0(a5)
        move.w  #$0,BLTCON1(a5)
        move.l  #-1,BLTAFWM(a5) ; Only want the first word

        move.w  #0,BLTAMOD(a5)
        move.w  #0,BLTBMOD(a5)
        move.w  #18,BLTCMOD(a5)
        move.w  #18,BLTDMOD(a5)

        move.l  a3,BLTAPTH(a5)          ; Load the mask address
        move.l  a2,BLTBPTH(a5)          ; Sprite address
        move.l  a1,BLTCPTH(a5)          ; Destination background
        move.l  a1,BLTDPTH(a5)
        move.w  #(75*64)+11,BLTSIZE(a5)
        movem.l (a7)+,d0-d1/a0-a3
        rts


BLIT_CAPTION_RESTORE:
	FUNCID	#$21582f8f
	
        move.l  LSTPTR_POST_SCREEN(a4),a1
        move.l  LSTPTR_FRONT_SCREEN(a4),a2

        move.l  #(PLAYFIELD_SIZE_X*5)*CAPTION_YPOS,d1
        add.w   MAP_POSX(a4),d1
        add.l   d1,a1
        add.l   d1,a2

        WAIT_FOR_BLITTER
        move.w  #$09f0,BLTCON0(a5)
        move.w  #$0,BLTCON1(a5)
        move.l  #-1,BLTAFWM(a5) ; Only want the first word

        move.w  #0,BLTAMOD(a5)
        move.w  #0,BLTDMOD(a5)

        move.l  a1,BLTAPTH(a5)          ; Load the mask address
        move.l  a2,BLTDPTH(a5)
        move.w  #(80*64)+20,BLTSIZE(a5)

        move.l  LSTPTR_POST_SCREEN(a4),a1
        move.l  LSTPTR_BACK_SCREEN(a4),a2

        move.l  #(PLAYFIELD_SIZE_X*5)*CAPTION_YPOS,d1
        add.w   MAP_POSX(a4),d1
        add.l   d1,a1
        add.l   d1,a2

        WAIT_FOR_BLITTER
        move.w  #$09f0,BLTCON0(a5)
        move.w  #$0,BLTCON1(a5)
        move.l  #-1,BLTAFWM(a5) ; Only want the first word

        move.w  #0,BLTAMOD(a5)
        move.w  #0,BLTDMOD(a5)

        move.l  a1,BLTAPTH(a5)          ; Load the mask address
        move.l  a2,BLTDPTH(a5)
        move.w  #(80*64)+20,BLTSIZE(a5)

        rts