ROUND30_EVENT_AUTOSCROLL:
	nop
	
; Cause backgrounds to fade
	move.w  #15,BACKGROUND_TRANSITION_FRAMES(a4)
	
	move.w	#-1,INPUT_DISABLED(a4)
	move.w  #-1,INIT_END_SEQUENCE(a4)
	
	move.w	#24,BLIT_BG_START(a4)
	move.w	#108,BLIT_BG_LENGTH(a4)
	
	clr.w	LIGAR_FIGHT(a4)

	rts
	