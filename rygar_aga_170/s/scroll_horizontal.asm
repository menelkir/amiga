ASSET_SIZE16:   equ     16
ASSET_MODULO:   equ     32                              ; 32
ASSET_TILES_X:  equ     16              ; 320           ; 16
ASSET_WIDTH:    equ     ASSET_MODULO*BITPLANES

CANVAS_TILES_X: equ     20    ; 256+32+32 = 320

MAP_WIDTH:      equ     176
MAP_DRAW_DEPTH: equ     12
DEFAULT_MAP_DEPTH_START:        equ     0

CANVAS_MODULO:  equ     CANVAS_TILES_X*2
CANVAS_WIDTH:   equ     CANVAS_MODULO*BITPLANES                 ;40*5 = 320

;CANVAS_SIZE:   equ     CANVAS_WIDTH*(MAP_DEPTH*ASSET_SIZE16)
CANVAS_XBUFF:   equ     1024

MAP_COMPENSATE: equ     256

FG_SCROLL_SPEED:        equ     2
SCROLL_LEFT:    equ     -1
SCROLL_RIGHT:   equ     1

BITPLANE1:      equ     0
BITPLANE2:      equ     1
BITPLANE3:      equ     2
BITPLANE4:      equ     3
BITPLANE5:      equ     4

BG_START_PIXEL: equ     256



; This is slightly complicated...
; There are 1024 possible tiles which take up bits 0-9
; If bit 15 is unset and bits 14 + 13 are set then tile command mode is selected.
;   if tile command mode is used then the bits from 12-10 are used to select the command (8 possible)
; If bit 15 is set then obstacle mode is set
;   If obstacle mode is used then bits from 14-10 are used to select the Obstacle to merge.
; If bit 16 is unset then item mode is set
;   This then merges the items/stones from bits 15-10

;----------------------------------------------------------------------------------
; | 15 | 14 | 13 | 12 | 11 | 10 |  9 |  8 |  7 |  6 |  5 |  4 |  3 |  2 |  1 |  0 |
;-------------------------------|                TILE SELECT (0-1023)             |
; |  0 |  1 |  1 | COMMAND INX. |             TILE COMMAND SELECT (0-7)           |
;----------------------------------------------------------------------------------
; |     TILE MERGE INDEX        |   TILE MERGE SELECT (0-63) (24-31 unavailable)  |         
;----------------------------------------------------------------------------------
; Upto 63 possible scroll in sprites can be mapped.
;SPRITE_TO_TILE_MERGE_MAP:
;	dc.w	1284,1285,1286,1287,1288,1289,1290,1291,1304,1305,1306,1307,1308,1309,1310,0000	; 0-15 Collectables (Items)
;	dc.w	1250,1251,0000,0000,0000,0000,0000,0000,XXXX,XXXX,XXXX,XXXX,XXXX,XXXX,XXXX,XXXX	; 16-23 Destroyables (Stones) 24-31 Unusable
;	dc.w	1216,1217,1218,1219,1236,1237,1238,1239,1240,1241,1260,1261,0000,0000,0000,0000	; 32-47 Obstacles (Big Brown Shit)	
;	dc.w	0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000	; 48-63 Obstacles	


TILECMD_PUSHSTONE_JUMP:         equ     %0110100000000000               ; 2
TILECMD_PUSHSTONE_WALK:         equ     %0110110000000000               ; 3
TILECMD_PUSHSTONE_ITEM:         equ     %0111000000000000               ; 4

        CNOP    0,4

TILECONTROL_JUMPTABLE:          dc.w    TILECONTROL_NOP-TILECONTROL_JUMPTABLE           	; 0
                                dc.w    TILECONTROL_NOP-TILECONTROL_JUMPTABLE         		; 1
                                dc.w    TILECONTROL_PUSHSTONE_JUMP-TILECONTROL_JUMPTABLE      ; 2
                                dc.w    TILECONTROL_PUSHSTONE_WALK-TILECONTROL_JUMPTABLE      ; 3
                                dc.w    TILECONTROL_PUSHSTONE_ITEM-TILECONTROL_JUMPTABLE      ; 4
                                dc.w    TILECONTROL_NOP-TILECONTROL_JUMPTABLE                 ; 5
                                dc.w    TILECONTROL_NOP-TILECONTROL_JUMPTABLE                 ; 6
                                dc.w    TILECONTROL_NOP-TILECONTROL_JUMPTABLE                 ; 7     ; BG_TOGGLE

TILECONTROL_ROUND_START:        FUNCID	#$292a5b44
                                moveq   #-1,d5
                                rts
TILECONTROL_ROUND_END:          FUNCID	#$ea0d6344
				rts
TILECONTROL_NOP:                FUNCID	#$aac5ef7b
				rts
TILECONTROL_PUSHSTONE_JUMP:     FUNCID	#$d0767f8f
				rts

                                CNOP    0,4

TILECONTROL_PUSHSTONE_WALK:     
				FUNCID	#$36ae2910
				movem.l d0-d7/a0-a3,-(a7)
                                moveq   #8,d1                           ; X screen position (20 for off right)

				
; This code here is bollox				; Get lowest platform
				moveq   #0,d2	
				move.w  MAP_PIXEL_POSX(a4),d2
				sub.w   #256,d2
				add.w   RYGAR_XPOS(a4),d2
				lsr.w   #4,d2
				lea     ROUND_PLATFORMS(a4),a0
				move.l  (a0,d2*4),d2
				and.l   #$f,d2
; BOLLOX			
                                subq.w  #2,d2                           ; Compensate
                                bra.s   TILE_CONTROL_PUSHSTONE

                                CNOP    0,4

TILECONTROL_PUSHSTONE_ITEM:     FUNCID	#$ee3036ea
				movem.l d0-d7/a0-a3,-(a7)               ; Cause a stone to screen onscreen.
                                moveq   #18,d1                          ; X screen position (20 for off right)
                                bra.s   TILE_CONTROL_PUSHSTONE
                                nop

                                CNOP    0,4

TILE_CONTROL_PUSHSTONE:
				FUNCID	#$50cf5d5a
                                move.l  a3,-(a7)
                                move.l  #STONE_ASSET,d0                 ; Put the stone asset on screen
                                move.l  #STONE_TIMEOUT,d3
                                bsr     PUSH_STONE
                                move.l  (a7)+,a3
                                and.w   #%0000001111111111,(a3)         ; Clear tile command once hit.
                                or.w    #STONE_BLOCK1,(a3)              ; Put stone block one in its place.
                                movem.l (a7)+,d0-d7/a0-a3
                                moveq   #STONE_BLOCK1/1024,d5           ; Tile number to display in place of this!
                                rts


LTM
LOAD_TILE_MERGE_BUFFERS:
; Run this on every new 16th pixel block when scrolling.

		moveq	#$2,d6						; Load left tiles on 0 position
		tst.w	SCROLL_DIRECTION_X(a4)
		bmi.s	.moving_left		
		moveq	#$c,d6						; Load right tiles on 12th position
.moving_left:
		move.w	MAP_PIXEL_POSX(a4),d5
		and.w	#$f,d5
		cmp.w	d6,d5
		bne.s	.exit
; if equal to 0...

		move.l	#MAP_WIDTH*2,d5
		
		moveq	#0,d6
		moveq	#12-1,d7

		moveq	#-18,d1						; left index
		lea     TILEMERGE_BUFFERS_LEFT(a4),a2
		tst.w	SCROLL_DIRECTION_X(a4)
		bmi.s	.is_left
		lea     TILEMERGE_BUFFERS_RIGHT(a4),a2	
		moveq	#0,d1		; right index

.is_left:

		move.l  ROUND_TILEMAP(a4),a0
		moveq   #0,d0
		lea     MAP_TABLE_Y1(a4),a1     ; Index to start of Map
		move.w  MAP_POSY(a4),d0
		add.l   (a1,d0*4),a0
		
		moveq   #0,d0
		move.w  MAP_PIXEL_POSX(a4),d0
		lsr.w	#4,d0
		add.l   d0,a0                   ; Index into map (far left)
		add.l	d0,a0
		
.loop:		
		move.w	2(a0,d1*2),d0					; Load map cell	
		move.w	d0,d2
		and.w	#%0110000000000000,d2
		cmp.w	#%0110000000000000,d2
		bne.s	.is_sprite
.is_tilecmd:	moveq	#0,d0						;Read tile cmd and push stones into map.

.is_sprite:	move.w	d0,d2
		lsr.w	#8,d2
		lsr.w	#2,d2						; d1=Tile index.
		move.w	d2,(a2,d6*2)					; Store sprite to merge.
		
		tst.w	d2
		beq.s	.xxx
		move.w	#$070,FIRECOLOUR
		;bsr	FIREWAITSR
.xxx:


.next:		add.l	d5,a0
		addq.w	#1,d6
		dbf	d7,.loop
.exit:		rts
		
		
		

; d0=Source tile asset number
; d1=Dest X in camera view (0,1,18,19) will be in borders, 2-17 are viewable
; d2=Dest Y in camera view (0,1
BLIT_TILE_XY:
	FUNCID	#$3422e6e7
.start: move.w  d0,d5					; save for tile_merge
        move.w  d0,d7					; save for tile command

; Check bits 12-10 to see if this is a tile merge.
        lsr.w   #8,d5
        lsr.w   #2,d5                                   ; d5 has sprite tile to merge
        tst.w   d5                                      ; Sprite merge or tile command required?
        beq.s   .no_merge                               ; No

; Check bits 15-13 to see if this is a tile command!
        lsr.w   #8,d7
        lsr.w   #5,d7
        and.w   #%011,d7                                ; if top 3 bits are set
        cmp.w   #$3,d7                                  ; Then this is a tile command
        bne.s   .merge                                  ; if not, then it is an object

.tilecmd:
        move.w  d0,d7
        lsr.w   #8,d7
        lsr.w   #2,d7
        and.w   #%111,d7

        move.l  a0,a3
        lea     TILECONTROL_JUMPTABLE(pc),a0
        add.w  (a0,d7*2),a0
	
        jsr     (a0)
        tst.w   d5                              ; If an item needs to be placed then do it here.
        bpl.s   .merge                          ; 0 = top left

        and.w   #%0000001111111111,d0           ; Otherwise clear all special bits and merge nothing.
        bra.s   .start

.merge: 
;	lea     TILEMERGE_BUFFERS_LEFT(a4),a0
;	tst.w	SCROLL_DIRECTION_X(a4)
;	bmi.s	.is_left
;        lea     TILEMERGE_BUFFERS_RIGHT(a4),a0	
;.is_left:
;        
;	move.w  d5,(a0,d2*2)                    ; Store the sprite number to overlay.

.no_merge:
        and.w   #%0000001111111111,d0   	; Only tiles below 1024
        lea     CANVAS_TABLE_Y1(a4),a0
        move.w  (a0,d2.w*2),d2
        add.w   d1,d2
        moveq   #0,d1
        move.w  d2,d1
        bra.s   BLIT_TILE
        nop

        CNOP    0,4

; d0=Tile Number
; d1=Screen Tile Dest
BLIT_TILE:
	FUNCID	#$033b7216
        movem.l d0-d5/a0-a3,-(a7)

        and.w   #%0000001111111111,d0

; Find Destination tile
        move.l  LSTPTR_FRONT_SCREEN(a4),a1              ; Screen location
        move.l  LSTPTR_BACK_SCREEN(a4),a2                       ; Screen location Back Buffer
        move.l  LSTPTR_POST_SCREEN(a4),a3                       ; Screen location Post Buffer

        moveq   #0,d2
        move.w  MAP_POSX(a4),d2
        add.l   d2,a1                           ; Add map position offset
        add.l   d2,a2                           ; Add map position offset
        add.l   d2,a3                           ; Add map position offset

; Fast divide by 20 with remainder
        lea     DIV20_TABLE(a4),a0                              ; CANVAS_TILES_X = 20
        move.w  (a0,d1*2),d2
	moveq	#0,d1
        move.b  d2,d1                                           ; X Value in d1
        lsr.w   #8,d2                                           ; Y Value in d2

        subq.l  #2,d1

        add.l   d1,d1                                           ; Bytes to words
        add.l   d1,a1                                           ; Add to screen offset
        add.l   d1,a2
        add.l   d1,a3

        lea     CANVAS_TABLE_Y2(a4),a0                  ; Fast index    ; for Y position
        add.l   (a0,d2.w*4),a1                                  ; Add to screen offset
        add.l   (a0,d2.w*4),a2                                  ; Add to screen offset
        add.l   (a0,d2.w*4),a3                                  ; Add to screen offset

        moveq   #0,d2

; Find Source tile (source tile in d0)
; Fast divide by 16 with remainder
        move.w  d0,d1
        move.w  d0,d2
        and.l   #$f,d1                          ; d1 = x position
        lsr.w   #4,d2                           ; d2 = Y position
        and.w   #$ff,d2

        lea     ASSET_TABLE_Y1(a4),a0                   ; Fast Multiply to Y
        move.l  (a0,d2.w*4),d2                                  ; Bring in the offset

        add.l   d1,d1                                           ; Bytes to words for offset
        move.l  TILE_ASSETS_PTR(a4),a0                  ; Tiles location
        add.l   d1,a0                                           ; Add X Offset
        add.l   d2,a0                                           ; Add Y offset

; a0=source
; a1=destination Front Scroll
; a2=destination Back Scroll
; a3=destination Post Scroll
; d2=source modulo
; d3=destimation modulo
; d0=y_pixels size

        move.w  #(80*64)+1,d0
        WAIT_FOR_BLITTER
        move.l  #$09f00000,BLTCON0(a5)  ; Select straigt A-D mode $f0
        move.l  #-1,BLTAFWM(a5)         ; No masking needed
        move.w  #ASSET_MODULO-2,BLTAMOD(a5)             ; Playfield Modulo Src
        move.w  #CANVAS_MODULO-2,BLTDMOD(a5)            ; Playfield Modulo Dest
        move.l  a0,BLTAPTH(a5)          ; set source address
        move.l  a3,BLTDPTH(a5)          ; set dest address to back screen
        move.w  d0,BLTSIZE(a5)          ; boom.....

        WAIT_FOR_BLITTER
        move.l  #$09f00000,BLTCON0(a5)  ; Select straigt A-D mode $f0
        move.l  #-1,BLTAFWM(a5)         ; No masking needed
        move.w  #ASSET_MODULO-2,BLTAMOD(a5)             ; Playfield Modulo Src
        move.w  #CANVAS_MODULO-2,BLTDMOD(a5)            ; Playfield Modulo Dest
        move.l  a0,BLTAPTH(a5)          ; set source address
        move.l  a2,BLTDPTH(a5)          ; set dest address to front screen
        move.w  d0,BLTSIZE(a5)          ; boom.....

        WAIT_FOR_BLITTER
        move.l  #$09f00000,BLTCON0(a5)  ; Select straigt A-D mode $f0
        move.l  #-1,BLTAFWM(a5)         ; No masking needed
        move.w  #ASSET_MODULO-2,BLTAMOD(a5)             ; Playfield Modulo Src
        move.w  #CANVAS_MODULO-2,BLTDMOD(a5)            ; Playfield Modulo Dest
        move.l  a0,BLTAPTH(a5)          ; set source address
        move.l  a1,BLTDPTH(a5)          ; set dest address to post screen
        move.w  d0,BLTSIZE(a5)          ; boom.....
.exit:  movem.l (a7)+,d0-d5/a0-a3
        rts

        CNOP    0,4

; This function blits a tile to the left or right border area
; depending on the value of hpos regsiter.
; d3 controls the screen x offset
; d4 controls the map x offset
BLIT_TILE_ONSCROLL_X:
	FUNCID	#$5392a642
        tst.w   VERTSCROLL_ENABLE(a4)
        bmi     .exit
        moveq   #0,d3                                   ; default to left border
        moveq   #-34,d4                                 ; default to right border
        moveq   #0,d2
        move.w  SCROLL_HPOS(a4),d2                      ; Tile Yposition
        subq.w  #1,d2                                   ; Compensate for left scroll
        tst.w   SCROLL_DIRECTION_X(a4)
        beq     .exit
        bmi     .blit_left_border
.blit_right_border:
; moving right
        addq.w  #1,d2                                   ; Recompensate if right.
        moveq   #CANVAS_TILES_X-2,d3                    ; into right border
        add.l   #(CANVAS_TILES_X*2)-4,d4                ; into map index
	
        not.w   d2                                      ; Reverse tiles top to bottom

;d3=Tile X Offset, 0=Left, 23=Right
;d4=Screen map offset, 0=Left, Right=(CANVAS_TILES_X*2)-2
; moving left
.blit_left_border:
        
; Left scroll problem...
; When scrolling Right we need to start blitting from inner right
; When scrolling Left we need to start blitting inner left instead of inner right.
	and.l   #$1f,d2                         ; 0-31 possible positions where the blit will take place.
        
; Pure manipulation of this d2 register here if moving left
	tst.w	SCROLL_DIRECTION_X
	bpl.s	.moving_right
	bchg	#4,d2				; swap order of left placement to inner first
	
.moving_right:
	move.w  d2,d1				
        and.w   #$f,d1				; 16 possible places to put the tile on Y Axis.

; a0 now points to top left of the tile area
        lsr.w   #4,d2                           ; shift bit 5 (16) to 1
        add.w   d2,d3                           ; add offset to screen border x 
	

	
        add.w   d2,d2                           ; double it.
        add.l   d2,d4                           ; add offset to map 

        move.w  d1,d2
; d3 will be the x offset position




       cmp.w   #12,d2				
       bge     SPRITE_MERGE_WITH_TILE		;; This will be called on 12 and 14
						; The upper blocks have already been blit
						; This then merges the sprite with the block.
						; which is fucking stupid.
	move.w  d1,-(a7)
        move.w  d2,-(a7)

; Call to BLIT_TILE_XY
;d0 needs to be source tile number
;d1 needs to be x position
;d2 needs to be y position

        move.l  ROUND_TILEMAP(a4),a0
        moveq   #0,d0
        lea     MAP_TABLE_Y1(a4),a1     ; Index to start of Map
        move.w  MAP_POSY(a4),d0
        add.l   (a1,d0*4),a0
        moveq   #0,d0
        move.w  MAP_POSX(a4),d0
        add.l   d0,a0                   ; Index into map (far left)

        lea     MAP_TABLE_Y1(a4),a1
        move.l  (a1,d1.w*4),d1
        add.l   d1,a0                   ; Index into Y position in map
        add.l   d4,a0
        move.l  d3,d1                   ; Set tile X location to blit
        move.w  (a0),d0                 ; Tile num to blit.
        move.w  d0,d5                   ; Saved
        bsr     BLIT_TILE_XY


        move.w  (a7)+,d2
        move.w  (a7)+,d1

        addq.w  #1,d2                   ; Next tile down

        move.l  ROUND_TILEMAP(a4),a0
        moveq   #0,d0
        lea     MAP_TABLE_Y1(a4),a1                     ; Index to start of Map
        move.w  MAP_POSY(a4),d0
        add.l   (a1,d0*4),a0

        moveq   #0,d0
        move.w  MAP_POSX(a4),d0
        add.w   #MAP_WIDTH*2,d0
        add.l   d0,a0           ; Index into map (far left)

        lea     MAP_TABLE_Y1(a4),a1
        move.l  (a1,d1.w*4),d1

        add.l   d1,a0                   ; Index into Y position in map
        add.l   d4,a0
        move.l  d3,d1                   ; Set tile X location to blit
        move.w  (a0),d0                 ; Tile to blit.
        bsr     BLIT_TILE_XY

.exit:  
	rts





CHECK_DIRECTION_CHANGE:
	FUNCID	#$ab22f9af
	movem.l	d0-d3/a0,-(a7)
	
	tst.w	SCROLL_DIRECTION_X(a4)
	beq.s	.exit
	move.w	SCROLL_DIRECTION_LAST_X(a4),d0
	move.w	SCROLL_DIRECTION_X(a4),SCROLL_DIRECTION_LAST_X(a4)
	
	cmp.w	SCROLL_DIRECTION_X(a4),d0
	beq.s	.exit

	nop
; Code when direction changes.

.exit:	movem.l	(a7)+,d0-d3/a0
	rts
	
        CNOP    0,4

; This routine will be called twice maximum.
; They handle the sprites coming on the borders that are mixed with the
; environment.  Such as obstacles or enblems.

; In = d3 = Screen X offset position.
;
SPRITE_MERGE_WITH_TILE:

	FUNCID	#$15c86163
        movem.l d0-d7/a0-a3,-(a7)
        move.l  d3,d1				; d3=X Offset
        lea     TILEMERGE_BUFFERS_LEFT(a4),a0
	tst.w	SCROLL_DIRECTION_X(a4)
	bmi.s	.is_left
        lea     TILEMERGE_BUFFERS_RIGHT(a4),a0	
.is_left:
        moveq   #15,d3

        moveq   #-1,d2
.loop:  addq.l  #1,d2
        move.w  (a0)+,d5                        ; This is the sprite number (0-15 Obstacles,16-31 Emblems)
        bne.s   .sprite
        dbf     d3,.loop
        bra     .exit
.sprite:
        clr.w   -2(a0)                          ; remove it.

	IFEQ	ENABLE_SHOW_HIDDEN_STONES
	cmp.w	#9,d2				; Avoid placing any tile attributes into vertical position 10
	beq	.exit
	ENDC

;d5=Sprite number to dereference
        cmp.w   #64,d5
        bge     .exit
	
	btst	#4,d5				; Is this a stone?
	bne.s	.is_stone			; Yes

.is_obstacle_or_item:
; Adjust blit placement.
        move.w  #CANVAS_WIDTH*3,d4              ; Cause placement offset for obstacles.
        cmp.w   #5,d2
        bne.s   .lower_obstacle
        move.w  #CANVAS_WIDTH*6,d4
.lower_obstacle:
        lea     CANVAS_TABLE_Y1(a4),a0
        add.w   (a0,d2.w*2),d1
	bra.s	.handle_merge

.is_stone:
	moveq	#0,d4
        lea     CANVAS_TABLE_Y1(a4),a0
        add.w   (a0,d2.w*2),d1


; Find Destination tile
.handle_merge:
        move.l  LSTPTR_FRONT_SCREEN(a4),a1                      ; Screen location
        move.l  LSTPTR_BACK_SCREEN(a4),a2                       ; Screen location Back Buffer
        move.l  LSTPTR_POST_SCREEN(a4),a3                       ; Screen location Post Buffer

        moveq   #0,d2
        move.w  MAP_POSX(a4),d2
        add.l   d2,a1                                           ; Add map position offset
        add.l   d2,a2                                           ; Add map position offset
        add.l   d2,a3                                           ; Add map position offset

; Fast divide by 20 with remainder
        lea     DIV20_TABLE(a4),a0                              ; CANVAS_TILES_X = 20
        move.w  (a0,d1*2),d2
        moveq	#0,d1
	move.b  d2,d1                                           ; X Value in d1
        lsr.w   #8,d2                                           ; Y Value in d2

        subq.l  #2,d1
	
        add.l   d1,d1                                           ; Bytes to words
        add.l   d1,a1                                           ; Add to screen offset
        add.l   d1,a2
        add.l   d1,a3

        lea     CANVAS_TABLE_Y2(a4),a0                          ; Fast index    ; for Y position
        add.l   (a0,d2.w*4),a1                                  ; Add to screen offset
        add.l   (a0,d2.w*4),a2                                  ; Add to screen offset
        add.l   (a0,d2.w*4),a3                                  ; Add to screen offset

; a0 has tile source addres of the tile.
; d0 has blit size.

        moveq   #-1,d7
        move.l  #$0fca0000,d3
        moveq   #PLAYFIELD_SIZE_X-2,d2
        moveq   #SPRITE_ASSETS_SIZE_X-2,d6
        move.w  #(160*64)+1,d0          ; Default to 16x32
	moveq	#0,d1
        move.w  d4,d1

; if bit number 0,1,2 or 3 is set then it is an item = 16x16
; if bit number 4 is set then it is a Stone = 16x32
; if bit number 5 is set then it is an obstacle = 16x32

	btst	#4,d5			; Is this a stone?
	bne.s	.16x32			; Yes
        btst    #5,d5                   ; Is this an obstacle?
        bne.s   .16x32			; Yes.

; Items need to be positioned between two words... difficult because we have to blit the item

; This for a 16x16 item.
        move.w  #(80*64)+1,d0           
        move.w  #CANVAS_WIDTH*0,d1

.16x32:
        add.l   d1,a3
        add.l   d1,a2
        add.l   d1,a1

        move.l  a3,-(a7)                ; save post screen address
        move.l  a2,-(a7)                ; save front screen address
        move.l  a1,-(a7)                ; save back screen address

        subq.w  #1,d5
        lea     SPRITE_TO_TILE_MERGE_MAP(a4),a0
        move.w  (a0,d5*2),d5                    ; Get the sprite number

        move.l  MEMCHK0_SPRITE_POINTERS(a4),a0
        move.l  (a0,d5*8),a2            ; Get sprite address
        move.l  4(a0,d5*8),a3           ; Get mask address

        rept 3

        WAIT_FOR_BLITTER
        move.l  d3,BLTCON0(a5)  ; Select straigt A-D mode $f0
        move.l  d7,BLTAFWM(a5)          ; No masking needed
        move.w  d6,BLTAMOD(a5)  ; Mask
        move.w  d6,BLTBMOD(a5)  ; Sprite
        move.w  d2,BLTCMOD(a5)                          ; Background
        move.w  d2,BLTDMOD(a5)                          ; Destination
        move.l  a3,BLTAPTH(a5)                          ; Mask
        move.l  a2,BLTBPTH(a5)                          ; Sprite
        move.l  (a7)+,d1
        move.l  d1,BLTCPTH(a5)                          ; Background
        move.l  d1,BLTDPTH(a5)                          ; Dest
        move.w  d0,BLTSIZE(a5)

        endr

.exit:  movem.l (a7)+,d0-d7/a0-a3
        rts



;---------------------------------------------------------------

CHECK_TILE_COMMANDS:
	FUNCID	#$8e7146e0
        move.l  d0,-(a7)
        move.l  d1,-(a7)
        move.l  a0,-(a7)
        move.w  d0,d1

        lsr.w   #8,d1
        lsr.w   #5,d1
        and.w   #%111,d1                                ; if top 3 bits are set
        cmp.w   #$7,d1                                  ; Then this is a tile command
        bne.s   .exit                                   ; if not, then it is an object

        move.w  d0,d1
        lsr.w   #8,d1
        lsr.w   #2,d1
        and.w   #%111,d1
        subq.w  #1,d1

        lea     TILECONTROL_JUMPTABLE(pc),a0
        move.l  (a0,d1*4),a0
        jsr     (a0)

.exit:  move.l  (a7)+,a0
        move.l  (a7)+,d1
        move.l  (a7)+,d0
        rts




;a0= IFF bitmap
;a1= destination
;d7= size (in bytes
COPY_BITMAP:
	FUNCID	#$ddf09dfe
.loop:  cmp.l   #"BODY",(a0)
        beq.s   .ok
        addq.w  #2,a0
        bra.s   .loop

.ok:    addq.w  #4,a0
        move.l  (a0)+,d7
.copy:  move.b  (a0)+,(a1)+
        dbf     d7,.copy
        rts

        CNOP    0,4

; d0=Map offset to draw.
; a0=Start pointer in map (x/y already derived.
DRAW_CANVAS:
	FUNCID	#$7b7696a0
        moveq   #0,d4
        moveq   #0,d5
        move.w  MAP_POSX(a4),d4                 ; Get current map X position
        add.w   d0,d4                                   ; Add X offset to map position
        add.w   d4,d4
        add.w   d4,a0

        moveq   #0,d1

        moveq   #(MAP_DRAW_DEPTH)-1,d7

	move.l	a0,$224.w
	move.l	ROUND_TILEMAP(a4),d2
	sub.l	d2,$224.w
	
.column:
	move.l	a0,-(a7)			; Save row position.
        moveq   #(CANVAS_TILES_X)-1,d6  ;       19 tiles X

.row:   moveq	#0,d0
	move.w	(a0)+,d0
	cmp.w	#$345,d0
	beq.s	.skip
	;move.w	#312,d0
	bsr     BLIT_TILE
.skip:	addq.w  #1,d1                   ; next destination tile
        dbf     d6,.row
	
	move.l	(a7)+,a0
	
        add.l   #MAP_WIDTH*2,a0		; Next row.
        dbf     d7,.column
        rts

        CNOP    0,4

; Fix the tiles to the right at the end of each temple scene.
; d0=Map offset to draw.
FIX_TEMPLE:
	FUNCID	#$8438eff6
        move.l  ROUND_TILEMAP(a4),a0
        moveq   #0,d2
        lea     MAP_TABLE_Y1(a4),a1                     ; Index to start of Map
        move.w  MAP_POSY(a4),d2
        add.l   (a1,d2*4),a0

        moveq   #0,d2

        moveq   #(MAP_DRAW_DEPTH)-1,d3

.loop:  moveq   #18,d1                          ; X Block 18 in border
        move.w  (a0),d0                         ; Feed left tile
        movem.l d0-d3/a0,-(a7)
        bsr     BLIT_TILE_XY
        movem.l (a7)+,d0-d3/a0

        moveq   #19,d1                          ; X Block 19 in border
        move.w  2(a0),d0                        ; Feed right tile.
        movem.l d0-d3/a0,-(a7)
        bsr     BLIT_TILE_XY
        movem.l (a7)+,d0-d3/a0

        add.w   #MAP_WIDTH*2,a0

        addq.w  #1,d2
        dbf     d3,.loop
        rts


DRAW_SUNSET:
	FUNCID	#$fc0438e0
        add.w   #(PLAYFIELD_SIZE_X*3)*50,a0
        add.w   #16,a0
        ;lea    RYGAR_SUNSET(a4),a1
        move.l  RYGAR_SUNSET(a4),a1
.loop:  cmp.l   #"BODY",(a1)
        beq.s   .found
        addq.w  #2,a1
        bra.s   .loop

.found: addq.w  #8,a1
        move.l  #PLAYFIELD_SIZE_X,d1
        move.l  #96/8,d2

        moveq   #84-1,d7

.line:  move.l  (a1),(a0)                       ;1-32
        move.l  4(a1),4(a0)                     ;33-64
        move.l  8(a1),8(a0)                     ;65-96

        add.l   d1,a0                           ; next bitplane
        add.l   d2,a1

        move.l  (a1),(a0)                       ;1-32
        move.l  4(a1),4(a0)                     ;33-64
        move.l  8(a1),8(a0)                     ;65-96

        add.l   d1,a0                           ; next bitplane
        add.l   d2,a1

        add.l   d1,a0                           ; advance three bitplanes at destination
        add.l   d1,a0
        add.l   d1,a0

        add.l   d2,a1                           ; advance to line
        dbf     d7,.line
        rts

        CNOP    0,4

DRAW_TILES:
	FUNCID	#$4bc63f05
        moveq   #4,d6           ; number of ROWS
        moveq   #0,d0           ; Source Tile Number
        moveq   #0,d1           ; Destination Tile
        moveq   #0,d2
.loop_y:
        move.l  #19,d7          ; NUMBER OF XROWS-1
.tiles: bsr     BLIT_TILE
        addq.w  #1,d0           ;
        addq.w  #1,d1           ; Next column (horizontal) in destination
        dbf     d7,.tiles
        add.w   #20,d1          ; Move to next row (vertical) in destination
        dbf     d6,.loop_y
        rts



        CNOP    0,4

;a0 = pointer to start of copper bitplane registers
;d2 = screen address
UPDATE_COPPER_BITPLANES:
	FUNCID	#$b4128197
        moveq   #0,d0
        moveq   #0,d1
        move.w  MAP_POSX(a4),d0
        add.l   d0,d2                                   ; 64 bit words in.

        rept    BITPLANES
        move.w  d2,2(a0)
        swap    d2
        move.w  d2,6(a0)
        swap    d2
        add.l   #CANVAS_MODULO*1,d2
        addq.w  #8,a0
        endr
        rts

PAN_RIGHT:
	FUNCID	#$8b1a1118
	tst.w	RYGAR_LOCK_LEFT_RIGHT(a4)
	bmi	.exit
        tst.w   VERTSCROLL_ENABLE(a4)
        bmi     SWING_INPUT
        tst.w   RYGAR_JUMP_STATE(a4)
        bmi.s   .jumping
        tst.w   RYGAR_DISKARM_STATE(a4)                         ; Scrolling pauses when weapon is discharges when running
        bmi.s   .exit
.jumping:
        tst.w   RYGAR_LEAPING_STATE(a4)
        bpl.s   .exit
        tst.w   VERTSCROLL_ENABLE(a4)
        bmi.s   .exit
        cmp.w   #1,RYGAR_BLOCKED_STATE(a4)
        beq.s   .exit
        move.w  #SCROLL_RIGHT,SCROLL_DIRECTION_X(a4)    ; Set move direction to right.
        
	tst.w	LIGAR_FIGHT(a4)
	bmi.s	.exit
	
	tst.w   SANCTUARY_STAGE(a4)
        bne.s   .sanctuary
        cmp.w   #SCROLL_LOCK_POSX,RYGAR_XPOS(a4)            ; If Rygar is not at mid position
        ble.s   .exit
.sanctuary:         
	addq.w	#1,MOON_BG_OFFSET(a4)                            ; Then we don't scroll right yet
        move.w  MAP_PIXEL_POSX(a4),d0
        move.b  #-1,SCROLL_EDGE_LOCK_X(a4)                      ; Scroll lock off
        moveq   #SCROLL_RIGHT,d0                                ; vertical scrolling.
        moveq   #FG_SCROLL_SPEED,d1
        bsr     PAN_CAMERA_AXIS_X
.exit:  rts

        CNOP    0,4


PAN_CAMERA_POSITION:
	FUNCID	#$dd3380b8
        clr.w   RYGAR_CROUCH_STATE(a4)
        clr.w   RYGAR_ATTACK_UP(a4)
        clr.w   SCROLL_DIRECTION_X(a4)
        clr.b   SCROLL_DIRECTION_Y(a4)
        clr.b   SCROLL_EDGE_LOCK_X(a4)                  ; Default to scroll lock on.

        tst.w   DISABLE_ENEMIES(a4)
        bmi     .exit
        tst.w   AUTO_PAN_RIGHT(a4)
        bne.s   .exit
        tst.w   RYGAR_DEAD_STATE(a4)
        bmi     .exit

        ;bsr     BTN_DETECT

        tst.w   AUTO_SCROLL_RIGHT(a4)
        bne.s   .auto_enabled
	
	bsr     JOY_DETECT
	move.w	d3,JOY_PORT1
	tst.w	d3
	bne.s	.not_held
	clr.w	JOY1_HELP_UP

.not_held:
	
        cmp.w   #JOY1_LEFT,d3
        beq     PAN_RIGHT
        cmp.w   #JOY1_RIGHT,d3
        beq     PAN_LEFT
	
        cmp.w   #JOY1_UP_LEFT,d3	
        beq     JUMP_OR_LEFT
        cmp.w   #JOY1_UP_RIGHT,d3
        beq     JUMP_OR_RIGHT
	
.auto_enabled:
        tst.w   VERTSCROLL_ENABLE(a4)
        bmi.s   SWING_INPUT
        cmp.w   #JOY1_DOWN,d3                           ; Which direction are we moving?
        beq	SET_CROUCH
        cmp.w   #JOY1_DOWN_LEFT,d3
        beq	SET_CROUCH_LEFT
        cmp.w   #JOY1_DOWN_RIGHT,d3
        beq	SET_CROUCH_RIGHT
        cmp.w   #JOY1_UP,d3
        beq	JUMP_OR_ATTACK
        cmp.w   #JOY1_UP_LEFT,d3
        beq	JUMP_OR_ATTACK
        cmp.w   #JOY1_UP_RIGHT,d3
        beq	JUMP_OR_ATTACK	
	
	
.exit:  rts

SWING_INPUT:
	FUNCID	#$54ee721c
        ;clr.w   RYGAR_SWING_STATE(a4)
        cmp.w   #JOY1_UP,d3
        beq     PAN_UP
        cmp.w   #JOY1_DOWN,d3
        beq     PAN_DOWN
        cmp.w   #JOY1_LEFT,d3
        beq     SWING_RIGHT
        cmp.w   #JOY1_RIGHT,d3
        beq     SWING_LEFT
        rts

SWING_LEFT:
	FUNCID	#$88cc3396
        move.w  #-1,RYGAR_SWING_STATE(a4)
        rts

SWING_RIGHT:
	FUNCID	#$cc314c45
	tst.w	ROUND_HEIGHT_STATE(a4)
	bmi	DISABLE_VERTICAL_SCROLL
        move.w  #1,RYGAR_SWING_STATE(a4)
.exit:	rts
	
	
DISABLE_VERTICAL_SCROLL_LOCK:
	FUNCID	#$5a16dc57
	clr.w	ROPE_LINE_ENABLE(a4)		; << This is bugged.. with SET_LAST_MAP_POSITION.		
	clr.w	VERTSCROLL_ENABLE(a4)
	clr.b	SCROLL_EDGE_LOCK_X(a4)
	
	tst.w	DISMOUNT_ROPE_ADJUST(a4)
	bmi.s	.pan_left
	bsr	PAN_RIGHT
; Need to store map position so that it can't be scrolled left and beyond.
	move.w	MAP_PIXEL_POSX(a4),MAP_PIXEL_POSX_LIMIT(a4)
	bra.s	.pan
	
.pan_left:	bsr	PAN_LEFT
.pan:		bsr	RESET_EVENTS			; Cause all check events to be reset after Rygar's
						; Current position.
	move.w	#-1,RYGAR_LOCK_LEFT_RIGHT(a4)
	bra.s	DISABLE_VERTICAL_SCROLL
	
	nop
	
DISABLE_VERTICAL_SCROLL:
	FUNCID	#$ce0e6cfe
	
	;SET_LAST_MAP_POSITION
	
	move.l  ROUND_TILEMAP(a4),a0
	bsr	GET_MAP_PLATFORMS		; Get platforms at current position.
	clr.w	ROPE_LINE_ENABLE(a4)		; << This is bugged.. with SET_LAST_MAP_POSITION.		
	clr.w	VERTSCROLL_ENABLE(a4)		; disable vertical scrolling
	clr.b	SCROLL_EDGE_LOCK_X(a4)		; Re-enable horizontal scrolling
	move.w	#-1,ROUND_RESTORE_POST(a4)
	move.l	#-1,ROUND_FG_LAVA_CYCLE(a4)
	move.w	#-1,BACKGROUND_ENABLE(a4)				; Enable the background
	move.w	#-1,RYGAR_MINI_JUMP(a4)		; Mini jump 
	bsr	RYGAR_INITIATE_JUMP
	rts
	
        CNOP    0,4

; If one button configured then cause a jump
; if two buttons configured then set attack up.

JUMP_OR_ATTACK:
	FUNCID	#$5987b065
; Is Rygar already Jumping?  If so then set attack up.
; If Rygar is not jumping then initiate a jump.

	tst.w	CURRENT_BUTTON_MODE
	beq	SET_ATTACK_UP
; one button mode.

	tst.w	RYGAR_JUMP_STATE(a4)		; Is Rygar already jumping?
	bmi	SET_ATTACK_UP
	tst.w	RYGAR_FALL_STATE(a4)		
	bmi	SET_ATTACK_UP

	tst.w	JOY1_HELP_UP
	bmi.s	.exit
	move.w	#-1,JOY1_HELP_UP
	bra	RYGAR_INITIATE_JUMP
.exit:	rts

JUMP_OR_LEFT
	FUNCID	#$08adb351
; Is Rygar already Jumping?  If so then set attack up.
; If Rygar is not jumping then initiate a jump.

	tst.w	CURRENT_BUTTON_MODE
	beq	PAN_LEFT
; one button mode.
	tst.w	RYGAR_JUMP_STATE(a4)		; Is Rygar already jumping?
	bmi	PAN_LEFT
	tst.w	RYGAR_FALL_STATE(a4)		
	bmi	PAN_LEFT
	tst.w	JOY1_HELP_UP
	bmi.s	.exit
	move.w	#-1,JOY1_HELP_UP
	bra	RYGAR_INITIATE_JUMP
.exit:	rts

JUMP_OR_RIGHT
	FUNCID	#$82f50220
; Is Rygar already Jumping?  If so then set attack up.
; If Rygar is not jumping then initiate a jump.

	tst.w	CURRENT_BUTTON_MODE
	beq	PAN_RIGHT
; one button mode.
	tst.w	RYGAR_JUMP_STATE(a4)		; Is Rygar already jumping?
	bmi	PAN_RIGHT
	tst.w	RYGAR_FALL_STATE(a4)		
	bmi	PAN_RIGHT
	tst.w	JOY1_HELP_UP
	bmi.s	.exit
	move.w	#-1,JOY1_HELP_UP
	bra	RYGAR_INITIATE_JUMP
.exit:	rts

SET_ATTACK_UP:
	FUNCID	#$a573b0e5
        move.w  #-1,RYGAR_ATTACK_UP(a4)
        rts

        CNOP    0,4

SET_CROUCH:
	FUNCID	#$98cd1722
	clr.w	JOY1_HELP_UP
        move.w  #-1,RYGAR_CROUCH_STATE(a4)
        rts

SET_CROUCH_LEFT:
	FUNCID	#$0169e8a5
	clr.w	JOY1_HELP_UP
        move.w  #-1,RYGAR_CROUCH_STATE(a4)
        rts

SET_CROUCH_RIGHT:
	FUNCID	#$8a5fa01d
	clr.w	JOY1_HELP_UP
        move.w  #1,RYGAR_CROUCH_STATE(a4)
        rts

        CNOP    0,4

PAN_LEFT:
	FUNCID	#$6486288b
	tst.w	RYGAR_LOCK_LEFT_RIGHT(a4)
	bmi	.exit
        tst.w   VERTSCROLL_ENABLE(a4)
        bmi   	SWING_INPUT
        tst.w   RYGAR_JUMP_STATE(a4)                            ; Scrolling continues when weapon discharged
        bmi.s   .jumping
        tst.w   RYGAR_DISKARM_STATE(a4)                         ; Scrolling pauses when weapon is discharges when running
        bmi.s   .exit

.jumping:
        tst.w   RYGAR_LEAPING_STATE(a4)
        bpl.s   .exit
        tst.w   VERTSCROLL_ENABLE(a4)
        bmi.s   .exit
        tst.w   RYGAR_BLOCKED_STATE(a4)
        bmi.s   .exit
	tst.w	INIT_END_SEQUENCE(a4)
	bmi.s	.exit
        move.w  #SCROLL_LEFT,SCROLL_DIRECTION_X(a4)
	tst.w	LIGAR_FIGHT(a4)
	bmi.s	.exit
	tst.w	ROPE_LINE_ENABLE(a4)			; Rope line detached.
	bmi.s	.exit
	
	move.w	MAP_PIXEL_POSX_LIMIT(a4),d0
	beq.s	.not_vertical
	cmp.w	MAP_PIXEL_POSX(a4),d0
	bge.s	.exit
.not_vertical:	
        cmp.w   #246,MAP_PIXEL_POSX(a4)                 ; This is so that Rygar can't move back left
        ble.s   .exit                                           ; 256 is actually the start of the round
	subq.w	#1,MOON_BG_OFFSET(a4)
        move.b  #-1,SCROLL_EDGE_LOCK_X(a4)                      ; Scroll lock off
        moveq   #SCROLL_LEFT,d0
        moveq   #FG_SCROLL_SPEED,d1
        bsr     PAN_CAMERA_AXIS_X                               ; Scroll hw
.exit:  rts


        CNOP    0,4

;d0: $ff=left, $00=null, $01=right
;d1: X Axis value
PAN_CAMERA_AXIS_X:
	FUNCID	#$7b60aefd
        tst.w   d0                                              ; Check input from Joystick
        beq.s   .done                                           ; True if did not move left or right
        bmi.s   PAN_CAMERA_LEFT                                 ; -1 so move left
        bpl     PAN_CAMERA_RIGHT                                ; +1 so move right
.done:  rts

        CNOP    0,4

PAN_CAMERA_LEFT:
	FUNCID	#$549c9891
        move.w  MAP_PIXEL_POSX(a4),d2                   ; Get scroll position
        sub.w   d1,d2                                           ; subtract scroll speed
        move.w  d2,MAP_PIXEL_POSX(a4)                   ; update the foreground position
        ;sub.w   BG_START_OFFSET(a4),d2                  ; Compensate start of background offset
        move.w  BG_SCROLL_SPEED(a4),d1
        lsr.w   d1,d2                                           ; divide scroll position by 2
        move.w  d2,MAP_OFFSET_BG(a4)                        ; update the background position
	
	cmp.w	#BG_SCROLL_CLOUDS,BG_SCROLL_TYPE(a4)
	bne.s	.no_cloud
	
; If on the clouds round then update positions.	
	moveq	#16,d6
	lea	CLOUDS_CONFIG(a4),a3
	sub.l	#$18000,(a3)				; speed 1		1.5px 
	add.l	d6,a3
	sub.l	#$16000,(a3)				; speed 2
	add.l	d6,a3
	sub.l	#$14000,(a3)				; speed 3
	add.l	d6,a3
	sub.l	#$12000,(a3)				; speed 4
	add.l	d6,a3
	sub.l	#$10000,(a3)				; speed 5
	add.l	d6,a3
	sub.l	#$8000,(a3)				; speed 6
	add.l	d6,a3
	sub.l	#$6000,(a3)				; speed 7
	add.l	d6,a3
	sub.l	#$12000,(a3)				; speed 8	- Mountains.
	add.l	d6,a3

.no_cloud:
        tst.w   d0
        bne.s   .done
        move.w  #(CANVAS_TILES_X/2)*16,MAP_PIXEL_POSX(a4)
.done:  bra     MAP_POS_UPDATE

        CNOP    0,4

PAN_CAMERA_RIGHT:
	FUNCID	#$5e7660a5
        move.w  MAP_PIXEL_POSX(a4),d2
        add.w   d1,d2
        move.w  d2,MAP_PIXEL_POSX(a4)
        ;sub.w   BG_START_OFFSET(a4),d2
        move.w  BG_SCROLL_SPEED(a4),d1
        lsr.w   d1,d2
        move.w  d2,MAP_OFFSET_BG(a4)
        move.w  MAP_PIXEL_POSX(a4),d0
	cmp.w	#BG_SCROLL_CLOUDS,BG_SCROLL_TYPE(a4)
	bne	MAP_POS_UPDATE
; If on the clouds round then update positions.
	moveq	#16,d6
	lea	CLOUDS_CONFIG(a4),a3
	add.l	#$18000,(a3)				; speed 1
	add.l	d6,a3
	add.l	#$16000,(a3)				; speed 2
	add.l	d6,a3
	add.l	#$14000,(a3)				; speed 3
	add.l	d6,a3
	add.l	#$12000,(a3)				; speed 4
	add.l	d6,a3
	add.l	#$10000,(a3)				; speed 5
	add.l	d6,a3
	add.l	#$8000,(a3)				; speed 6
	add.l	d6,a3
	add.l	#$6000,(a3)				; speed 7
	add.l	d6,a3
	add.l	#$12000,(a3)				; speed 8	- Mountains.
	add.l	d6,a3
	bra     MAP_POS_UPDATE

        CNOP    0,4

MAP_POS_UPDATE:
	FUNCID	#$dde4314d
        move.w  MAP_PIXEL_POSX(a4),d0                   ; 32
        and.w   #$ffe0,d0
        lsr.w   #3,d0
        move.w  d0,MAP_POSX(a4)                         ; MAP_POSX(a4) = MAP_PIXEL_POS / 8

        moveq   #0,d6
        moveq   #0,d7
        move.w  MAP_PIXEL_POSX(a4),d6           ; Pixel Position of current camera
        sub.w   #256,d6
        move.w  d6,d7                           ; Get left most position
        add.w   #(PLAYFIELD_SIZE_X*8),d7        ; Get Right most position
        lsl.w   #4,d6                           ; Interpolate to 16*16... 4 for speed interpolate
        move.w  d6,CAMERA_X1POS(a4)
        lsl.w   #4,d7                           ;
        move.w  d7,CAMERA_X2POS(a4)

        sub.w   #ENEMY_BOUNDARY_TOLERANCE,d6
        bpl.s   .is_plus
.is_minus:
        moveq   #0,d6
.is_plus:
        move.l  d6,BOUNDARY_X1POS(a4)
        add.w   #ENEMY_BOUNDARY_TOLERANCE,d7
        move.l  d7,BOUNDARY_X2POS(a4)

        rts

        CNOP    0,4

;d1=Xposition
SET_MAP_POSITION:
	FUNCID	#$227cffb2
        move.w  d1,MAP_PIXEL_POSX(a4)                   ; Pixel position
        ;sub.w   BG_START_OFFSET(a4),d1
        move.w  BG_SCROLL_SPEED(a4),d2
        lsr.w   d2,d1
        move.w  d1,MAP_OFFSET_BG(a4)                        ; Back ground position / 2
.done:  bra     MAP_POS_UPDATE



        CNOP    0,4

;a0 = objects pointer start
; $7fff = terminate
; -1 =
; x,y,top left, top right, bottom left, bottom right
BLIT_OBJECTS_INTO_CANVAS:
	FUNCID	#$3a0322fc
.loop:
        moveq   #0,d0
        moveq   #0,d1
        move.w  (a0)+,d6                                ; Read X Point
        move.w  (a0)+,d7                                ; Read Y Point

        cmp.w   #$7fff,d6                               ; finished?
        beq     .exit

.draw_block:
        move.w  (a0)+,d5                                ;Read First Tile
        cmp.w   #-1,d5
        beq.s   .loop
        move.w  d6,d0
        move.w  d7,d1
        bsr     .tile
        addq.w  #1,d6
        bra.s   .draw_block

.tile:
        move.l  a0,-(a7)
        lsr.w   #8,d5
        lsr.w   #2,d5
        subq.w  #1,d5
        lea     SPRITE_TO_TILE_MERGE_MAP(a4),a1
        move.w  (a1,d5*2),d5                    ; Get the sprite number
        lea     CANVAS_TABLE_Y3(a4),a1
        lsl.w   #4,d1                           ; multiply by 16 for y tiles depth
        move.l  (a1,d1*4),d1

; does d0 (xpos) land within the camera
        add.w   #16,d0
        move.w  d0,d4                           ; d4 has block position
        add.w   d0,d0                           ; Multiple by 2
        add.l   d0,d1
; d1 should now have screen address to plot.
        add.l   #(CANVAS_WIDTH*4),d1
        subq.w  #2,d1

; d2 and d3 have window start and end.
        move.w  MAP_PIXEL_POSX(a4),d2
        lsr.w   #4,d2                           ; d2 now has map block start
	subq.w	#2,d2
        move.w  d2,d3
        add.w   #20,d3                          ; d3 has block end

; need to check MAP_POSX(a4) to MAP_POSX(a4)+20 against X Value.
        cmp.w   d2,d4                           ; BLK START (24) < WINDOWS_START (20) = skip
        ble.s   .skip
        cmp.w   d3,d4
        bge.s   .skip

        move.l  LSTPTR_POST_SCREEN(a4),a1
        add.l   d1,a1
        bsr     .blit_item
        move.l  LSTPTR_FRONT_SCREEN(a4),a1
        add.l   d1,a1
        bsr     .blit_item
        move.l  LSTPTR_BACK_SCREEN(a4),a1
        add.l   d1,a1
        bsr     .blit_item

.skip:
        move.l  (a7)+,a0
        bra     .exit

;d5 = sprite tile object
;a1 = screen pointer
.blit_item:
        move.l  a0,-(a7)
        move.l  d1,-(a7)
        move.l  d5,-(a7)

        ;lea    SPRITE_POINTERS(a4),a0
        move.l  MEMCHK0_SPRITE_POINTERS(a4),a0
        move.l  (a0,d5*8),a2            ; Get sprite address
        move.l  4(a0,d5*8),a3           ; Get mask address

        WAIT_FOR_BLITTER
        move.l  #$0fca0000,BLTCON0(a5)  ; Select straigt A-D mode $f0
        move.l  #$ffffffff,BLTAFWM(a5)          ; No masking needed
        move.w  #SPRITE_ASSETS_SIZE_X-2,BLTAMOD(a5)     ; Mask
        move.w  #SPRITE_ASSETS_SIZE_X-2,BLTBMOD(a5)     ; Sprite
        move.w  #PLAYFIELD_SIZE_X-2,BLTCMOD(a5)                         ; Background
        move.w  #PLAYFIELD_SIZE_X-2,BLTDMOD(a5)                         ; Destination
        move.l  a3,BLTAPTH(a5)                          ; Mask
        move.l  a2,BLTBPTH(a5)                          ; Sprite
        move.l  a1,BLTCPTH(a5)                          ; Background
        move.l  a1,BLTDPTH(a5)                          ; Dest
        move.w  #(160*64)+1,BLTSIZE(a5)
        move.l  (a7)+,d5
        move.l  (a7)+,d1
        move.l  (a7)+,a0
.exit:  rts
