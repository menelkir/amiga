; d0=Number of bombs bnus

MENU_WAKE_DELAY:	equ	1

MENU_WINDOW_X:  equ     32
MENU_WINDOW_Y:  equ     32

MENU_SCORE_STEP:        equ     200
MENU_DELAY_TIME:        equ     350             ; 7 seconds

FLASH_DELAY:    equ     50
FLASH_COLOUR:   equ     $f00
TRANSITION_DELAY_COUNT: equ     400

PASSWORD_LENGTH:	equ	5
PASSWORD_CHARACTERS:	equ	26

GREETS_YPOS:	equ	179

MENU:
	FUNCID	#$9c2a9106
	
	clr.b	_mt_Enable
	
	bsr	CLEAR_FRONT_SCREEN_BUFFERS
	bsr	CLEAR_BACK_SCREEN_BUFFERS
	
	bsr	LOAD_RYGAR_LOGO
	
	move.l	d0,-(a7)
        moveq   #SND_GROWL,d0
        bsr     PLAY_SAMPLE		
	move.l	(a7)+,d0
	
	move.w	#3,MENU_OPTIONS_POS(a4)

	clr.w	START_HIGHER_ROUND(a4)

.menu:

	
        clr.w   MENU_EXIT(a4)
	clr.w	JOY_MENU_FIRE(a4)
	
	move.w	#28,MENU_FLASH_PTR(a4)

        move.l  RYGAR_LOGO_PTR(a4),a0
        lea     RYGAR_LOGO_COLOUR_PAL(a4),a1
        bsr     GET_16_PALETTE

		
	lea     RYGAR_LOGO_FLUID_PAL(a4),a0
        rept    8
	clr.l  (a0)+
        endr
	
	lea     RYGAR_LOGO_BLACK_PAL(a4),a0
        rept    8
	clr.l  (a0)+
        endr

        lea     RYGAR_LOGO_COLOUR_PAL(a4),a0
        lea     RYGAR_LOGO_GREY_PAL(a4),a1
        rept    8
        move.l  (a0)+,(a1)+
        endr

        lea     RYGAR_LOGO_GREY_PAL(a4),a1
        move.w  2(a1),d0
        bsr     SET_GREY_COL
        move.w  d0,2(a1)
        move.w  16(a1),d0
        bsr     SET_GREY_COL
        move.w  d0,16(a1)
        move.w  18(a1),d0
        bsr     SET_GREY_COL
        move.w  d0,18(a1)
        move.w  20(a1),d0
        bsr     SET_GREY_COL
        move.w  d0,20(a1)
        move.w  22(a1),d0
        bsr     SET_GREY_COL
        move.w  d0,22(a1)
        move.w  24(a1),d0
        bsr     SET_GREY_COL
        move.w  d0,24(a1)
        move.w  26(a1),d0
        bsr     SET_GREY_COL
        move.w  d0,26(a1)
        move.w  28(a1),d0
        bsr     SET_GREY_COL
        move.w  d0,28(a1)
        move.w  30(a1),d0
        bsr     SET_GREY_COL
        move.w  d0,30(a1)

        lea     RYGAR_LOGO_COLOUR_PAL(a4),a0
        lea     RYGAR_LOGO_WHITE_PAL(a4),a1
        rept    8
        move.l  (a0)+,(a1)+
        endr

        lea     RYGAR_LOGO_WHITE_PAL(a4),a1
        move.w  #$fff,2(a1)
        move.w  #$fff,16(a1)
        move.w  #$fff,18(a1)
        move.w  #$fff,20(a1)
        move.w  #$fff,22(a1)
        move.w  #$fff,24(a1)
        move.w  #$fff,26(a1)
        move.w  #$fff,28(a1)
        move.w  #$fff,30(a1)

        clr.b   TEXT_PLANE(a4)
        clr.w   TEXT_INDEX(a4)
	
        IFEQ    ENABLE_DEBUG
	bsr	WAIT_FOR_VERTICAL_BLANK	
        move.w  #%0000000110000000,DMACON(a5)		; Disable Copper
	bsr	WAIT_FOR_VERTICAL_BLANK
	ENDC
	
	bsr	CLEAR_FRONT_SCREEN_BUFFERS	
	
        bsr     SETSCR_FBUFF
        lea     LSTPTR_FRONT_SCREEN(a4),a1
        bsr     SET_FRAMEBUFF
	
        bsr     INIT_PANELS
        bsr     DRAW_RYGAR_LOGO
	
	bsr     INIT_MENU_COPPER

        IFEQ    ENABLE_DEBUG
	move.w	#COPRUN_MENU,COPRUN_TYPE(a4)
        move.l  MEMCHK2_COPPER1(a4),COP1LCH(a5)
        move.l  MEMCHK2_COPPER1(a4),COP2LCH(a5)
        ;         FEDCBA9876543210
        move.w  #%1000011111000000,DMACON(a5)
        ENDC

	bsr     LOGO_TRANS_BLACK_TO_GREY

;-----------------------------------------
	move.w	#$ffff,MENU_OPTIONS_OLDSEL(a4)
	bsr	MENU_GAME_CONFIG					
;-----------------------------------------

.update:
	move.w	#MENU_WAKE_DELAY,MENU_OPTIONS_WAKE(a4)

.vbloop:


	subq.w	#1,MENU_OPTIONS_WAKE(a4)
	tst.w	MENU_OPTIONS_WAKE(a4)
	bmi.s	.show_hiscore
	
	tst.w	MENU_EXIT(a4)
	bmi.s	.exit

        bsr     TRANSITION_DELAY

        bsr     LOGO_TRANS_GREY_TO_WHITE
	
	tst.w	MENU_EXIT(a4)
	bmi.s	.exit	
        bsr     LOGO_TRANS_WHITE_TO_COLOUR

        bsr     TRANSITION_DELAY
        bsr     LOGO_TRANS_COLOUR_TO_WHITE
	
	tst.w	MENU_EXIT(a4)
	bmi.s	.exit
        bsr     LOGO_TRANS_WHITE_TO_GREY
        bra.s   .vbloop
.exit:  bsr	LOGO_TRANS_WHITE_TO_BLACK
	moveq   #0,d6
	rts

.show_hiscore:
	bsr	HISCORE
	bra	.menu


;----------------------------------------------------------------------------------------------
; VERTICAL BLANK!!!!
;--------------------------------------
MENU_WAIT_FOR_VERTICAL_BLANK:
	FUNCID	#$561e439b
	movem.l	d0-d7/a0-a3,-(a7)
        bsr     WAIT_FOR_VERTICAL_BLANK
	
	IFNE	DEBUG_TEXT_ENABLE
	bsr     DEBUG_TEXT
	ENDC
	
	tst.w	ENABLE_SCROLL(a4)
	beq.s	.skip_scroll
	
	bsr	MENU_SCROLLER
	
.skip_scroll:
	lea	LSTPTR_FRONT_SCREEN(a4),a1	
	lea	LINE_BLANK(a4),a0		; 'Enter Password'
	moveq	#4,d0				; x pos
	moveq	#12,d1				; y pos
	moveq	#0,d6				; colour
	bsr	DRAW_MENU_TEXT

	lea	LSTPTR_FRONT_SCREEN(a4),a1	
	lea	TEXT_PASSWORD(a4),a0		; 'Enter Password'
	moveq	#11,d0				; x pos
	moveq	#12,d1				; y pos
	moveq	#0,d6				; colour
	bsr	DRAW_MENU_TEXT
	
	tst.w	MENU_FLASH_PTR(a4)
	bpl.s	.flash
	move.w	#20,MENU_FLASH_PTR(a4)
	
.flash:
	move.w	MENU_FLASH_PTR,d0
	lea	MENU_FLASH_PAL(a4),a0
	move.w	(a0,d0*2),MENU_FLASH_COL(a4)
	subq.w	#1,MENU_FLASH_PTR(a4)
	
; Check Password Entry.
	lea	PASSENTRY_INITIALS(a4),a0
	moveq	#1,d6
	bsr	DRAW_PASSENTRY_INITIALS	
	lea	PASSENTRY_BLANK(a4),a0	
	moveq	#4,d6
	bsr	DRAW_PASSENTRY_INITIALS	
	
	tst.w	PASSWORD_ENTRY_ENABLE(a4)
	beq	.pwd_entry_done

	bsr	MENU_CLEAR_ENTER_PASSWORD

	lea	LSTPTR_FRONT_SCREEN(a4),a1	
	lea	LINE_BLANK(a4),a0		; 'Enter Password'
	moveq	#4,d0				; x pos
	moveq	#12,d1				; y pos
	moveq	#0,d6				; colour
	bsr	DRAW_MENU_TEXT

	lea	LSTPTR_FRONT_SCREEN(a4),a1	
	lea	TEXT_PASSWORD(a4),a0		; 'Enter Password'
	moveq	#11,d0				; x pos
	moveq	#12,d1				; y pos
	moveq	#1,d6				; colour
	bsr	DRAW_MENU_TEXT

	lea	PASSENTRY_BLANK(a4),a0
	moveq	#0,d6
	bsr	DRAW_PASSENTRY_INITIALS
	lea	PASSENTRY_BLANK(a4),a0
	moveq	#1,d6
	bsr	DRAW_PASSENTRY_INITIALS
	lea	PASSENTRY_INITIALS(a4),a0	
	moveq	#0,d6
	bsr	DRAW_PASSENTRY_INITIALS	
	
	bsr	MENU_GAME_CONFIG
	
	bsr	PASSWORD_INPUT	
	bra	.exit
.pwd_entry_done:
	clr.w	PASSWORD_ENTRY_ENABLE(a4)
	
; Normal menu config input.
	bsr	MENU_GAME_CONFIG
	bsr	MENU_JOY_DETECT
	tst.w	d6
	bmi.s	.menu_exit

	bra	.exit

.menu_exit:	;move.w  #-1,MENU_EXIT(a4)
	nop

.exit:  movem.l	(a7)+,d0-d7/a0-a3

	rts





TRANSITION_DELAY:
	FUNCID	#$383817de
	
        move.l  #TRANSITION_DELAY_COUNT,d7
.loop:  bsr     MENU_WAIT_FOR_VERTICAL_BLANK
        tst.w   MENU_EXIT(a4)
        bmi.s   .exit
        dbf     d7,.loop
.exit:  rts



MENU_JOY_NO_MOVE:
	FUNCID	#$8c3de035
	
	cmp.w	JOY_MENU_INPUT(a4),d3
	beq.s	.exit
	move.w	d3,JOY_MENU_INPUT(a4)
.exit:	rts

MENU_SELECT_UP:
	FUNCID	#$b203d985
	
	cmp.w	JOY_MENU_INPUT(a4),d3
	beq.s	.exit
	tst.w	MENU_OPTIONS_POS(a4)
	beq.s	.reset
	subq.w	#1,MENU_OPTIONS_POS(a4)
	move.w	d3,JOY_MENU_INPUT(a4)
	bra.s	.exit
.reset:	clr.w	MENU_OPTIONS_POS(a4)
.exit:	rts

MENU_SELECT_DOWN:
	FUNCID	#$b3370f3e
	
	cmp.w	JOY_MENU_INPUT(a4),d3
	beq.s	.exit
	cmp.w	#3,MENU_OPTIONS_POS(a4)
	beq.s	.reset
	addq.w	#1,MENU_OPTIONS_POS(a4)
	move.w	d3,JOY_MENU_INPUT(a4)
	bra.s	.exit
.reset:	move.w	#3,MENU_OPTIONS_POS(a4)
.exit:	rts

MENU_JOY_DETECT:
	FUNCID	#$0ab72922
	
	btst	#7,CIAAPRA		; Left mouse pressed?
	beq.s	MENU_SELECT
	clr.w	JOY_MENU_FIRE(a4)	; Set fire button not pressed

	bsr	JOY_DETECT
	cmp.w	#JOY1_NO_MOVE,d3		
	beq	MENU_JOY_NO_MOVE
	
	move.w	#MENU_WAKE_DELAY,MENU_OPTIONS_WAKE(a4)
	cmp.w	#JOY1_UP,d3
	beq	MENU_SELECT_UP
	cmp.w	#JOY1_DOWN,d3
	beq	MENU_SELECT_DOWN
	rts


MENU_SELECT:
	FUNCID	#$db8a8a16
	
	moveq	#0,d6
	tst.w	JOY_MENU_FIRE(a4)	; Is fire button still pressed?
	bmi.s	.exit				; Yes, reloop
	move.w	#-1,JOY_MENU_FIRE(a4)	; no, set fire button pressed

	cmp.w	#0,MENU_OPTIONS_POS(a4)
	beq	MENU_ENTER_PASSWORD
	cmp.w	#1,MENU_OPTIONS_POS(a4)
	beq	MENU_TOGGLE_DIFFICULTY
	cmp.w	#2,MENU_OPTIONS_POS(a4)
	beq	MENU_TOGGLE_BUTTONS
	cmp.w	#3,MENU_OPTIONS_POS(a4)
	beq	MENU_START_GAME
.exit:	rts

MENU_ENTER_PASSWORD:
	FUNCID	#$acabe0fc
	move.w	#-1,PASSWORD_ENTRY_ENABLE(a4)
.exit:	rts

MENU_TOGGLE_DIFFICULTY:
	FUNCID	#$36b04bb0
	move.w	MENU_OPTIONS_SEL(a4),d2
	bchg	#1,d2
	move.w	d2,MENU_OPTIONS_SEL(a4)
	;not.w	SOUND_ON(a4)
	rts
	
MENU_TOGGLE_BUTTONS:
	FUNCID	#$af28082b
	
	move.w	MENU_OPTIONS_SEL(a4),d2
	bchg	#2,d2
	move.w	d2,MENU_OPTIONS_SEL(a4)
	not.w	CURRENT_BUTTON_MODE(a4)
	rts

MENU_START_GAME:
	FUNCID	#$cfa3db35
	
	move.w	#-1,MENU_EXIT(a4)
	rts

;d2=Selection
;d3=Config
MENU_GAME_CONFIG:
	FUNCID	#$018355e5
	
	move.w	MENU_OPTIONS_SEL(a4),d2
	move.w	MENU_OPTIONS_POS(a4),d3
	cmp.w	MENU_OPTIONS_OLDSEL(a4),d2
	bne.s	.go
	cmp.w	MENU_OPTIONS_OLDPOS(a4),d3
	bne.s	.go
	
; d3 = position
; Set new position in the copper.
	lea	MENU_COPPER_POSITIONS(a4),a1
	move.w	#$0af,d0
	rept	4
	move.l	(a1)+,a0
	move.w	d0,6(a0)
	endr
	
	lea	MENU_COPPER_POSITIONS(a4),a1
	move.l	(a1,d3*4),a0
	move.w	MENU_FLASH_COL(a4),6(a0)
	
	bra	.exit

; Game menu config text
.go:
	move.w	d2,MENU_OPTIONS_OLDSEL(a4)
	move.w	d3,MENU_OPTIONS_OLDPOS(a4)

	move.w	#MENU_DELAY_TIME,MENU_DELAY

; Draw all menu items.
	lea	LSTPTR_FRONT_SCREEN(a4),a1	
	lea	TEXT_PASSWORD(a4),a0		; 'Enter Password'
	moveq	#11,d0				; x pos
	moveq	#12,d1				; y pos
	moveq	#0,d6				; colour
	bsr	DRAW_MENU_TEXT

	lea	TEXT_DIFFICULTY(a4),a0		; 'Difficulty'
	lea	LSTPTR_FRONT_SCREEN(a4),a1	
	moveq	#11,d0				; x pos
	moveq	#15,d1				; y pos
	moveq	#0,d6				; colour
	bsr	DRAW_MENU_TEXT

	lea	LSTPTR_FRONT_SCREEN(a4),a1
	bsr	DIFFICULTY_BEGINNER
	lea	TEXT_DIFFICULTY_EASY(a4),a0
	btst	#1,d2				; 
	beq	.difficulty
	bsr	DIFFICULTY_WARRIOR
	lea	TEXT_DIFFICULTY_HARD(a4),a0
.difficulty:
	moveq	#13,d0				; x pos
	moveq	#16,d1				; y pos
	moveq	#1,d6				; colour
	bsr	DRAW_MENU_TEXT

	lea	TEXT_CONTROLLER(a4),a0
	lea	LSTPTR_FRONT_SCREEN(a4),a1	
	moveq	#11,d0				; x pos
	moveq	#18,d1				; y pos
	moveq	#0,d6				; colour
	bsr	DRAW_MENU_TEXT

; Do buttons text
	lea	LSTPTR_FRONT_SCREEN(a4),a1	
	lea	TEXT_TWO_BUTT(a4),a0
	btst	#2,d2				; Buttons one or two?
	beq	.buttons
	lea	TEXT_ONE_BUTT(a4),a0
.buttons:
	lea	LSTPTR_FRONT_SCREEN(a4),a1	
	moveq	#13,d0				; x pos
	moveq	#19,d1				; y pos
	moveq	#1,d6				; colour
	bsr	DRAW_MENU_TEXT
	
	lea	TEXT_START_GAME(a4),a0
	lea	LSTPTR_FRONT_SCREEN(a4),a1	
	moveq	#11,d0				; x pos
	moveq	#21,d1				; y pos
	moveq	#0,d6				; colour
	bsr	DRAW_MENU_TEXT
.exit:	rts



LOAD_RYGAR_LOGO:
	LEVEL5_STOP
	move.l	MEMCHK1_POST_SCROLL(a4),a0
	lea	FILE_RYGAR_LOGO(a4),a1
        move.l  MEMCHK5_LOADBUFF(a4),a2
        sub.l   a3,a3
        moveq   #0+64,d0

        IFNE    ENABLE_DEBUG
        move.w  #$4000,$dff09a
        ELSE
        move.w #$8,$dff09a
        ENDC
        bsr     ocean
	beq	DOS_ERROR
        moveq   #-3,d0
        bsr     ocean
        IFNE    ENABLE_DEBUG
        move.w  #$4000,$dff09a
        ELSE
        move.w #$8,$dff09a
        ENDC
        move.l  MEMCHK1_POST_SCROLL(a4),a0          ; source	
        move.l  MEMCHK1_POST_SCROLL(a4),a1          ; dest
        bsr     Unpack
	move.l	a0,RYGAR_LOGO_PTR(a4)
	LEVEL5_START
	rts

; ----------------------------
; Draw BOMBJACK logo
;----------------------------

;a1=Screen pointers
DRAW_RYGAR_LOGO:
	FUNCID	#$9e702885
	
        lea     LSTPTR_FRONT_SCREEN(a4),a1

        moveq   #0,d0
        moveq   #0,d1
        moveq   #0,d2
        moveq   #0,d3   ; 16
        moveq   #0,d4
        moveq   #0,d5
        moveq   #0,d6   ; 28

        move.l  #223*5,d7
        move.l  (a1)+,a2
.bp1    movem.l d0-d6,(a2)
        add.w   #28,a2
        dbf     d7,.bp1

        move.l  RYGAR_LOGO_PTR(a4),a0
.loop:  cmp.l   #"BODY",(a0)
        beq.s   .found
        addq.w  #2,a0
        bra.s   .loop

.found:
        addq.w  #8,a0

;a0=bitmap - width = = 24
;a1=screen pointer - width = 40 bytes

        moveq   #64-1,d7
        lea     LSTPTR_FRONT_SCREEN(a4),a1
        move.l  (a1),a1
        add.l   #(PLAYFIELD_SIZE_X*4)*16,a1
        addq.w  #8,a1

.logo:
        rept    12
        move.w  (a0)+,(a1)+
        endr
        add.w   #16,a1                  ; Playfield Modulo
        rept    12
        move.w  (a0)+,(a1)+
        endr
        add.w   #16,a1                  ; Playfield Modulo
        rept    12
        move.w  (a0)+,(a1)+
        endr
        add.w   #16,a1                  ; Playfield Modulo
        rept    12
        move.w  (a0)+,(a1)+
        endr
        add.w   #16,a1                  ; Playfield Modulo
        dbf     d7,.logo
        rts





; d0=8x8 x
; d1=8x8 y
; d6=Colour number 0-3 (0=BP1, 1=BP2, 2=BP3, 3=BP4, 4=BP5)
; d7=-1 Draw on nibble 0=byte
; a0=text
; a1=Screen addresses
DRAW_MENU_TEXT:
	FUNCID	#$6d4b5ac1
	
        movem.l d0-d7/a0-a3,-(a7)
	addq.w	#2,d0
        lsl.w   #3,d1
        mulu    #PLAYFIELD_SIZE_X*4,d1
        lsl.w   #2,d6
        lea     LSTPTR_FRONT_SCREEN(a4),a1
        move.l  (a1,d6),a1              ; Bitplane to draw in.
        add.l   d1,a1
        add.l   d0,a1

        moveq   #0,d0
        move.b  (a0)+,d7
.loop:  ;lea    FONT_ASSET(a4)+86,a2    ; start of BODY
        move.l  FONT_ASSET(a4),a2
        add.w   #86,a2                  ; Offset to BODY
        tst.b   (a0)
        beq     .exit
        move.b  (a0)+,d0
        sub.b   #32,d0

.draw:
        add.l   d0,a2           ; index into character

        move.b  (a2),(a1)
        move.b  FONT_WIDTH*1(a2),PLAYFIELD_SIZE_X*4*1(a1)
        move.b  FONT_WIDTH*2(a2),PLAYFIELD_SIZE_X*4*2(a1)
        move.b  FONT_WIDTH*3(a2),PLAYFIELD_SIZE_X*4*3(a1)
        move.b  FONT_WIDTH*4(a2),PLAYFIELD_SIZE_X*4*4(a1)
        move.b  FONT_WIDTH*5(a2),PLAYFIELD_SIZE_X*4*5(a1)
        move.b  FONT_WIDTH*6(a2),PLAYFIELD_SIZE_X*4*6(a1)
        move.b  FONT_WIDTH*7(a2),PLAYFIELD_SIZE_X*4*7(a1)
        addq.w  #1,a1
        bra.s     .loop

.exit:  movem.l (a7)+,d0-d7/a0-a3
        rts



;
; INIT_MENU_COPPER
;
; Initialize the Copper list
;
INIT_MENU_COPPER:
	FUNCID	#$4535b6f1
	
        move.l  MEMCHK2_COPPER1(a4),a1
        move.l  a1,d4

        move.w  #DDFSTRT,(a1)+  ;
        move.w  #$0038,(a1)+    ; 3c
        move.w  #DDFSTOP,(a1)+  ;
        move.w  #$00d0,(a1)+    ; a4

        move.l  #$1c01ff00,(a1)+
        move.l  a1,COPPTR_UPPER_PANEL(a4)
        bsr     INIT_COPPER_SPRITES

        bsr     COPPER_UPPER_PANEL

        move.l  #$3e0dfffe,(a1)+

        move.w  #FMODE,(a1)+
        move.w  #%0000000000001100,(a1)+
        
	move.w  #BPLCON0,(a1)+  ; 0             ; This is play area
        move.w  #$0000,(a1)+    ; 2             ; 8 PLanes here
        move.w  #BPLCON1,(a1)+  ; 4
        move.w  #0,(a1)+        ; 6
        move.w  #BPLCON2,(a1)+  ; 8
        move.w  #$00,(a1)+      ; 30
        move.w  #BPL1MOD,(a1)+  ;
        move.w  #$78,(a1)+      ; a0
        move.w  #BPL2MOD,(a1)+  ;
        move.w  #$78,(a1)+      ; a0
	
        move.w  #DIWSTRT,(a1)+  ;
        move.w  #$27a1,(a1)+    ;		27a1
        move.w  #DIWSTOP,(a1)+  ;
        move.w  #$1098,(a1)+    ;		10a8

        lea     COPPTR_HARDWARE_UPPER_SPRITE_0(a4),a3
        bsr     CREATE_COPPER_SPRITE_POINTERS

; Load Main Palette
        lea     RYGAR_LOGO_BLACK_PAL(a4),a0
        move.l  #$180,d2
        moveq   #16-1,d7
        move.l  a1,COPPTR_RYGAR_LOGO_PAL(a4)

.main_pal:
        move.w  d2,(a1)+
        move.w  (a0)+,(a1)+
        addq.w  #2,d2
        dbf     d7,.main_pal

        lea     LSTPTR_FRONT_SCREEN(a4),a0
        moveq   #4-1,d7                 ; number of planes to load
        move.w  #BPL0PTL,d2
        move.w  #BPL0PTH,d3

.plane: move.l  (a0)+,d0
        move.w  d2,(a1)+
        move.w  d0,(a1)+
        swap    d0
        move.w  d3,(a1)+
        move.w  d0,(a1)+
        addq.l  #4,d2
        addq.l  #4,d3
        dbf     d7,.plane

        move.l  #$400dfffe,(a1)+
        move.w  #BPLCON0,(a1)+  ; 0             ; This is play area
        move.w  #$4200,(a1)+    ; 2             ; 8 PLanes here


; Wait for line 26
	
        move.l  #$9f01ff00,(a1)+
        move.l  #$01820c00,(a1)+
        move.l  #$018400c0,(a1)+		; 1 2 4 8 10
        move.l  #$01880cc0,(a1)+
        move.l  #$01900ccf,(a1)+
	
	lea	MENU_COPPER_POSITIONS(a4),a2
	move.l	a1,(a2)+
	move.l	#$a001ff00,(a1)+		; Position 1
	move.l	#$01820c00,(a1)+
	move.l	a1,(a2)+	
	move.l	#$b801ff00,(a1)+		; Position 2
	move.l	#$01820c00,(a1)+
	move.l	a1,(a2)+
	move.l	#$c001ff00,(a1)+		; Position 3
	move.l	#$01820c00,(a1)+
	move.l	a1,(a2)+	
	move.l	#$d801ff00,(a1)+		; Position 4
	move.l	#$01820c00,(a1)+

;----------------------

	move.l  #$f301ff00,(a1)+                
        move.l  #$018200a0,(a1)+
	move.l  #$f301ff00,(a1)+                
        move.l  #$01840a00,(a1)+	
	move.l  #$f301ff00,(a1)+                
        move.l  #$0188000f,(a1)+
	move.l  #$f301ff00,(a1)+                
        move.l  #$01900aa0,(a1)+
	
        move.l  #$f401ff00,(a1)+                
        move.l  #$018201b1,(a1)+
        move.l  #$f401ff00,(a1)+                
        move.l  #$01840b00,(a1)+   
	move.l  #$f401ff00,(a1)+                
        move.l  #$0188011f,(a1)+	
	move.l  #$f401ff00,(a1)+                
        move.l  #$01900bb0,(a1)+
	
	move.l  #$f501ff00,(a1)+                
        move.l  #$018202c2,(a1)+
        move.l  #$f501ff00,(a1)+                
        move.l  #$01840c00,(a1)+  
	move.l  #$f501ff00,(a1)+                
        move.l  #$0188022f,(a1)+
	move.l  #$f501ff00,(a1)+                
        move.l  #$01900cc0,(a1)+	
	
	move.l  #$f601ff00,(a1)+                
        move.l  #$018203d3,(a1)+
        move.l  #$f601ff00,(a1)+                
        move.l  #$01840d00,(a1)+
	move.l  #$f601ff00,(a1)+                
        move.l  #$0188033f,(a1)+  
	move.l  #$f601ff00,(a1)+                
        move.l  #$01900dd0,(a1)+	
	
	move.l  #$f701ff00,(a1)+                
        move.l  #$018204e4,(a1)+
        move.l  #$f701ff00,(a1)+                
        move.l  #$01840e00,(a1)+
	move.l  #$f701ff00,(a1)+                
        move.l  #$0188044f,(a1)+  
	move.l  #$f701ff00,(a1)+                
        move.l  #$01900ee0,(a1)+	
	
	move.l  #$f801ff00,(a1)+                
        move.l  #$018205f5,(a1)+
        move.l  #$f801ff00,(a1)+                
        move.l  #$01840f00,(a1)+
	move.l  #$f801ff00,(a1)+                
        move.l  #$0188055f,(a1)+
	move.l  #$f801ff00,(a1)+                
        move.l  #$01900ff0,(a1)+	
	
	move.l  #$f901ff00,(a1)+                
        move.l  #$018206f6,(a1)+
        move.l  #$f901ff00,(a1)+                
        move.l  #$01840f11,(a1)+
	move.l  #$f901ff00,(a1)+                
        move.l  #$0188066f,(a1)+
	move.l  #$f901ff00,(a1)+                
        move.l  #$01900ff1,(a1)+	
	
	move.l  #$fa01ff00,(a1)+                
        move.l  #$018207f7,(a1)+
        move.l  #$fa01ff00,(a1)+                
        move.l  #$01840f22,(a1)+
	move.l  #$fa01ff00,(a1)+                
        move.l  #$0188077f,(a1)+
	move.l  #$fa01ff00,(a1)+                
        move.l  #$01900ff2,(a1)+	
	
	move.l  #$fb01ff00,(a1)+
        move.l  #$01820000,(a1)+
        move.l  #$fb01ff00,(a1)+                
        move.l  #$01840f33,(a1)+
	move.l  #$fb01ff00,(a1)+                
        move.l  #$0188088f,(a1)+
	move.l  #$fb01ff00,(a1)+                
        move.l  #$01900ff3,(a1)+	
	
	move.l  #$fc01ff00,(a1)+
        move.l  #$018208f8,(a1)+
        move.l  #$fc01ff00,(a1)+                
        move.l  #$01840f44,(a1)+
	move.l  #$fc01ff00,(a1)+                
        move.l  #$0188099f,(a1)+
	move.l  #$fc01ff00,(a1)+                
        move.l  #$01900ff4,(a1)+	
	
	move.l	#$fd01ff00,(a1)+		; 	
        move.l  #$01820c00,(a1)+
        move.l  #$018400c0,(a1)+		; 182 184 188 190
        move.l  #$01880cc0,(a1)+
        move.l  #$01900ccf,(a1)+
	
	
	IFEQ    DEBUG_TEXT_ENABLE
        bsr     COPPER_LOWER_PANEL
        ELSE
        move.l  #$3e0dfffe,(a1)+
        move.w  #FMODE,(a1)+
        move.w  #%0000000000000101,(a1)+                ; 4x fetch and 32x sprites
        move.w  #DDFSTRT,(a1)+  ;
        move.w  #$0038,(a1)+    ; 3c
        move.w  #DDFSTOP,(a1)+  ;
        move.w  #$00B0,(a1)+    ; a4

        move.w  #BPLCON0,(a1)+          ; turn off the bitplanes
        move.w  #$0000,(a1)+
; Set display properties, modulo, bitplanes...

        move.w  #BPLCON2,(a1)+  ; 8
        move.w  #%00100100,(a1)+        ; 30            %0110000    000100100   = %100100 Sprites have pritority
        move.w  #BPLCON4,(a1)+
        move.w  #$0011,(a1)+
        move.w  #BPL1MOD,(a1)+  ;
        move.w  #$A4,(a1)+      ; a0
        move.w  #BPL2MOD,(a1)+  ;
        move.w  #$A4,(a1)+      ; a0
        bsr     COPPER_DEBUG_TEXT
        ENDC

	lea	DUMMY_BPLCON0(a4),a3
        move.l  a3,COPPTR_BPLCON(a4)

        move.l  #$fffffffe,(a1)+
        rts

SET_GREY_COL:
	FUNCID	#$02c4072f
	
        and.w   #$0f0,d0
        move.w  d0,d1
        lsr.w   #4,d1
        or.w    d1,d0
        lsl.w   #8,d1
        or.w    d1,d0
        rts


MENU_COPY_PAL_TO_COPPER:
	FUNCID	#$111d07f2
	
        move.l  a0,-(a7)
        move.l  a1,-(a7)
        lea     RYGAR_LOGO_FLUID_PAL(a4),a0
        move.l  COPPTR_RYGAR_LOGO_PAL(a4),a1

        rept    16
        move.w  (a0)+,2(a1)
        addq.w  #4,a1
        endr

        move.l  (a7)+,a1
        move.l  (a7)+,a0

        rts

LOGO_TRANS_GREY_TO_WHITE:
	FUNCID	#$d8bad071
	
        moveq   #16-1,d7

.loop:
        bsr     MENU_WAIT_FOR_VERTICAL_BLANK
        bsr     MENU_COPY_PAL_TO_COPPER

        lea     RYGAR_LOGO_GREY_PAL(a4),a0
        lea     RYGAR_LOGO_WHITE_PAL(a4),a1
        lea     RYGAR_LOGO_FLUID_PAL(a4),a2
        lea     32(a0),a3

.next:  move.w  (a0)+,d2
        move.w  (a1)+,d1
        bsr     COLOUR_INTERPOLATE
        move.w  d0,(a2)+
        cmp.l   a0,a3
        bne.s   .next
        dbf     d7,.loop
        rts


LOGO_TRANS_WHITE_TO_COLOUR:
	FUNCID	#$167922ec
	
        moveq   #16-1,d7

.loop:
        bsr     MENU_WAIT_FOR_VERTICAL_BLANK
        bsr     MENU_COPY_PAL_TO_COPPER

        lea     RYGAR_LOGO_WHITE_PAL(a4),a0
        lea     RYGAR_LOGO_COLOUR_PAL(a4),a1
        lea     RYGAR_LOGO_FLUID_PAL(a4),a2
        lea     32(a0),a3

.next:  move.w  (a0)+,d2
        move.w  (a1)+,d1
        bsr     COLOUR_INTERPOLATE
        move.w  d0,(a2)+
        cmp.l   a0,a3
        bne.s   .next
        dbf     d7,.loop
        rts

LOGO_TRANS_COLOUR_TO_WHITE:
	FUNCID	#$3c578b9a
	
        moveq   #16-1,d7

.loop:
        bsr     MENU_WAIT_FOR_VERTICAL_BLANK
        bsr     MENU_COPY_PAL_TO_COPPER

        lea     RYGAR_LOGO_COLOUR_PAL(a4),a0
        lea     RYGAR_LOGO_WHITE_PAL(a4),a1
        lea     RYGAR_LOGO_FLUID_PAL(a4),a2
        lea     32(a0),a3

.next:  move.w  (a0)+,d2
        move.w  (a1)+,d1
        bsr     COLOUR_INTERPOLATE
        move.w  d0,(a2)+
        cmp.l   a0,a3
        bne.s   .next
        dbf     d7,.loop
        rts


LOGO_TRANS_WHITE_TO_GREY:
	FUNCID	#$70fe6b67
	
        moveq   #16-1,d7

.loop:
        bsr     MENU_WAIT_FOR_VERTICAL_BLANK
        bsr     MENU_COPY_PAL_TO_COPPER

        lea     RYGAR_LOGO_WHITE_PAL(a4),a0
        lea     RYGAR_LOGO_GREY_PAL(a4),a1
        lea     RYGAR_LOGO_FLUID_PAL(a4),a2
        lea     32(a0),a3

.next:  move.w  (a0)+,d2
        move.w  (a1)+,d1
        bsr     COLOUR_INTERPOLATE
        move.w  d0,(a2)+
        cmp.l   a0,a3
        bne.s   .next
        dbf     d7,.loop
        rts
	
LOGO_TRANS_BLACK_TO_GREY:
	FUNCID	#$70fe6b67
	
        moveq   #16-1,d7

.loop:
        bsr     MENU_WAIT_FOR_VERTICAL_BLANK
        bsr     MENU_COPY_PAL_TO_COPPER

        lea     RYGAR_LOGO_BLACK_PAL(a4),a0
        lea     RYGAR_LOGO_GREY_PAL(a4),a1
        lea     RYGAR_LOGO_FLUID_PAL(a4),a2
        lea     32(a0),a3

.next:  move.w  (a0)+,d2
        move.w  (a1)+,d1
        bsr     COLOUR_INTERPOLATE
        move.w  d0,(a2)+
        cmp.l   a0,a3
        bne.s   .next
        dbf     d7,.loop
        rts
	
LOGO_TRANS_FLUID_TO_BLACK:
	FUNCID	#$70fe6b67
	
        moveq   #16-1,d7

.loop:
        bsr     MENU_WAIT_FOR_VERTICAL_BLANK
        bsr     MENU_COPY_PAL_TO_COPPER

        lea     RYGAR_LOGO_FLUID_PAL(a4),a0
        lea     RYGAR_LOGO_BLACK_PAL(a4),a1
        lea     RYGAR_LOGO_FLUID_PAL(a4),a2
        lea     32(a0),a3

.next:  move.w  (a0)+,d2
        move.w  (a1)+,d1
        bsr     COLOUR_INTERPOLATE
        move.w  d0,(a2)+
        cmp.l   a0,a3
        bne.s   .next
        dbf     d7,.loop
        rts


LOGO_TRANS_WHITE_TO_BLACK:
	FUNCID	#$70fe6b67
	
        moveq   #16-1,d7

.loop:
        bsr     MENU_WAIT_FOR_VERTICAL_BLANK
        bsr     MENU_COPY_PAL_TO_COPPER

        lea     RYGAR_LOGO_WHITE_PAL(a4),a0
        lea     RYGAR_LOGO_BLACK_PAL(a4),a1
        lea     RYGAR_LOGO_FLUID_PAL(a4),a2
        lea     32(a0),a3

.next:  move.w  (a0)+,d2
        move.w  (a1)+,d1
        bsr     COLOUR_INTERPOLATE
        move.w  d0,(a2)+
        cmp.l   a0,a3
        bne.s   .next
        dbf     d7,.loop
        rts	
	
MENU_CLEAR_ENTER_PASSWORD:
	lea	LSTPTR_FRONT_SCREEN(a4),a1	
	lea	LINE_BLANK(a4),a0		; 'Enter Password'
	moveq	#4,d0				; x pos
	moveq	#12,d1				; y pos
	movem.l	d0-d5/a0-a3,-(a7)
	moveq	#0,d6				; colour
	bsr	DRAW_MENU_TEXT
	movem.l	(a7)+,d0-d5/a0-a3

	movem.l	d0-d5/a0-a3,-(a7)
	moveq	#1,d6				; colour
	bsr	DRAW_MENU_TEXT
	movem.l	(a7)+,d0-d5/a0-a3

	movem.l	d0-d5/a0-a3,-(a7)
	moveq	#2,d6				; colour
	bsr	DRAW_MENU_TEXT
	movem.l	(a7)+,d0-d5/a0-a3

	movem.l	d0-d5/a0-a3,-(a7)
	moveq	#3,d6				; colour
	bsr	DRAW_MENU_TEXT
	movem.l	(a7)+,d0-d5/a0-a3

	movem.l	d0-d5/a0-a3,-(a7)
	moveq	#4,d6				; colour
	bsr	DRAW_MENU_TEXT
	movem.l	(a7)+,d0-d5/a0-a3	
	rts
	


;-----------------------------------------


;---------------------------------------------
; Start the high score input initials loop
;=============================================

PASSWORD_INPUT:
	move.w	#MENU_WAKE_DELAY,MENU_OPTIONS_WAKE(a4)
	
	lea	PASSENTRY_SELECT_CHARS(a4),a0
	add.w	PASSWORD_CHAR_POSITION(a4),a0
	lea	PASSENTRY_INITIALS+1(a4),a1
	add.w	PASSWORD_ENTRY_OFFSET(a4),a1

	move.b	(a0),(a1)

	btst	#7,CIAAPRA		; Left mouse pressed?
	beq.s	PASSWORD_CHAR_ENTER
	clr.w	JOY_PASSWORD_FIRE(a4)	; Set fire button not pressed

	bsr	JOY_DETECT
	cmp.w	#JOY1_NO_MOVE,d3		
	beq.s	PASSWORD_JOY_NO_MOVE

	cmp.w	#JOY1_LEFT,d3
	beq.s	PASSWORD_CHAR_LEFT
	cmp.w	#JOY1_RIGHT,d3
	beq.s	PASSWORD_CHAR_RIGHT

.no_move:	
	rts

PASSWORD_JOY_NO_MOVE:
	cmp.w	JOY_PASSWORD_INPUT(a4),d3
	beq.s	.exit
	move.w	d3,JOY_PASSWORD_INPUT(a4)
.exit:	rts

; ---------------------
; There is a stupid bug in here when the initials entry move
; that i need to fix.
;======================
PASSWORD_CHAR_RIGHT:
	cmp.w	JOY_PASSWORD_INPUT(a4),d3
	beq.s	.exit
	tst.w	PASSWORD_CHAR_POSITION(a4)
	beq.s	.reset
	subq.w	#1,PASSWORD_CHAR_POSITION(a4)
	move.w	d3,JOY_PASSWORD_INPUT(a4)
	bra.s	.exit
.reset:	move.w	#PASSWORD_CHARACTERS,PASSWORD_CHAR_POSITION(a4)
.exit:	rts

PASSWORD_CHAR_LEFT:
	cmp.w	JOY_PASSWORD_INPUT(a4),d3
	beq.s	.exit
	
	addq.w	#1,PASSWORD_CHAR_POSITION(a4)
	cmp.w	#PASSWORD_CHARACTERS,PASSWORD_CHAR_POSITION(a4)
	beq.s	.reset

	move.w	d3,JOY_PASSWORD_INPUT(a4)
	bra.s	.exit
.reset:	clr.w	PASSWORD_CHAR_POSITION(a4)
	move.w	#-1,PASSWORD_CHAR_POSITION(a4)
.exit:	rts

PASSWORD_CHAR_ENTER:	
	tst.w	JOY_PASSWORD_FIRE(a4)	; Is fire button still pressed?
	bmi.s	.exit				; Yes, reloop
	move.w	#-1,JOY_PASSWORD_FIRE(a4)	; no, set fire button pressed

	lea	PASSENTRY_SELECT_CHARS(a4),a0
	add.w	PASSWORD_CHAR_POSITION(a4),a0

	lea	PASSENTRY_INITIALS+1(a4),a1
	add.w	PASSWORD_ENTRY_OFFSET(a4),a1	
	move.b	(a0),1(a1)

	addq.w	#1,PASSWORD_ENTRY_OFFSET(a4)	; add 1 to the initial entry

	cmp.w	#PASSWORD_LENGTH,PASSWORD_ENTRY_OFFSET(a4)
	beq.s	.save_password
	bra.s	.exit
.save_password:
	clr.w	PASSWORD_ENTRY_OFFSET(a4)
	;addq.w	#1,PASSWORD_ENTRY_OFFSET(a4)


	clr.w	PASSWORD_ENTRY_ENABLE(a4)
	bsr	MENU_CLEAR_ENTER_PASSWORD

	lea	PASSENTRY_INITIALS+1(a4),a1
	clr.b	PASSWORD_LENGTH(a1)				; Terminate.
	
	bsr	FIND_ROUND
	
	;lea	PASSENTRY_INITIALS(a4),a1
	;move.b	(a1)+,(a0)+			;Initial 1
	;move.b	(a1)+,(a0)+			;Initial 2
	;move.b	(a1)+,(a0)+			;Initial 3
	;clr.b	(a0)
.exit:	rts


FIND_ROUND:
	lea	PASSENTRY_INITIALS+1(a4),a0
	lea	ROUND_PASSWORDS(a4),a1
	moveq	#36-1,d6			; Rounds to check
	
.check:	moveq	#PASSWORD_LENGTH-1,d7
	
.loop:	
	move.b	(a0,d7),d0
	cmp.b	(a1,d7),d0
	bne	.next
	dbf	d7,.loop
; 
	cmp.b	#$80,7(a1)
	beq	START_GREETS_SCROLL
	cmp.b	#$81,7(a1)
	beq	START_CHEAT_SCROLL
	cmp.b	#$82,7(a1)
	beq	START_TWATS_SCROLL
	cmp.b	#$83,7(a1)
	beq	START_COMPO_SCROLL
	cmp.b	#$84,7(a1)
	beq	START_TECMO_SCROLL	
	cmp.b	#$85,7(a1)
	beq	START_STATS_SCROLL
	
; Found...
	lea	TEXT_START_GAME(a4),a0
	move.b	5(a1),13(a0)
	move.b	6(a1),14(a0)
	move.b	7(a1),ROUND_CURRENT+1(a4)
	move.w	#-1,ROUND_ADVANCE(a4)
	move.b	#" ",6(a0)	
	move.b	#"R",7(a0)
	move.b	#"O",8(a0)
	move.b	#"U",9(a0)
	move.b	#"N",10(a0)
	move.b	#"D",11(a0)	

	lea	LSTPTR_FRONT_SCREEN(a4),a1	
	moveq	#11,d0				; x pos
	moveq	#21,d1				; y pos
	moveq	#0,d6				; colour
	bsr	DRAW_MENU_TEXT
	
	move.w	#-1,START_HIGHER_ROUND(a4)
	
	bra.s	.exit
	
.next:	addq.w	#8,a1
	dbf	d6,.check
	
.exit:	rts

START_GREETS_SCROLL:
	clr.w	TEXT_INDEX(a4)
	move.l	#GREETS_SCROLLER,TEXT_SCROLL(a4)
	move.w	#-1,ENABLE_SCROLL
	rts
	
START_CHEAT_SCROLL:
	clr.w	TEXT_INDEX(a4)
	move.l	#CHEAT_SCROLLER,TEXT_SCROLL(a4)
	move.w	#-1,ENABLE_SCROLL(a4)
	move.w	#-1,ENABLE_CHEATS(a4)
	rts
	
START_TWATS_SCROLL:
	clr.w	TEXT_INDEX(a4)
	move.l	#TWATS_SCROLLER,TEXT_SCROLL(a4)
	move.w	#-1,ENABLE_SCROLL(a4)
	rts
	
START_COMPO_SCROLL:
	clr.w	TEXT_INDEX(a4)
	move.l	#COMPO_SCROLLER,TEXT_SCROLL(a4)
	move.w	#-1,ENABLE_SCROLL(a4)
	rts
	
START_TECMO_SCROLL:
	clr.w	TEXT_INDEX(a4)
	move.l	#TECMO_SCROLLER,TEXT_SCROLL(a4)
	move.w	#-1,ENABLE_SCROLL(a4)
	rts
	
START_STATS_SCROLL:
	clr.w	TEXT_INDEX(a4)
	move.l	#STATS_SCROLLER,TEXT_SCROLL(a4)
	move.w	#-1,ENABLE_SCROLL(a4)
	rts


;-------------------------------------------
; Draw the high score entry initials
;===========================================
DRAW_PASSENTRY_INITIALS:

	moveq	#13,d0				; xpos 
	moveq	#13,d1
	;moveq	#4,d6
	moveq	#0,d2
	bsr	DRAW_MENU_TEXT
	rts





;a0 = Pointer to scroll text
MENU_SCROLLER:
	movem.l	d0-d7/a0-a3,-(a7)
	move.w	#100,MENU_OPTIONS_WAKE(a4)
	cmp.w	#8,MENU_SCROLL_COUNT(a4)
	bne	.scroll

        move.l  FONT_ASSET(a4),a2
        add.w   #86,a2                  ; Offset to BODY
		
.restart:
	clr.w	MENU_SCROLL_COUNT(a4)
	
	moveq	#0,d6
	move.b	TEXT_PLANE(a4),d6
	lea	LSTPTR_FRONT_SCREEN(a4),a1
	move.l	(a1,d6*4),a1

	add.l	#(40*4)*GREETS_YPOS,d6		; Last 8 lines
	add.w	#35,d6
	add.l	d6,a1
	
; a1 & a4 are pointing to correct entry points

.loop_text:
	move.l	TEXT_SCROLL(a4),a0
	add.w	TEXT_INDEX(a4),a0
	moveq	#0,d0
	move.b	(a0),d0
	tst.b	d0
	bne.s	.bp
	clr.w	TEXT_INDEX(a4)
	bra.s	.loop_text
	
;d0=char
.bp:	cmp.b	#4,d0
	bgt.s	.cont
	move.b	d0,TEXT_PLANE(a4)
	addq.w	#1,TEXT_INDEX(a4)
	bra.s	.restart

.cont:
	addq.w	#1,TEXT_INDEX(a4)
	sub.b	#32,d0
	add.l	d0,a2		; index into character

        move.b  (a2),(a1)
        move.b  FONT_WIDTH*1(a2),PLAYFIELD_SIZE_X*4*1(a1)
        move.b  FONT_WIDTH*2(a2),PLAYFIELD_SIZE_X*4*2(a1)
        move.b  FONT_WIDTH*3(a2),PLAYFIELD_SIZE_X*4*3(a1)
        move.b  FONT_WIDTH*4(a2),PLAYFIELD_SIZE_X*4*4(a1)
        move.b  FONT_WIDTH*5(a2),PLAYFIELD_SIZE_X*4*5(a1)
        move.b  FONT_WIDTH*6(a2),PLAYFIELD_SIZE_X*4*6(a1)
        move.b  FONT_WIDTH*7(a2),PLAYFIELD_SIZE_X*4*7(a1)


.scroll:
	move.l	LSTPTR_FRONT_SCREEN(a4),a2
	add.l	#(40*4)*GREETS_YPOS,a2		

	move.l	a2,a3
	subq.w	#2,a3
	
	WAIT_FOR_BLITTER
	move.w	#$f9f0,BLTCON0(a5)		; A shift 1, A>D Mode
	move.w	#0,BLTCON1(a5)			; Descend mode (Bit 1)
	move.w	#$0,BLTAFWM(a5)			; Only want the first word
	move.w	#$fffe,BLTALWM(a5)		; 
	move.w	#0,BLTAMOD(a5)	
	move.w	#0,BLTDMOD(a5)	
	move.l	a2,BLTAPTH(a5)			;
	move.l	a3,BLTDPTH(a5)
	move.w	#(32*64)+20,BLTSIZE(a5)		; 14

	addq.w	#1,MENU_SCROLL_COUNT(a4)
	
	movem.l	(a7)+,d0-d7/a0-a3
	rts
	
	
DIFFICULTY_BEGINNER:
	movem.l	d0-d7/a0-a3,-(a7)
	lea	TEXT_EXTRAS_BEGINNER(a4),a0
	lea	TEXT_EXTRAS_TABLE(a4),a1
	rept	6
	move.l	(a0)+,(a1)+
	endr

	lea	EXTRAS_BEGINNER(a4),a0
	lea	EXTRAS(a4),a1
	move.l	(a0)+,(a1)+
	move.l	(a0)+,(a1)+
	move.l	(a0)+,(a1)+
	
	lea	TXT_TIMER_BEGINNER(a4),a0
	lea	TXT_TIMER_START(a4),a1
	move.b	(a0)+,(a1)+
	move.b	(a0)+,(a1)+
	move.b	(a0)+,(a1)+
	move.b	(a0)+,(a1)+
	move.b	(a0)+,(a1)+

	move.w	#START_TIME_BEGINNER,TIME_REMAINING(a4)
	move.w	#START_TIME_BEGINNER,START_TIME(a4)
	bsr	UPDATE_EXTRA_AMOUNT
	bsr	UPDATE_TIMER
	movem.l	(a7)+,d0-d7/a0-a3
	rts
	
DIFFICULTY_WARRIOR:
	movem.l	d0-d7/a0-a3,-(a7)
	lea	TEXT_EXTRAS_WARRIOR(a4),a0
	lea	TEXT_EXTRAS_TABLE(a4),a1
	rept	6
	move.l	(a0)+,(a1)+
	endr

	lea	EXTRAS_WARRIOR(a4),a0
	lea	EXTRAS(a4),a1
	move.l	(a0)+,(a1)+
	move.l	(a0)+,(a1)+
	move.l	(a0)+,(a1)+
	
	lea	TXT_TIMER_WARRIOR(a4),a0
	lea	TXT_TIMER_START(a4),a1
	move.b	(a0)+,(a1)+
	move.b	(a0)+,(a1)+
	move.b	(a0)+,(a1)+
	move.b	(a0)+,(a1)+
	move.b	(a0)+,(a1)+

	move.w	#START_TIME_WARRIOR,TIME_REMAINING(a4)
	move.w	#START_TIME_WARRIOR,START_TIME(a4)
	bsr	UPDATE_EXTRA_AMOUNT
	bsr	UPDATE_TIMER
	movem.l	(a7)+,d0-d7/a0-a3
	rts
	
	