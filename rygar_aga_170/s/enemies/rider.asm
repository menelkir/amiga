RIDER_GROUND_ENEMY:             equ     1       ; 1 = Ground enemy means enemy is stunnable.
RIDER_HWSPR_ENABLE:             equ     0
RIDER_HWSPR_UPPER:              equ     0
RIDER_HWSPR_MID:                equ     0
RIDER_HWSPR_LOWER:              equ     0

RIDER_TO_RYGAR_X1_BOUND:        equ     8
RIDER_TO_RYGAR_Y1_BOUND:        equ     0
RIDER_TO_RYGAR_X2_BOUND:        equ     23
RIDER_TO_RYGAR_Y2_BOUND:        equ     31

RIDER_TO_DISKARM_X1_BOUND:      equ     8
RIDER_TO_DISKARM_Y1_BOUND:      equ     0
RIDER_TO_DISKARM_X2_BOUND:      equ     23
RIDER_TO_DISKARM_Y2_BOUND:      equ     31

RIDER_MOVE_SPEED:               equ     28      ; Initial speed.
RIDER_JUMP_SPEED:               equ     20      ; 2 pixels per frame.
RIDER_SINE_SPEED:               equ     3

RIDER_JUMP_LENGTH:              equ     RIDER_JUMP_SPEED/16      ; 50 pi

RIDER_LAND_DELAY:               equ     12
RIDER_LEAP_DELAY:               equ     12
RIDER_SPEED_INCREASE:           equ     30

RIDER_HAMMER_INTERVAL_TIMER:	equ	25

RIDER_HANGING:                  equ     6
RIDER_DESCEND_LEFT:             equ     7
RIDER_DESCEND_RIGHT:            equ     8
RIDER_LAND_LEFT:                equ     9
RIDER_LAND_RIGHT:               equ     10
RIDER_RUN_LEFT:                 equ     11
RIDER_RUN_RIGHT:                equ     12
RIDER_JUMP_LEFT:                equ     13
RIDER_JUMP_RIGHT:               equ     14
RIDER_LEAP_LEFT:                equ     15
RIDER_LEAP_RIGHT:               equ     16
RIDER_ON_BACK:			equ	17
RIDER_HAMMER_THROW:		equ	18

PRIVATE_RIDER_MOVE_SPEED:       equ     32
PRIVATE_RIDER_TIMER:            equ     34
PRIVATE_RIDER_IS_DESCENDING:    equ     36
PRIVATE_RIDER_SINE_POINTER:     equ     38
PRIVATE_RIDER_FIXED_YPOS:       equ     40
PRIVATE_RIDER_INCREMENT_SPEED:  equ     42
PRIVATE_RIDER_CURRENT_PLATFORM: equ     44
PRIVATE_RIDER_RUN_SPEED:	equ	46

HDL_RIDER:
		FUNCID	#$652f4784
                SPRITE_INITIATE

; Set the initial variables and status of the sprite.
.construct:
                FIND_CAMERA_XPOS
		
		move.w  #RIDER_MOVE_SPEED,PRIVATE_RIDER_MOVE_SPEED(a2)  ; Set move speed.
                move.w  #RIDER_HANGING,THIS_SPRITE_STATUS(a2)
		
		move.w	THIS_SPRITE_PARAMETER(a2),d2
		and.w	#$1,d2
		beq.s	.active
		move.w	#RIDER_ON_BACK,THIS_SPRITE_STATUS(a2)
		move.w	#RIDER_HAMMER_INTERVAL_TIMER,PRIVATE_RIDER_TIMER(a2)
		
		move.w	ROUND_CURRENT(a4),d2
		add.w	d2,d2
		move.w	d2,PRIVATE_RIDER_RUN_SPEED(a2)
		
;;; Init code here.

.active:	moveq	#ZERO_POINTS,d2

		ENEMY_CHECK_END_OF_ROUND

.sprite_0:	;ENEMY_BOUNDARY_CHECK

                moveq   #0,d2
                move.w  THIS_SPRITE_MAP_XPOS(a2),d2		; is 0
                lea     BOUNDARY_X1POS(a4),a0
		cmp.l	(a0),d2
		blt.s	.dectimer
		cmp.l	4(a0),d2
		bgt.s	.dectimer
		bra.s	.inbound
			
.dectimer:
                subq.w  #1,THIS_SPRITE_BOUNDARY_TIMER(a2)
                bpl.s   .outbound
                move.w  #ENEMY_STATUS_BONES,THIS_SPRITE_STATUS(a2)
.inbound:       move.w  #ENEMY_BOUNDARY_TIME,THIS_SPRITE_BOUNDARY_TIMER(a2)
.outbound:   
		bra.s   .sprite_1

.animate_table: dc.l    .method_animate_none-.animate_table      	; 0
                dc.l    .method_animate_bones-.animate_table            ; 1
                dc.l    .method_animate_sweep_left-.animate_table       ; 2
                dc.l    .method_animate_sweep_right-.animate_table      ; 3
                dc.l    .method_animate_stun_left-.animate_table        ; 2
                dc.l    .method_animate_stun_right-.animate_table       ; 3
                dc.l    .method_animate_hanging-.animate_table          ; 6
                dc.l    .method_animate_descend_left-.animate_table     ; 7
                dc.l    .method_animate_descend_right-.animate_table    ; 8
                dc.l    .method_animate_land_left-.animate_table        ; 9
                dc.l    .method_animate_land_right-.animate_table       ; 10
                dc.l    .method_animate_run_left-.animate_table         ; 11
                dc.l    .method_animate_run_right-.animate_table        ; 12
                dc.l    .method_animate_jump_left-.animate_table        ; 13
                dc.l    .method_animate_jump_right-.animate_table       ; 14
                dc.l    .method_animate_leap_left-.animate_table        ; 15
                dc.l    .method_animate_leap_right-.animate_table       ; 16
                dc.l    .method_animate_on_back-.animate_table      	; 17
                dc.l    .method_animate_hammer_throw-.animate_table     ; 18
                dc.l    .method_animate_none-.animate_table      	; 19
                dc.l    .method_animate_none-.animate_table      	; 20
                dc.l    .method_animate_none-.animate_table      	; 21
                dc.l    .method_animate_none-.animate_table      	; 22
                dc.l    .method_animate_none-.animate_table      	; 23
                dc.l    .method_animate_none-.animate_table      	; 24
		
.method_animate_none:	bra	.exit

.sprite_1:      cmp.w   #ENEMY_STATUS_SWEEP_RIGHT,THIS_SPRITE_STATUS(a2)                ; if doing bones or sweeps then skip all collisions
                ble     .run_sprite_method
		tst.w	SHIELD_TIMER
		bpl.s	.shield_on
                cmp.w   #ENEMY_STATUS_STUN_RIGHT,THIS_SPRITE_STATUS(a2)                 ; if doing stuns then skip Rygar collision
                ble     .sprite_stunned
.shield_on:

;Test this enemy collided with Rygar (the player)
.sprite_2:      move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4                                   ; Check a 32x32 boundary from x1/y1
                move.w  d3,d5
                add.w   #RIDER_TO_RYGAR_X1_BOUND,d2
                add.w   #RIDER_TO_RYGAR_Y1_BOUND,d3
                add.w   #RIDER_TO_RYGAR_X2_BOUND,d4
                add.w   #RIDER_TO_RYGAR_Y2_BOUND,d5
                lea     RYGAR_COORDS(a4),a3
                ENEMY_TO_RYGAR_COLLISION
                move.l  #POINTS_RIDER,d2
                tst.w   d6                                      ; Did this sprite collide with Rygar?
                IFEQ    DISABLE_ENEMY_COLLISION
                bmi     .method_set_collision_action            ; Yes
                ENDC

; Check if the enemy has been hit with disk armor
.sprite_stunned:
                move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4                                   ; Check a 32x32 boundary from x1/y1
                move.w  d3,d5
                add.w   #RIDER_TO_DISKARM_X1_BOUND,d2
                add.w   #RIDER_TO_DISKARM_Y1_BOUND,d3
                add.w   #RIDER_TO_DISKARM_X2_BOUND,d4
                add.w   #RIDER_TO_DISKARM_Y2_BOUND,d5
                lea     DISKARM_COORDS(a4),a3
                ENEMY_TO_DISKARM_COLLISION
                move.l  #POINTS_RIDER,d2
                tst.w   d6                                      ; Did this sprite get hit with the disk armor
                bmi     .method_set_sweep_direction             ; Yes

; Test this enemy collided with the disk armour

                RUN_SPRITE_METHOD

                METHOD_ANIMATE_BONES

                METHOD_ANIMATE_SWEEP_LEFT

                METHOD_ANIMATE_SWEEP_RIGHT

                METHOD_SET_BONES

                METHOD_SET_SWEEP_DIRECTION

                METHOD_SET_STUN_OR_DESTROY

                METHOD_SET_STUN_DIRECTION

                METHOD_SET_RYGAR_DEATH_SEQUENCE


.method_set_collision_action:
                tst.w   RYGAR_FALLPREV_STATE(a4)                                ; Is Rygar falling on enemy
                bmi     .method_set_stun_or_destroy                     ; Yes!

                IFNE    DISABLE_ENEMY_COLLISION
                bra     .exit
                ELSE
                bra     .method_set_rygar_death_sequence
                ENDC


.method_destroy_sprite:
                tst.w   RIDERS_IN_USE(a4)
                beq.s   .method_destroy_sprite_0
                subq.w  #1,RIDERS_IN_USE(a4)
		
		lea	ATTACH_TABLE(a4),a3
		move.b	#-1,(a3,d0)

.method_destroy_sprite_0:
                clr.w   THIS_SPRITE_STATUS(a2)
                IFNE    RIDER_HWSPR_ENABLE
                IFNE    RIDER_HWSPR_UPPER
                lea     ENEMY_HWSPR_UPPER_ALLOCATED(a4),a0                  ; Free up hardware sprite
                ENDC
                IFNE    RIDER_HWSPR_MID
                lea     ENEMY_HWSPR_MID_ALLOCATED(a4),a0                    ; Free up hardware sprite
                ENDC
                IFNE    RIDER_HWSPR_LOWER
                lea     ENEMY_HWSPR_LOWER_ALLOCATED(a4),a0                  ; Free up hardware sprite
                ENDC
                move.w  THIS_SPRITE_HWSLOT(a2),d7
                bmi     DESTROY_SPRITE
                FREE_HARDWARE_SPRITE
                ENDC
                bra     DESTROY_SPRITE

.method_animate_stun_left:
                moveq   #ENEMY_STATUS_STUN_LEFT,d1
                move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                subq.w  #1,THIS_SPRITE_STUN_TIMER(a2)
                bmi     .method_set_run_left
                ENEMY_SHAKE
                bra     ANIMATE

.method_animate_stun_right:
                moveq   #ENEMY_STATUS_STUN_RIGHT,d1
                move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                subq.w  #1,THIS_SPRITE_STUN_TIMER(a2)
                bmi     .method_set_run_right
                ENEMY_SHAKE
                bra     ANIMATE

.method_set_hanging:
                move.w  #RIDER_HANGING,THIS_SPRITE_STATUS(a2)
                bra     .exit

.method_animate_descend:
.method_animate_hanging:
                moveq   #0,d2				; Find out what Dragon Sprite we're hanging from.
		lea	ATTACH_TABLE(a4),a0
		move.b	(a0,d0),d2
		tst.b	(a0,d2)
		bpl.s	.method_animate_hanging_0			; Check still attached.
		
; If the Dragon no longer exists then Rider must descend (Rygar probably killed the Dragon).

; This if the Dragon sprite was destroyed.
                cmp.w   #SPRITE_MOVE_RIGHT,THIS_SPRITE_DIRECTION(a0)
                beq     .method_set_descend_right
                bra     .method_set_descend_left

; d2 = Dragon Sprite
.method_animate_hanging_0:
                move.l  MEMCHK0_SPRITE_HDLVARS(a4),a0
                lea     TAB_64(a4),a3
                add.w   (a3,d2*2),a0
                cmp.w   #SPRITE_MOVE_RIGHT,THIS_SPRITE_DIRECTION(a0)
                beq.s   .method_animate_hanging_right
                bra.s   .method_animate_hanging_left

                nop

.method_animate_hanging_left:                           ; It is moving left
                lea     SPRITE_SLOTS(a4),a0		; Get the Dragon Context
                move.l  (a0,d2*4),a0
                add.w   (a0),a0

                move.w  SPRITE_PLOT_XPOS(a0),d3
                add.w   #14,d3
                move.w  d3,SPRITE_PLOT_XPOS(a1)
		
		move.w	#0+100,d4
		move.w	ROUND_CURRENT(a4),d5
		moveq	#13,d5
		lsl.w	#2,d5
		add.w	d5,d4
		
                cmp.w   d4,d3
                ble     .method_set_descend_left
                move.w  SPRITE_PLOT_YPOS(a0),d3
                add.w   #30,d3
                move.w  d3,SPRITE_PLOT_YPOS(a1)
                
		move.w  SPRITE_PLOT_XPOS(a1),d3                 ; Get x position
                add.w   MAP_PIXEL_POSX(a4),d3
                sub.w   #$100,d3
                lsl.w   #4,d3
                move.w  d3,THIS_SPRITE_MAP_XPOS(a2)
		
                moveq   #RIDER_HANGING,d1
                bra     ANIMATE


.method_animate_hanging_right:                          ; It is moving right
                lea     SPRITE_SLOTS(a4),a0
                move.l  (a0,d2*4),a0
                add.w   (a0),a0

                move.w  SPRITE_PLOT_XPOS(a0),d3
                add.w   #14,d3
                move.w  d3,SPRITE_PLOT_XPOS(a1)
		
		move.w	#200,d4
		move.w	ROUND_CURRENT(a4),d5
		moveq	#13,d5
		lsl.w	#2,d5
		sub.w	d5,d4
		
                cmp.w   d4,d3
                bge     .method_set_descend_right
                move.w  SPRITE_PLOT_YPOS(a0),d3
                add.w   #30,d3
                move.w  d3,SPRITE_PLOT_YPOS(a1)
		
		move.w  SPRITE_PLOT_XPOS(a1),d3                 ; Get x position
                add.w   MAP_PIXEL_POSX(a4),d3
                sub.w   #$100,d3
                lsl.w   #4,d3
                move.w  d3,THIS_SPRITE_MAP_XPOS(a2)
		
                moveq   #RIDER_HANGING,d1
                bra     ANIMATE

;------------------------------------------------------------------------------------
; DESCEND LEFT
;------------------------------------------------------------------------------------
.method_set_descend_left:
		lea	ATTACH_TABLE(a4),a3
		move.b	#-1,(a3,d0)
		
                move.w  SPRITE_PLOT_YPOS(a1),PRIVATE_RIDER_FIXED_YPOS(a2)
                move.w  #1,PRIVATE_RIDER_IS_DESCENDING(a2)
                move.w  SPRITE_PLOT_XPOS(a1),d3                 ; Get x position
                add.w   MAP_PIXEL_POSX(a4),d3
                sub.w   #$100,d3
                lsl.w   #4,d3
                move.w  d3,THIS_SPRITE_MAP_XPOS(a2)
                move.w  #RIDER_DESCEND_LEFT,THIS_SPRITE_STATUS(a2)
                clr.w   PRIVATE_RIDER_SINE_POINTER(a2)

		move.l	d0,-(a7)	
		moveq	#SND_RIDERFALL,d0
		bsr	PLAY_SAMPLE
		move.l	(a7)+,d0
                bra     .exit

.method_animate_descend_left:
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                subq.w  #8,d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS
; Update Y Position

                lea     ENEMY_GRAVITY_SINE(a4),a0
                move.w  PRIVATE_RIDER_SINE_POINTER(a2),d3
                addq.w  #2,PRIVATE_RIDER_SINE_POINTER(a2)
                move.w  PRIVATE_RIDER_FIXED_YPOS(a2),d4
                add.w   (a0,d3*2),d4

                cmp.w   #SCREEN_BOTTOM,d4
                bge     .method_destroy_sprite

                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  SPRITE_PLOT_YPOS(a1),d7
                lsr.w   #4,d7
                moveq   #0,d5
                IS_PATH_DIFFERENT                       ;d6=xpos interpolated, d7=current platform
                tst.b   d6
                bmi     .method_set_land_left

                move.w  d4,SPRITE_PLOT_YPOS(a1)
                moveq   #RIDER_DESCEND_LEFT,d1
                bra     ANIMATE


;------------------------------------------------------------------------------------
; DESCEND RIGHT
;------------------------------------------------------------------------------------
.method_set_descend_right:
		lea	ATTACH_TABLE(a4),a3
		move.b	#-1,(a3,d0)
		
                move.w  SPRITE_PLOT_YPOS(a1),PRIVATE_RIDER_FIXED_YPOS(a2)
                clr.w   PRIVATE_RIDER_SINE_POINTER(a4)
                move.w  #1,PRIVATE_RIDER_IS_DESCENDING(a2)
                move.w  SPRITE_PLOT_XPOS(a1),d3                 ; Get x position
                add.w   MAP_PIXEL_POSX(a4),d3
                sub.w   #$100,d3
                lsl.w   #4,d3
                move.w  d3,THIS_SPRITE_MAP_XPOS(a2)
                clr.w   PRIVATE_RIDER_SINE_POINTER(a2)
                move.w  #RIDER_DESCEND_RIGHT,THIS_SPRITE_STATUS(a2)

		move.l	d0,-(a7)	
		moveq	#SND_RIDERFALL,d0
		bsr	PLAY_SAMPLE
		move.l	(a7)+,d0
                bra     .exit

.method_animate_descend_right:
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                addq.w  #8,d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS
; Update Y Position
                lea     ENEMY_GRAVITY_SINE(a4),a0
                move.w  PRIVATE_RIDER_SINE_POINTER(a2),d3
                addq.w  #2,PRIVATE_RIDER_SINE_POINTER(a2)
                move.w  PRIVATE_RIDER_FIXED_YPOS(a2),d4
                add.w   (a0,d3*2),d4

                cmp.w   #SCREEN_BOTTOM,d4
                bge     .method_destroy_sprite

                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  SPRITE_PLOT_YPOS(a1),d7
                lsr.w   #4,d7
                moveq   #0,d5
                IS_PATH_DIFFERENT                       ;d6=xpos interpolated, d7=current platform
                tst.b   d6
                bmi     .method_set_land_right

                move.w  d4,SPRITE_PLOT_YPOS(a1)
                moveq   #RIDER_DESCEND_RIGHT,d1
                bra     ANIMATE

;------------------------------------------------------------------------------------
; LAND_LEFT
;------------------------------------------------------------------------------------
.method_set_land_left:
                move.w  d4,SPRITE_PLOT_YPOS(a1)
                move.w  #RIDER_LAND_DELAY,PRIVATE_RIDER_TIMER(a2)
                move.w  #RIDER_LAND_LEFT,THIS_SPRITE_STATUS(a2)
                move.w  #RIDER_MOVE_SPEED,PRIVATE_RIDER_MOVE_SPEED(a2)  ; Set move speed.
                move.w  SPRITE_PLOT_YPOS(a1),d2
                and.w   #$fff0,d2
                addq.w  #2,d2
                move.w  d2,SPRITE_PLOT_YPOS(a1)
                bra     .exit

.method_animate_land_left:
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.l  #ENEMY_DEF_RIDER,d7
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS

                subq.w  #1,PRIVATE_RIDER_TIMER(a2)
                tst.w   PRIVATE_RIDER_TIMER(a2)
                bmi     .method_animate_land_left_0
                moveq   #RIDER_LAND_LEFT,d1
                bra     ANIMATE

.method_animate_land_left_0:
                tst.w   PRIVATE_RIDER_IS_DESCENDING(a2)
                beq     .method_set_run_left
                bra     .method_set_run_direction

;------------------------------------------------------------------------------------
; LAND RIGHT
;------------------------------------------------------------------------------------
.method_set_land_right:
                move.w  d4,SPRITE_PLOT_YPOS(a1)
                move.w  #RIDER_LAND_DELAY,PRIVATE_RIDER_TIMER(a2)
                move.w  #RIDER_LAND_RIGHT,THIS_SPRITE_STATUS(a2)
                move.w  #RIDER_MOVE_SPEED,PRIVATE_RIDER_MOVE_SPEED(a2)  ; Set move speed.
                move.w  SPRITE_PLOT_YPOS(a1),d2
                and.w   #$fff0,d2
                addq.w  #2,d2
                move.w  d2,SPRITE_PLOT_YPOS(a1)
                bra     .exit

.method_animate_land_right:
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.l  #ENEMY_DEF_RIDER,d7
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS

                subq.w  #1,PRIVATE_RIDER_TIMER(a2)
                tst.w   PRIVATE_RIDER_TIMER(a2)
                bmi     .method_animate_land_right_0
                moveq   #RIDER_LAND_RIGHT,d1
                bra     ANIMATE

.method_animate_land_right_0:
                tst.w   PRIVATE_RIDER_IS_DESCENDING(a2)
                beq     .method_set_run_right
                bra     .method_set_run_direction

                nop

; This method causes the Rider to run in the direction of Rygar.
.method_set_run_direction:
                move.w  RYGAR_XPOS(a4),d3
                cmp.w   SPRITE_PLOT_XPOS(a1),d3
                ble.s   .method_set_run_left
                bra     .method_set_run_right

;------------------------------------------------------------------------------------
; RUN LEFT
;------------------------------------------------------------------------------------
.method_set_run_left:
                move.w  #RIDER_SPEED_INCREASE,PRIVATE_RIDER_INCREMENT_SPEED(a2)
                move.w  #RIDER_RUN_LEFT,THIS_SPRITE_STATUS(a2)
                bra     .exit

.method_animate_run_left:
                subq.w  #1,PRIVATE_RIDER_INCREMENT_SPEED(a2)
                tst.w   PRIVATE_RIDER_INCREMENT_SPEED(a2)
                bpl.s   .method_animate_run_left_0
                move.w  #RIDER_SPEED_INCREASE,PRIVATE_RIDER_INCREMENT_SPEED(a2)
                addq.w  #1,PRIVATE_RIDER_MOVE_SPEED(a2)

.method_animate_run_left_0:
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                sub.w   PRIVATE_RIDER_MOVE_SPEED(a2),d6
		sub.w	PRIVATE_RIDER_RUN_SPEED(a2),d6
		move.w	d6,d2

                move.w  SPRITE_PLOT_YPOS(a1),d7
                lsr.w   #4,d7
                move.w  d7,d3
                subq.w  #2,d7
; Check an upper obstacle
                moveq   #1,d5
                IS_PATH_DIFFERENT                       ;d6=xpos interpolated, d7=current platform
                tst.b   d6
                bmi     .method_set_leap_left
; Check platform
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                sub.w   PRIVATE_RIDER_MOVE_SPEED(a2),d6
		sub.w	PRIVATE_RIDER_RUN_SPEED(a2),d6

                move.w  d3,d7
                moveq   #0,d5
                IS_PATH_DIFFERENT                       ;d6=xpos interpolated, d7=current platform
                tst.b   d6
                bpl     .method_set_leap_left

                UPDATE_SPRITE_PLOT_XPOS
                moveq   #RIDER_RUN_LEFT,d1
                bra     ANIMATE




;------------------------------------------------------------------------------------
; RUN RIGHT
;------------------------------------------------------------------------------------
.method_set_run_right:
                move.w  #RIDER_SPEED_INCREASE,PRIVATE_RIDER_INCREMENT_SPEED(a2)
                move.w  #RIDER_RUN_RIGHT,THIS_SPRITE_STATUS(a2)
                bra     .exit

.method_animate_run_right:
                subq.w  #1,PRIVATE_RIDER_INCREMENT_SPEED(a2)
                tst.w   PRIVATE_RIDER_INCREMENT_SPEED(a2)
                bpl.s   .method_animate_run_right_0
                move.w  #RIDER_SPEED_INCREASE,PRIVATE_RIDER_INCREMENT_SPEED(a2)
                addq.w  #1,PRIVATE_RIDER_MOVE_SPEED(a2)

.method_animate_run_right_0:
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                add.w   PRIVATE_RIDER_MOVE_SPEED(a2),d6
		add.w	PRIVATE_RIDER_RUN_SPEED(a2),d6
		move.w	d6,d2

                move.w  SPRITE_PLOT_YPOS(a1),d7
                lsr.w   #4,d7
                move.w  d7,d3
                subq.w  #2,d7
; Check an upper obstacle
                moveq   #-1,d5
                IS_PATH_DIFFERENT                       ;d6=xpos interpolated, d7=current platform
                tst.b   d6
                bmi     .method_set_leap_right

; Check platform
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                add.w   PRIVATE_RIDER_MOVE_SPEED(a2),d6
		add.w	PRIVATE_RIDER_RUN_SPEED(a2),d6
		
                move.w  d3,d7
                moveq   #0,d5
                IS_PATH_DIFFERENT                       ;d6=xpos interpolated, d7=current platform
                tst.b   d6
                bpl     .method_set_leap_right

                UPDATE_SPRITE_PLOT_XPOS
                moveq   #RIDER_RUN_RIGHT,d1
                bra     ANIMATE

;------------------------------------------------------------------------------------
; LEAP LEFT
;------------------------------------------------------------------------------------
.method_set_leap_left:
                move.w  #RIDER_LEAP_DELAY,PRIVATE_RIDER_TIMER(a2)
                move.w  #RIDER_LEAP_LEFT,THIS_SPRITE_STATUS(a2)
                bra     .exit

.method_animate_leap_left:
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.l  #ENEMY_DEF_RIDER,d7
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS

                subq.w  #1,PRIVATE_RIDER_TIMER(a2)
                bmi     .method_set_jump_left
                moveq   #RIDER_LEAP_LEFT,d1
                bra     ANIMATE

;------------------------------------------------------------------------------------
; LEAP RIGHT
;------------------------------------------------------------------------------------
.method_set_leap_right:
                move.w  #RIDER_LEAP_DELAY,PRIVATE_RIDER_TIMER(a2)
                move.w  #RIDER_LEAP_RIGHT,THIS_SPRITE_STATUS(a2)
                bra     .exit

.method_animate_leap_right:
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.l  #ENEMY_DEF_RIDER,d7
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS

                subq.w  #1,PRIVATE_RIDER_TIMER(a2)
                bmi     .method_set_jump_right
                moveq   #RIDER_LEAP_RIGHT,d1
                bra     ANIMATE

;------------------------------------------------------------------------------------
; JUMP LEFT
;------------------------------------------------------------------------------------
.method_set_jump_left:
                clr.w   PRIVATE_RIDER_IS_DESCENDING(a2)
                move.w  #RIDER_JUMP_LEFT,THIS_SPRITE_STATUS(a2)
                clr.w   PRIVATE_RIDER_SINE_POINTER(a2)
                move.w  SPRITE_PLOT_YPOS(a1),PRIVATE_RIDER_FIXED_YPOS(a2)
                bra     .exit

.method_animate_jump_left:
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                sub.w   #RIDER_JUMP_SPEED,d6
                move.l  #ENEMY_DEF_RIDER,d7
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS

                lea     RIDER_JUMP_SINE(a4),a0
                move.w  PRIVATE_RIDER_SINE_POINTER(a2),d2
                addq.w  #RIDER_SINE_SPEED,PRIVATE_RIDER_SINE_POINTER(a2)
                move.w  PRIVATE_RIDER_FIXED_YPOS(a2),d4
                add.w   (a0,d2*2),d4                            ; Add sine(y)

                cmp.w   #64,d2                                  ; are we on descend curve
                ble.s   .method_animate_jump_left_1            ; No, don't check platforms...
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  SPRITE_PLOT_YPOS(a1),d7
                lsr.w   #4,d7

                moveq   #1,d5
                IS_PATH_DIFFERENT                       ;d6=xpos interpolated, d7=current platform
                tst.b   d6
                bmi     .method_set_land_left

.method_animate_jump_left_1:
                move.w  d4,SPRITE_PLOT_YPOS(a1)
                moveq   #RIDER_JUMP_LEFT,d1
                bra     ANIMATE


;------------------------------------------------------------------------------------
; JUMP RIGHT
;------------------------------------------------------------------------------------
.method_set_jump_right:
                clr.w   PRIVATE_RIDER_IS_DESCENDING(a2)
                move.w  #RIDER_JUMP_RIGHT,THIS_SPRITE_STATUS(a2)
                clr.w   PRIVATE_RIDER_SINE_POINTER(a2)
                move.w  SPRITE_PLOT_YPOS(a1),PRIVATE_RIDER_FIXED_YPOS(a2)
                bra     .exit

.method_animate_jump_right:
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                add.w   #RIDER_JUMP_SPEED,d6
                move.l  #ENEMY_DEF_RIDER,d7
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS

                lea     RIDER_JUMP_SINE(a4),a0
                move.w  PRIVATE_RIDER_SINE_POINTER(a2),d2
                addq.w  #RIDER_SINE_SPEED,PRIVATE_RIDER_SINE_POINTER(a2)
                move.w  PRIVATE_RIDER_FIXED_YPOS(a2),d4
                add.w   (a0,d2*2),d4                            ; Add sine(y)
                cmp.w   #64,d2                                  ; are we on descend curve
                ble.s   .method_animate_jump_right_1            ; No, don't check platforms...

                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  SPRITE_PLOT_YPOS(a1),d7         ; Y Position
                lsr.w   #4,d7                           ; Divide by 16
                moveq   #-1,d5
                IS_PATH_DIFFERENT                       ;d6=xpos interpolated, d7=current platform
                tst.b   d6
                bmi     .method_set_land_right

.method_animate_jump_right_1:
                move.w  d4,SPRITE_PLOT_YPOS(a1)
                ;bsr    DEBUG_TEXT
                moveq   #RIDER_JUMP_RIGHT,d1
                bra     ANIMATE

.method_set_on_back:
		move.w	#RIDER_ON_BACK,THIS_SPRITE_STATUS(a2)
		move.w	#RIDER_HAMMER_INTERVAL_TIMER,PRIVATE_RIDER_TIMER(a2)
		bra	.exit

.method_animate_on_back:
		subq.w	#1,PRIVATE_RIDER_TIMER(a2)
		tst.w	PRIVATE_RIDER_TIMER(a2)
		bmi.s	.method_set_hammer_throw

		moveq   #0,d2				; Find out what Dragon Sprite we're hanging from.
		lea	ATTACH_TABLE(a4),a0
		move.b	(a0,d0),d2
		
		lea     SPRITE_SLOTS(a4),a0		;
                move.l  (a0,d2*4),a0
                add.w   (a0),a0
		
		move.w  SPRITE_PLOT_XPOS(a0),SPRITE_PLOT_XPOS(a1)
                add.w   #14,SPRITE_PLOT_XPOS(a1)
               
                move.w  SPRITE_PLOT_YPOS(a0),SPRITE_PLOT_YPOS(a1)
                sub.w   #23,SPRITE_PLOT_YPOS(a1)
	
		move.w  SPRITE_PLOT_XPOS(a1),d3                 
                add.w   MAP_PIXEL_POSX(a4),d3
                sub.w   #$100,d3
                lsl.w   #4,d3
                move.w  d3,THIS_SPRITE_MAP_XPOS(a2)
		
		moveq	#RIDER_ON_BACK,d1
		bra	ANIMATE


.method_set_hammer_throw:
		move.w	#RIDER_HAMMER_THROW,THIS_SPRITE_STATUS(a2)
		
		movem.l d0-d7/a0-a3,-(a7)
                moveq   #SPR_HAMMER,d0
                move.w  THIS_SPRITE_MAP_XPOS(a2),d1
                lsr.w   #4,d1
                move.w  #192,d2
                moveq   #SPR_TYPE_ENEMY_1X16,d3
                lea     HDL_HAMMER(pc),a0
                bsr     PUSH_SPRITE
                movem.l (a7)+,d0-d7/a0-a3
		
		bra	.exit

.method_animate_hammer_throw:
		moveq   #0,d2				; Find out what Dragon Sprite we're hanging from.
		lea	ATTACH_TABLE(a4),a0
		move.b	(a0,d0),d2
		
		tst.b	(a0,d2)
		bpl.s	.method_animate_hammer_throw_0			; Check still attached.
		
		cmp.w   #SPRITE_MOVE_RIGHT,THIS_SPRITE_DIRECTION(a0)
                beq     .method_set_descend_right
                bra     .method_set_descend_left
		
.method_animate_hammer_throw_0:
		lea     SPRITE_SLOTS(a4),a0		;
                move.l  (a0,d2*4),a0
                add.w   (a0),a0
		
		move.w  SPRITE_PLOT_XPOS(a0),SPRITE_PLOT_XPOS(a1)
                add.w   #14,SPRITE_PLOT_XPOS(a1)
               
                move.w  SPRITE_PLOT_YPOS(a0),SPRITE_PLOT_YPOS(a1)
                sub.w   #23,SPRITE_PLOT_YPOS(a1)
	
		move.w  SPRITE_PLOT_XPOS(a1),d3                 
                add.w   MAP_PIXEL_POSX(a4),d3
                sub.w   #$100,d3
                lsl.w   #4,d3
                move.w  d3,THIS_SPRITE_MAP_XPOS(a2)
		
		moveq	#RIDER_HAMMER_THROW,d1
		movem.l	a1-a2,-(a7)
		bsr	ANIMATE
		movem.l	(a7)+,a1-a2
		tst.w	d6
		bmi	.method_set_on_back
		bra	.exit






.set_update:

.exit:          rts

RIDER1