
SQUIRREL_STATUS_CLEAR:          equ     6
SQUIRREL_STATUS_WAIT:           equ     7
SQUIRREL_STATUS_SWOOP:          equ     8

SQUIRREL_SWOOP_POINT:           equ     24      ; 24 pixels in before squirrel attackes Rygar,

PRIVATE_SQUIRREL_SPEED:         equ     32
PRIVATE_SQUIRREL_FIXED_YPOS:    equ     34
PRIVATE_SQUIRREL_ACTIVATED:     equ     36
PRIVATE_SQUIRREL_TRIGGER_POSX:  equ     38

SQUIRREL_TO_RYGAR_X1_BOUND:     equ     0
SQUIRREL_TO_RYGAR_Y1_BOUND:     equ     0
SQUIRREL_TO_RYGAR_X2_BOUND:     equ     32
SQUIRREL_TO_RYGAR_Y2_BOUND:     equ     32

SQUIRREL_TO_DISKARM_X1_BOUND:   equ     0
SQUIRREL_TO_DISKARM_Y1_BOUND:   equ     0
SQUIRREL_TO_DISKARM_X2_BOUND:   equ     32
SQUIRREL_TO_DISKARM_Y2_BOUND:   equ     32


HDL_SQUIRREL:
		FUNCID	#$c579f5e3
                SPRITE_INITIATE

; Set the initial variables and status of the sprite.
.construct:
                move.w  SPRITE_PLOT_XPOS(a1),d2
                lsl.w   #4,d2
                move.w  d2,THIS_SPRITE_MAP_XPOS(a2)
                move.w  #SQUIRREL_STATUS_WAIT,THIS_SPRITE_STATUS(a2)
                move.w  SPRITE_PLOT_YPOS(a1),PRIVATE_SQUIRREL_FIXED_YPOS(a2)
                move.w  SPRITE_PLOT_XPOS(a1),PRIVATE_SQUIRREL_TRIGGER_POSX(a2)
                add.w   #SQUIRREL_SWOOP_POINT,PRIVATE_SQUIRREL_TRIGGER_POSX(a2)
                clr.w   THIS_SPRITE_SWEEP_INDEX(a2)
                clr.w   PRIVATE_SQUIRREL_ACTIVATED(a2)

.active:        ENEMY_CHECK_END_OF_ROUND

.sprite_0:      cmp.w   #ENEMY_STATUS_SWEEP_RIGHT,THIS_SPRITE_STATUS(a2)
                ble     .run_sprite_method

                tst.w   PRIVATE_SQUIRREL_ACTIVATED(a2)                  ; Is sprite already
                bmi   	.sprite_2                                       ; Active?
                move.w  PRIVATE_SQUIRREL_TRIGGER_POSX(a2),d2
                cmp.w   MAP_PIXEL_POSX(a4),d2           ; Reached Trigger point?
                bne     .sprite_2                                       ; Nope... then wait
                move.w  #SQUIRREL_STATUS_SWOOP,THIS_SPRITE_STATUS(a2)   ; Yes....
                move.w  #-1,PRIVATE_SQUIRREL_ACTIVATED(a2)
		move.l	d0,-(a7)
		moveq	#SND_SQUIRREL,d0
		bsr	PLAY_SAMPLE
		move.l	(a7)+,d0
                bra.s   .sprite_2

.animate_table:
                dc.l    .method_destroy_sprite-.animate_table           ; 0
                dc.l    .method_animate_bones-.animate_table            ; 1
                dc.l    .method_animate_sweep_left-.animate_table       ; 2
                dc.l    .method_animate_sweep_right-.animate_table      ; 3
		dc.l    .method_animate_none-.animate_table      	; 4
		dc.l    .method_animate_none-.animate_table      	; 5
                dc.l    .method_animate_clear-.animate_table            ; 6
                dc.l    .method_animate_waiting-.animate_table          ; 7
                dc.l    .method_animate_swooping-.animate_table         ; 8
                dc.l    .method_animate_none-.animate_table      	; 9
                dc.l    .method_animate_none-.animate_table      	; 10
                dc.l    .method_animate_none-.animate_table      	; 11
                dc.l    .method_animate_none-.animate_table      	; 12
                dc.l    .method_animate_none-.animate_table      	; 13
                dc.l    .method_animate_none-.animate_table      	; 14
                dc.l    .method_animate_none-.animate_table      	; 15
                dc.l    .method_animate_none-.animate_table      	; 16
                dc.l    .method_animate_none-.animate_table      	; 17
                dc.l    .method_animate_none-.animate_table      	; 18
                dc.l    .method_animate_none-.animate_table      	; 19
                dc.l    .method_animate_none-.animate_table      	; 15
                dc.l    .method_animate_none-.animate_table      	; 16
                dc.l    .method_animate_none-.animate_table      	; 17
                dc.l    .method_animate_none-.animate_table      	; 18
                dc.l    .method_animate_none-.animate_table      	; 19

.method_animate_none:	bra	.exit


;Test this enemy collided with Rygar (the player)
.sprite_2:      move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4
                move.w  d3,d5
                add.w   #SQUIRREL_TO_RYGAR_X1_BOUND,d2
                add.w   #SQUIRREL_TO_RYGAR_Y1_BOUND,d3
                add.w   #SQUIRREL_TO_RYGAR_X2_BOUND,d4
                add.w   #SQUIRREL_TO_RYGAR_Y2_BOUND,d5
                lea     RYGAR_COORDS(a4),a3
                ENEMY_TO_RYGAR_COLLISION
                move.l  #POINTS_SQUIRREL,d2
                tst.w   d6                                      ; Did this sprite collide with Rygar?
                IFEQ    DISABLE_ENEMY_COLLISION
                bmi     .method_set_collision_action            ; Yes
                ENDC

; Check if the enemy has been hit with disk armor
.sprite_stunned:
                move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4
                move.w  d3,d5
                add.w   #SQUIRREL_TO_DISKARM_X1_BOUND,d2
                add.w   #SQUIRREL_TO_DISKARM_Y1_BOUND,d3
                add.w   #SQUIRREL_TO_DISKARM_X2_BOUND,d4
                add.w   #SQUIRREL_TO_DISKARM_Y2_BOUND,d5
                lea     DISKARM_COORDS(a4),a3
                ENEMY_TO_DISKARM_COLLISION
                move.l  #POINTS_SQUIRREL,d2
                tst.w   d6                                      ; Did this sprite get hit with the disk armor
                bmi     .method_set_sweep_direction             ; Yes

                RUN_SPRITE_METHOD

                METHOD_ANIMATE_BONES

                METHOD_ANIMATE_SWEEP_LEFT

                METHOD_ANIMATE_SWEEP_RIGHT

                METHOD_SET_BONES

                METHOD_SET_SWEEP_DIRECTION

                METHOD_SET_JUMP_OR_DESTROY

                METHOD_SET_RYGAR_DEATH_SEQUENCE

.method_animate_swooping:
                lea     SQUIRREL_SWOOP_SINE(a4),a3
.sine_y_loop:   moveq   #0,d3
                move.w  THIS_SPRITE_SWEEP_INDEX(a2),d3
                move.w  (a3,d3*2),d2
                cmp.w   #$8000,d2
                bne.s   .sine_y_apply
                bra     .method_set_clear

.sine_y_apply:  addq.w  #1,THIS_SPRITE_SWEEP_INDEX(a2)
                add.w   PRIVATE_SQUIRREL_FIXED_YPOS(a2),d2
                move.w  d2,SPRITE_PLOT_YPOS(a1)

                moveq   #0,d2
                move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                ;sub.w   #50,d2
                sub.w	THIS_SPRITE_PARAMETER(a2),d2	
		UPDATE_SPRITE_PLOT_XPOS
                cmp.w   #2,d2
                ble     .method_set_clear
                tst.w   SPRITE_PLOT_YPOS(a1)
                beq     .method_set_clear
                bmi     .method_set_clear

                moveq   #SQUIRREL_STATUS_SWOOP,d1
                bra     ANIMATE

.method_animate_waiting:
                moveq   #0,d2
                move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                moveq   #SQUIRREL_STATUS_WAIT,d1
                bra     ANIMATE

.method_animate_clear:
                moveq   #SQUIRREL_STATUS_CLEAR,d1
                movem.l d0-1/a1-a2,-(a7)
                bsr     ANIMATE
                movem.l (a7)+,d0-1/a1-a2
                tst.w   d6
                bmi     .method_destroy_sprite
                bra     .exit

.method_destroy_sprite:
                clr.w   THIS_SPRITE_STATUS(a2)
                bra     DESTROY_SPRITE


.method_set_collision_action:
                tst.w   RYGAR_FALLPREV_STATE(a4)                                ; Is Rygar falling on enemy
                bmi     .method_set_jump_or_destroy                     ; Yes!

                IFNE    DISABLE_ENEMY_COLLISION
                bra     .exit
                ELSE
                bra     .method_set_rygar_death_sequence
                ENDC

.method_set_clear:
                move.w  #1,SPRITE_PLOT_XPOS(a1)
                move.w  #1,SPRITE_PLOT_YPOS(a1)
                move.w  #SQUIRREL_STATUS_CLEAR,THIS_SPRITE_STATUS(a2)
                bra.s   .set_update
                nop

.set_update:
.exit:          rts
