ENEMY_DRAGON1_XPOS:             equ     500
ENEMY_DRAGON1_YPOS:             equ     40

DRAGON1_GROUND_ENEMY:           equ     0       ; 1 = Ground enemy means enemy is stunnable.
DRAGON_HWSPR_ENABLE:            equ     1
DRAGON_HWSPR_UPPER:             equ     1
DRAGON_HWSPR_MID:               equ     0
DRAGON_HWSPR_LOWER:             equ     0

DRAGON1_FLY_SPEED:              equ     24

DRAGON1_STATUS_FLY_SWOOP_LEFT:  equ     6
DRAGON1_STATUS_FLY_SWOOP_RIGHT  equ     7
DRAGON1_STATUS_FLY_LIFT_LEFT:   equ     8
DRAGON1_STATUS_FLY_LIFT_RIGHT   equ     9

DRAGON1_GO_LEFT:                        equ     -1
DRAGON1_GO_RIGHT:                       equ     0


; Private Variables
PRIVATE_DRAGON1_SPEED:                  equ     32
PRIVATE_DRAGON1_FIXED_YPOS:             equ     34      ; Y Position of Sprite.
;PRIVATE_DRAGON1_DIRECTION:              equ     36      ; Current FLYing direction.
PRIVATE_DRAGON1_XPOS:                   equ     38
PRIVATE_DRAGON1_YPOS:                   equ     40
PRIVATE_DRAGON1_ANIM_FRAME:             equ     42
PRIVATE_DRAGON1_BOUNDARY_TIMER:         equ     44
PRIVATE_DRAGON1_PREV_STATUS:            equ     46
PRIVATE_DRAGON1_SINE_INDEX:             equ     48
PRIVATE_DRAGON1_HIT_COUNT:              equ     50

DRAGON1_HIT_POINTS:             equ     2

DRAGON1_TO_RYGAR_X1_BOUND:      equ     16
DRAGON1_TO_RYGAR_Y1_BOUND:      equ     3
DRAGON1_TO_RYGAR_X2_BOUND:      equ     47
DRAGON1_TO_RYGAR_Y2_BOUND:      equ     28

DRAGON1_TO_DISKARM_X1_BOUND:    equ     16
DRAGON1_TO_DISKARM_Y1_BOUND:    equ     3
DRAGON1_TO_DISKARM_X2_BOUND:    equ     47
DRAGON1_TO_DISKARM_Y2_BOUND:    equ     28


HDL_DRAGON1:
		FUNCID	#$63cbaf9c
                SPRITE_INITIATE

; Set the initial variables and status of the sprite.
.construct:
                FIND_CAMERA_XPOS
		
		move.l	#ENEMY_DEF_DRAGON,d5		
		CREATE_ENEMY_LEFT_OR_RIGHT
.create:
; d7 = spawn x position
                move.w  d7,THIS_SPRITE_MAP_XPOS(a2)                     ; Set start position in the map.
                move.w  #DRAGON1_FLY_SPEED,PRIVATE_DRAGON1_SPEED(a2)
		move.w	ROUND_CURRENT(a4),d2
		lsr.w	#1,d2
		add.w	d2,PRIVATE_DRAGON1_SPEED(a2)
		
                move.w  #ENEMY_DRAGON1_YPOS,PRIVATE_DRAGON1_FIXED_YPOS(a2)
                move.w  #DRAGON1_HIT_POINTS,PRIVATE_DRAGON1_HIT_COUNT(a2)

; Generate Random Number
                move.w  VPOSR(a5),d2
                swap    d2
                move.b  $bfe801,d2
                lsl.w   #8,d2
                move.b  $bfd800,d2
                and.w   #$1,d2
                beq.s   .create_1
                addq.w  #8,PRIVATE_DRAGON1_FIXED_YPOS(a2)
.create_1:

		lea	ENEMY_HWSPR_UPPER_ALLOCATED(a4),a3

.bank_0:
		cmp.w	2(a3),d0
		beq.s	.alloc_bank_0
		cmp.w	10(a3),d0
		beq.s	.alloc_bank_1
		move.w	#-1,THIS_SPRITE_HWSLOT(a2)		; Needs a bob
		bra	.active					; Neither sprites were allocated, needs to be a bob
		
.alloc_bank_0:	
                move.w  (a3),SPRITE_PLOT_TYPE(a1)                               ; Set hardware type.
		move.w	#0,THIS_SPRITE_HWSLOT(a2)				; Bank 0 allocated
		bra	.active
		
.alloc_bank_1:
		move.w	8(a3),SPRITE_PLOT_TYPE(a1)
		move.w	#2,THIS_SPRITE_HWSLOT(a2)
		bra	.active
		
		
		nop
		
.active:
		moveq	#ZERO_POINTS,d2
                tst.w   SPRITE_DESTROY_END_ROUND(a2)            ; Is the sprite set destroyed at the end of the round?
                beq.s   .sprite_0                               ; No, so carry on
                clr.w   SPRITE_DESTROY_END_ROUND(a2)            ; Yes, so set the sweep direction to destroy the enemy.	
		bra     .method_set_bones

		moveq	#ZERO_POINTS,d2
		move.w  THIS_SPRITE_STATUS(a2),PRIVATE_DRAGON1_PREV_STATUS(a2)
		
; Dragon at end of round does not do a sweep.
                tst.w   SPRITE_DESTROY_END_ROUND(a2)            ; Is the sprite set destroyed at the end of the round?
                beq.s   .sprite_0                               ; No, so carry on
                clr.w   SPRITE_DESTROY_END_ROUND(a2)            ; Yes, so set the sweep direction to destroy the enemy.  	
		bra     .method_add_points_and_destroy
         

.sprite_0:      
		ENEMY_BOUNDARY_CHECK
		
		;cmp.w	#ENEMY_STATUS_BONES,THIS_SPRITE_STATUS(a2)
		;beq	.method_set_bones
		bra.s   .sprite_1

.animate_table: dc.l    .method_destroy_sprite-.animate_table           ; 0
                dc.l    .method_animate_bones-.animate_table            ; 1
                dc.l    .method_animate_sweep_left-.animate_table       ; 2
                dc.l    .method_animate_sweep_right-.animate_table      ; 3                                        ; 3
		dc.l    .method_animate_none-.animate_table      	; 4
		dc.l    .method_animate_none-.animate_table      	; 5
                dc.l    .method_animate_fly_swoop_left-.animate_table   ; 6
                dc.l    .method_animate_fly_swoop_right-.animate_table  ; 7
                dc.l    .method_animate_fly_lift_left-.animate_table    ; 8
                dc.l    .method_animate_fly_lift_right-.animate_table   ; 9
                dc.l    .method_animate_none-.animate_table      	; 10
                dc.l    .method_animate_none-.animate_table      	; 11
                dc.l    .method_animate_none-.animate_table      	; 12
                dc.l    .method_animate_none-.animate_table      	; 13
                dc.l    .method_animate_none-.animate_table      	; 14
                dc.l    .method_animate_none-.animate_table      	; 15
                dc.l    .method_animate_none-.animate_table      	; 16
                dc.l    .method_animate_none-.animate_table      	; 17
                dc.l    .method_animate_none-.animate_table      	; 18
                dc.l    .method_animate_none-.animate_table      	; 19
                dc.l    .method_animate_none-.animate_table      	; 20
                dc.l    .method_animate_none-.animate_table      	; 21
                dc.l    .method_animate_none-.animate_table      	; 22
                dc.l    .method_animate_none-.animate_table      	; 23
                dc.l    .method_animate_none-.animate_table      	; 24


.method_animate_none:
		bra	.exit

.sprite_1:
		cmp.w	#DRAGON1_STATUS_FLY_SWOOP_LEFT,THIS_SPRITE_STATUS(a2)		; Do not apply sine when
		blt	.run_sprite_method						; Dragon marked destroyed.

; Apply swooping sine animation 
                lea     DRAGON1_YSINE(a4),a3
.sine_y_loop:   moveq   #0,d3
                move.w  PRIVATE_DRAGON1_SINE_INDEX(a2),d3
                move.w  (a3,d3*2),d2
                cmp.w   #$8000,d2
                bne.s   .sine_y_apply
                clr.w   PRIVATE_DRAGON1_SINE_INDEX(a2)
                bra.s   .sine_y_loop

.sine_y_apply:  addq.w  #1,PRIVATE_DRAGON1_SINE_INDEX(a2)
                add.w   PRIVATE_DRAGON1_FIXED_YPOS(a2),d2
                move.w  d2,SPRITE_PLOT_YPOS(a1)
                move.w  d2,PRIVATE_DRAGON1_YPOS(a2)

; Test this enemy collided with Rygar (the player)
.sprite_2:      move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4
                move.w  d3,d5
                add.w   #DRAGON1_TO_RYGAR_X1_BOUND,d2
                add.w   #DRAGON1_TO_RYGAR_Y1_BOUND,d3
                add.w   #DRAGON1_TO_RYGAR_X2_BOUND,d4
                add.w   #DRAGON1_TO_RYGAR_Y2_BOUND,d5
                lea     RYGAR_COORDS(a4),a3
                ENEMY_TO_RYGAR_COLLISION
                move.l  #POINTS_DRAGON,d2
                tst.w   d6                                      ; Did this sprite collide with Rygar?
                IFEQ    DISABLE_ENEMY_COLLISION
                bmi     .method_set_collision_action            ; Yes
                ENDC

; Check if the enemy has been hit with disk armor
                move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4
                move.w  d3,d5
                add.w   #DRAGON1_TO_DISKARM_X1_BOUND,d2
                add.w   #DRAGON1_TO_DISKARM_Y1_BOUND,d3
                add.w   #DRAGON1_TO_DISKARM_X2_BOUND,d4
                add.w   #DRAGON1_TO_DISKARM_Y2_BOUND,d5
                lea     DISKARM_COORDS(a4),a3
                ENEMY_TO_DISKARM_COLLISION
                move.l  #POINTS_DRAGON,d2
                tst.w   d6                                      ; Did this sprite get hit with the disk armor
                bmi     .method_add_points_and_destroy                  ; Yes

                RUN_SPRITE_METHOD

                METHOD_ANIMATE_BONES

                METHOD_ANIMATE_SWEEP_LEFT

                METHOD_ANIMATE_SWEEP_RIGHT

                METHOD_SET_SWEEP_DIRECTION

                METHOD_SET_JUMP_OR_DESTROY

                METHOD_SET_RYGAR_DEATH_SEQUENCE

; The Dragon takes multiple hits...
; Unless Rygar has extra powers...
.method_add_points_and_destroy:
		tst.w	STATE_KILL_ALL_ENEMIES(a4)
		bmi.s	.xxx
		move.w	#-1,DISKARM_DISABLE(a4)	
.xxx
		tst.w	ITEM_KILLALL_SET(a4)
		bmi.s	.method_set_bones
		
; Play hit sound
                move.l  d0,-(a7)
                moveq   #SND_LAND_ON_ENEMY,d0
                bsr     PLAY_SAMPLE
                move.l  (a7)+,d0
		
		moveq	#1,d3
		move.w  RYGAR_CURRENT_POWERS(a4),d4
                and.w   #POWER_CROWN,d4
                beq     .has_crown                     ; If Rygar has Crown then the Dragon is killed
		moveq	#3,d3
.has_crown:                		
                sub.w  	d3,PRIVATE_DRAGON1_HIT_COUNT(a2)

		move.w	#HITPOINT_DELAY,HITPOINT_FRAMES(a4)
		tst.w	PRIVATE_DRAGON1_HIT_COUNT(a2)
		beq	.run_sprite_method
		bpl	.run_sprite_method

                add.l   d2,VAL_PLAYER1_SCORE(a4)
                move.w  #-1,UPDATE_SCORE(a4)
                addq.w  #1,REPULSE_BONUS_NUM(a4)                    ; Add one to enemies killed.
                bra     .method_set_bones

.method_set_bones:
		lea	ATTACH_TABLE(a4),a3
		move.b	#-1,(a3,d0)
		
		
; This defo triggers.
                lea     ENEMY_HWSPR_UPPER_ALLOCATED(a4),a0
		

		
                cmp.w   2(a0),d0
                beq.s   .found_partner1
                addq.w  #8,a0
		cmp.w	2(a0),d0
		beq.s	.found_partner1
.loop1:
		add.w	#$123,FIRECOLOUR
		bsr	FIREWAITSR
		bra	.loop1

.found_partner1: 

		
		move.w  6(a0),d2
		bmi	.method_destroy_sprite
                move.l  MEMCHK0_SPRITE_HDLVARS(a4),a0
                lea     TAB_64(a4),a3
                add.w   (a3,d2*2),a0
                move.w  #ENEMY_STATUS_BONES,THIS_SPRITE_STATUS(a0)		; Set partner to bones
                move.w  #ENEMY_STATUS_BONES,THIS_SPRITE_STATUS(a2)		; Set this sprite to bones
                bra     .set_update

; d2=xpos to plot sprite
;
.method_animate_fly_lift_left:
.method_animate_fly_swoop_left:
                move.w  #SPRITE_MOVE_LEFT,THIS_SPRITE_DIRECTION(a2)
                move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                sub.w   PRIVATE_DRAGON1_SPEED(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                move.w  d2,PRIVATE_DRAGON1_XPOS(a2)
                cmp.w   #32,PRIVATE_DRAGON1_SINE_INDEX(a2)
                ble.s   .flyleft_up
.flyleft_down:  moveq   #DRAGON1_STATUS_FLY_SWOOP_LEFT,d1
                bra.s   .flyleft
.flyleft_up:    moveq   #DRAGON1_STATUS_FLY_LIFT_LEFT,d1
.flyleft:       move.w  d1,PRIVATE_DRAGON1_ANIM_FRAME(a2)
                bra     ANIMATE


.method_animate_fly_lift_right:
.method_animate_fly_swoop_right:
                move.w  #SPRITE_MOVE_RIGHT,THIS_SPRITE_DIRECTION(a2)
                move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                add.w   PRIVATE_DRAGON1_SPEED(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                move.w  d2,PRIVATE_DRAGON1_XPOS(a2)
                cmp.w   #32,PRIVATE_DRAGON1_SINE_INDEX(a2)
                ble.s   .flyright_up
.flyright_down: moveq   #DRAGON1_STATUS_FLY_SWOOP_RIGHT,d1
                bra.s   .flyright
.flyright_up:   moveq   #DRAGON1_STATUS_FLY_LIFT_RIGHT,d1
.flyright:      move.w  d1,PRIVATE_DRAGON1_ANIM_FRAME(a2)
                bra     ANIMATE


.method_destroy_sprite:      

		;clr.w	$230
		
		lea     ENEMY_HWSPR_UPPER_ALLOCATED(a4),a0
                cmp.w   2(a0),d0
                beq.s   .found_partner2
                addq.w  #8,a0
		cmp.w	2(a0),d0
		beq.s	.found_partner2
		add.w	#$123,FIRECOLOUR
		;bsr	FIREWAITSR

.found_partner2: 
		move.w  6(a0),d2		; Get Dragon 2 sprite

		move.w	#-1,2(a0)
		move.w	#-1,6(a0)
		
                move.l  MEMCHK0_SPRITE_HDLVARS(a4),a0
                lea     TAB_64(a4),a3
                add.w   (a3,d2*2),a0
                
		move.w  #ENEMY_STATUS_DESTROYED,THIS_SPRITE_STATUS(a0)		; Set partner to bones
                move.w  #ENEMY_STATUS_DESTROYED,THIS_SPRITE_STATUS(a2)		; Set this sprite to bones
                		
                subq.w  #1,DRAGONS_IN_USE(a4)
		
                clr.w   THIS_SPRITE_STATUS(a2)
                ;lea     ENEMY_HWSPR_UPPER_ALLOCATED(a4),a0                      ; Free up hardware sprite
		;move.w  THIS_SPRITE_HWSLOT(a2),d7
                ;bmi     DESTROY_SPRITE
		;move.w  #-1,2(a0,d7*4)	
                bra     DESTROY_SPRITE
		
		
.method_set_destroy_sprite:
                move.w  #ENEMY_STATUS_DESTROYED,THIS_SPRITE_STATUS(a2)
                bra     .set_update

.method_set_fly_lift_left:
                move.w  #SPRITE_MOVE_LEFT,THIS_SPRITE_DIRECTION(a2)
                move.w  #DRAGON1_STATUS_FLY_LIFT_LEFT,THIS_SPRITE_STATUS(a2)
                bra     .set_update

.method_set_fly_lift_right:
                move.w  #SPRITE_MOVE_RIGHT,THIS_SPRITE_DIRECTION(a2)
                move.w  #DRAGON1_STATUS_FLY_LIFT_RIGHT,THIS_SPRITE_STATUS(a2)
                bra     .set_update

.method_set_collision_action:
                tst.w   RYGAR_FALLPREV_STATE(a4)                                ; Is Rygar falling on enemy
                bmi     .method_set_jump_or_destroy                     ; Yes!

                IFNE    DISABLE_ENEMY_COLLISION
                bra     .exit
                ELSE
                bra     .method_set_rygar_death_sequence
                ENDC


.set_update:
.exit:          rts



HDL_DRAGON2:
		FUNCID	#$42650782
                SPRITE_INITIATE
		
                move.w  #-1,THIS_SPRITE_STATUS(a2)
		
		lea	ENEMY_HWSPR_UPPER_ALLOCATED(a4),a3
.bank_0:
		cmp.w	6(a3),d0
		beq.s	.alloc_bank_0
		cmp.w	14(a3),d0
		beq.s	.alloc_bank_1
		move.w	#-1,THIS_SPRITE_HWSLOT(a2)		; Needs a bob
		bra	.exit					; Neither sprites were allocated, needs to be a bob
		
.alloc_bank_0:	
                move.w  4(a3),SPRITE_PLOT_TYPE(a1)                               ; Set hardware type.
		move.w	#1,THIS_SPRITE_HWSLOT(a2)				; Bank 0 allocated
		bra	.exit
		
.alloc_bank_1:
		move.w	12(a3),SPRITE_PLOT_TYPE(a1)
		move.w	#3,THIS_SPRITE_HWSLOT(a2)
		bra	.exit
 
		nop

; Get Dragon 1 context
.active:	

.sprite_0:      

; THIS BIT IS THE PROBLEM WITH THE CODE!!!!
; Copy the position of the first dragon slot.
                lea     ENEMY_HWSPR_UPPER_ALLOCATED(a4),a0
                cmp.w   6(a0),d0
                beq.s   .found_alloc
                addq.w  #8,a0

.found_alloc:   move.w  2(a0),d2
		bmi	.method_destroy_sprite
                                                                ; Previous sprite number adjacent.
                move.l  MEMCHK0_SPRITE_HDLVARS(a4),a0
                lea     TAB_64(a4),a3
                add.w   (a3,d2*2),a0
; Dragon 1 sprite context now in a0
                move.w  PRIVATE_DRAGON1_XPOS(a0),d2
                add.w   #32,d2                                  ; 32 pixels to right
                and.w   #$7ff,d2
                move.w  d2,SPRITE_PLOT_XPOS(a1)
                move.w  PRIVATE_DRAGON1_YPOS(a0),SPRITE_PLOT_YPOS(a1)


		cmp.w   #ENEMY_STATUS_BONES,THIS_SPRITE_STATUS(a2)
                beq     .dbg_bones
                cmp.w   #ENEMY_STATUS_DESTROYED,THIS_SPRITE_STATUS(a2)
                beq     .method_destroy_sprite	
		
		
		
                move.w  PRIVATE_DRAGON1_ANIM_FRAME(a0),d1
                bra     ANIMATE

.method_destroy_sprite:
                clr.w   THIS_SPRITE_STATUS(a2)
                lea     ENEMY_HWSPR_UPPER_ALLOCATED(a4),a0                      ; Free up hardware sprite
		move.w  THIS_SPRITE_HWSLOT(a2),d7
                bmi     DESTROY_SPRITE
		;move.w  #-1,2(a0,d7*4)		
                bra     DESTROY_SPRITE

.dbg_bones:	;bsr	FIREWAITSR

                METHOD_ANIMATE_BONES


.exit:          rts


