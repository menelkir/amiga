ENEMY_EVIL_XPOS:                equ     2
ENEMY_EVIL_YPOS:                equ     100

EVIL_TO_RYGAR_X1_BOUND:         equ     0
EVIL_TO_RYGAR_Y1_BOUND:         equ     0
EVIL_TO_RYGAR_X2_BOUND:         equ     64
EVIL_TO_RYGAR_Y2_BOUND:         equ     64

EVIL_MOVE_SPEED:                equ     34
EVIL_STATUS_MOVE:                       equ     6

EVIL_SINE_SPEED:                equ     2

EVIL_MAX_SPEED_X:               equ     60


EVIL_GO_LEFT:                   equ     -1
EVIL_GO_RIGHT:                  equ     0

; Private Variables
PRIVATE_EVIL_SPEED:             equ     32
PRIVATE_EVIL_FIXED_YPOS:        equ     34      ; Y Position of Sprite.
PRIVATE_EVIL_SINE_INDEX:        equ     36      ; Index to add to Y Position
PRIVATE_EVIL_DIRECTION:         equ     38      ; Current FLYing direction.
PRIVATE_EVIL_XPOS:              equ     40
PRIVATE_EVIL_YPOS:              equ     42
PRIVATE_EVIL_ANIM_FRAME:        equ     44
PRIVATE_EVIL_BOUNDARY_TIMER:    equ     46
PRIVATE_EVIL_LAST_ANIM:         equ     58




; Input
;d2=X Position of Enemy
;d3=Y position of Enemy
;a3 = Coordinates pointer x1,x2,y1,y2
; Output
;d6=Return value (-1 is collision)


EVIL_TO_RYGAR_COLLISION MACRO
                move.l  d4,-(a7)
                move.l  d5,-(a7)

                move.w  d2,d4                                   ; Check a 32x32 boundary from x1/y1
                move.w  d3,d5
                add.w   #64,d4
                add.w   #64,d5

                moveq   #0,d6
.ryg_coll_loop: cmp.w   2(a3),d2                                ; Check X1 against Rygar X2
                bgt.s   .ryg_coll_next
                cmp.w   (a3),d4                                 ; Check X2 against Rygar X1
                blt.s   .ryg_coll_next
                cmp.w   6(a3),d3                                ; Check Y1 against Rygar Y2
                bgt.s   .ryg_coll_next
                cmp.w   4(a3),d5                                ; Check Y2 against Rygar Y1
                blt.s   .ryg_coll_next
                moveq   #-1,d6
.ryg_coll_next: nop
.ryg_coll_exit: move.l  (a7)+,d5
                move.l  (a7)+,d4
                ENDM


HDL_EVIL_UL:
		FUNCID	#$5fdf1683
                SPRITE_INITIATE

; Set the initial variables and status of the sprite.
.construct:
                FIND_CAMERA_XPOS

                move.w  #EVIL_STATUS_MOVE,THIS_SPRITE_STATUS(a2)      ; Create to the left, FLY right
                move.w  #EVIL_GO_RIGHT,PRIVATE_EVIL_DIRECTION(a2)
                move.w  #EVIL_MOVE_SPEED,PRIVATE_EVIL_SPEED(a2)
                move.w  #ENEMY_EVIL_YPOS,PRIVATE_EVIL_FIXED_YPOS(a2)

                moveq   #0,d2                                           ; Set X Position
                move.w  MAP_PIXEL_POSX(a4),d2
                sub.w   #$100,d2
                lsl.w   #4,d2
                move.w  d2,THIS_SPRITE_MAP_XPOS(a2)

.active:        

		ENEMY_CHECK_END_OF_ROUND

.sprite_0:      cmp.w   #ENEMY_STATUS_BONES,THIS_SPRITE_STATUS(a2)
                beq     .method_animate_bones
                bra.s   .sprite_1

.animate_table: dc.l    .method_animate_none-.animate_table      	; 0
                dc.l    .method_animate_bones-.animate_table            ; 1
		dc.l    .method_animate_none-.animate_table      	; 2
		dc.l    .method_animate_none-.animate_table      	; 3
		dc.l    .method_animate_none-.animate_table      	; 4
		dc.l    .method_animate_none-.animate_table      	; 5
                dc.l    .method_animate_move-.animate_table             ; 6

.method_animate_none:	bra	.exit


.sprite_1:      lea     EVIL_YSINE(a4),a3
.sine_y_loop:   moveq   #0,d3
                move.w  PRIVATE_EVIL_SINE_INDEX(a2),d3
                move.w  (a3,d3*2),d2
                cmp.w   #$8000,d2
                bne.s   .sine_y_apply
                clr.w   PRIVATE_EVIL_SINE_INDEX(a2)
                bra.s   .sine_y_loop

.sine_y_apply:
                addq.w  #EVIL_SINE_SPEED,PRIVATE_EVIL_SINE_INDEX(a2)
                add.w   PRIVATE_EVIL_FIXED_YPOS(a2),d2
                move.w  d2,SPRITE_PLOT_YPOS(a1)
                move.w  d2,PRIVATE_EVIL_YPOS(a2)

; Test this enemy collided with Rygar (the player)
.sprite_2:      move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4
                move.w  d3,d5

                add.w   #EVIL_TO_RYGAR_X1_BOUND,d2
                add.w   #EVIL_TO_RYGAR_Y1_BOUND,d3
                add.w   #EVIL_TO_RYGAR_X2_BOUND,d4
                add.w   #EVIL_TO_RYGAR_Y2_BOUND,d5
                lea     RYGAR_COORDS(a4),a3
                ENEMY_TO_RYGAR_COLLISION
                tst.w   d6
                IFEQ    DISABLE_ENEMY_COLLISION
                bmi             .method_set_rygar_death_sequence            ; Yes
                ENDC

; Test this enemy collided with the disk armour
                RUN_SPRITE_METHOD


; d2=xpos to plot sprite
;
.method_animate_move:   move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                add.w   PRIVATE_EVIL_SPEED(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                move.w  d2,PRIVATE_EVIL_XPOS(a2)
                move.w  SPRITE_PLOT_YPOS(a1),EVIL_YPOS(a4)

                cmp.w   #400,d2
                ble.s   .move

                cmp.w   #EVIL_MAX_SPEED_X,PRIVATE_EVIL_SPEED(a2)
                bge.s   .speed_up
                addq.w  #2,PRIVATE_EVIL_SPEED(a2)               ; Increase Speed

.speed_up:              moveq   #0,d2
                move.w  MAP_PIXEL_POSX(a4),d2
                sub.w   #$100,d2
                lsl.w   #4,d2
                move.w  d2,THIS_SPRITE_MAP_XPOS(a2)
.move:

                moveq   #EVIL_STATUS_MOVE,d1
                bra     ANIMATE

                METHOD_ANIMATE_BONES

.method_set_sweep_direction:
                bra.s   .method_set_bones

.method_destroy_sprite:
                clr.w   THIS_SPRITE_STATUS(a2)
                bra     DESTROY_SPRITE

.method_set_move:
                move.w  #EVIL_GO_LEFT,PRIVATE_EVIL_DIRECTION(a2)
                move.w  #EVIL_STATUS_MOVE,THIS_SPRITE_STATUS(a2)
                bra     .set_update

.method_set_bones:      move.w  #ENEMY_STATUS_BONES,THIS_SPRITE_STATUS(a2)

; d2 = sprite number of UR
                lea     TAB_64(a4),a3
                move.l  MEMCHK0_SPRITE_HDLVARS(a4),a0
                move.w  EVIL_UR_SPRNUM(a4),d2
                add.w   (a3,d2*2),a0
                move.w  #ENEMY_STATUS_BONES,THIS_SPRITE_STATUS(a0)

; d2 = sprite number of DL
                move.l  MEMCHK0_SPRITE_HDLVARS(a4),a0
                move.w  EVIL_DL_SPRNUM(a4),d2
                add.w   (a3,d2*2),a0
                move.w  #ENEMY_STATUS_BONES,THIS_SPRITE_STATUS(a0)

; d2 = sprite number of DR
                ;lea    HDL_SPRITE_STATUS(a4),a0
                move.l  MEMCHK0_SPRITE_HDLVARS(a4),a0
                move.w  EVIL_DR_SPRNUM(a4),d2
                add.w   (a3,d2*2),a0
                move.w  #ENEMY_STATUS_BONES,THIS_SPRITE_STATUS(a0)
                bra     .set_update

                METHOD_SET_RYGAR_DEATH_SEQUENCE

.set_update:

.exit:                  rts



HDL_EVIL_UR:
		FUNCID	#$83c30dd5
                SPRITE_INITIATE

                move.w  #-1,THIS_SPRITE_HWSLOT(a2)
.active:
                tst.w   SPRITE_DESTROY_END_ROUND(a2)
                beq.s   .sprite_0
                clr.w   SPRITE_DESTROY_END_ROUND(a2)
                move.w  #ENEMY_STATUS_BONES,THIS_SPRITE_STATUS(a2)

.sprite_0:      cmp.w   #ENEMY_STATUS_BONES,THIS_SPRITE_STATUS(a2)
                beq     .method_animate_bones
                bra.s   .method_animate_move

                nop

.method_animate_move:
                move.w  EVIL_UL_SPRNUM(a4),d2
                move.l  MEMCHK0_SPRITE_HDLVARS(a4),a0
                lea     TAB_64(a4),a3
                add.w   (a3,d2*2),a0
                move.w  PRIVATE_EVIL_XPOS(a0),d2
                add.w   #32,d2                                  ; 32 pixels to right
                move.w  d2,SPRITE_PLOT_XPOS(a1)
                move.w  PRIVATE_EVIL_YPOS(a0),SPRITE_PLOT_YPOS(a1)
                moveq   #EVIL_STATUS_MOVE,d1
                bra     ANIMATE

.method_animate_bones:
                moveq   #ENEMY_STATUS_BONES,d1
                movem.l d0-1/a1-a2,-(a7)
                bsr     ANIMATE
                movem.l (a7)+,d0-1/a1-a2
                tst.w   d6
                bmi.s   .destroy_enemy
                bra     .exit

.destroy_enemy: clr.w   THIS_SPRITE_STATUS(a2)
                clr.w   THIS_SPRITE_STATUS(a1)
                bra     DESTROY_SPRITE

.exit:          rts



HDL_EVIL_DL:
		FUNCID	#$5ee592de
                SPRITE_INITIATE

                move.w  #-1,THIS_SPRITE_HWSLOT(a2)
.active:
                tst.w   SPRITE_DESTROY_END_ROUND(a2)
                beq.s   .sprite_0
                clr.w   SPRITE_DESTROY_END_ROUND(a2)
                move.w  #ENEMY_STATUS_BONES,THIS_SPRITE_STATUS(a2)

.sprite_0:              cmp.w   #ENEMY_STATUS_BONES,THIS_SPRITE_STATUS(a2)
                beq     .method_animate_bones

.method_animate_move:
                move.w  EVIL_UL_SPRNUM(a4),d2
                move.l  MEMCHK0_SPRITE_HDLVARS(a4),a0
                lea     TAB_64(a4),a3
                add.w   (a3,d2*2),a0
                move.w  PRIVATE_EVIL_XPOS(a0),d2
                move.w  PRIVATE_EVIL_YPOS(a0),d3
                add.w   #32,d3                                  ; 32 pixels below.
                move.w  d2,SPRITE_PLOT_XPOS(a1)
                move.w  d3,SPRITE_PLOT_YPOS(a1)
                moveq   #EVIL_STATUS_MOVE,d1
                bra     ANIMATE

.method_animate_bones:
                moveq   #ENEMY_STATUS_BONES,d1
                movem.l d0-1/a1-a2,-(a7)
                bsr     ANIMATE
                movem.l (a7)+,d0-1/a1-a2
                tst.w   d6
                bmi.s   .destroy_enemy
                bra     .exit

.destroy_enemy: clr.w   THIS_SPRITE_STATUS(a2)
                clr.w   THIS_SPRITE_STATUS(a1)
                bra     DESTROY_SPRITE

.exit:          rts




HDL_EVIL_DR:
		FUNCID	#$c58ff422
                SPRITE_INITIATE

                move.w  #-1,THIS_SPRITE_HWSLOT(a2)
.active:
                tst.w   SPRITE_DESTROY_END_ROUND(a2)
                beq.s   .sprite_0
                clr.w   SPRITE_DESTROY_END_ROUND(a2)
                move.w  #ENEMY_STATUS_BONES,THIS_SPRITE_STATUS(a2)

.sprite_0:      cmp.w   #ENEMY_STATUS_BONES,THIS_SPRITE_STATUS(a2)
                beq     .method_animate_bones

.method_animate_move:
                move.w  EVIL_UL_SPRNUM(a4),d2
                move.l  MEMCHK0_SPRITE_HDLVARS(a4),a0
                lea     TAB_64(a4),a3
                add.w   (a3,d2*2),a0
                move.w  PRIVATE_EVIL_XPOS(a0),d2
                move.w  PRIVATE_EVIL_YPOS(a0),d3
                add.w   #32,d2
                add.w   #32,d3                                  ; 32 pixels below.
                move.w  d2,SPRITE_PLOT_XPOS(a1)
                move.w  d3,SPRITE_PLOT_YPOS(a1)
                moveq   #EVIL_STATUS_MOVE,d1
                bra     ANIMATE

.method_animate_bones:
                moveq   #ENEMY_STATUS_BONES,d1
                movem.l d0-1/a1-a2,-(a7)
                bsr     ANIMATE
                movem.l (a7)+,d0-1/a1-a2
                tst.w   d6
                bmi.s   .destroy_enemy
                bra     .exit

.destroy_enemy: clr.w   THIS_SPRITE_STATUS(a2)
                clr.w   THIS_SPRITE_STATUS(a1)
                bra     DESTROY_SPRITE

.exit:          rts

