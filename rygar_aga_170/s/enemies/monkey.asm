MONKEY_GROUND_ENEMY:             equ     1       ; 1 = Ground enemy means enemy is stunnable.
MONKEY_HWSPR_ENABLE:             equ     0
MONKEY_HWSPR_UPPER:              equ     0
MONKEY_HWSPR_MID:                equ     0
MONKEY_HWSPR_LOWER:              equ     0


MONKEY_TO_RYGAR_X1_BOUND:        equ     8
MONKEY_TO_RYGAR_Y1_BOUND:        equ     0
MONKEY_TO_RYGAR_X2_BOUND:        equ     23
MONKEY_TO_RYGAR_Y2_BOUND:        equ     31

MONKEY_TO_DISKARM_X1_BOUND:      equ     8
MONKEY_TO_DISKARM_Y1_BOUND:      equ     0
MONKEY_TO_DISKARM_X2_BOUND:      equ     23
MONKEY_TO_DISKARM_Y2_BOUND:      equ     31

MONKEY_MOVE_SPEED:               equ     16      ; Initial speed.
MONKEY_JUMP_SPEED:               equ     22      ; 2 pixels per frame.
MONKEY_SINE_SPEED:               equ     3

MONKEY_JUMP_LENGTH:              equ     MONKEY_JUMP_SPEED/16      ; 50 pi

MONKEY_LAND_DELAY:               equ     1
MONKEY_LEAP_DELAY:               equ     1
MONKEY_SPEED_INCREASE:           equ     30

MONKEY_LAND_LEFT:                equ     6
MONKEY_LAND_RIGHT:               equ     7
MONKEY_RUN_LEFT:                 equ     8
MONKEY_RUN_RIGHT:                equ     9
MONKEY_JUMP_LEFT:                equ     10
MONKEY_JUMP_RIGHT:               equ     11
MONKEY_LEAP_LEFT:                equ     12
MONKEY_LEAP_RIGHT:               equ     13

MONKEY_GO_LEFT:                  equ     -1
MONKEY_GO_RIGHT:                 equ     0
MONKEY_STATUS_WALK_LEFT: equ     6

PRIVATE_MONKEY_MOVE_SPEED:       equ     32
PRIVATE_MONKEY_TIMER:            equ     34
PRIVATE_MONKEY_IS_DESCENDING:    equ     36
PRIVATE_MONKEY_SINE_POINTER:     equ     38
PRIVATE_MONKEY_FIXED_YPOS:       equ     40
PRIVATE_MONKEY_INCREMENT_SPEED:  equ     42
PRIVATE_MONKEY_CURRENT_PLATFORM: equ     44
PRIVATE_MONKEY_BOUNDARY_TIMER:   equ     46      ;
PRIVATE_MONKEY_DIRECTION:	equ	48

HDL_MONKEY:
		FUNCID	#$273595fb
                SPRITE_INITIATE

; Set the initial variables and status of the sprite.
.construct:
			
		FIND_CAMERA_XPOS

		bra.s	.create_right
		
.create_left:
                move.w  #MONKEY_RUN_RIGHT,THIS_SPRITE_STATUS(a2) ; Create to the right, walk left
                move.w  #MONKEY_GO_RIGHT,PRIVATE_MONKEY_DIRECTION(a2)
                exg     d6,d7                                           ; set space position
.create_left_loop:
                move.l  d7,d2
                lea     ROUND_ENEMY_POSITIONS(a4),a0
                lsr.w   #8,d2                           ; Interpolate down
                move.l  (a0,d2*4),d2
                and.l   #ENEMY_DEF_MONKEY,d2
                bne.s   .create
                sub.w   #16*16,d7
                bra.s   .create_left_loop

.create_right:  move.w  #MONKEY_RUN_LEFT,THIS_SPRITE_STATUS(a2)  ; Create to the left, walk right
                move.w  #MONKEY_GO_LEFT,PRIVATE_MONKEY_DIRECTION(a2)

.create_right_loop:
                move.l  d7,d2
                lea     ROUND_ENEMY_POSITIONS(a4),a0
                lsr.w   #8,d2                           ; Interpolate down
                move.l  (a0,d2*4),d2
                and.l   #ENEMY_DEF_MONKEY,d2
                bne.s   .create
                add.w   #16*16,d7
                bra.s   .create_right_loop

.create:
		move.w  d7,THIS_SPRITE_MAP_XPOS(a2)                     ; Set start position in the map.
		
                move.w  #MONKEY_MOVE_SPEED,PRIVATE_MONKEY_MOVE_SPEED(a2)  ; Set move speed.
		move.w	THIS_SPRITE_PARAMETER(a2),d3
		add.w	d3,PRIVATE_MONKEY_MOVE_SPEED(a2)
		
                move.w  #-1,THIS_SPRITE_STUN_TIMER(a2)
                move.w  #ENEMY_BOUNDARY_TIME,PRIVATE_MONKEY_BOUNDARY_TIMER(a2)
		
		

		
;;; Init code here.

.active:
                ENEMY_CHECK_END_OF_ROUND

.sprite_0:
		ENEMY_BOUNDARY_CHECK
		
		bra.s	.sprite_1


.animate_table: dc.l    .method_animate_none-.animate_table      	; 4
                dc.l    .method_animate_bones-.animate_table            ; 1
                dc.l    .method_animate_sweep_left-.animate_table       ; 2
                dc.l    .method_animate_sweep_right-.animate_table      ; 3
                dc.l    .method_animate_stun_left-.animate_table        ; 4
                dc.l    .method_animate_stun_right-.animate_table       ; 5
                dc.l    .method_animate_land_left-.animate_table        ; 6
                dc.l    .method_animate_land_right-.animate_table       ; 7
                dc.l    .method_animate_run_left-.animate_table         ; 8
                dc.l    .method_animate_run_right-.animate_table        ; 9
                dc.l    .method_animate_jump_left-.animate_table        ; 10
                dc.l    .method_animate_jump_right-.animate_table       ; 11
                dc.l    .method_animate_leap_left-.animate_table        ; 12
                dc.l    .method_animate_leap_right-.animate_table       ; 13
                dc.l    .method_animate_none-.animate_table      	; 14
                dc.l    .method_animate_none-.animate_table      	; 15
                dc.l    .method_animate_none-.animate_table      	; 16
                dc.l    .method_animate_none-.animate_table      	; 17
                dc.l    .method_animate_none-.animate_table      	; 18
                dc.l    .method_animate_none-.animate_table      	; 19
                dc.l    .method_animate_none-.animate_table      	; 20
		dc.l    .method_animate_none-.animate_table      	; 21
                dc.l    .method_animate_none-.animate_table      	; 22
                dc.l    .method_animate_none-.animate_table      	; 23
                dc.l    .method_animate_none-.animate_table      	; 24
		
.method_animate_none:	bra	.exit

.sprite_1:      cmp.w   #ENEMY_STATUS_SWEEP_RIGHT,THIS_SPRITE_STATUS(a2)                ; if doing bones or sweeps then skip all collisions
                ble     .run_sprite_method

		tst.w	SHIELD_TIMER
		bpl.s	.shield_on
                cmp.w   #ENEMY_STATUS_STUN_RIGHT,THIS_SPRITE_STATUS(a2)                 ; if doing stuns then skip Rygar collision
                ble     .sprite_stunned

.shield_on:

;Test this enemy collided with Rygar (the player)
.sprite_2:      move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4                                   ; Check a 32x32 boundary from x1/y1
                move.w  d3,d5
                add.w   #MONKEY_TO_RYGAR_X1_BOUND,d2
                add.w   #MONKEY_TO_RYGAR_Y1_BOUND,d3
                add.w   #MONKEY_TO_RYGAR_X2_BOUND,d4
                add.w   #MONKEY_TO_RYGAR_Y2_BOUND,d5
                lea     RYGAR_COORDS(a4),a3
                ENEMY_TO_RYGAR_COLLISION
                move.l  #POINTS_MONKEY,d2
                tst.w   d6                                      ; Did this sprite collide with Rygar?
                IFEQ    DISABLE_ENEMY_COLLISION
                bmi     .method_set_collision_action            ; Yes
                ENDC

; Check if the enemy has been hit with disk armor
.sprite_stunned:
                move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4                                   ; Check a 32x32 boundary from x1/y1
                move.w  d3,d5
                add.w   #MONKEY_TO_DISKARM_X1_BOUND,d2
                add.w   #MONKEY_TO_DISKARM_Y1_BOUND,d3
                add.w   #MONKEY_TO_DISKARM_X2_BOUND,d4
                add.w   #MONKEY_TO_DISKARM_Y2_BOUND,d5
                lea     DISKARM_COORDS(a4),a3
                ENEMY_TO_DISKARM_COLLISION
                move.l  #POINTS_MONKEY,d2
                tst.w   d6                                      ; Did this sprite get hit with the disk armor
                bmi     .method_set_sweep_direction             ; Yes

; Test this enemy collided with the disk armour

                RUN_SPRITE_METHOD

                METHOD_ANIMATE_BONES

                METHOD_ANIMATE_SWEEP_LEFT

                METHOD_ANIMATE_SWEEP_RIGHT

                METHOD_SET_BONES

                METHOD_SET_SWEEP_DIRECTION

                METHOD_SET_STUN_OR_DESTROY

                METHOD_SET_STUN_DIRECTION

                METHOD_SET_RYGAR_DEATH_SEQUENCE


.method_set_collision_action:
                tst.w   RYGAR_FALLPREV_STATE(a4)                                ; Is Rygar falling on enemy
                bmi     .method_set_stun_or_destroy                     ; Yes!

                IFNE    DISABLE_ENEMY_COLLISION
                bra     .exit
                ELSE
                bra     .method_set_rygar_death_sequence
                ENDC


.method_destroy_sprite:
                tst.w   MONKEYS_IN_USE(a4)
                beq.s   .method_destroy_sprite_0
                subq.w  #1,MONKEYS_IN_USE(a4)

.method_destroy_sprite_0:
                clr.w   THIS_SPRITE_STATUS(a2)
                IFNE    MONKEY_HWSPR_ENABLE
                IFNE    MONKEY_HWSPR_UPPER
                lea     ENEMY_HWSPR_UPPER_ALLOCATED(a4),a0                  ; Free up hardware sprite
                ENDC
                IFNE    MONKEY_HWSPR_MID
                lea     ENEMY_HWSPR_MID_ALLOCATED(a4),a0                    ; Free up hardware sprite
                ENDC
                IFNE    MONKEY_HWSPR_LOWER
                lea     ENEMY_HWSPR_LOWER_ALLOCATED(a4),a0                  ; Free up hardware sprite
                ENDC
                move.w  THIS_SPRITE_HWSLOT(a2),d7
                bmi     DESTROY_SPRITE
                FREE_HARDWARE_SPRITE
                ENDC
                bra     DESTROY_SPRITE

.method_animate_stun_left:
                moveq   #ENEMY_STATUS_STUN_LEFT,d1
                move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                subq.w  #1,THIS_SPRITE_STUN_TIMER(a2)
                bmi     .method_set_run_left
                ENEMY_SHAKE
                bra     ANIMATE

.method_animate_stun_right:
                moveq   #ENEMY_STATUS_STUN_RIGHT,d1
                move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                subq.w  #1,THIS_SPRITE_STUN_TIMER(a2)
                bmi     .method_set_run_right
                ENEMY_SHAKE
                bra     ANIMATE


;------------------------------------------------------------------------------------
; LAND_LEFT
;------------------------------------------------------------------------------------
.method_set_land_left:
                move.w  d4,SPRITE_PLOT_YPOS(a1)
                move.w  #MONKEY_LAND_DELAY,PRIVATE_MONKEY_TIMER(a2)
                move.w  #MONKEY_LAND_LEFT,THIS_SPRITE_STATUS(a2)
                move.w  #MONKEY_MOVE_SPEED,PRIVATE_MONKEY_MOVE_SPEED(a2)  ; Set move speed.
                move.w  SPRITE_PLOT_YPOS(a1),d2
                and.w   #$fff0,d2
                addq.w  #2,d2
                move.w  d2,SPRITE_PLOT_YPOS(a1)
		move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                bra     .exit

.method_animate_land_left:
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.l  #ENEMY_DEF_MONKEY,d7
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS

                subq.w  #1,PRIVATE_MONKEY_TIMER(a2)
                tst.w   PRIVATE_MONKEY_TIMER(a2)
                bmi     .method_animate_land_left_0
                moveq   #MONKEY_LAND_LEFT,d1
                bra     ANIMATE

.method_animate_land_left_0:
                tst.w   PRIVATE_MONKEY_IS_DESCENDING(a2)
                beq     .method_set_run_left
                bra     .method_set_run_direction

;------------------------------------------------------------------------------------
; LAND RIGHT
;------------------------------------------------------------------------------------
.method_set_land_right:
                move.w  d4,SPRITE_PLOT_YPOS(a1)
                move.w  #MONKEY_LAND_DELAY,PRIVATE_MONKEY_TIMER(a2)
                move.w  #MONKEY_LAND_RIGHT,THIS_SPRITE_STATUS(a2)
                move.w  #MONKEY_MOVE_SPEED,PRIVATE_MONKEY_MOVE_SPEED(a2)  ; Set move speed.
                move.w  SPRITE_PLOT_YPOS(a1),d2
                and.w   #$fff0,d2
                addq.w  #2,d2
                move.w  d2,SPRITE_PLOT_YPOS(a1)
		move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                bra     .exit

.method_animate_land_right:
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.l  #ENEMY_DEF_MONKEY,d7
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS

                subq.w  #1,PRIVATE_MONKEY_TIMER(a2)
                tst.w   PRIVATE_MONKEY_TIMER(a2)
                bmi     .method_animate_land_right_0
                moveq   #MONKEY_LAND_RIGHT,d1
                bra     ANIMATE

.method_animate_land_right_0:
                tst.w   PRIVATE_MONKEY_IS_DESCENDING(a2)
                beq     .method_set_run_right
                bra     .method_set_run_direction

                nop

; This method causes the Rider to run in the direction of Rygar.
.method_set_run_direction:
                move.w  RYGAR_XPOS(a4),d3
                cmp.w   SPRITE_PLOT_XPOS(a1),d3
                ble.s   .method_set_run_left
                bra     .method_set_run_right

;------------------------------------------------------------------------------------
; RUN LEFT
;------------------------------------------------------------------------------------

.method_set_run_left:
                move.w  #MONKEY_SPEED_INCREASE,PRIVATE_MONKEY_INCREMENT_SPEED(a2)
                move.w  #MONKEY_RUN_LEFT,THIS_SPRITE_STATUS(a2)
		move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                bra     .exit

.method_animate_run_left:
                subq.w  #1,PRIVATE_MONKEY_INCREMENT_SPEED(a2)
                tst.w   PRIVATE_MONKEY_INCREMENT_SPEED(a2)
                bpl.s   .method_animate_run_left_0
                move.w  #MONKEY_SPEED_INCREASE,PRIVATE_MONKEY_INCREMENT_SPEED(a2)
                addq.w  #1,PRIVATE_MONKEY_MOVE_SPEED(a2)

.method_animate_run_left_0:
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                sub.w   PRIVATE_MONKEY_MOVE_SPEED(a2),d6
                move.w  d6,d2

                move.w  SPRITE_PLOT_YPOS(a1),d7
                lsr.w   #4,d7
                move.w  d7,d3
                subq.w  #2,d7
; Check an upper obstacle
                moveq   #1,d5
                IS_PATH_DIFFERENT                       ;d6=xpos interpolated, d7=current platform
                tst.b   d6
                bmi     .method_set_leap_left
; Check platform
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                sub.w   PRIVATE_MONKEY_MOVE_SPEED(a2),d6

                move.w  d3,d7
                moveq   #0,d5
                IS_PATH_DIFFERENT                       ;d6=xpos interpolated, d7=current platform
                tst.b   d6
                bpl     .method_set_leap_left

                UPDATE_SPRITE_PLOT_XPOS
                moveq   #MONKEY_RUN_LEFT,d1
                bra     ANIMATE


;------------------------------------------------------------------------------------
; RUN RIGHT
;------------------------------------------------------------------------------------
.method_set_run_right:
                move.w  #MONKEY_SPEED_INCREASE,PRIVATE_MONKEY_INCREMENT_SPEED(a2)
                move.w  #MONKEY_RUN_RIGHT,THIS_SPRITE_STATUS(a2)
		move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                bra     .exit

.method_animate_run_right:
                subq.w  #1,PRIVATE_MONKEY_INCREMENT_SPEED(a2)
                tst.w   PRIVATE_MONKEY_INCREMENT_SPEED(a2)
                bpl.s   .method_animate_run_right_0
                move.w  #MONKEY_SPEED_INCREASE,PRIVATE_MONKEY_INCREMENT_SPEED(a2)
                addq.w  #1,PRIVATE_MONKEY_MOVE_SPEED(a2)

.method_animate_run_right_0:
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                add.w   PRIVATE_MONKEY_MOVE_SPEED(a2),d6
                move.w  d6,d2

                move.w  SPRITE_PLOT_YPOS(a1),d7
                lsr.w   #4,d7
                move.w  d7,d3
                subq.w  #2,d7
; Check an upper obstacle
                moveq   #-1,d5
                IS_PATH_DIFFERENT                       ;d6=xpos interpolated, d7=current platform
                tst.b   d6
                bmi     .method_set_leap_right
; Check platform
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                add.w   PRIVATE_MONKEY_MOVE_SPEED(a2),d6

                move.w  d3,d7
                moveq   #-1,d5
                IS_PATH_DIFFERENT                       ;d6=xpos interpolated, d7=current platform
                tst.b   d6
                bpl     .method_set_leap_right

                UPDATE_SPRITE_PLOT_XPOS
                moveq   #MONKEY_RUN_RIGHT,d1
                bra     ANIMATE

;------------------------------------------------------------------------------------
; LEAP LEFT
;------------------------------------------------------------------------------------
.method_set_leap_left:
                move.w  #MONKEY_LEAP_DELAY,PRIVATE_MONKEY_TIMER(a2)
                move.w  #MONKEY_LEAP_LEFT,THIS_SPRITE_STATUS(a2)
		move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                bra     .exit

.method_animate_leap_left:
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.l  #ENEMY_DEF_MONKEY,d7
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS

                subq.w  #1,PRIVATE_MONKEY_TIMER(a2)
                bmi     .method_set_jump_left
                moveq   #MONKEY_LEAP_LEFT,d1
                bra     ANIMATE

;------------------------------------------------------------------------------------
; LEAP RIGHT
;------------------------------------------------------------------------------------
.method_set_leap_right:
                move.w  #MONKEY_LEAP_DELAY,PRIVATE_MONKEY_TIMER(a2)
                move.w  #MONKEY_LEAP_RIGHT,THIS_SPRITE_STATUS(a2)
		move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                bra     .exit

.method_animate_leap_right:
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.l  #ENEMY_DEF_MONKEY,d7
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS

                subq.w  #1,PRIVATE_MONKEY_TIMER(a2)
                bmi     .method_set_jump_right
                moveq   #MONKEY_LEAP_RIGHT,d1
                bra     ANIMATE

;------------------------------------------------------------------------------------
; JUMP LEFT
;------------------------------------------------------------------------------------
.method_set_jump_left:
                clr.w   PRIVATE_MONKEY_IS_DESCENDING(a2)
                move.w  #MONKEY_JUMP_LEFT,THIS_SPRITE_STATUS(a2)
                clr.w   PRIVATE_MONKEY_SINE_POINTER(a2)
                move.w  SPRITE_PLOT_YPOS(a1),PRIVATE_MONKEY_FIXED_YPOS(a2)
		move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                bra     .exit

.method_animate_jump_left:
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                sub.w   #MONKEY_JUMP_SPEED,d6
                move.l  #ENEMY_DEF_MONKEY,d7
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS

                lea     MONKEY_JUMP_SINE(a4),a0
                move.w  PRIVATE_MONKEY_SINE_POINTER(a2),d2
                addq.w  #MONKEY_SINE_SPEED,PRIVATE_MONKEY_SINE_POINTER(a2)
                move.w  PRIVATE_MONKEY_FIXED_YPOS(a2),d4
                add.w   (a0,d2*2),d4                            ; Add sine(y)

                cmp.w   #64,d2                                  ; are we on descend curve
                ble.s   .method_animate_jump_left_1            ; No, don't check platforms...

		moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  SPRITE_PLOT_YPOS(a1),d7
		move.w	d7,d3
                lsr.w   #4,d7
		
                moveq   #0,d5
                IS_PATH_DIFFERENT                       ;d6=xpos interpolated, d7=current platform
                tst.b   d6
                bmi     .method_set_land_left

.method_animate_jump_left_1:
		cmp.w	#226,d4
		bgt	.method_destroy_sprite

                move.w  d4,SPRITE_PLOT_YPOS(a1)
                moveq   #MONKEY_JUMP_LEFT,d1
                bra     ANIMATE


;------------------------------------------------------------------------------------
; JUMP RIGHT
;------------------------------------------------------------------------------------
.method_set_jump_right:
                clr.w   PRIVATE_MONKEY_IS_DESCENDING(a2)
                move.w  #MONKEY_JUMP_RIGHT,THIS_SPRITE_STATUS(a2)
                clr.w   PRIVATE_MONKEY_SINE_POINTER(a2)
                move.w  SPRITE_PLOT_YPOS(a1),PRIVATE_MONKEY_FIXED_YPOS(a2)
		move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                bra     .exit

.method_animate_jump_right:
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                add.w   #MONKEY_JUMP_SPEED,d6
                move.l  #ENEMY_DEF_MONKEY,d7
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS

                lea     MONKEY_JUMP_SINE(a4),a0
                move.w  PRIVATE_MONKEY_SINE_POINTER(a2),d2
                addq.w  #MONKEY_SINE_SPEED,PRIVATE_MONKEY_SINE_POINTER(a2)
                move.w  PRIVATE_MONKEY_FIXED_YPOS(a2),d4
                add.w   (a0,d2*2),d4                            ; Add sine(y)
                cmp.w   #64,d2                                  ; are we on descend curve
                ble.s   .method_animate_jump_right_1            ; No, don't check platforms...

                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  SPRITE_PLOT_YPOS(a1),d7         ; Y Position
                lsr.w   #4,d7                           ; Divide by 16
                moveq   #-1,d5
                IS_PATH_DIFFERENT                       ;d6=xpos interpolated, d7=current platform
                tst.b   d6
                bmi     .method_set_land_right

.method_animate_jump_right_1:
		cmp.w	#224,d4
		bgt	.method_destroy_sprite
		
                move.w  d4,SPRITE_PLOT_YPOS(a1)
                moveq   #MONKEY_JUMP_RIGHT,d1
                bra     ANIMATE


.set_update:

.exit:          rts
