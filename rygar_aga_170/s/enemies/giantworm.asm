
GIANTWORM_GROUND_ENEMY:         equ     1       ; 1 = Ground enemy means enemy is stunnable.
GIANTWORM_HWSPR_ENABLE:         equ     0
GIANTWORM_HWSPR_UPPER:          equ     0
GIANTWORM_HWSPR_MID:            equ     0
GIANTWORM_HWSPR_LOWER:          equ     0

GIANTWORM_TO_RYGAR_X1_BOUND:    equ     6
GIANTWORM_TO_RYGAR_Y1_BOUND:    equ     2
GIANTWORM_TO_RYGAR_X2_BOUND:    equ     25
GIANTWORM_TO_RYGAR_Y2_BOUND:    equ     31

GIANTWORM_TO_DISKARM_X1_BOUND:  equ     0
GIANTWORM_TO_DISKARM_Y1_BOUND:  equ     4
GIANTWORM_TO_DISKARM_X2_BOUND:  equ     32
GIANTWORM_TO_DISKARM_Y2_BOUND:  equ     32

GIANTWORM_MOVE_SPEED:           equ     16

GIANTWORM_STATUS_MOVE_LEFT:     equ     6                       ; Sprite comes in moving left from right
GIANTWORM_STATUS_MOVE_RIGHT     equ     7                       ; Sprite comes in moving right from left
GIANTWORM_STATUS_SURFACE:       equ     8                       ; Sprite moves to the surface (two stages)
GIANTWORM_STATUS_BOUNCE:        equ     9                       ; Sprite bounces at the surface
GIANTWORM_STATUS_BOUNCE_LEFT:   equ     10
GIANTWORM_STATUS_BOUNCE_RIGHT:  equ     11
GIANTWORM_STATUS_DESCEND:       equ     12

GIANTWORM_BOUNCE_TIME:          equ     60                      ; 60 frames = 1 second bounce time.
GIANTWORM_SURFACE_TIME:         equ     60*5

PRIVATE_GIANTWORM_SPEED:        equ     32
PRIVATE_GIANTWORM_FRAMECOUNT:   equ     36
PRIVATE_GIANTWORM_FIXED_YPOS:   equ     38
PRIVATE_GIANTWORM_SINE_POINTER  equ     40
PRIVATE_GIANTWORM_SURFACE_POINT:        equ     42
PRIVATE_GIANTWORM_TIMER:        equ     44
PRIVATE_GIANTWORM_BOUNCE_TIMER: equ     46
PRIVATE_GIANTWORM_DESCEND:      equ     48
PRIVATE_GIANTWORM_BOUNDARY_TIMER:       equ     50




HDL_GIANTWORM:
		FUNCID	#$6874dbd7
                SPRITE_INITIATE

; Set the initial variables and status of the sprite.
.construct:
		clr.w	2(a1)
		clr.w	4(a1)
		
                move.w  MAP_PIXEL_POSX(a4),d2
                add.w   #256,d2
                move.w  d2,SPRITE_PLOT_XPOS(a1)
                lsl.w   #4,d2                           ; Based on the Plot position after
                move.w  d2,THIS_SPRITE_MAP_XPOS(a2)             ; Enemy is Spawned.

                moveq   #0,d2
                move.b  $bfd800,d2
                move.w  d2,PRIVATE_GIANTWORM_SURFACE_POINT(a2)
                clr.w   PRIVATE_GIANTWORM_DESCEND(a2)
		
		move.w	ROUND_CURRENT(a4),d2			; 1
		lsl.w	#1,d2					; 2
		
		move.w	THIS_SPRITE_PARAMETER(a2),d3		; 0
		bne.s	.pset					; no
		moveq	#GIANTWORM_MOVE_SPEED,d3		; 32
		
.pset:		add.w	d3,d2					;34
		move.w  d2,PRIVATE_GIANTWORM_SPEED(a2)       ; Set move speed.
                
		move.w  #GIANTWORM_STATUS_MOVE_LEFT,THIS_SPRITE_STATUS(a2)
		move.w  #192,SPRITE_PLOT_YPOS(a1)                       ; set mid area
		move.w	#SPR_TYPE_ENEMY_2X32,SPRITE_PLOT_TYPE(a1)
;;; Init code here.
		move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS

.active:
		ENEMY_CHECK_END_OF_ROUND

; Check boundary of sprite!
.sprite_0:	ENEMY_BOUNDARY_CHECK
		
		bra.s   .sprite_1

.animate_table: dc.l    .method_animate_none-.animate_table      	; 0
                dc.l    .method_animate_bones-.animate_table    	; 1
                dc.l    .method_animate_sweep_left-.animate_table       ; 2
                dc.l    .method_animate_sweep_right-.animate_table      ; 3
                dc.l    .method_animate_stun_left-.animate_table       	; 4                         ; 4 Reserved for Stun
                dc.l    .method_animate_stun_right-.animate_table       ; 5                     ; 5 Reserved for Stun
                dc.l    .method_animate_move_left-.animate_table        ; 6
                dc.l    .method_animate_move_right-.animate_table       ; 7
                dc.l    .method_animate_surface-.animate_table  	; 8
                dc.l    .method_animate_bounce-.animate_table   	; 9
                dc.l    .method_animate_bounce_left-.animate_table      ; 10
                dc.l    .method_animate_bounce_right-.animate_table     ; 11
                dc.l    .method_animate_descend-.animate_table  	; 12
                dc.l    .method_animate_none-.animate_table      	; 13
                dc.l    .method_animate_none-.animate_table      	; 14
                dc.l    .method_animate_none-.animate_table      	; 15
                dc.l    .method_animate_none-.animate_table      	; 16
                dc.l    .method_animate_none-.animate_table      	; 17
                dc.l    .method_animate_none-.animate_table      	; 18
                dc.l    .method_animate_none-.animate_table      	; 19
                dc.l    .method_animate_none-.animate_table      	; 20
                dc.l    .method_animate_none-.animate_table      	; 21
                dc.l    .method_animate_none-.animate_table      	; 22
                dc.l    .method_animate_none-.animate_table      	; 23
                dc.l    .method_animate_none-.animate_table      	; 24
                dc.l    .method_animate_none-.animate_table      	; 25
		
.method_animate_none:	bra	.exit

.sprite_1:      cmp.w   #ENEMY_STATUS_SWEEP_RIGHT,THIS_SPRITE_STATUS(a2)                ; if doing bones or sweeps then skip all collisions
                ble     .run_sprite_method
		
		tst.w	SHIELD_TIMER
		bpl.s	.shield_on
                cmp.w   #ENEMY_STATUS_STUN_RIGHT,THIS_SPRITE_STATUS(a2)                 ; if doing stuns then skip Rygar collision
                ble     .sprite_stunned

.shield_on:

;Test this enemy collided with Rygar (the player)
.sprite_2:	move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4                                   ; Check a 32x32 boundary from x1/y1
                move.w  d3,d5
                add.w   #GIANTWORM_TO_RYGAR_X1_BOUND,d2
                add.w   #GIANTWORM_TO_RYGAR_Y1_BOUND,d3
                add.w   #GIANTWORM_TO_RYGAR_X2_BOUND,d4
                add.w   #GIANTWORM_TO_RYGAR_Y2_BOUND,d5
                lea     RYGAR_COORDS(a4),a3
                ENEMY_TO_RYGAR_COLLISION
                move.l  #POINTS_GIANTWORM,d2
                tst.w   d6                                      ; Did this sprite collide with Rygar?
                IFEQ    DISABLE_ENEMY_COLLISION
                bmi     .method_set_collision_action            ; Yes
                ENDC

; Check if the enemy has been hit with disk armor
.sprite_stunned:
                move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4                                   ; Check a 32x32 boundary from x1/y1
                move.w  d3,d5
                add.w   #GIANTWORM_TO_DISKARM_X1_BOUND,d2
                add.w   #GIANTWORM_TO_DISKARM_Y1_BOUND,d3
                add.w   #GIANTWORM_TO_DISKARM_X2_BOUND,d4
                add.w   #GIANTWORM_TO_DISKARM_Y2_BOUND,d5
                lea     DISKARM_COORDS(a4),a3
                ENEMY_TO_DISKARM_COLLISION
                move.l  #POINTS_GIANTWORM,d2
                tst.w   d6                                      ; Did this sprite get hit with the disk armor
                bmi     .method_set_sweep_direction             ; Yes

; Test this enemy collided with the disk armour

                RUN_SPRITE_METHOD

                METHOD_ANIMATE_BONES

                METHOD_ANIMATE_SWEEP_LEFT

                METHOD_ANIMATE_SWEEP_RIGHT

                METHOD_SET_BONES

                METHOD_SET_SWEEP_DIRECTION

                METHOD_SET_STUN_OR_DESTROY

.method_set_stun_direction:
                move.w  #146,SPRITE_PLOT_YPOS(a1)
                move.w  #TIGER_STUN_STATIC_TIME,THIS_SPRITE_STUN_TIMER(a2)              ; Set timer
                move.w	ROUND_CURRENT(a4),d2
		lsl.w	#2,d2
		sub.w	d2,THIS_SPRITE_STUN_TIMER(a2)
		
		tst.w   THIS_SPRITE_DIRECTION(a2)
                bmi.s   .method_set_stun_left
                bra.s   .method_set_stun_right

.method_set_stun_left:
                move.w  #SPRITE_MOVE_LEFT,THIS_SPRITE_DIRECTION(a2)
                move.w  #ENEMY_STATUS_STUN_LEFT,THIS_SPRITE_STATUS(a2)
                bra     .set_update

.method_set_stun_right:
                move.w  #SPRITE_MOVE_RIGHT,THIS_SPRITE_DIRECTION(a2)
                move.w  #ENEMY_STATUS_STUN_RIGHT,THIS_SPRITE_STATUS(a2)
                bra     .set_update

                METHOD_SET_RYGAR_DEATH_SEQUENCE


.method_set_collision_action:
                tst.w   RYGAR_FALLPREV_STATE(a4)                                ; Is Rygar falling on enemy
                bmi     .method_set_stun_or_destroy                     ; Yes!

		tst.w	SHIELD_TIMER
		bpl.s	.shield_on_1

                cmp.w   #GIANTWORM_STATUS_BOUNCE,THIS_SPRITE_STATUS(a2)
                beq     .sprite_stunned
                cmp.w   #GIANTWORM_STATUS_SURFACE,THIS_SPRITE_STATUS(a2)
                beq     .sprite_stunned

.shield_on_1:
                IFNE    DISABLE_ENEMY_COLLISION
                bra     .exit
                ELSE
                bra     .method_set_rygar_death_sequence
                ENDC


.method_destroy_sprite: 
		subq.w  #1,GIANTWORMS_IN_USE(a4)
                clr.w   THIS_SPRITE_STATUS(a2)
                IFNE    GIANTWORM_HWSPR_ENABLE
                IFNE    GIANTWORM_HWSPR_UPPER
                lea     ENEMY_HWSPR_UPPER_ALLOCATED(a4),a0                      ; Free up hardware sprite
                ENDC
                IFNE    GIANTWORM_HWSPR_MID
                lea     ENEMY_HWSPR_MID_ALLOCATED(a4),a0                        ; Free up hardware sprite
                ENDC
                IFNE    GIANTWORM_HWSPR_LOWER
                lea     ENEMY_HWSPR_LOWER_ALLOCATED(a4),a0                      ; Free up hardware sprite
                ENDC
                move.w  THIS_SPRITE_HWSLOT(a2),d7
                bmi     DESTROY_SPRITE
                FREE_HARDWARE_SPRITE
                ENDC
                bra     DESTROY_SPRITE

.method_animate_stun_left:
                moveq   #ENEMY_STATUS_STUN_LEFT,d1
                move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                subq.w  #1,THIS_SPRITE_STUN_TIMER(a2)
                bmi     .method_set_run_left
                ENEMY_SHAKE
                bra     ANIMATE

.method_animate_stun_right:
                moveq   #ENEMY_STATUS_STUN_RIGHT,d1
                move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                subq.w  #1,THIS_SPRITE_STUN_TIMER(a2)
                bmi     .method_set_run_right
                ENEMY_SHAKE
                bra     ANIMATE

;------------------------------------------------------------------------------------
; MOVE LEFT
;------------------------------------------------------------------------------------
.method_set_move_left:
                move.w  #SPRITE_MOVE_LEFT,THIS_SPRITE_DIRECTION(a2)
                move.w  #GIANTWORM_STATUS_MOVE_LEFT,THIS_SPRITE_STATUS(a2)
		move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                bra     .exit

.method_animate_move_left:
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6

                tst.w   PRIVATE_GIANTWORM_DESCEND(a2)
                bmi.s   .method_animate_move_left_0
                move.w  PRIVATE_GIANTWORM_SURFACE_POINT(a2),d2
                cmp.w   SPRITE_PLOT_XPOS(a1),d2                         ; Need this to be a range.
                bge     .method_set_surface
.method_animate_move_left_0:
		move.w	PRIVATE_GIANTWORM_SPEED(a2),d5
                sub.w   d5,d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS
                moveq   #GIANTWORM_STATUS_MOVE_LEFT,d1
                bra     ANIMATE


;------------------------------------------------------------------------------------
; MOVE RIGHT
;------------------------------------------------------------------------------------
.method_set_move_right:
                move.w  #SPRITE_MOVE_RIGHT,THIS_SPRITE_DIRECTION(a2)
                move.w  #GIANTWORM_STATUS_MOVE_RIGHT,THIS_SPRITE_STATUS(a2)
		move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                bra     .exit

.method_animate_move_right:
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                tst.w   PRIVATE_GIANTWORM_DESCEND(a2)
                bmi.s   .method_animate_move_right_0
                move.w  PRIVATE_GIANTWORM_SURFACE_POINT(a2),d2
                cmp.w   SPRITE_PLOT_XPOS(a1),d2                         ; Need this to be a range.
                ble.s   .method_set_surface
.method_animate_move_right_0:
		move.w	PRIVATE_GIANTWORM_SPEED(a2),d5
                add.w   d5,d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS

                moveq   #GIANTWORM_STATUS_MOVE_RIGHT,d1
                bra     ANIMATE


;------------------------------------------------------------------------------------
; SURFACE
;------------------------------------------------------------------------------------
.method_set_surface:
                move.w  #GIANTWORM_STATUS_SURFACE,THIS_SPRITE_STATUS(a2)
                clr.w   PRIVATE_GIANTWORM_TIMER(a2)
		move.l	d0,-(a7)
		moveq	#SND_LAVARISE,d0
		bsr	PLAY_SAMPLE
		move.l	(a7)+,d0
                bra     .exit

.method_animate_surface:
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6

                cmp.w   #50,PRIVATE_GIANTWORM_TIMER(a2)
                beq     .method_set_bounce
                addq.w  #1,PRIVATE_GIANTWORM_TIMER(a2)
                cmp.w   #22,PRIVATE_GIANTWORM_TIMER(a2)
                bne.s   .method_animate_surface_0
                move.w  #146,SPRITE_PLOT_YPOS(a1)

.method_animate_surface_0:
; Modify d2 to move sprite.
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS
                moveq   #GIANTWORM_STATUS_SURFACE,d1
                bra     ANIMATE


;------------------------------------------------------------------------------------
; DESCEND
;------------------------------------------------------------------------------------
.method_set_descend:
                move.w  #GIANTWORM_STATUS_DESCEND,THIS_SPRITE_STATUS(a2)
                clr.w   PRIVATE_GIANTWORM_TIMER(a2)
                move.w  #-1,PRIVATE_GIANTWORM_DESCEND(a2)
                move.w  #146,SPRITE_PLOT_YPOS(a1)
		move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                bra     .exit

.method_animate_descend:
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                cmp.w   #50,PRIVATE_GIANTWORM_TIMER(a2)
                beq     .method_set_move_left
                addq.w  #1,PRIVATE_GIANTWORM_TIMER(a2)

                cmp.w   #20,PRIVATE_GIANTWORM_TIMER(a2)
                bne.s   .method_animate_descend_0

                move.w  #192,SPRITE_PLOT_YPOS(a1)

.method_animate_descend_0:
; Modify d2 to move sprite.
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS
                moveq   #GIANTWORM_STATUS_DESCEND,d1
                bra     ANIMATE


;------------------------------------------------------------------------------------
; BOUNCE
;------------------------------------------------------------------------------------
.method_set_bounce:
                move.w  #GIANTWORM_STATUS_BOUNCE,THIS_SPRITE_STATUS(a2)
                move.w  SPRITE_PLOT_YPOS(a1),PRIVATE_GIANTWORM_FIXED_YPOS(a2)
                clr.w   PRIVATE_GIANTWORM_SINE_POINTER(a2)
		
		moveq	#0,d2
		move.b  $bfd800,d2
		and.w	#$f,d2
		move.w	d2,PRIVATE_GIANTWORM_SINE_POINTER(a2)
		
                move.w  #GIANTWORM_SURFACE_TIME,PRIVATE_GIANTWORM_BOUNCE_TIMER(a2)
		move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                bra     .exit

.method_animate_bounce:
                tst.w   PRIVATE_GIANTWORM_TIMER(a2)
                bmi.s   .method_set_bounce_left
                subq.w  #1,PRIVATE_GIANTWORM_TIMER(a2)

                lea     GIANTWORM_BOUNCE_SINE(a4),a0
                move.w  PRIVATE_GIANTWORM_SINE_POINTER(a2),d2
                move.w  (a0,d2*2),d3
                cmp.w   #$8000,d3
                bne.s   .method_animate_bounce_0
                clr.w   PRIVATE_GIANTWORM_SINE_POINTER(a2)
                moveq   #0,d3
.method_animate_bounce_0:
                add.w   PRIVATE_GIANTWORM_FIXED_YPOS(a2),d3
                move.w  d3,SPRITE_PLOT_YPOS(a1)
                addq.w  #1,PRIVATE_GIANTWORM_SINE_POINTER(a2)

                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
; Modify d2 to move sprite.
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS

                moveq   #GIANTWORM_STATUS_BOUNCE,d1
                bra     ANIMATE


;------------------------------------------------------------------------------------
; BOUNCE LEFT
;------------------------------------------------------------------------------------
.method_set_run_left:
.method_set_bounce_left:
                move.w  #SPRITE_MOVE_LEFT,THIS_SPRITE_DIRECTION(a2)
                move.w  #GIANTWORM_STATUS_BOUNCE_LEFT,THIS_SPRITE_STATUS(a2)
		move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                bra     .exit

.method_animate_bounce_left:
                tst.w   PRIVATE_GIANTWORM_BOUNCE_TIMER(a2)
                bmi     .method_set_descend
                subq.w  #1,PRIVATE_GIANTWORM_BOUNCE_TIMER(a2)

                lea     GIANTWORM_BOUNCE_SINE(a4),a0
                move.w  PRIVATE_GIANTWORM_SINE_POINTER(a2),d2
                move.w  (a0,d2*2),d3
                cmp.w   #$8000,d3
                bne.s   .method_animate_bounce_left_0
                clr.w   PRIVATE_GIANTWORM_SINE_POINTER(a2)
                moveq   #0,d3
.method_animate_bounce_left_0:
                add.w   PRIVATE_GIANTWORM_FIXED_YPOS(a2),d3
                move.w  d3,SPRITE_PLOT_YPOS(a1)
                addq.w  #1,PRIVATE_GIANTWORM_SINE_POINTER(a2)

                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
		move.w	PRIVATE_GIANTWORM_SPEED(a2),d5
                sub.w   d5,d6
                move.w  d6,d2

                move.w  SPRITE_PLOT_YPOS(a1),d7
                lsr.w   #4,d7
                move.w  d7,d3
                subq.w  #1,d7
; Check an upper obstacle
                moveq   #1,d5
                IS_PATH_DIFFERENT                       ;d6=xpos interpolated, d7=current platform
                tst.b   d6
                bmi     .method_set_bounce_right

                UPDATE_SPRITE_PLOT_XPOS

                cmp.w   #1,SPRITE_PLOT_XPOS(a1)
                ble     .method_set_bounce_right

                moveq   #GIANTWORM_STATUS_BOUNCE_LEFT,d1
                bra     ANIMATE

;------------------------------------------------------------------------------------
; BOUNCE RIGHT
;------------------------------------------------------------------------------------
.method_set_run_right:
.method_set_bounce_right:
                move.w  #SPRITE_MOVE_RIGHT,THIS_SPRITE_DIRECTION(a2)
                move.w  #GIANTWORM_STATUS_BOUNCE_RIGHT,THIS_SPRITE_STATUS(a2)
		move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                bra     .exit

.method_animate_bounce_right:
                tst.w   PRIVATE_GIANTWORM_BOUNCE_TIMER(a2)
                bmi     .method_set_descend
                subq.w  #1,PRIVATE_GIANTWORM_BOUNCE_TIMER(a2)

                lea     GIANTWORM_BOUNCE_SINE(a4),a0
                move.w  PRIVATE_GIANTWORM_SINE_POINTER(a2),d2
                move.w  (a0,d2*2),d3
                cmp.w   #$8000,d3
                bne.s   .method_animate_bounce_right_0
                clr.w   PRIVATE_GIANTWORM_SINE_POINTER(a2)
                moveq   #0,d3
.method_animate_bounce_right_0:
                add.w   PRIVATE_GIANTWORM_FIXED_YPOS(a2),d3
                move.w  d3,SPRITE_PLOT_YPOS(a1)
                addq.w  #1,PRIVATE_GIANTWORM_SINE_POINTER(a2)

                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
		move.w	PRIVATE_GIANTWORM_SPEED(a2),d5
                add.w   d5,d6
                move.w  d6,d2

                move.w  SPRITE_PLOT_YPOS(a1),d7
                lsr.w   #4,d7
                move.w  d7,d3
                subq.w  #1,d7
; Check an upper obstacle
                moveq   #0,d5
                IS_PATH_DIFFERENT                       ;d6=xpos interpolated, d7=current platform
                tst.b   d6
                bmi     .method_set_bounce_left

                UPDATE_SPRITE_PLOT_XPOS

                cmp.w   #288,SPRITE_PLOT_XPOS(a1)
                bge     .method_set_bounce_left

                moveq   #GIANTWORM_STATUS_BOUNCE_RIGHT,d1
                bra     ANIMATE

.set_update:

.exit:          rts
