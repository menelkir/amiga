ENEMY_BIGRHINO1_XPOS:           equ     500
ENEMY_BIGRHINO1_YPOS:           equ     146

BIGRHINO1_GROUND_ENEMY:         equ     1       ; 1 = Ground enemy means enemy is stunnable.
BIGRHINO1_HWSPR_ENABLE:         equ     1
BIGRHINO1_HWSPR_UPPER:          equ     0
BIGRHINO1_HWSPR_MID:            equ     1
BIGRHINO1_HWSPR_LOWER:          equ     0

BIGRHINO1_WALK_SPEED:           equ     12
BIGRHINO1_STATUS_WALK_LEFT:     equ     6
BIGRHINO1_STATUS_WALK_RIGHT     equ     7

BIGRHINO1_GO_LEFT:                      equ     -1
BIGRHINO1_GO_RIGHT:                     equ     0

; Private Variables
PRIVATE_BIGRHINO1_SPEED:                        equ     32
PRIVATE_BIGRHINO1_FIXED_YPOS:           equ     34      ; Y Position of Sprite.
PRIVATE_BIGRHINO1_DIRECTION:            equ     38      ; Current FLYing direction.
PRIVATE_BIGRHINO1_XPOS:                 equ     40
PRIVATE_BIGRHINO1_YPOS:                 equ     42
PRIVATE_BIGRHINO1_ANIM_FRAME:           equ     44
PRIVATE_BIGRHINO1_BOUNDARY_TIMER:       equ     46
PRIVATE_BIGRHINO1_HIT_COUNT:		equ	48

BIGRHINO1_TO_RYGAR_X1_BOUND:    equ     0
BIGRHINO1_TO_RYGAR_Y1_BOUND:    equ     0
BIGRHINO1_TO_RYGAR_X2_BOUND:    equ     64
BIGRHINO1_TO_RYGAR_Y2_BOUND:    equ     32

BIGRHINO1_TO_DISKARM_X1_BOUND:  equ     0
BIGRHINO1_TO_DISKARM_Y1_BOUND:  equ     4
BIGRHINO1_TO_DISKARM_X2_BOUND:  equ     64
BIGRHINO1_TO_DISKARM_Y2_BOUND:  equ     32

BIGRHINO1_HIT_POINTS:		equ	8

HDL_BIGRHINO1:
		FUNCID	#$5a212c90
                SPRITE_INITIATE

; Set the initial variables and status of the sprite.
.construct:
                FIND_CAMERA_XPOS

.create_right:  move.w  #BIGRHINO1_STATUS_WALK_LEFT,THIS_SPRITE_STATUS(a2)      ; Create to the left, FLY right
                move.w  #BIGRHINO1_GO_LEFT,PRIVATE_BIGRHINO1_DIRECTION(a2)
.create:
; d7 = spawn x position
                move.w  d7,THIS_SPRITE_MAP_XPOS(a2)                     ; Set start position in the map.
                move.w  #BIGRHINO1_WALK_SPEED,PRIVATE_BIGRHINO1_SPEED(a2)
                move.w  #ENEMY_BOUNDARY_TIME,PRIVATE_BIGRHINO1_BOUNDARY_TIMER(a2)
                move.w  #ENEMY_BIGRHINO1_YPOS,PRIVATE_BIGRHINO1_FIXED_YPOS(a2)
		move.w  #BIGRHINO1_HIT_POINTS,PRIVATE_BIGRHINO1_HIT_COUNT(a2)

		lea	ENEMY_HWSPR_MID_ALLOCATED(a4),a3

.bank_0:
		cmp.w	2(a3),d0
		beq.s	.alloc_bank_0
		cmp.w	10(a3),d0
		beq.s	.alloc_bank_1
		move.w	#-1,THIS_SPRITE_HWSLOT(a2)		; Needs a bob
		bra.s	.active					; Neither sprites were allocated, needs to be a bob
		
.alloc_bank_0:	
                move.w  (a3),SPRITE_PLOT_TYPE(a1)                               ; Set hardware type.
		move.w	#0,THIS_SPRITE_HWSLOT(a2)				; Bank 0 allocated
		bra.s	.active
		
.alloc_bank_1:
		move.w	8(a3),SPRITE_PLOT_TYPE(a1)
		move.w	#2,THIS_SPRITE_HWSLOT(a2)
		bra.s	.active
		nop
		
.active:        
	tst.w   SPRITE_DESTROY_END_ROUND(a2)            ; Is the sprite set destroyed at the end of the round?
                beq.s   .sprite_0                               ; No, so carry on
                clr.w   SPRITE_DESTROY_END_ROUND(a2)            ; Yes, so set the sweep direction to destroy the enemy.	
		bra     .method_set_bones

.sprite_0:      cmp.w   #ENEMY_STATUS_BONES,THIS_SPRITE_STATUS(a2)
                beq     .method_animate_bones

                move.w  d1,PRIVATE_BIGRHINO1_ANIM_FRAME(a2)

; Check boundary of sprite!
                moveq   #0,d2
                move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                lea     BOUNDARY_X1POS(a4),a0
		cmp.l	(a0),d2
		blt.s	.dectimer
		cmp.l	4(a0),d2
		bgt.s	.dectimer
		bra.s	.inbound
			
.dectimer:
                subq.w  #1,PRIVATE_BIGRHINO1_BOUNDARY_TIMER(a2)
                bpl.s   .outbound
                bra     .method_set_bones

.inbound:       move.w  #ENEMY_BOUNDARY_TIME,PRIVATE_BIGRHINO1_BOUNDARY_TIMER(a2)
.outbound:
                bra.s   .sprite_1
		
.animate_table: dc.l    .method_destroy_sprite-.animate_table           ; 0
                dc.l    .method_animate_bones-.animate_table            ; 1
                dc.l    .method_animate_none-.animate_table      	; 2
	        dc.l    .method_animate_none-.animate_table      	; 3
		dc.l    .method_animate_none-.animate_table      	; 4
	        dc.l    .method_animate_none-.animate_table      	; 5
                dc.l    .method_animate_walk_left-.animate_table        ; 6
                dc.l    .method_animate_walk_right-.animate_table       ; 7
		dc.l    .method_animate_none-.animate_table      	; 8		
		dc.l    .method_animate_none-.animate_table      	; 9
                dc.l    .method_animate_none-.animate_table      	; 10
                dc.l    .method_animate_none-.animate_table      	; 11
                dc.l    .method_animate_none-.animate_table      	; 12
                dc.l    .method_animate_none-.animate_table      	; 13
                dc.l    .method_animate_none-.animate_table      	; 14
                dc.l    .method_animate_none-.animate_table      	; 15
                dc.l    .method_animate_none-.animate_table      	; 16
                dc.l    .method_animate_none-.animate_table      	; 17
                dc.l    .method_animate_none-.animate_table      	; 18
                dc.l    .method_animate_none-.animate_table      	; 19
                dc.l    .method_animate_none-.animate_table      	; 20
                dc.l    .method_animate_none-.animate_table      	; 21
                dc.l    .method_animate_none-.animate_table      	; 22
                dc.l    .method_animate_none-.animate_table      	; 23
                dc.l    .method_animate_none-.animate_table      	; 24
		
.method_animate_none:	bra	.exit

.sprite_1:


.sprite_2:      move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4
                move.w  d3,d5
                add.w   #BIGRHINO1_TO_RYGAR_X1_BOUND,d2
                add.w   #BIGRHINO1_TO_RYGAR_Y1_BOUND,d3
                add.w   #BIGRHINO1_TO_RYGAR_X2_BOUND,d4
                add.w   #BIGRHINO1_TO_RYGAR_Y2_BOUND,d5
                lea     RYGAR_COORDS(a4),a3
                ENEMY_TO_RYGAR_COLLISION
                move.l  #POINTS_BIGRHINO,d2
                tst.w   d6                                      ; Did this sprite collide with Rygar?
                IFEQ    DISABLE_ENEMY_COLLISION
                bmi     .method_set_collision_action            ; Yes
                ENDC

; Check if the enemy has been hit with disk armor
                move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4
                move.w  d3,d5
                add.w   #BIGRHINO1_TO_DISKARM_X1_BOUND,d2
                add.w   #BIGRHINO1_TO_DISKARM_Y1_BOUND,d3
                add.w   #BIGRHINO1_TO_DISKARM_X2_BOUND,d4
                add.w   #BIGRHINO1_TO_DISKARM_Y2_BOUND,d5
                lea     DISKARM_COORDS(a4),a3
                ENEMY_TO_DISKARM_COLLISION
                move.l  #POINTS_BIGRHINO,d2
                tst.w   d6                                      ; Did this sprite get hit with the disk armor
                bmi     .method_add_points_and_destroy                  ; Yes

                RUN_SPRITE_METHOD


		
                METHOD_ANIMATE_BONES

                ;METHOD_ANIMATE_SWEEP_LEFT

                ;METHOD_ANIMATE_SWEEP_RIGHT

                ;METHOD_SET_SWEEP_DIRECTION

                ;METHOD_SET_JUMP_OR_DESTROY
		
.method_set_jump_or_destroy:
                move.l  d0,-(a7)
                moveq   #SND_LAND_ON_ENEMY,d0
                bsr     PLAY_SAMPLE
                move.l  (a7)+,d0

; Reset Roll Fall frames
		movem.l	d0-d1,-(a7)
		moveq	#SPR_RYGAR,d0
		moveq	#RYGAR_ROLLFALL_LEFT,d1
		bsr	RESET_SPRITE_FRAMES
		moveq	#SPR_RYGAR,d0
		moveq	#RYGAR_ROLLFALL_RIGHT,d1
		bsr	RESET_SPRITE_FRAMES
		movem.l	(a7)+,d0-d1
		
		move.w	#-1,RYGAR_ROLLFALL(a4)
		
		move.w	#HITPOINT_DELAY,HITPOINT_FRAMES(a4)

                clr.w   RYGAR_FALL_STATE(a4)                            ; Stop Falling
                move.w  #-1,RYGAR_JUMP_STATE(a4)                            ; Start Jumping
                clr.w   RYGAR_SINE_INDEX(a4)                            ; Reset Jump Sine pointer.
                GET_PLAYER_SPRITE_CONTEXT
                move.w  #SPRITE_INSTATE_JUMPING,THIS_SPRITE_STATUS(a0)  ; Set to jumping.
; Check here if Rygar has Tiger power.

                move.w  RYGAR_CURRENT_POWERS(a4),d2
                and.w   #POWER_TIGER,d2
                bne     .method_set_bones                     ; If Rygar has Tiger then enemy is destroy
                bra     .big_rhino_force_back                     ; If not then the enemy is stunned.
		
                METHOD_SET_RYGAR_DEATH_SEQUENCE

.method_set_sweep_direction:
.method_add_points_and_destroy:
		tst.w	STATE_KILL_ALL_ENEMIES(a4)
		bmi.s	.xxx
		move.w	#-1,DISKARM_DISABLE(a4)	
.xxx
		tst.w	ITEM_KILLALL_SET(a4)
		bmi	.method_set_bones
			
                move.l  d0,-(a7)
                moveq   #SND_STONE_HIT,d0
                bsr     PLAY_SAMPLE
                move.l  (a7)+,d0
		
		moveq	#1,d3
		move.w  RYGAR_CURRENT_POWERS(a4),d4
                and.w   #POWER_CROWN,d4
                beq     .has_crown                     ; If Rygar has Crown then the Dragon is killed
		moveq	#3,d3
.has_crown:               		
                sub.w  	d3,PRIVATE_BIGRHINO1_HIT_COUNT(a2)
	
		move.w	#HITPOINT_DELAY,HITPOINT_FRAMES(a4)		
		tst.w	PRIVATE_BIGRHINO1_HIT_COUNT(a2)
		beq.s	.big_rhino_force_back
		bpl.s	.big_rhino_force_back
	
                add.l   d2,VAL_PLAYER1_SCORE(a4)
                move.w  #-1,UPDATE_SCORE(a4)
                addq.w  #1,REPULSE_BONUS_NUM(a4)                    ; Add one to enemies killed.
                bra     .method_set_bones

.big_rhino_force_back:
		add.w	#16*6,THIS_SPRITE_MAP_XPOS(a2)			; force back 6 pixels...
		bra	.run_sprite_method

; d2=xpos to plot sprite
;
.method_animate_walk_left:
                move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                sub.w   PRIVATE_BIGRHINO1_SPEED(a2),d2
		sub.w	THIS_SPRITE_PARAMETER(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                move.w  #ENEMY_BIGRHINO1_YPOS,SPRITE_PLOT_YPOS(a1)
                move.w  d2,PRIVATE_BIGRHINO1_XPOS(a2)
                move.w  d1,PRIVATE_BIGRHINO1_ANIM_FRAME(a2)
                moveq   #BIGRHINO1_STATUS_WALK_LEFT,d1
                move.w  d1,PRIVATE_BIGRHINO1_ANIM_FRAME(a2)
                bra     ANIMATE


.method_animate_walk_right:
                move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                add.w   PRIVATE_BIGRHINO1_SPEED(a2),d2
		add.w	THIS_SPRITE_PARAMETER(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                move.w  d2,PRIVATE_BIGRHINO1_XPOS(a2)
                move.w  d1,PRIVATE_BIGRHINO1_ANIM_FRAME(a2)
                moveq   #BIGRHINO1_STATUS_WALK_RIGHT,d1
                move.w  d1,PRIVATE_BIGRHINO1_ANIM_FRAME(a2)
                bra     ANIMATE

.method_destroy_sprite:
                subq.w  #1,BIGRHINOS_IN_USE(a4)
                clr.w   THIS_SPRITE_STATUS(a2)
                IFNE    RHINO_HWSPR_ENABLE
              
                IFNE    RHINO_HWSPR_MID
                lea     ENEMY_HWSPR_MID_ALLOCATED(a4),a0                        ; Free up hardware sprite
                ENDC
               
                move.w  THIS_SPRITE_HWSLOT(a2),d7
                bmi     DESTROY_SPRITE
                FREE_HARDWARE_SPRITE
                ENDC
                bra     DESTROY_SPRITE

.method_set_destroy_sprite:
                move.w  #ENEMY_STATUS_DESTROYED,THIS_SPRITE_STATUS(a2)
                bra     .set_update

.method_set_walk_left:
                move.w  #BIGRHINO1_GO_LEFT,PRIVATE_BIGRHINO1_DIRECTION(a2)
                move.w  #BIGRHINO1_STATUS_WALK_LEFT,THIS_SPRITE_STATUS(a2)
                bra     .set_update

.method_set_walk_right:
                move.w  #BIGRHINO1_GO_RIGHT,PRIVATE_BIGRHINO1_DIRECTION(a2)
                move.w  #BIGRHINO1_STATUS_WALK_RIGHT,THIS_SPRITE_STATUS(a2)
                bra     .set_update

.method_set_bones:	
		lea	ATTACH_TABLE(a4),a3
		move.b	#-1,(a3,d0)
		
                lea     ENEMY_HWSPR_MID_ALLOCATED(a4),a0
                cmp.w   2(a0),d0
                beq.s   .found_partner
                addq.w  #8,a0

.found_partner: move.w  6(a0),d2
                move.l  MEMCHK0_SPRITE_HDLVARS(a4),a0
                lea     TAB_64(a4),a3
                add.w   (a3,d2*2),a0
                move.w  #ENEMY_STATUS_BONES,THIS_SPRITE_STATUS(a0)
                move.w  #ENEMY_STATUS_BONES,THIS_SPRITE_STATUS(a2)
                bra     .set_update

.method_set_collision_action:
                tst.w   RYGAR_FALLPREV_STATE(a4)                                ; Is Rygar falling on enemy
                bmi     .method_set_jump_or_destroy                     ; Yes!

                IFNE    DISABLE_ENEMY_COLLISION
                bra     .exit
                ELSE
                bra     .method_set_rygar_death_sequence
                ENDC

.set_update:

.exit:          rts



HDL_BIGRHINO2:
		FUNCID	#$26d2479b
                SPRITE_INITIATE

		lea	ENEMY_HWSPR_MID_ALLOCATED(a4),a3
.bank_0:
		cmp.w	6(a3),d0
		beq.s	.alloc_bank_0
		cmp.w	14(a3),d0
		beq.s	.alloc_bank_1
		move.w	#-1,THIS_SPRITE_HWSLOT(a2)		; Needs a bob
		bra.s	.active					; Neither sprites were allocated, needs to be a bob
		
.alloc_bank_0:	
                move.w  4(a3),SPRITE_PLOT_TYPE(a1)                               ; Set hardware type.
		move.w	#1,THIS_SPRITE_HWSLOT(a2)				; Bank 0 allocated
		bra.s	.active
		
.alloc_bank_1:
		move.w	12(a3),SPRITE_PLOT_TYPE(a1)
		move.w	#3,THIS_SPRITE_HWSLOT(a2)
		bra.s	.active
 
		nop

; Get BIGRHINO 1 context
.active:        ENEMY_CHECK_END_OF_ROUND

.sprite_0:      cmp.w   #ENEMY_STATUS_BONES,THIS_SPRITE_STATUS(a2)
                beq     .method_animate_bones

; Copy the position of the first BIGRHINO slot.
                lea     ENEMY_HWSPR_MID_ALLOCATED(a4),a0
                cmp.w   6(a0),d0
                beq.s   .found_alloc
                addq.w  #8,a0

.found_alloc:   move.w  2(a0),d2
                                                                ; Previous sprite number adjacent.
                move.l  MEMCHK0_SPRITE_HDLVARS(a4),a0
                lea     TAB_64(a4),a3
                add.w   (a3,d2*2),a0
; BIGRHINO 1 sprite context now in a0
                move.w  PRIVATE_BIGRHINO1_XPOS(a0),d2
                add.w   #32,d2                                  ; 32 pixels to right
                and.w   #$7ff,d2
                move.w  d2,SPRITE_PLOT_XPOS(a1)
                move.w  PRIVATE_BIGRHINO1_YPOS(a0),SPRITE_PLOT_YPOS(a1)
                move.w  #ENEMY_BIGRHINO1_YPOS,SPRITE_PLOT_YPOS(a1)
                move.w  PRIVATE_BIGRHINO1_ANIM_FRAME(a0),d1
                bra     ANIMATE

.method_set_sweep_direction:
                METHOD_ANIMATE_BONES

.method_destroy_sprite:
                clr.w   THIS_SPRITE_STATUS(a2)
                IFNE    BIGRHINO1_HWSPR_ENABLE
                IFNE    BIGRHINO1_HWSPR_MID
                lea     ENEMY_HWSPR_MID_ALLOCATED(a4),a0                        ; Free up hardware sprite
                ENDC
                move.w  THIS_SPRITE_HWSLOT(a2),d7
                bmi     DESTROY_SPRITE
                FREE_HARDWARE_SPRITE
                ENDC
                bra     DESTROY_SPRITE

		METHOD_SET_BONES

.set_update:

.exit:          rts
