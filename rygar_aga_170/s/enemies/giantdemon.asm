GIANTDEMON_GROUND_ENEMY:                equ     1       ; 1 = Ground enemy means enemy is stunnable.
GIANTDEMON_HWSPR_ENABLE:                equ     0
GIANTDEMON_HWSPR_UPPER:         equ     0
GIANTDEMON_HWSPR_MID:           equ     0
GIANTDEMON_HWSPR_LOWER:         equ     0

GIANTDEMON_STATUS_WAIT:         equ     6
GIANTDEMON_STATUS_FORWARD:      equ     7
GIANTDEMON_STATUS_BACKWARD:     equ     8
GIANTDEMON_STATUS_LUNGE:        equ     9
GIANTDEMON_STATUS_STAND:        equ     10

GIANTDEMON_TO_RYGAR_X1_BOUND:   equ     0
GIANTDEMON_TO_RYGAR_Y1_BOUND:   equ     0
GIANTDEMON_TO_RYGAR_X2_BOUND:   equ     32
GIANTDEMON_TO_RYGAR_Y2_BOUND:   equ     64

GIANTDEMON_TO_DISKARM_X1_BOUND: equ     0
GIANTDEMON_TO_DISKARM_Y1_BOUND: equ     0
GIANTDEMON_TO_DISKARM_X2_BOUND: equ     32
GIANTDEMON_TO_DISKARM_Y2_BOUND: equ     64

GIANTDEMON_MOVE_SPEED:          equ     32
GIANTDEMON_TRIGGER_POS:         equ     30

GIANTDEMON_BACKWARD_FRAMES:     equ     24
GIANTDEMON_FORWARD_FRAMES:      equ     34
GIANTDEMON_LUNGE_FRAMES:        equ     10

GIANTDEMON_STATUS_WALK_LEFT:    equ     6

PRIVATE_GIANTDEMON_SPEED:       equ     32
PRIVATE_GIANTDEMON_TRIGGER:     equ     34
PRIVATE_GIANTDEMON_FRAMECOUNT:  equ     36
PRIVATE_GIANTDEMON_FIXED_YPOS:  equ     38
PRIVATE_GIANTDEMON_SINE_POINTER equ     40
PRIVATE_GIANTDEMON_HIT_COUNT:	equ	42

GIANTDEMON_HIT_POINTS:             equ     9


HDL_GIANTDEMON:
		FUNCID	#$939912cd
                SPRITE_INITIATE

; Set the initial variables and status of the sprite.
.construct:
                move.w  SPRITE_PLOT_XPOS(a1),d2                 ; Set the initial Map Position
                lsl.w   #4,d2                           ; Based on the Plot position after
                move.w  d2,THIS_SPRITE_MAP_XPOS(a2)             ; Enemy is Spawned.
                move.w  #GIANTDEMON_MOVE_SPEED,PRIVATE_GIANTDEMON_SPEED(a2)     ; Set move speed.

                move.w  #GIANTDEMON_STATUS_WAIT,THIS_SPRITE_STATUS(a2)
                move.w  MAP_PIXEL_POSX(a4),PRIVATE_GIANTDEMON_TRIGGER(a2)
                add.w   #GIANTDEMON_TRIGGER_POS,PRIVATE_GIANTDEMON_TRIGGER(a2)
                move.w  #GIANTDEMON_HIT_POINTS,PRIVATE_GIANTDEMON_HIT_COUNT(a2)

;;; Init code here.

.active:
                ENEMY_CHECK_END_OF_ROUND

.sprite_0:
		cmp.w   #ENEMY_STATUS_SWEEP_RIGHT,THIS_SPRITE_STATUS(a2)                ; if doing bones or sweeps then skip all collisions
                ble     .run_sprite_method
		
		tst.w	SHIELD_TIMER
		bpl.s	.shield_on
                cmp.w   #ENEMY_STATUS_STUN_RIGHT,THIS_SPRITE_STATUS(a2)                 ; if doing stuns then skip Rygar collision
                ble     .sprite_stunned

.shield_on:
;; Code here....
                bra.s   .sprite_2

.animate_table: dc.l    .method_animate_none-.animate_table              ; 0
                dc.l    .method_animate_bones-.animate_table    	; 1
                dc.l    .method_animate_none-.animate_table       	; 2
                dc.l    .method_animate_none-.animate_table      	; 3
		dc.l    .method_animate_none-.animate_table      	; 4
		dc.l    .method_animate_none-.animate_table      	; 5
                dc.l    .method_animate_wait-.animate_table		; 6
                dc.l    .method_animate_forward-.animate_table		; 7
                dc.l    .method_animate_backward-.animate_table		; 8
                dc.l    .method_animate_lunge-.animate_table   		;9
                dc.l    .method_animate_stand-.animate_table		; 10
                dc.l    .method_animate_none-.animate_table      	; 11
                dc.l    .method_animate_none-.animate_table      	; 12
                dc.l    .method_animate_none-.animate_table      	; 13
                dc.l    .method_animate_none-.animate_table      	; 14
                dc.l    .method_animate_none-.animate_table      	; 15
                dc.l    .method_animate_none-.animate_table      	; 16
                dc.l    .method_animate_none-.animate_table      	; 17
                dc.l    .method_animate_none-.animate_table      	; 18
                dc.l    .method_animate_none-.animate_table      	; 19
                dc.l    .method_animate_none-.animate_table      	; 20
                dc.l    .method_animate_none-.animate_table      	; 21
                dc.l    .method_animate_none-.animate_table      	; 22
                dc.l    .method_animate_none-.animate_table      	; 23
                dc.l    .method_animate_none-.animate_table      	; 24
                dc.l    .method_animate_none-.animate_table      	; 25
		
.method_animate_none:	bra	.exit

;Test this enemy collided with Rygar (the player)
.sprite_2:              move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4                                   ; Check a 32x32 boundary from x1/y1
                move.w  d3,d5
                add.w   #GIANTDEMON_TO_RYGAR_X1_BOUND,d2
                add.w   #GIANTDEMON_TO_RYGAR_Y1_BOUND,d3
                add.w   #GIANTDEMON_TO_RYGAR_X2_BOUND,d4
                add.w   #GIANTDEMON_TO_RYGAR_Y2_BOUND,d5
                lea     RYGAR_COORDS(a4),a3
                ENEMY_TO_RYGAR_COLLISION
		move.l  #POINTS_GIANTDEMON,d2
                tst.w   d6                                      ; Did this sprite collide with Rygar?
                IFEQ    DISABLE_ENEMY_COLLISION
                bmi     .method_set_collision_action            ; Yes
                ENDC

; Check if the enemy has been hit with disk armor
.sprite_stunned:
                move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4                                   ; Check a 32x32 boundary from x1/y1
                move.w  d3,d5
                add.w   #GIANTDEMON_TO_DISKARM_X1_BOUND,d2
                add.w   #GIANTDEMON_TO_DISKARM_Y1_BOUND,d3
                add.w   #GIANTDEMON_TO_DISKARM_X2_BOUND,d4
                add.w   #GIANTDEMON_TO_DISKARM_Y2_BOUND,d5
                lea     DISKARM_COORDS(a4),a3
                ENEMY_TO_DISKARM_COLLISION
		move.l  #POINTS_GIANTDEMON,d2
                tst.w   d6                                      ; Did this sprite get hit with the disk armor
                bmi     .method_add_points_and_destroy              ; Yes

; Test this enemy collided with the disk armour

                RUN_SPRITE_METHOD

                METHOD_ANIMATE_BONES


.method_set_sweep_direction:
		tst.w	SHIELD_TIMER			; No bonus if Shield is on.
		bpl.s	.method_set_bones		
		
		lea	TEXT_FLASH_9(a4),a0				
		move.l	a0,TEXT_BONUS_POINTER(a4)
		move.w	#200,BONUS_DISPLAY_TIMER(a4)
		bsr	CREATE_BONUS_TEXT
		
		add.l	#POINTS_GIANTDEMON_STOMP,VAL_PLAYER1_SCORE(a4)
		move.w	#-1,UPDATE_SCORE(a4)
		
		move.l	d0,-(a7)
		move.w	#MUSIC_POWER_DELAY_TIME,MUSIC_DELAY_TIME(a4)
		moveq	#MUSIC_BONUS_POINTS,d0
		bsr	PLAY_TUNE
		move.l	(a7)+,d0
		
.method_set_bones:
                move.w  #ENEMY_STATUS_BONES,THIS_SPRITE_STATUS(a2)
                move.w  #SPR_TYPE_ENEMY_2X64,SPRITE_PLOT_TYPE(a1)
                bra     .set_update

                METHOD_SET_STUN_DIRECTION

                METHOD_SET_RYGAR_DEATH_SEQUENCE

.method_add_points_and_destroy:
		tst.w	STATE_KILL_ALL_ENEMIES(a4)
		bmi.s	.xxx
		move.w	#-1,DISKARM_DISABLE(a4)	
.xxx
		tst.w	ITEM_KILLALL_SET(a4)
		bmi	.method_set_bones
                
		move.l  d0,-(a7)
                moveq   #SND_STONE_HIT,d0
                bsr     PLAY_SAMPLE
                move.l  (a7)+,d0
		
		moveq	#1,d3
		move.w  RYGAR_CURRENT_POWERS(a4),d4
                and.w   #POWER_CROWN,d4
                beq     .has_crown                     ; If Rygar has Crown then the Dragon is killed
		moveq	#3,d3
.has_crown:                		
                sub.w  	d3,PRIVATE_GIANTDEMON_HIT_COUNT(a2)
		
		move.w	#HITPOINT_DELAY,HITPOINT_FRAMES(a4)
		tst.w	PRIVATE_GIANTDEMON_HIT_COUNT(a2)
		beq	.run_sprite_method		
		bpl	.run_sprite_method
		
                add.l   d2,VAL_PLAYER1_SCORE(a4)
                move.w  #-1,UPDATE_SCORE(a4)
                addq.w  #1,REPULSE_BONUS_NUM(a4)                    ; Add one to enemies killed.
                bra     .method_set_bones

.method_set_collision_action:
                tst.w   RYGAR_FALLPREV_STATE(a4)                                ; Is Rygar falling on enemy
                bmi.s   .method_set_jump_or_destroy                     ; Yes!

                IFNE    DISABLE_ENEMY_COLLISION
                bra     .exit
                ELSE
                bra     .method_set_rygar_death_sequence
                ENDC

                METHOD_SET_JUMP_OR_DESTROY                              ;



.method_destroy_sprite: subq.w  #1,GIANTDEMONS_IN_USE(a4)

                clr.w   THIS_SPRITE_STATUS(a2)
                IFNE    GIANTDEMON_HWSPR_ENABLE
                IFNE    GIANTDEMON_HWSPR_UPPER
                lea     ENEMY_HWSPR_UPPER_ALLOCATED(a4),a0                      ; Free up hardware sprite
                ENDC
                IFNE    GIANTDEMON_HWSPR_MID
                lea     ENEMY_HWSPR_MID_ALLOCATED(a4),a0                        ; Free up hardware sprite
                ENDC
                IFNE    GIANTDEMON_HWSPR_LOWER
                lea     ENEMY_HWSPR_LOWER_ALLOCATED(a4),a0                      ; Free up hardware sprite
                ENDC
                move.w  THIS_SPRITE_HWSLOT(a2),d7
                bmi     DESTROY_SPRITE
                FREE_HARDWARE_SPRITE
                ENDC
                bra     DESTROY_SPRITE


.method_set_wait:
                move.w  #SPR_TYPE_ENEMY_3X48,SPRITE_PLOT_TYPE(a1)
                move.w  #GIANTDEMON_STATUS_WAIT,THIS_SPRITE_STATUS(a2)
                bra     .exit

.method_animate_wait:
                move.w  MAP_PIXEL_POSX(a4),d2
                cmp.w   PRIVATE_GIANTDEMON_TRIGGER(a2),d2
                beq     .method_set_stand

                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS
                moveq   #GIANTDEMON_STATUS_WAIT,d1
                bra     ANIMATE

.method_set_stand:
                add.w   #16,PRIVATE_GIANTDEMON_TRIGGER(a2)                      ; frames after standing before attack
                move.w  #SPR_TYPE_ENEMY_2X64,SPRITE_PLOT_TYPE(a1)
                move.w  #GIANTDEMON_STATUS_STAND,THIS_SPRITE_STATUS(a2)
                ;bra     .exit

.method_animate_stand:
                move.w  MAP_PIXEL_POSX(a4),d2
                cmp.w   PRIVATE_GIANTDEMON_TRIGGER(a2),d2
                beq     .method_set_forward

                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS
                moveq   #GIANTDEMON_STATUS_STAND,d1
                bra     ANIMATE



.method_set_forward:
                move.w  #GIANTDEMON_FORWARD_FRAMES,PRIVATE_GIANTDEMON_FRAMECOUNT(a2)
                move.w  #GIANTDEMON_STATUS_FORWARD,THIS_SPRITE_STATUS(a2)
                move.w  #SPR_TYPE_ENEMY_2X64,SPRITE_PLOT_TYPE(a1)
                move.w  SPRITE_PLOT_YPOS(a1),PRIVATE_GIANTDEMON_FIXED_YPOS(a2)
                clr.w   PRIVATE_GIANTDEMON_SINE_POINTER(a2)
                bra     .exit

.method_animate_forward:
                subq.w  #1,PRIVATE_GIANTDEMON_FRAMECOUNT(a2)
                tst.w   PRIVATE_GIANTDEMON_FRAMECOUNT(a2)
                bmi     .method_set_lunge

                lea     GIANTDEMON_FORWARD_SINE(a4),a0
                move.w  PRIVATE_GIANTDEMON_SINE_POINTER(a2),d2
                move.w  PRIVATE_GIANTDEMON_FIXED_YPOS(a2),d3
                add.w   (a0,d2*2),d3
                move.w  d3,SPRITE_PLOT_YPOS(a1)
                addq.w  #1,PRIVATE_GIANTDEMON_SINE_POINTER(a2)

                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                sub.w   #GIANTDEMON_MOVE_SPEED,d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS
                moveq   #GIANTDEMON_STATUS_FORWARD,d1
                bra     ANIMATE

.method_set_backward:
                move.w  #GIANTDEMON_BACKWARD_FRAMES,PRIVATE_GIANTDEMON_FRAMECOUNT(a2)
                move.w  #SPR_TYPE_ENEMY_2X64,SPRITE_PLOT_TYPE(a1)
                move.w  #GIANTDEMON_STATUS_BACKWARD,THIS_SPRITE_STATUS(a2)
                move.w  SPRITE_PLOT_YPOS(a1),PRIVATE_GIANTDEMON_FIXED_YPOS(a2)
                clr.w   PRIVATE_GIANTDEMON_SINE_POINTER(a2)
		;add.w	#16*16,THIS_SPRITE_MAP_XPOS(a2)
                bra     .exit

.method_animate_backward:
                subq.w  #1,PRIVATE_GIANTDEMON_FRAMECOUNT(a2)
                tst.w   PRIVATE_GIANTDEMON_FRAMECOUNT(a2)
                bmi     .method_set_forward

                lea     GIANTDEMON_BACKWARD_SINE(a4),a0
                move.w  PRIVATE_GIANTDEMON_SINE_POINTER(a2),d2
                move.w  PRIVATE_GIANTDEMON_FIXED_YPOS(a2),d3
                add.w   (a0,d2*2),d3
                move.w  d3,SPRITE_PLOT_YPOS(a1)
                addq.w  #1,PRIVATE_GIANTDEMON_SINE_POINTER(a2)

                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                add.w   #GIANTDEMON_MOVE_SPEED,d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS
                moveq   #GIANTDEMON_STATUS_BACKWARD,d1
                bra     ANIMATE


.method_set_lunge:
                move.w  #GIANTDEMON_LUNGE_FRAMES,PRIVATE_GIANTDEMON_FRAMECOUNT(a2)
                move.w  #SPR_TYPE_ENEMY_3X48,SPRITE_PLOT_TYPE(a1)
                move.w  #GIANTDEMON_STATUS_LUNGE,THIS_SPRITE_STATUS(a2)
		sub.w	#16*16,THIS_SPRITE_MAP_XPOS(a2)
                ;bra     .exit

.method_animate_lunge:
                subq.w  #1,PRIVATE_GIANTDEMON_FRAMECOUNT(a2)
                tst.w   PRIVATE_GIANTDEMON_FRAMECOUNT(a2)
                bmi     .method_set_backward

                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS
                moveq   #GIANTDEMON_STATUS_LUNGE,d1
                bra     ANIMATE


.set_update:

.exit:          rts
