MUTANTFROG_GROUND_ENEMY:        equ     1       ; 1 = Ground enemy means enemy is stunnable.
MUTANTFROG_HWSPR_ENABLE:        equ     0
MUTANTFROG_HWSPR_UPPER:         equ     0
MUTANTFROG_HWSPR_MID:           equ     1
MUTANTFROG_HWSPR_LOWER:         equ     0

MUTANTFROG_STATUS_FACELEFT:	equ     6
MUTANTFROG_STATUS_FACERIGHT:	equ     7
MUTANTFROG_STATUS_LEAPLEFT:	equ     8
MUTANTFROG_STATUS_LEAPRIGHT:	equ     9

MUTANTFROG_TO_RYGAR_X1_BOUND:   equ     0
MUTANTFROG_TO_RYGAR_Y1_BOUND:   equ     0
MUTANTFROG_TO_RYGAR_X2_BOUND:   equ     32
MUTANTFROG_TO_RYGAR_Y2_BOUND:   equ     32

MUTANTFROG_TO_DISKARM_X1_BOUND: equ     0
MUTANTFROG_TO_DISKARM_Y1_BOUND: equ     0
MUTANTFROG_TO_DISKARM_X2_BOUND: equ     32
MUTANTFROG_TO_DISKARM_Y2_BOUND: equ     32

MUTANTFROG_MOVE_SPEED:          equ     64
MUTANTFROG_TRIGGER_POS:         equ     30

MUTANTFROG_LEAPLEFT_FRAMES:     equ     36
MUTANTFROG_LEAPRIGHT_FRAMES:     equ     36

MUTANTFROG_STATUS_WALK_LEFT:    equ     6

PRIVATE_MUTANTFROG_SPEED:       equ     32
PRIVATE_MUTANTFROG_TRIGGER:     equ     34
PRIVATE_MUTANTFROG_FRAMECOUNT:  equ     36
PRIVATE_MUTANTFROG_FIXED_YPOS:  equ     38
PRIVATE_MUTANTFROG_SINE_POINTER equ     40
PRIVATE_MUTANTFROG_BOUNDARY_TIMER:	equ	42





HDL_MUTANTFROG:
		FUNCID	#$7896b4f5
                SPRITE_INITIATE

; Set the initial variables and status of the sprite.
.construct:
		FIND_CAMERA_XPOS
		
                move.w  SPRITE_PLOT_XPOS(a1),d2                 ; Set the initial Map Position
                lsl.w   #4,d2                           ; Based on the Plot position after
                move.w  d2,THIS_SPRITE_MAP_XPOS(a2)             ; Enemy is Spawned.
   
		move.w  #MUTANTFROG_MOVE_SPEED,PRIVATE_MUTANTFROG_SPEED(a2)     ; Set move speed.
		move.w	THIS_SPRITE_PARAMETER(a2),d2
		;move.w	d2,DEBUG_DRAGON_SPEED
		add.w	d2,PRIVATE_MUTANTFROG_SPEED(a2)		
		
                move.w  #ENEMY_BOUNDARY_TIME,PRIVATE_MUTANTFROG_BOUNDARY_TIMER(a2)
                move.w  #-1,THIS_SPRITE_STUN_TIMER(a2)
		move.w	SPRITE_PLOT_YPOS(a1),PRIVATE_MUTANTFROG_FIXED_YPOS(a2)
		
		IFNE    MUTANTFROG_HWSPR_ENABLE
                move.w  #-1,THIS_SPRITE_HWSLOT(a2)
                IFNE    MUTANTFROG_HWSPR_MID
                lea     ENEMY_HWSPR_MID_ALLOCATED(a4),a0                        ; Free up hardware sprite
                ENDC
                bsr     ALLOC_HARDWARE_SPRITE
                tst.w   d7
                bmi.s   .active
                move.w  (a0),SPRITE_PLOT_TYPE(a1)                               ; Set hardware type.
                move.w  d7,THIS_SPRITE_HWSLOT(a2)
                ENDC

		bra	.method_set_facing
    
;;; Init code here.

.active:
                ENEMY_CHECK_END_OF_ROUND

.sprite_0:
		bra	.sprite_1

.animate_table: dc.l    .method_animate_none-.animate_table      	; 4
                dc.l    .method_animate_bones-.animate_table    	; 1
                dc.l    .method_animate_sweep_left-.animate_table     	; 2
                dc.l    .method_animate_sweep_right-.animate_table    	; 3
                dc.l    .method_animate_stun_left-.animate_table     	; 4 eserved for Stun
                dc.l    .method_animate_stun_right-.animate_table     	; 5 Reserved for Stun
                dc.l    .method_animate_faceleft-.animate_table		; 6
                dc.l    .method_animate_faceright-.animate_table	; 7
                dc.l    .method_animate_leapleft-.animate_table 	; 8
                dc.l    .method_animate_leapright-.animate_table	; 9
                dc.l    .method_animate_none-.animate_table      	; 10
                dc.l    .method_animate_none-.animate_table      	; 11
                dc.l    .method_animate_none-.animate_table      	; 12
                dc.l    .method_animate_none-.animate_table      	; 13
                dc.l    .method_animate_none-.animate_table      	; 14
                dc.l    .method_animate_none-.animate_table      	; 15
                dc.l    .method_animate_none-.animate_table      	; 16
                dc.l    .method_animate_none-.animate_table      	; 17
                dc.l    .method_animate_none-.animate_table      	; 18
                dc.l    .method_animate_none-.animate_table      	; 19
                dc.l    .method_animate_none-.animate_table      	; 20
                dc.l    .method_animate_none-.animate_table      	; 21
                dc.l    .method_animate_none-.animate_table      	; 22
                dc.l    .method_animate_none-.animate_table      	; 23
                dc.l    .method_animate_none-.animate_table      	; 24
		
.method_animate_none:	bra	.exit

.sprite_1:      cmp.w   #ENEMY_STATUS_SWEEP_RIGHT,THIS_SPRITE_STATUS(a2)                ; if doing bones or sweeps then skip all collisions
                ble     .run_sprite_method
		tst.w	SHIELD_TIMER
		bpl.s	.shield_on
                cmp.w   #ENEMY_STATUS_STUN_RIGHT,THIS_SPRITE_STATUS(a2)                 ; if doing stuns then skip Rygar collision
                ble     .sprite_stunned

.shield_on:

;Test this enemy collided with Rygar (the player)
.sprite_2:	move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4                                   ; Check a 32x32 boundary from x1/y1
                move.w  d3,d5
                add.w   #MUTANTFROG_TO_RYGAR_X1_BOUND,d2
                add.w   #MUTANTFROG_TO_RYGAR_Y1_BOUND,d3
                add.w   #MUTANTFROG_TO_RYGAR_X2_BOUND,d4
                add.w   #MUTANTFROG_TO_RYGAR_Y2_BOUND,d5
                lea     RYGAR_COORDS(a4),a3
                ENEMY_TO_RYGAR_COLLISION
                move.l  #POINTS_MUTANTFROG,d2
                tst.w   d6                                      ; Did this sprite collide with Rygar?
                IFEQ    DISABLE_ENEMY_COLLISION
                bmi     .method_set_collision_action            ; Yes
                ENDC

; Check if the enemy has been hit with disk armor
.sprite_stunned:
                move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4                                   ; Check a 32x32 boundary from x1/y1
                move.w  d3,d5
                add.w   #MUTANTFROG_TO_DISKARM_X1_BOUND,d2
                add.w   #MUTANTFROG_TO_DISKARM_Y1_BOUND,d3
                add.w   #MUTANTFROG_TO_DISKARM_X2_BOUND,d4
                add.w   #MUTANTFROG_TO_DISKARM_Y2_BOUND,d5
                lea     DISKARM_COORDS(a4),a3
                ENEMY_TO_DISKARM_COLLISION
                move.l  #POINTS_MUTANTFROG,d2
                tst.w   d6                                      ; Did this sprite get hit with the disk armor
                bmi     .method_set_sweep_direction             ; Yes

; Test this enemy collided with the disk armour

                RUN_SPRITE_METHOD

                METHOD_ANIMATE_BONES

                METHOD_ANIMATE_SWEEP_LEFT

                METHOD_ANIMATE_SWEEP_RIGHT

                METHOD_SET_BONES

                METHOD_SET_SWEEP_DIRECTION

                METHOD_SET_STUN_DIRECTION
		
		METHOD_SET_STUN_OR_DESTROY

                METHOD_SET_RYGAR_DEATH_SEQUENCE


.method_animate_stun_left:
		clr.w	PRIVATE_MUTANTFROG_SINE_POINTER(a2)
		move.w	PRIVATE_MUTANTFROG_FIXED_YPOS(a2),SPRITE_PLOT_YPOS(a1)		
                moveq   #ENEMY_STATUS_STUN_LEFT,d1
                move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                subq.w  #1,THIS_SPRITE_STUN_TIMER(a2)
                bmi     .method_set_facing
                ENEMY_SHAKE
                bra     ANIMATE

.method_animate_stun_right:
		clr.w	PRIVATE_MUTANTFROG_SINE_POINTER(a2)
		move.w	PRIVATE_MUTANTFROG_FIXED_YPOS(a2),SPRITE_PLOT_YPOS(a1)		
                moveq   #ENEMY_STATUS_STUN_RIGHT,d1
                move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                subq.w  #1,THIS_SPRITE_STUN_TIMER(a2)
                bmi     .method_set_facing
                ENEMY_SHAKE
                bra     ANIMATE

.method_set_collision_action:
                tst.w   RYGAR_FALLPREV_STATE(a4)                                ; Is Rygar falling on enemy
                bmi     .method_set_stun_or_destroy                     ; Yes!

                IFNE    DISABLE_ENEMY_COLLISION
                bra     .exit
                ELSE
                bra     .method_set_rygar_death_sequence
                ENDC
		
                ;tst.w   RYGAR_FALLPREV_STATE(a4)                                ; Is Rygar falling on enemy
                ;IFNE    MUTANTFROG_GROUND_ENEMY
                ;bmi.s   .method_set_stun_or_destroy                     ; Yes!
                ;ELSE
                ;bmi.s   .method_set_jump_or_destroy                     ; Yes!
                ;ENDC

                ;IFNE    DISABLE_ENEMY_COLLISION
                ;bra     .exit
                ;ELSE
                ;bra     .method_set_rygar_death_sequence
                ;ENDC

                ;IFNE    MUTANTFROG_GROUND_ENEMY
                ;METHOD_SET_STUN_OR_DESTROY                              ;
                ;ELSE
                ;METHOD_SET_JUMP_OR_DESTROY                              ;
                ;ENDC


.method_destroy_sprite: subq.w  #1,MUTANTFROGS_IN_USE(a4)

                clr.w   THIS_SPRITE_STATUS(a2)
                IFNE    MUTANTFROG_HWSPR_ENABLE
                IFNE    MUTANTFROG_HWSPR_UPPER
                lea     ENEMY_HWSPR_UPPER_ALLOCATED(a4),a0                      ; Free up hardware sprite
                ENDC
                IFNE    MUTANTFROG_HWSPR_MID
                lea     ENEMY_HWSPR_MID_ALLOCATED(a4),a0                        ; Free up hardware sprite
                ENDC
                IFNE    MUTANTFROG_HWSPR_LOWER
                lea     ENEMY_HWSPR_LOWER_ALLOCATED(a4),a0                      ; Free up hardware sprite
                ENDC
                move.w  THIS_SPRITE_HWSLOT(a2),d7
                bmi     DESTROY_SPRITE
                FREE_HARDWARE_SPRITE
                ENDC
                bra     DESTROY_SPRITE


.method_set_facing:
		move.w	RYGAR_XPOS(a4),d2
		cmp.w	SPRITE_PLOT_XPOS(a1),d2
		ble.s	.method_set_faceleft
		bra	.method_set_faceright
		
.method_set_leap:
		move.w	RYGAR_XPOS(a4),d2
		cmp.w	SPRITE_PLOT_XPOS(a1),d2
		ble	.method_set_leapleft
		bra	.method_set_leapright


.method_set_faceleft:
		move.w	#-1,THIS_SPRITE_DIRECTION(a2)
                move.w  #MUTANTFROG_STATUS_FACELEFT,THIS_SPRITE_STATUS(a2)
		clr.w	PRIVATE_MUTANTFROG_SINE_POINTER(a2)
                bra     .exit

.method_animate_faceleft:
                lea     MUTANTFROG_FACE_SINE(a4),a0
                move.w  PRIVATE_MUTANTFROG_SINE_POINTER(a2),d2
                addq.w  #1,PRIVATE_MUTANTFROG_SINE_POINTER(a2)
		move.w	THIS_SPRITE_PARAMETER(a2),d3
		lsr.w	#4,d3
		add.w	d3,PRIVATE_MUTANTFROG_SINE_POINTER(a2)

                move.w  (a0,d2*2),d3
                cmp.w   #$8000,d3
                beq.s   .method_set_leap

		move.w  PRIVATE_MUTANTFROG_FIXED_YPOS(a2),d4
                add.w   d3,d4
                move.w  d4,SPRITE_PLOT_YPOS(a1)

                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  d6,d2
		subq.w	#2,d2				; Slight move to the left
                UPDATE_SPRITE_PLOT_XPOS
                moveq   #MUTANTFROG_STATUS_FACELEFT,d1
                bra     ANIMATE



; Frog Leaps forward toward Rygar
.method_set_leapleft:
		move.w	#-1,THIS_SPRITE_DIRECTION(a2)
                move.w  #MUTANTFROG_LEAPLEFT_FRAMES,PRIVATE_MUTANTFROG_FRAMECOUNT(a2)
                move.w  #MUTANTFROG_STATUS_LEAPLEFT,THIS_SPRITE_STATUS(a2)
                ;move.w  SPRITE_PLOT_YPOS(a1),PRIVATE_MUTANTFROG_FIXED_YPOS(a2)
                clr.w   PRIVATE_MUTANTFROG_SINE_POINTER(a2)
                bra     .exit

.method_animate_leapleft:
                subq.w  #1,PRIVATE_MUTANTFROG_FRAMECOUNT(a2)
                tst.w   PRIVATE_MUTANTFROG_FRAMECOUNT(a2)
                bmi     .method_set_facing

                lea     MUTANTFROG_LEAP_SINE(a4),a0
                move.w  PRIVATE_MUTANTFROG_SINE_POINTER(a2),d2
                move.w  PRIVATE_MUTANTFROG_FIXED_YPOS(a2),d3
                add.w   (a0,d2*2),d3
                move.w  d3,SPRITE_PLOT_YPOS(a1)
                addq.w  #1,PRIVATE_MUTANTFROG_SINE_POINTER(a2)

                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                sub.w   PRIVATE_MUTANTFROG_SPEED(a2),d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS
                moveq   #MUTANTFROG_STATUS_LEAPLEFT,d1
                bra     ANIMATE
		
.method_set_faceright:
		clr.w	THIS_SPRITE_DIRECTION(a2)
                move.w  #MUTANTFROG_STATUS_FACERIGHT,THIS_SPRITE_STATUS(a2)
		clr.w	PRIVATE_MUTANTFROG_SINE_POINTER(a2)
                bra     .exit

.method_animate_faceright:
                lea     MUTANTFROG_FACE_SINE(a4),a0
                move.w  PRIVATE_MUTANTFROG_SINE_POINTER(a2),d2
                addq.w  #1,PRIVATE_MUTANTFROG_SINE_POINTER(a2)

		move.w	THIS_SPRITE_PARAMETER(a2),d3
		lsr.w	#4,d3
		add.w	d3,PRIVATE_MUTANTFROG_SINE_POINTER(a2)

                move.w  (a0,d2*2),d3
                cmp.w   #$8000,d3
                beq   .method_set_leap

		move.w  PRIVATE_MUTANTFROG_FIXED_YPOS(a2),d4
                add.w   d3,d4
                move.w  d4,SPRITE_PLOT_YPOS(a1)

                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  d6,d2
		addq.w	#2,d2				; Slight move to the right
                UPDATE_SPRITE_PLOT_XPOS
                moveq   #MUTANTFROG_STATUS_FACERIGHT,d1
                bra     ANIMATE

; Frog Leaps forward toward Rygar
.method_set_leapright:
		clr.w	THIS_SPRITE_DIRECTION(a2)
                move.w  #MUTANTFROG_LEAPRIGHT_FRAMES,PRIVATE_MUTANTFROG_FRAMECOUNT(a2)
                move.w  #MUTANTFROG_STATUS_LEAPRIGHT,THIS_SPRITE_STATUS(a2)
                ;move.w  SPRITE_PLOT_YPOS(a1),PRIVATE_MUTANTFROG_FIXED_YPOS(a2)
                clr.w   PRIVATE_MUTANTFROG_SINE_POINTER(a2)
                bra     .exit

.method_animate_leapright:
                subq.w  #1,PRIVATE_MUTANTFROG_FRAMECOUNT(a2)
                tst.w   PRIVATE_MUTANTFROG_FRAMECOUNT(a2)
                bmi     .method_set_facing

                lea     MUTANTFROG_LEAP_SINE(a4),a0
                move.w  PRIVATE_MUTANTFROG_SINE_POINTER(a2),d2
                move.w  PRIVATE_MUTANTFROG_FIXED_YPOS(a2),d3
                add.w   (a0,d2*2),d3
                move.w  d3,SPRITE_PLOT_YPOS(a1)
                addq.w  #1,PRIVATE_MUTANTFROG_SINE_POINTER(a2)

                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                add.w   PRIVATE_MUTANTFROG_SPEED(a2),d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS
                moveq   #MUTANTFROG_STATUS_LEAPRIGHT,d1
                bra     ANIMATE




.set_update:

.exit:          rts
