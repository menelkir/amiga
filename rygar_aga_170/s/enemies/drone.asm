ENEMY_DRONE_XPOS:               equ     500
ENEMY_DRONE_YPOS:               equ     114

DRONE_WALK_SPEED:               equ     10
;FRAMES_OFFSCREEN_BEFORE_DESTROYED:     equ     100             ; Number of frames after which the sprite will disappear and be destroyed.

DRONE_STATUS_WALK_LEFT:         equ     6
DRONE_STATUS_WALK_RIGHT         equ     7
DRONE_STATUS_GRAB_LEFT:         equ     8
DRONE_STATUS_GRAB_RIGHT:        equ     9
DRONE_STATUS_TURN_DELAY:        equ     10
DRONE_STATUS_STATIC_LEFT:       equ     11
DRONE_STATUS_STATIC_RIGHT:      equ     12

DRONE_TO_RYGAR_X1_BOUND:        equ     8
DRONE_TO_RYGAR_Y1_BOUND:        equ     0
DRONE_TO_RYGAR_X2_BOUND:        equ     23
DRONE_TO_RYGAR_Y2_BOUND:        equ     31

DRONE_TO_DISKARM_X1_BOUND:      equ     8
DRONE_TO_DISKARM_Y1_BOUND:      equ     4
DRONE_TO_DISKARM_X2_BOUND:      equ     23
DRONE_TO_DISKARM_Y2_BOUND:      equ     31

DRONE_GRABS:                    equ     1
DRONE_PROXIMITY_RANGE:          equ     48
DRONE_PROXIMITY_GRAB_SPEED:     equ     10
DRONE_GRAB_TIME:                equ     40
DRONE_TURN_DELAY_TIME:          equ     10
DRONE_GRAB_DELAY_TIME:          equ     30

; Private Variables
PRIVATE_DRONE_MOVE_SPEED:       equ     32
PRIVATE_DRONE_TURNS:            equ     34      ; Number of times the DRONE can turn to follow Rygar.
PRIVATE_DRONE_GRABS:            equ     36      ; True or False
						;  This will configure the number of times the DRONE will attempt to run and grab Rygar.
						; if zero then the enemy is less dangerous.
PRIVATE_DRONE_GRAB_TIMER:       equ     38
PRIVATE_DRONE_PROXIMITY_LEFT:   equ     40      ;
PRIVATE_DRONE_PROXIMITY_RIGHT:  equ     42      ;
PRIVATE_DRONEX_LAST_ANIM:       equ     44
PRIVATE_DRONE_TURN_TIMER:       equ     46


; Sets the private variable DRONE_IN_PROXIMITY to -1 if true, otherwise 0.
; If enemy is approaching Rygar then check proximity.
; -- If enemy is moving left, and Rygar is to the left then check.
; -- if enemy is moving right, and Rygar is to the right then check.
DRONE_CHECK_AND_SET_PROXIMITY MACRO
                tst.w   PRIVATE_DRONE_GRABS(a2)                         ; Number of grabs done?
                beq     .proximity_out_of_range
                tst.w   PRIVATE_DRONE_GRAB_TIMER(a2)                    ; Timer still running?
                bpl.s   .proximity_out_of_range
                tst.w   RYGAR_FALL_STATE(a4)                            ; Rygar falling or jumping
                bne     .proximity_out_of_range
                tst.w   RYGAR_JUMP_STATE(a4)
                bne     .proximity_out_of_range

                move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  RYGAR_XPOS(a4),d3
; If enemy is moving left then check right
                tst.w   THIS_SPRITE_DIRECTION(a2)                     ; If moving left then check right
                bmi.s   .proximity_right                                ; If moving right then check left
.proximity_left:
                sub.w   #DRONE_PROXIMITY_RANGE,d3
                move.w  d3,d4
                subq.w  #2,d3
                bra.s   .proximity_check

.proximity_right:
                add.w   #DRONE_PROXIMITY_RANGE,d3
                move.w  d3,d4
                addq.w  #2,d4

.proximity_check:
                move.w  d3,PRIVATE_DRONE_PROXIMITY_LEFT(a2)
                move.w  d4,PRIVATE_DRONE_PROXIMITY_RIGHT(a2)
		
		cmp.w   d3,d2
		blt.s  	.proximity_out_of_range
		cmp.w	d4,d2
		bgt.s	.proximity_out_of_range
		
                ;cxmp2.w  PRIVATE_DRONE_PROXIMITY_LEFT(a2),d2
                ;bcs.s   .proximity_out_of_range                         ;
                
		cmp.w   #DRONE_STATUS_WALK_LEFT,THIS_SPRITE_STATUS(a2)
                beq     .method_set_grab_left
                cmp.w   #DRONE_STATUS_WALK_RIGHT,THIS_SPRITE_STATUS(a2)
                beq     .method_set_grab_right

.proximity_out_of_range:
                ENDM


HDL_DRONE:
		FUNCID	#$5eacd362
                SPRITE_INITIATE

; Set the initial variables and status of the sprite.
.construct:
                FIND_CAMERA_XPOS

		move.l	#ENEMY_DEF_DRONE,d5		
		CREATE_ENEMY_LEFT_OR_RIGHT

.create:
; d7 = spawn x position
                move.w  d7,THIS_SPRITE_MAP_XPOS(a2)                     ; Set start position in the map.
                move.w  #DRONE_WALK_SPEED,PRIVATE_DRONE_MOVE_SPEED(a2)
                move.w  d0,d2

                move.w  RANDOM_SEED(a4),d3                                      ; abcd
                and.w   #$3,d2                                          ; 01
                lsl.w   #2,d2
                lsr.w   d2,d3
                and.w   #$f,d3
                lsr.w   #2,d3
		
; Adjust difficulty by increasing speed of sprites
		add.w	ENEMY_SPEED_MODIFIER(a4),d3			; Add sprite speed modifider
		move.w	ROUND_CURRENT(a4),d2				; Take current round
		lsr.w	#1,d2						; divide by 2 and add that
		add.w	d2,d3						; to the speed...  round 32 would be 16.
; Adjust difficulty
		
		add.w   d3,PRIVATE_DRONE_MOVE_SPEED(a2)
		move.w	THIS_SPRITE_PARAMETER(a2),d3
		add.w	d3,PRIVATE_DRONE_MOVE_SPEED(a2)
		
                move.w  #-1,PRIVATE_DRONE_GRAB_TIMER(a2)
                move.w  #DRONE_TURN_DELAY_TIME,PRIVATE_DRONE_TURN_TIMER(a2)
                move.w  #DRONE_GRABS,PRIVATE_DRONE_GRABS(a2)

                move.w  #-1,THIS_SPRITE_STUN_TIMER(a2)		
                move.w  #ENEMY_BOUNDARY_TIME,THIS_SPRITE_BOUNDARY_TIMER(a2)
		

.active:        ENEMY_CHECK_END_OF_ROUND

.sprite_0:	ENEMY_BOUNDARY_CHECK

		move.w	ENEMY_SPEED_ACCUMULATOR(a4),d2
		add.w	d2,PRIVATE_DRONE_MOVE_SPEED(a2)
		bra.s   .sprite_1

.animate_table: dc.l    .method_destroy_sprite-.animate_table   	; 0
                dc.l    .method_animate_bones-.animate_table    	; 1
                dc.l    .method_animate_sweep_left-.animate_table       ; 2
                dc.l    .method_animate_sweep_right-.animate_table      ; 3
                dc.l    .method_animate_stun_left-.animate_table        ; 4
                dc.l    .method_animate_stun_right-.animate_table       ; 5
                dc.l    .method_animate_walk_left-.animate_table        ; 6
                dc.l    .method_animate_walk_right-.animate_table       ; 7
                dc.l    .method_animate_grab_left-.animate_table        ; 8
                dc.l    .method_animate_grab_right-.animate_table       ; 9
                dc.l    .method_animate_turn_delay-.animate_table       ; 10
                dc.l    .method_animate_none-.animate_table      	; 10
                dc.l    .method_animate_none-.animate_table      	; 11
                dc.l    .method_animate_none-.animate_table      	; 12
                dc.l    .method_animate_none-.animate_table      	; 13
                dc.l    .method_animate_none-.animate_table      	; 14
                dc.l    .method_animate_none-.animate_table      	; 15
                dc.l    .method_animate_none-.animate_table      	; 16
                dc.l    .method_animate_none-.animate_table      	; 17
                dc.l    .method_animate_none-.animate_table      	; 18
                dc.l    .method_animate_none-.animate_table      	; 19
                dc.l    .method_animate_none-.animate_table      	; 20
                dc.l    .method_animate_none-.animate_table      	; 21
                dc.l    .method_animate_none-.animate_table      	; 22
                dc.l    .method_animate_none-.animate_table      	; 23
                dc.l    .method_animate_none-.animate_table      	; 24		

		
.method_animate_none:
		bra	.exit		

.sprite_1:      
		cmp.w   #ENEMY_STATUS_SWEEP_RIGHT,THIS_SPRITE_STATUS(a2)                ; if doing bones or sweeps then skip all collisions
                ble     .run_sprite_method
		
		tst.w	SHIELD_TIMER
		bpl.s	.shield_on
                cmp.w   #ENEMY_STATUS_STUN_RIGHT,THIS_SPRITE_STATUS(a2)                 ; if doing stuns then skip Rygar collision
                ble     .sprite_stunned

.shield_on:

; Set the proximity flag
                DRONE_CHECK_AND_SET_PROXIMITY

; Test this enemy collided with Rygar (the player)
.sprite_2:      		
		move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  d2,d4
                add.w   #DRONE_TO_RYGAR_X1_BOUND,d2
                add.w   #DRONE_TO_RYGAR_X2_BOUND,d4			
                move.w  SPRITE_PLOT_YPOS(a1),d3	
                move.w  d3,d5		
                add.w   #DRONE_TO_RYGAR_Y1_BOUND,d3
                add.w   #DRONE_TO_RYGAR_Y2_BOUND,d5
                
		lea     RYGAR_COORDS(a4),a3
                ENEMY_TO_RYGAR_COLLISION
                move.l  #POINTS_DRONE,d2
                tst.w   d6                                      ; Did this sprite collide with Rygar?
                IFEQ    DISABLE_ENEMY_COLLISION
                bmi     .method_set_collision_action            ; Yes
                ENDC

; Check if the enemy has been hit with disk armor
.sprite_stunned:
                move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4
                move.w  d3,d5
                add.w   #DRONE_TO_DISKARM_X1_BOUND,d2
                add.w   #DRONE_TO_DISKARM_Y1_BOUND,d3
                add.w   #DRONE_TO_DISKARM_X2_BOUND,d4
                add.w   #DRONE_TO_DISKARM_Y2_BOUND,d5
                lea     DISKARM_COORDS(a4),a3
                ENEMY_TO_DISKARM_COLLISION
                move.l  #POINTS_DRONE,d2
                tst.w   d6                                      ; Did this sprite get hit with the disk armor
                bmi     .method_set_sweep_direction             ; Yes

                RUN_SPRITE_METHOD

                METHOD_ANIMATE_BONES

                METHOD_ANIMATE_SWEEP_LEFT

                METHOD_ANIMATE_SWEEP_RIGHT

                METHOD_SET_BONES

                METHOD_SET_SWEEP_DIRECTION

                METHOD_SET_STUN_OR_DESTROY

                METHOD_SET_STUN_DIRECTION

                METHOD_SET_RYGAR_DEATH_SEQUENCE

.method_set_collision_action:
                tst.w   RYGAR_FALLPREV_STATE(a4)                                ; Is Rygar falling on enemy
                bmi     .method_set_stun_or_destroy                     ; Yes!

                IFNE    DISABLE_ENEMY_COLLISION
                bra     .exit
                ELSE
                bra.s   .method_set_rygar_death_sequence
                ENDC

; d2=xpos to plot sprite
.method_animate_walk_left:
                moveq   #DRONE_STATUS_WALK_LEFT,d1
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                moveq   #0,d5
                move.w  PRIVATE_DRONE_MOVE_SPEED(a2),d5
                sub.l   d5,d6
                bmi     .method_set_turn_delay

                move.l  #ENEMY_DEF_DRONE,d7
                move.w  d6,d2
                moveq   #1,d5
                IS_PATH_BLOCKED
                beq     .method_set_turn_delay

                UPDATE_SPRITE_PLOT_XPOS
                bra     .animate

.method_animate_walk_right:
                moveq   #DRONE_STATUS_WALK_RIGHT,d1
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                add.w   PRIVATE_DRONE_MOVE_SPEED(a2),d6

                move.l  #ENEMY_DEF_DRONE,d7
                move.w  d6,d2
                moveq   #0,d5
                IS_PATH_BLOCKED
                beq     .method_set_turn_delay

                UPDATE_SPRITE_PLOT_XPOS
                bra     .animate

.method_animate_stun_left:
                moveq   #ENEMY_STATUS_STUN_LEFT,d1
                move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                subq.w  #1,THIS_SPRITE_STUN_TIMER(a2)
                bmi     .method_set_walk_left
                ENEMY_SHAKE
                bra     .animate

.method_animate_stun_right:
                moveq   #ENEMY_STATUS_STUN_RIGHT,d1
                move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                subq.w  #1,THIS_SPRITE_STUN_TIMER(a2)
                bmi     .method_set_walk_right
                ENEMY_SHAKE
                bra     .animate

.method_animate_grab_left:
                tst.w   PRIVATE_DRONE_GRAB_TIMER(a2)                            ; Finished grabbing to the left?
                bmi     .method_reverse_direction
                subq.w  #1,PRIVATE_DRONE_GRAB_TIMER(a2)

                moveq   #DRONE_STATUS_GRAB_LEFT,d1
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                sub.w   PRIVATE_DRONE_MOVE_SPEED(a2),d6
                sub.w   #DRONE_PROXIMITY_GRAB_SPEED,d6
                bmi     .method_set_walk_right

                move.l  #ENEMY_DEF_DRONE,d7
                move.w  d6,d2
                moveq   #1,d5
                IS_PATH_BLOCKED
                beq     .method_set_walk_right

                UPDATE_SPRITE_PLOT_XPOS
                bra     .animate

.method_animate_grab_right:
                tst.w   PRIVATE_DRONE_GRAB_TIMER(a2)                            ; Finished grabbing to the right?
                bmi     .method_reverse_direction
                subq.w  #1,PRIVATE_DRONE_GRAB_TIMER(a2)

                moveq   #DRONE_STATUS_GRAB_RIGHT,d1
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                add.w   PRIVATE_DRONE_MOVE_SPEED(a2),d6
                add.w   #DRONE_PROXIMITY_GRAB_SPEED,d6

                move.l  #ENEMY_DEF_DRONE,d7
                move.w  d6,d2
                moveq   #0,d5
                IS_PATH_BLOCKED
                beq     .method_set_walk_left

                UPDATE_SPRITE_PLOT_XPOS
                bra     .animate

.method_animate_turn_delay:
                tst.w   PRIVATE_DRONE_TURN_TIMER(a2)
                bmi     .method_reverse_direction
                subq.w  #1,PRIVATE_DRONE_TURN_TIMER(a2)
                move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS

                moveq   #DRONE_STATUS_STATIC_LEFT,d1
                tst.w   THIS_SPRITE_DIRECTION(a2)
                bmi.s   .method_animate_turn_delay_0
                moveq   #DRONE_STATUS_STATIC_RIGHT,d1
.method_animate_turn_delay_0:
                bra     .animate

.method_reverse_direction:
                move.w  #DRONE_TURN_DELAY_TIME,PRIVATE_DRONE_TURN_TIMER(a2)
                tst.w   THIS_SPRITE_DIRECTION(a2)
                beq.s   .method_set_walk_left
                bra.s   .method_set_walk_right

.method_destroy_sprite:
                subq.w  #1,DRONES_IN_USE(a4)
                clr.w   THIS_SPRITE_STATUS(a2)
                bra     DESTROY_SPRITE

.method_set_destroy_sprite:
                move.w  #ENEMY_STATUS_DESTROYED,THIS_SPRITE_STATUS(a2)
                bra     .set_update

.method_set_walk_left:
                move.w  #SPRITE_MOVE_LEFT,THIS_SPRITE_DIRECTION(a2)
                move.w  #DRONE_STATUS_WALK_LEFT,THIS_SPRITE_STATUS(a2)
                bra     .set_update

.method_set_walk_right:
                move.w  #SPRITE_MOVE_RIGHT,THIS_SPRITE_DIRECTION(a2)
                move.w  #DRONE_STATUS_WALK_RIGHT,THIS_SPRITE_STATUS(a2)
                bra     .set_update


.method_set_grab_left:
                move.w  #SPRITE_MOVE_LEFT,THIS_SPRITE_DIRECTION(a2)
                move.w  #DRONE_STATUS_GRAB_LEFT,THIS_SPRITE_STATUS(a2)
                move.w  #DRONE_GRAB_TIME,PRIVATE_DRONE_GRAB_TIMER(a2)
                subq.w  #1,PRIVATE_DRONE_GRABS(a2)
                bra     .set_update

.method_set_grab_right:
                move.w  #SPRITE_MOVE_RIGHT,THIS_SPRITE_DIRECTION(a2)
                move.w  #DRONE_STATUS_GRAB_RIGHT,THIS_SPRITE_STATUS(a2)
                move.w  #DRONE_GRAB_TIME,PRIVATE_DRONE_GRAB_TIMER(a2)
                subq.w  #1,PRIVATE_DRONE_GRABS(a2)
                bra     .set_update

.method_set_turn_delay:
                move.w  #DRONE_STATUS_TURN_DELAY,THIS_SPRITE_STATUS(a2)
                move.w  #DRONE_TURN_DELAY_TIME,PRIVATE_DRONE_GRAB_TIMER(a2)
		UPDATE_SPRITE_PLOT_XPOS
                bra     .set_update

.method_set_grab_delay:
                move.w  #DRONE_STATUS_TURN_DELAY,THIS_SPRITE_STATUS(a2)
                move.w  #DRONE_GRAB_DELAY_TIME,PRIVATE_DRONE_GRAB_TIMER(a2)
                bra     .set_update

.set_update:	bra.s	.exit

.animate:	bra	ANIMATE

.exit:          rts

