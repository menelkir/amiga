FISH_GROUND_ENEMY:             equ     1       ; 1 = Ground enemy means enemy is stunnable.
FISH_HWSPR_ENABLE:             equ     0
FISH_HWSPR_UPPER:              equ     0
FISH_HWSPR_MID:                equ     0
FISH_HWSPR_LOWER:              equ     0

FISH_YPOS:			equ	148

FISH_TO_RYGAR_X1_BOUND:        equ     10
FISH_TO_RYGAR_Y1_BOUND:        equ     0
FISH_TO_RYGAR_X2_BOUND:        equ     31-10
FISH_TO_RYGAR_Y2_BOUND:        equ     31

FISH_TO_DISKARM_X1_BOUND:      equ     8
FISH_TO_DISKARM_Y1_BOUND:      equ     0
FISH_TO_DISKARM_X2_BOUND:      equ     23
FISH_TO_DISKARM_Y2_BOUND:      equ     31

FISH_MOVE_SPEED:               equ     64      ; Initial speed.
FISH_SINE_SPEED:               equ     1
FISH_SURFACE_DELAY:		equ	150
FISH_LEAP_DELAY:		equ	22		; 30 frames

FISH_SURFACE:		       equ     6
FISH_LEAP_LEFT:                equ     7
FISH_LEAP_RIGHT:               equ     8

PRIVATE_FISH_MOVE_SPEED:       equ     32
PRIVATE_FISH_TIMER:            equ     34
PRIVATE_FISH_SINE_POINTER:     equ     36
PRIVATE_FISH_FIXED_YPOS:       equ     38
PRIVATE_FISH_DIRECTION:		equ	40
PRIVATE_FISH_BOUNDARY_TIMER:	equ	42
PRIVATE_FISH_START_XPOS:	equ	44

HDL_FISH:
		FUNCID	#$273595fb
                SPRITE_INITIATE

; Set the initial variables and status of the sprite.
.construct:
			
		FIND_CAMERA_XPOS

		move.w  SPRITE_PLOT_XPOS(a1),d2                 ; Set the initial Map Position
                lsl.w   #4,d2                           ; Based on the Plot position after
                move.w  d2,THIS_SPRITE_MAP_XPOS(a2)             ; Enemy is Spawned.
		move.w	d2,PRIVATE_FISH_START_XPOS(a2)
                move.w  #FISH_MOVE_SPEED,PRIVATE_FISH_MOVE_SPEED(a2) ; Set move speed.
                move.w  #-1,THIS_SPRITE_STUN_TIMER(a2)
                move.w  #ENEMY_BOUNDARY_TIME,PRIVATE_FISH_BOUNDARY_TIMER(a2)
		
		move.w	THIS_SPRITE_PARAMETER(a2),d2
		move.w	d2,d3
		and.w	#1,d3
		move.w	d3,PRIVATE_FISH_DIRECTION(a2)
		
		and.w	#$fffe,d2
		move.w	d2,PRIVATE_FISH_TIMER(a2)
		
                ;move.w  #FISH_SURFACE_DELAY,PRIVATE_FISH_TIMER(a2)		
		move.w	#FISH_SURFACE,THIS_SPRITE_STATUS(a2)
		
;;; Init code here.

.active:
                ENEMY_CHECK_END_OF_ROUND

.sprite_0:
; Check boundary of sprite!
                move.w  SPRITE_PLOT_XPOS(a1),d2
                lea     .boundary(pc),a0
		cmp.w	(a0),d2
		blt.s	.dectimer
		cmp.w	2(a0),d2
		bgt.s	.dectimer
		bra.s	.inbound
.dectimer:	subq.w  #1,PRIVATE_FISH_BOUNDARY_TIMER(a2)
                bpl.s   .outbound
                move.w  #ENEMY_STATUS_BONES,THIS_SPRITE_STATUS(a2)
.inbound:       move.w  #ENEMY_BOUNDARY_TIME,PRIVATE_FISH_BOUNDARY_TIMER(a2)
.outbound:      bra.s   .sprite_1

.boundary:      dc.w    1,288


.animate_table: dc.l    .method_animate_none-.animate_table      	; 0
                dc.l    .method_animate_bones-.animate_table            ; 1
                dc.l    .method_animate_sweep_left-.animate_table       ; 2
                dc.l    .method_animate_sweep_right-.animate_table      ; 3
                dc.l    .method_animate_none-.animate_table        	; 4
                dc.l    .method_animate_none-.animate_table       	; 5
                dc.l    .method_animate_surface-.animate_table        	; 6
                dc.l    .method_animate_leap_left-.animate_table        ; 7
                dc.l    .method_animate_leap_right-.animate_table       ; 8
                dc.l    .method_animate_none-.animate_table      	; 9
                dc.l    .method_animate_none-.animate_table      	; 10
                dc.l    .method_animate_none-.animate_table      	; 11
                dc.l    .method_animate_none-.animate_table      	; 12
                dc.l    .method_animate_none-.animate_table      	; 13		
                dc.l    .method_animate_none-.animate_table      	; 14
                dc.l    .method_animate_none-.animate_table      	; 15
                dc.l    .method_animate_none-.animate_table      	; 16
                dc.l    .method_animate_none-.animate_table      	; 17
                dc.l    .method_animate_none-.animate_table      	; 18
                dc.l    .method_animate_none-.animate_table      	; 19
                dc.l    .method_animate_none-.animate_table      	; 20
                dc.l    .method_animate_none-.animate_table      	; 21
                dc.l    .method_animate_none-.animate_table      	; 22
                dc.l    .method_animate_none-.animate_table      	; 23
                dc.l    .method_animate_none-.animate_table      	; 24

.method_animate_none:	bra	.exit

.sprite_1:      cmp.w   #ENEMY_STATUS_SWEEP_RIGHT,THIS_SPRITE_STATUS(a2)                ; if doing bones or sweeps then skip all collisions
                ble     .run_sprite_method

		tst.w	SHIELD_TIMER
		bpl.s	.shield_on
                cmp.w   #FISH_SURFACE,THIS_SPRITE_STATUS(a2)                 ; if doing stuns then skip Rygar collision
                ble     .surface

.shield_on:

;Test this enemy collided with Rygar (the player)
.sprite_2:      move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4                                   ; Check a 32x32 boundary from x1/y1
                move.w  d3,d5
                add.w   #FISH_TO_RYGAR_X1_BOUND,d2
                add.w   #FISH_TO_RYGAR_Y1_BOUND,d3
                add.w   #FISH_TO_RYGAR_X2_BOUND,d4
                add.w   #FISH_TO_RYGAR_Y2_BOUND,d5
                lea     RYGAR_COORDS(a4),a3
                ENEMY_TO_RYGAR_COLLISION
		move.l	#POINTS_FISH,d2
                tst.w   d6                                      ; Did this sprite collide with Rygar?
                IFEQ    DISABLE_ENEMY_COLLISION
                bmi     .method_set_collision_action            ; Yes
                ENDC

; Check if the enemy has been hit with disk armor
.sprite_stunned:
                move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4                                   ; Check a 32x32 boundary from x1/y1
                move.w  d3,d5
                add.w   #FISH_TO_DISKARM_X1_BOUND,d2
                add.w   #FISH_TO_DISKARM_Y1_BOUND,d3
                add.w   #FISH_TO_DISKARM_X2_BOUND,d4
                add.w   #FISH_TO_DISKARM_Y2_BOUND,d5
                lea     DISKARM_COORDS(a4),a3
                ENEMY_TO_DISKARM_COLLISION
		move.l	#POINTS_FISH,d2
                tst.w   d6                                      ; Did this sprite get hit with the disk armor
                bmi     .method_set_sweep_direction             ; Yes

.surface:
; Test this enemy collided with the disk armour

                RUN_SPRITE_METHOD

                METHOD_ANIMATE_BONES

                METHOD_ANIMATE_SWEEP_LEFT

                METHOD_ANIMATE_SWEEP_RIGHT

                METHOD_SET_BONES

                METHOD_SET_SWEEP_DIRECTION

                METHOD_SET_JUMP_OR_DESTROY

                METHOD_SET_STUN_DIRECTION

                METHOD_SET_RYGAR_DEATH_SEQUENCE


.method_set_collision_action:
                tst.w   RYGAR_FALLPREV_STATE(a4)                                ; Is Rygar falling on enemy
                bmi     .method_set_jump_or_destroy                     ; Yes!

                IFNE    DISABLE_ENEMY_COLLISION
                bra     .exit
                ELSE
                bra     .method_set_rygar_death_sequence
                ENDC


.method_destroy_sprite:
		clr.w   THIS_SPRITE_STATUS(a2)
		
.recreate:

		tst.w	DISABLE_ENEMIES(a4)				; Do not recreate if enemies are disabled
		bmi	DESTROY_SPRITE					

		movem.l	d0-d7/a0-a3,-(a7)
		moveq	#SPR_FISH,d0
		move.w	PRIVATE_FISH_START_XPOS(a2),d1
		lsr.w	#4,d1
		move.w	#FISH_YPOS,d2
		moveq	#SPR_TYPE_ENEMY_2X32,d3	
		move.w	THIS_SPRITE_PARAMETER(a2),d4
		lsl.w	#8,d4
		or.w	d4,d3						; Parameter Sets Delay Speed
		lea	HDL_FISH(pc),a0
		bsr	PUSH_SPRITE
		movem.l	(a7)+,d0-d7/a0-a3	
			
		bra     DESTROY_SPRITE	



;------------------------------------------------------------------------------------
; SURFACE
;------------------------------------------------------------------------------------

.method_animate_surface:
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS

                subq.w  #1,PRIVATE_FISH_TIMER(a2)
                bmi     .method_set_leap_direction
                moveq   #FISH_SURFACE,d1
                bra     ANIMATE

.method_set_leap_direction:
		tst.w	PRIVATE_FISH_DIRECTION(a2)
		beq.s	.method_set_leap_left
		bra.s	.method_set_leap_right

;------------------------------------------------------------------------------------
; LEAP LEFT
;------------------------------------------------------------------------------------
.method_set_leap_left:
                move.w  #FISH_LEAP_DELAY,PRIVATE_FISH_TIMER(a2)
                move.w  #FISH_LEAP_LEFT,THIS_SPRITE_STATUS(a2)
                bra     .exit

.method_animate_leap_left:
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
		sub.w	#FISH_MOVE_SPEED,d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS

                subq.w  #1,PRIVATE_FISH_TIMER(a2)
                bmi     .method_destroy_sprite
                moveq   #FISH_LEAP_LEFT,d1
                bra     ANIMATE

;------------------------------------------------------------------------------------
; LEAP RIGHT
;------------------------------------------------------------------------------------
.method_set_leap_right:
                move.w  #FISH_LEAP_DELAY,PRIVATE_FISH_TIMER(a2)
                move.w  #FISH_LEAP_RIGHT,THIS_SPRITE_STATUS(a2)
                bra     .exit

.method_animate_leap_right:
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
		add.w	#FISH_MOVE_SPEED,d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS

                subq.w  #1,PRIVATE_FISH_TIMER(a2)
                bmi     .method_destroy_sprite
                moveq   #FISH_LEAP_RIGHT,d1
                bra     ANIMATE


.set_update:

.exit:          rts
