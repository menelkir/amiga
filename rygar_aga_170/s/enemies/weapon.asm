
WEAPON_GROUND_ENEMY:          equ     0       ; 1 = Ground enemy means enemy is stunnable.
WEAPON_HWSPR_ENABLE:          equ     0
WEAPON_HWSPR_UPPER:           equ     0
WEAPON_HWSPR_MID:             equ     0
WEAPON_HWSPR_LOWER:           equ     0

WEAPON_TO_RYGAR_X1_BOUND:     equ     0
WEAPON_TO_RYGAR_Y1_BOUND:     equ     0
WEAPON_TO_RYGAR_X2_BOUND:     equ     15
WEAPON_TO_RYGAR_Y2_BOUND:     equ     15

WEAPON_BASE_YPOS:             equ     108		; This needs deriving from parameter.
WEAPON_DISPLAY_TIME:		equ	50*20		; 4 seconds

WEAPON_POWER_TIGER:		equ     6
WEAPON_POWER_STAR:		equ     7
WEAPON_POWER_CROWN:		equ     8
WEAPON_POWER_SUN:		equ     9
WEAPON_POWER_CROSS:		equ     10
WEAPON_POWER_CLEAR:		equ	11
WEAPON_POWER_TIMER:		equ	12
WEAPON_POWER_EMBLEM3:		equ     13

PRIVATE_WEAPON_TIMER:         	equ     32		; Used for delay before disappear.
PRIVATE_WEAPON_SINE_POINTER:  	equ     34
PRIVATE_WEAPON_FIXED_YPOS:    	equ     36
PRIVATE_WEAPON_GROUND:		equ	38		; -1 when hit the ground
PRIVATE_WEAPON_DROPPED:		equ	40

KILLS_TO_WEAPON_DROP:		equ	60		; Number of enemy kills before a weapon is dropped		

DROP_WEAPON:
; Get number between 0 and 7
		move.w  VPOSR(a5),d2
                lsr.w	#8,d2
                move.b  $bfe801,d3
		eor.b	d3,d2
                and.w   #$3,d2
		move.w	FRAME_BG(a4),d3
		and.w	#1,d3
		add.w	d3,d2
; d2 has random number between 0 and 4
		lea	.drop_items(pc),a0
		moveq	#4,d7
.loop:		move.w	(a0,d2*2),d4
		
		move.w	RYGAR_CURRENT_POWERS(a4),d3
		btst	d2,d3				; not right, this is a bit set.
		beq.s	.drop
		
; Rygar already has the power..
		addq.w	#1,d2
		cmp.b	#5,d2
		bne.s	.next
		moveq	#0,d2				; Reset to 0
.next:		dbf	d7,.loop

		move.w	#ITEM_EMBLEM3,d4

.drop:
		cmp.w	#ITEM_CROSSPOWER,d4
		bne.s	.even_round

; Cross power selected
		move.w	ROUND_CURRENT(a4),d2				; Only drop Cross power on even rounds.

		cmp.w	#2-1,d2					
		beq.s	.avoid_cross
		cmp.w	#4-1,d2					
		beq.s	.avoid_cross
		cmp.w	#12-1,d2					
		beq.s	.avoid_cross
		cmp.w	#14-1,d2					
		beq.s	.avoid_cross		
		cmp.w	#22-1,d2					; 
		beq.s	.avoid_cross
		cmp.w	#24-1,d2					; 
		beq.s	.avoid_cross
		and.w	#1,d2
		bne.s	.even_round
		
.avoid_cross:
		move.w	#ITEM_EMBLEM3,d4

.even_round:	

		bsr	ROUND_EVENT_INJECT_WEAPON
		bra.s	.exit

.drop_items:	dc.w	ITEM_STARPOWER
		dc.w	ITEM_CROWNPOWER
		dc.w	ITEM_SUNPOWER
		dc.w	ITEM_CROSSPOWER
		dc.w	ITEM_TIGERPOWER
		
		CNOP	0,4
		
.exit:		rts



HDL_WEAPON:
		FUNCID	#$adcf3b62
                SPRITE_INITIATE

; Set the initial variables and status of the sprite.
.construct:
                move.w  SPRITE_PLOT_XPOS(a1),d2                 ; Set the initial Map Position
                lsl.w   #4,d2                           ; Based on the Plot position after
                move.w  d2,THIS_SPRITE_MAP_XPOS(a2)             ; Enemy is Spawned.
                move.w  #SPR_TYPE_ENEMY_1X16,SPRITE_PLOT_TYPE(a1)
		
		clr.w	PRIVATE_WEAPON_GROUND(a2)
		
		move.w	SPRITE_PLOT_YPOS(a1),PRIVATE_WEAPON_FIXED_YPOS(a2)
		move.w	#WEAPON_POWER_STAR,THIS_SPRITE_STATUS(a2)
		
		move.w	THIS_SPRITE_PARAMETER(a2),d2		; Item to display!
		and.w	#$f,d2
		
		cmp.b	#3,d2					; This will be the Emblem 3
		bne.s	.is_power
		move.w	#WEAPON_POWER_EMBLEM3,PRIVATE_WEAPON_DROPPED(a2)
		bra.s	.active

.is_power:		
		subq.w	#5,d2
		move.w	d2,PRIVATE_WEAPON_DROPPED(a2)		; Item that was dropped.
		
		
;;; Init code here.

.active:

.sprite_0:
		cmp.w	#WEAPON_POWER_CLEAR,THIS_SPRITE_STATUS(a2)
		beq	.method_animate_clear
;; Code here....
                bra.s   .sprite_2

.animate_table: dc.l    .method_animate_none-.animate_table             ; 0
                dc.l    .method_animate_none-.animate_table    		; 1
                dc.l    .method_animate_none-.animate_table       	; 2
                dc.l    .method_animate_none-.animate_table      	; 3
		dc.l    .method_animate_none-.animate_table      	; 4
		dc.l    .method_animate_none-.animate_table      	; 5
                dc.l    .method_animate_drop-.animate_table      	; 6
                dc.l    .method_animate_drop-.animate_table    		; 7
                dc.l    .method_animate_drop-.animate_table    		; 8
                dc.l    .method_animate_drop-.animate_table      	; 9
                dc.l    .method_animate_drop-.animate_table      	; 10
                dc.l    .method_animate_clear-.animate_table      	; 11
                dc.l    .method_animate_timer-.animate_table      	; 12
                dc.l    .method_animate_drop-.animate_table      	; 13
                dc.l    .method_animate_none-.animate_table      	; 14
                dc.l    .method_animate_none-.animate_table      	; 15
                dc.l    .method_animate_none-.animate_table      	; 16
                dc.l    .method_animate_none-.animate_table      	; 17
                dc.l    .method_animate_none-.animate_table      	; 18
                dc.l    .method_animate_none-.animate_table      	; 19
		dc.l    .method_animate_none-.animate_table      	; 15
                dc.l    .method_animate_none-.animate_table      	; 16
                dc.l    .method_animate_none-.animate_table      	; 17
                dc.l    .method_animate_none-.animate_table      	; 18
                dc.l    .method_animate_none-.animate_table      	; 19

.method_animate_none:	bra	.exit

;Test this enemy collided with Rygar (the player)
.sprite_2:              move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4                                   ; Check a 32x32 boundary from x1/y1
                move.w  d3,d5
                add.w   #WEAPON_TO_RYGAR_X1_BOUND,d2
                add.w   #WEAPON_TO_RYGAR_Y1_BOUND,d3
                add.w   #WEAPON_TO_RYGAR_X2_BOUND,d4
                add.w   #WEAPON_TO_RYGAR_Y2_BOUND,d5
                lea     RYGAR_COORDS(a4),a3
                ENEMY_TO_RYGAR_COLLISION
		moveq	#0,d2
                tst.w   d6                                      ; Did this sprite collide with Rygar?
                bmi     .method_set_collect_power            ; Yes

; Check if the enemy has been hit with disk armor
.sprite_stunned:


; Test this enemy collided with the disk armour

                RUN_SPRITE_METHOD

.method_destroy_sprite:
                clr.w   THIS_SPRITE_STATUS(a2)
                bra     DESTROY_SPRITE		
		
.method_animate_drop:
                lea     WEAPON_SINE(pc),a0
                move.w  PRIVATE_WEAPON_SINE_POINTER(a2),d2
                move.w  (a0,d2*2),d3
		addq.w  #1,PRIVATE_WEAPON_SINE_POINTER(a2)

		cmp.w   #$8000,d3
                beq.s	.method_set_timer
		
.method_animate_display_0:
                move.w  PRIVATE_WEAPON_FIXED_YPOS(a2),d4
                add.w   d3,d4
                move.w  d4,SPRITE_PLOT_YPOS(a1)

                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS
		move.w	PRIVATE_WEAPON_DROPPED(a2),d1
                bra     ANIMATE
		
.method_set_timer:
		move.w	#WEAPON_POWER_TIMER,THIS_SPRITE_STATUS(a2)
		move.w	#WEAPON_DISPLAY_TIME,PRIVATE_WEAPON_TIMER(a2)
		bra	.exit
		
.method_animate_timer:
		tst.w	PRIVATE_WEAPON_TIMER(a2)
		bmi.s	.method_set_clear
		subq.w	#1,PRIVATE_WEAPON_TIMER(a2)
		
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS
		move.w	PRIVATE_WEAPON_DROPPED(a2),d1
                bra     ANIMATE

.method_set_clear:
		move.w	#WEAPON_POWER_CLEAR,THIS_SPRITE_STATUS(a2)
		move.w	#WEAPON_DISPLAY_TIME,PRIVATE_WEAPON_TIMER(a2)
		bra	.exit


.method_animate_clear:		
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS

		moveq	#WEAPON_POWER_CLEAR,d1
		movem.l	d0-d1/a1-a2,-(a7)
		bsr	ANIMATE
		movem.l	(a7)+,d0-d1/a1-a2
		tst.w	d6
		bmi	.method_destroy_sprite
		bra	.exit
		
.method_set_collect_power:		
; Re-ravel the item that dropped.
		movem.l	d0-d1/a1-a2,-(a7)
		move.w	PRIVATE_WEAPON_DROPPED(a2),d4
		cmp.w	#WEAPON_POWER_EMBLEM3,d4
		bne.s	.pickup_power
		
		moveq   #SND_PICKUP,d0
		bsr     PLAY_SAMPLE
		move.w	#ITEM_EMBLEM3,d4
		bra.s	.pickup_emblem
		
.pickup_power:
		addq.w	#5,d4
		lsl.w	#8,d4
		lsl.w	#2,d4
.pickup_emblem:
		bsr	HANDLE_POWERS



		movem.l	(a7)+,d0-d1/a1-a2
; Add points too.
		bra	.method_set_clear
		nop

.set_update:

.exit:          rts

	CNOP	0,4
	
WEAPON_SINE:
	;dc.w	$0,$1,$2,$3,$4,$5,$7,$8,$9,$a,$b,$c,$d,$f,$10,$11
	;dc.w	$12,$13,$14,$15,$16,$17,$18,$19,$1a,$1b,$1c,$1d,$1e,$1f,$20,$21
	;dc.w	$21,$22,$23,$24,$25,$25,$26,$27,$27,$28,$29,$29,$2a,$2a,$2b,$2b
	;dc.w	$2c,$2c,$2d,$2d,$2d,$2e,$2e,$2e,$2f,$2f,$2f,$2f,$2f,$2f,$2f,$2f,$30
	dc.w	0,0,0,0,0,0,1,1,1,2,3,3,4,4,5,6
	dc.w	7,8,9,10,11,12,13,14,15,16,17,18,19,21,22,23
	dc.w	24,25,26,28,29,30,31,32
	
	dc.w	$8000
	
	CNOP	0,4