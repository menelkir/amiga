
FIREBALL_GROUND_ENEMY:          equ     0       ; 1 = Ground enemy means enemy is stunnable.
FIREBALL_HWSPR_ENABLE:          equ     0
FIREBALL_HWSPR_UPPER:           equ     0
FIREBALL_HWSPR_MID:             equ     0
FIREBALL_HWSPR_LOWER:           equ     0


FIREBALL_TO_RYGAR_X1_BOUND:     equ     0
FIREBALL_TO_RYGAR_Y1_BOUND:     equ     0
FIREBALL_TO_RYGAR_X2_BOUND:     equ     15
FIREBALL_TO_RYGAR_Y2_BOUND:     equ     15

FIREBALL_TO_DISKARM_X1_BOUND:   equ     0
FIREBALL_TO_DISKARM_Y1_BOUND:   equ     0
FIREBALL_TO_DISKARM_X2_BOUND:   equ     15
FIREBALL_TO_DISKARM_Y2_BOUND:   equ     15

FIREBALL_MOVE_SPEED:            equ     20
FIREBALL_BURN_TIME:             equ     50
FIREBALL_BASE_YPOS:             equ     208


FIREBALL_STATUS_BURN:           equ     6
FIREBALL_STATUS_DOWN:           equ     7
FIREBALL_STATUS_UP:             equ     8
FIREBALL_STATUS_EMBLEM:         equ     9

PRIVATE_FIREBALL_SPEED:         equ     32
PRIVATE_FIREBALL_SINE_POINTER:              equ     34
PRIVATE_FIREBALL_FIXED_YPOS:    equ     36
PRIVATE_FIREBALL_BURN_COUNT:    equ     38
PRIVATE_FIREBALL_EMBLEM:        equ     40              ; if set will change to emblem on descent.
PRIVATE_FIREBALL_IS_EMBLEM:     equ     42              ; if set then the Emblem is on display.


HDL_FIREBALL:
		FUNCID	#$adcf3b62
                SPRITE_INITIATE

; Set the initial variables and status of the sprite.
.construct:
                move.w  SPRITE_PLOT_XPOS(a1),d2                 ; Set the initial Map Position
                lsl.w   #4,d2                           ; Based on the Plot position after
                move.w  d2,THIS_SPRITE_MAP_XPOS(a2)             ; Enemy is Spawned.
                move.w  #SPR_TYPE_ENEMY_1X16,SPRITE_PLOT_TYPE(a1)
                move.w  #FIREBALL_STATUS_BURN,THIS_SPRITE_STATUS(a2)
                move.w  #FIREBALL_BASE_YPOS,PRIVATE_FIREBALL_FIXED_YPOS(a2)
                move.w  #FIREBALL_BURN_TIME,PRIVATE_FIREBALL_BURN_COUNT(a2)
		move.w	THIS_SPRITE_PARAMETER(a2),d2
		add.w	d2,PRIVATE_FIREBALL_BURN_COUNT(a2)
                clr.w   PRIVATE_FIREBALL_EMBLEM(a2)
                move.w  FRAME_BG(a4),d2
                and.w   #$1,d2
                bne.s   .active
                move.w  #-1,PRIVATE_FIREBALL_EMBLEM(a2)

;;; Init code here.

.active:
                ENEMY_CHECK_END_OF_ROUND

.sprite_0:
		cmp.w   #FIREBALL_STATUS_BURN,THIS_SPRITE_STATUS(a2)                ; if doing bones or sweeps then skip all collisions
                ble     .run_sprite_method
		
		tst.w	SHIELD_TIMER
		bpl.s	.shield_on
                cmp.w   #ENEMY_STATUS_STUN_RIGHT,THIS_SPRITE_STATUS(a2)                 ; if doing stuns then skip Rygar collision
                ble     .sprite_stunned

.shield_on:
;; Code here....
                bra.s   .sprite_2

.animate_table: dc.l    .method_animate_none-.animate_table             ; 0
                dc.l    .method_animate_bones-.animate_table    	; 1
                dc.l    .method_animate_sweep_left-.animate_table       ; 2
                dc.l    .method_animate_sweep_right-.animate_table      ; 3
		dc.l    .method_animate_none-.animate_table      	; 4
		dc.l    .method_animate_none-.animate_table      	; 5
                dc.l    .method_animate_fireball_burn-.animate_table    ; 6
                dc.l    .method_animate_fireball_down-.animate_table    ; 7
                dc.l    .method_animate_fireball_up-.animate_table      ; 8
                dc.l    .method_animate_none-.animate_table      	; 9
                dc.l    .method_animate_none-.animate_table      	; 10
                dc.l    .method_animate_none-.animate_table      	; 11
                dc.l    .method_animate_none-.animate_table      	; 12
                dc.l    .method_animate_none-.animate_table      	; 13
                dc.l    .method_animate_none-.animate_table      	; 14
                dc.l    .method_animate_none-.animate_table      	; 15
                dc.l    .method_animate_none-.animate_table      	; 16
                dc.l    .method_animate_none-.animate_table      	; 17
                dc.l    .method_animate_none-.animate_table      	; 18
                dc.l    .method_animate_none-.animate_table      	; 19
                dc.l    .method_animate_none-.animate_table      	; 20
                dc.l    .method_animate_none-.animate_table      	; 21
                dc.l    .method_animate_none-.animate_table      	; 22
                dc.l    .method_animate_none-.animate_table      	; 23
                dc.l    .method_animate_none-.animate_table      	; 24

.method_animate_none:	bra	.exit

;Test this enemy collided with Rygar (the player)
.sprite_2:      move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4                                   ; Check a 32x32 boundary from x1/y1
                move.w  d3,d5
                add.w   #FIREBALL_TO_RYGAR_X1_BOUND,d2
                add.w   #FIREBALL_TO_RYGAR_Y1_BOUND,d3
                add.w   #FIREBALL_TO_RYGAR_X2_BOUND,d4
                add.w   #FIREBALL_TO_RYGAR_Y2_BOUND,d5
                lea     RYGAR_COORDS(a4),a3
                ENEMY_TO_RYGAR_COLLISION
		move.l   #POINTS_FIREBALL,d2
                tst.w   d6                                      ; Did this sprite collide with Rygar?
                IFEQ    DISABLE_ENEMY_COLLISION
                bmi     .method_set_collision_action            ; Yes
                ENDC

; Check if the enemy has been hit with disk armor
.sprite_stunned:
                move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4                                   ; Check a 32x32 boundary from x1/y1
                move.w  d3,d5
                add.w   #FIREBALL_TO_DISKARM_X1_BOUND,d2
                add.w   #FIREBALL_TO_DISKARM_Y1_BOUND,d3
                add.w   #FIREBALL_TO_DISKARM_X2_BOUND,d4
                add.w   #FIREBALL_TO_DISKARM_Y2_BOUND,d5
                lea     DISKARM_COORDS(a4),a3
                ENEMY_TO_DISKARM_COLLISION
		move.l   #POINTS_FIREBALL,d2
                tst.w   d6                                      ; Did this sprite get hit with the disk armor
                bmi     .method_set_sweep_direction             ; Yes


; Test this enemy collided with the disk armour

                RUN_SPRITE_METHOD

                METHOD_ANIMATE_BONES

                METHOD_ANIMATE_SWEEP_LEFT

                METHOD_ANIMATE_SWEEP_RIGHT

                METHOD_SET_BONES

                METHOD_SET_SWEEP_DIRECTION

                METHOD_SET_STUN_DIRECTION

                METHOD_SET_RYGAR_DEATH_SEQUENCE


.method_set_collision_action:
                tst.w   RYGAR_FALLPREV_STATE(a4)                                ; Is Rygar falling on enemy
                IFNE    FIREBALL_GROUND_ENEMY
                bmi.s   .method_set_stun_or_destroy                     ; Yes!
                ELSE
                bmi.s   .method_set_jump_or_destroy                     ; Yes!
                ENDC

                tst.w   PRIVATE_FIREBALL_IS_EMBLEM(a2)
                bmi     .collect_emblem


                IFNE    DISABLE_ENEMY_COLLISION
                bra     .exit
                ELSE
                bra     .method_set_rygar_death_sequence
                ENDC

                IFNE    FIREBALL_GROUND_ENEMY
                METHOD_SET_STUN_OR_DESTROY                              ;
                ELSE
                METHOD_SET_JUMP_OR_DESTROY                              ;
                ENDC

.collect_emblem:
		move.l	d0,-(a7)
                moveq   #SND_PICKUP,d0
                bsr     PLAY_SAMPLE		
		move.l	(a7)+,d0
; Add points too.
		bra.s	.method_destroy_sprite
		nop

.method_destroy_sprite:
                clr.w   THIS_SPRITE_STATUS(a2)
                IFNE    FIREBALL_HWSPR_ENABLE
                IFNE    FIREBALL_HWSPR_UPPER
                lea     ENEMY_HWSPR_UPPER_ALLOCATED(a4),a0                      ; Free up hardware sprite
                ENDC
                IFNE    FIREBALL_HWSPR_MID
                lea     ENEMY_HWSPR_MID_ALLOCATED(a4),a0                        ; Free up hardware sprite
                ENDC
                IFNE    FIREBALL_HWSPR_LOWER
                lea     ENEMY_HWSPR_LOWER_ALLOCATED(a4),a0                      ; Free up hardware sprite
                ENDC
                move.w  THIS_SPRITE_HWSLOT(a2),d7
                bmi     .recreate
                FREE_HARDWARE_SPRITE
                ENDC
		
	
; Recreate the destroyed Fireball	
.recreate:

		tst.w	DISABLE_ENEMIES(a4)				; Do not recreate if enemies are disabled
		bmi	DESTROY_SPRITE					

		movem.l	d0-d7/a0-a3,-(a7)
		moveq	#SPR_FIREBALL,d0
		move.w	THIS_SPRITE_MAP_XPOS(a2),d1
		lsr.w	#4,d1
		move.w	#186+8,d2
		moveq	#SPR_TYPE_ENEMY_1X16,d3	
		move.w	THIS_SPRITE_PARAMETER(a2),d4
		lsl.w	#8,d4
		or.w	d4,d3						; Parameter Sets Delay Speed
		lea	HDL_FIREBALL(pc),a0
		bsr	PUSH_SPRITE
		movem.l	(a7)+,d0-d7/a0-a3	
			
		bra     DESTROY_SPRITE	
		

.method_set_fireball_up:
                move.w  #FIREBALL_STATUS_UP,THIS_SPRITE_STATUS(a2)
                clr.w   PRIVATE_FIREBALL_SINE_POINTER(a2)
		cmp.w	#16,SPRITE_PLOT_XPOS(a1)
		ble	.exit
		cmp.w	#320-16,SPRITE_PLOT_XPOS(a1)
		bge	.exit
		move.l	d0,-(a7)
		moveq	#SND_FIREBALL,d0
		bsr	PLAY_SAMPLE
		move.l	(a7)+,d0
                bra     .exit

.method_animate_fireball_down:
.method_animate_fireball_up:
                lea     FIREBALL_SINE(a4),a0
                move.w  PRIVATE_FIREBALL_SINE_POINTER(a2),d2
                addq.w  #1,PRIVATE_FIREBALL_SINE_POINTER(a2)

                moveq   #FIREBALL_STATUS_UP,d1
                move.w  (a0,d2*2),d3

                clr.w   PRIVATE_FIREBALL_IS_EMBLEM(a2)
                cmp.w   #64,PRIVATE_FIREBALL_SINE_POINTER(a2)
                ble.s   .up

                moveq   #FIREBALL_STATUS_DOWN,d1
                tst.w   PRIVATE_FIREBALL_EMBLEM(a2)
                beq.s   .up

                moveq   #FIREBALL_STATUS_EMBLEM,d1
                move.w  #-1,PRIVATE_FIREBALL_IS_EMBLEM(a2)
.up:

                cmp.w   #$8000,d3
                bne.s   .method_animate_fireball_up_0
                bra     .method_set_fireball_burn

.method_animate_fireball_up_0:
                move.w  PRIVATE_FIREBALL_FIXED_YPOS(a2),d4
                add.w   d3,d4
                move.w  d4,SPRITE_PLOT_YPOS(a1)

                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS

                bra     ANIMATE



.method_set_fireball_burn:
                move.w  #FIREBALL_STATUS_BURN,THIS_SPRITE_STATUS(a2)
                move.w  #FIREBALL_BASE_YPOS,PRIVATE_FIREBALL_FIXED_YPOS(a2)
                move.w  #FIREBALL_BURN_TIME,PRIVATE_FIREBALL_BURN_COUNT(a2)
                bra     .exit

.method_animate_fireball_burn:
                subq.w  #1,PRIVATE_FIREBALL_BURN_COUNT(a2)
                tst.w   PRIVATE_FIREBALL_BURN_COUNT(a2)
                bmi     .method_set_fireball_up
                move.w  #FIREBALL_BASE_YPOS,SPRITE_PLOT_YPOS(a1)

                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS

                moveq   #FIREBALL_STATUS_BURN,d1
                bra     ANIMATE


.set_update:

.exit:          rts

