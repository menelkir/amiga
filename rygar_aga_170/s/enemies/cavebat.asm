CAVEBAT_GROUND_ENEMY:          	equ     0       ; 1 = Ground enemy means enemy is stunnable.
CAVEBAT_HWSPR_ENABLE:          	equ     1
CAVEBAT_HWSPR_UPPER:           	equ     1
CAVEBAT_HWSPR_MID:             	equ     0
CAVEBAT_HWSPR_LOWER:           	equ     0

CAVEBAT_TO_RYGAR_X1_BOUND:     	equ     0
CAVEBAT_TO_RYGAR_Y1_BOUND:     	equ     0
CAVEBAT_TO_RYGAR_X2_BOUND:     	equ     32
CAVEBAT_TO_RYGAR_Y2_BOUND:     	equ     32

CAVEBAT_TO_DISKARM_X1_BOUND:	equ     0
CAVEBAT_TO_DISKARM_Y1_BOUND:	equ     0
CAVEBAT_TO_DISKARM_X2_BOUND:	equ     32
CAVEBAT_TO_DISKARM_Y2_BOUND:	equ     32

CAVEBAT_STATUS_MOVEMENT:		equ	6
CAVEBAT_STATUS_PAUSE:			equ	7
CAVEBAT_STATUS_APPROACH:		equ	8
CAVEBAT_STATUS_DEPLOY:			equ	9

CAVEBAT_MOVE_SPEED:		equ     20
CAVEBAT_MOVE_INTERVAL:		equ	300			; Deploy rocket every 10 seconds
CAVEBAT_DEPLOY_INTERVAL:	equ	100			; Deploy rocket every 10 seconds
CAVEBAT_PAUSE_INTERVAL:		equ	50			; Every 10 seconds

CAVEBAT_STATUS_WALK_LEFT:	equ     6

PRIVATE_CAVEBAT_SPEED:		equ     32
PRIVATE_CAVEBAT_FIXED_XPOS:	equ	34
PRIVATE_CAVEBAT_FIXED_YPOS:	equ	36
PRIVATE_CAVEBAT_SINE_XPTR:	equ	38
PRIVATE_CAVEBAT_SINE_YPTR:	equ	40
PRIVATE_CAVEBAT_MOVE_TIMER:	equ	42
PRIVATE_CAVEBAT_DEPLOY_TIMER:	equ	44
PRIVATE_CAVEBAT_PAUSE_TIMER:	equ	46



HDL_CAVEBAT:
		FUNCID	#$76b107a2
		
                SPRITE_INITIATE

; Set the initial variables and status of the sprite.
.construct:
                move.w  SPRITE_PLOT_XPOS(a1),d2                 ; Set the initial Map Position
                lsl.w   #4,d2                           ; Based on the Plot position after
                move.w  d2,THIS_SPRITE_MAP_XPOS(a2)             ; Enemy is Spawned.
                move.w  #CAVEBAT_MOVE_SPEED,PRIVATE_CAVEBAT_SPEED(a2) ; Set move speed.
		move.w	#CAVEBAT_STATUS_MOVEMENT,THIS_SPRITE_STATUS(a2)
		clr.w	PRIVATE_CAVEBAT_FIXED_YPOS(a2)
		move.w	#(320/2)-32,PRIVATE_CAVEBAT_FIXED_XPOS(a2)
		clr.w	PRIVATE_CAVEBAT_SINE_XPTR(a2)
		clr.w	PRIVATE_CAVEBAT_SINE_YPTR(a2)
		move.w	#CAVEBAT_MOVE_INTERVAL,PRIVATE_CAVEBAT_MOVE_TIMER(a2)
		
		move.w  #-1,THIS_SPRITE_HWSLOT(a2)
		lea     ENEMY_HWSPR_UPPER_ALLOCATED(a4),a0                      ; Free up hardware sprite
		
                bsr     ALLOC_HARDWARE_SPRITE
                tst.w   d7
                bmi.s   .active
                move.w  (a0),SPRITE_PLOT_TYPE(a1)                               ; Set hardware type.
                move.w  d7,THIS_SPRITE_HWSLOT(a2)
		
;;; Init code here.

.active:
                ENEMY_CHECK_END_OF_ROUND

.sprite_0:
		cmp.w   #ENEMY_STATUS_SWEEP_RIGHT,THIS_SPRITE_STATUS(a2)                ; if doing bones or sweeps then skip all collisions
                ble     .run_sprite_method
;; Code here....
                bra.s   .sprite_2

.animate_table: dc.l    .method_animate_none-.animate_table             ; 0
                dc.l    .method_animate_bones-.animate_table    	; 1
                dc.l    .method_animate_sweep_left-.animate_table       ; 2
                dc.l    .method_animate_sweep_right-.animate_table      ; 3
		dc.l    .method_animate_none-.animate_table      	; 4
		dc.l    .method_animate_none-.animate_table      	; 5
                dc.l    .method_animate_movement-.animate_table      	; 6
                dc.l    .method_animate_pause-.animate_table      	; 7
		dc.l    .method_animate_none-.animate_table      	; 8
                dc.l    .method_animate_deploy-.animate_table      	; 9
                dc.l    .method_animate_none-.animate_table      	; 10
                dc.l    .method_animate_none-.animate_table      	; 11
                dc.l    .method_animate_none-.animate_table      	; 12
                dc.l    .method_animate_none-.animate_table      	; 13
                dc.l    .method_animate_none-.animate_table      	; 14
                dc.l    .method_animate_none-.animate_table      	; 15
                dc.l    .method_animate_none-.animate_table      	; 16
                dc.l    .method_animate_none-.animate_table      	; 17
                dc.l    .method_animate_none-.animate_table      	; 18
                dc.l    .method_animate_none-.animate_table      	; 19
                dc.l    .method_animate_none-.animate_table      	; 20
                dc.l    .method_animate_none-.animate_table      	; 21
                dc.l    .method_animate_none-.animate_table      	; 22
                dc.l    .method_animate_none-.animate_table      	; 23
                dc.l    .method_animate_none-.animate_table      	; 24

.method_animate_none:	bra	.exit
		
;Test this enemy collided with Rygar (the player)
.sprite_2:              move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4                                   ; Check a 32x32 boundary from x1/y1
                move.w  d3,d5
                add.w   #CAVEBAT_TO_RYGAR_X1_BOUND,d2
                add.w   #CAVEBAT_TO_RYGAR_Y1_BOUND,d3
                add.w   #CAVEBAT_TO_RYGAR_X2_BOUND,d4
                add.w   #CAVEBAT_TO_RYGAR_Y2_BOUND,d5
                lea     RYGAR_COORDS(a4),a3
                ENEMY_TO_RYGAR_COLLISION
                move.l  #POINTS_CAVEBAT,d2
                tst.w   d6                                      ; Did this sprite collide with Rygar?
                IFEQ    DISABLE_ENEMY_COLLISION
                bmi     .method_set_collision_action            ; Yes
                ENDC

; Check if the enemy has been hit with disk armor
.sprite_stunned:
                move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4                                   ; Check a 32x32 boundary from x1/y1
                move.w  d3,d5
                add.w   #CAVEBAT_TO_DISKARM_X1_BOUND,d2
                add.w   #CAVEBAT_TO_DISKARM_Y1_BOUND,d3
                add.w   #CAVEBAT_TO_DISKARM_X2_BOUND,d4
                add.w   #CAVEBAT_TO_DISKARM_Y2_BOUND,d5
                lea     DISKARM_COORDS(a4),a3
                ENEMY_TO_DISKARM_COLLISION
                move.l  #POINTS_CAVEBAT,d2
                tst.w   d6                   		; Did this sprite get hit with the disk armor
                bmi     .method_set_sweep_direction             ; Yes

; Test this enemy collided with the disk armour

                RUN_SPRITE_METHOD

                METHOD_ANIMATE_BONES

                METHOD_ANIMATE_SWEEP_LEFT

                METHOD_ANIMATE_SWEEP_RIGHT

                METHOD_SET_BONES

                METHOD_SET_SWEEP_DIRECTION

                METHOD_SET_STUN_DIRECTION

                METHOD_SET_RYGAR_DEATH_SEQUENCE


.method_set_collision_action:
                tst.w   RYGAR_FALLPREV_STATE(a4)                                ; Is Rygar falling on enemy
                IFNE    CAVEBAT_GROUND_ENEMY
                bmi.s   .method_set_stun_or_destroy                     ; Yes!
                ELSE
                bmi.s   .method_set_jump_or_destroy                     ; Yes!
                ENDC

                IFNE    DISABLE_ENEMY_COLLISION
                bra     .exit
                ELSE
                bra     .method_set_rygar_death_sequence
                ENDC

                IFNE    CAVEBAT_GROUND_ENEMY
                METHOD_SET_STUN_OR_DESTROY                              ;
                ELSE
                METHOD_SET_JUMP_OR_DESTROY                              ;
                ENDC


.method_destroy_sprite: 
		subq.w  #1,CAVEBATS_IN_USE(a4)

                clr.w   THIS_SPRITE_STATUS(a2)
                IFNE    CAVEBAT_HWSPR_ENABLE
                lea     ENEMY_HWSPR_UPPER_ALLOCATED(a4),a0                      ; Free up hardware sprite
                move.w  THIS_SPRITE_HWSLOT(a2),d7
                bmi     DESTROY_SPRITE
                FREE_HARDWARE_SPRITE
                ENDC
                bra     DESTROY_SPRITE

.method_set_movement:
		move.w	#CAVEBAT_STATUS_MOVEMENT,THIS_SPRITE_STATUS(a2)
		move.w	#CAVEBAT_MOVE_INTERVAL,PRIVATE_CAVEBAT_MOVE_TIMER(a2)
		bra	.exit
		
.method_animate_movement:
		cmp.w	#42,PRIVATE_CAVEBAT_FIXED_YPOS(a2)
		bge.s	.y_reached
		addq.w	#1,PRIVATE_CAVEBAT_FIXED_YPOS(a2)
.y_reached:
		subq.w	#1,PRIVATE_CAVEBAT_MOVE_TIMER(a2)
		tst.w	PRIVATE_CAVEBAT_MOVE_TIMER(a2)
		bpl.s	.no_pause
		bra	.method_set_pause
.no_pause:		
		

; ---- Do Y Axis
		lea     CAVEBAT_SINE_Y(a4),a3
.sine_y_loop:   moveq   #0,d3
                move.w  PRIVATE_CAVEBAT_SINE_YPTR(a2),d3
                move.w  (a3,d3*2),d2
                cmp.w   #$8000,d2
                bne.s   .sine_y_apply
                clr.w   PRIVATE_CAVEBAT_SINE_YPTR(a2)
                bra.s   .sine_y_loop

.sine_y_apply:	addq.w  #2,PRIVATE_CAVEBAT_SINE_YPTR(a2)
                add.w   PRIVATE_CAVEBAT_FIXED_YPOS(a2),d2
                move.w  d2,SPRITE_PLOT_YPOS(a1)

; ---- Do X Axis
		lea     CAVEBAT_SINE_Y(a4),a3
.sine_x_loop:   moveq   #0,d3
                move.w  PRIVATE_CAVEBAT_SINE_XPTR(a2),d3
                move.w  (a3,d3*2),d2
                cmp.w   #$8000,d2
                bne.s   .sine_x_apply
                clr.w   PRIVATE_CAVEBAT_SINE_XPTR(a2)
                bra.s   .sine_x_loop

.sine_x_apply:	addq.w  #1,PRIVATE_CAVEBAT_SINE_XPTR(a2)
                add.w   PRIVATE_CAVEBAT_FIXED_XPOS(a2),d2
                move.w  d2,SPRITE_PLOT_XPOS(a1)

		;move.w	#140,SPRITE_PLOT_XPOS(a1)
		;move.w	#50,SPRITE_PLOT_YPOS(a1)

.method_animate_movement_0:
		moveq	#CAVEBAT_STATUS_MOVEMENT,d1
		bra	ANIMATE

.method_set_pause:
		move.w	#CAVEBAT_PAUSE_INTERVAL,PRIVATE_CAVEBAT_PAUSE_TIMER(a2)
		move.w	#CAVEBAT_STATUS_PAUSE,THIS_SPRITE_STATUS(a2)		
		bra	.exit
		
.method_animate_pause:
		tst.w	PRIVATE_CAVEBAT_PAUSE_TIMER(a2)
		bmi	.method_set_deploy
		subq.w	#1,PRIVATE_CAVEBAT_PAUSE_TIMER(a2)	
		moveq	#CAVEBAT_STATUS_MOVEMENT,d1
		bra	ANIMATE
		
		
		
.method_set_deploy:
		move.w	#CAVEBAT_DEPLOY_INTERVAL,PRIVATE_CAVEBAT_DEPLOY_TIMER(a2)
		move.w	#CAVEBAT_STATUS_DEPLOY,THIS_SPRITE_STATUS(a2)		
		
		moveq   #0,d1
                moveq   #SPR_ROCKET,d0           ; sprite number
		move.w	SPRITE_PLOT_XPOS(a1),d1
		add.w	MAP_PIXEL_POSX(a4),d1
		sub.w	#256,d1
		addq.w	#8,d1

                move.w  SPRITE_PLOT_YPOS(a1),d2                  ; yposition
                addq.w	#8,d2
		move.w  #SPR_TYPE_ENEMY_1X16,d3 ; control
                lea	HDL_ROCKET(pc),a0           ; handler
                bsr     PUSH_SPRITE
		
		bra	.exit
		
.method_animate_deploy:
		tst.w	PRIVATE_CAVEBAT_DEPLOY_TIMER(a2)
		bmi	.method_set_movement
		subq.w	#1,PRIVATE_CAVEBAT_DEPLOY_TIMER(a2)	
		moveq	#CAVEBAT_STATUS_DEPLOY,d1
		bra	ANIMATE
				
		
		

.set_update:

.exit:          rts
