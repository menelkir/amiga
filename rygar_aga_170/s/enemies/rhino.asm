ENEMY_RHINO_XPOS:               equ     500
ENEMY_RHINO_YPOS:               equ     114

RHINO_GROUND_ENEMY:             equ     1       ; 1 = Ground enemy means enemy is stunnable.
RHINO_HWSPR_ENABLE:             equ     1
RHINO_HWSPR_UPPER:              equ     0
RHINO_HWSPR_MID:                equ     1
RHINO_HWSPR_LOWER:              equ     0

RHINO_WALK_SPEED:               equ     8

RHINO_STATUS_WALK_LEFT:         equ     6
RHINO_STATUS_WALK_RIGHT         equ     7
RHINO_STATUS_GRAB_LEFT:         equ     8
RHINO_STATUS_GRAB_RIGHT:        equ     9
RHINO_STATUS_TURN_DELAY:        equ     10
RHINO_STATUS_STATIC_LEFT:       equ     11
RHINO_STATUS_STATIC_RIGHT:      equ     12

RHINO_TO_RYGAR_X1_BOUND:        equ     0
RHINO_TO_RYGAR_Y1_BOUND:        equ     16
RHINO_TO_RYGAR_X2_BOUND:        equ     31
RHINO_TO_RYGAR_Y2_BOUND:        equ     31

RHINO_TO_DISKARM_X1_BOUND:      equ     4
RHINO_TO_DISKARM_Y1_BOUND:      equ     22
RHINO_TO_DISKARM_X2_BOUND:      equ     27
RHINO_TO_DISKARM_Y2_BOUND:      equ     31

RHINO_GRABS:                    equ     1
RHINO_PROXIMITY_RANGE:          equ     48
RHINO_PROXIMITY_GRAB_SPEED:     equ     10
RHINO_GRAB_TIME:                equ     40
RHINO_TURN_DELAY_TIME:          equ     10
RHINO_GRAB_DELAY_TIME:          equ     30

; Private Variables
PRIVATE_RHINO_MOVE_SPEED:       equ     32
PRIVATE_RHINO_TURNS:            equ     34      ; Number of times the RHINO can turn to follow Rygar.
PRIVATE_RHINO_GRABS:            equ     36      ; True or False
						;  This will configure the number of times the RHINO will attempt to run and grab Rygar.
						; if zero then the enemy is less dangerous.
PRIVATE_RHINO_GRAB_TIMER:       equ     38
PRIVATE_RHINO_PROXIMITY_LEFT:   equ     40      ;
PRIVATE_RHINO_PROXIMITY_RIGHT:  equ     42      ;
PRIVATE_RHINOX_LAST_ANIM:       equ     44
PRIVATE_RHINO_TURN_TIMER:       equ     46


; Sets the private variable RHINO_IN_PROXIMITY to -1 if true, otherwise 0.
; If enemy is approaching Rygar then check proximity.
; -- If enemy is moving left, and Rygar is to the left then check.
; -- if enemy is moving right, and Rygar is to the right then check.
RHINO_CHECK_AND_SET_PROXIMITY MACRO
                tst.w   PRIVATE_RHINO_GRABS(a2)                         ; Number of grabs done?
                beq     .proximity_out_of_range
                tst.w   PRIVATE_RHINO_GRAB_TIMER(a2)                    ; Timer still running?
                bpl.s   .proximity_out_of_range
                tst.w   RYGAR_FALL_STATE(a4)                            ; Rygar falling or jumping
                bne     .proximity_out_of_range
                tst.w   RYGAR_JUMP_STATE(a4)
                bne     .proximity_out_of_range

                move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  RYGAR_XPOS(a4),d3
; If enemy is moving left then check right
                tst.w   THIS_SPRITE_DIRECTION(a2)                     ; If moving left then check right
                bmi.s   .proximity_right                                ; If moving right then check left
.proximity_left:
                sub.w   #RHINO_PROXIMITY_RANGE,d3
                move.w  d3,d4
                subq.w  #2,d3
                bra.s   .proximity_check

.proximity_right:
                add.w   #RHINO_PROXIMITY_RANGE,d3
                move.w  d3,d4
                addq.w  #2,d4

.proximity_check:
                move.w  d3,PRIVATE_RHINO_PROXIMITY_LEFT(a2)
                move.w  d4,PRIVATE_RHINO_PROXIMITY_RIGHT(a2)

		cmp.w   d3,d2
		blt.s  	.proximity_out_of_range
		cmp.w	d4,d2
		bgt.s	.proximity_out_of_range
		
                ;cmxp2.w  PRIVATE_RHINO_PROXIMITY_LEFT(a2),d2
                ;bcs.s   .proximity_out_of_range                         ;
                
		cmp.w   #RHINO_STATUS_WALK_LEFT,THIS_SPRITE_STATUS(a2)
                beq     .method_set_grab_left
                cmp.w   #RHINO_STATUS_WALK_RIGHT,THIS_SPRITE_STATUS(a2)
                beq     .method_set_grab_right

.proximity_out_of_range:
                ENDM


HDL_RHINO:
		FUNCID	#$da411e8f
                SPRITE_INITIATE

; Set the initial variables and status of the sprite.
.construct:
                FIND_CAMERA_XPOS
		
		move.l	#ENEMY_DEF_RHINO,d5		
		CREATE_ENEMY_LEFT_OR_RIGHT

.create:
; d7 = spawn x position
                move.w  d7,THIS_SPRITE_MAP_XPOS(a2)                     ; Set start position in the map.
                move.w  #RHINO_WALK_SPEED,PRIVATE_RHINO_MOVE_SPEED(a2)
                move.w  d0,d2

                move.w  RANDOM_SEED(a4),d3                                      ; abcd
                and.w   #$3,d2                                          ; 01
                lsl.w   #2,d2
                lsr.w   d2,d3
                and.w   #$f,d3
                lsr.w   #2,d3
                
; Adjust difficulty by increasing speed of sprites
		add.w	ENEMY_SPEED_MODIFIER(a4),d3			; Add sprite speed modifider
		move.w	ROUND_CURRENT(a4),d2				; Take current round
		lsr.w	#1,d2						; divide by 2 and add that
		add.w	d2,d3						; to the speed...  round 32 would be 16.
; Adjust difficulty
		add.w   d3,PRIVATE_RHINO_MOVE_SPEED(a2)
		
		move.w	THIS_SPRITE_PARAMETER(a2),d3
		add.w	d3,PRIVATE_RHINO_MOVE_SPEED(a2)
		
		;move.w	d3,DEBUG_DRAGON_SPEED
		
		
                move.w  #-1,PRIVATE_RHINO_GRAB_TIMER(a2)
                move.w  #RHINO_TURN_DELAY_TIME,PRIVATE_RHINO_TURN_TIMER(a2)
                move.w  #RHINO_GRABS,PRIVATE_RHINO_GRABS(a2)

; Enable Hardware Sprite for this enemy.
                IFNE    RHINO_HWSPR_ENABLE
                move.w  #-1,THIS_SPRITE_HWSLOT(a2)
                IFNE    RHINO_HWSPR_UPPER
                lea     ENEMY_HWSPR_UPP_ALLOCATED(a4),a0                        ; Free up hardware sprite
                ENDC
                IFNE    RHINO_HWSPR_MID
                lea     ENEMY_HWSPR_MID_ALLOCATED(a4),a0                        ; Free up hardware sprite
                ENDC
                IFNE    RHINO_HWSPR_LOWER
                lea     ENEMY_HWSPR_LOWER_ALLOCATED(a4),a0                        ; Free up hardware sprite
                ENDC
                bsr     ALLOC_HARDWARE_SPRITE
                tst.w   d7
                bmi.s   .active
                move.w  (a0),SPRITE_PLOT_TYPE(a1)                               ; Set hardware type.
                move.w  d7,THIS_SPRITE_HWSLOT(a2)
                ENDC	

.active:        ENEMY_CHECK_END_OF_ROUND

.sprite_0:	ENEMY_BOUNDARY_CHECK

		move.w	ENEMY_SPEED_ACCUMULATOR(a4),d2
		add.w	d2,PRIVATE_RHINO_MOVE_SPEED(a2)
		
		bra.s   .sprite_1

.animate_table: dc.l    .method_destroy_sprite-.animate_table   ; 0
                dc.l    .method_animate_bones-.animate_table    ; 1
                dc.l    .method_animate_sweep_left-.animate_table       ; 2
                dc.l    .method_animate_sweep_right-.animate_table      ; 3
                dc.l    .method_animate_stun_left-.animate_table        ; 4
                dc.l    .method_animate_stun_right-.animate_table       ; 5
                dc.l    .method_animate_walk_left-.animate_table        ; 6
                dc.l    .method_animate_walk_right-.animate_table       ; 7
                dc.l    .method_animate_grab_left-.animate_table        ; 8
                dc.l    .method_animate_grab_right-.animate_table       ; 9
                dc.l    .method_animate_turn_delay-.animate_table       ; 10
                dc.l    .method_animate_none-.animate_table      	; 11
                dc.l    .method_animate_none-.animate_table      	; 12
                dc.l    .method_animate_none-.animate_table      	; 13
                dc.l    .method_animate_none-.animate_table      	; 14
                dc.l    .method_animate_none-.animate_table      	; 15
                dc.l    .method_animate_none-.animate_table      	; 16
                dc.l    .method_animate_none-.animate_table      	; 17
                dc.l    .method_animate_none-.animate_table      	; 18
                dc.l    .method_animate_none-.animate_table      	; 19
                dc.l    .method_animate_none-.animate_table      	; 20
                dc.l    .method_animate_none-.animate_table      	; 21
                dc.l    .method_animate_none-.animate_table      	; 22
                dc.l    .method_animate_none-.animate_table      	; 23
                dc.l    .method_animate_none-.animate_table      	; 24
		
.method_animate_none:	bra	.exit

.sprite_1:      cmp.w   #ENEMY_STATUS_SWEEP_RIGHT,THIS_SPRITE_STATUS(a2)                ; if doing bones or sweeps then skip all collisions
                ble     .run_sprite_method
		tst.w	SHIELD_TIMER
		bpl.s	.shield_on
                cmp.w   #ENEMY_STATUS_STUN_RIGHT,THIS_SPRITE_STATUS(a2)                 ; if doing stuns then skip Rygar collision
                ble     .sprite_stunned
.shield_on:

; Set the proximity flag
                RHINO_CHECK_AND_SET_PROXIMITY

; Test this enemy collided with Rygar (the player)
.sprite_2:      move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4
                move.w  d3,d5
                add.w   #RHINO_TO_RYGAR_X1_BOUND,d2
                add.w   #RHINO_TO_RYGAR_Y1_BOUND,d3
                add.w   #RHINO_TO_RYGAR_X2_BOUND,d4
                add.w   #RHINO_TO_RYGAR_Y2_BOUND,d5
                lea     RYGAR_COORDS(a4),a3
                ENEMY_TO_RYGAR_COLLISION
                move.l  #POINTS_RHINO,d2
                tst.w   d6                                      ; Did this sprite collide with Rygar?
                IFEQ    DISABLE_ENEMY_COLLISION
                bmi     .method_set_collision_action            ; Yes
                ENDC

; Check if the enemy has been hit with disk armor
.sprite_stunned:
                move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4
                move.w  d3,d5
                add.w   #RHINO_TO_DISKARM_X1_BOUND,d2
                add.w   #RHINO_TO_DISKARM_Y1_BOUND,d3
                add.w   #RHINO_TO_DISKARM_X2_BOUND,d4
                add.w   #RHINO_TO_DISKARM_Y2_BOUND,d5
                lea     DISKARM_COORDS(a4),a3
                ENEMY_TO_DISKARM_COLLISION
                move.l  #POINTS_RHINO,d2
                tst.w   d6                                      ; Did this sprite get hit with the disk armor
                bmi     .method_set_sweep_direction             ; Yes

                RUN_SPRITE_METHOD

                METHOD_ANIMATE_BONES

                METHOD_ANIMATE_SWEEP_LEFT

                METHOD_ANIMATE_SWEEP_RIGHT

                METHOD_SET_BONES

                METHOD_SET_SWEEP_DIRECTION

                METHOD_SET_STUN_OR_DESTROY

                METHOD_SET_STUN_DIRECTION

                METHOD_SET_RYGAR_DEATH_SEQUENCE

.method_set_collision_action:
                tst.w   RYGAR_FALLPREV_STATE(a4)                                ; Is Rygar falling on enemy
                bmi     .method_set_stun_or_destroy                     ; Yes!

                IFNE    DISABLE_ENEMY_COLLISION
                bra     .exit
                ELSE
                bra.s   .method_set_rygar_death_sequence
                ENDC

; d2=xpos to plot sprite
.method_animate_walk_left:
                moveq   #RHINO_STATUS_WALK_LEFT,d1
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                moveq   #0,d5
                move.w  PRIVATE_RHINO_MOVE_SPEED(a2),d5
                sub.l   d5,d6
                bmi     .method_set_turn_delay

                move.l  #ENEMY_DEF_RHINO,d7
                move.w  d6,d2
                moveq   #1,d5
                IS_PATH_BLOCKED
                beq     .method_set_turn_delay

                UPDATE_SPRITE_PLOT_XPOS
		;move.w	#$AA00,$110.w
                bra     ANIMATE

.method_animate_walk_right:
                moveq   #RHINO_STATUS_WALK_RIGHT,d1
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                add.w   PRIVATE_RHINO_MOVE_SPEED(a2),d6

                move.l  #ENEMY_DEF_RHINO,d7
                move.w  d6,d2
                moveq   #0,d5
                IS_PATH_BLOCKED
                beq     .method_set_turn_delay

                UPDATE_SPRITE_PLOT_XPOS
		;move.w	#$AA01,$110.w
                bra     ANIMATE

.method_animate_stun_left:
                moveq   #ENEMY_STATUS_STUN_LEFT,d1
                move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                subq.w  #1,THIS_SPRITE_STUN_TIMER(a2)
                bmi     .method_set_walk_left
                ENEMY_SHAKE
		;move.w	#$AA02,$110.w
                bra     ANIMATE

.method_animate_stun_right:
                moveq   #ENEMY_STATUS_STUN_RIGHT,d1
                move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                subq.w  #1,THIS_SPRITE_STUN_TIMER(a2)
                bmi     .method_set_walk_right
                ENEMY_SHAKE
		;move.w	#$AA03,$110.w
                bra     ANIMATE

.method_animate_grab_left:
                tst.w   PRIVATE_RHINO_GRAB_TIMER(a2)                            ; Finished grabbing to the left?
                bmi     .method_reverse_direction
                subq.w  #1,PRIVATE_RHINO_GRAB_TIMER(a2)

                moveq   #RHINO_STATUS_GRAB_LEFT,d1
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                sub.w   PRIVATE_RHINO_MOVE_SPEED(a2),d6
                sub.w   #RHINO_PROXIMITY_GRAB_SPEED,d6
                bmi     .method_set_walk_right

                move.l  #ENEMY_DEF_RHINO,d7
                move.w  d6,d2
                moveq   #1,d5
                IS_PATH_BLOCKED
                beq     .method_set_walk_right

                UPDATE_SPRITE_PLOT_XPOS
		;move.w	#$AA04,$110.w
                bra     ANIMATE

.method_animate_grab_right:
                tst.w   PRIVATE_RHINO_GRAB_TIMER(a2)                            ; Finished grabbing to the right?
                bmi     .method_reverse_direction
                subq.w  #1,PRIVATE_RHINO_GRAB_TIMER(a2)

                moveq   #RHINO_STATUS_GRAB_RIGHT,d1
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                add.w   PRIVATE_RHINO_MOVE_SPEED(a2),d6
                add.w   #RHINO_PROXIMITY_GRAB_SPEED,d6

                move.l  #ENEMY_DEF_RHINO,d7
                move.w  d6,d2
                moveq   #0,d5
                IS_PATH_BLOCKED
                beq     .method_set_walk_left

                UPDATE_SPRITE_PLOT_XPOS
		;move.w	#$AA05,$110.w
                bra     ANIMATE

.method_animate_turn_delay:
                tst.w   PRIVATE_RHINO_TURN_TIMER(a2)
                bmi     .method_reverse_direction
                subq.w  #1,PRIVATE_RHINO_TURN_TIMER(a2)
                move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS

                moveq   #RHINO_STATUS_STATIC_LEFT,d1
                tst.w   THIS_SPRITE_DIRECTION(a2)
                bmi.s   .method_animate_turn_delay_0
                moveq   #RHINO_STATUS_STATIC_RIGHT,d1
.method_animate_turn_delay_0:
		;move.w	#$AA06,$110.w
                bra     ANIMATE

.method_reverse_direction:
                move.w  #RHINO_TURN_DELAY_TIME,PRIVATE_RHINO_TURN_TIMER(a2)
                tst.w   THIS_SPRITE_DIRECTION(a2)
                beq.s   .method_set_walk_left
                bra.s   .method_set_walk_right


.method_destroy_sprite: 
		subq.w  #1,RHINOS_IN_USE(a4)

                clr.w   THIS_SPRITE_STATUS(a2)
                IFNE    RHINO_HWSPR_ENABLE
		IFNE    RHINO_HWSPR_UPPER
                lea     ENEMY_HWSPR_UPP_ALLOCATED(a4),a0                        ; Free up hardware sprite
                ENDC
                IFNE    RHINO_HWSPR_MID
                lea     ENEMY_HWSPR_MID_ALLOCATED(a4),a0                        ; Free up hardware sprite
                ENDC
		IFNE    RHINO_HWSPR_LOWER
                lea     ENEMY_HWSPR_LOWER_ALLOCATED(a4),a0                        ; Free up hardware sprite
                ENDC
                move.w  THIS_SPRITE_HWSLOT(a2),d7
                bmi     DESTROY_SPRITE
                FREE_HARDWARE_SPRITE
                ENDC
                bra     DESTROY_SPRITE

.method_set_destroy_sprite:
                move.w  #ENEMY_STATUS_DESTROYED,THIS_SPRITE_STATUS(a2)
                bra     .set_update

.method_set_walk_left:
                move.w  #SPRITE_MOVE_LEFT,THIS_SPRITE_DIRECTION(a2)
                move.w  #RHINO_STATUS_WALK_LEFT,THIS_SPRITE_STATUS(a2)
                bra     .set_update

.method_set_walk_right:
                move.w  #SPRITE_MOVE_RIGHT,THIS_SPRITE_DIRECTION(a2)
                move.w  #RHINO_STATUS_WALK_RIGHT,THIS_SPRITE_STATUS(a2)
                bra     .set_update


.method_set_grab_left:
                move.w  #SPRITE_MOVE_LEFT,THIS_SPRITE_DIRECTION(a2)
                move.w  #RHINO_STATUS_GRAB_LEFT,THIS_SPRITE_STATUS(a2)
                move.w  #RHINO_GRAB_TIME,PRIVATE_RHINO_GRAB_TIMER(a2)
                subq.w  #1,PRIVATE_RHINO_GRABS(a2)
                bra     .set_update

.method_set_grab_right:
                move.w  #SPRITE_MOVE_RIGHT,THIS_SPRITE_DIRECTION(a2)
                move.w  #RHINO_STATUS_GRAB_RIGHT,THIS_SPRITE_STATUS(a2)
                move.w  #RHINO_GRAB_TIME,PRIVATE_RHINO_GRAB_TIMER(a2)
                subq.w  #1,PRIVATE_RHINO_GRABS(a2)
                bra     .set_update

.method_set_turn_delay:
                move.w  #RHINO_STATUS_TURN_DELAY,THIS_SPRITE_STATUS(a2)
                move.w  #RHINO_TURN_DELAY_TIME,PRIVATE_RHINO_GRAB_TIMER(a2)
		UPDATE_SPRITE_PLOT_XPOS
		;move.w	#$AA99,$110.w
                bra     .set_update

.method_set_grab_delay:
                move.w  #RHINO_STATUS_TURN_DELAY,THIS_SPRITE_STATUS(a2)
                move.w  #RHINO_GRAB_DELAY_TIME,PRIVATE_RHINO_GRAB_TIMER(a2)
                bra     .set_update

.set_update:

.exit:          rts

