
LAVAMAN_GROUND_ENEMY:           equ     1       ; 1 = Ground enemy means enemy is stunnable.
LAVAMAN_HWSPR_ENABLE:           equ     0
LAVAMAN_HWSPR_UPPER:            equ     0
LAVAMAN_HWSPR_MID:              equ     0
LAVAMAN_HWSPR_LOWER:            equ     0

LAVAMAN_TO_RYGAR_X1_BOUND:      equ     6
LAVAMAN_TO_RYGAR_Y1_BOUND:      equ     2
LAVAMAN_TO_RYGAR_X2_BOUND:      equ     25
LAVAMAN_TO_RYGAR_Y2_BOUND:      equ     31

LAVAMAN_TO_DISKARM_X1_BOUND:    equ     0
LAVAMAN_TO_DISKARM_Y1_BOUND:    equ     4
LAVAMAN_TO_DISKARM_X2_BOUND:    equ     32
LAVAMAN_TO_DISKARM_Y2_BOUND:    equ     32

LAVAMAN_SPRITE_TEST:		equ	0
LAVAMAN_MOVE_SPEED:             equ     32

LAVAMAN_STATUS_MOVE_LEFT:       equ     6                       ; Sprite comes in moving left from right
LAVAMAN_STATUS_MOVE_RIGHT       equ     7                       ; Sprite comes in moving right from left
LAVAMAN_STATUS_FACE_LEFT:       equ     8                       ; Sprite moves to the surface (two stages)
LAVAMAN_STATUS_FACE_RIGHT:      equ     9                       ; Sprite bounces at the surface
LAVAMAN_STATUS_SPIT_LEFT:       equ     10
LAVAMAN_STATUS_SPIT_RIGHT:      equ     11
LAVAMAN_STATUS_DESCEND_LEFT:    equ     12
LAVAMAN_STATUS_DESCEND_RIGHT:   equ     13
LAVAMAN_STATUS_SURFACE_BUBBLE:  equ     14
LAVAMAN_STATUS_SURFACE_RAISE:   equ     15
LAVAMAN_STATUS_SURFACE_RISE_LEFT:       equ     16
LAVAMAN_STATUS_SURFACE_RISE_RIGHT:      equ     17
LAVAMAN_STATUS_TEST:		equ	8

LAVAMAN_SURFACE_BUBBLE_TIME:    equ     100                     ; Bubble time when active.
LAVAMAN_SURFACE_RAISE_TIME:     equ     30
LAVAMAN_SURFACE_RISE_TIME:      equ     70
LAVAMAN_SURFACE_TIME:           equ     400
LAVAMAN_DESCEND_TIME:           equ     90
LAVAMAN_SPIT_INTERVAL:          equ     36                      ; Spitting interval.
LAVAMAN_SPIT_FRAMES:            equ     10                      ; Spit for 10 frames
LAVAMAN_SPIT_MAX:		equ	3			; Max spits is 3.

PRIVATE_LAVAMAN_SPEED:          equ     32
PRIVATE_LAVAMAN_FRAMECOUNT:     equ     36
PRIVATE_LAVAMAN_FIXED_YPOS:     equ     38
PRIVATE_LAVAMAN_SINE_POINTER    equ     40
PRIVATE_LAVAMAN_SURFACE_POINT:  equ     42
PRIVATE_LAVAMAN_TIMER:          equ     44
PRIVATE_LAVAMAN_SURFACE_TIMER:  equ     46
PRIVATE_LAVAMAN_DESCEND:        equ     48
PRIVATE_LAVAMAN_BOUNDARY_TIMER: equ     50
PRIVATE_LAVAMAN_DIRECTION:      equ     52              ; -1 = left 0=right
PRIVATE_LAVAMAN_SPIT_TIMER:     equ     54
PRIVATE_LAVAMAN_SPIT_COUNT:     equ     56

HDL_LAVAMAN:
		FUNCID	#$7db09901
                SPRITE_INITIATE

; Set the initial variables and status of the sprite.
.construct:
                move.w  MAP_PIXEL_POSX(a4),d2
                add.w   #256,d2
                move.w  d2,SPRITE_PLOT_XPOS(a1)
		lsl.w   #4,d2                           ; Based on the Plot position after
                move.w  d2,THIS_SPRITE_MAP_XPOS(a2)             ; Enemy is Spawned.

                move.w  VPOSR(a5),d2
                swap    d2
                move.b  $bfe801,d2
                lsl.w   #8,d2
                move.b  $bfd800,d2
                and.w   #%0000000011111111,d2           ; up to 1023 delay frames
                move.w  d2,PRIVATE_LAVAMAN_SURFACE_POINT(a2)
                clr.w   PRIVATE_LAVAMAN_DESCEND(a2)

                move.w  #-1,THIS_SPRITE_STUN_TIMER(a2)
                move.w  #ENEMY_BOUNDARY_TIME,PRIVATE_LAVAMAN_BOUNDARY_TIMER(a2)
                move.w  #LAVAMAN_MOVE_SPEED,PRIVATE_LAVAMAN_SPEED(a2)   ; Set move speed.
                move.w  #LAVAMAN_STATUS_MOVE_LEFT,THIS_SPRITE_STATUS(a2)
                move.w  #LAVAMAN_SURFACE_TIME,PRIVATE_LAVAMAN_SURFACE_TIMER(a2)
                move.w  #LAVAMAN_SPIT_INTERVAL,PRIVATE_LAVAMAN_SPIT_TIMER(a2)
		move.w	ROUND_CURRENT(a4),d2
		sub.w	d2,PRIVATE_LAVAMAN_SPIT_TIMER(a2)
		

; Allocate to hardware sprite in lower area.
               lea     ENEMY_HWSPR_LOWER_ALLOCATED(a4),a0                      ; Free up hardware sprite
               bsr     ALLOC_HARDWARE_SPRITE
               tst.w   d7
               bmi.s   .active
               move.w  (a0),SPRITE_PLOT_TYPE(a1)                               ; Set hardware type.
               move.w  d7,THIS_SPRITE_HWSLOT(a2)


;;; Init code here.

.active:
                ENEMY_CHECK_END_OF_ROUND

.sprite_0:
; Check boundary of sprite!
                move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                lea     .boundary(pc),a0
		cmp.w	(a0),d2
		blt.s	.dectimer
		cmp.w	2(a0),d2
		bgt.s	.dectimer
		bra.s	.inbound
.dectimer:	subq.w  #1,PRIVATE_LAVAMAN_BOUNDARY_TIMER(a2)
                bpl.s   .outbound

.inbound:       move.w  #ENEMY_BOUNDARY_TIME,PRIVATE_LAVAMAN_BOUNDARY_TIMER(a2)
.outbound:      bra.s   .sprite_1

.boundary:      dc.w    1,319

.animate_table: dc.l    .method_animate_none-.animate_table             ; 0
                dc.l    .method_animate_bones-.animate_table    	; 1
                dc.l    .method_animate_sweep_left-.animate_table       ; 2
                dc.l    .method_animate_sweep_right-.animate_table      ; 3
                dc.l    .method_animate_stun_left-.animate_table                                ; 4 Reserved for Stun
                dc.l    .method_animate_stun_right-.animate_table                               ; 5 Reserved for Stun
                dc.l    .method_animate_move_left-.animate_table        ; 6
                dc.l    .method_animate_move_right-.animate_table       ; 7
                dc.l    .method_animate_face_left-.animate_table        ; 8
                dc.l    .method_animate_face_right-.animate_table       ; 9
                dc.l    .method_animate_spit_left-.animate_table        ; 10
                dc.l    .method_animate_spit_right-.animate_table       ; 11
                dc.l    .method_animate_descend_left-.animate_table     ; 12
                dc.l    .method_animate_descend_right-.animate_table    ; 13
                dc.l    .method_animate_surface_bubble-.animate_table   ; 14
                dc.l    .method_animate_surface_raise-.animate_table    ; 15
                dc.l    .method_animate_surface_rise_left-.animate_table        ; 16
                dc.l    .method_animate_surface_rise_right-.animate_table       ; 17
                dc.l    .method_animate_none-.animate_table        ; 18
                dc.l    .method_animate_none-.animate_table       ; 19
                dc.l    .method_animate_none-.animate_table       ; 20
                dc.l    .method_animate_none-.animate_table       ; 21
                dc.l    .method_animate_none-.animate_table       ; 22
                dc.l    .method_animate_none-.animate_table       ; 23
                dc.l    .method_animate_none-.animate_table       ; 24

		
.method_animate_none:
		bra	.exit

.sprite_1:      
		cmp.w   #ENEMY_STATUS_SWEEP_RIGHT,THIS_SPRITE_STATUS(a2)                ; if doing bones or sweeps then skip all collisions
                ble     .run_sprite_method

		tst.w	SHIELD_TIMER
		bpl.s	.shield_on
                cmp.w   #ENEMY_STATUS_STUN_RIGHT,THIS_SPRITE_STATUS(a2)                 ; if doing stuns then skip Rygar collision
                ble     .sprite_stunned

.shield_on:

;Test this enemy collided with Rygar (the player)
.sprite_2:              move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4                                   ; Check a 32x32 boundary from x1/y1
                move.w  d3,d5
                add.w   #LAVAMAN_TO_RYGAR_X1_BOUND,d2
                add.w   #LAVAMAN_TO_RYGAR_Y1_BOUND,d3
                add.w   #LAVAMAN_TO_RYGAR_X2_BOUND,d4
                add.w   #LAVAMAN_TO_RYGAR_Y2_BOUND,d5
                lea     RYGAR_COORDS(a4),a3
                ENEMY_TO_RYGAR_COLLISION
                move.l  #POINTS_LAVAMAN,d2
                tst.w   d6                                      ; Did this sprite collide with Rygar?
                IFEQ    DISABLE_ENEMY_COLLISION
                bmi     .method_set_collision_action            ; Yes
                ENDC

; Check if the enemy has been hit with disk armor
.sprite_stunned:
                move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4                                   ; Check a 32x32 boundary from x1/y1
                move.w  d3,d5
                add.w   #LAVAMAN_TO_DISKARM_X1_BOUND,d2
                add.w   #LAVAMAN_TO_DISKARM_Y1_BOUND,d3
                add.w   #LAVAMAN_TO_DISKARM_X2_BOUND,d4
                add.w   #LAVAMAN_TO_DISKARM_Y2_BOUND,d5
                lea     DISKARM_COORDS(a4),a3
                ENEMY_TO_DISKARM_COLLISION
                move.l  #POINTS_LAVAMAN,d2
                tst.w   d6                                      ; Did this sprite get hit with the disk armor
                bmi     .method_set_sweep_direction             ; Yes

; Test this enemy collided with the disk armour

                RUN_SPRITE_METHOD

                METHOD_ANIMATE_BONES

                METHOD_ANIMATE_SWEEP_LEFT

                METHOD_ANIMATE_SWEEP_RIGHT

                METHOD_SET_BONES

                METHOD_SET_SWEEP_DIRECTION

                METHOD_SET_STUN_OR_DESTROY

.method_set_stun_direction:
                move.w  #146,SPRITE_PLOT_YPOS(a1)
                move.w  #TIGER_STUN_STATIC_TIME,THIS_SPRITE_STUN_TIMER(a2)              ; Set timer
		move.w	ROUND_CURRENT(a4),d2
		lsl.w	#2,d2
		sub.w	d2,THIS_SPRITE_STUN_TIMER(a2)
		tst.w   PRIVATE_LAVAMAN_DIRECTION(a2)
                bmi.s   .method_set_stun_left
                bra.s   .method_set_stun_right

.method_set_stun_left:
                move.w  #ENEMY_STATUS_STUN_LEFT,THIS_SPRITE_STATUS(a2)
                bra     .set_update

.method_set_stun_right:
                move.w  #ENEMY_STATUS_STUN_RIGHT,THIS_SPRITE_STATUS(a2)
                bra     .set_update

                METHOD_SET_RYGAR_DEATH_SEQUENCE


.method_set_collision_action:
                tst.w   RYGAR_FALLPREV_STATE(a4)                                ; Is Rygar falling on enemy
                bmi     .method_set_stun_or_destroy                     ; Yes!

		tst.w	SHIELD_TIMER
		bpl.s	.shield_on_1

                cmp.w   #LAVAMAN_STATUS_DESCEND_RIGHT,THIS_SPRITE_STATUS(a2)
                bgt     .sprite_stunned

.shield_on_1:
                IFNE    DISABLE_ENEMY_COLLISION
                bra     .exit
                ELSE
                bra     .method_set_rygar_death_sequence
                ENDC


.method_destroy_sprite:
                subq.w  #1,LAVAMEN_IN_USE(a4)

                clr.w   THIS_SPRITE_STATUS(a2)

                move.w  THIS_SPRITE_HWSLOT(a2),d7
                lea     ENEMY_HWSPR_MID_ALLOCATED(a4),a0                        ; Free up hardware sprite
                FREE_HARDWARE_SPRITE

                bra     DESTROY_SPRITE

.method_animate_stun_left:
                moveq   #ENEMY_STATUS_STUN_LEFT,d1
                move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                subq.w  #1,THIS_SPRITE_STUN_TIMER(a2)
                bmi     .method_set_face_left
                ENEMY_SHAKE
                bra     ANIMATE

.method_animate_stun_right:
                moveq   #ENEMY_STATUS_STUN_RIGHT,d1
                move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                subq.w  #1,THIS_SPRITE_STUN_TIMER(a2)
                bmi     .method_set_face_right
                ENEMY_SHAKE
                bra     ANIMATE

;------------------------------------------------------------------------------------
; MOVE LEFT
;------------------------------------------------------------------------------------
.method_set_move_left:
                move.w  #SPRITE_MOVE_LEFT,THIS_SPRITE_DIRECTION(a2)
                move.w  #LAVAMAN_STATUS_MOVE_LEFT,THIS_SPRITE_STATUS(a2)
		move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS		
                bra     .exit

.method_animate_move_left:
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                tst.w   PRIVATE_LAVAMAN_DESCEND(a2)
                bmi.s   .method_animate_move_left_0
                subq.w  #1,PRIVATE_LAVAMAN_SURFACE_POINT(a2)
                tst.w   PRIVATE_LAVAMAN_SURFACE_POINT(a2)
                bmi     .method_set_surface_bubble
                moveq   #0,d2
                cmp.w   SPRITE_PLOT_XPOS(a1),d2                         ; Need this to be a range.
                bge.s   .method_set_move_right
.method_animate_move_left_0:
                sub.w   #LAVAMAN_MOVE_SPEED,d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS
                moveq   #LAVAMAN_STATUS_MOVE_LEFT,d1
                bra     ANIMATE


;------------------------------------------------------------------------------------
; MOVE RIGHT
;------------------------------------------------------------------------------------
.method_set_move_right:
                move.w  #SPRITE_MOVE_RIGHT,THIS_SPRITE_DIRECTION(a2)
                move.w  #LAVAMAN_STATUS_MOVE_RIGHT,THIS_SPRITE_STATUS(a2)
		move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS		
                bra     .exit

.method_animate_move_right:
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                tst.w   PRIVATE_LAVAMAN_DESCEND(a2)
                bmi.s   .method_animate_move_right_0
                subq.w  #1,PRIVATE_LAVAMAN_SURFACE_POINT(a2)
                tst.w   PRIVATE_LAVAMAN_SURFACE_POINT(a2)
                bmi     .method_set_surface_bubble
                move.w  #287,d2
                cmp.w   SPRITE_PLOT_XPOS(a1),d2                         ; Need this to be a range.
                ble     .method_set_move_left
.method_animate_move_right_0:
                add.w   #LAVAMAN_MOVE_SPEED,d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS

                moveq   #LAVAMAN_STATUS_MOVE_RIGHT,d1
                bra     ANIMATE


;------------------------------------------------------------------------------------
; SURFACE_BUBBLE
;------------------------------------------------------------------------------------
.method_set_surface_bubble:
                move.w  #LAVAMAN_STATUS_SURFACE_BUBBLE,THIS_SPRITE_STATUS(a2)
                move.w  #LAVAMAN_SURFACE_BUBBLE_TIME,PRIVATE_LAVAMAN_TIMER(a2)
                bra     .exit

.method_animate_surface_bubble:
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                subq.w  #1,PRIVATE_LAVAMAN_TIMER(a2)
                tst.w   PRIVATE_LAVAMAN_TIMER(a2)
                bmi.s   .method_set_surface_raise
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS
                moveq   #LAVAMAN_STATUS_SURFACE_BUBBLE,d1
                bra     ANIMATE

;------------------------------------------------------------------------------------
; SURFACE_RAISE
;------------------------------------------------------------------------------------
.method_set_surface_raise:
                move.w  #LAVAMAN_STATUS_SURFACE_RAISE,THIS_SPRITE_STATUS(a2)
                move.w  #LAVAMAN_SURFACE_RAISE_TIME,PRIVATE_LAVAMAN_TIMER(a2)
                bra     .exit

.method_animate_surface_raise:
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                subq.w  #1,PRIVATE_LAVAMAN_TIMER(a2)
                tst.w   PRIVATE_LAVAMAN_TIMER(a2)
                bmi.s   .method_set_surface_rise
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS
                moveq   #LAVAMAN_STATUS_SURFACE_RAISE,d1
                bra     ANIMATE

; Check where Rygar is...
.method_set_surface_rise:
                move.w  RYGAR_XPOS(a4),d2
                cmp.w   SPRITE_PLOT_XPOS(a1),d2
                ble     .method_set_surface_rise_right
                bra     .method_set_surface_rise_left

;------------------------------------------------------------------------------------
; SURFACE_RISE_LEFT
;------------------------------------------------------------------------------------
.method_set_surface_rise_left:
                move.w  #LAVAMAN_STATUS_SURFACE_RISE_LEFT,THIS_SPRITE_STATUS(a2)
                move.w  #LAVAMAN_SURFACE_RISE_TIME,PRIVATE_LAVAMAN_TIMER(a2)
; Deallocate from lower hardware sprite
; Allocate to mid hardware sprite.
                move.w  THIS_SPRITE_HWSLOT(a2),d7
                lea     ENEMY_HWSPR_LOWER_ALLOCATED(a4),a0                      ; Free up hardware sprite
                FREE_HARDWARE_SPRITE

                lea     ENEMY_HWSPR_MID_ALLOCATED(a4),a0                        ; Free up hardware sprite
                bsr     ALLOC_HARDWARE_SPRITE
                move.w  #146,SPRITE_PLOT_YPOS(a1)                       ; set mid area
		
		tst.w	d7
		bmi.s	.method_set_surface_rise_left_0	
                move.w  (a0),SPRITE_PLOT_TYPE(a1)                       ; Set hardware type.
                move.w  d7,THIS_SPRITE_HWSLOT(a2)
                bra     .exit

.method_set_surface_rise_left_0:
		move.w	#SPR_TYPE_ENEMY_2X32,SPRITE_PLOT_TYPE(a1)
		move.w	d7,THIS_SPRITE_HWSLOT(a2)
		bra	.exit

.method_animate_surface_rise_left:
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                subq.w  #1,PRIVATE_LAVAMAN_TIMER(a2)
                tst.w   PRIVATE_LAVAMAN_TIMER(a2)
                bmi     .method_set_face_left
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS
                moveq   #LAVAMAN_STATUS_SURFACE_RISE_LEFT,d1
                bra     ANIMATE

;------------------------------------------------------------------------------------
; SURFACE_RISE_RIGHT
;------------------------------------------------------------------------------------
.method_set_surface_rise_right:
                move.w  #LAVAMAN_STATUS_SURFACE_RISE_RIGHT,THIS_SPRITE_STATUS(a2)
                move.w  #LAVAMAN_SURFACE_RISE_TIME,PRIVATE_LAVAMAN_TIMER(a2)
                move.w  THIS_SPRITE_HWSLOT(a2),d7
                lea     ENEMY_HWSPR_LOWER_ALLOCATED(a4),a0                      ; Free up hardware sprite
                FREE_HARDWARE_SPRITE
                lea     ENEMY_HWSPR_MID_ALLOCATED(a4),a0                        ; Free up hardware sprite
                bsr     ALLOC_HARDWARE_SPRITE
                move.w  #146,SPRITE_PLOT_YPOS(a1)                       ; set mid area
		tst.w	d7
		bmi.s	.method_set_surface_rise_right_0	
                move.w  (a0),SPRITE_PLOT_TYPE(a1)                       ; Set hardware type.
                move.w  d7,THIS_SPRITE_HWSLOT(a2)
                bra     .exit

.method_set_surface_rise_right_0:
		move.w	#SPR_TYPE_ENEMY_2X32,SPRITE_PLOT_TYPE(a1)
		move.w	d7,THIS_SPRITE_HWSLOT(a2)
		bra	.exit

.method_animate_surface_rise_right:
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                subq.w  #1,PRIVATE_LAVAMAN_TIMER(a2)
                tst.w   PRIVATE_LAVAMAN_TIMER(a2)
                bmi     .method_set_face_right
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS
                moveq   #LAVAMAN_STATUS_SURFACE_RISE_RIGHT,d1
                bra     ANIMATE



;------------------------------------------------------------------------------------
; FACE LEFT
;------------------------------------------------------------------------------------
.method_set_face_left:
                move.w  #LAVAMAN_STATUS_FACE_LEFT,THIS_SPRITE_STATUS(a2)
                move.w  #-1,PRIVATE_LAVAMAN_DIRECTION(a2)
                ;move.w  #LAVAMAN_SPIT_INTERVAL,PRIVATE_LAVAMAN_SPIT_TIMER(a2)
		moveq	#LAVAMAN_SPIT_INTERVAL,d2
		sub.w	ROUND_CURRENT(a4),d2
		move.w	d2,PRIVATE_LAVAMAN_SPIT_TIMER(a2)
                bra     .exit

.method_animate_face_left:
                cmp.w	#3,PRIVATE_LAVAMAN_SPIT_COUNT(a2)
		beq	.method_set_descend_left

                move.w  FRAME_BG(a4),d2
                and.w   #1,d2
                ;bne    .method_animate_face_left_0

                subq.w  #1,PRIVATE_LAVAMAN_SPIT_TIMER(a2)
                tst.w   PRIVATE_LAVAMAN_SPIT_TIMER(a2)
                bmi     .method_set_spit_left

.method_animate_face_left_0:
                move.w  RYGAR_XPOS(a4),d2
                cmp.w   SPRITE_PLOT_XPOS(a1),d2
                bge     .method_set_face_right
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS
                moveq   #LAVAMAN_STATUS_FACE_LEFT,d1
                bra     ANIMATE


;------------------------------------------------------------------------------------
; FACE RIGHT
;------------------------------------------------------------------------------------
.method_set_face_right:
                move.w  #LAVAMAN_STATUS_FACE_RIGHT,THIS_SPRITE_STATUS(a2)
                clr.w   PRIVATE_LAVAMAN_DIRECTION(a2)
                ;move.w  #LAVAMAN_SPIT_INTERVAL,PRIVATE_LAVAMAN_SPIT_TIMER(a2)
		moveq	#LAVAMAN_SPIT_INTERVAL,d2
		sub.w	ROUND_CURRENT(a4),d2
		move.w	d2,PRIVATE_LAVAMAN_SPIT_TIMER(a2)
                bra     .exit

.method_animate_face_right:
                cmp.w	#3,PRIVATE_LAVAMAN_SPIT_COUNT(a2)
		beq	.method_set_descend_right

                move.w  FRAME_BG(a4),d2
                and.w   #1,d2
                ;beq    .method_animate_face_right_0

                subq.w  #1,PRIVATE_LAVAMAN_SPIT_TIMER(a2)
                tst.w   PRIVATE_LAVAMAN_SPIT_TIMER(a2)
                bmi     .method_set_spit_right

.method_animate_face_right_0:
                move.w  RYGAR_XPOS(a4),d2
                cmp.w   SPRITE_PLOT_XPOS(a1),d2
                blt     .method_set_face_left
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS

                moveq   #LAVAMAN_STATUS_FACE_RIGHT,d1
                bra     ANIMATE


;------------------------------------------------------------------------------------
; SPIT LEFT
;------------------------------------------------------------------------------------
.method_set_spit_left:
                move.w  #LAVAMAN_STATUS_SPIT_LEFT,THIS_SPRITE_STATUS(a2)
                move.w  #LAVAMAN_SPIT_FRAMES,PRIVATE_LAVAMAN_SPIT_TIMER(a2)
		move.w	ROUND_CURRENT(a4),d2
		sub.w	d2,PRIVATE_LAVAMAN_SPIT_TIMER(a2)		
		addq.w	#1,PRIVATE_LAVAMAN_SPIT_COUNT(a2)
		
                movem.l d0-d7/a0-a3,-(a7)
                moveq   #SPR_LAVASPIT,d0
                move.w  THIS_SPRITE_MAP_XPOS(a2),d1
                lsr.w   #4,d1
                move.w  #192,d2
                moveq   #SPR_TYPE_ENEMY_1X16,d3
                lea     HDL_LAVASPIT(pc),a0
                bsr     PUSH_SPRITE
                movem.l (a7)+,d0-d7/a0-a3
                bra     .exit

.method_animate_spit_left:
                subq.w  #1,PRIVATE_LAVAMAN_SPIT_TIMER(a2)
                tst.w   PRIVATE_LAVAMAN_SPIT_TIMER(a2)
                bmi     .method_set_face_left

                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS
                moveq   #LAVAMAN_STATUS_SPIT_LEFT,d1
                bra     ANIMATE


;------------------------------------------------------------------------------------
; SPIT RIGHT
;------------------------------------------------------------------------------------
.method_set_spit_right:
                move.w  #LAVAMAN_STATUS_SPIT_RIGHT,THIS_SPRITE_STATUS(a2)
                move.w  #LAVAMAN_SPIT_FRAMES,PRIVATE_LAVAMAN_SPIT_TIMER(a2)
		addq.w	#1,PRIVATE_LAVAMAN_SPIT_COUNT(a2)
		move.w	ROUND_CURRENT(a4),d2
		sub.w	d2,PRIVATE_LAVAMAN_SPIT_TIMER(a2)
		
                movem.l d0-d7/a0-a3,-(a7)
                moveq   #SPR_LAVASPIT,d0
                move.w  THIS_SPRITE_MAP_XPOS(a2),d1
                lsr.w   #4,d1
                move.w  #192,d2
                moveq   #SPR_TYPE_ENEMY_1X16,d3
                lea     HDL_LAVASPIT(pc),a0
                bsr     PUSH_SPRITE
                movem.l (a7)+,d0-d7/a0-a3
                bra     .exit

.method_animate_spit_right:
                subq.w  #1,PRIVATE_LAVAMAN_SPIT_TIMER(a2)
                tst.w   PRIVATE_LAVAMAN_SPIT_TIMER(a2)
                bmi     .method_set_face_right

                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS
                moveq   #LAVAMAN_STATUS_SPIT_RIGHT,d1
                bra     ANIMATE

;------------------------------------------------------------------------------------
; DESCEND_LEFT
;------------------------------------------------------------------------------------
.method_set_descend_left:
                move.w  #LAVAMAN_STATUS_DESCEND_LEFT,THIS_SPRITE_STATUS(a2)
                move.w  #LAVAMAN_DESCEND_TIME,PRIVATE_LAVAMAN_TIMER(a2)
                move.w  #-1,PRIVATE_LAVAMAN_DESCEND(a2)
                bra     .exit

.method_animate_descend_left:
                subq.w  #1,PRIVATE_LAVAMAN_TIMER(a2)
                tst.w   PRIVATE_LAVAMAN_TIMER(a2)
                bmi     .method_destroy_sprite

                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS
                moveq   #LAVAMAN_STATUS_DESCEND_LEFT,d1
                bra     ANIMATE

;------------------------------------------------------------------------------------
; DESCEND_RIGHT
;------------------------------------------------------------------------------------
.method_set_descend_right:
                move.w  #LAVAMAN_STATUS_DESCEND_RIGHT,THIS_SPRITE_STATUS(a2)
                move.w  #LAVAMAN_DESCEND_TIME,PRIVATE_LAVAMAN_TIMER(a2)
                move.w  #-1,PRIVATE_LAVAMAN_DESCEND(a2)
                bra     .exit

.method_animate_descend_right:
                subq.w  #1,PRIVATE_LAVAMAN_TIMER(a2)
                tst.w   PRIVATE_LAVAMAN_TIMER(a2)
                bmi     .method_destroy_sprite

                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS
                moveq   #LAVAMAN_STATUS_DESCEND_RIGHT,d1
                bra     ANIMATE


.set_update:

.exit:          rts

