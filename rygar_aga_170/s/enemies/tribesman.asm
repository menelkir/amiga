TRIBESMAN_FORMATION_STACK:	equ	0	; Stack facing left
TRIBESMAN_FORMATION_LEFTATTACK:	equ	1	; Attack from Right leaping left
TRIBESMAN_FORMATION_LEFT_OBS:	equ	2	; Attack from Right leaping left
TRIBESMAN_FORMATION_RIGHT_OBS:	equ	3	; Attack from Left 

TRIBESMAN_GROUND_ENEMY:         equ     0       ; 1 = Ground enemy means enemy is stunnable.
TRIBESMAN_HWSPR_ENABLE:         equ     0
TRIBESMAN_HWSPR_UPPER:          equ     0
TRIBESMAN_HWSPR_MID:            equ     0
TRIBESMAN_HWSPR_LOWER:          equ     0

TRIBESMAN_STATUS_STACK_LEFT:    equ     6
TRIBESMAN_STATUS_STACK_RIGHT:   equ     7
TRIBESMAN_STATUS_FLY_LEFT:      equ     8
TRIBESMAN_STATUS_FLY_RIGHT:     equ     9
TRIBESMAN_STATUS_LEAP_LEFT:     equ     8
TRIBESMAN_STATUS_LEAP_RIGHT:    equ     9
TRIBESMAN_STATUS_CLIMB_LEFT:    equ     10
TRIBESMAN_STATUS_CLIMB_RIGHT:   equ     11
TRIBESMAN_STATUS_HOLD_LEFT:     equ     12
TRIBESMAN_STATUS_HOLD_RIGHT:    equ     13
TRIBESMAN_STATUS_RUN_LEFT:      equ     14
TRIBESMAN_STATUS_RUN_RIGHT:     equ     15
TRIBESMAN_STATUS_JUMP_LEFT:     equ     16
TRIBESMAN_STATUS_JUMP_RIGHT:    equ     17

TRIBESMAN_TO_RYGAR_X1_BOUND:    equ     0
TRIBESMAN_TO_RYGAR_Y1_BOUND:    equ     0
TRIBESMAN_TO_RYGAR_X2_BOUND:    equ     32
TRIBESMAN_TO_RYGAR_Y2_BOUND:    equ     32

TRIBESMAN_TO_DISKARM_X1_BOUND:  equ     4
TRIBESMAN_TO_DISKARM_Y1_BOUND:  equ     4
TRIBESMAN_TO_DISKARM_X2_BOUND:  equ     27
TRIBESMAN_TO_DISKARM_Y2_BOUND:  equ     27

TRIBESMAN_MOVE_SPEED:           equ     32
TRIBESMAN_MOVE_SPEED2:           equ     42

;TRIBESMAN_STATUS_WALK_LEFT:    equ     6

PRIVATE_TRIBESMAN_MOVE_SPEED:                equ     32
PRIVATE_TRIBESMAN_FIXED_YPOS:           equ     34
PRIVATE_TRIBESMAN_ANIMPOINTER:          equ     36
PRIVATE_TRIBESMAN_DIRECTION:            equ     40
PRIVATE_TRIBESMAN_HOLD_TIMER:           equ     42
PRIVATE_TRIBESMAN_SINE_POINTER:         equ     44
PRIVATE_TRIBESMAN_CASCADE_COUNTER:	equ	46
PRIVATE_TRIBESMAN_CASCADING:		equ	48
PRIVATE_TRIBESMAN_FORMATION_FRAMES:	equ	52
PRIVATE_TRIBESMAN_FORMATION_PTR:	equ	54		; Long word

HDL_TRIBESMAN:
		FUNCID	#$571f952a
                SPRITE_INITIATE

; Set the initial variables and status of the sprite.
.construct:
                move.w  SPRITE_PLOT_XPOS(a1),d2                 ; Set the initial Map Position
                lsl.w   #4,d2                           ; Based on the Plot position after
                move.w  d2,THIS_SPRITE_MAP_XPOS(a2)             ; Enemy is Spawned.
		
                move.w  #TRIBESMAN_MOVE_SPEED,PRIVATE_TRIBESMAN_MOVE_SPEED(a2)       ; Set move speed.
                clr.w   PRIVATE_TRIBESMAN_ANIMPOINTER(a2)


;;; Init code here.		
		move.w	#-1,PRIVATE_TRIBESMAN_CASCADE_COUNTER(a2)
		clr.w	PRIVATE_TRIBESMAN_CASCADING(a2)
		
		;move.w	#-1,PRIVATE_TRIBESMAN_HOLD_TIMER(a2)
		
		lea	TRIBESMAN_FORMATIONS(a4),a3
		moveq	#0,d3
		move.w	THIS_SPRITE_PARAMETER(a2),d3				; Formation number
		move.w	d3,d4
		lsr.w	#4,d4
		and.w	#$f,d3
		move.l	(a3,d3*4),PRIVATE_TRIBESMAN_FORMATION_PTR(a2)		; Store Formation Pointer.
		add.w	d4,d4
		add.w	d4,PRIVATE_TRIBESMAN_MOVE_SPEED(a2)
		
		tst.w	THIS_SPRITE_PARAMETER(a4)
		bne.s	.not_a_stack
		addq.w	#1,TRIBESMAN_STACK_COUNT(a4)
.not_a_stack:		
                move.w  #TRIBESMAN_STATUS_STACK_LEFT,THIS_SPRITE_STATUS(a2)		
		cmp.b	#TRIBESMAN_FORMATION_RIGHT_OBS,d3
		bne.s	.active
; Appear from left running right.
                move.w  #TRIBESMAN_STATUS_RUN_RIGHT,THIS_SPRITE_STATUS(a2)
		sub.w	#356*16,d2
		move.w	d2,THIS_SPRITE_MAP_XPOS(a2)
		
.active:
                ENEMY_CHECK_END_OF_ROUND

.sprite_0:
;; Code here....
                moveq   #0,d2
                move.w  THIS_SPRITE_MAP_XPOS(a2),d2		; is 0
                lea     BOUNDARY_X1POS(a4),a0
		cmp.l	(a0),d2
		blt.s	.dectimer
		cmp.l	4(a0),d2
		bgt.s	.dectimer
		bra.s	.inbound
			
.dectimer:
                subq.w  #1,THIS_SPRITE_BOUNDARY_TIMER(a2)
                bpl.s   .outbound
                move.w  #ENEMY_STATUS_BONES,THIS_SPRITE_STATUS(a2)
.inbound:       move.w  #ENEMY_BOUNDARY_TIME,THIS_SPRITE_BOUNDARY_TIMER(a2)
.outbound:   

		cmp.w   #ENEMY_STATUS_SWEEP_RIGHT,THIS_SPRITE_STATUS(a2)                ; if doing bones or sweeps then skip all collisions
                ble     .run_sprite_method
		
		tst.w	SHIELD_TIMER
		bpl.s	.shield_on
                cmp.w   #ENEMY_STATUS_STUN_RIGHT,THIS_SPRITE_STATUS(a2)                 ; if doing stuns then skip Rygar collision
                ble     .sprite_stunned
.shield_on:
                bra.s   .sprite_2

.animate_table: dc.l    .method_animate_none-.animate_table                               ; 0
                dc.l    .method_animate_bones-.animate_table    	; 1
                dc.l    .method_animate_sweep_left-.animate_table       ; 2
                dc.l    .method_animate_sweep_right-.animate_table      ; 3
                dc.l    .method_animate_none-.animate_table                      ; 4 Reserved for Stun
                dc.l    .method_animate_none-.animate_table                      ; 5 Reserved for Stun
                dc.l    .method_animate_stack_left-.animate_table       ; 6
                dc.l    .method_animate_none-.animate_table      	; 7
                dc.l    .method_animate_none-.animate_table      	; 8
                dc.l    .method_animate_none-.animate_table      	; 9
                dc.l    .method_animate_climb_left-.animate_table        ; 10                        ; 10
                dc.l    .method_animate_none-.animate_table      	; 11
                dc.l    .method_animate_hold_left-.animate_table        ; 12
                dc.l    .method_animate_none-.animate_table      	; 13
                dc.l    .method_animate_run_left-.animate_table         ; 14
                dc.l    .method_animate_run_right-.animate_table      	; 15
                dc.l    .method_animate_jump_left-.animate_table      	; 16
                dc.l    .method_animate_jump_right-.animate_table      	; 17
                dc.l    .method_animate_none-.animate_table      	; 18
                dc.l    .method_animate_none-.animate_table      	; 19
                dc.l    .method_animate_none-.animate_table      	; 20
                dc.l    .method_animate_none-.animate_table      	; 21
                dc.l    .method_animate_none-.animate_table      	; 22
                dc.l    .method_animate_none-.animate_table      	; 23
                dc.l    .method_animate_none-.animate_table      	; 24
		
		
.method_animate_none:	bra	.exit


;Test this enemy collided with Rygar (the player)
.sprite_2:              move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4                                   ; Check a 32x32 boundary from x1/y1
                move.w  d3,d5
                add.w   #TRIBESMAN_TO_RYGAR_X1_BOUND,d2
                add.w   #TRIBESMAN_TO_RYGAR_Y1_BOUND,d3
                add.w   #TRIBESMAN_TO_RYGAR_X2_BOUND,d4
                add.w   #TRIBESMAN_TO_RYGAR_Y2_BOUND,d5
                lea     RYGAR_COORDS(a4),a3
                ENEMY_TO_RYGAR_COLLISION
                move.l  #POINTS_TRIBESMAN,d2
                tst.w   d6                                      ; Did this sprite collide with Rygar?
                IFEQ    DISABLE_ENEMY_COLLISION
                bmi     .method_set_collision_action            ; Yes
                ENDC

; Check if the enemy has been hit with disk armor
.sprite_stunned:
                move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4                                   ; Check a 32x32 boundary from x1/y1
                move.w  d3,d5
                add.w   #TRIBESMAN_TO_DISKARM_X1_BOUND,d2
                add.w   #TRIBESMAN_TO_DISKARM_Y1_BOUND,d3
                add.w   #TRIBESMAN_TO_DISKARM_X2_BOUND,d4
                add.w   #TRIBESMAN_TO_DISKARM_Y2_BOUND,d5
                lea     DISKARM_COORDS(a4),a3
                ENEMY_TO_DISKARM_COLLISION
                move.l  #POINTS_TRIBESMAN,d2
                tst.w   d6                                      ; Did this sprite get hit with the disk armor
                bmi     .method_set_sweep_direction             ; Yes

; Test this enemy collided with the disk armour

                RUN_SPRITE_METHOD

                METHOD_ANIMATE_BONES

                METHOD_ANIMATE_SWEEP_LEFT

                METHOD_ANIMATE_SWEEP_RIGHT

                METHOD_SET_BONES

                METHOD_SET_STUN_DIRECTION

                METHOD_SET_RYGAR_DEATH_SEQUENCE



.method_set_sweep_direction:
; Was it hit before the drop?
; Is it a stack sprite?
; If answer to both is yes then subtract 1 from the stacks
; if the stacks reach 0 then show the bonus.

		tst.w	PRIVATE_TRIBESMAN_HOLD_TIMER(a2)
		bmi.s	.time_up

		subq.w	#1,TRIBESMAN_STACK_COUNT(a4)
		
.time_up:
		tst.w	TRIBESMAN_STACK_COUNT(a4)
		bne.s	.one_remains
		
		bsr	ADD_TRIBESMAN_BONUS
		
; show remains.
.one_remains:
		lea     TRIBESMAN_SPRITE_ALLOC(a4),a0
                move.b  #-1,(a0,d0)
		
                add.l   d2,VAL_PLAYER1_SCORE(a4)
                move.w  #-1,UPDATE_SCORE(a4)
		
		move.w	#HITPOINT_DELAY,HITPOINT_FRAMES(a4)

                addq.w  #1,REPULSE_BONUS_NUM(a4)                    ; Add one to enemies killed.

                move.l  d0,-(a7)
                moveq   #SND_LAND_ON_ENEMY,d0
                bsr     PLAY_SAMPLE
                move.l  (a7)+,d0
		
		move.w	RYGAR_CURRENT_POWERS(a4),d2
		and.w	#POWER_CROWN,d2
		bne.s	.has_crown_power
		
		tst.w	STATE_KILL_ALL_ENEMIES(a4)
		bmi.s	.xxx
		move.w	#-1,DISKARM_DISABLE(a4)	
.xxx

.has_crown_power:

                move.w  RYGAR_XPOS(a4),d2                                       ; d2 = 114
                add.w   #16,d2                                          ; centre of Rygar
                cmp.w   SPRITE_PLOT_XPOS(a1),d2                         ; xpos = 80
                ble.s   .method_set_sweep_right
                bra.s   .method_set_sweep_left

.method_set_sweep_right:
                move.w  #ENEMY_STATUS_SWEEP_RIGHT,THIS_SPRITE_STATUS(a2)
                bra     .set_update

.method_set_sweep_left:
                move.w  #ENEMY_STATUS_SWEEP_LEFT,THIS_SPRITE_STATUS(a2)
                bra     .set_update

.method_set_collision_action:		
                tst.w   RYGAR_FALLPREV_STATE(a4)                                ; Is Rygar falling on enemy
                IFNE    TRIBESMAN_GROUND_ENEMY
                bmi.s   .method_set_stun_or_destroy                     ; Yes!
                ELSE
                bmi.s   .method_set_jump_or_destroy                     ; Yes!
                ENDC

                IFNE    DISABLE_ENEMY_COLLISION
                bra     .exit
                ELSE
                bra     .method_set_rygar_death_sequence
                ENDC

                IFNE    TRIBESMAN_GROUND_ENEMY
                METHOD_SET_STUN_OR_DESTROY                              ;
                ELSE
                METHOD_SET_JUMP_OR_DESTROY                              ;
                ENDC


.method_destroy_sprite:
		subq.w	#1,TRIBESMEN_IN_USE(a4)
		
                clr.w   THIS_SPRITE_STATUS(a2)
                IFNE    TRIBESMAN_HWSPR_ENABLE
                IFNE    TRIBESMAN_HWSPR_UPPER
                lea     ENEMY_HWSPR_UPPER_ALLOCATED(a4),a0                      ; Free up hardware sprite
                ENDC
                IFNE    TRIBESMAN_HWSPR_MID
                lea     ENEMY_HWSPR_MID_ALLOCATED(a4),a0                        ; Free up hardware sprite
                ENDC
                IFNE    TRIBESMAN_HWSPR_LOWER
                lea     ENEMY_HWSPR_LOWER_ALLOCATED(a4),a0                      ; Free up hardware sprite
                ENDC
                move.w  THIS_SPRITE_HWSLOT(a2),d7
                bmi     DESTROY_SPRITE
                FREE_HARDWARE_SPRITE
                ENDC
                bra     DESTROY_SPRITE


.method_set_stack_left:
                move.w  #TRIBESMAN_STATUS_STACK_LEFT,THIS_SPRITE_STATUS(a2)
                bra     .set_update

.method_animate_stack_left:
                moveq   #TRIBESMAN_STATUS_STACK_LEFT,d1
                lea     TRIBESMAN_SPRITE_ALLOC(a4),a0
                moveq   #0,d2
                move.b  (a0,d0),d2
		move.l	PRIVATE_TRIBESMAN_FORMATION_PTR(a2),a0
                move.l  (a0,d2*4),a0

                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  PRIVATE_TRIBESMAN_ANIMPOINTER(a2),d2
		
		cmp.w	#$8000,(a0,d2*4)
                beq.s   .method_set_hold_left

                addq.w  #1,PRIVATE_TRIBESMAN_ANIMPOINTER(a2)

                add.w   (a0,d2*4),d6

                move.w  2(a0,d2*4),d3
                beq.s   .method_animate_run_left_0

                moveq   #TRIBESMAN_STATUS_FLY_LEFT,d1
                add.w   d3,SPRITE_PLOT_YPOS(a1)

.method_animate_run_left_0:
                move.l  #ENEMY_DEF_TRIBESMAN,d7
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS
                bra     ANIMATE

.method_set_hold_left:
                move.w  #TRIBESMAN_STATUS_HOLD_LEFT,THIS_SPRITE_STATUS(a2)
                lea     TRIBESMAN_SPRITE_ALLOC(a4),a0
                moveq   #0,d2
                move.b  (a0,d0),d2      ; d2 has sprite number. 0-3  (3 must climb first...)
                ;lea     TRIBESMAN_TAB_ROUND2(a4),a0
                move.l	PRIVATE_TRIBESMAN_FORMATION_PTR(a2),a0
		move.l  16(a0),a0     		; Get delay table
                move.w  (a0,d2*2),d3             ; Get delay time.
		sub.w	ROUND_CURRENT(a4),d3		; Adjust for round progress.
		sub.w	ROUND_CURRENT(a4),d3
		move.w	d3,PRIVATE_TRIBESMAN_HOLD_TIMER(a2)
		;clr.w	PRIVATE_TRIBESMAN_HOLD_TIMER(a2)		
                bra     .set_update

.method_animate_hold_left:
; First lets check for the cascade
		bsr	.cascade_check
; always 0 on first run....

		moveq	#0,d3
		lea	TRIBESMAN_SPRITE_ALLOC(a4),a3
		move.b	(a3,d0),d3
; Get this tribesman sprite
		lea	TRIBESMAN_CASCADE_YPOS(a4),a3
		tst.b	(a3,d3)
		bmi.s	.method_animate_hold_left_0
		subq.b	#1,(a3,d3)
		addq.w	#2,SPRITE_PLOT_YPOS(a1)
		
		

.method_animate_hold_left_0:
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.l  #ENEMY_DEF_TRIBESMAN,d7
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS
                subq.w  #1,PRIVATE_TRIBESMAN_HOLD_TIMER(a2)
                tst.w   PRIVATE_TRIBESMAN_HOLD_TIMER(a2)
                bmi     .method_set_climb_left
                moveq   #TRIBESMAN_STATUS_HOLD_LEFT,d1
                bra     ANIMATE
	
; Subroutine...here...	
.cascade_check:	
		moveq	#3,d5					; Counter
		move.l	TRIBESMAN_CASCADE(a4),d3		; 07 08 0b 0c
		
.cascade_loop:	tst.b	d3					;
		beq	.exit
		cmp.b	d0,d3					; this sprite = 0c?
		beq.s	.cascade_check_0
		lsr.l	#8,d3
		subq.b	#1,d5
		bra.s	.cascade_loop

.cascade_check_0:
		lsr.l	#8,d3			; what is the sprite tribesman sprite under this one?
		tst.b	d3			; If it is the one on the bottom then there are no more
		beq	.exit			; Bottom
		
		lea	TRIBESMAN_SPRITE_ALLOC(a4),a3
		moveq	#0,d4
		move.b	d3,d4
		tst.b	(a3,d4)			; Has the sprite underneath been killed?
		bpl	.exit			; No...so exit.
						; No so lets set the cascade counter
						
		clr.b	(a3,d4)
		
		lea	TRIBESMAN_CASCADE_YPOS(a4),a3
		subq.b	#1,d5
		
		move.w	d5,TRIBESMAN_HIT(a4)
		
.cascade_set:	move.b	#14,(a3,d5)
		addq.b	#1,d5
		cmp.b	#4,d5
		bne.s	.cascade_set
		
		bra	.exit

;--------------------------------

.method_set_climb_left:
                move.w  #TRIBESMAN_STATUS_CLIMB_LEFT,THIS_SPRITE_STATUS(a2)
                clr.w   PRIVATE_TRIBESMAN_SINE_POINTER(a2)
                move.w  SPRITE_PLOT_YPOS(a1),PRIVATE_TRIBESMAN_FIXED_YPOS(a2)
                bra     .set_update

.method_animate_climb_left:
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                sub.w   PRIVATE_TRIBESMAN_MOVE_SPEED(a2),d6

                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS

                lea     ENEMY_GRAVITY_SINE(a4),a0
		tst.w	THIS_SPRITE_PARAMETER(a2)
		beq.s	.leap
		lea     ENEMY_GRAVITY_SINE_1(a4),a0
.leap:          move.w  PRIVATE_TRIBESMAN_SINE_POINTER(a2),d3
                addq.w  #2,PRIVATE_TRIBESMAN_SINE_POINTER(a2)
                move.w  PRIVATE_TRIBESMAN_FIXED_YPOS(a2),d4
                add.w   (a0,d3*2),d4

                cmp.w   #SCREEN_BOTTOM,d4
                bge     .method_destroy_sprite

                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  SPRITE_PLOT_YPOS(a1),d7
                lsr.w   #4,d7
                moveq   #0,d5
                IS_PATH_DIFFERENT                       ;d6=xpos interpolated, d7=current platform
                tst.b   d6
                bmi     .method_set_run_left
                move.w  d4,SPRITE_PLOT_YPOS(a1)

                moveq   #TRIBESMAN_STATUS_CLIMB_LEFT,d1
                bra     ANIMATE


;------------------------------------------------------------------------------------
; RUN LEFT
;------------------------------------------------------------------------------------
.method_set_run_left:
                move.w  #TRIBESMAN_STATUS_RUN_LEFT,THIS_SPRITE_STATUS(a2)
                bra     .exit

.method_animate_run_left:
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                sub.w   PRIVATE_TRIBESMAN_MOVE_SPEED(a2),d6
                move.w  d6,d2

                move.w  SPRITE_PLOT_YPOS(a1),d7
                lsr.w   #4,d7
                move.w  d7,d3
                subq.w  #2,d7
; Check an upper obstacle
                moveq   #1,d5
                IS_PATH_DIFFERENT                       ;d6=xpos interpolated, d7=current platform
                tst.b   d6
                bmi     .method_set_jump_left
; Check platform
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                sub.w   PRIVATE_TRIBESMAN_MOVE_SPEED(a2),d6

                move.w  d3,d7
                moveq   #0,d5
                IS_PATH_DIFFERENT                       ;d6=xpos interpolated, d7=current platform
                tst.b   d6
                bpl     .method_set_jump_left

                UPDATE_SPRITE_PLOT_XPOS
                moveq   #TRIBESMAN_STATUS_RUN_LEFT,d1
                bra     ANIMATE

;------------------------------------------------------------------------------------
; JUMP LEFT
;------------------------------------------------------------------------------------
.method_set_jump_left:
		rept	6
		swap	d7
		endr
                move.w  #TRIBESMAN_STATUS_JUMP_LEFT,THIS_SPRITE_STATUS(a2)
                clr.w   PRIVATE_TRIBESMAN_SINE_POINTER(a2)
                move.w  SPRITE_PLOT_YPOS(a1),PRIVATE_TRIBESMAN_FIXED_YPOS(a2)
                bra     .exit

.method_animate_jump_left:
		rept	6
		swap	d3
		endr
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                sub.w   PRIVATE_TRIBESMAN_MOVE_SPEED(a2),d6
                move.l  #ENEMY_DEF_TRIBESMAN,d7
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS

                lea     RIDER_JUMP_SINE(a4),a0
                move.w  PRIVATE_TRIBESMAN_SINE_POINTER(a2),d2
                addq.w  #RIDER_SINE_SPEED,PRIVATE_TRIBESMAN_SINE_POINTER(a2)
                move.w  PRIVATE_TRIBESMAN_FIXED_YPOS(a2),d4
                add.w   (a0,d2*2),d4                            ; Add sine(y)

                cmp.w   #64,d2                                  ; are we on descend curve
                ble.s   .method_animate_jump_left_1            ; No, don't check platforms...
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  SPRITE_PLOT_YPOS(a1),d7
                lsr.w   #4,d7

                moveq   #1,d5
                IS_PATH_DIFFERENT                       ;d6=xpos interpolated, d7=current platform
                tst.b   d6
                bmi     .method_set_run_left

.method_animate_jump_left_1:
                move.w  d4,SPRITE_PLOT_YPOS(a1)
                moveq   #TRIBESMAN_STATUS_JUMP_LEFT,d1
                bra     ANIMATE


;------------------------------------------------------------------------------------
; RUN RIGHT
;------------------------------------------------------------------------------------
.method_set_run_right:
                move.w  #TRIBESMAN_STATUS_RUN_RIGHT,THIS_SPRITE_STATUS(a2)
                bra     .exit

.method_animate_run_right:
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                add.w   PRIVATE_TRIBESMAN_MOVE_SPEED(a2),d6
                move.w  d6,d2

                move.w  SPRITE_PLOT_YPOS(a1),d7
                lsr.w   #4,d7
                move.w  d7,d3
                subq.w  #2,d7
; Check an upper obstacle
                moveq   #1,d5
                IS_PATH_DIFFERENT                       ;d6=xpos interpolated, d7=current platform
                tst.b   d6
                bmi     .method_set_jump_right
; Check platform
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                add.w   PRIVATE_TRIBESMAN_MOVE_SPEED(a2),d6

                move.w  d3,d7
                moveq   #0,d5
                IS_PATH_DIFFERENT                       ;d6=xpos interpolated, d7=current platform
                tst.b   d6
                bpl     .method_set_jump_right

                UPDATE_SPRITE_PLOT_XPOS
                moveq   #TRIBESMAN_STATUS_RUN_RIGHT,d1
                bra     ANIMATE

;------------------------------------------------------------------------------------
; JUMP RIGHT
;------------------------------------------------------------------------------------
.method_set_jump_right:
                move.w  #TRIBESMAN_STATUS_JUMP_RIGHT,THIS_SPRITE_STATUS(a2)
                clr.w   PRIVATE_TRIBESMAN_SINE_POINTER(a2)
                move.w  SPRITE_PLOT_YPOS(a1),PRIVATE_TRIBESMAN_FIXED_YPOS(a2)
                bra     .exit

.method_animate_jump_right:
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                add.w   PRIVATE_TRIBESMAN_MOVE_SPEED(a2),d6
                move.l  #ENEMY_DEF_TRIBESMAN,d7
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS

                lea     RIDER_JUMP_SINE(a4),a0
                move.w  PRIVATE_TRIBESMAN_SINE_POINTER(a2),d2
                addq.w  #RIDER_SINE_SPEED,PRIVATE_TRIBESMAN_SINE_POINTER(a2)
                move.w  PRIVATE_TRIBESMAN_FIXED_YPOS(a2),d4
                add.w   (a0,d2*2),d4                            ; Add sine(y)

                cmp.w   #64,d2                                  ; are we on descend curve
                ble.s   .method_animate_jump_right_1            ; No, don't check platforms...
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  SPRITE_PLOT_YPOS(a1),d7
                lsr.w   #4,d7

                moveq   #1,d5
                IS_PATH_DIFFERENT                       ;d6=xpos interpolated, d7=current platform
                tst.b   d6
                bmi     .method_set_run_right

.method_animate_jump_right_1:
                move.w  d4,SPRITE_PLOT_YPOS(a1)
                moveq   #TRIBESMAN_STATUS_JUMP_RIGHT,d1
                bra     ANIMATE



.set_update:

.exit:          rts

