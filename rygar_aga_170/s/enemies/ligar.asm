LIGAR1_GROUND_ENEMY:            equ     1       ; 1 = Ground enemy means enemy is stunnable.
LIGAR1_HWSPR_ENABLE:           	equ     0
LIGAR1_HWSPR_UPPER:        	equ     0
LIGAR1_HWSPR_MID:           	equ     0
LIGAR1_HWSPR_LOWER:         	equ     0

LIGAR1_TO_RYGAR_X1_BOUND:   	equ     16
LIGAR1_TO_RYGAR_Y1_BOUND:   	equ     0
LIGAR1_TO_RYGAR_X2_BOUND:   	equ     63-16
LIGAR1_TO_RYGAR_Y2_BOUND:   	equ     63

LIGAR1_TO_DISKARM_X1_BOUND: 	equ     0
LIGAR1_TO_DISKARM_Y1_BOUND:	 equ     0
LIGAR1_TO_DISKARM_X2_BOUND: 	equ     64
LIGAR1_TO_DISKARM_Y2_BOUND: 	equ     64

LIGAR1_LEAP_FRAMES:		equ	34

LIGAR1_MOVE_SPEED:          	equ    	28

LIGAR_PAUSE_LONG:		equ	30
LIGAR_PAUSE_SHORT:		equ	10


LIGAR1_STATUS_FACE_LEFT_SHORT:    	equ     6
LIGAR1_STATUS_FACE_RIGHT_SHORT:    	equ     7
LIGAR1_STATUS_FACE_LEFT_LONG:    	equ     8
LIGAR1_STATUS_FACE_RIGHT_LONG:    	equ     9
LIGAR1_STATUS_LEAP_LEFT:    	equ     10
LIGAR1_STATUS_LEAP_RIGHT:    	equ     11

PRIVATE_LIGAR1_SPEED:       	equ     32
PRIVATE_LIGAR1_TRIGGER:     	equ     34
PRIVATE_LIGAR1_FRAMECOUNT:  	equ     36
PRIVATE_LIGAR1_FIXED_YPOS:  	equ     38
PRIVATE_LIGAR1_SINE_POINTER 	equ     40
PRIVATE_LIGAR1_HIT_COUNT:	equ	42
PRIVATE_LIGAR1_VULNERBALE:	equ	44

LIGAR1_HIT_POINTS:             equ    	20


HDL_LIGAR1:
		FUNCID	#$939912cd
                SPRITE_INITIATE

; Set the initial variables and status of the sprite.
.construct:
                move.w  SPRITE_PLOT_XPOS(a1),d2                 ; Set the initial Map Position
                lsl.w   #4,d2                           ; Based on the Plot position after
                move.w  d2,THIS_SPRITE_MAP_XPOS(a2)             ; Enemy is Spawned.
		
		move.w	#114,PRIVATE_LIGAR1_FIXED_YPOS(a2)
                move.w  #LIGAR1_MOVE_SPEED,PRIVATE_LIGAR1_SPEED(a2)     ; Set move speed
		move.w	#130,PRIVATE_LIGAR1_FRAMECOUNT(a2)
                move.w  #LIGAR1_STATUS_FACE_LEFT_SHORT,THIS_SPRITE_STATUS(a2)
		clr.w	PRIVATE_LIGAR1_SINE_POINTER(a2)	
		clr.w	PRIVATE_LIGAR1_VULNERBALE(a2)
		
                move.w  #LIGAR1_HIT_POINTS,PRIVATE_LIGAR1_HIT_COUNT(a2)
		bra	.exit

;;; Init code here.

.active:
                ENEMY_CHECK_END_OF_ROUND

.sprite_0:
;; Code here....
                bra.s   .sprite_2

.animate_table: dc.l    .method_animate_none-.animate_table              ; 0
                dc.l    .method_animate_bones-.animate_table    	; 1
                dc.l    .method_animate_none-.animate_table       ; 2
                dc.l    .method_animate_none-.animate_table      ; 3
		dc.l    .method_animate_none-.animate_table      	; 4
		dc.l    .method_animate_none-.animate_table      	; 5
                dc.l    .method_animate_face_left_short-.animate_table	; 6
                dc.l    .method_animate_face_right_short-.animate_table	; 7
                dc.l    .method_animate_face_left_long-.animate_table	; 8
                dc.l    .method_animate_face_right_long-.animate_table  	;9
                dc.l    .method_animate_leap_left-.animate_table		; 10
                dc.l    .method_animate_leap_right-.animate_table      	; 11
                dc.l    .method_animate_none-.animate_table      	; 12
                dc.l    .method_animate_none-.animate_table      	; 13
                dc.l    .method_animate_none-.animate_table      	; 14
                dc.l    .method_animate_none-.animate_table      	; 15
                dc.l    .method_animate_none-.animate_table      	; 16
                dc.l    .method_animate_none-.animate_table      	; 17
                dc.l    .method_animate_none-.animate_table      	; 18
                dc.l    .method_animate_none-.animate_table      	; 19
                dc.l    .method_animate_none-.animate_table      	; 20
                dc.l    .method_animate_none-.animate_table      	; 21
                dc.l    .method_animate_none-.animate_table      	; 22
                dc.l    .method_animate_none-.animate_table      	; 23
                dc.l    .method_animate_none-.animate_table      	; 24
		
		
.method_animate_none:	bra	.exit

;Test this enemy collided with Rygar (the player)
.sprite_2:              move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4                                   ; Check a 32x32 boundary from x1/y1
                move.w  d3,d5
                add.w   #LIGAR1_TO_RYGAR_X1_BOUND,d2
                add.w   #LIGAR1_TO_RYGAR_Y1_BOUND,d3
                add.w   #LIGAR1_TO_RYGAR_X2_BOUND,d4
                add.w   #LIGAR1_TO_RYGAR_Y2_BOUND,d5
                lea     RYGAR_COORDS(a4),a3
                ENEMY_TO_RYGAR_COLLISION
		move.l  #POINTS_LIGAR,d2
                tst.w   d6                                      ; Did this sprite collide with Rygar?
                IFEQ    DISABLE_ENEMY_COLLISION
                bmi     .method_set_collision_action            ; Yes
                ENDC

; Check if the enemy has been hit with disk armor
.sprite_stunned:
                move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4                                   ; Check a 32x32 boundary from x1/y1
                move.w  d3,d5
                add.w   #LIGAR1_TO_DISKARM_X1_BOUND,d2
                add.w   #LIGAR1_TO_DISKARM_Y1_BOUND,d3
                add.w   #LIGAR1_TO_DISKARM_X2_BOUND,d4
                add.w   #LIGAR1_TO_DISKARM_Y2_BOUND,d5
                lea     DISKARM_COORDS(a4),a3
                ENEMY_TO_DISKARM_COLLISION
		move.l  #POINTS_LIGAR,d2
                tst.w   d6                                      ; Did this sprite get hit with the disk armor
                bmi     .method_add_points_and_destroy              ; Yes

; Test this enemy collided with the disk armour

                RUN_SPRITE_METHOD

                METHOD_ANIMATE_BONES

.method_set_bones:
                move.w  #ENEMY_STATUS_BONES,THIS_SPRITE_STATUS(a2)
                move.w  #SPR_TYPE_ENEMY_2X64,SPRITE_PLOT_TYPE(a1)
		move.w	THIS_SPRITE_STATUS(a2),LIGAR1_ANIM
                bra     .set_update

                METHOD_SET_SWEEP_DIRECTION

                METHOD_SET_STUN_DIRECTION

                METHOD_SET_RYGAR_DEATH_SEQUENCE

.method_add_points_and_destroy:
		move.w	#-1,DISKARM_DISABLE(a4)
		tst.w	ITEM_KILLALL_SET(a4)
		bmi	.method_set_bones

		move.l  d0,-(a7)	
	
                moveq   #SND_LAND_ON_ENEMY,d0 
		tst.w	PRIVATE_LIGAR1_VULNERBALE(a2)
		bmi.s	.vulnerable

                moveq   #SND_STONE_HIT,d0				
                bsr     PLAY_SAMPLE
                move.l  (a7)+,d0
		bra	.set_update
		
.vulnerable:	bsr     PLAY_SAMPLE
                move.l  (a7)+,d0
		
		moveq	#3,d3
		move.w  RYGAR_CURRENT_POWERS(a4),d4
                and.w   #POWER_CROWN,d4
                beq     .has_crown                     ; If Rygar has Crown then the Dragon is killed
		moveq	#1,d3
		
.has_crown:             
		sub.w  	d3,PRIVATE_LIGAR1_HIT_COUNT(a2)	
		
		move.w	#HITPOINT_DELAY,HITPOINT_FRAMES(a4)
		tst.w	PRIVATE_LIGAR1_HIT_COUNT(a2)
		bpl	.run_sprite_method
		
                add.l   d2,VAL_PLAYER1_SCORE(a4)
                move.w  #-1,UPDATE_SCORE(a4)
                addq.w  #1,REPULSE_BONUS_NUM(a4)                    ; Add one to enemies killed.
                bra     .method_set_bones

.method_set_collision_action:
                tst.w   RYGAR_FALLPREV_STATE(a4)                                ; Is Rygar falling on enemy
                IFNE    LIGAR1_GROUND_ENEMY
                bmi.s   .method_set_stun_or_destroy                     ; Yes!
                ELSE
                bmi.s   .method_set_jump_or_destroy                     ; Yes!
                ENDC

                IFNE    DISABLE_ENEMY_COLLISION
                bra     .exit
                ELSE
                bra     .method_set_rygar_death_sequence
                ENDC

                IFNE    LIGAR1_GROUND_ENEMY
                METHOD_SET_STUN_OR_DESTROY                              ;
                ELSE
                METHOD_SET_JUMP_OR_DESTROY                              ;
                ENDC


.method_destroy_sprite: 
                clr.w   THIS_SPRITE_STATUS(a2)
		move.w	#15,BACKGROUND_TRANSITION_FRAMES(a4)		; Darken background.
                move.w	#-1,INIT_END_SEQUENCE(a4)
		move.w	#-1,SHOW_RYGAR_WIN(a4)
		movem.l	d0-d7/a0-a3,-(a7)
		bsr	PAN_RIGHT
		movem.l	(a7)+,d0-d7/a0-a3
		bra     DESTROY_SPRITE

;--------------------------------
; Face Left for only 10 frames
; Wait 10 frames facing left
;--------------------------------

.method_set_face_left_short:
		move.w	#10,PRIVATE_LIGAR1_FRAMECOUNT(a2)
                move.w  #LIGAR1_STATUS_FACE_LEFT_SHORT,THIS_SPRITE_STATUS(a2)
		clr.w	PRIVATE_LIGAR1_VULNERBALE(a2)
		bra	.exit

.method_animate_face_left_short:
		subq.w	#1,PRIVATE_LIGAR1_FRAMECOUNT(a2)
		tst.w	PRIVATE_LIGAR1_FRAMECOUNT(a2)
		bmi	.method_set_leap_left				; -- Leap left triggered

                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS
                moveq   #LIGAR1_STATUS_FACE_LEFT_SHORT,d1
                bra     .animate
	
;--------------------------------
; Leap left
;--------------------------------			
.method_set_leap_left:
                move.w  #LIGAR1_STATUS_LEAP_LEFT,THIS_SPRITE_STATUS(a2)
		clr.w	PRIVATE_LIGAR1_SINE_POINTER(a2)
		move.l	d0,-(a7)
		moveq   #SND_GROWL,d0
		bsr     PLAY_SAMPLE		
		move.l	(a7)+,d0
		clr.w	PRIVATE_LIGAR1_VULNERBALE(a2)
		bra	.exit
		

.method_animate_leap_left:
                lea     LIGAR_SINE(a4),a0
                move.w  PRIVATE_LIGAR1_SINE_POINTER(a2),d2
                
		move.w  PRIVATE_LIGAR1_FIXED_YPOS(a2),d3
		move.b  (a0,d2),d4
		cmp.b	#$80,d4
		beq	.method_set_face_left_long			; Set face left long pause 
		add.b	d4,d3
                move.w  d3,SPRITE_PLOT_YPOS(a1)

                addq.w  #2,PRIVATE_LIGAR1_SINE_POINTER(a2)
		
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                sub.w   #LIGAR1_MOVE_SPEED,d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS
                moveq   #LIGAR1_STATUS_LEAP_LEFT,d1
                bra     .animate
	
;--------------------------------
; Face Left for 50 frames
;--------------------------------	
.method_set_face_left_long:
		move.w	#LIGAR_PAUSE_LONG,PRIVATE_LIGAR1_FRAMECOUNT(a2)
                move.w  #LIGAR1_STATUS_FACE_LEFT_LONG,THIS_SPRITE_STATUS(a2)
		move.w	#-1,PRIVATE_LIGAR1_VULNERBALE(a2)
		bra	.exit
		
.method_animate_face_left_long:
		subq.w	#1,PRIVATE_LIGAR1_FRAMECOUNT(a2)
		tst.w	PRIVATE_LIGAR1_FRAMECOUNT(a2)
		bmi	.method_set_choose

                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS
                moveq   #LIGAR1_STATUS_FACE_LEFT_LONG,d1
                bra     .animate
		
; Face right for short time

.method_set_face_right_short:
		move.w	#LIGAR_PAUSE_SHORT,PRIVATE_LIGAR1_FRAMECOUNT(a2)
                move.w  #LIGAR1_STATUS_FACE_RIGHT_SHORT,THIS_SPRITE_STATUS(a2)
		clr.w	PRIVATE_LIGAR1_VULNERBALE(a2)
		bra	.exit
		
.method_animate_face_right_short:
		subq.w	#1,PRIVATE_LIGAR1_FRAMECOUNT(a2)
		tst.w	PRIVATE_LIGAR1_FRAMECOUNT(a2)
		bmi	.method_set_leap_right				; Cause a leap right

                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS
                moveq   #LIGAR1_STATUS_FACE_RIGHT_SHORT,d1
                bra     .animate

; Leap right....
.method_set_leap_right:
                move.w  #LIGAR1_STATUS_LEAP_RIGHT,THIS_SPRITE_STATUS(a2)
		clr.w	PRIVATE_LIGAR1_SINE_POINTER(a2)
		move.l	d0,-(a7)
		moveq   #SND_GROWL,d0
		bsr     PLAY_SAMPLE		
		move.l	(a7)+,d0
		clr.w	PRIVATE_LIGAR1_VULNERBALE(a2)
		bra	.exit

.method_animate_leap_right:
                lea     LIGAR_SINE(a4),a0
                move.w  PRIVATE_LIGAR1_SINE_POINTER(a2),d2
                move.w  PRIVATE_LIGAR1_FIXED_YPOS(a2),d3
                move.b  (a0,d2),d4
		cmp.b	#$80,d4
		beq	.method_set_face_right_long
		add.b	d4,d3
                move.w  d3,SPRITE_PLOT_YPOS(a1)
		
                addq.w  #2,PRIVATE_LIGAR1_SINE_POINTER(a2)

                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                add.w   #LIGAR1_MOVE_SPEED,d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS
                moveq   #LIGAR1_STATUS_LEAP_RIGHT,d1
                bra     .animate
	
.method_set_face_right_long:
		move.w	#LIGAR_PAUSE_LONG,PRIVATE_LIGAR1_FRAMECOUNT(a2)
                move.w  #LIGAR1_STATUS_FACE_RIGHT_LONG,THIS_SPRITE_STATUS(a2)
		move.w	#-1,PRIVATE_LIGAR1_VULNERBALE(a2)
		bra	.exit
		
.method_animate_face_right_long:
		subq.w	#1,PRIVATE_LIGAR1_FRAMECOUNT(a2)
		tst.w	PRIVATE_LIGAR1_FRAMECOUNT(a2)
		bmi	.method_set_choose

                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS
                moveq   #LIGAR1_STATUS_FACE_RIGHT_LONG,d1
                bra     .animate
			
.method_set_choose:	
		move.w	RYGAR_XPOS(a4),d2
		cmp.w	SPRITE_PLOT_XPOS(a1),d2
		ble	.method_set_face_left_short
		bra	.method_set_face_right_short

.animate:	move.w	SPRITE_PLOT_XPOS(a1),LIGAR1_XPOS
		move.w	SPRITE_PLOT_YPOS(a1),LIGAR1_YPOS
		move.w	THIS_SPRITE_STATUS(a2),LIGAR1_ANIM
		bra	ANIMATE
.set_update:

.exit:          rts



HDL_LIGAR2:
		FUNCID	#$939912cd
                SPRITE_INITIATE

; Set the initial variables and status of the sprite.
.construct:
       
		bra	.exit

;;; Init code here.

.active:
		move.w	LIGAR1_ANIM,THIS_SPRITE_STATUS(a2)
                bra.s   .sprite_2

.animate_table: dc.l    .method_animate_none-.animate_table             ; 0
                dc.l    .method_animate_bones-.animate_table    	; 1
                dc.l    .method_animate_none-.animate_table       	; 2
                dc.l    .method_animate_none-.animate_table      	; 3
		dc.l    .method_animate_none-.animate_table      	; 4
		dc.l    .method_animate_none-.animate_table      	; 5
                dc.l    .method_animate_move-.animate_table		; 6
                dc.l    .method_animate_move-.animate_table		; 7
                dc.l    .method_animate_move-.animate_table		; 8
                dc.l    .method_animate_move-.animate_table  		; 9
                dc.l    .method_animate_move-.animate_table		; 10
                dc.l    .method_animate_move-.animate_table      	; 11
                dc.l    .method_animate_none-.animate_table      	; 12
                dc.l    .method_animate_none-.animate_table      	; 13
                dc.l    .method_animate_none-.animate_table      	; 14
                dc.l    .method_animate_none-.animate_table      	; 15
                dc.l    .method_animate_none-.animate_table      	; 16
                dc.l    .method_animate_none-.animate_table      	; 17
                dc.l    .method_animate_none-.animate_table      	; 18
                dc.l    .method_animate_none-.animate_table      	; 19

.method_animate_none:	bra	.exit

.sprite_2:

                RUN_SPRITE_METHOD

                METHOD_ANIMATE_BONES


.method_set_bones:
                move.w  #ENEMY_STATUS_BONES,THIS_SPRITE_STATUS(a2)
                move.w  #SPR_TYPE_ENEMY_2X64,SPRITE_PLOT_TYPE(a1)
                bra     .set_update

.method_destroy_sprite: 
                clr.w   THIS_SPRITE_STATUS(a2)
                bra     DESTROY_SPRITE

.method_animate_move:
		move.w	LIGAR1_XPOS,SPRITE_PLOT_XPOS(a1)
		move.w	LIGAR1_YPOS,SPRITE_PLOT_YPOS(a1)
		add.w	#32,SPRITE_PLOT_XPOS(a1)
		
		move.w	LIGAR1_ANIM,THIS_SPRITE_STATUS(a2)
		moveq	#0,d1
		move.w	THIS_SPRITE_STATUS(a2),d1
		bra	ANIMATE
		
.set_update:

.exit:          rts




