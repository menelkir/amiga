TESTSPRITE_GROUND_ENEMY:          equ     0       ; 1 = Ground enemy means enemy is stunnable.
TESTSPRITE_HWSPR_ENABLE:          equ     1
TESTSPRITE_HWSPR_UPPER:           equ     1
TESTSPRITE_HWSPR_MID:             equ     0
TESTSPRITE_HWSPR_LOWER:           equ     0

TESTSPRITE_STATUS_MOVE:		equ	6

TESTSPRITE_TO_RYGAR_X1_BOUND:     equ     0
TESTSPRITE_TO_RYGAR_Y1_BOUND:     equ     0
TESTSPRITE_TO_RYGAR_X2_BOUND:     equ     32
TESTSPRITE_TO_RYGAR_Y2_BOUND:     equ     32

TESTSPRITE_TO_DISKARM_X1_BOUND:   equ     0
TESTSPRITE_TO_DISKARM_Y1_BOUND:   equ     0
TESTSPRITE_TO_DISKARM_X2_BOUND:   equ     32
TESTSPRITE_TO_DISKARM_Y2_BOUND:   equ     32

TESTSPRITE_MOVE_SPEED:            equ     20

TESTSPRITE_STATUS_WALK_LEFT:      equ     6

PRIVATE_TESTSPRITE_SPEED:         equ     32


HDL_TESTSPRITE:
		FUNCID	#$eee675c9
                SPRITE_INITIATE

; Set the initial variables and status of the sprite.
.construct:
                move.w  SPRITE_PLOT_XPOS(a1),d2                 ; Set the initial Map Position
                lsl.w   #4,d2                           ; Based on the Plot position after
                move.w  d2,THIS_SPRITE_MAP_XPOS(a2)             ; Enemy is Spawned.
                move.w  #TESTSPRITE_MOVE_SPEED,PRIVATE_TESTSPRITE_SPEED(a2) ; Set move speed.
		move.w	#TESTSPRITE_STATUS_MOVE,THIS_SPRITE_STATUS(a2)
		move.w	#100,SPRITE_PLOT_XPOS(a1)
		move.w	#0,SPRITE_PLOT_YPOS(a1)
;;; Init code here.

.active:
                ENEMY_CHECK_END_OF_ROUND

.sprite_0:
;; Code here....
                bra.s   .sprite_2

.animate_table: dc.l    0                               ; 0
                dc.l    .method_animate_bones-.animate_table    ; 1
                dc.l    .method_animate_sweep_left-.animate_table       ; 2
                dc.l    .method_animate_sweep_right-.animate_table      ; 3
                dc.l    0                               ; 4 Reserved for Stun
                dc.l    0                               ; 5 Reserved for Stun
                dc.l    .method_animate_move-.animate_table      ; 3

;Test this enemy collided with Rygar (the player)
.sprite_2:      

; Check if the enemy has been hit with disk armor
.sprite_stunned:
                

; Test this enemy collided with the disk armour

                RUN_SPRITE_METHOD

                METHOD_ANIMATE_BONES

                METHOD_ANIMATE_SWEEP_LEFT

                METHOD_ANIMATE_SWEEP_RIGHT

                METHOD_SET_BONES

                METHOD_SET_SWEEP_DIRECTION

                METHOD_SET_STUN_DIRECTION

                METHOD_SET_RYGAR_DEATH_SEQUENCE


.method_set_collision_action:
                tst.w   RYGAR_FALLPREV_STATE(a4)                                ; Is Rygar falling on enemy
                IFNE    TESTSPRITE_GROUND_ENEMY
                bmi.s   .method_set_stun_or_destroy                     ; Yes!
                ELSE
                bmi.s   .method_set_jump_or_destroy                     ; Yes!
                ENDC

                IFNE    DISABLE_ENEMY_COLLISION
                bra     .exit
                ELSE
                bra     .method_set_rygar_death_sequence
                ENDC

                IFNE    TESTSPRITE_GROUND_ENEMY
                METHOD_SET_STUN_OR_DESTROY                              ;
                ELSE
                METHOD_SET_JUMP_OR_DESTROY                              ;
                ENDC


.method_destroy_sprite: 
                clr.w   THIS_SPRITE_STATUS(a2)
                IFNE    TESTSPRITE_HWSPR_ENABLE
                IFNE    TESTSPRITE_HWSPR_UPPER
                lea     ENEMY_HWSPR_UPPER_ALLOCATED(a4),a0                      ; Free up hardware sprite
                ENDC
                IFNE    TESTSPRITE_HWSPR_MID
                lea     ENEMY_HWSPR_MID_ALLOCATED(a4),a0                        ; Free up hardware sprite
                ENDC
                IFNE    TESTSPRITE_HWSPR_LOWER
                lea     ENEMY_HWSPR_LOWER_ALLOCATED(a4),a0                      ; Free up hardware sprite
                ENDC
                move.w  THIS_SPRITE_HWSLOT(a2),d7
                bmi     DESTROY_SPRITE
                FREE_HARDWARE_SPRITE
                ENDC
                bra     DESTROY_SPRITE

.method_animate_move:
		move.w	#100,SPRITE_PLOT_XPOS(a1)
		addq.w	#1,SPRITE_PLOT_YPOS(a1)
		cmp.w	#192,SPRITE_PLOT_YPOS(a1)
		bne.s	.x
		
		bsr	FIREWAITSR
.x:

		moveq	#TESTSPRITE_STATUS_MOVE,d1
		bra	ANIMATE


.set_update:

.exit:          rts

BUSTA
