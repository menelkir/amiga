


SHIELD_STATUS_SHIELD_OFF:     equ     6
SHIELD_STATUS_SHIELD_ON:      equ     7


HDL_SHIELD:
		FUNCID	#$7fb64bb9
                SPRITE_INITIATE

; Set the initial variables and status of the sprite.
.construct:
                move.w  SPRITE_PLOT_XPOS(a1),d2
                lsl.w   #4,d2
                move.w  d2,THIS_SPRITE_MAP_XPOS(a2)
		
		move.w	#SHIELD_STATUS_SHIELD_ON,THIS_SPRITE_STATUS(a2)

.active:        
		;ENEMY_CHECK_END_OF_ROUND

.sprite_0:      
                RUN_SPRITE_METHOD

.animate_table:
                dc.l    .method_destroy_sprite-.animate_table           ; 0
                dc.l    .method_animate_none-.animate_table      	; 1
                dc.l    .method_animate_none-.animate_table      	; 2
                dc.l    .method_animate_none-.animate_table      	; 3
                dc.l    .method_animate_none-.animate_table      	; 4
                dc.l    .method_animate_none-.animate_table      	; 5
                dc.l    .method_animate_shield_off-.animate_table            ; 6
                dc.l    .method_animate_shield_on-.animate_table          	; 7
		dc.l	.method_animate_none-.animate_table		; 8
                dc.l    .method_animate_none-.animate_table      	; 9
                dc.l    .method_animate_none-.animate_table      	; 10
                dc.l    .method_animate_none-.animate_table      	; 11
                dc.l    .method_animate_none-.animate_table      	; 13
                dc.l    .method_animate_none-.animate_table      	; 14
                dc.l    .method_animate_none-.animate_table      	; 15
                dc.l    .method_animate_none-.animate_table      	; 16
                dc.l    .method_animate_none-.animate_table      	; 17
                dc.l    .method_animate_none-.animate_table      	; 18
                dc.l    .method_animate_none-.animate_table      	; 19
                dc.l    .method_animate_none-.animate_table      	; 15
                dc.l    .method_animate_none-.animate_table      	; 16
                dc.l    .method_animate_none-.animate_table      	; 17
                dc.l    .method_animate_none-.animate_table      	; 18
                dc.l    .method_animate_none-.animate_table      	; 19
		
.method_animate_none: 	bra	.exit

.method_set_shield_off:
                move.w  #SHIELD_STATUS_SHIELD_OFF,THIS_SPRITE_STATUS(a2)
                bra.s   .set_update
                nop

.method_set_shield_on:
                move.w  #SHIELD_STATUS_SHIELD_ON,THIS_SPRITE_STATUS(a2)
                bra.s   .set_update
                nop

.method_animate_shield_off:
                moveq   #SHIELD_STATUS_SHIELD_OFF,d1
                movem.l d0-1/a1-a2,-(a7)
                bsr     ANIMATE
                movem.l (a7)+,d0-1/a1-a2
                tst.w   d6
                bmi     .method_destroy_sprite
                bra     .exit
		
.method_animate_shield_on:
		move.w	SHIELD_XPOS(a4),SPRITE_PLOT_XPOS(a1)
		move.w	SHIELD_YPOS(a4),SPRITE_PLOT_YPOS(a1)		
		moveq	#SHIELD_STATUS_SHIELD_ON,d1
		bra	ANIMATE

.method_destroy_sprite:
                clr.w   THIS_SPRITE_STATUS(a2)
                bra     DESTROY_SPRITE



.set_update:
.exit:          rts
