
ELDER_GROUND_ENEMY:          equ     0       ; 1 = Ground enemy means enemy is stunnable.
ELDER_HWSPR_ENABLE:          equ     0
ELDER_HWSPR_UPPER:           equ     0
ELDER_HWSPR_MID:             equ     0
ELDER_HWSPR_LOWER:           equ     0

ELDER_STATUS_HIDDEN:		equ	6
ELDER_STATUS_SHOW:		equ	7
ELDER_STATUS_CLEAN:		equ	8

ELDER_TO_RYGAR_X1_BOUND:     equ     0
ELDER_TO_RYGAR_Y1_BOUND:     equ     0
ELDER_TO_RYGAR_X2_BOUND:     equ     15
ELDER_TO_RYGAR_Y2_BOUND:     equ     15

ELDER_TO_DISKARM_X1_BOUND:   equ     0
ELDER_TO_DISKARM_Y1_BOUND:   equ     0
ELDER_TO_DISKARM_X2_BOUND:   equ     15
ELDER_TO_DISKARM_Y2_BOUND:   equ     15

ELDER_HIT_POINTS:            equ     3

ELDER_STATUS_UP:             equ     6
ELDER_STATUS_FALL:           equ     7
ELDER_STATUS_BURN:           equ     8
ELDER_STATUS_EMBLEM:         equ     9

PRIVATE_ELDER_SPEED:         equ     32
PRIVATE_ELDER_BYTE_INDEX:  equ     34
PRIVATE_ELDER_FIXED_YPOS:    equ     36
PRIVATE_ELDER_STATE:		equ	38			; 0 = Hidden, -1=Shown/Available
PRIVATE_ELDER_HIT_COUNT:	equ	40


HDL_ELDER:
		FUNCID	#$adcf3b62
                SPRITE_INITIATE

; Set the initial variables and status of the sprite.
.construct:
                move.w  SPRITE_PLOT_XPOS(a1),d2                 ; Set the initial Map Position
                lsl.w   #4,d2                           ; Based on the Plot position after
                move.w  d2,THIS_SPRITE_MAP_XPOS(a2)             ; Enemy is Spawned.

		clr.w	PRIVATE_ELDER_STATE(a2)		; State hidden
		
		move.w	THIS_SPRITE_PARAMETER(a2),d2
		and.w	#$fe,d2
		move.w	d2,SPRITE_PLOT_YPOS(a1)			; Store Y Position from parameter
		
		move.w	d2,PRIVATE_ELDER_FIXED_YPOS(a2)
		
		move.w	#ELDER_STATUS_HIDDEN,THIS_SPRITE_STATUS(a2)
                move.w  #ELDER_HIT_POINTS,PRIVATE_ELDER_HIT_COUNT(a2)
                move.w  #SPR_TYPE_ENEMY_1X16,SPRITE_PLOT_TYPE(a1)

		move.w	#$8000,PRIVATE_ELDER_BYTE_INDEX(a2)
;;; Init code here.

.active:
                ;ENEMY_CHECK_END_OF_ROUND

.sprite_0:
;; Code here....
                bra.s   .sprite_2

.animate_table: dc.l    .method_animate_none-.animate_table             ; 0
                dc.l    .method_animate_none-.animate_table    		; 1
                dc.l    .method_animate_none-.animate_table       	; 2
                dc.l    .method_animate_none-.animate_table      	; 3
		dc.l    .method_animate_none-.animate_table      	; 4
		dc.l    .method_animate_none-.animate_table      	; 5
                dc.l    .method_animate_hidden-.animate_table      	; 6
                dc.l    .method_animate_show-.animate_table    		; 7
                dc.l    .method_animate_clean-.animate_table    		; 8
                dc.l    .method_animate_none-.animate_table      	; 9
                dc.l    .method_animate_none-.animate_table      	; 10
                dc.l    .method_animate_none-.animate_table      	; 11
                dc.l    .method_animate_none-.animate_table      	; 12
                dc.l    .method_animate_none-.animate_table      	; 13
                dc.l    .method_animate_none-.animate_table      	; 14
                dc.l    .method_animate_none-.animate_table      	; 15
                dc.l    .method_animate_none-.animate_table      	; 16
                dc.l    .method_animate_none-.animate_table      	; 17
                dc.l    .method_animate_none-.animate_table      	; 18
                dc.l    .method_animate_none-.animate_table      	; 19
                dc.l    .method_animate_none-.animate_table      	; 15
                dc.l    .method_animate_none-.animate_table      	; 16
                dc.l    .method_animate_none-.animate_table      	; 17
                dc.l    .method_animate_none-.animate_table      	; 18
                dc.l    .method_animate_none-.animate_table      	; 19

.method_animate_none:	bra	.exit

;Test this enemy collided with Rygar (the player)
.sprite_2:              move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4                                   ; Check a 32x32 boundary from x1/y1
                move.w  d3,d5
                add.w   #ELDER_TO_RYGAR_X1_BOUND,d2
                add.w   #ELDER_TO_RYGAR_Y1_BOUND,d3
                add.w   #ELDER_TO_RYGAR_X2_BOUND,d4
                add.w   #ELDER_TO_RYGAR_Y2_BOUND,d5
                lea     RYGAR_COORDS(a4),a3
                ENEMY_TO_RYGAR_COLLISION
		moveq	#0,d2		
		tst.w	PRIVATE_ELDER_STATE(a2)
		beq	.sprite_stunned
                tst.w   d6                                      ; Did this sprite collide with Rygar?
                bmi     .method_set_clean            ; Yes


; Check if the enemy has been hit with disk armor
.sprite_stunned:
                move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4                                   ; Check a 32x32 boundary from x1/y1
                move.w  d3,d5
                add.w   #ELDER_TO_DISKARM_X1_BOUND,d2
                add.w   #ELDER_TO_DISKARM_Y1_BOUND,d3
                add.w   #ELDER_TO_DISKARM_X2_BOUND,d4
                add.w   #ELDER_TO_DISKARM_Y2_BOUND,d5
                lea     DISKARM_COORDS(a4),a3
                ENEMY_TO_DISKARM_COLLISION
		moveq	#0,d2
                tst.w   d6                                      ; Did this sprite get hit with the disk armor
                bmi     .method_change_item             	; Yes


; Test this enemy collided with the disk armour

                RUN_SPRITE_METHOD


.method_change_item:
		tst.w	PRIVATE_ELDER_STATE(a2)
		bmi	.exit

		move.w	#-1,DISKARM_DISABLE(a4)
	
; Play hit sound
                move.l  d0,-(a7)
                moveq   #SND_STONE_HIT,d0
                bsr     PLAY_SAMPLE
                move.l  (a7)+,d0
		
                subq.w  #1,PRIVATE_ELDER_HIT_COUNT(a2)

		tst.w	PRIVATE_ELDER_HIT_COUNT(a2)
		bne	.run_sprite_method
		
		move.w	#-1,PRIVATE_ELDER_STATE(a2)		; Set to active.
                move.w  #SPR_TYPE_ENEMY_2X32,SPRITE_PLOT_TYPE(a1)
		
		move.w	#ELDER_STATUS_SHOW,THIS_SPRITE_STATUS(a2)		
		bra	.exit



.method_destroy_sprite:
                clr.w   THIS_SPRITE_STATUS(a2)
                IFNE    ELDER_HWSPR_ENABLE
                IFNE    ELDER_HWSPR_UPPER
                lea     ENEMY_HWSPR_UPPER_ALLOCATED(a4),a0                      ; Free up hardware sprite
                ENDC
                IFNE    ELDER_HWSPR_MID
                lea     ENEMY_HWSPR_MID_ALLOCATED(a4),a0                        ; Free up hardware sprite
                ENDC
                IFNE    ELDER_HWSPR_LOWER
                lea     ENEMY_HWSPR_LOWER_ALLOCATED(a4),a0                      ; Free up hardware sprite
                ENDC
                move.w  THIS_SPRITE_HWSLOT(a2),d7
                bmi     .recreate
                FREE_HARDWARE_SPRITE
                ENDC
			
.recreate:
	        bra     DESTROY_SPRITE	
		
	 
.method_set_clean:
		cmp.w	#ELDER_STATUS_CLEAN,THIS_SPRITE_STATUS(a2)
		beq.s	.method_set_clean_0				; Don't play collect sound if already set.

.collect_elder:	bsr	ITEM_ELDER_ADD_POINTS
		move.l	d0,-(a7)
                moveq   #SND_PICKUP,d0
                bsr     PLAY_SAMPLE		
		move.l	(a7)+,d0

.method_set_clean_0:
		move.w  #ELDER_STATUS_CLEAN,THIS_SPRITE_STATUS(a2)
		moveq	#ELDER_STATUS_CLEAN,d1
		bra	ANIMATE

.method_animate_hidden:
		moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS
		moveq	#ELDER_STATUS_HIDDEN,d1
                bra     ANIMATE

.method_animate_show:		
		moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS
		
		lea	BYTE_SINE(a4),a0
		moveq	#0,d3		
		addq.b	#4,PRIVATE_ELDER_BYTE_INDEX(a2)	
		move.b	PRIVATE_ELDER_BYTE_INDEX(a2),d3
		move.b	(a0,d3),d3
		lsr.w	#3,d3
		add.w	PRIVATE_ELDER_FIXED_YPOS(a2),d3
		move.w	d3,SPRITE_PLOT_YPOS(a1)
		
		moveq	#ELDER_STATUS_SHOW,d1
                bra     ANIMATE	

.method_animate_clean:
		moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS
		moveq	#ELDER_STATUS_CLEAN,d1
                bsr     ANIMATE			
		tst.w	d6
		bmi	.method_destroy_sprite
		bra	.exit


.set_falldate:

.exit:          rts

