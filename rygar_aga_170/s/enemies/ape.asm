
APE_STATUS_CLEAR:          equ     6
APE_STATUS_WAIT:           equ     7
APE_STATUS_ROCK:		equ	8

APE_ROCK_INTERVAL_TIME:		equ	50
APE_ROCK_THROW_TIME:		equ	25


PRIVATE_APE_SPEED:         equ     32
PRIVATE_APE_FIXED_YPOS:    equ     34
PRIVATE_APE_TIMER:		equ	36



APE_TO_RYGAR_X1_BOUND:     equ     0
APE_TO_RYGAR_Y1_BOUND:     equ     0
APE_TO_RYGAR_X2_BOUND:     equ     32
APE_TO_RYGAR_Y2_BOUND:     equ     32

APE_TO_DISKARM_X1_BOUND:   equ     0
APE_TO_DISKARM_Y1_BOUND:   equ     0
APE_TO_DISKARM_X2_BOUND:   equ     32
APE_TO_DISKARM_Y2_BOUND:   equ     32


HDL_APE:
		FUNCID	#$e80b4ff3
                SPRITE_INITIATE

; Set the initial variables and status of the sprite.
.construct:
                move.w  SPRITE_PLOT_XPOS(a1),d2
                lsl.w   #4,d2
                move.w  d2,THIS_SPRITE_MAP_XPOS(a2)
                move.w  SPRITE_PLOT_YPOS(a1),PRIVATE_APE_FIXED_YPOS(a2)
                clr.w   THIS_SPRITE_SWEEP_INDEX(a2)
                move.w  #APE_STATUS_WAIT,THIS_SPRITE_STATUS(a2)
		
		move.w	#APE_ROCK_INTERVAL_TIME,PRIVATE_APE_TIMER(a2)
		
		tst.w	THIS_SPRITE_PARAMETER(a2)
		beq.s	.active
		move.w	THIS_SPRITE_PARAMETER(a2),d3
		move.w	d3,PRIVATE_APE_TIMER(a2)
		

.active:        ENEMY_CHECK_END_OF_ROUND

.sprite_0:      cmp.w   #ENEMY_STATUS_SWEEP_RIGHT,THIS_SPRITE_STATUS(a2)
                ble     .run_sprite_method

                bra.s   .sprite_2

.animate_table:
                dc.l    .method_destroy_sprite-.animate_table           ; 0
                dc.l    .method_animate_bones-.animate_table            ; 1
                dc.l    .method_animate_sweep_left-.animate_table       ; 2
                dc.l    .method_animate_sweep_right-.animate_table      ; 3
                dc.l    .method_animate_none-.animate_table      	; 4
                dc.l    .method_animate_none-.animate_table      	; 5
                dc.l    .method_animate_clear-.animate_table            ; 6
                dc.l    .method_animate_waiting-.animate_table          ; 7
		dc.l	.method_animate_throw_rock-.animate_table	; 8
                dc.l    .method_animate_none-.animate_table      	; 9
                dc.l    .method_animate_none-.animate_table      	; 10
                dc.l    .method_animate_none-.animate_table      	; 11
                dc.l    .method_animate_none-.animate_table      	; 12
                dc.l    .method_animate_none-.animate_table      	; 13
                dc.l    .method_animate_none-.animate_table      	; 14
                dc.l    .method_animate_none-.animate_table      	; 15
                dc.l    .method_animate_none-.animate_table      	; 16
                dc.l    .method_animate_none-.animate_table      	; 17
                dc.l    .method_animate_none-.animate_table      	; 18
                dc.l    .method_animate_none-.animate_table      	; 19
                dc.l    .method_animate_none-.animate_table      	; 20
		dc.l    .method_animate_none-.animate_table      	; 21
                dc.l    .method_animate_none-.animate_table      	; 22
                dc.l    .method_animate_none-.animate_table      	; 23
                dc.l    .method_animate_none-.animate_table      	; 24
		
.method_animate_none: 	bra	.exit


;Test this enemy collided with Rygar (the player)
.sprite_2:      move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4
                move.w  d3,d5
                add.w   #APE_TO_RYGAR_X1_BOUND,d2
                add.w   #APE_TO_RYGAR_Y1_BOUND,d3
                add.w   #APE_TO_RYGAR_X2_BOUND,d4
                add.w   #APE_TO_RYGAR_Y2_BOUND,d5
                lea     RYGAR_COORDS(a4),a3
                ENEMY_TO_RYGAR_COLLISION
                move.l  #POINTS_APE,d2
                tst.w   d6                                      ; Did this sprite collide with Rygar?
                IFEQ    DISABLE_ENEMY_COLLISION
                bmi     .method_set_collision_action            ; Yes
                ENDC

; Check if the enemy has been hit with disk armor
.sprite_stunned:
                move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4
                move.w  d3,d5
                add.w   #APE_TO_DISKARM_X1_BOUND,d2
                add.w   #APE_TO_DISKARM_Y1_BOUND,d3
                add.w   #APE_TO_DISKARM_X2_BOUND,d4
                add.w   #APE_TO_DISKARM_Y2_BOUND,d5
                lea     DISKARM_COORDS(a4),a3
                ENEMY_TO_DISKARM_COLLISION
                move.l  #POINTS_APE,d2
                tst.w   d6                                      ; Did this sprite get hit with the disk armor
                bmi     .method_set_sweep_direction             ; Yes

                RUN_SPRITE_METHOD

                METHOD_ANIMATE_BONES

                METHOD_ANIMATE_SWEEP_LEFT

                METHOD_ANIMATE_SWEEP_RIGHT

                METHOD_SET_BONES

                METHOD_SET_SWEEP_DIRECTION

                METHOD_SET_JUMP_OR_DESTROY

                METHOD_SET_RYGAR_DEATH_SEQUENCE

.method_set_waiting:	
		move.w  #APE_STATUS_WAIT,THIS_SPRITE_STATUS(a2)
		move.w	#APE_ROCK_INTERVAL_TIME,PRIVATE_APE_TIMER(a2)
		tst.w	THIS_SPRITE_PARAMETER(a2)
		beq	.exit
		move.w	THIS_SPRITE_PARAMETER(a2),d3
		move.w	d3,PRIVATE_APE_TIMER(a2)
		bra	.exit

.method_animate_waiting:
		subq.w	#1,PRIVATE_APE_TIMER(a2)
		tst.w	PRIVATE_APE_TIMER(a2)
		bmi.s	.method_set_throw_rock
                moveq   #0,d2
                move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                moveq   #APE_STATUS_WAIT,d1
                bra     ANIMATE
		
.method_set_throw_rock:
		move.w  #APE_STATUS_ROCK,THIS_SPRITE_STATUS(a2)	
		move.w	#APE_ROCK_THROW_TIME,PRIVATE_APE_TIMER(a2)

		movem.l d0-d7/a0-a3,-(a7)
                moveq   #SPR_ROCK,d0
                move.w  THIS_SPRITE_MAP_XPOS(a2),d1
                lsr.w   #4,d1
                move.w  SPRITE_PLOT_YPOS(a1),d2
		add.w	#16,d2
                moveq   #SPR_TYPE_ENEMY_1X16,d3
                lea     HDL_ROCK(pc),a0
                bsr     PUSH_SPRITE
                movem.l (a7)+,d0-d7/a0-a3
		
		bra	.exit
		
		
.method_animate_throw_rock:
		subq.w	#1,PRIVATE_APE_TIMER(a2)
		tst.w	PRIVATE_APE_TIMER(a2)
		bmi	.method_set_waiting
                moveq   #0,d2
                move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                moveq   #APE_STATUS_ROCK,d1
                bra     ANIMATE


.method_animate_clear:
                moveq   #APE_STATUS_CLEAR,d1
                movem.l d0-1/a1-a2,-(a7)
                bsr     ANIMATE
                movem.l (a7)+,d0-1/a1-a2
                tst.w   d6
                bmi     .method_destroy_sprite
                bra     .exit

.method_destroy_sprite:
                clr.w   THIS_SPRITE_STATUS(a2)
                bra     DESTROY_SPRITE


.method_set_collision_action:
                tst.w   RYGAR_FALLPREV_STATE(a4)                                ; Is Rygar falling on enemy
                bmi     .method_set_jump_or_destroy                     ; Yes!

                IFNE    DISABLE_ENEMY_COLLISION
                bra     .exit
                ELSE
                bra     .method_set_rygar_death_sequence
                ENDC

.method_set_clear:
                move.w  #1,SPRITE_PLOT_XPOS(a1)
                move.w  #1,SPRITE_PLOT_YPOS(a1)
                move.w  #APE_STATUS_CLEAR,THIS_SPRITE_STATUS(a2)
                bra.s   .set_update
                nop

.set_update:
.exit:          rts
