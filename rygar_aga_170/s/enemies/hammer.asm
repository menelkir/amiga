
HAMMER_GROUND_ENEMY:          equ     0       ; 1 = Ground enemy means enemy is stunnable.
HAMMER_HWSPR_ENABLE:          equ     0
HAMMER_HWSPR_UPPER:           equ     0
HAMMER_HWSPR_MID:             equ     0
HAMMER_HWSPR_LOWER:           equ     0

HAMMER_TO_RYGAR_X1_BOUND:     equ     0
HAMMER_TO_RYGAR_Y1_BOUND:     equ     0
HAMMER_TO_RYGAR_X2_BOUND:     equ     15
HAMMER_TO_RYGAR_Y2_BOUND:     equ     15

HAMMER_TO_DISKARM_X1_BOUND:   equ     0
HAMMER_TO_DISKARM_Y1_BOUND:   equ     0
HAMMER_TO_DISKARM_X2_BOUND:   equ     15
HAMMER_TO_DISKARM_Y2_BOUND:   equ     15

HAMMER_MOVE_SPEED:            equ     48
HAMMER_BASE_YPOS:             equ     126

HAMMER_STATUS_LEFT:           equ     6


PRIVATE_HAMMER_SPEED:         equ     32
PRIVATE_HAMMER_SINE_POINTER:	equ     34
PRIVATE_HAMMER_FIXED_YPOS:    equ     36
PRIVATE_HAMMER_BURN_COUNT:    equ     38


HDL_HAMMER:
		FUNCID	#$fe240ecc
                SPRITE_INITIATE

; Set the initial variables and status of the sprite.
.construct:
                move.w  SPRITE_PLOT_XPOS(a1),d2                 ; Set the initial Map Position
		subq.w	#8,d2
                lsl.w   #4,d2                           ; Based on the Plot position after
                move.w  d2,THIS_SPRITE_MAP_XPOS(a2)             ; Enemy is Spawned.
                move.w  #SPR_TYPE_ENEMY_1X16,SPRITE_PLOT_TYPE(a1)
                move.w  #HAMMER_BASE_YPOS,PRIVATE_HAMMER_FIXED_YPOS(a2)

                move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                move.w  MAP_PIXEL_POSX(a4),d3
                sub.w   #$100,d3
                lsr.w   #4,d2
                sub.w   d3,d2

                move.w  #HAMMER_STATUS_LEFT,THIS_SPRITE_STATUS(a2)

                move.w  SPRITE_PLOT_XPOS(a1),d2
                add.w   #16,d2
                lsl.w   #4,d2
                move.w  d2,THIS_SPRITE_MAP_XPOS(a2)

                ;bsr    FIREWAITSR


;;; Init code here.

.active:
                ENEMY_CHECK_END_OF_ROUND

.sprite_0:
;; Code here....
                bra.s   .sprite_2

.animate_table: dc.l    .method_animate_none-.animate_table              ; 0
                dc.l    .method_animate_bones-.animate_table    	; 1
                dc.l    .method_animate_sweep_left-.animate_table       ; 2
                dc.l    .method_animate_sweep_right-.animate_table      ; 3
		dc.l    .method_animate_none-.animate_table      	; 4
		dc.l    .method_animate_none-.animate_table      	; 5
                dc.l    .method_animate_hammer_left-.animate_table    	; 6
                dc.l    .method_animate_none-.animate_table   	; 7
                dc.l    .method_animate_none-.animate_table    		; 8
                dc.l    .method_animate_none-.animate_table      	; 9
                dc.l    .method_animate_none-.animate_table      	; 10
                dc.l    .method_animate_none-.animate_table      	; 11
                dc.l    .method_animate_none-.animate_table      	; 12
                dc.l    .method_animate_none-.animate_table      	; 13
                dc.l    .method_animate_none-.animate_table      	; 14
                dc.l    .method_animate_none-.animate_table      	; 15
                dc.l    .method_animate_none-.animate_table      	; 16
                dc.l    .method_animate_none-.animate_table      	; 17
                dc.l    .method_animate_none-.animate_table      	; 18
                dc.l    .method_animate_none-.animate_table      	; 19
                dc.l    .method_animate_none-.animate_table      	; 20
                dc.l    .method_animate_none-.animate_table      	; 21
                dc.l    .method_animate_none-.animate_table      	; 22
                dc.l    .method_animate_none-.animate_table      	; 23
                dc.l    .method_animate_none-.animate_table      	; 24
		
.method_animate_none:	bra	.exit

;Test this enemy collided with Rygar (the player)
.sprite_2:              move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4                                   ; Check a 32x32 boundary from x1/y1
                move.w  d3,d5
                add.w   #HAMMER_TO_RYGAR_X1_BOUND,d2
                add.w   #HAMMER_TO_RYGAR_Y1_BOUND,d3
                add.w   #HAMMER_TO_RYGAR_X2_BOUND,d4
                add.w   #HAMMER_TO_RYGAR_Y2_BOUND,d5
                lea     RYGAR_COORDS(a4),a3
                ENEMY_TO_RYGAR_COLLISION
		move.l	#POINTS_HAMMER,d2
                tst.w   d6                                      ; Did this sprite collide with Rygar?
                IFEQ    DISABLE_ENEMY_COLLISION
                bmi     .method_set_collision_action            ; Yes
                ENDC

; Check if the enemy has been hit with disk armor
.sprite_stunned:
                move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4                                   ; Check a 32x32 boundary from x1/y1
                move.w  d3,d5
                add.w   #HAMMER_TO_DISKARM_X1_BOUND,d2
                add.w   #HAMMER_TO_DISKARM_Y1_BOUND,d3
                add.w   #HAMMER_TO_DISKARM_X2_BOUND,d4
                add.w   #HAMMER_TO_DISKARM_Y2_BOUND,d5
                lea     DISKARM_COORDS(a4),a3
                ENEMY_TO_DISKARM_COLLISION
		move.l	#POINTS_HAMMER,d2
                tst.w   d6                                      ; Did this sprite get hit with the disk armor
                bmi     .method_set_sweep_direction             ; Yes

; Test this enemy collided with the disk armour

                RUN_SPRITE_METHOD

                METHOD_ANIMATE_BONES

                METHOD_ANIMATE_SWEEP_LEFT

                METHOD_ANIMATE_SWEEP_RIGHT

                METHOD_SET_BONES

                METHOD_SET_SWEEP_DIRECTION

                METHOD_SET_STUN_DIRECTION

                METHOD_SET_RYGAR_DEATH_SEQUENCE


.method_set_collision_action:
                tst.w   RYGAR_FALLPREV_STATE(a4)                                ; Is Rygar falling on enemy
                IFNE    HAMMER_GROUND_ENEMY
                bmi.s   .method_set_stun_or_destroy                     ; Yes!
                ELSE
                bmi.s   .method_set_jump_or_destroy                     ; Yes!
                ENDC

                IFNE    DISABLE_ENEMY_COLLISION
                bra     .exit
                ELSE
                bra     .method_set_rygar_death_sequence
                ENDC

                IFNE    HAMMER_GROUND_ENEMY
                METHOD_SET_STUN_OR_DESTROY                              ;
                ELSE
                METHOD_SET_JUMP_OR_DESTROY                              ;
                ENDC


;-------------------------------------------------------------------------
.method_destroy_sprite:
                clr.w   THIS_SPRITE_STATUS(a2)
                bra     DESTROY_SPRITE


;-------------------------------------------------------------------------
.method_set_hammer_left:
                move.w  #HAMMER_STATUS_LEFT,THIS_SPRITE_STATUS(a2)
                clr.w   PRIVATE_HAMMER_SINE_POINTER(a2)
                bra     .exit

.method_animate_hammer_left:
                lea     HAMMER_SINE(a4),a0
                move.w  PRIVATE_HAMMER_SINE_POINTER(a2),d2
                addq.w  #2,PRIVATE_HAMMER_SINE_POINTER(a2)
		moveq	#0,d3
                move.b  (a0,d2),d3
		cmp.b	#$80,d3
		beq	.method_destroy_sprite
	
		ext.w	d3
		
                move.w  PRIVATE_HAMMER_FIXED_YPOS(a2),d4
                add.w   d3,d4
                move.w  d4,SPRITE_PLOT_YPOS(a1)
		cmp.w	#230,d4
		bge	.method_destroy_sprite

                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                sub.w   #HAMMER_MOVE_SPEED,d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS
                moveq   #HAMMER_STATUS_LEFT,d1
                bra     ANIMATE


.set_update:

.exit:          rts

HAM1