
REAPER_STATUS_MOVE:          	equ     6

PRIVATE_REAPER_SPEED:         	equ     32
PRIVATE_REAPER_FIXED_YPOS:    	equ     34
PRIVATE_REAPER_SINE_PTR:	equ	36

HDL_REAPER:
		FUNCID	#$e80b4ff3
                SPRITE_INITIATE

; Set the initial variables and status of the sprite.
.construct:

.method_set_move:	
		move.w  #REAPER_STATUS_MOVE,THIS_SPRITE_STATUS(a2)
		moveq	#0,d4
		lea	SPRITE_SLOTS(a4),a0     ; Sprites base pointer
		move.l	(a0,d4*4),a0
		add.w	(a0),a0                 ; a0 now has sprite address

		move.w	SPRITE_PLOT_YPOS(a0),SPRITE_PLOT_YPOS(a1)
		sub.w	#32,SPRITE_PLOT_YPOS(a1)				; Set Reaper above Rygar
		
                move.w  SPRITE_PLOT_XPOS(a0),SPRITE_PLOT_XPOS(a1)
		subq.w	#4,SPRITE_PLOT_XPOS(a1)
		
		move.w	SPRITE_PLOT_YPOS(a1),PRIVATE_REAPER_FIXED_YPOS(a2)	; Store Fixed Yposition
		clr.w	PRIVATE_REAPER_SINE_PTR(a2)
.active:        
		moveq	#0,d2
		move.w  #REAPER_STATUS_MOVE,THIS_SPRITE_STATUS(a2)

		
.sprite_0:      
                bra.s   .sprite_2

.animate_table:
                dc.l    .method_destroy_sprite-.animate_table           ; 0
                dc.l    .method_animate_none-.animate_table            	; 1
                dc.l    .method_animate_none-.animate_table       	; 2
                dc.l    .method_animate_none-.animate_table      	; 3
                dc.l    .method_animate_none-.animate_table      	; 4
                dc.l    .method_animate_none-.animate_table      	; 5
                dc.l    .method_animate_move-.animate_table            	; 6
                dc.l    .method_animate_none-.animate_table          	; 7
		dc.l	.method_animate_none-.animate_table		; 8
                dc.l    .method_animate_none-.animate_table      	; 9
                dc.l    .method_animate_none-.animate_table      	; 10
                dc.l    .method_animate_none-.animate_table      	; 11
                dc.l    .method_animate_none-.animate_table      	; 12
                dc.l    .method_animate_none-.animate_table      	; 13
                dc.l    .method_animate_none-.animate_table      	; 14
                dc.l    .method_animate_none-.animate_table      	; 15
                dc.l    .method_animate_none-.animate_table      	; 16
                dc.l    .method_animate_none-.animate_table      	; 17
                dc.l    .method_animate_none-.animate_table      	; 18
                dc.l    .method_animate_none-.animate_table      	; 19
		
.method_animate_none: 	bra	.exit

.sprite_2:      

.sprite_stunned:
                RUN_SPRITE_METHOD

             

.method_animate_move:
		moveq	#0,d4
		lea	SPRITE_SLOTS(a4),a0     ; Sprites base pointer
		move.l	(a0,d4*4),a0
		add.w	(a0),a0                 ; a0 now has sprite address

		lea	ENEMY_GRAVITY_SINE_1(a4),a3
		move.w	PRIVATE_REAPER_SINE_PTR(a2),d2
		
		move.w	(a3,d2*2),d2
		cmp.w	#$8000,d2
		beq.s	.method_destroy_sprite
		addq.w	#1,PRIVATE_REAPER_SINE_PTR(a2)

		move.w	PRIVATE_REAPER_FIXED_YPOS(a2),d3
		sub.w	d2,d3
		move.w	d3,SPRITE_PLOT_YPOS(a1)
		add.w	#32,d3
		bmi.s	.method_destroy_sprite
		move.w	d3,SPRITE_PLOT_YPOS(a0)

		move.w	FRAME_BG,d2
		btst	#0,d2
		bne.s	.no_shift
		addq.w	#1,SPRITE_PLOT_XPOS(a1)
		addq.w	#1,SPRITE_PLOT_XPOS(a0)
.no_shift:

                moveq   #REAPER_STATUS_MOVE,d1
                bra     ANIMATE
		

.method_destroy_sprite:
                clr.w   THIS_SPRITE_STATUS(a2)
		clr.w	REAPER_ACTIVE(a4)
		move.w	#-1,RYGAR_THE_END(a4)
                bra     DESTROY_SPRITE

.set_update:
.exit:          rts
