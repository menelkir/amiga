VILLAGER_GROUND_ENEMY:          equ     1       ; 1 = Ground enemy means enemy is stunnable.
VILLAGER_HWSPR_ENABLE:          equ     0
VILLAGER_HWSPR_UPPER:           equ     0
VILLAGER_HWSPR_MID:             equ     0
VILLAGER_HWSPR_LOWER:           equ     0

VILLAGER_TO_RYGAR_X1_BOUND:     equ     0
VILLAGER_TO_RYGAR_Y1_BOUND:     equ     0
VILLAGER_TO_RYGAR_X2_BOUND:     equ     32
VILLAGER_TO_RYGAR_Y2_BOUND:     equ     32

VILLAGER_TO_DISKARM_X1_BOUND:   equ     0
VILLAGER_TO_DISKARM_Y1_BOUND:   equ     0
VILLAGER_TO_DISKARM_X2_BOUND:   equ     32
VILLAGER_TO_DISKARM_Y2_BOUND:   equ     32

VILLAGER_MOVE_SPEED:            equ     32

VILLAGER_GO_LEFT:		equ	-1
VILLAGER_GO_RIGHT:		equ	0

VILLAGER_STATUS_OUTOFHUT:	equ     6
VILLAGER_STATUS_UPPER_LEFT:	equ	7
VILLAGER_STATUS_UPPER_RIGHT:	equ	8
VILLAGER_STATUS_DESCEND:	equ	9
VILLAGER_STATUS_LOWER_LEFT:	equ	10
VILLAGER_STATUS_LOWER_RIGHT:	equ	11
VILLAGER_STATUS_DELAY_FRAMES:	equ	12

PRIVATE_VILLAGER_SPEED:         equ     32
PRIVATE_VILLAGER_MOVE_FRAMES:	equ     34
PRIVATE_VILLAGE_DIRECTION:	equ	36
PRIVATE_VILLAGER_FIXED_YPOS:	equ	38
PRIVATE_VILLAGER_SINE_POINTER:	equ	40
PRIVATE_VILLAGER_BOUNDARY_TIMER:	equ	42
PRIVATE_VILLAGER_RANDOM_SEED:	equ	44
PRIVATE_VILLAGER_DELAY_FRAMES:	equ	46


HDL_VILLAGER:
		FUNCID	#$b09c91ea
                SPRITE_INITIATE

; Set the initial variables and status of the sprite.
.construct:
                move.w  SPRITE_PLOT_XPOS(a1),d2                 ; Set the initial Map Position
                lsl.w   #4,d2                           ; Based on the Plot position after
                move.w  d2,THIS_SPRITE_MAP_XPOS(a2)             ; Enemy is Spawned.
                move.w  #VILLAGER_MOVE_SPEED,PRIVATE_VILLAGER_SPEED(a2) ; Set move speed.
		add.w	d0,PRIVATE_VILLAGER_SPEED(a2)
		
		move.w	ROUND_CURRENT(a4),d2
		lsr.w	#1,d2
		add.w	d2,PRIVATE_VILLAGER_SPEED(a2)
		
		move.w	RANDOM_SEED(a4),d2
		rol.w	d0,d2
		move.w	d2,PRIVATE_VILLAGER_RANDOM_SEED(a2)
		
		move.w	#-1,THIS_SPRITE_STUN_TIMER(a2)
		
		move.w	THIS_SPRITE_PARAMETER(a2),PRIVATE_VILLAGER_DELAY_FRAMES(a2)
		
		move.w	#VILLAGER_STATUS_DELAY_FRAMES,THIS_SPRITE_STATUS(a2)
;;; Init code here.

.active:
                ENEMY_CHECK_END_OF_ROUND			;calls sprite_0

.sprite_0:
;; Code here....
; Check boundary of sprite!
                moveq   #0,d2
                move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                lea     BOUNDARY_X1POS(a4),a0
		cmp.l	(a0),d2
		blt.s	.dectimer
		cmp.l	4(a0),d2
		bgt.s	.dectimer
		bra.s	.inbound
.dectimer:	subq.w  #1,PRIVATE_VILLAGER_BOUNDARY_TIMER(a2)
                bpl.s   .outbound
                move.w  #ENEMY_STATUS_BONES,THIS_SPRITE_STATUS(a2)
.inbound:       move.w  #ENEMY_BOUNDARY_TIME,PRIVATE_VILLAGER_BOUNDARY_TIMER(a2)
.outbound:      bra.s   .sprite_1



.animate_table: dc.l    .method_animate_none-.animate_table      	; 
                dc.l    .method_animate_bones-.animate_table    ; 1
                dc.l    .method_animate_sweep_left-.animate_table       ; 2
                dc.l    .method_animate_sweep_right-.animate_table      ; 3
                dc.l    .method_animate_stun_left-.animate_table       ; 4
                dc.l    .method_animate_stun_right-.animate_table      ; 5
                dc.l    .method_animate_outofhut-.animate_table      	; 6
                dc.l    .method_animate_upper_left-.animate_table      	; 7
                dc.l    .method_animate_upper_right-.animate_table      ; 8
                dc.l    .method_animate_descend-.animate_table      	; 9
                dc.l    .method_animate_lower_left-.animate_table      	; 10
                dc.l    .method_animate_lower_right-.animate_table      ; 11
                dc.l    .method_animate_delay-.animate_table      	; 12
                dc.l    .method_animate_none-.animate_table      	; 13
                dc.l    .method_animate_none-.animate_table      	; 14
                dc.l    .method_animate_none-.animate_table      	; 15
                dc.l    .method_animate_none-.animate_table      	; 16
                dc.l    .method_animate_none-.animate_table      	; 17
                dc.l    .method_animate_none-.animate_table      	; 18
                dc.l    .method_animate_none-.animate_table      	; 19
                dc.l    .method_animate_none-.animate_table      	; 15
                dc.l    .method_animate_none-.animate_table      	; 16
                dc.l    .method_animate_none-.animate_table      	; 17
                dc.l    .method_animate_none-.animate_table      	; 18
                dc.l    .method_animate_none-.animate_table      	; 19

.method_animate_none:	bra	.exit

.sprite_1:      cmp.w   #ENEMY_STATUS_SWEEP_RIGHT,THIS_SPRITE_STATUS(a2)                ; if doing bones or sweeps then skip all collisions
                ble     .run_sprite_method
		tst.w	SHIELD_TIMER
		bpl.s	.shield_on
                ;cmp.w   #ENEMY_STATUS_STUN_RIGHT,THIS_SPRITE_STATUS(a2)                 ; if doing stuns then skip Rygar collision
                
		cmp.w	#VILLAGER_STATUS_OUTOFHUT,THIS_SPRITE_STATUS(a2)
		ble     .sprite_stunned

.shield_on:

;Test this enemy collided with Rygar (the player)
.sprite_2:              move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4                                   ; Check a 32x32 boundary from x1/y1
                move.w  d3,d5
                add.w   #VILLAGER_TO_RYGAR_X1_BOUND,d2
                add.w   #VILLAGER_TO_RYGAR_Y1_BOUND,d3
                add.w   #VILLAGER_TO_RYGAR_X2_BOUND,d4
                add.w   #VILLAGER_TO_RYGAR_Y2_BOUND,d5
                lea     RYGAR_COORDS(a4),a3
                ENEMY_TO_RYGAR_COLLISION
                move.l  #POINTS_VILLAGER,d2
                tst.w   d6                                      ; Did this sprite collide with Rygar?
                IFEQ    DISABLE_ENEMY_COLLISION
                bmi     .method_set_collision_action            ; Yes
                ENDC

; Check if the enemy has been hit with disk armor
.sprite_stunned:
                move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4                                   ; Check a 32x32 boundary from x1/y1
                move.w  d3,d5
                add.w   #VILLAGER_TO_DISKARM_X1_BOUND,d2
                add.w   #VILLAGER_TO_DISKARM_Y1_BOUND,d3
                add.w   #VILLAGER_TO_DISKARM_X2_BOUND,d4
                add.w   #VILLAGER_TO_DISKARM_Y2_BOUND,d5
                lea     DISKARM_COORDS(a4),a3
                ENEMY_TO_DISKARM_COLLISION
                move.l  #POINTS_VILLAGER,d2
                tst.w   d6                                      ; Did this sprite get hit with the disk armor
                bmi     .method_set_sweep_direction             ; Yes

; Test this enemy collided with the disk armour

                RUN_SPRITE_METHOD

                METHOD_ANIMATE_BONES

                METHOD_ANIMATE_SWEEP_LEFT

                METHOD_ANIMATE_SWEEP_RIGHT

                METHOD_SET_BONES

                METHOD_SET_SWEEP_DIRECTION

                METHOD_SET_STUN_DIRECTION
		
		METHOD_SET_STUN_OR_DESTROY                              ;

                METHOD_SET_RYGAR_DEATH_SEQUENCE

.method_animate_stun_left:	
                moveq   #ENEMY_STATUS_STUN_LEFT,d1
                move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                subq.w  #1,THIS_SPRITE_STUN_TIMER(a2)
                bmi     .method_set_facing
                ENEMY_SHAKE
                bra     ANIMATE

.method_animate_stun_right:		
                moveq   #ENEMY_STATUS_STUN_RIGHT,d1
                move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                subq.w  #1,THIS_SPRITE_STUN_TIMER(a2)
                bmi     .method_set_facing
                ENEMY_SHAKE
                bra     ANIMATE

.method_animate_delay:
		subq.w	#1,PRIVATE_VILLAGER_DELAY_FRAMES(a2)
		tst.w	PRIVATE_VILLAGER_DELAY_FRAMES(a2)
		bpl	.exit
		move.w	#VILLAGER_STATUS_OUTOFHUT,THIS_SPRITE_STATUS(a2)
		bra	.exit

.method_set_collision_action:
                tst.w   RYGAR_FALLPREV_STATE(a4)                                ; Is Rygar falling on enemy
                bmi   .method_set_stun_or_destroy                     ; Yes!

                IFNE    DISABLE_ENEMY_COLLISION
                bra     .exit
                ELSE
                bra     .method_set_rygar_death_sequence
                ENDC

.method_destroy_sprite: 
		subq.w  #1,VILLAGERS_IN_USE(a4)

                clr.w   THIS_SPRITE_STATUS(a2)
                IFNE    VILLAGER_HWSPR_ENABLE
                IFNE    VILLAGER_HWSPR_UPPER
                lea     ENEMY_HWSPR_UPPER_ALLOCATED(a4),a0                      ; Free up hardware sprite
                ENDC
                IFNE    VILLAGER_HWSPR_MID
                lea     ENEMY_HWSPR_MID_ALLOCATED(a4),a0                        ; Free up hardware sprite
                ENDC
                IFNE    VILLAGER_HWSPR_LOWER
                lea     ENEMY_HWSPR_LOWER_ALLOCATED(a4),a0                      ; Free up hardware sprite
                ENDC
                move.w  THIS_SPRITE_HWSLOT(a2),d7
                bmi     DESTROY_SPRITE
                FREE_HARDWARE_SPRITE
                ENDC
                bra     DESTROY_SPRITE
		
.method_set_facing:
		move.w	RYGAR_XPOS(a4),d2
		cmp.w	SPRITE_PLOT_XPOS(a1),d2
		ble	.method_set_lower_left
		bra	.method_set_lower_right

.method_set_outofhut:
		move.w	#VILLAGER_STATUS_OUTOFHUT,THIS_SPRITE_STATUS(a2)
		bra	.exit
		
.method_animate_outofhut:
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS

		moveq	#VILLAGER_STATUS_OUTOFHUT,d1
		movem.l	d0-d1/a1-a2,-(a7)
		bsr	ANIMATE
		movem.l	(a7)+,d0-d1/a1-a2
		tst.w	d6
		bmi.s	.method_set_run_or_descend
		bra	.exit
		
.method_set_run_or_descend:
		cmp.w	#10,ROUND_CURRENT(a4)		; 10 < 17
		blt.s	.method_set_run_direction	; True = Round 6
		bra.s	.method_set_descend		; False = Round 17/18

; If on rounds 17/18 then set MOVE frames to 0.
	
.method_set_run_direction:
		move.w  VPOSR(a5),d2				; If on low platform
                swap    d2					; Move frames is 
                move.b  $bfe801,d2
                lsl.w   #8,d2
                move.b  $bfd800,d2
		rol.w	d0,d2
		move.w	PRIVATE_VILLAGER_RANDOM_SEED(a2),d3
		eor.w	d3,d2
		and.w	#$7f,d2					; Possible to move 128 pixels max.
		move.w	d2,PRIVATE_VILLAGER_MOVE_FRAMES(a2)
		
                and.w   #1,d2
                beq   	.method_set_upper_right
		bra	.method_set_upper_left

.method_set_descend:
		move.w	#VILLAGER_STATUS_DESCEND,THIS_SPRITE_STATUS(a2)
                move.w  SPRITE_PLOT_YPOS(a1),PRIVATE_VILLAGER_FIXED_YPOS(a2)
                clr.w   PRIVATE_VILLAGER_SINE_POINTER(a2)
		bra	.exit

.method_set_upper_left:
		move.w	#VILLAGER_STATUS_UPPER_LEFT,THIS_SPRITE_STATUS(a2)
		move.w	#-1,PRIVATE_VILLAGE_DIRECTION(a2)

.method_animate_upper_left:
		subq.w	#1,PRIVATE_VILLAGER_MOVE_FRAMES(a2)
		tst.w	PRIVATE_VILLAGER_MOVE_FRAMES(a2)
		bmi	.method_set_descend
		
		cmp.w	#16,SPRITE_PLOT_XPOS(a1)
		ble.s	.method_set_upper_right
		
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  d6,d2
		sub.w	PRIVATE_VILLAGER_SPEED(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
		moveq	#VILLAGER_STATUS_UPPER_LEFT,d1
		bra	ANIMATE

.method_set_upper_right:
		move.w	#VILLAGER_STATUS_UPPER_RIGHT,THIS_SPRITE_STATUS(a2)
		clr.w	PRIVATE_VILLAGE_DIRECTION(a2)

.method_animate_upper_right:
		subq.w	#1,PRIVATE_VILLAGER_MOVE_FRAMES(a2)
		tst.w	PRIVATE_VILLAGER_MOVE_FRAMES(a2)
		bmi	.method_set_descend
		cmp.w	#288,SPRITE_PLOT_XPOS(a1)
		bge.s	.method_set_upper_left
		
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  d6,d2
		add.w	PRIVATE_VILLAGER_SPEED(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
		moveq	#VILLAGER_STATUS_UPPER_RIGHT,d1
		bra	ANIMATE		
	

		
.method_animate_descend:
		lea     ENEMY_GRAVITY_SINE(a4),a0
                move.w  PRIVATE_VILLAGER_SINE_POINTER(a2),d3
                addq.w  #2,PRIVATE_VILLAGER_SINE_POINTER(a2)
                move.w  PRIVATE_VILLAGER_FIXED_YPOS(a2),d4
                add.w   (a0,d3*2),d4

                cmp.w   #146,d4
                ble	.method_animate_descend_0
		bra	.method_set_facing
	


.method_animate_descend_0:		
		move.w  d4,SPRITE_PLOT_YPOS(a1)
		
		moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  d6,d2
                UPDATE_SPRITE_PLOT_XPOS

		moveq	#VILLAGER_STATUS_DESCEND,d1
		bra	ANIMATE	

.method_set_lower_left:
		move.w	#VILLAGER_STATUS_LOWER_LEFT,THIS_SPRITE_STATUS(a2)
		bra	.exit

.method_animate_lower_left:
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  d6,d2
		sub.w	PRIVATE_VILLAGER_SPEED(a2),d2           
		UPDATE_SPRITE_PLOT_XPOS
		
		moveq	#VILLAGER_STATUS_LOWER_LEFT,d1
		bra	ANIMATE

.method_set_lower_right:
		move.w	#VILLAGER_STATUS_LOWER_RIGHT,THIS_SPRITE_STATUS(a2)
		bra	.exit		

.method_animate_lower_right:
                moveq   #0,d6
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                move.w  d6,d2
		add.w	PRIVATE_VILLAGER_SPEED(a2),d2           
		UPDATE_SPRITE_PLOT_XPOS
		moveq	#VILLAGER_STATUS_LOWER_RIGHT,d1
		bra	ANIMATE


.set_update:

.exit:          rts
