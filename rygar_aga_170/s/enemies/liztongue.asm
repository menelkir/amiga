LIZTONGUE_GROUND_ENEMY:          equ     0       ; 1 = Ground enemy means enemy is stunnable.
LIZTONGUE_HWSPR_ENABLE:          equ     0
LIZTONGUE_HWSPR_UPPER:           equ     0
LIZTONGUE_HWSPR_MID:             equ     0
LIZTONGUE_HWSPR_LOWER:           equ     0

LIZTONGUE_STATUS_LEFT:		equ	6
LIZTONGUE_STATUS_RIGHT:		equ	7

LIZTONGUE_FRAME_SPEED:		equ	2

LIZTONGUE_TO_RYGAR_X1_BOUND:     equ     0
LIZTONGUE_TO_RYGAR_Y1_BOUND:     equ     0
LIZTONGUE_TO_RYGAR_X2_BOUND:     equ     48
LIZTONGUE_TO_RYGAR_Y2_BOUND:     equ     16

PRIVATE_FRAME_COUNTER:         	equ     32
PRIVATE_COLLISION_STEP:		equ	34

HDL_LIZTONGUE:
		FUNCID	#$842f96a4
                SPRITE_INITIATE

; Set the initial variables and status of the sprite.
.construct:
                move.w  SPRITE_PLOT_XPOS(a1),d2                 ; Set the initial Map Position
                lsl.w   #4,d2                           ; Based on the Plot position after
                move.w  d2,THIS_SPRITE_MAP_XPOS(a2)             ; Enemy is Spawned.
		
		clr.w	PRIVATE_COLLISION_STEP(a2)
		clr.w	PRIVATE_FRAME_COUNTER(a2)
		
		UPDATE_SPRITE_PLOT_XPOS
		
		move.w	RYGAR_XPOS(a4),d2
		cmp.w	SPRITE_PLOT_XPOS(a1),d2
		ble	.method_set_left
		bra	.method_set_right
		
;;; Init code here.
.active:
                ENEMY_CHECK_END_OF_ROUND
		
.sprite_0:
		addq.w	#4,PRIVATE_COLLISION_STEP(a2)
		cmp.w	#16,PRIVATE_FRAME_COUNTER(a2)		; Reset frame counter if the sprite is on 
		ble	.sprite_2				; it's way back.
		subq.w	#8,PRIVATE_COLLISION_STEP(a2)
		bra	.sprite_2
;; Code here....

		tst.w	SHIELD_TIMER
		bpl.s	.shield_on
                cmp.w   #ENEMY_STATUS_STUN_RIGHT,THIS_SPRITE_STATUS(a2)                 ; if doing stuns then skip Rygar collision
                ble     .sprite_stunned

.shield_on:
                bra.s   .sprite_2

.animate_table: dc.l    .method_animate_none-.animate_table             ; 0
                dc.l    .method_animate_bones-.animate_table    	; 1
                dc.l    .method_animate_sweep_left-.animate_table       ; 2
                dc.l    .method_animate_sweep_right-.animate_table      ; 3
		dc.l    .method_animate_none-.animate_table      	; 4
		dc.l    .method_animate_none-.animate_table      	; 5
                dc.l    .method_animate_left-.animate_table      	; 6	- 16x16
                dc.l    .method_animate_right-.animate_table      	; 7	- 16x16
                dc.l    .method_animate_none-.animate_table      	; 8
                dc.l    .method_animate_none-.animate_table      	; 9
                dc.l    .method_animate_none-.animate_table      	; 10
                dc.l    .method_animate_none-.animate_table      	; 11
                dc.l    .method_animate_none-.animate_table      	; 12
                dc.l    .method_animate_none-.animate_table      	; 13
                dc.l    .method_animate_none-.animate_table      	; 14
                dc.l    .method_animate_none-.animate_table      	; 15
                dc.l    .method_animate_none-.animate_table      	; 16
                dc.l    .method_animate_none-.animate_table      	; 17
                dc.l    .method_animate_none-.animate_table      	; 18
                dc.l    .method_animate_none-.animate_table      	; 19
                dc.l    .method_animate_none-.animate_table      	; 20
                dc.l    .method_animate_none-.animate_table      	; 21
                dc.l    .method_animate_none-.animate_table      	; 22
                dc.l    .method_animate_none-.animate_table      	; 23
                dc.l    .method_animate_none-.animate_table      	; 24
		
.method_animate_none:
		bra	.exit

;Test this enemy collided with Rygar (the player)
.sprite_2:      



; Check if the enemy has been hit with disk armor
.sprite_stunned:

; Test this enemy collided with the disk armour

                RUN_SPRITE_METHOD

                METHOD_ANIMATE_BONES

                METHOD_ANIMATE_SWEEP_LEFT

                METHOD_ANIMATE_SWEEP_RIGHT

                METHOD_SET_BONES

                METHOD_SET_SWEEP_DIRECTION

                METHOD_SET_STUN_DIRECTION

                METHOD_SET_RYGAR_DEATH_SEQUENCE


.method_set_collision_action:
                tst.w   RYGAR_FALLPREV_STATE(a4)                                ; Is Rygar falling on enemy
                IFNE    LIZTONGUE_GROUND_ENEMY
                bmi.s   .method_set_stun_or_destroy                     ; Yes!
                ELSE
                bmi.s   .method_set_jump_or_destroy                     ; Yes!
                ENDC

                IFNE    DISABLE_ENEMY_COLLISION
                bra     .exit
                ELSE
                bra     .method_set_rygar_death_sequence
                ENDC

                IFNE    LIZTONGUE_GROUND_ENEMY
                METHOD_SET_STUN_OR_DESTROY                              ;
                ELSE
                METHOD_SET_JUMP_OR_DESTROY                              ;
                ENDC


.method_destroy_sprite: 
                clr.w   THIS_SPRITE_STATUS(a2)
                IFNE    LIZTONGUE_HWSPR_ENABLE
                IFNE    LIZTONGUE_HWSPR_UPPER
                lea     ENEMY_HWSPR_UPPER_ALLOCATED(a4),a0                      ; Free up hardware sprite
                ENDC
                IFNE    LIZTONGUE_HWSPR_MID
                lea     ENEMY_HWSPR_MID_ALLOCATED(a4),a0                        ; Free up hardware sprite
                ENDC
                IFNE    LIZTONGUE_HWSPR_LOWER
                lea     ENEMY_HWSPR_LOWER_ALLOCATED(a4),a0                      ; Free up hardware sprite
                ENDC
                move.w  THIS_SPRITE_HWSLOT(a2),d7
                bmi     DESTROY_SPRITE
                FREE_HARDWARE_SPRITE
                ENDC
                bra     DESTROY_SPRITE

.check_collision_left:

		move.w  SPRITE_PLOT_XPOS(a1),d2
		add.w	#64,d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4                                   ; Check a 32x32 boundary from x1/y1
                move.w  d3,d5
								; Add 28 maximum frame count
		sub.w	PRIVATE_COLLISION_STEP(a2),d2		; then subtract toward Rygar
                add.w   #LIZTONGUE_TO_RYGAR_Y2_BOUND,d5
		
		;move.w	PRIVATE_COLLISION_STEP(a2),DEBUG_LIZCOLLSTEP
		;move.w	PRIVATE_FRAME_COUNTER(a2),DEBUG_LIZCOLLFRAME
		;move.w	d2,DEBUG_LIZTONGUE_X1
		;move.w	d3,DEBUG_LIZTONGUE_Y1
		;move.w	d4,DEBUG_LIZTONGUE_X2
		;move.w	d5,DEBUG_LIZTONGUE_Y2

                lea     RYGAR_COORDS(a4),a3
                moveq   #0,d6
.ryg_coll_loopl: cmp.w   2(a3),d2                                ; Check X1 against Rygar X2
                bgt.s   .ryg_coll_nextl
                cmp.w   (a3),d4                                 ; Check X2 against Rygar X1
                blt.s   .ryg_coll_nextl
                cmp.w   6(a3),d3                                ; Check Y1 against Rygar Y2
                bgt.s   .ryg_coll_nextl
                cmp.w   4(a3),d5                                ; Check Y2 against Rygar Y1
                blt.s   .ryg_coll_nextl
                moveq   #-1,d6

.ryg_coll_nextl: nop
.ryg_coll_exitl:
                rts

.check_collision_right:
		move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4                                   ; Check a 32x32 boundary from x1/y1
                move.w  d3,d5
		add.w	PRIVATE_COLLISION_STEP(a2),d4		; then add frame counter offset.        
                add.w   #LIZTONGUE_TO_RYGAR_Y2_BOUND,d5

		
                lea     RYGAR_COORDS(a4),a3
		moveq   #0,d6
.ryg_coll_loopr: cmp.w   2(a3),d2                                ; Check X1 against Rygar X2
                bgt.s   .ryg_coll_nextr
                cmp.w   (a3),d4                                 ; Check X2 against Rygar X1
                blt.s   .ryg_coll_nextr
                cmp.w   6(a3),d3                                ; Check Y1 against Rygar Y2
                bgt.s   .ryg_coll_nextr
                cmp.w   4(a3),d5                                ; Check Y2 against Rygar Y1
                blt.s   .ryg_coll_nextr
                moveq   #-1,d6
.ryg_coll_nextr: nop
.ryg_coll_exitr:
                rts
	

;--------------------------------------
.method_set_left:
		move.w	#LIZTONGUE_STATUS_LEFT,THIS_SPRITE_STATUS(a2)
		clr.w	PRIVATE_FRAME_COUNTER(a2)
		;bra	.exit
		
.method_animate_left:
		bsr	.check_collision_left
		tst.w   d6                                      ; Did this sprite collide with Rygar?
                IFEQ    DISABLE_ENEMY_COLLISION
                bmi     .method_set_collision_action            ; Yes
                ENDC
		
		addq.w	#1,PRIVATE_FRAME_COUNTER(a2)
		moveq	#0,d7
		moveq	#0,d2
		move.b	SCROLL_DIRECTION_Y(a4),d2
		add.w	d2,d2
		ext.w	d2
		sub.w	d2,SPRITE_PLOT_YPOS(a1)
		
		moveq   #0,d2
                move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
		moveq	#LIZTONGUE_STATUS_LEFT,d1
		movem.l	d0-d1/a1-a2,-(a7)
		bsr	ANIMATE
		movem.l	(a7)+,d0-d1/a1-a2
		tst.w	d6
		bmi	.method_destroy_sprite
		bra	.exit

.method_set_right:
		move.w	#LIZTONGUE_STATUS_RIGHT,THIS_SPRITE_STATUS(a2)
		clr.w	PRIVATE_FRAME_COUNTER(a2)
		add.w	#80*16,THIS_SPRITE_MAP_XPOS(a2)
		;bra	.exit
		
.method_animate_right:
		bsr	.check_collision_right
		tst.w   d6                                      ; Did this sprite collide with Rygar?
                IFEQ    DISABLE_ENEMY_COLLISION
                bmi     .method_set_collision_action            ; Yes
                ENDC
		
		addq.w	#1,PRIVATE_FRAME_COUNTER(a2)
		moveq	#0,d7
		moveq	#0,d2
		move.b	SCROLL_DIRECTION_Y(a4),d2
		add.w	d2,d2
		ext.w	d2
		sub.w	d2,SPRITE_PLOT_YPOS(a1)
		
		moveq   #0,d2
                move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
		moveq	#LIZTONGUE_STATUS_RIGHT,d1
		movem.l	d0-d1/a1-a2,-(a7)
		bsr	ANIMATE
		movem.l	(a7)+,d0-d1/a1-a2
		tst.w	d6
		bmi	.method_destroy_sprite
		bra	.exit
	
	
	
.set_update:

.exit:          rts
