ENEMY_GRIFFIN_XPOS:             equ     500
ENEMY_GRIFFIN_YPOS:             equ     50

GRIFFIN_GROUND_ENEMY:           equ     0       ; 1 = Ground enemy means enemy is stunnable.
GRIFFIN_HWSPR_ENABLE:           equ     1
GRIFFIN_HWSPR_UPPER:            equ     1
GRIFFIN_HWSPR_MID:              equ     0
GRIFFIN_HWSPR_LOWER:            equ     0

GRIFFIN_SPRITE_TEST:		equ	0

GRIFFIN_FLY_SPEED:              equ     24

GRIFFIN_TO_RYGAR_X1_BOUND:      equ     4
GRIFFIN_TO_RYGAR_Y1_BOUND:      equ     8
GRIFFIN_TO_RYGAR_X2_BOUND:      equ     27
GRIFFIN_TO_RYGAR_Y2_BOUND:      equ     27

GRIFFIN_TO_DISKARM_X1_BOUND:    equ     6
GRIFFIN_TO_DISKARM_Y1_BOUND:    equ     6
GRIFFIN_TO_DISKARM_X2_BOUND:    equ     31-6
GRIFFIN_TO_DISKARM_Y2_BOUND:    equ     31-6


GRIFFIN_STATUS_FLY_LEFT:        equ     6
GRIFFIN_STATUS_FLY_RIGHT        equ     7
GRIFFIN_STATUS_SWOOP_LEFT:      equ     8
GRIFFIN_STATUS_SWOOP_RIGHT      equ     9
GRIFFIN_STATUS_TEST:	      	equ     10

GRIFFIN_GO_LEFT:                        equ     -1
GRIFFIN_GO_RIGHT:                       equ     0

GRIFFIN_ATTACK_LEFT_XPOS:               equ     66
GRIFFIN_ATTACK_RIGHT_XPOS:              equ     212

; Private Variables
PRIVATE_GRIFFIN_SPEED:                  equ     32
PRIVATE_GRIFFIN_FIXED_YPOS:             equ     34      ; Y Position of Sprite.
PRIVATE_GRIFFIN_SINE_INDEX:             equ     36      ; Index to add to Y Position
PRIVATE_GRIFFIN_DIRECTION:              equ     38      ; Current FLYing direction.
PRIVATE_GRIFFIN_XPOS:                   equ     40
PRIVATE_GRIFFIN_YPOS:                   equ     42
PRIVATE_GRIFFIN_ANIM_FRAME:             equ     44
PRIVATE_GRIFFIN_BOUNDARY_TIMER:         equ     46
PRIVATE_GRIFFIN_SWEEP_INDEX:            equ     48
PRIVATE_GRIFFIN_LAST_ANIM:              equ     50
PRIVATE_GRIFFIN_SWOOP_INDEX:            equ     52
PRIVATE_GRIFFIN_ROCK_ENABLE:		equ	54


; Input
;d2=X Position of Enemy
;d3=Y position of Enemy
;a3 = Coordinates pointer x1,x2,y1,y2
; Output
;d6=Return value (-1 is collision)



HDL_GRIFFIN:
		FUNCID	#$f812e1ff
                SPRITE_INITIATE

; Set the initial variables and status of the sprite.
.construct:

		move.l	d0,-(a7)
		moveq	#SND_GRIFFIN,d0
		bsr	PLAY_SAMPLE
		move.l	(a7)+,d0

                FIND_CAMERA_XPOS

                move.w  VPOSR(a5),d2
                swap    d2
                move.b  $bfe801,d2
                lsl.w   #8,d2
                move.b  $bfd800,d2
                and.w   #1,d2
		IFEQ	GRIFFIN_SPRITE_TEST
                beq.s   .create_right
		ELSE
		bra.s	.create_right
		ENDC

.create_left:   exg     d6,d7                                           ; set space position
                move.w  #GRIFFIN_STATUS_FLY_RIGHT,THIS_SPRITE_STATUS(a2)        ; Create to the right, FLY left
                move.w  #GRIFFIN_GO_RIGHT,PRIVATE_GRIFFIN_DIRECTION(a2)
                bra.s   .create
.create_right:  move.w  #GRIFFIN_STATUS_FLY_LEFT,THIS_SPRITE_STATUS(a2) ; Create to the left, FLY right
                move.w  #GRIFFIN_GO_LEFT,PRIVATE_GRIFFIN_DIRECTION(a2)
.create:
; d7 = spawn x position
                move.w  d7,THIS_SPRITE_MAP_XPOS(a2)                     ; Set start position in the map.
                move.w  #GRIFFIN_FLY_SPEED,PRIVATE_GRIFFIN_SPEED(a2)
                move.w  #ENEMY_BOUNDARY_TIME,PRIVATE_GRIFFIN_BOUNDARY_TIMER(a2)
                move.w  #ENEMY_GRIFFIN_YPOS,PRIVATE_GRIFFIN_FIXED_YPOS(a2)
                move.w  #-1,PRIVATE_GRIFFIN_SWOOP_INDEX(a2)

; Generate Random Number
                move.w  VPOSR(a5),d2
                swap    d2
                move.b  $bfe801,d2
                lsl.w   #8,d2
                move.b  $bfd800,d2
                and.w   #%0000000000001111,d2
                add.w   d2,PRIVATE_GRIFFIN_FIXED_YPOS(a2)

		move.w	TIME_REMAINING(a4),d3
		ror.w	d2,d3
                and.w   #%0000000000001111,d3	
	
		move.w	d3,PRIVATE_GRIFFIN_SINE_INDEX(a2)

                IFNE    GRIFFIN_HWSPR_ENABLE
                move.w  #-1,THIS_SPRITE_HWSLOT(a2)
                lea     ENEMY_HWSPR_UPPER_ALLOCATED(a4),a0                      ; Free up hardware sprite
                bsr     ALLOC_HARDWARE_SPRITE
                tst.w   d7
                bmi.s   .active
                move.w  (a0),SPRITE_PLOT_TYPE(a1)                               ; Set hardware type.
                move.w  d7,THIS_SPRITE_HWSLOT(a2)
                ENDC
		
		cmp.w	#15,ROUND_CURRENT(a4)
		ble.s	.active
		move.w	#-1,PRIVATE_GRIFFIN_ROCK_ENABLE(a2)




.active:        ENEMY_CHECK_END_OF_ROUND

                tst.w   SPRITE_DESTROY_END_ROUND(a2)
                beq.s   .sprite_0
                clr.w   SPRITE_DESTROY_END_ROUND(a2)
                bra     .method_set_bones

.sprite_0:      cmp.w   #ENEMY_STATUS_BONES,THIS_SPRITE_STATUS(a2)
                beq     .method_animate_bones
		
                move.w  d1,PRIVATE_GRIFFIN_LAST_ANIM(a2)

; Check boundary of sprite!
                moveq   #0,d2
                move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                lea     BOUNDARY_X1POS(a4),a0
		cmp.l	(a0),d2
		blt.s	.dectimer
		cmp.l	4(a0),d2
		bgt.s	.dectimer
		bra.s	.inbound
			
.dectimer:
                subq.w  #1,PRIVATE_GRIFFIN_BOUNDARY_TIMER(a2)
                bpl.s   .outbound
                bra     .method_set_bones

.inbound:               move.w  #ENEMY_BOUNDARY_TIME,PRIVATE_GRIFFIN_BOUNDARY_TIMER(a2)
.outbound:
                bra.s   .sprite_1

.animate_table: dc.l    .method_destroy_sprite-.animate_table   ; 0
                dc.l    .method_animate_bones-.animate_table    ; 1
                dc.l    .method_animate_sweep_left-.animate_table       ; 2
                dc.l    .method_animate_sweep_right-.animate_table      ; 3
		dc.l    .method_animate_none-.animate_table      	; 4
		dc.l    .method_animate_none-.animate_table      	; 5
                dc.l    .method_animate_fly_left-.animate_table ; 6
                dc.l    .method_animate_fly_right-.animate_table        ; 7
                dc.l    .method_animate_swoop_left-.animate_table       ; 8
                dc.l    .method_animate_swoop_right-.animate_table      ; 9
                dc.l    .method_animate_none-.animate_table      	; 10
                dc.l    .method_animate_none-.animate_table      	; 11
                dc.l    .method_animate_none-.animate_table      	; 12
                dc.l    .method_animate_none-.animate_table      	; 13
                dc.l    .method_animate_none-.animate_table      	; 14
                dc.l    .method_animate_none-.animate_table      	; 15
                dc.l    .method_animate_none-.animate_table      	; 16
                dc.l    .method_animate_none-.animate_table      	; 17
                dc.l    .method_animate_none-.animate_table      	; 18
                dc.l    .method_animate_none-.animate_table      	; 19
                dc.l    .method_animate_none-.animate_table      	; 20
                dc.l    .method_animate_none-.animate_table      	; 21
                dc.l    .method_animate_none-.animate_table      	; 22
                dc.l    .method_animate_none-.animate_table      	; 23
                dc.l    .method_animate_none-.animate_table      	; 24

.method_animate_none:	bra	.exit

.sprite_1:	
		cmp.w   #ENEMY_STATUS_SWEEP_RIGHT,THIS_SPRITE_STATUS(a2)                ; if doing bones or sweeps then skip all collisions
                ble     .run_sprite_method
		
		tst.w	SHIELD_TIMER
		bpl.s	.shield_on
                cmp.w   #ENEMY_STATUS_STUN_RIGHT,THIS_SPRITE_STATUS(a2)                 ; if doing stuns then skip Rygar collision
                ble     .sprite_stunned

.shield_on:

                tst.w   THIS_SPRITE_HWSLOT(a2)                  ; Do not do hardware Sprites
                bpl.s   .fly

                tst.w   PRIVATE_GRIFFIN_SWOOP_INDEX(a2)                 ; Swooping?
                bpl.s   .swoop                                  ; Yes?
; No....
.fly:
                lea     GRIFFIN_YSINE(a4),a3
.sine_y_loop:   moveq   #0,d3
                move.w  PRIVATE_GRIFFIN_SINE_INDEX(a2),d3
                move.w  (a3,d3*2),d2
                cmp.w   #$8000,d2
                bne.s   .sine_y_apply
                clr.w   PRIVATE_GRIFFIN_SINE_INDEX(a2)
                bra.s   .sine_y_loop

.sine_y_apply:
                addq.w  #1,PRIVATE_GRIFFIN_SINE_INDEX(a2)
                add.w   PRIVATE_GRIFFIN_FIXED_YPOS(a2),d2
                move.w  d2,SPRITE_PLOT_YPOS(a1)
                move.w  d2,PRIVATE_GRIFFIN_YPOS(a2)
                bra     .sprite_2


.swoop:
                lea     GRIFFIN_SWOOP_SINE(a4),a3
.swoop_y_loop:  moveq   #0,d3
                move.w  PRIVATE_GRIFFIN_SWOOP_INDEX(a2),d3
                move.w  (a3,d3*2),d2
                cmp.w   #$8000,d2
                bne.s   .swoop_y_apply
                move.w  #-1,PRIVATE_GRIFFIN_SWOOP_INDEX(a2)
                bra     .exit

.swoop_y_apply:
                addq.w  #1,PRIVATE_GRIFFIN_SWOOP_INDEX(a2)
                add.w   PRIVATE_GRIFFIN_FIXED_YPOS(a2),d2
                move.w  d2,SPRITE_PLOT_YPOS(a1)
                move.w  d2,PRIVATE_GRIFFIN_YPOS(a2)
                bra     .sprite_2

                nop

; Test this enemy collided with Rygar (the player)
.sprite_2:      
;---- Test sprites ----	
		IFNE	GRIFFIN_SPRITE_TEST
		move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                move.w  d2,PRIVATE_GRIFFIN_XPOS(a2)
                moveq	#GRIFFIN_STATUS_FLY_LEFT,d1
		bra	ANIMATE
		ENDC
.test:		
;---- Test sprites ----


		move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4
                move.w  d3,d5
                add.w   #GRIFFIN_TO_RYGAR_X1_BOUND,d2
                add.w   #GRIFFIN_TO_RYGAR_Y1_BOUND,d3
                add.w   #GRIFFIN_TO_RYGAR_X2_BOUND,d4
                add.w   #GRIFFIN_TO_RYGAR_Y2_BOUND,d5
                lea     RYGAR_COORDS(a4),a3
                ENEMY_TO_RYGAR_COLLISION
                move.l  #POINTS_GRIFFIN,d2
                tst.w   d6                                      ; Did this sprite collide with Rygar?
                IFEQ    DISABLE_ENEMY_COLLISION
                bmi     .method_set_collision_action            ; Yes
                ENDC

.sprite_stunned:
; Check if the enemy has been hit with disk armor
                move.w  SPRITE_PLOT_XPOS(a1),d2
                move.w  SPRITE_PLOT_YPOS(a1),d3
                move.w  d2,d4
                move.w  d3,d5
                add.w   #GRIFFIN_TO_DISKARM_X1_BOUND,d2
                add.w   #GRIFFIN_TO_DISKARM_Y1_BOUND,d3
                add.w   #GRIFFIN_TO_DISKARM_X2_BOUND,d4
                add.w   #GRIFFIN_TO_DISKARM_Y2_BOUND,d5
                lea     DISKARM_COORDS(a4),a3
                ENEMY_TO_DISKARM_COLLISION
                move.l  #POINTS_GRIFFIN,d2
                tst.w   d6                                      ; Did this sprite get hit with the disk armor
                bmi     .method_set_sweep_direction             ; Yes


; Test this enemy collided with the disk armour
                RUN_SPRITE_METHOD

                METHOD_ANIMATE_BONES

                METHOD_ANIMATE_SWEEP_LEFT

                METHOD_ANIMATE_SWEEP_RIGHT

                METHOD_SET_BONES

                METHOD_SET_SWEEP_DIRECTION


; d2=xpos to plot sprite
;
.method_animate_fly_left:
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                sub.w   PRIVATE_GRIFFIN_SPEED(a2),d6
		move.w	d6,d2
                UPDATE_SPRITE_PLOT_XPOS
                move.w  d2,PRIVATE_GRIFFIN_XPOS(a2)
		
; Check to see if we can throw a rock
		tst.w	PRIVATE_GRIFFIN_ROCK_ENABLE(a2)		; Rock throwing enabled?  
		beq.s	.rock_dropping
		
		tst.w	ROCK_IN_FLIGHT(a4)			; Is there already a Rock?
		bmi.s	.rock_dropping
	
		move.w	FRAME_BG(a4),d5				; Throw a Rock every 32 frames
		and.w	#$1f,d5
		bne.s	.rock_dropping
		move.w	#-1,ROCK_IN_FLIGHT(a4)
		
		bsr	.method_throw_rock

.rock_dropping:
                cmp.w   #32,SPRITE_PLOT_XPOS(a1)
                ble     .method_set_fly_right

                tst.w   THIS_SPRITE_HWSLOT(a2)                  ; Do not do hardware Sprites
                bpl.s   .method_animate_fly_left_1              ; Avoid hardware sprites
                cmp.w   #GRIFFIN_ATTACK_RIGHT_XPOS,d2
                beq     .method_set_swoop_left
.method_animate_fly_left_1:
                moveq   #GRIFFIN_STATUS_FLY_LEFT,d1
                move.w  d1,PRIVATE_GRIFFIN_ANIM_FRAME(a2)
                bra     ANIMATE
		
.method_throw_rock:
		movem.l d0-d7/a0-a3,-(a7)
                moveq   #SPR_ROCK,d0
		move.w  THIS_SPRITE_MAP_XPOS(a2),d1
                lsr.w   #4,d1
                move.w  SPRITE_PLOT_YPOS(a1),d2
		add.w	#16,d2
                moveq   #SPR_TYPE_ENEMY_1X16,d3
                lea     HDL_ROCK(pc),a0
                bsr     PUSH_SPRITE
                movem.l (a7)+,d0-d7/a0-a3
		rts

.method_animate_fly_right:
                move.w  THIS_SPRITE_MAP_XPOS(a2),d6
                add.w   PRIVATE_GRIFFIN_SPEED(a2),d6
		move.w	d6,d2
                UPDATE_SPRITE_PLOT_XPOS
                move.w  d2,PRIVATE_GRIFFIN_XPOS(a2)
                tst.w   THIS_SPRITE_HWSLOT(a2)                  ; Do not do hardware Sprites
                bpl.s   .method_animate_fly_right_1             ; Avoid hardware sprites
                cmp.w   #GRIFFIN_ATTACK_LEFT_XPOS,d2
                beq     .method_set_swoop_right
.method_animate_fly_right_1:
                moveq   #GRIFFIN_STATUS_FLY_RIGHT,d1
                move.w  d1,PRIVATE_GRIFFIN_ANIM_FRAME(a2)
                bra     ANIMATE

.method_animate_swoop_left:
                move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                sub.w   PRIVATE_GRIFFIN_SPEED(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                move.w  d2,PRIVATE_GRIFFIN_XPOS(a2)
                moveq   #GRIFFIN_STATUS_SWOOP_LEFT,d1
                move.w  d1,PRIVATE_GRIFFIN_ANIM_FRAME(a2)
                bra     ANIMATE


.method_animate_swoop_right:
                move.w  THIS_SPRITE_MAP_XPOS(a2),d2
                add.w   PRIVATE_GRIFFIN_SPEED(a2),d2
                UPDATE_SPRITE_PLOT_XPOS
                move.w  d2,PRIVATE_GRIFFIN_XPOS(a2)
                moveq   #GRIFFIN_STATUS_SWOOP_RIGHT,d1
                move.w  d1,PRIVATE_GRIFFIN_ANIM_FRAME(a2)
                bra     ANIMATE

.method_destroy_sprite:
                subq.w  #1,GRIFFINS_IN_USE(a4)
                clr.w   THIS_SPRITE_STATUS(a2)
                IFNE    GRIFFIN_HWSPR_ENABLE
                IFNE    GRIFFIN_HWSPR_UPPER
                lea     ENEMY_HWSPR_UPPER_ALLOCATED(a4),a0                      ; Free up hardware sprite
                ENDC
                IFNE    GRIFFIN_HWSPR_MID
                lea     ENEMY_HWSPR_MID_ALLOCATED(a4),a0                        ; Free up hardware sprite
                ENDC
                IFNE    GRIFFIN_HWSPR_LOWER
                lea     ENEMY_HWSPR_LOWER_ALLOCATED(a4),a0                      ; Free up hardware sprite
                ENDC
                move.w  THIS_SPRITE_HWSLOT(a2),d7
                bmi     DESTROY_SPRITE
                FREE_HARDWARE_SPRITE
                ENDC
                bra     DESTROY_SPRITE

.method_set_destroy_sprite:
                move.w  #ENEMY_STATUS_DESTROYED,THIS_SPRITE_STATUS(a2)
                bra     .set_update


.method_set_fly_left:
                move.w  #GRIFFIN_GO_LEFT,PRIVATE_GRIFFIN_DIRECTION(a2)
                move.w  #GRIFFIN_STATUS_FLY_LEFT,THIS_SPRITE_STATUS(a2)
		moveq	#GRIFFIN_STATUS_FLY_LEFT,d1
		bra	ANIMATE
                ;bra     .set_update

.method_set_fly_right:
                move.w  #GRIFFIN_GO_RIGHT,PRIVATE_GRIFFIN_DIRECTION(a2)
                move.w  #GRIFFIN_STATUS_FLY_RIGHT,THIS_SPRITE_STATUS(a2)
		moveq	#GRIFFIN_STATUS_FLY_RIGHT,d1
		bra	ANIMATE
                ;bra     .set_update

.method_set_swoop_left:
                clr.w   PRIVATE_GRIFFIN_SWOOP_INDEX(a2)
                move.w  #GRIFFIN_GO_LEFT,PRIVATE_GRIFFIN_DIRECTION(a2)
                move.w  #GRIFFIN_STATUS_SWOOP_LEFT,THIS_SPRITE_STATUS(a2)
		moveq	#GRIFFIN_STATUS_SWOOP_LEFT,d1
		move.l	d0,-(a7)
		moveq	#SND_SQUIRREL,d0
		bsr	PLAY_SAMPLE
		move.l	(a7)+,d0
		bra	ANIMATE
                ;bra     .set_update

.method_set_swoop_right:
                clr.w   PRIVATE_GRIFFIN_SWOOP_INDEX(a2)
                move.w  #GRIFFIN_GO_RIGHT,PRIVATE_GRIFFIN_DIRECTION(a2)
                move.w  #GRIFFIN_STATUS_SWOOP_RIGHT,THIS_SPRITE_STATUS(a2)
		moveq	#GRIFFIN_STATUS_SWOOP_RIGHT,d1
		move.l	d0,-(a7)
		moveq	#SND_SQUIRREL,d0
		bsr	PLAY_SAMPLE
		move.l	(a7)+,d0
		bra	ANIMATE
                ;bra     .set_update


.method_set_collision_action:
                tst.w   RYGAR_FALLPREV_STATE1(a4)                                ; Is Rygar falling on enemy
                bmi.s   .method_set_jump_or_destroy                     ; Yes!

                IFNE    DISABLE_ENEMY_COLLISION
                bra     .exit
                ELSE
                bra.s   .method_set_rygar_death_sequence
                ENDC

                METHOD_SET_JUMP_OR_DESTROY

                METHOD_SET_RYGAR_DEATH_SEQUENCE

.set_update:

.exit:          rts


