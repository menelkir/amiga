
CROWD_STATUS_RYGAR_ASC:         equ     6
CROWD_STATUS_RYGAR_DESC:        equ     7

CROWD_STATUS_LEFT_ASC:		equ	8
CROWD_STATUS_LEFT_DESC:		equ	9

CROWD_STATUS_RIGHT_ASC:		equ	10
CROWD_STATUS_RIGHT_DESC:	equ	11

CROWD_LEFT_XPOS:		equ	(320/2)-32
CROWD_RIGHT_XPOS:		equ	(320/2)
CROWD_MIDDLE_XPOS:		equ	(320/2)-16

PRIVATE_CROWD_SINE_POINTER:	equ	32
PRIVATE_CROWD_FIXED_YPOS:    	equ     34
PRIVATE_CROWD_TIMER:		equ	36

; If this sprite parameter = 0 then it's left sprite, 1= right sprite, 2=middle sprite. 

HDL_CROWD:
		FUNCID	#$e80b4ff3
                SPRITE_INITIATE

; Set the initial variables and status of the sprite.
.construct:
                move.w  SPRITE_PLOT_XPOS(a1),d2
                lsl.w   #4,d2
                move.w  d2,THIS_SPRITE_MAP_XPOS(a2)
                move.w  #146,PRIVATE_CROWD_FIXED_YPOS(a2)
                move.w  #146,SPRITE_PLOT_YPOS(a2)    
		clr.w	PRIVATE_CROWD_SINE_POINTER(a2)
		
		clr.w   THIS_SPRITE_SWEEP_INDEX(a2)
	
.active:        
		moveq	#0,d2
		
		cmp.w	#0,THIS_SPRITE_PARAMETER(a2)
		beq	.method_animate_crowd_left
		cmp.w	#1,THIS_SPRITE_PARAMETER(a2)
		beq	.method_animate_crowd_right
		cmp.w	#2,THIS_SPRITE_PARAMETER(a2)
		beq	.method_animate_rygar
				
.method_animate_none: 	bra	.exit
		
.method_destroy_sprite:
                clr.w   THIS_SPRITE_STATUS(a2)
                bra     DESTROY_SPRITE

.method_animate_rygar:
		move.w	#CROWD_MIDDLE_XPOS,SPRITE_PLOT_XPOS(a1)

                lea     LIGAR_SINE(a4),a0
.sine_loop:
                move.w  PRIVATE_LIGAR1_SINE_POINTER(a2),d2
		move.l  #146-24,d3
		move.b  (a0,d2),d4
		cmp.b	#$80,d4
		bne.s	.sine			; Set face left long pause 
		clr.w	PRIVATE_LIGAR1_SINE_POINTER(a2)
		bra.s	.sine_loop
.sine:
		add.b	d4,d3
                move.w  d3,SPRITE_PLOT_YPOS(a1)

                addq.w  #2,PRIVATE_LIGAR1_SINE_POINTER(a2)
		
		cmp.w	#0,PRIVATE_LIGAR1_SINE_POINTER(a2)
		beq.s	.set_descend
		cmp.w	#8,PRIVATE_LIGAR1_SINE_POINTER(a2)
		beq.s	.set_ascend
		cmp.w	#128-8,PRIVATE_LIGAR1_SINE_POINTER(a2)
		beq.s	.set_descend		

.set:
		moveq	#CROWD_STATUS_RYGAR_ASC,d1
		cmp.w	#63,PRIVATE_LIGAR1_SINE_POINTER(a2)
		ble.s	.anim
		moveq	#CROWD_STATUS_RYGAR_DESC,d1		
.anim:		bra	ANIMATE
		
	
.set_ascend:
		move.w	#CROWD_STATUS_LEFT_ASC,CROWD_LEFT_ANIM(a4)
		move.w	#CROWD_STATUS_RIGHT_ASC,CROWD_RIGHT_ANIM(a4)
		move.l	d0,-(a7)
		moveq	#SND_RYGARWIN,d0
		bsr	PLAY_SAMPLE_CHANNEL_4
		move.l	(a7)+,d0
		bra.s	.set

.set_descend:
		move.w	#CROWD_STATUS_LEFT_DESC,CROWD_LEFT_ANIM(a4)
		move.w	#CROWD_STATUS_RIGHT_DESC,CROWD_RIGHT_ANIM(a4)
		bra.s	.set		
		
		
.method_animate_crowd_left:
		move.w	#CROWD_LEFT_XPOS,SPRITE_PLOT_XPOS(a1)
		move.w	#146,SPRITE_PLOT_YPOS(a1)
		move.w	CROWD_LEFT_ANIM(a4),d1
		bra	ANIMATE

.method_animate_crowd_right:
		move.w	#CROWD_RIGHT_XPOS,SPRITE_PLOT_XPOS(a1)
		move.w	#146,SPRITE_PLOT_YPOS(a1)
		move.w	CROWD_RIGHT_ANIM(a4),d1
		bra	ANIMATE

.set_update:
.exit:          rts
