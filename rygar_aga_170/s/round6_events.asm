
ROUND6_EVENT_INJECT_VILLAGERS_1:
	nop
	FUNCID	#$59bb3cd7
	
	rept	2
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#SPR_VILLAGER,d0
	move.w	#$4a0,d1
	move.w	#80,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	lea	HDL_VILLAGER(pc),a0
	bsr	PUSH_SPRITE
	movem.l	(a7)+,d0-d7/a0-a3
	
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#SPR_VILLAGER,d0
	move.w	#$4e0,d1
	move.w	#80,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	lea	HDL_VILLAGER(pc),a0
	bsr	PUSH_SPRITE
	movem.l	(a7)+,d0-d7/a0-a3
	
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#SPR_VILLAGER,d0
	move.w	#$510,d1
	move.w	#80,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	lea	HDL_VILLAGER(pc),a0
	bsr	PUSH_SPRITE
	movem.l	(a7)+,d0-d7/a0-a3
	
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#SPR_VILLAGER,d0
	move.w	#$530,d1
	move.w	#80,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	lea	HDL_VILLAGER(pc),a0
	bsr	PUSH_SPRITE
	movem.l	(a7)+,d0-d7/a0-a3
	endr
	rts
	

	
ROUND6_EVENT_INJECT_VILLAGERS_2:
	nop
	FUNCID	#$63df540f
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#SPR_VILLAGER,d0
	move.w	#$4e0,d1
	move.w	#80,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	lea	HDL_VILLAGER(pc),a0
	bsr	PUSH_SPRITE
	movem.l	(a7)+,d0-d7/a0-a3
	
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#SPR_VILLAGER,d0
	move.w	#$510,d1
	move.w	#80,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	lea	HDL_VILLAGER(pc),a0
	bsr	PUSH_SPRITE
	movem.l	(a7)+,d0-d7/a0-a3
	
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#SPR_VILLAGER,d0
	move.w	#$530,d1
	move.w	#80,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	lea	HDL_VILLAGER(pc),a0
	bsr	PUSH_SPRITE
	movem.l	(a7)+,d0-d7/a0-a3
	
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#SPR_VILLAGER,d0
	move.w	#$590,d1
	move.w	#80,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	lea	HDL_VILLAGER(pc),a0
	bsr	PUSH_SPRITE
	movem.l	(a7)+,d0-d7/a0-a3
	
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#SPR_VILLAGER,d0
	move.w	#$5b0,d1
	move.w	#80,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	lea	HDL_VILLAGER(pc),a0
	bsr	PUSH_SPRITE
	movem.l	(a7)+,d0-d7/a0-a3
	
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#SPR_VILLAGER,d0
	move.w	#$5d0,d1
	move.w	#80,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	lea	HDL_VILLAGER(pc),a0
	bsr	PUSH_SPRITE
	movem.l	(a7)+,d0-d7/a0-a3
	
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#SPR_VILLAGER,d0
	move.w	#$5f0,d1
	move.w	#80,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	lea	HDL_VILLAGER(pc),a0
	bsr	PUSH_SPRITE
	movem.l	(a7)+,d0-d7/a0-a3
	
	rts
	
	
ROUND6_EVENT_INJECT_VILLAGERS_3:
	nop
	FUNCID	#$bf2af44b
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#SPR_VILLAGER,d0
	move.w	#$5d0,d1
	move.w	#80,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	lea	HDL_VILLAGER(pc),a0
	bsr	PUSH_SPRITE
	movem.l	(a7)+,d0-d7/a0-a3
	
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#SPR_VILLAGER,d0
	move.w	#$5f0,d1
	move.w	#80,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	lea	HDL_VILLAGER(pc),a0
	bsr	PUSH_SPRITE
	movem.l	(a7)+,d0-d7/a0-a3
	
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#SPR_VILLAGER,d0
	move.w	#$640,d1
	move.w	#80,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	lea	HDL_VILLAGER(pc),a0
	bsr	PUSH_SPRITE
	movem.l	(a7)+,d0-d7/a0-a3
	
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#SPR_VILLAGER,d0
	move.w	#$560,d1
	move.w	#80,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	lea	HDL_VILLAGER(pc),a0
	bsr	PUSH_SPRITE
	movem.l	(a7)+,d0-d7/a0-a3
	
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#SPR_VILLAGER,d0
	move.w	#$6b0,d1
	move.w	#80,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	lea	HDL_VILLAGER(pc),a0
	bsr	PUSH_SPRITE
	movem.l	(a7)+,d0-d7/a0-a3
	
	rts
	
	
ROUND6_EVENT_INJECT_VILLAGERS_4:
	nop
	FUNCID	#$5c283535
	rept	2
	
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#SPR_VILLAGER,d0
	move.w	#$6d0,d1
	move.w	#80,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	lea	HDL_VILLAGER(pc),a0
	bsr	PUSH_SPRITE
	movem.l	(a7)+,d0-d7/a0-a3
	
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#SPR_VILLAGER,d0
	move.w	#$7a0,d1
	move.w	#80,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	lea	HDL_VILLAGER(pc),a0
	bsr	PUSH_SPRITE
	movem.l	(a7)+,d0-d7/a0-a3
	
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#SPR_VILLAGER,d0
	move.w	#$7e0,d1
	move.w	#80,d2
	moveq	#SPR_TYPE_ENEMY_2X32,d3
	lea	HDL_VILLAGER(pc),a0
	bsr	PUSH_SPRITE
	movem.l	(a7)+,d0-d7/a0-a3
	endr
	rts