
UPPER_PANEL_SIZE:       equ     24
UPPER_PANEL_PLANES:     equ     5
LOWER_PANEL_SIZE:       equ     16
LOWER_PANEL_PLANES:     equ     5

LOWER_RANK_PIXELS_X:	equ	2512

MAX_RANK:               equ     29
UPGRADE_RANK_EVERY:     equ     50      ; frames

PLAYER1:        equ     0
PLAYER2:        equ     1

INIT_PANELS:
	FUNCID	#$3121bde1
	
        moveq   #UPPER_PANEL_SIZE,d7
        move.l  MEMCHK9_UPPER_PANEL(a4),a0
        bsr     CLEAR_PANEL
        moveq   #LOWER_PANEL_SIZE,d7
        move.l  MEMCHK9_LOWER_PANEL(a4),a0
        bsr     CLEAR_PANEL

; Get panel palette
	move.l	RANKS_MAIN(a4),a0
        lea     UPPER_PANEL_PAL(a4),a1
        moveq   #32-1,d7
        bsr     GET_PALETTE_FROM_IFF

        move.l  LOWER_PANEL_ASSETS(a4),a0
        lea     LOWER_PANEL_PAL(a4),a1
        moveq   #32-1,d7
        bsr     GET_PALETTE_FROM_IFF

        moveq   #PLAYER1,d0
        moveq   #0,d1
	bsr     BLIT_UPPER_PANEL_RANK
        moveq   #PLAYER1,d0
        moveq   #0,d1
	move.w	RYGAR_CURRENT_RANK(a4),d1
	bsr     BLIT_UPPER_PANEL_RANK

        moveq   #PLAYER2,d0
        moveq   #0,d1
        bsr     BLIT_UPPER_PANEL_RANK

; Draw Hi Score Text
        moveq   #16,d0
        moveq   #0,d1
        moveq   #0,d2
        moveq   #4,d6
        lea     TXT_HISCORE_TEXT(a4),a0
        bsr     DRAW_UPPER_TEXT

; Draw player 1
        moveq   #4,d0
        moveq   #1,d1
        moveq   #0,d2
        moveq   #1,d6
        lea     TXT_PLAYER1_TEXT(a4),a0
        bsr     DRAW_UPPER_TEXT

; Draw player 2
        moveq   #26,d0
        moveq   #1,d1
        moveq   #0,d2
        moveq   #2,d6
        lea     TXT_EXTRA_TEXT(a4),a0
        bsr     DRAW_UPPER_TEXT

; Draw Hi-Score Score
        moveq   #16,d0
        moveq   #1,d1
        moveq   #0,d2
        moveq   #3,d6
        lea     TXT_HISCORE_SCORE(a4),a0
        bsr     DRAW_UPPER_TEXT

; Draw Player 1 score
        bsr     UPDATE_PLAYER_SCORE
; Draw Extra score
	bsr	UPDATE_EXTRA_AMOUNT

; Draw time
        moveq   #16,d0
        moveq   #2,d1
        moveq   #1,d6
        bsr     DRAW_TIME

; Draw timer
        moveq   #18,d0
        moveq   #2,d1
        moveq   #1,d2
        moveq   #2,d6
        lea     TXT_TIMER_START(a4),a0
        bsr     DRAW_UPPER_TEXT

; Blit Lives
        move.w  RYGAR_LIVES(a4),d0
        bsr     BLIT_RYGAR_LIVES

;Powers

        moveq   #0,d0
        move.w  RYGAR_CURRENT_POWERS(a4),d2
        and.w   #POWER_STAR,d2
        beq.s   .star_power
        moveq   #1,d0
.star_power:
        moveq   #0,d1
        bsr     BLIT_RYGAR_POWER

        moveq   #0,d0
        move.w  RYGAR_CURRENT_POWERS(a4),d2
        and.w   #POWER_CROWN,d2
        beq.s   .crown_power
        moveq   #1,d0
.crown_power:
        moveq   #1,d1
        bsr     BLIT_RYGAR_POWER

        moveq   #0,d0
        move.w  RYGAR_CURRENT_POWERS(a4),d2
        and.w   #POWER_TIGER,d2
        beq.s   .tiger_power
        moveq   #1,d0
.tiger_power:
        moveq   #2,d1
        bsr     BLIT_RYGAR_POWER

        moveq   #0,d0
        move.w  RYGAR_CURRENT_POWERS(a4),d2
        and.w   #POWER_CROSS,d2
        beq.s   .cross_power
        moveq   #1,d0
.cross_power:
        moveq   #3,d1
        bsr     BLIT_RYGAR_POWER

        moveq   #0,d0
        move.w  RYGAR_CURRENT_POWERS(a4),d2
        and.w   #POWER_SUN,d2
        beq.s   .sun_power
        moveq   #1,d0
.sun_power:
        moveq   #4,d1
        bsr     BLIT_RYGAR_POWER

        moveq   #0,d0
        move.w  ROUND_CURRENT(a4),d0
        bsr     BLIT_RYGAR_ROUND
        rts


UPDATE_EXTRA_AMOUNT:
	movem.l	d0-d7/a0-a3,-(a7)
	moveq	#0,d0
	move.w	EXTRAS_POSITION(a4),d0
	lea	TEXT_EXTRAS_TABLE(a4),a0
	move.l	(a0,d0*4),a0
        moveq   #26,d0
        moveq   #2,d1
        moveq   #0,d2
        moveq   #3,d6
        bsr     DRAW_UPPER_TEXT
	movem.l	(a7)+,d0-d7/a0-a3
	rts
	
; Draw Player score
UPS
UPDATE_PLAYER_SCORE:
	FUNCID	#$d4b47fba
	
	tst.w	ROUND_TRANSITION(a4)
	bmi.s	.drop
	tst.w	DISABLE_ENEMIES(a4)
	bmi.s	.drop
	
.check_weapon_drop:
	lea	WEAPON_DROP_COUNTS(a4),a0
	moveq	#0,d0
	addq.w	#1,ENEMY_KILL_COUNT(a4)
	move.w	ENEMY_KILL_COUNT(a4),d0
	moveq	#0,d1	
	move.w	ROUND_CURRENT(a4),d1
	
	cmp.w	#KILLS_TO_WEAPON_DROP,d0
	bne.s	.check_rank_increase			; required to drop a weapon.
	clr.w	ENEMY_KILL_COUNT(a4)
	bsr	DROP_WEAPON				

.check_rank_increase:
	lea	RANK_INCREASE_COUNTS(a4),a0
	moveq	#0,d0
	addq.w	#1,RANK_INCREASE_COUNT(a4)
	move.w	RANK_INCREASE_COUNT(a4),d0
	moveq	#0,d1
	move.w	RYGAR_CURRENT_RANK(a4),d1
	cmp.w	(a0,d1*2),d0
	bne.s	.drop
	clr.w	RANK_INCREASE_COUNT(a4)
	addq.w	#1,RYGAR_CURRENT_RANK(a4)
	bsr	INCREASE_RANK

.drop:	

        move.l  VAL_PLAYER1_SCORE(a4),d0
	lea	EXTRAS(a4),a1
	moveq	#0,d1
	move.w	EXTRAS_POSITION(a4),d1
	move.l	(a1,d1*4),d1
	
	cmp.l	d0,d1
	bgt.s	.no_extra
	cmp.w	#3,EXTRAS_POSITION(a4)
	beq.s	.no_extra
	addq.w	#1,EXTRAS_POSITION(a4)
	bsr	ADD_EXTRA_LIFE
	
	bsr	UPDATE_EXTRA_AMOUNT
.no_extra:
        move.l  VAL_PLAYER1_SCORE(a4),d0
        bsr     Binary2Decimal
        lea     TXT_PLAYER1_SCORE(a4),a1
        moveq   #8-1,d6					; d6 7
        bsr     COPY_DECIMAL
        moveq   #4,d0
        moveq   #2,d1
        moveq   #0,d2
        moveq   #3,d6
        lea     TXT_PLAYER1_SCORE(a4),a0
        bsr     DRAW_UPPER_TEXT
.exit:	rts


;d0=x position
;d1=y position
;d6=bitplane (1-4)
DRAW_TIME:
	FUNCID	#$7711705f
	
        move.l  MEMCHK9_UPPER_PANEL(a4),a1
        add.l   d0,a1                                   ; add x position
        lsl.w   #3,d1                                   ; multiply y by 8
        lea     CANVAS_TABLE_Y3(a4),a2
        add.l   (a2,d1*4),a1                            ; add y position

        moveq   #0,d0
        moveq   #2-1,d7                                 ; 4 chars

        mulu    #PLAYFIELD_SIZE_X,d6
        add.l   d6,a1

        move.l  FONT_ASSET(a4),a2
        add.w   #86,a2                  ; Offset to BODY

        add.w   #(512/8)+12,a2

.loop:
        move.b  (a2),(a1)
        move.b  FONT_WIDTH*1(a2),(PLAYFIELD_SIZE_X*5)*1(a1)
        move.b  FONT_WIDTH*2(a2),(PLAYFIELD_SIZE_X*5)*2(a1)
        move.b  FONT_WIDTH*3(a2),(PLAYFIELD_SIZE_X*5)*3(a1)
        move.b  FONT_WIDTH*4(a2),(PLAYFIELD_SIZE_X*5)*4(a1)
        move.b  FONT_WIDTH*5(a2),(PLAYFIELD_SIZE_X*5)*5(a1)
        move.b  FONT_WIDTH*6(a2),(PLAYFIELD_SIZE_X*5)*6(a1)
        move.b  FONT_WIDTH*7(a2),(PLAYFIELD_SIZE_X*5)*7(a1)
        addq.w  #1,a2
        addq.w  #1,a1
        dbf     d7,.loop
.exit:  rts


; Update remaining time (called every frame)
UPDATE_TIMER:
	FUNCID	#$c2a95675
	
	tst.w	LIGAR_FIGHT(a4)
	bmi	.exit
	
        tst.w   DISABLE_ENEMIES(a4)
        bmi     .exit
	moveq	#0,d0
        move.w  TIME_REMAINING(a4),d0
        beq   	.evil_comes
        bmi   	.exit
	
	clr.w	TIME_LOW(a4)
	cmp.w	#64*9,d0
	bgt.s	.low_time
	move.w	#-1,TIME_LOW(a4)
	addq.w	#1,TIME_OVER(a4)

.low_time:
	
	cmp.w	#(64*4)-21,d0
	bne.s	.last
; Darkness comes tune 5 seconds before fade.

	tst.w	HISCORE_UPDATE_TIMER(a4)
	bmi.s	.last

	move.l	d0,-(a7)
	moveq	#MUSIC_TIMER,d0
	bsr	PLAY_TUNE
	move.l	(a7)+,d0
	
.last:  subq.w  #1,TIME_REMAINING(a4)
	clr.w	ENEMY_SPEED_ACCUMULATOR(a4)
	
	move.w	d0,d1
	and.w	#%1111111111,d1			; increase sprite speed every 256 frames
	bne.s	.speed
	
	tst.w	TIME_REMAINING(a4)
	bmi.s	.speed
	
	move.w	#1,ENEMY_SPEED_ACCUMULATOR(a4)
	addq.w	#1,ENEMY_SPEED_MODIFIER(a4)
.speed:		

        move.w  d0,d1
        and.w   #%0000000000111111,d1
        tst.w   d1
        beq.s   .draw_sec
        bra.s   .draw_msec

.draw_sec:
        lea     TXT_TIMER_SEC(a4),a0
        lsr.w   #4,d0                           ; Seconds value
        add.l   d0,a0

        moveq   #18,d0
        moveq   #2,d1
        moveq   #1,d2
        moveq   #2,d6
        bsr     DRAW_UPPER_TEXT
	bra.s	.exit


.draw_msec:
	moveq	#0,d0
        move.w  d1,d0
        lea     TXT_TIMER_MSEC(a4),a0
        and.w   #$ff,d0
        add.w   d0,d0
        add.w   d0,d0
        add.l   d0,a0

        moveq   #22,d0
        moveq   #2,d1
        moveq   #1,d2
        moveq   #2,d6
	;clr.w	$100.w
        bsr     DRAW_UPPER_TEXT
        bra.s   .exit
	
	
.evil_comes:
	tst.w	HISCORE_UPDATE_TIMER(a4)
	bmi.s	.exit
	
	moveq	#MUSIC_EVIL,d0
	bsr	PLAY_TUNE
        move.w  #15,BACKGROUND_TRANSITION_FRAMES(a4)
        bra   	.last

.exit:  ;
        rts





;d0=pixel offset (0-79)
;d3=round number
UPDATE_ROUND_GUIDE:
		FUNCID	#$b6249312
		
		moveq	#0,d0
                moveq   #0,d3
                move.w  ROUND_CURRENT(a4),d3
		move.w  MAP_PIXEL_POSX(a4),d0
                sub.w   #256,d0
                lsr.w   #5,d0

                addq.w  #8,d0
                lea     ROUND_GUIDE_ASSET(a4),a0                ; Asset

                move.l  MEMCHK9_LOWER_PANEL(a4),a1
                add.w   #24,a1                                  ; x offset
                add.w   #(PLAYFIELD_SIZE_X*5)*8,a1              ; y offset

                move.w  d0,d1
                move.w  d0,d2
                and.w   #$7,d1
                and.l   #$fff8,d2
                lsr.w   #3,d2
                add.l   d2,a1


                lea     LOWER_PANEL_ASSETS+$b0(a4),a2

                move.l  LOWER_PANEL_ASSETS(a4),a2
                add.w   #$B0,a2

                add.w   #(LOWER_RANK_PIXELS_X/8*5)*8,a2

                add.w   #112/8,a2
                subq.w  #1,a2
                add.l   d2,a2


                add.w   d3,d3                   ; d0=2
                move.w  d3,d4                   ; d1=2
                add.w   d3,d3                   ; d0=4
                add.w   d3,d3                   ; d0=8
                add.w   d4,d3                   ; d0=10
                add.l   d3,a2

                rept    7
                move.w  (a0)+,d0
                lsr.w   d1,d0

                move.w  (a2),d2
                or.w    d0,d2
                move.w  d2,(a1)

                move.b  (LOWER_RANK_PIXELS_X/8)(a2),d2
                lsl.w   #8,d2
                move.b  (LOWER_RANK_PIXELS_X/8)+1(a2),d2
                or.w    d0,d2
                move.w  d2,PLAYFIELD_SIZE_X(a1)

                move.b  (LOWER_RANK_PIXELS_X/8)*2(a2),d2
                lsl.w   #8,d2
                move.b  (LOWER_RANK_PIXELS_X/8)*2+1(a2),d2
                or.w    d0,d2
                move.w  d2,PLAYFIELD_SIZE_X*2(a1)

                move.b  (LOWER_RANK_PIXELS_X/8)*3(a2),d2
                lsl.w   #8,d2
                move.b  (LOWER_RANK_PIXELS_X/8)*3+1(a2),d2
                or.w    d0,d2
                move.w  d2,PLAYFIELD_SIZE_X*3(a1)

                move.b  (LOWER_RANK_PIXELS_X/8)*4(a2),d2
                lsl.w   #8,d2
                move.b  (LOWER_RANK_PIXELS_X/8)*4+1(a2),d2
                or.w    d0,d2
                move.w  d2,PLAYFIELD_SIZE_X*4(a1)

                add.w   #(PLAYFIELD_SIZE_X*5),a1
                add.w   #(LOWER_RANK_PIXELS_X/8)*5,a2
                endr

.purple_cycle_guide:
                lea     ROUND_GUIDE_PAL(a4),a0
                addq.w  #1,ROUND_GUIDE_PAL_PTR(a4)
                move.w  ROUND_GUIDE_PAL_PTR(a4),d0
                lsr.w   #1,d0
                move.w  (a0,d0*2),d0
                tst.w   d0
                bpl.s   .purple

                clr.w   ROUND_GUIDE_PAL_PTR(a4)
                moveq   #0,d0
                move.w  (a0,d0*2),d0
		
.purple:        move.l  COPPTR_ROUND_GUIDE_PAL(a4),a0
                move.w  d0,2(a0)


.active_player_flash:
                lea     ACTIVE_PLAYER_PAL(a4),a0
                addq.w  #1,ACTIVE_PLAYER_PTR(a4)
                move.w  ACTIVE_PLAYER_PTR(a4),d0
                move.w  (a0,d0*2),d0
                tst.w   d0
                bpl.s   .player

                clr.w   ACTIVE_PLAYER_PTR(a4)
                moveq   #0,d0
                move.w  (a0,d0*2),d0
.player:        move.l  COPPTR_ACTIVE_PLAYER(a4),a0

                move.w  #$666,2(a0)             ; Player 1
                move.w  #$666,6(a0)             ; Player 2

                move.w  ACTIVE_PLAYER(a4),d1
                move.w  d0,2(a0,d1*4)
                rts


; d0=8x8 x
; d1=8x8 y
; d2=0 = normal font, 1=Time font.
; a0=text
; d6 = Bitplane
DRAW_UPPER_TEXT:
	FUNCID	#$f749e301
	
        move.l  MEMCHK9_UPPER_PANEL(a4),a1
        add.l   d0,a1                                   ; add x position
        lsl.w   #3,d1                                   ; multiply y by 8
        lea     CANVAS_TABLE_Y3(a4),a2
        add.l   (a2,d1*4),a1                            ; add y position

        lea     TAB_Y(a4),a2
        add.w   (a2,d6*2),a1

        moveq   #32,d1
        moveq   #(512/8)-16,d3


.loop:
        ;lea    FONT_ASSET+86(a4),a2    ; start of BODY
        move.l  FONT_ASSET(a4),a2
        add.w   #86,a2                  ; Offset to BODY

        tst.w   d2
        beq.s   .normal_font

.timer_font:
        add.l   d3,a2                   ; timer font.
	;clr.w	$100.w

.normal_font:
        move.b  (a0)+,d0
	tst.b	d0
        beq.s   .exit
        sub.b   d1,d0

.draw:  add.l   d0,a2           ; index into character
        move.b  (a2),(a1)
        move.b  FONT_WIDTH*1(a2),(PLAYFIELD_SIZE_X*5)*1(a1)
        move.b  FONT_WIDTH*2(a2),(PLAYFIELD_SIZE_X*5)*2(a1)
        move.b  FONT_WIDTH*3(a2),(PLAYFIELD_SIZE_X*5)*3(a1)
        move.b  FONT_WIDTH*4(a2),(PLAYFIELD_SIZE_X*5)*4(a1)
        move.b  FONT_WIDTH*5(a2),(PLAYFIELD_SIZE_X*5)*5(a1)
        move.b  FONT_WIDTH*6(a2),(PLAYFIELD_SIZE_X*5)*6(a1)
        addq.w  #1,a1
        bra     .loop
.exit:  rts




INCREASE_RANK:
	FUNCID	#$7ce603d9
		
;        moveq   #0,d0
;        move.w  FRAME_BG(a4),d0
;        divu    #UPGRADE_RANK_EVERY,d0
;        swap    d0
;        tst.w   d0
;        beq.s   .inc
;        bra.s   .exit
;.inc:
;        swap    d0
;        move.w  d0,d1
;        move.w  d0,RYGAR_CURRENT_RANK(a4)

	moveq	#0,d1
	move.w	RYGAR_CURRENT_RANK(a4),d1
        move.l  d1,-(a7)
        moveq   #PLAYER1,d0
        bsr     BLIT_UPPER_PANEL_RANK
        move.l  (a7)+,d1
	
        ;moveq   #PLAYER2,d0
        ;bsr     BLIT_UPPER_PANEL_RANK
	
; Show rank timer.
	;lea	RANKS_TEXT_TAB(a4),a0
	;move.w	RYGAR_CURRENT_RANK(a4),d0
	;move.l	(a0,d0*4),TEXT_BONUS_POINTER(a4)
	;move.w	#300,BONUS_DISPLAY_TIMER
	;bsr	CREATE_BONUS_TEXT
	
.exit:  rts


;d0:0=Player 1 1=Player 2
;d1:rank to blit
;
BLIT_UPPER_PANEL_RANK:
	FUNCID	#$eb99a446
	
        cmp.w   #MAX_RANK,d1
        bgt     .exit

        ;lea    UPPER_PANEL_ASSETS+$b0(a4),a0

        ;move.l  UPPER_PANEL_ASSETS(a4),a0
	move.l	RANKS_MAIN(a4),a0
        add.w   #$B0,a0

        move.l  MEMCHK9_UPPER_PANEL(a4),a1

        tst.w   d0
        beq.s   .player1
        add.w   #22,a1
.player1:

        WAIT_FOR_BLITTER
        move.w  #$09f0,BLTCON0(a5)
        move.w  #$0,BLTCON1(a5)
        move.l  #-1,BLTAFWM(a5)                                 ; Only want the first word

        tst.w   d1
        bne.s   .above_rank1

        addq.w  #6,a1
        move.w  #(512/8)-6,BLTAMOD(a5)                          ; screen size is $a2
        move.w  #PLAYFIELD_SIZE_X-6,BLTDMOD(a5)

        move.l  a0,BLTAPTH(a5)                                  ; Load the mask address
        move.l  a1,BLTDPTH(a5)
        move.w  #(40*64)+3,BLTSIZE(a5)                          ; 8*5 bitplanes
        bra.s   .exit
.above_rank1:
        addq.w  #4,a0                                           ; rank at least 2
        add.w   d1,d1
        add.l   d1,a0
        addq.w  #8,a1                                           ; Offset in from left

        move.w  #(512/8)-2,BLTAMOD(a5)                          ; screen size is $a2
        move.w  #PLAYFIELD_SIZE_X-2,BLTDMOD(a5)
        move.l  a0,BLTAPTH(a5)                                  ; Load the mask address
        move.l  a1,BLTDPTH(a5)
        move.w  #(40*64)+1,BLTSIZE(a5)                          ; 8*5 bitplanes
.exit:  rts



;d0=number of lives
BLIT_RYGAR_LIVES:
	FUNCID	#$db0db639
	
        subq.w  #1,d0
        ;beq.s  .exit
        bmi.s   .exit
        ;lea    LOWER_PANEL_ASSETS(a4)+$b0,a0

        move.l  LOWER_PANEL_ASSETS(a4),a0
        add.w   #$B0,a0

        move.l  MEMCHK9_LOWER_PANEL(a4),a1
        addq.w  #4,a1

        cmp.w   #4,d0
        ble.s   .blit
        moveq   #4,d0
.blit:
        WAIT_FOR_BLITTER
        move.w  #$09f0,BLTCON0(a5)
        move.w  #$0,BLTCON1(a5)
        move.l  #-1,BLTAFWM(a5)                                 ; Only want the first word


        move.w  #(LOWER_RANK_PIXELS_X/8)-2,BLTAMOD(a5)                         ; screen size is $a2
        move.w  #PLAYFIELD_SIZE_X-2,BLTDMOD(a5)
        move.l  a0,BLTAPTH(a5)                                  ; Load the mask address
        move.l  a1,BLTDPTH(a5)
        move.w  #(80*64)+1,BLTSIZE(a5)                          ; 8*5 bitplanes

        addq.w  #2,a1
        dbf     d0,.blit
                                                ; 8*5 bitplanes
.exit:  rts




;d0=0 Power Type off, =1 Type on
;d1=0 Star Power On 1=Crown Power, 2=Tiger, 3=Cross, 4=Sun
BLIT_RYGAR_POWER:
	FUNCID	#$3e8e76ea
	
        ;lea    LOWER_PANEL_ASSETS(a4)+$b0,a0
        move.l  LOWER_PANEL_ASSETS(a4),a0
        add.w   #$B0,a0
        move.l  MEMCHK9_LOWER_PANEL(a4),a1
        add.w   #14,a1                  ; Centre
        addq.w  #2,a0                   ; Default to off.

        add.w   d1,d1
        add.l   d1,a1                   ; Correct position on screen

        tst.w   d0                      ; Power off?
        beq.s   .blit
        add.w   #2,a0
                                        ; power on.
        ;addq.w #2,a1                   ; Index to power on screen
        add.l   d1,a0                   ; Index to correct power

.blit:
        WAIT_FOR_BLITTER
        move.w  #$09f0,BLTCON0(a5)
        move.w  #$0,BLTCON1(a5)
        move.l  #-1,BLTAFWM(a5)                                 ; Only want the first word
        move.w  #(LOWER_RANK_PIXELS_X/8)-2,BLTAMOD(a5)                         ; screen size is $a2
        move.w  #PLAYFIELD_SIZE_X-2,BLTDMOD(a5)
        move.l  a0,BLTAPTH(a5)                                  ; Load the mask address
        move.l  a1,BLTDPTH(a5)
        move.w  #(80*64)+1,BLTSIZE(a5)                          ; 8*5 bitplanes
.exit:  rts


;d0=Round Number
BLIT_RYGAR_ROUND:
	FUNCID	#$cf51bd58

        move.l  LOWER_PANEL_ASSETS(a4),a0
        add.w   #$B0,a0
        move.l  MEMCHK9_LOWER_PANEL(a4),a1

        add.w   #112/8,a0
        add.w   #24,a1                  ; Right

;d0 =1
        add.w   d0,d0                   ; d0=2
        move.w  d0,d1                   ; d1=2
        add.w   d0,d0                   ; d0=4
        add.w   d0,d0                   ; d0=8
        add.w   d1,d0                   ; d0=10

        add.l   d0,a0

.blit:
        WAIT_FOR_BLITTER
        move.w  #$89f0,BLTCON0(a5)
        move.w  #$8000,BLTCON1(a5)
        move.w  #%1111111111111111,BLTAFWM(a5)
        move.w  #$0000,BLTALWM(a5)                                      ; Only want the first word
        move.w  #(LOWER_RANK_PIXELS_X/8)-12,BLTAMOD(a5)                                ; screen size is $a2
        move.w  #PLAYFIELD_SIZE_X-12,BLTDMOD(a5)
        move.l  a0,BLTAPTH(a5)                                  ; Load the mask address
        move.l  a1,BLTDPTH(a5)
        move.w  #(80*64)+6,BLTSIZE(a5)                          ; 8*5 bitplanes
.exit:  rts


CLEAR_PANEL:
	FUNCID	#$cecd14a2
	
.loop:
        moveq   #-1,d0          ;4
        moveq   #-1,d1          ;8
        moveq   #-1,d2          ;12
        moveq   #-1,d3          ;16
        moveq   #-1,d4          ;18
        moveq   #-1,d5          ;20
        moveq   #-1,d6          ;24
        move.l  #-1,a1          ;28
        move.l  #-1,a2          ;32
        move.l  #-1,a3          ;36

        movem.l d0-d6/a1-a3,(a0)
        add.w   #PLAYFIELD_SIZE_X,a0

        moveq   #0,d0
        moveq   #0,d1
        moveq   #0,d2
        moveq   #0,d3
        moveq   #0,d4
        moveq   #0,d5
        moveq   #0,d6
        move.l  #0,a1
        move.l  #0,a2
        move.l  #0,a3

        rept    UPPER_PANEL_PLANES-1
        movem.l d0-d6/a1-a3,(a0)
        add.w   #PLAYFIELD_SIZE_X,a0
        endr

        dbf     d7,.loop

        rts

